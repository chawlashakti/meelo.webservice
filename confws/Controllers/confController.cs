﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Configuration;
using System.Runtime.Caching;
using System.Diagnostics;
using System.Diagnostics.Tracing;
using Com.Meelo.Utils;
using Com.Meelo.Conferencing.WebEx;
using System.Threading.Tasks;
using Com.Meelo.CoreLib;

namespace confws.Controllers
{
    public class confController : ApiController
    {
        public const string URI_SSL = "https://meelo.io/api";

        #region Ping
        [Route("api/conf/oping")]
        public HttpStatusCode GetOPing()
        {
            try
            {


                return HttpStatusCode.OK;



            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);

            }
            //return "OK";

            return HttpStatusCode.InternalServerError;
        }
        [Route("api/conf/ping")]
        public HttpStatusCode GetPing()
        {
            try
            {
                if (RESTApiUtils.ValidateHashPassword(Request))
                {

                    return HttpStatusCode.OK;

                }
                else
                {
                    return HttpStatusCode.Forbidden;

                }

            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                return HttpStatusCode.InternalServerError;
            }
       

           
        }

        #endregion

        #region User Service
        [Route("api/webex/user")]
        public async Task<User> GetUser(WebExSecurityContext securityContext)
        {
            try
            {
                if (RESTApiUtils.ValidateHashPassword(Request))
                {
                    // return await GameUtils.GetRandomGameTemplate(level, displayunits, culture, StorageHelper);

                    return null;
                }
                else
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Forbidden) { ReasonPhrase = "BadHash" });

                }

            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                throw ex;
            }

        }
        #endregion

        #region General Session Service

        #endregion

        #region Meeting Service

        #endregion

        #region Meeting Attendee Service

        #endregion
        /*
        #region Login
        [Route("api/wwtsam/login")]
        public HttpResponseMessage PostUser(FBUser user)
        {
            try
            {
                if (WwtsamRESTApiUtils.ValidateHashPassword(Request))
                {

                    RegisterUserResult lu = UserService.RegisterIfDoesNotExist(user);


                    var response = Request.CreateResponse<RegisterUserResult>(HttpStatusCode.Created, lu);

                    UriBuilder ub = new UriBuilder(string.Format("{0}/{1}", URI_SSL, lu.Session.Tid));
                    response.Headers.Location = ub.Uri;
                    return response;

                }
                else
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Forbidden) { ReasonPhrase = "BadHash" });

                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                var response = Request.CreateResponse<string>(HttpStatusCode.InternalServerError, ex.Message);
                UriBuilder ub = new UriBuilder(string.Format("{0}/{1}", URI_SSL, 0));
                response.Headers.Location = ub.Uri;
                return response;
            }

        }
       
        #endregion

        #region User Updates
       

        [Route("api/wwtsam/u/{provider:minlength(3)}/{id:minlength(5)}/level/{level:int:min(0)}")]
        public HttpResponseMessage PostUpdateLevel(string provider, string id, int level)
        {
            if (WwtsamRESTApiUtils.ValidateHashPassword(Request))
            {

                bool ret = (level > 0) ? UserService.UpdateLevel(provider, id, level) : UserService.ClearLevel(provider, id);

                var response = Request.CreateResponse<bool>(HttpStatusCode.Accepted, ret);

                UriBuilder ub = new UriBuilder(string.Format("{0}/{1}/{2}", URI_SSL, provider, id));
                response.Headers.Location = ub.Uri;
                return response;

            }
            else
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Forbidden) { ReasonPhrase = "BadHash" });

            }

        }

      

       
        [Route("api/wwtsam/u/{provider:minlength(3)}/{id:minlength(5)}/bb/{balance:long:min(1)}/addbalance")]
        public HttpResponseMessage PostAddBankBalance(string provider, string id, long balance)
        {
            if (WwtsamRESTApiUtils.ValidateHashPassword(Request))
            {

                bool ret = UserService.AddBankBalance(provider, id, balance);

                var response = Request.CreateResponse<bool>(HttpStatusCode.Accepted, ret);

                UriBuilder ub = new UriBuilder(string.Format("{0}/{1}/{2}", URI_SSL, provider, id));
                response.Headers.Location = ub.Uri;
                return response;

            }
            else
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Forbidden) { ReasonPhrase = "BadHash" });

            }

        }

        #region Achievements

      


        [Route("api/wwtsam/u/{provider:minlength(3)}/{id:minlength(5)}/ach/addachievement")]
        public HttpResponseMessage PostAddAchievement(string provider, string id, Achievement achievement)
        {
            try
            {
                if (WwtsamRESTApiUtils.ValidateHashPassword(Request))
                {

                    bool ret = UserService.AddAchievement(provider, id, achievement);

                    var response = Request.CreateResponse<bool>(HttpStatusCode.Created, ret);

                    UriBuilder ub = new UriBuilder(string.Format("{0}/{1}/{2}", URI_SSL, provider, id));
                    response.Headers.Location = ub.Uri;
                    return response;

                }
                else
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Forbidden) { ReasonPhrase = "BadHash" });

                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                var response = Request.CreateResponse<string>(HttpStatusCode.InternalServerError, ex.Message);
                UriBuilder ub = new UriBuilder(string.Format("{0}/{1}", URI_SSL, 0));
                response.Headers.Location = ub.Uri;
                return response;
            }

        }

       

        [Route("api/wwtsam/u/{provider:minlength(3)}/{id:minlength(5)}/getachievements")]
        public IEnumerable<int> GetAchievements(string provider, string id)
        {
            try
            {
                if (WwtsamRESTApiUtils.ValidateHashPassword(Request))
                {

                    return UserService.GetAchievements(provider, id);

                }
                else
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Forbidden) { ReasonPhrase = "BadHash" });

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

       
       

        [Route("api/wwtsam/u/{provider:minlength(3)}/{id:minlength(5)}/resetperformance")]
        public HttpResponseMessage PostResetUserPerformance(string provider, string id)
        {
            if (WwtsamRESTApiUtils.ValidateHashPassword(Request))
            {

                bool ret = UserService.ResetUserPerformance(provider, id);

                var response = Request.CreateResponse<bool>(HttpStatusCode.Accepted, ret);

                UriBuilder ub = new UriBuilder(string.Format("{0}/{1}/{2}", URI_SSL, provider, id));
                response.Headers.Location = ub.Uri;
                return response;

            }
            else
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Forbidden) { ReasonPhrase = "BadHash" });

            }
        }
        #endregion

        #region Leaderboard

        [Route("api/wwtsam/leaderboard/{count:int:min(1)}")]
        public IEnumerable<Leader> GetLeaderboard(int count = 100)
        {
            try
            {
                if (WwtsamRESTApiUtils.ValidateHashPassword(Request))
                {

                    return UserService.GetLeaders(count);

                }
                else
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Forbidden) { ReasonPhrase = "BadHash" });

                }

            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                // throw ex;
            }
            return null;
        }
        #endregion
        #region Game Templates

        [Route("api/wwtsam/gametemplate/uri/{culture:minlength(3)}/{level:int:min(1)}/{displayunits:int:min(1)}")]
        public string GetRandomGameTemplateUri(string culture, int level, int displayunits)
        {
            try
            {
                if (WwtsamRESTApiUtils.ValidateHashPassword(Request))
                {
                    return GameUtils.GetRandomGameTemplateUri(level, displayunits, culture, StorageHelper);


                }
                else
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Forbidden) { ReasonPhrase = "BadHash" });

                }

            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                throw ex;
            }

        }


        [Route("api/wwtsam/gametemplate/uri/{culture:minlength(3)}/{level:int:min(1)}/{displayunits:int:min(1)}/{provider:int:min(1)}")]
        public string GetRandomGameTemplateUriByProvider(string culture, int level, int displayunits, int provider)
        {
            try
            {
                if (WwtsamRESTApiUtils.ValidateHashPassword(Request))
                {

                    return GameUtils.GetRandomGameTemplateUriByProvider(level, displayunits, culture, provider.ToEnum<ProductDataProvider>(), StorageHelper);


                }
                else
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Forbidden) { ReasonPhrase = "BadHash" });

                }

            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                throw ex;
            }

        }

        [Route("api/wwtsam/gametemplate/{culture:minlength(3)}/{level:int:min(1)}/{displayunits:int:min(1)}")]
        public async Task<GameTemplate> GetRandomGameTemplate(string culture, int level, int displayunits)
        {
            try
            {
                if (WwtsamRESTApiUtils.ValidateHashPassword(Request))
                {
                    return await GameUtils.GetRandomGameTemplate(level, displayunits, culture, StorageHelper);


                }
                else
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Forbidden) { ReasonPhrase = "BadHash" });

                }

            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                throw ex;
            }

        }

        [Route("api/wwtsam/gametemplate/{culture:minlength(3)}/{level:int:min(1)}/{displayunits:int:min(1)}/{provider:int:min(1)}")]
        public async Task<GameTemplate> GetRandomGameTemplateByProvider(string culture, int level, int displayunits, int provider)
        {
            try
            {
                if (WwtsamRESTApiUtils.ValidateHashPassword(Request))
                {
                    return await GameUtils.GetRandomGameTemplateByProvider(level, displayunits, culture, provider.ToEnum<ProductDataProvider>(), StorageHelper);


                }
                else
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Forbidden) { ReasonPhrase = "BadHash" });

                }

            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                throw ex;
            }

        }

        [Route("api/wwtsam/gt/u/c/{culture:minlength(3)}/{level:int:min(1)}/{displayunits:int:min(1)}/{category:minlength(3)}")]
        public string GetRandomGameTemplateUriByCategory(string culture, int level, int displayunits, string category)
        {
            try
            {
                if (WwtsamRESTApiUtils.ValidateHashPassword(Request))
                {
                    ICategoryGameTemplate cm = new CategoryTemplateDBService();
                    var s = cm.GetRandomCategoryGameTemplateUrls(culture, level, displayunits, category);

                    if (s != null && s.Count() > 0)
                    {
                        return s.FirstOrDefault();
                    }
                    else
                    {
                        return GetRandomGameTemplateUri(culture, level, displayunits);
                    }


                }
                else
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Forbidden) { ReasonPhrase = "BadHash" });

                }

            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                throw ex;
            }

        }

        [Route("api/wwtsam/gametemplate/sas")]
        public string GetGameTemplateContainerSharedAccessSignature()
        {
            try
            {
                if (WwtsamRESTApiUtils.ValidateHashPassword(Request))
                {

                    return StorageHelper.GetSharedAccessSignatureForPackageContainer(WwtsamNamingStrategy.GetGameTemplateContainerName(), "wwtsampublicreadonly", 43800);


                }
                else
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Forbidden) { ReasonPhrase = "BadHash" });

                }

            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                throw ex;
            }

        }
        #endregion

        #region Logging

        [Route("api/wwtsam/logs/sas")]
        public string GetLogsContainerUrl()
        {
            try
            {
                if (WwtsamRESTApiUtils.ValidateHashPassword(Request))
                {

                    return ConfigUtils.GetConfigValueAsString("LogsContainerUrl");


                }
                else
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Forbidden) { ReasonPhrase = "BadHash" });

                }

            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                // throw ex;
            }

            return null;

        }
        #endregion

        #region Categories


        [Route("api/wwtsam/categories")]
        public IEnumerable<string> GetCategories()
        {
            try
            {
                if (WwtsamRESTApiUtils.ValidateHashPassword(Request))
                {
                    ICategoryGameTemplate pSvc = new CategoryTemplateDBService();
                    return pSvc.GetAllCategoriesWithContent();

                }
                else
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Forbidden) { ReasonPhrase = "BadHash" });

                }

            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                // throw ex;
            }
            return null;
        }

        [Route("api/wwtsam/categories/d")]
        public IEnumerable<CategoryGameTemplate> GetDistinctCategoriesForEachLevel()
        {
            try
            {
                if (WwtsamRESTApiUtils.ValidateHashPassword(Request))
                {
                    ICategoryGameTemplate pSvc = new CategoryTemplateDBService();
                    return pSvc.GetAllDistinctCategoriesWithContent();

                }
                else
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Forbidden) { ReasonPhrase = "BadHash" });

                }

            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                // throw ex;
            }
            return null;
        }

        [Route("api/wwtsam/categories/{culture:minlength(3)}/{level:int:min(1)}/{displayunits:int:min(1)}")]
        public IEnumerable<string> GetCategoriesByLevel(string culture, int level, int displayunits)
        {
            try
            {
                if (WwtsamRESTApiUtils.ValidateHashPassword(Request))
                {
                    ICategoryGameTemplate pSvc = new CategoryTemplateDBService();
                    return pSvc.GetAllCategoriesWithContent(level, displayunits, culture);

                }
                else
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Forbidden) { ReasonPhrase = "BadHash" });

                }

            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                // throw ex;
            }
            return null;
        }
        #endregion

        #region Shopping Cart

        [Route("api/wwtsam/carts/sas")]
        public string GetCartsContainerUrl()
        {
            try
            {
                if (WwtsamRESTApiUtils.ValidateHashPassword(Request))
                {

                    return ConfigUtils.GetConfigValueAsString("CartsContainerUrl");


                }
                else
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Forbidden) { ReasonPhrase = "BadHash" });

                }

            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                // throw ex;
            }

            return null;

        }
        #endregion
    */
        // GET: api/conf
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/conf/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/conf
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/conf/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/conf/5
        public void Delete(int id)
        {
        }
    }
}
