﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeolocationServiceConsole
{
    public enum Operation
    {
        Create = 1
    }

    public enum ObjectType
    {
        Workspace=1,
        Person=2,
        Location=3
    }
    public class Configuration
    {
        public Operation Operation { get; set; }
        public ObjectType ObjectType
        {
            get;set;
        }
        public long TotalCount { get; set; }

    }
}
