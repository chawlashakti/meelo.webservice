﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tectil.NCommand.Contract;
using Tectil.NCommand;
using Com.Meelo.Geolocation;
using GoogleMaps.LocationServices;

namespace GeolocationServiceConsole
{
    class Program
    {
        [Command(description: "Get Lat-Long from address", Name ="latlong")]
        public string LatLong(
           [Argument(description: "Type address")] string address
       )
        {
            MapPoint point = GeolocationService.GetLatLongFromAddress(address);
            if (point != null)
            {
                return string.Format("Latitude:{0}, Longitude: {1}", point.Latitude, point.Longitude);
            }else
            {
                return "LatLong not returned";
            }
        }

        [Command(description: "Get address from lat-long", Name = "address")]
        public string Address(
          [Argument(description: "latitude")] double latitude,
           [Argument(description: "longitude")] double longitude
      )
        {
            var reversedAddress = GeolocationService.GetAddressFromLatLang(latitude, longitude);

           
            if (reversedAddress != null)
            {
                return string.Format("Reversed Address from ({1},{2}) is {0}", reversedAddress, latitude, longitude);
            }
            else
            {
                return "Address not returned";
            }
        }

        [Command(description: "Get address list from address", Name = "addresslist")]
        public string AddressList(
          [Argument(description: "Type address")] string address
      )
        {
            string[] addresses = GeolocationService.GetAddressesListFromAddress(address);
            if (addresses != null)
            {
                StringBuilder strb = new StringBuilder();
                foreach (string s in addresses)
                {
                    strb.AppendLine(s);
                }

                return strb.ToString();
            }
            else
            {
                return "LatLong not returned";
            }
        }
        [Command(description: "Test Address", Name = "testaddress")]
        public string TestAddress(
           [Argument(description: "Type address")] string address
       )
        {
            MapPoint point = GeolocationService.GetLatLongFromAddress(address);
            if (point != null)
            {
                string addr = string.Format("Latitude:{0}, Longitude: {1}", point.Latitude, point.Longitude);

                string reverseAddress = Address(point.Latitude, point.Longitude);

                return string.Format("{0}\n{1}", addr, reverseAddress);
            }
            else
            {
                return "Test failed!!!";
            }
        }
        static void Main(string[] args)
        {
            NCommands commands = new NCommands();
            commands.RunConsole(args);
        }
    }
}
