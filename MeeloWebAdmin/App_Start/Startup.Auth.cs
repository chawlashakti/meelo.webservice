﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Claims;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OpenIdConnect;
using Owin;
using MeeloWebAdmin.Models;
using MeeloWebAdmin.Helper;
using MeeloWebAdmin.Controllers;

namespace MeeloWebAdmin
{
    public partial class Startup
    {
  
        private ApplicationDbContext db = new ApplicationDbContext();

        public void ConfigureAuth(IAppBuilder app)
        {

            app.SetDefaultSignInAsAuthenticationType(CookieAuthenticationDefaults.AuthenticationType);
            app.UseKentorOwinCookieSaver();
            app.UseCookieAuthentication(new CookieAuthenticationOptions { });

            app.UseOpenIdConnectAuthentication(
                new OpenIdConnectAuthenticationOptions
                {
                    ClientId = AuthenticationHelper.clientId,
                    Authority = AuthenticationHelper.authority,
                    RedirectUri = AuthenticationHelper.ReplyURLs,
                    TokenValidationParameters = new System.IdentityModel.Tokens.TokenValidationParameters
                    {
                        // instead of using the default validation (validating against a single issuer value, as we do in line of business apps), 
                        // we inject our own multitenant validation logic
                        ValidateIssuer = false,
                    },
                    Notifications = new OpenIdConnectAuthenticationNotifications()
                    {
                        SecurityTokenValidated = (context) =>
                        {
                            return Task.FromResult(0);
                        },
                        AuthorizationCodeReceived = (context) =>
                        {
                            var code = context.Code;

                            ClientCredential credential = new ClientCredential(AuthenticationHelper.clientId, AuthenticationHelper.appKey);
                            string tenantID = context.AuthenticationTicket.Identity.FindFirst("http://schemas.microsoft.com/identity/claims/tenantid").Value;
                            string signedInUserID = context.AuthenticationTicket.Identity.FindFirst(ClaimTypes.NameIdentifier).Value;
                            //HttpContext.Current.Session["tenantID"] =tenantID; //"6870dce1-da05-427f-9ec9-227908686627"; 
                            AuthenticationContext authContext = new AuthenticationContext(AuthenticationHelper.aadInstance + tenantID, new ADALTokenCache(signedInUserID));
                            AuthenticationResult result = authContext.AcquireTokenByAuthorizationCodeAsync(
                                code, new Uri(/*HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Path)*/AuthenticationHelper.ReplyURLs), credential, AuthenticationHelper.graphResourceID).Result;
                            //new OrganizationController().Index();
                            return Task.FromResult(0);
                        },
                        AuthenticationFailed = (context) =>
                        {
                            //context.OwinContext.Response.Redirect("/Home/Error");
                            //context.HandleResponse(); // Suppress the exception
                            return Task.FromResult(0);
                        }
                    }
                });

        }
    }
}
