﻿using Com.Meelo.BusinessServices.UserProfile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MeeloWebAdmin.Controllers
{
    [Authorize]
    public class UserProfileController : Controller
    {
        IUserProfileService iUserProfileService;
        public ActionResult Index()
        {
            return View();
        }
        public async Task<JsonResult> UsersList(int jtStartIndex, int jtPageSize, string jtSorting = null, string name = "")
        {
            string TenantId = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/tenantid").Value;
            try
            {
                iUserProfileService = new UserProfileService();
                var users = await iUserProfileService.GetUsers(TenantId);

                return Json(new { Result = "OK", Records = users, TotalRecordCount = users.Count() });
            }

            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
    }
}