﻿ using Com.Meelo.BusinessServices.Building;
using Com.Meelo.BusinessServices.Workspace;
using Com.Meelo.CoreLib;
using ExcelDataReader;
using MeeloWebAdmin.Helper;
using Microsoft.Azure.ActiveDirectory.GraphClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Configuration;
using Com.Meelo.Utils.Azure.Data.Wrappers;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage;
using Microsoft.Azure.ActiveDirectory.GraphClient.Extensions;

namespace MeeloWebAdmin.Controllers
{
    [Authorize]
    public class WorkspaceController : Controller
    {
        IWorkspaceService iWorkspaceServiceObj;
        IBuildingService iBuildingService;
        public ActionResult Index()
        {
            if (TempData["Message"] != null)
            {
                ViewBag.Message = TempData["Message"].ToString();
                ViewBag.Success = Convert.ToBoolean(TempData["Success"]);
            }
            if (TempData["SuccessRecords"] != null)
            {
                ViewBag.SuccessRecords = TempData["SuccessRecords"].ToString();
                ViewBag.FailureRecords = TempData["FailureRecords"].ToString();
            }


            return View();
        }
        
        public async Task<JsonResult> WorkspaceList(int jtStartIndex, int jtPageSize, string jtSorting = null, string name = "")
        {
            Organization Org = System.Web.HttpContext.Current.Session["Organization"] as Organization;
            iWorkspaceServiceObj = new WorkspaceService();
            var Output = await iWorkspaceServiceObj.GetWorkspacesList(Org.OrgId,jtStartIndex, jtPageSize, jtSorting,name);
            return Json(new { Result = "OK", Records = Output.WorkspaceList, TotalRecordCount = Output.Count });//Workspaces.Count() });
        }
       
        public ActionResult Add(long? WorkspaceId=0)
        {
            if (TempData["Message"] != null)
            {
                ViewBag.Message = TempData["Message"].ToString();
                ViewBag.Success = Convert.ToBoolean(TempData["Success"]);
            }
            long WsId = WorkspaceId??0;
            Organization Org = System.Web.HttpContext.Current.Session["Organization"] as Organization;
            Workspace ws = new Workspace();
            ws.Id = WsId;
            WorkspaceOutput Output = new WorkspaceOutput();

            if (WorkspaceId > 0)
            {
                WorkspaceInput Input = new WorkspaceInput();
                Input.Workspace = ws;
                Input.Workspace.Id = WsId;
                Input.Workspace.OrgId = Input.OrgId = Org.OrgId;
                iWorkspaceServiceObj = new WorkspaceService();
                Output = iWorkspaceServiceObj.GetWorkspacesById(Input).Result;
                ws = Output.WorkspaceList.FirstOrDefault();
            }
            else
            {
                ws.LastRenovated = DateTime.UtcNow;
                ws.Address = new Address();
            }
            iBuildingService = new BuildingService();
            var BuildingOutput = iBuildingService.GetBuilding(Org.OrgId, null).Result;
            BuildingOutput.buildings.Insert(0, new Building { BuildingName = "--Select Building--", Id = 0 });
            ViewBag.Buildings =BuildingOutput.buildings; //new SelectList(BuildingOutput.buildings, "Id", "BuildingName", ws.Building.Id); 
            ViewBag.Country= Countries.CountryList;
            if(ws.Building==null)
            ws.Building = new Building();
            return View(ws);
        }
        public ActionResult AddUpdate(Workspace WorkspaceObj, HttpPostedFileBase photo)
        {
            AddWorkspaceOutput AddOutput = new AddWorkspaceOutput();
            bool IsUpdate = false; ;
            try
            {
                Organization Org = System.Web.HttpContext.Current.Session["Organization"] as Organization;
                //var blobStorageConnectionString = ConfigurationManager.AppSettings["BlobStorageConnectionString"];

                AddWorkspaceInput WorkspaceInput = new AddWorkspaceInput();
                WorkspaceInput.WorkspaceList = new List<Workspace> { WorkspaceObj };
                iWorkspaceServiceObj = new WorkspaceService();
                WorkspaceObj.LastUpdated = DateTime.UtcNow;
                WorkspaceObj.NextReservationStartTime = DateTime.UtcNow;
                WorkspaceObj.NextReservationEndTime = DateTime.UtcNow;
                WorkspaceObj.OrgId = Org.OrgId;
               
                if (WorkspaceObj.Id == 0)
                {
                    //if (WorkspaceObj.Floor == null)
                    //    WorkspaceObj.Floor = new Floor { Name = WorkspaceObj.Address.Floor };
                    WorkspaceObj.Created = DateTime.UtcNow;
                    AddOutput = iWorkspaceServiceObj.AddWorkspace(WorkspaceInput).Result;
                    AddOutput.Message = "Workspace saved successfully";
                    AddOutput.IsCallSuccessful = true;
                }
                else
                {
                    WorkspaceInput.Workspace = WorkspaceObj;
                    AddOutput = iWorkspaceServiceObj.UpdateWorkspace(WorkspaceInput).Result;
                    AddOutput.Message = "Workspace updated successfully";
                    AddOutput.IsCallSuccessful = true;
                    IsUpdate = true;
                }
                if (photo != null && AddOutput!=null)
                {
                    Com.Meelo.Utils.Azure.WABlobStorageHelper blobHelper = new Com.Meelo.Utils.Azure.WABlobStorageHelper("BlobStorageConnectionString");

                    Com.Meelo.Utils.Azure.WABlobStorageHelper cloudStorageAccount = new Com.Meelo.Utils.Azure.WABlobStorageHelper("BlobStorageConnectionString");
                    CloudBlobClient cloudBlobClient = cloudStorageAccount.BlobClient;
                    CloudBlobContainer cloudBlobContainer = blobHelper.GetBlobContainer("org-" + Org.OrgId);
                    cloudBlobContainer.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });
                    string extension = Path.GetExtension(photo.FileName);
                    CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference("workspace-" + AddOutput.Workspace.Id + "/img-1" + extension);
                    cloudBlockBlob.Properties.ContentType = photo.ContentType;
                    cloudBlockBlob.UploadFromStream(photo.InputStream);
                    WorkspaceObj.PhotoLinks = cloudBlockBlob.Uri.ToString();

                    WorkspaceInput.Workspace = WorkspaceObj;
                    AddOutput = iWorkspaceServiceObj.UpdateWorkspace(WorkspaceInput).Result;
                    AddOutput.Message = "Workspace saved successfully";
                    AddOutput.IsCallSuccessful = true;
                }
                
                iBuildingService = new BuildingService();
                var BuildingOutput = iBuildingService.GetBuilding(Org.OrgId, null).Result;
                ViewBag.Buildings = BuildingOutput.buildings;
                ViewBag.Country = Countries.CountryList;
            }
            catch(Exception ex)
            {
                AddOutput.Message = "<strong> Sorry, an error has occurred</strong><br/><p>Unfortunately an error has occurred during the processing of your page request. We apologize for the inconvenience, Please refresh the page or try again later.</p><br/><p>Error description: " + ex.Message + "</p>";
                AddOutput.IsCallSuccessful = false;
            }
            ViewBag.Message = AddOutput.Message;
            ViewBag.Success = AddOutput.IsCallSuccessful;
            if (IsUpdate)
                return View("Add", WorkspaceObj);
            else
            {
                TempData["Message"] = AddOutput.Message;
                TempData["Success"] = AddOutput.IsCallSuccessful;
                return this.RedirectToAction("Add", new { WorkspaceId = 0 });
            }
        }
        public PartialViewResult WorkspaceSetting(Int64 WorkspaceId)
        {
            string tenantID = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/tenantid").Value;
            Organization Org = System.Web.HttpContext.Current.Session["Organization"] as Organization;
            iWorkspaceServiceObj = new WorkspaceService();
            WorkspaceInput Input = new WorkspaceInput();
            Input.Workspace = new Workspace();
            Input.Workspace.Id = WorkspaceId;
            Input.Workspace.OrgId = Input.OrgId = Org.OrgId;
           var Output= iWorkspaceServiceObj.GetWorkspacesById(Input).Result;
            if (Output.WorkspaceList!=null && Output.WorkspaceList.Count > 0)
                return PartialView("WorkspacePartialView", Output.WorkspaceList[0]);
            else return null;
        }
        public ActionResult Upload(string ReturnUrl, HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {
                if (upload != null && upload.ContentLength > 0)
                {
                    Stream stream = upload.InputStream;
                    IExcelDataReader reader = null;
                    if (upload.FileName.EndsWith(".xls"))
                    {
                        reader = ExcelReaderFactory.CreateBinaryReader(stream);
                    }
                    else if (upload.FileName.EndsWith(".xlsx"))
                    {
                        reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    }
                    else
                    {
                        ModelState.AddModelError("File", "This file format is not supported");
                        TempData["Message"] = "<strong>This file format is not supported</strong>";
                        TempData["Success"] = false;
                        //return RedirectToAction("../TenantOnboarding/Index#tab1");
                        return new RedirectResult(Url.Action(ReturnUrl) + "#tab1");
                    }
                    DataSet DataSetObj = reader.AsDataSet(new ExcelDataSetConfiguration() { ConfigureDataTable = (_) => new ExcelDataTableConfiguration() { UseHeaderRow = true } });
                    iWorkspaceServiceObj = new WorkspaceService();
                    List<Com.Meelo.CoreLib.Workspace> WorkspaceList = new List<Com.Meelo.CoreLib.Workspace>();
                    Organization Org = System.Web.HttpContext.Current.Session["Organization"] as Organization;
                    var Result =iWorkspaceServiceObj.AddWorkspace(DataSetObj.Tables[0]).Result;
                    if (Result.Message == null)
                        Result.Message = "Workspace Import Details";
                    TempData["Message"] = Result.Message;
                    TempData["Success"] = Result.IsCallSuccessful;
                    TempData["SuccessRecords"] = Result.SuccessRecordsCount;
                    TempData["FailureRecords"] = Result.FailureRecordsCount;

                }
            }
            return new RedirectResult(Url.Action(ReturnUrl) + "#tab1");
        }
       public ActionResult ExportWorkspaces()
        {
            return View();
        }
        public async Task<JsonResult> ExportWorkspacesfromO365()
        {
            //Users and workspaces
            Organization Org = System.Web.HttpContext.Current.Session["Organization"] as Organization;
            ActiveDirectoryClient client = AuthenticationHelper.GetActiveDirectoryClientAsApplication();

            //GraphApiHelper helper = new GraphApiHelper();
            //var Users = helper.UsersList(client);
            List<IUser> Users = new List<IUser>();
            IPagedCollection<IUser> pagedCollection;
            pagedCollection = await client.Users.ExecuteAsync();
            while (pagedCollection != null)
            {
                var usersList = pagedCollection.CurrentPage.ToList();
                Users.AddRange(usersList.Cast<Microsoft.Azure.ActiveDirectory.GraphClient.User>());
                pagedCollection = await pagedCollection.GetNextPageAsync();
            }
            List<Com.Meelo.CoreLib.User> usersListObj = new List<Com.Meelo.CoreLib.User>();
            List<Workspace> workspaceListObj = new List<Workspace>();
            AddWorkspaceOutput Output = new AddWorkspaceOutput();
            foreach (var usr in Users)
            {
                if (usr.DisplayName.ToLower().Contains("conf") || usr.DisplayName.ToLower().Contains("room") || usr.DisplayName.ToLower().Contains("workspace"))
                {
                    Workspace workspaceObj = new Workspace
                    {
                        OrgId = Org.OrgId,
                        Name = usr.DisplayName,
                        Description = usr.GivenName,
                        FriendlyName = usr.DisplayName,
                        IdmResourceIdentity = null,
                        Address = new Com.Meelo.CoreLib.Address
                        {
                            City = usr.City,
                            Country = usr.Country,
                            PostalCode = usr.PostalCode,
                            Street = usr.StreetAddress,
                            State = usr.State
                        },
                        WebLink = null,
                        Photos = null,
                        LimitedAccessRoles = new List<RoleType> { RoleType.User },
                        LimitedAccessUsers = null,
                        Calendar = new Calendar { },
                        //Floor = new Floor { },
                        Building = new Building { Address = new Address { GpsLocation = new GpsLocation { } } },
                        Ratings = 0,
                        Created = DateTime.Now,
                        LastUpdated = DateTime.Now,
                        NextReservationStartTime = DateTime.Now,
                        NextReservationEndTime = DateTime.Now,
                        PhotoLinks = null,
                        Tags = new Dictionary<string, string> { { string.Empty, string.Empty } },
                        Maps = null,
                        WorkspaceType = 0,
                        OccupancyStatus = 0,
                        Capacity = 0,
                        //Capabilities = null,
                        LastRenovated = DateTime.Now,
                        CommunicationInfo = new CommunicationInfo
                        {
                            PhoneNumber = usr.Mobile,
                            TelephonyDetails = null,
                            AccessDescription = null,
                            OtherInfo = null
                        },
                        IsAutoCheckinEnabled = false,
                        IsAutoCheckoutEnabled = false,
                        AccessType = WorkspaceAccessType.Public,
                        MaintenanceOwner = new Com.Meelo.CoreLib.User { },
                        AreaInSqFt = 0,
                        DistanceFromFloorOrigin = null,
                        HasControllableDoor = false,
                        BeaconUid = null,
                        IsBlocked = false,
                        IsHidden = false,
                        ObjectId = usr.ObjectId

                    };
                    workspaceListObj.Add(workspaceObj);
                }
            }
            if (workspaceListObj != null && workspaceListObj.Count() > 0)
            {
                iWorkspaceServiceObj = new WorkspaceService();
                //iBuildingService = new BuildingService();
                //Building BuildingObj = new Building();
                //BuildingObj = workspaceListObj.FirstOrDefault().Building;
                //BuildingObj.OrgId = Org.OrgId;
                //BuildingObj.Address = workspaceListObj.FirstOrDefault().Address;
                //BuildingObj.Address.GpsLocation = new GpsLocation { };
                //BuildingObj.Id = 0;
                //AddBuildingInput BLdInput = new AddBuildingInput();
                //BLdInput.building = BuildingObj;
                AddWorkspaceInput WorkspaceInput = new AddWorkspaceInput();
                WorkspaceInput.WorkspaceList = workspaceListObj;
                Output = iWorkspaceServiceObj.AddWorkspaces(WorkspaceInput).Result;
                if (Output.Message == null)
                    Output.Message = "Workspace Import Details";
            }
            return Json(new { Result = "OK", Records = Output });
        }
    }
}