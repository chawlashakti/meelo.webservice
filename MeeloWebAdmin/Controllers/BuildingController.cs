﻿using Com.Meelo.BusinessServices.Building;
using Com.Meelo.CoreLib;
using Com.Meelo.CoreLib.ViewModel;
using ExcelDataReader;
using MeeloWebAdmin.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MeeloWebAdmin.Controllers
{
    [Authorize]
    public class BuildingController : Controller
    {
        IBuildingService iBuildingServiceObj;
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult Add(Building BuildingObj)
        {
            Organization Org = System.Web.HttpContext.Current.Session["Organization"] as Organization;
            if (Org != null)
            {
                iBuildingServiceObj = new BuildingService();
                AddBuildingInput Input = new AddBuildingInput();
                Input.building = BuildingObj;
                Input.OrgId = Input.building.OrgId = Org.OrgId;
                var Output = iBuildingServiceObj.AddBuilding(Input).Result;

                return Json(new { Result = "OK", Record = Output.building });
            }
            return Json(new { Result = "Failed", Record = 0 });

        }
        public JsonResult Update(Building BuildingObj)
        {
            Organization Org = System.Web.HttpContext.Current.Session["Organization"] as Organization;
            if (Org != null)
            {
                iBuildingServiceObj = new BuildingService();
                AddBuildingInput Input = new AddBuildingInput();
                Input.building = BuildingObj;
                Input.OrgId = Input.building.OrgId = Org.OrgId;
                var Output = iBuildingServiceObj.UpdateBuilding(Input).Result;

                return Json(new { Result = "OK", Record = Output.building });
            }
            return Json(new { Result = "Failed", Record = 0 });

        }
        public async Task<JsonResult> BuildingList(int jtStartIndex, int jtPageSize, string jtSorting = null, string FilterbyBuilding = null)
        {
            Organization Org = System.Web.HttpContext.Current.Session["Organization"] as Organization;
            iBuildingServiceObj = new BuildingService();
            var Output = await iBuildingServiceObj.GetBuilding(Org.OrgId, jtStartIndex, jtPageSize, jtSorting, FilterbyBuilding);
            //if (Buildings == null)
            //    Buildings = new BuildingOutput { building = new Building {Address=new Address {GpsLocation=new GpsLocation() } } };
            return Json(new { Result = "OK", Records = Output.buildings, TotalRecordCount = Output.Count });//Workspaces.Count() });
        }
        public JsonResult GetCountries()
        {
            try
            {
                List<OptionsViewModel> Options = new List<OptionsViewModel>();
                foreach(SelectListItem items in Countries.CountryList)
                {
                    OptionsViewModel Option = new OptionsViewModel();
                    Option.DisplayText = items.Text;
                    Option.Value = items.Value;
                    Options.Add(Option);
                }
                return Json(new { Result = "OK", Options = Options });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        //public PartialViewResult WorkspaceSetting(Int64 WorkspaceId)
        //{
        //    string tenantID = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/tenantid").Value;
        //    iBuildingServiceObj = new BuildingService();
        //    BuildingInput Input = new BuildingInput();
        //    Input.Building.Id = WorkspaceId;
        //    var Output = iBuildingServiceObj.GetBuildingsById(Input).Result;
        //    return PartialView("WorkspacePartialView", Output);
        //}
        //public ActionResult Upload(HttpPostedFileBase upload)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        if (upload != null && upload.ContentLength > 0)
        //        {
        //            Stream stream = upload.InputStream;
        //            IExcelDataReader reader = null;
        //            if (upload.FileName.EndsWith(".xls"))
        //            {
        //                reader = ExcelReaderFactory.CreateBinaryReader(stream);
        //            }
        //            else if (upload.FileName.EndsWith(".xlsx"))
        //            {
        //                reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
        //            }
        //            else
        //            {
        //                ModelState.AddModelError("File", "This file format is not supported");
        //                return View();
        //            }
        //            DataSet DataSetObj = reader.AsDataSet(new ExcelDataSetConfiguration() { ConfigureDataTable = (_) => new ExcelDataTableConfiguration() { UseHeaderRow = true } });
        //            iBuildingServiceObj = new BuildingService();
        //            List<Com.Meelo.CoreLib.Workspace> WorkspaceList = new List<Com.Meelo.CoreLib.Workspace>();
        //            var Result = iWorkspaceServiceObj.AddWorkspace(DataSetObj.Tables[0]).Result;

        //            TempData["Message"] = Result.Message;
        //            TempData["Success"] = Result.IsCallSuccessful;

        //        }
        //    }
        //    return RedirectToAction("Index");
        //}
    }
}
