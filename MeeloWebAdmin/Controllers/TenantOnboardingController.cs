﻿using Com.Meelo.CoreLib;
using MeeloWebAdmin.Helper;
using Microsoft.Azure.ActiveDirectory.GraphClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeeloWebAdmin.Controllers
{
    [Authorize]
    public class TenantOnboardingController : Controller
    {
        Organization Organization;
        public ActionResult Index()
        {
            if (TempData["Message"] != null)
            {
                ViewBag.Message = TempData["Message"].ToString();
                ViewBag.Success =Convert.ToBoolean(TempData["Success"]);
            }
            if (TempData["SuccessRecords"] != null)
            {
                ViewBag.SuccessRecords = TempData["SuccessRecords"].ToString();
                ViewBag.FailureRecords = TempData["FailureRecords"].ToString();
            }
            ActiveDirectoryClient client = AuthenticationHelper.GetActiveDirectoryClientAsApplication();
            //////////////Azure Functions//////////////////
            //string apiUrl = "https://meelofunctionapp.azurewebsites.net/api/ProfilePhotoAPICSharp";//?graphToken="+AuthenticationHelper.TokenForApplication;

            //using (System.Net.Http.HttpClient Httpclient = new System.Net.Http.HttpClient())
            //{
            //    Httpclient.BaseAddress = new Uri(apiUrl);
            //    Httpclient.DefaultRequestHeaders.Accept.Clear();
            //    Httpclient.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            //    Httpclient.DefaultRequestHeaders.Authorization =
            //        new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", AuthenticationHelper.TokenForApplication);
            //    System.Net.Http.HttpResponseMessage response = Httpclient.GetAsync(apiUrl).Result;
            //    var content = response.Content.ReadAsStringAsync();
            //    if (response.IsSuccessStatusCode)
            //    {
            //        var data = response.Content.ReadAsStringAsync().Result;
            //        var table = Newtonsoft.Json.JsonConvert.DeserializeObject<System.Data.DataTable>(data);

            //    }
            //}
                ///////////////Azure Functions ends here//////////
                GraphApiHelper helper = new GraphApiHelper();
            ITenantDetail Tenant = helper.GetTenantDetails(client);
            Organization = new Organization
            {
                TenantId = Tenant.ObjectId,
                FriendlyName = Tenant.DisplayName,
                CorporateName = Tenant.DisplayName,
                Address = new Address
                {
                    Street = Tenant.Street,
                    City = Tenant.City,
                    State = Tenant.State,
                    Country = Tenant.Country,
                    PostalCode = Tenant.PostalCode,
                },
                OrgContact = new Com.Meelo.CoreLib.User
                {
                    Id = 0,
                    BusinessPhones = new List<string> { Tenant.TelephoneNumber },
                    Mail = Tenant.TechnicalNotificationMails.FirstOrDefault()
                },
                MeeloLicense = new MeeloLicense { }
            };
            ViewBag.Country = Countries.CountryList;
            return View(Organization);
        }
        [HttpPost]
        public JsonResult AddOrganizationBuilding(Organization Org)
        {
            if (Org.Address!=null && Org.Address.GpsLocation.Lat == null)
                Org.Address.GpsLocation.Lat = Org.Address.GpsLocation.Long = 0;
                var OrgOutput = new OrganizationController().Add(Org);

                Building Bld = new Building();
                Bld.Address = Org.Address;
                Bld.Address.GpsLocation = Org.Address.GpsLocation;
                Bld.BuildingName = Org.CorporateName + " Building";
                var BuildingOutput = new BuildingController().Add(Bld);
            
            return OrgOutput;
        }
    }
}