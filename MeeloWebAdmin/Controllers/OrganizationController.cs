﻿using MeeloWebAdmin.Helper;
using Microsoft.Azure.ActiveDirectory.GraphClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Com.Meelo.CoreLib;
using Com.Meelo.BusinessServices.Tenant;
using Com.Meelo.BusinessServices.UserProfile;
using Com.Meelo.BusinessServices.Workspace;
using Com.Meelo.BusinessServices.Building;
using System.Security.Claims;
using System.Threading.Tasks;
using Com.Meelo.Utils;
namespace MeeloWebAdmin.Controllers
{
    [Authorize]
    public class OrganizationController : Controller
    {
        Organization Organization;
        IOrganizationService iOrganizationService;
        IUserProfileService iUserProfileService;
        IWorkspaceService iWorkspaceService;
        IBuildingService iBuildingService;
        public ActionResult Index()
        {
         
            string tenantID = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/tenantid").Value;
            iOrganizationService = new OrganizationService();
            OrganizationInput Input = new OrganizationInput();
            Input.Organization = new Organization();
            Input.Organization.TenantId = tenantID;
            var  Output = iOrganizationService.GetOrganization(Input).Result;
            if (Output.Organization == null || Output.Organization.OrgId == 0)
            {
                //Organization= OnboardTenant();
               return RedirectToAction("Index", "TenantOnboarding");
            }
            System.Web.HttpContext.Current.Session["Organization"] = Output.Organization;
            ViewBag.Country = Countries.CountryList;
            return View(Output.Organization);
        }
        [HttpPost]
        public JsonResult Add(Organization Org)
        {
            AddOrganizationOutput Output = new AddOrganizationOutput();
            try
            {
                iOrganizationService = new OrganizationService();
                AddOrganizationInput input = new AddOrganizationInput();
                input.Organization = Org;
                Output = iOrganizationService.AddOrganization(input).Result;
                if(Output.Organization.OrgId>0)
                {
                    Output.IsCallSuccessful = true;
                    Output.Message = Com.Meelo.Utils.ResourceHelper.GetResources("OrganizationAddSuccess");
                    System.Web.HttpContext.Current.Session["Organization"] = Output.Organization;
                }
                else
                {
                    Output.IsCallSuccessful = false;
                    Output.Message = Com.Meelo.Utils.ResourceHelper.GetResources("OrganizationAddException");
                }
            }
            catch(Exception ex)
            {
                Output.IsCallSuccessful = false;
                Output.Message = Com.Meelo.Utils.ResourceHelper.GetResources("OrganizationAddException");
            }
            return Json(Output);
        }
        [HttpPost]
        public ViewResult Update(Organization Org)
        {
            AddOrganizationOutput Output = new AddOrganizationOutput();
            try
            {
                iOrganizationService = new OrganizationService();
                AddOrganizationInput input = new AddOrganizationInput();
                input.Organization = Org;
                Output = iOrganizationService.UpdateOrganization(input).Result;
                if (Output.IsCallSuccessful)
                {
                    Output.Message = Com.Meelo.Utils.ResourceHelper.GetResources("OrganizationUpdateSuccess");
                    System.Web.HttpContext.Current.Session["Organization"] = Output.Organization;
                }
                else
                {
                    Output.IsCallSuccessful = false;
                    Output.Message = Com.Meelo.Utils.ResourceHelper.GetResources("OrganizationUpdateException");
                }
                ViewBag.Country = Countries.CountryList;
            }
            catch (Exception ex)
            {
                Output.IsCallSuccessful = false;
                Output.Message = Com.Meelo.Utils.ResourceHelper.GetResources("OrganizationUpdateException");
            }
            return View("Index",Output.Organization);
        }
        public ActionResult Onboard()
        {
            string tenantID = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/tenantid").Value;
            iOrganizationService = new OrganizationService();
            OrganizationInput Input = new OrganizationInput();
            Input.Organization = new Organization();
            Input.Organization.TenantId = tenantID;
            var Output = iOrganizationService.GetOrganization(Input).Result;
            if (Organization == null || Organization.OrgId == 0)
            {
                Organization = OnboardTenant();
            }
            return View("Index", Organization);
        }
    
        public Organization OnboardTenant()
        {
            ActiveDirectoryClient client = AuthenticationHelper.GetActiveDirectoryClientAsApplication();

            GraphApiHelper helper = new GraphApiHelper();
            ITenantDetail Tenant = helper.GetTenantDetails(client);
                Organization = new Organization
                {
                    TenantId = Tenant.ObjectId,
                    FriendlyName = Tenant.DisplayName,
                    CorporateName = Tenant.DisplayName,
                    Address = new Address
                    {
                        Street = Tenant.Street,
                        City = Tenant.City,
                        State = Tenant.State,
                        Country = Tenant.Country,
                        PostalCode = Tenant.PostalCode,
                    },
                    OrgContact = new Com.Meelo.CoreLib.User
                    {
                        Id = 0
                    },
                    MeeloLicense = new MeeloLicense { }

                };
                AddOrganizationInput input = new AddOrganizationInput();
                input.Organization = Organization;
                AddOrganizationOutput Output = iOrganizationService.AddOrganization(input).Result;

                //Users and workspaces
                var Users = helper.UsersList(client).Result;
                List<Com.Meelo.CoreLib.User> usersListObj = new List<Com.Meelo.CoreLib.User>();
                List<Workspace> workspaceListObj = new List<Workspace>();
                foreach (var usr in Users)
                {
                    if (usr.DisplayName.ToLower().Contains("conf") || usr.DisplayName.ToLower().Contains("room") || usr.DisplayName.ToLower().Contains("workspace"))
                    {
                        Workspace workspaceObj = new Workspace
                        {
                            OrgId = Organization.OrgId,
                            Name = usr.DisplayName,
                            Description = usr.GivenName,
                            FriendlyName = usr.DisplayName,
                            IdmResourceIdentity = null,
                            Address = new Com.Meelo.CoreLib.Address
                            {
                                City = usr.City,
                                Country = usr.Country,
                                PostalCode = usr.PostalCode,
                                Street = usr.StreetAddress,
                                State = usr.State
                            },
                            WebLink = null,
                            Photos = null,
                            LimitedAccessRoles = new List<RoleType> { RoleType.User },
                            LimitedAccessUsers = null,
                            Calendar = new Calendar { },
                            //Floor = new Floor { },
                            Building = new Building { Address = new Address { GpsLocation = new GpsLocation { } } },
                            Ratings = 0,
                            Created = DateTime.Now,
                            LastUpdated = DateTime.Now,
                            NextReservationStartTime = DateTime.Now,
                            NextReservationEndTime = DateTime.Now,
                            PhotoLinks = null,
                            Tags = new Dictionary<string, string> { { string.Empty, string.Empty } },
                            Maps = null,
                            WorkspaceType = 0,
                            OccupancyStatus = 0,
                            Capacity = 0,
                            //Capabilities = null,
                            LastRenovated = DateTime.Now,
                            CommunicationInfo = new CommunicationInfo
                            {
                                PhoneNumber = usr.Mobile,
                                TelephonyDetails = null,
                                AccessDescription = null,
                                OtherInfo = null
                            },
                            IsAutoCheckinEnabled = false,
                            IsAutoCheckoutEnabled = false,
                            AccessType = WorkspaceAccessType.Public,
                            MaintenanceOwner = new Com.Meelo.CoreLib.User { },
                            AreaInSqFt = 0,
                            DistanceFromFloorOrigin = null,
                            HasControllableDoor = false,
                            BeaconUid = null,
                            IsBlocked = false,
                            IsHidden = false,
                            ObjectId = usr.ObjectId

                        };
                        workspaceListObj.Add(workspaceObj);
                    }
                    else
                    {
                        Com.Meelo.CoreLib.User UsersInList = new Com.Meelo.CoreLib.User
                        {
                            OrgId = Organization.OrgId,
                            DisplayName = usr.DisplayName,
                            FirstName = usr.GivenName,
                            PreferredName = usr.GivenName,
                            ObjectId = usr.ObjectId,
                            DepartmentName = usr.Department,
                            //CompanyName = usr.CompanyName,
                            Mail = usr.Mail,
                            JobTitle = usr.JobTitle,
                            BusinessPhones = new List<string> { usr.TelephoneNumber },
                            OfficeLocation = new GpsLocation { Lat = 0, Long = 0, Location = string.Empty },
                            Address = new Com.Meelo.CoreLib.Address
                            {
                                City = usr.City,
                                Country = usr.Country,
                                PostalCode = usr.PostalCode,
                                Street = usr.StreetAddress,
                                State = usr.State
                            },
                            MobilePhone = usr.Mobile,
                            PreferredLanguage = usr.PreferredLanguage,
                            UserPrincipalName = usr.UserPrincipalName,
                            Surname = usr.Surname,
                            AccountEnabled = Convert.ToBoolean(usr.AccountEnabled),
                            MeeloLicense = new MeeloLicense { LicenseId = 0 },
                            MeeloPlan = new MeeloPlan { PlanId = "0" },
                            CollabProviderUniqueId = usr.ObjectId,
                            MailboxSettings = new MailboxSettings
                            {
                                LocaleInfo = new LocaleInfo { Locale = null, DisplayName = null },
                                AutomaticRepliesSetting = new AutomaticRepliesSetting
                                {
                                    ExternalAudience = null,
                                    ExternalReplyMessage = null,
                                    InternalReplyMessage = null,
                                    ScheduledStartDateTime = null,
                                    ScheduledEndDateTime = null,
                                    Status = null
                                }
                            },
                            UsageLocation = new GpsLocation
                            {
                                Location = usr.UsageLocation
                            }

                        };
                        usersListObj.Add(UsersInList);
                    }
                }
                if (workspaceListObj != null && workspaceListObj.Count() > 0)
                {
                    iWorkspaceService = new WorkspaceService();
                    iBuildingService = new BuildingService();
                    Com.Meelo.CoreLib.Building BuildingObj = new Building();
                    BuildingObj = workspaceListObj.FirstOrDefault().Building;
                    BuildingObj.OrgId = Organization.OrgId;
                    BuildingObj.Address = workspaceListObj.FirstOrDefault().Address;
                    BuildingObj.Address.GpsLocation = new GpsLocation { };
                AddBuildingInput BLdInput = new AddBuildingInput();
                BLdInput.building = BuildingObj;
                    var BldOutput = iBuildingService.AddBuilding(BLdInput).Result;
                    workspaceListObj.ForEach(i => i.Building.Id = BldOutput.building.Id);
                    //var obj = iWorkspaceService.AddWorkspace(workspaceListObj);
                }
                if (usersListObj != null && usersListObj.Count > 0)
                {
                    iUserProfileService = new UserProfileService();
                    var Id = iUserProfileService.AddUser(usersListObj);
                }
            return Organization;
            }
        
    }
}