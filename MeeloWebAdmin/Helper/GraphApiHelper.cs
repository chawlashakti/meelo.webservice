﻿using Microsoft.Azure.ActiveDirectory.GraphClient;
using Microsoft.Azure.ActiveDirectory.GraphClient.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace MeeloWebAdmin.Helper
{
    public class GraphApiHelper
    {
        public async Task<List<IUser>> UsersList( ActiveDirectoryClient client,string Filter = "")
        {
            List<IUser> Users = new List<IUser>();
            IPagedCollection<IUser> pagedCollection;
            // use the token for querying the graph to get the user details
            if (Filter == "")
                pagedCollection =await client.Users.ExecuteAsync();
            else
                pagedCollection = client.Users.Where(u => u.GivenName.Contains(Filter)).ExecuteAsync().Result;
            while (pagedCollection != null)
            {
                var usersList = pagedCollection.CurrentPage.ToList();
                Users.AddRange(usersList.Cast<Microsoft.Azure.ActiveDirectory.GraphClient.User>());
                pagedCollection =await pagedCollection.GetNextPageAsync();
            }
            return Users;
        }
        public ITenantDetail GetTenantDetails(ActiveDirectoryClient client)
        {
            var tenantDetails = client.TenantDetails.ExecuteAsync().Result;
            ITenantDetail Tenant = tenantDetails.CurrentPage.ToList().First();

            return Tenant;
        }
    }
}