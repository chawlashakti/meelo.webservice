﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.DataServices
{
  public interface IUserDbService
    {
        Task<Int64> AddUser(List<CoreLib.User> input);
        Task<SqlDataReader> GetUser(string ObjectId);
        Task<DataTable> GetUsers(string TenantId);
    }
}
