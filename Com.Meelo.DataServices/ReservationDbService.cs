﻿using Com.Meelo.CoreLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.Meelo.Utils;
using Newtonsoft.Json;
using Com.Meelo.CoreLib.ViewModel;
using System.Data;

namespace Com.Meelo.DataServices
{
    public class ReservationDbService : IReservationDbService
    {
        public Task<int> Reserve(Event Input)
        {
            var SqlConnection = Utils.Config.DbUtils.GetDbConnection(Utils.Config.DbUtils.GetDbConnectionStringFromConfig("MeeloAzureSQLConnectionString"));
                var Result = Utils.Sql.SqlHelper.ExecuteScalar(SqlConnection, "Event_Insert", Input.Id,Input.Workspace.OrgId,Input.Subject, Input.CreatedDateTime.DateTime,Input.StartTime.DateTime,Input.EndTime.DateTime,
                    Input.Body,Input.BodyPreview,null,Input.iCalUId,Input.RefId,Input.Importance, Input.PrimaryOwner.Id,Input.Sensitivity,Input.Type,Convert.ToString(Input.WebLink),null,null,Input.CheckinPerformedBy,
                    Input.CheckoutPerformedBy,Input.EventCalendar.Id, Input.Workspace.Id, Input.OnlineMeetingUrl,Input.PrimaryOwner.ObjectId,Input.CalenderEventId);
            SqlConnection.Close();
            return Task.FromResult<int>(Convert.ToInt32(Result));
        }
        public Task<int> CancelReservation(Event Input)
        {
            var SqlConnection = Utils.Config.DbUtils.GetDbConnection(Utils.Config.DbUtils.GetDbConnectionStringFromConfig("MeeloAzureSQLConnectionString"));
            var Result = Utils.Sql.SqlHelper.ExecuteNonQuery(SqlConnection, "Event_Cancel", Input.Id);
            SqlConnection.Close();
            return Task.FromResult<int>(Result);
        }
      public Task<int> CancelReservation()
        {
            var SqlConnection = Utils.Config.DbUtils.GetDbConnection(Utils.Config.DbUtils.GetDbConnectionStringFromConfig("MeeloAzureSQLConnectionString"));
            var Result = Utils.Sql.SqlHelper.ExecuteNonQuery(SqlConnection, "AutoScheduleServices", "Cancel");
            SqlConnection.Close();
            return Task.FromResult<int>(Result);
        }
        public Task<int> CheckIn(Event Input)
        {
            var SqlConnection = Utils.Config.DbUtils.GetDbConnection(Utils.Config.DbUtils.GetDbConnectionStringFromConfig("MeeloAzureSQLConnectionString"));
            var Result = Utils.Sql.SqlHelper.ExecuteNonQuery(SqlConnection, "Event_CheckInOut", Input.Id,"True",Input.PrimaryOwner.ObjectId);
            SqlConnection.Close();
            return Task.FromResult<int>(Result);
        }
        public Task<int> CheckOut(Event Input)
        {
            var SqlConnection = Utils.Config.DbUtils.GetDbConnection(Utils.Config.DbUtils.GetDbConnectionStringFromConfig("MeeloAzureSQLConnectionString"));
            var Result = Utils.Sql.SqlHelper.ExecuteNonQuery(SqlConnection, "Event_CheckInOut", Input.Id, "False", Input.PrimaryOwner.ObjectId);
            SqlConnection.Close();
            return Task.FromResult<int>(Result);
        }
        public Task<DataTable> GetReservations(DateTimeOffset? StartTime, DateTimeOffset? EndTime, string UserIdFilter,string TenantId)
        {
            var SqlConnection = Utils.Config.DbUtils.GetDbConnection(Utils.Config.DbUtils.GetDbConnectionStringFromConfig("MeeloAzureSQLConnectionString"));
            System.Data.DataSet DataSetObj = new System.Data.DataSet();
            Utils.Sql.SqlHelper.FillDataset(SqlConnection, "Event_Select", DataSetObj, new string[] { "Events" }, StartTime.Value.DateTime, EndTime.Value.DateTime, UserIdFilter, TenantId);
            SqlConnection.Close();

            return Task.FromResult<DataTable>(DataSetObj.Tables[0]);
        }
        public Task<int> GetReservationCount(DateTimeOffset? StartTime, DateTimeOffset? EndTime, string UserIdFilter)
        {
            var SqlConnection = Utils.Config.DbUtils.GetDbConnection(Utils.Config.DbUtils.GetDbConnectionStringFromConfig("MeeloAzureSQLConnectionString"));
            var Result=Utils.Sql.SqlHelper.ExecuteScalar(SqlConnection, "Event_Select_Count", StartTime.Value.DateTime, EndTime.Value.DateTime, UserIdFilter);
            SqlConnection.Close();

            return Task.FromResult<int>(Convert.ToInt32(Result));
        }
    }
}
