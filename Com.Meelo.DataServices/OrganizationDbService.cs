﻿using System.Data.SqlClient;
using System.Threading.Tasks;
using Com.Meelo.CoreLib;
namespace Com.Meelo.DataServices
{
    public class OrganizationDbService:IOrganizationDbService
    {
       public SqlConnection SqlConnection = null;
        public void CloseConnection()
        {
            if(SqlConnection!=null)
            SqlConnection.Close();
        }
        public Task<SqlDataReader> AddOrganization(Organization input)
        {
            SqlConnection = Utils.Config.DbUtils.GetDbConnection(Utils.Config.DbUtils.GetDbConnectionStringFromConfig("MeeloAzureSQLConnectionString"));
            var Output = Utils.Sql.SqlHelper.ExecuteReader(SqlConnection, "Organizations_Insert", input.FriendlyName, input.CorporateName, input.MeeloLicense.LicenseId,
             input.Address.Street, input.Address.Building, input.Address.City, input.Address.Country, input.Address.State, input.Address.PostalCode, "", input.Address.GpsLocation.Lat, input.Address.GpsLocation.Long, input.OrgContact.Mail, input.TenantId);
            return Task.FromResult<SqlDataReader>(Output);
        }
        public Task<SqlDataReader> UpdateOrganization(Organization input)
        {
            SqlConnection = Utils.Config.DbUtils.GetDbConnection(Utils.Config.DbUtils.GetDbConnectionStringFromConfig("MeeloAzureSQLConnectionString"));
            var Output = Utils.Sql.SqlHelper.ExecuteReader(SqlConnection, "Organizations_Update",input.OrgId, input.FriendlyName, input.CorporateName, input.MeeloLicense.LicenseId,
             input.Address.Street, input.Address.Building, input.Address.City, input.Address.Country, input.Address.State, input.Address.PostalCode, "", 0, 0, input.OrgContact.Mail, input.TenantId);
            return Task.FromResult<SqlDataReader>(Output);
        }
        
        public Task<SqlDataReader> GetOrganization(long OrgId)
        {
            SqlConnection = Utils.Config.DbUtils.GetDbConnection(Utils.Config.DbUtils.GetDbConnectionStringFromConfig("MeeloAzureSQLConnectionString"));
            var Output = Utils.Sql.SqlHelper.ExecuteReader(SqlConnection, "Organizations_Select", OrgId,null);
            SqlConnection.Close();
            return Task.FromResult<SqlDataReader>(Output);
        }
        public Task<SqlDataReader> GetOrganization(string TenantId)
        {
            SqlConnection = Utils.Config.DbUtils.GetDbConnection(Utils.Config.DbUtils.GetDbConnectionStringFromConfig("MeeloAzureSQLConnectionString"));
            var Output = Utils.Sql.SqlHelper.ExecuteReader(SqlConnection, "Organizations_Select", 0, TenantId);
            //SqlConnection.Close();
            return Task.FromResult<SqlDataReader>(Output);
        }
    }
}
