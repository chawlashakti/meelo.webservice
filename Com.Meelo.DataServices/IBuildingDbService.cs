﻿using Com.Meelo.CoreLib;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Com.Meelo.DataServices
{
    public interface IBuildingDbService
    {
        //Task<IEnumerable<BuildingViewModel>> GetBuildingsAndWorkspaceCount(string TenantId, GpsLocation location);
        Task<DataTable> GetBuildingsAndWorkspaceCount(string TenantId, GpsLocation location);
        //Task<long> AddBuilding(CoreLib.Building input);
        Task<DataTable> AddBuilding(CoreLib.Building input);
        Task<DataTable> UpdateBuilding(Building input);
        void GetBuilding(DataTable dt, long OrgId, long BuildingId);
        Task<DataTable> GetBuilding(long OrgId, int StartIndex, int PageSize, string SortBy, string SortOrder, string Filter);
        Task<DataTable> GetBuilding(long OrgId, string FilterbyBuilding);
    }
}
