﻿using System.Data.SqlClient;
using System.Threading.Tasks;
namespace Com.Meelo.DataServices
{
    public interface IOrganizationDbService
    {
        Task<SqlDataReader> AddOrganization(CoreLib.Organization input);
        Task<SqlDataReader> UpdateOrganization(CoreLib.Organization input);
        Task<SqlDataReader> GetOrganization(long OrgId);
        Task<SqlDataReader> GetOrganization(string TenantId);
        void CloseConnection();
    }
}
