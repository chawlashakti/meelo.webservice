﻿using Com.Meelo.CoreLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Resources;
using System.IO;
using Com.Meelo.CoreLib.ViewModel;
using Newtonsoft.Json;
using System.Collections;
using System.Security.Claims;

namespace Com.Meelo.DataServices
{
   public class WorkspaceDbService:IWorkspaceDbService
    {
        public void GetWorkspaces(DataTable dt,long OrgId,string TenantId,long BuildingId)
        {
            var SqlConnection = Utils.Config.DbUtils.GetDbConnection(Utils.Config.DbUtils.GetDbConnectionStringFromConfig("MeeloAzureSQLConnectionString"));
            DataSet DataSetObj = new DataSet();
            Utils.Sql.SqlHelper.FillDataset(SqlConnection, "Workspace_GetWorkspaceByBuilding", DataSetObj, new string[] { "Workspaces" },OrgId,TenantId ,BuildingId);
            SqlConnection.Close();
            dt=DataSetObj.Tables[0];
        }
        public DataTable GetWorkspaces(long OrgId, long WorkspaceId)
        {
            var SqlConnection = Utils.Config.DbUtils.GetDbConnection(Utils.Config.DbUtils.GetDbConnectionStringFromConfig("MeeloAzureSQLConnectionString"));
            DataSet DataSetObj = new DataSet();
            DataSetObj=  Utils.Sql.SqlHelper.ExecuteDataset(SqlConnection, "Workspace_SelectById", OrgId,WorkspaceId);
            SqlConnection.Close();
            return DataSetObj.Tables[0];
        }
        public DataTable GetWorkspaces(long OrgId, int StartIndex, int PageSize, string SortBy, string SortOrder, string FilterbyName)
        {
            var SqlConnection = Utils.Config.DbUtils.GetDbConnection(Utils.Config.DbUtils.GetDbConnectionStringFromConfig("MeeloAzureSQLConnectionString"));
            System.Data.DataSet DataSetObj = new System.Data.DataSet();

            Utils.Sql.SqlHelper.FillDataset(SqlConnection, "Workspace_List", DataSetObj, new string[] { "Workspaces" },OrgId, FilterbyName, StartIndex, PageSize,SortBy,SortOrder);
            SqlConnection.Close();

          DataTable  dt = DataSetObj.Tables[0];
            return dt;
        }
        public Task<DataTable> GetAvailableWorkspaces(BuildingWorkspaceRequestModel Input)
        {
            var SqlConnection = Utils.Config.DbUtils.GetDbConnection(Utils.Config.DbUtils.GetDbConnectionStringFromConfig("MeeloAzureSQLConnectionString"));
            System.Data.DataSet DataSetObj = new System.Data.DataSet();

            Utils.Sql.SqlHelper.FillDataset(SqlConnection, "GetWorkspacesbyFilter", DataSetObj, new string[] { "Workspaces" }, Input.TenantId, Input.Lat, Input.Long, Input.StartTime, Input.EndTime, Input.BuildingId, Input.SearchText);
            SqlConnection.Close();
            return Task.FromResult<DataTable>(DataSetObj.Tables[0]);
        }
        //public Task<DataTable> GetWorkspacesByBuilding(long BuildingId)
        //{
        //    var SqlConnection = Utils.Config.DbUtils.GetDbConnection(Utils.Config.DbUtils.GetDbConnectionStringFromConfig("MeeloAzureSQLConnectionString"));
        //    DataSet DataSetObj = new DataSet();
        //    Utils.Sql.SqlHelper.FillDataset(SqlConnection, "Workspace_GetWorkspaceByBuilding", DataSetObj, new string[] { "Workspaces" }, BuildingId);
        //    SqlConnection.Close();
        //    return Task.FromResult<DataTable>(DataSetObj.Tables[0]);

        //}
        //public Task<SqlDataReader> GetWorkspacesById(string TenantId,long WorkspaceId)
        //{
        //    var SqlConnection = Utils.Config.DbUtils.GetDbConnection(Utils.Config.DbUtils.GetDbConnectionStringFromConfig("MeeloAzureSQLConnectionString"));
        //    var Output = Utils.Sql.SqlHelper.ExecuteReader(SqlConnection, "Workspaces_SelectById", TenantId, WorkspaceId);
        //    SqlConnection.Close();

        //    return Task.FromResult<SqlDataReader>(Output);
        //}
        //public Task<DataTable> GetWorkspacesList(int startIndex, int count, string sorting)
        //{
        //    var SqlConnection = Utils.Config.DbUtils.GetDbConnection(Utils.Config.DbUtils.GetDbConnectionStringFromConfig("MeeloAzureSQLConnectionString"));
        //    System.Data.DataSet DataSetObj = new System.Data.DataSet();

        //    Utils.Sql.SqlHelper.FillDataset(SqlConnection, "Workspaces_Select", DataSetObj, new string[] { "Workspaces" }, 1, startIndex, count);
        //    SqlConnection.Close();

        //    return Task.FromResult<DataTable>(DataSetObj.Tables[0]);
        //}

        public Task<DataTable> AddWorkspace(List<Workspace> input)
        {
            //string tenantID = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/tenantid").Value;

            var SqlConnection = Utils.Config.DbUtils.GetDbConnection(Utils.Config.DbUtils.GetDbConnectionStringFromConfig("MeeloAzureSQLConnectionString"));
            DataSet Output = null;
            try
            {
                foreach (CoreLib.Workspace workspace in input)
                {
                    ArrayList list = new ArrayList();
                    list.Add(workspace.OrgId);
                    list.Add(workspace.Name);
                    list.Add(workspace.Description);
                    list.Add(workspace.FriendlyName);
                    list.Add(workspace.IdmResourceIdentity);
                    if (workspace.Building == null)
                    {
                        list.Add(0);
                        //list.Add(null);
                    }
                    else
                    {
                        list.Add(workspace.Building.Id);
                        //list.Add(workspace.Building.BuildingName);
                    }
                    //if (workspace.Floor == null)
                    //    list.Add(0);
                    //else
                    //    list.Add(workspace.Floor.Name);
                    

                    if (workspace.Address==null)
                    {
                        list.Add(0); //Floor
                        list.Add(0);   //Lat 
                        list.Add(0);   //Long 
                        list.Add(null);//LocationString 
                        list.Add(null);//City 
                        list.Add(null);//Country 
                        list.Add(null);//State 
                        list.Add(null);//PostalCode
                    }
                    else
                    {
                        list.Add(workspace.Address.Floor);//Floor
                        if (workspace.Address.GpsLocation == null)
                        {
                            list.Add(0);   //Lat 
                            list.Add(0);   //Long 
                            list.Add(null);//LocationString
                        }
                        else
                        {
                            list.Add(workspace.Address.GpsLocation.Lat);
                            list.Add(workspace.Address.GpsLocation.Long);
                            list.Add(workspace.Address.GpsLocation.Location);
                           
                        }
                        list.Add(workspace.Address.City);
                        list.Add(workspace.Address.Country);
                        list.Add(workspace.Address.State);
                        list.Add(workspace.Address.PostalCode);
                    }

                   
                    list.Add(Convert.ToString(workspace.WebLink));
                    list.Add(workspace.PhotoLinks);
                    list.Add(workspace.Ratings);
                    list.Add(workspace.Created.DateTime);
                    list.Add(workspace.LastUpdated.DateTime);
                    if (workspace.NextReservationStartTime.GetValueOrDefault().DateTime == DateTime.MinValue)
                        list.Add(null);
                    else
                    list.Add(workspace.NextReservationStartTime.GetValueOrDefault().DateTime);
                    if (workspace.NextReservationEndTime.GetValueOrDefault().DateTime == DateTime.MinValue)
                        list.Add(null);
                    else
                    list.Add(workspace.NextReservationEndTime.GetValueOrDefault().DateTime);
                    if (workspace.LimitedAccessRoles != null && workspace.LimitedAccessRoles.ToList().Count>0)
                        list.Add(workspace.LimitedAccessRoles.FirstOrDefault());
                    else
                        list.Add(null);
                    if (workspace.LimitedAccessUsers != null && workspace.LimitedAccessUsers.ToList().Count > 0)
                        list.Add(workspace.LimitedAccessUsers.FirstOrDefault().DisplayName ?? string.Empty);
                    else
                        list.Add(null);
                    if (workspace.Tags != null && workspace.Tags.Count() > 0)
                        list.Add(workspace.Tags.FirstOrDefault().Value ?? string.Empty);
                    else
                        list.Add(null);
                    if (workspace.Maps != null)
                        list.Add(workspace.Maps);
                    else
                        list.Add(null);
                    list.Add(workspace.WorkspaceType);
                    list.Add(workspace.OccupancyStatus);
                    list.Add(workspace.Capacity);
                    if (workspace.LastRenovated.DateTime == DateTime.MinValue)
                        list.Add(null);
                    else
                        list.Add(workspace.LastRenovated.DateTime);
                    if (workspace.CommunicationInfo.OtherInfo != null && workspace.CommunicationInfo.OtherInfo.Count() > 0)
                        list.Add(workspace.CommunicationInfo.OtherInfo.FirstOrDefault().Value);
                    else
                        list.Add(null);
                    list.Add(workspace.CommunicationInfo.PhoneNumber);
                    list.Add(workspace.CommunicationInfo.TelephonyDetails);
                    list.Add(workspace.CommunicationInfo.AccessDescription);
                    list.Add(null);
                    list.Add(workspace.IsAutoCheckinEnabled);
                    list.Add(workspace.IsAutoCheckoutEnabled);
                    list.Add(workspace.AccessType);
                    if (workspace.MaintenanceOwner != null)
                        list.Add(workspace.MaintenanceOwner.Id);
                    else
                        list.Add(0);
                    list.Add(workspace.AreaInSqFt);
                    if (workspace.DistanceFromFloorOrigin != null)
                        list.Add(Convert.ToString(workspace.DistanceFromFloorOrigin ?? new Point()));
                    else
                        list.Add(null);
                    list.Add(workspace.HasControllableDoor);
                    list.Add(workspace.BeaconUid);
                    list.Add(workspace.IsBlocked);
                    list.Add(workspace.IsHidden);
                    list.Add(workspace.ObjectId);
                    //list.Add(tenantID);
                    
                     Output = Utils.Sql.SqlHelper.ExecuteDataset(SqlConnection, "Workspace_Insert", list.ToArray());
   
                }
            }
            catch(Exception ex)
            {
            }
            SqlConnection.Close();
            return Task.FromResult<DataTable>(Output.Tables[0]);
        }
        public Task<DataTable> UpdateWorkspace(Workspace workspace)
        {
            var SqlConnection = Utils.Config.DbUtils.GetDbConnection(Utils.Config.DbUtils.GetDbConnectionStringFromConfig("MeeloAzureSQLConnectionString"));
            DataSet Output = null;
            try
            {
                    ArrayList list = new ArrayList();
                    list.Add(workspace.Id);
                    list.Add(workspace.OrgId);
                    list.Add(workspace.Name);
                    list.Add(workspace.Description);
                    list.Add(workspace.FriendlyName);
                    list.Add(workspace.IdmResourceIdentity);
                    if (workspace.Building == null)
                    {
                        list.Add(0);
                        //list.Add(null);
                    }
                    else
                    {
                        list.Add(workspace.Building.Id);
                        //list.Add(workspace.Building.BuildingName);
                    }
                    //if (workspace.Floor == null)
                    //    list.Add(0);
                    //else
                    //    list.Add(workspace.Floor.Id);
                    if (workspace.Address == null)
                    {

                        list.Add(0);   //Floor 
                        list.Add(0);   //Lat 
                        list.Add(0);   //Long 
                        list.Add(null);//LocationString 
                        list.Add(null);//City 
                        list.Add(null);//Country 
                        list.Add(null);//State 
                        list.Add(null);//PostalCode
                    }
                    else
                    {
                    list.Add(workspace.Address.Floor);   //Floor 
                    if (workspace.Address.GpsLocation == null)
                        {
                            list.Add(0);   //Lat 
                            list.Add(0);   //Long 
                            list.Add(null);//LocationString
                        }
                        else
                        {
                            list.Add(workspace.Address.GpsLocation.Lat);
                            list.Add(workspace.Address.GpsLocation.Long);
                            list.Add(workspace.Address.GpsLocation.Location);

                        }
                        list.Add(workspace.Address.City);
                        list.Add(workspace.Address.Country);
                        list.Add(workspace.Address.State);
                        list.Add(workspace.Address.PostalCode);
                    }
                    list.Add(Convert.ToString(workspace.WebLink));
                    list.Add(workspace.PhotoLinks);
                    list.Add(workspace.Ratings);
                    list.Add(workspace.Created.DateTime);
                    list.Add(workspace.LastUpdated.DateTime);
                    if (workspace.NextReservationStartTime.GetValueOrDefault().DateTime == DateTime.MinValue)
                        list.Add(null);
                    else
                        list.Add(workspace.NextReservationStartTime.GetValueOrDefault().DateTime);
                    if (workspace.NextReservationEndTime.GetValueOrDefault().DateTime == DateTime.MinValue)
                        list.Add(null);
                    else
                        list.Add(workspace.NextReservationEndTime.GetValueOrDefault().DateTime);
                    if (workspace.LimitedAccessRoles != null && workspace.LimitedAccessRoles.ToList().Count > 0)
                        list.Add(workspace.LimitedAccessRoles.FirstOrDefault());
                    else
                        list.Add(null);
                    if (workspace.LimitedAccessUsers != null && workspace.LimitedAccessUsers.ToList().Count > 0)
                        list.Add(workspace.LimitedAccessUsers.FirstOrDefault().DisplayName ?? string.Empty);
                    else
                        list.Add(null);
                    if (workspace.Tags != null && workspace.Tags.Count() > 0)
                        list.Add(workspace.Tags.FirstOrDefault().Value ?? string.Empty);
                    else
                        list.Add(null);
                    if (workspace.Maps != null)
                        list.Add(workspace.Maps);
                    else
                        list.Add(null);
                    list.Add(workspace.WorkspaceType);
                    list.Add(workspace.OccupancyStatus);
                    list.Add(workspace.Capacity);
                    if (workspace.LastRenovated.DateTime == DateTime.MinValue)
                        list.Add(null);
                    else
                        list.Add(workspace.LastRenovated.DateTime);
                    if (workspace.CommunicationInfo.OtherInfo != null && workspace.CommunicationInfo.OtherInfo.Count() > 0)
                        list.Add(workspace.CommunicationInfo.OtherInfo.FirstOrDefault().Value);
                    else
                        list.Add(null);
                    list.Add(workspace.CommunicationInfo.PhoneNumber);
                    list.Add(workspace.CommunicationInfo.TelephonyDetails);
                    list.Add(workspace.CommunicationInfo.AccessDescription);
                    list.Add(null);
                    list.Add(workspace.IsAutoCheckinEnabled);
                    list.Add(workspace.IsAutoCheckoutEnabled);
                    list.Add(workspace.AccessType);
                    if (workspace.MaintenanceOwner != null)
                        list.Add(workspace.MaintenanceOwner.Id);
                    else
                        list.Add(0);
                    list.Add(workspace.AreaInSqFt);
                    if (workspace.DistanceFromFloorOrigin != null)
                        list.Add(Convert.ToString(workspace.DistanceFromFloorOrigin ?? new Point()));
                    else
                        list.Add(null);
                    list.Add(workspace.HasControllableDoor);
                    list.Add(workspace.BeaconUid);
                    list.Add(workspace.IsBlocked);
                    list.Add(workspace.IsHidden);
                    list.Add(workspace.ObjectId);
                    Output = Utils.Sql.SqlHelper.ExecuteDataset(SqlConnection, "Workspace_Update", list.ToArray());
            }
            catch (Exception ex)
            {
            }
            SqlConnection.Close();
            return Task.FromResult<DataTable>(Output.Tables[0]);
        }
        public Task<DataTable> AddWorkspace(Workspace workspace)
        {
            var SqlConnection = Utils.Config.DbUtils.GetDbConnection(Utils.Config.DbUtils.GetDbConnectionStringFromConfig("MeeloAzureSQLConnectionString"));
            DataSet Output = null;
            DataTable dt = null;
            try
            {
                    ArrayList list = new ArrayList();
                    list.Add(workspace.OrgId);
                    list.Add(workspace.Name);
                    list.Add(workspace.Description);
                    list.Add(workspace.FriendlyName);
                    list.Add(workspace.IdmResourceIdentity);
                    if (workspace.Building == null)
                    {
                        list.Add(0);
                    }
                    else
                    {
                        list.Add(workspace.Building.Id);
                    }
                    if (workspace.Address == null)
                    {
                        list.Add(0); //Floor
                        list.Add(0);   //Lat 
                        list.Add(0);   //Long 
                        list.Add(null);//LocationString 
                        list.Add(null);//City 
                        list.Add(null);//Country 
                        list.Add(null);//State 
                        list.Add(null);//PostalCode
                    }
                    else
                    {
                        list.Add(workspace.Address.Floor);//Floor
                        if (workspace.Address.GpsLocation == null)
                        {
                            list.Add(0);   //Lat 
                            list.Add(0);   //Long 
                            list.Add(null);//LocationString
                        }
                        else
                        {
                            list.Add(workspace.Address.GpsLocation.Lat);
                            list.Add(workspace.Address.GpsLocation.Long);
                            list.Add(workspace.Address.GpsLocation.Location);

                        }
                        list.Add(workspace.Address.City);
                        list.Add(workspace.Address.Country);
                        list.Add(workspace.Address.State);
                        list.Add(workspace.Address.PostalCode);
                    }
                    list.Add(Convert.ToString(workspace.WebLink));
                    list.Add(workspace.PhotoLinks);
                    list.Add(workspace.Ratings);
                    list.Add(workspace.Created.DateTime);
                    list.Add(workspace.LastUpdated.DateTime);
                    if (workspace.NextReservationStartTime.GetValueOrDefault().DateTime == DateTime.MinValue)
                        list.Add(null);
                    else
                        list.Add(workspace.NextReservationStartTime.GetValueOrDefault().DateTime);
                    if (workspace.NextReservationEndTime.GetValueOrDefault().DateTime == DateTime.MinValue)
                        list.Add(null);
                    else
                        list.Add(workspace.NextReservationEndTime.GetValueOrDefault().DateTime);
                    if (workspace.LimitedAccessRoles != null && workspace.LimitedAccessRoles.ToList().Count > 0)
                        list.Add(workspace.LimitedAccessRoles.FirstOrDefault());
                    else
                        list.Add(null);
                    if (workspace.LimitedAccessUsers != null && workspace.LimitedAccessUsers.ToList().Count > 0)
                        list.Add(workspace.LimitedAccessUsers.FirstOrDefault().DisplayName ?? string.Empty);
                    else
                        list.Add(null);
                    if (workspace.Tags != null && workspace.Tags.Count() > 0)
                        list.Add(workspace.Tags.FirstOrDefault().Value ?? string.Empty);
                    else
                        list.Add(null);
                    if (workspace.Maps != null)
                        list.Add(workspace.Maps);
                    else
                        list.Add(null);
                    list.Add(workspace.WorkspaceType);
                    list.Add(workspace.OccupancyStatus);
                    list.Add(workspace.Capacity);
                    if (workspace.LastRenovated.DateTime == DateTime.MinValue)
                        list.Add(null);
                    else
                        list.Add(workspace.LastRenovated.DateTime);
                    if (workspace.CommunicationInfo.OtherInfo != null && workspace.CommunicationInfo.OtherInfo.Count() > 0)
                        list.Add(workspace.CommunicationInfo.OtherInfo.FirstOrDefault().Value);
                    else
                        list.Add(null);
                    list.Add(workspace.CommunicationInfo.PhoneNumber);
                    list.Add(workspace.CommunicationInfo.TelephonyDetails);
                    list.Add(workspace.CommunicationInfo.AccessDescription);
                    list.Add(null);
                    list.Add(workspace.IsAutoCheckinEnabled);
                    list.Add(workspace.IsAutoCheckoutEnabled);
                    list.Add(workspace.AccessType);
                    if (workspace.MaintenanceOwner != null)
                        list.Add(workspace.MaintenanceOwner.Id);
                    else
                        list.Add(0);
                    list.Add(workspace.AreaInSqFt);
                    if (workspace.DistanceFromFloorOrigin != null)
                        list.Add(Convert.ToString(workspace.DistanceFromFloorOrigin ?? new Point()));
                    else
                        list.Add(null);
                    list.Add(workspace.HasControllableDoor);
                    list.Add(workspace.BeaconUid);
                    list.Add(workspace.IsBlocked);
                    list.Add(workspace.IsHidden);
                    list.Add(workspace.ObjectId);

                    Output = Utils.Sql.SqlHelper.ExecuteDataset(SqlConnection, "Workspace_Insert", list.ToArray());
                dt = Output.Tables[0];

            }
            catch (Exception ex)
            {
                return Task.FromResult<DataTable>(dt);
            }
            finally
            {
                SqlConnection.Close();
            }
           
            return Task.FromResult<DataTable>(dt);
        }
        public Task<int> RateWorkspace(WorkspaceReviewInput Input)
        {
            var SqlConnection = Utils.Config.DbUtils.GetDbConnection(Utils.Config.DbUtils.GetDbConnectionStringFromConfig("MeeloAzureSQLConnectionString"));
            var Result = Utils.Sql.SqlHelper.ExecuteNonQuery(SqlConnection, "Workspace_Rating", Input.WorkspaceId, Input.ObjectId,Input.Rating, Input.Comments,Input.OrgId);
            SqlConnection.Close();
            return Task.FromResult<int>(Result);
        }
    }
}
