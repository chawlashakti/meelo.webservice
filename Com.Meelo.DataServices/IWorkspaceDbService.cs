﻿using Com.Meelo.CoreLib;
using Com.Meelo.CoreLib.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.DataServices
{
    public interface IWorkspaceDbService
    {
        void GetWorkspaces(DataTable dt, long OrgId, string TenantId, long BuildingId);
        DataTable GetWorkspaces(long OrgId, long WorkspaceId);
        DataTable GetWorkspaces(long OrgId, int StartIndex, int PageSize, string SortBy, string SortOrder, string FilterbyName);
        //Task<WorkspaceBuildingOuputViewModel> GetAvailableWorkspaces(BuildingWorkspaceRequestModel Input);
        Task<DataTable> GetAvailableWorkspaces(BuildingWorkspaceRequestModel Input);
        //Task<DataTable> GetWorkspacesByBuilding(long BuildingId);
        //Task<SqlDataReader> GetWorkspacesById(string TenantId, long OrgId);
        Task<DataTable> AddWorkspace(List<Workspace> input);
        Task<DataTable> AddWorkspace(Workspace input);
        Task<DataTable> UpdateWorkspace(Workspace workspace);
        Task<int> RateWorkspace(WorkspaceReviewInput Input);
        //Task<DataTable> GetWorkspacesList(int startIndex, int count, string sorting);
    }
}
