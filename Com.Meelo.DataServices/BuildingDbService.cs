﻿using Com.Meelo.CoreLib;
using Com.Meelo.CoreLib.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.DataServices
{
    public class BuildingDbService : IBuildingDbService
    {
        public Task<DataTable> GetBuildingsAndWorkspaceCount(string TenantId, GpsLocation location)
        {
            var SqlConnection = Utils.Config.DbUtils.GetDbConnection(Utils.Config.DbUtils.GetDbConnectionStringFromConfig("MeeloAzureSQLConnectionString"));
            System.Data.DataSet DataSetObj = new System.Data.DataSet();

            Utils.Sql.SqlHelper.FillDataset(SqlConnection, "Building_GetBuildingsAndWorkspaceCount", DataSetObj, new string[] { "Building" }, TenantId, location.Lat, location.Long);
            SqlConnection.Close();
           
            return Task.FromResult<DataTable>(DataSetObj.Tables[0]);
        }
       
        public Task<DataTable> AddBuilding(Building input)
        {
            string Map = Utils.ResourceHelper.GetResources("DefaultUrl");
            var SqlConnection = Utils.Config.DbUtils.GetDbConnection(Utils.Config.DbUtils.GetDbConnectionStringFromConfig("MeeloAzureSQLConnectionString"));
            if (input.Maps != null)
                Map = Convert.ToString(input.Maps.FirstOrDefault() ?? new Uri(Utils.ResourceHelper.GetResources("DefaultUrl")));
            DataSet DataSetObj = new DataSet();
            var Output = Utils.Sql.SqlHelper.ExecuteDataset(SqlConnection, "Org_Buildings_Insert", input.OrgId, input.Address.Street, input.BuildingName, input.Address.City,
                  input.Address.Country, input.Address.State, input.Address.PostalCode, input.Address.GpsLocation.Location, input.Address.GpsLocation.Lat, input.Address.GpsLocation.Long,
                  /*PrimaryContactId*/0, input.Description, Map, input.DoFloorsStartAtGround);

            SqlConnection.Close();
            return Task.FromResult<DataTable>(Output.Tables[0]);
        }
        public Task<DataTable> UpdateBuilding(Building input)
        {
            string Map = Utils.ResourceHelper.GetResources("DefaultUrl");
            var SqlConnection = Utils.Config.DbUtils.GetDbConnection(Utils.Config.DbUtils.GetDbConnectionStringFromConfig("MeeloAzureSQLConnectionString"));
            if (input.Maps != null)
                Map = Convert.ToString(input.Maps.FirstOrDefault() ?? new Uri(Utils.ResourceHelper.GetResources("DefaultUrl")));
            DataSet DataSetObj = new DataSet();
            var Output = Utils.Sql.SqlHelper.ExecuteDataset(SqlConnection, "Org_Buildings_Update",input.Id, input.OrgId, input.Address.Street, input.BuildingName, input.Address.City,
                  input.Address.Country, input.Address.State, input.Address.PostalCode, input.Address.GpsLocation.Location, input.Address.GpsLocation.Lat, input.Address.GpsLocation.Long,
                  /*PrimaryContactId*/0, input.Description, Map, input.DoFloorsStartAtGround);

            SqlConnection.Close();
            return Task.FromResult<DataTable>(Output.Tables[0]);
        }
        public void GetBuilding(DataTable dt, long OrgId, long BuildingId)
        {
            //var SqlConnection = Utils.Config.DbUtils.GetDbConnection(Utils.Config.DbUtils.GetDbConnectionStringFromConfig("MeeloAzureSQLConnectionString"));
            //DataSet DataSetObj = new DataSet();
            //dr = Utils.Sql.SqlHelper.ExecuteReader(SqlConnection, "Org_Buildings_SelectById", OrgId, BuildingId);
            //SqlConnection.Close();
        }

        public Task<DataTable> GetBuilding(long OrgId, int StartIndex, int PageSize, string SortBy, string SortOrder,string FilterbyBuilding)
        {
            var SqlConnection = Utils.Config.DbUtils.GetDbConnection(Utils.Config.DbUtils.GetDbConnectionStringFromConfig("MeeloAzureSQLConnectionString"));
            System.Data.DataSet DataSetObj = new System.Data.DataSet();

            Utils.Sql.SqlHelper.FillDataset(SqlConnection, "Org_Building_List", DataSetObj, new string[] { "Org_Buildings" }, OrgId, FilterbyBuilding, StartIndex, PageSize, SortBy,SortOrder);
            SqlConnection.Close();

           DataTable dt = DataSetObj.Tables[0];
            return Task.FromResult<DataTable>(dt);
        }
        public Task<DataTable> GetBuilding(long OrgId, string FilterbyBuilding)
        {
            var SqlConnection = Utils.Config.DbUtils.GetDbConnection(Utils.Config.DbUtils.GetDbConnectionStringFromConfig("MeeloAzureSQLConnectionString"));
            System.Data.DataSet DataSetObj = new System.Data.DataSet();

            Utils.Sql.SqlHelper.FillDataset(SqlConnection, "Org_Buildings_Select", DataSetObj, new string[] { "Org_Buildings" }, OrgId, FilterbyBuilding);
            SqlConnection.Close();

            DataTable dt = DataSetObj.Tables[0];
            return Task.FromResult<DataTable>(dt);
        }

    }
}
