ALTER TABLE [dbo].[Worspace_Blackouts] DROP CONSTRAINT [FK_Worspace_Blackouts_Workspaces]
GO
ALTER TABLE [dbo].[Workspaces] DROP CONSTRAINT [FK_Workspaces_Organizations]
GO
ALTER TABLE [dbo].[Workspaces] DROP CONSTRAINT [FK_Workspaces_Org_Buildings]
GO
ALTER TABLE [dbo].[Workspace_Rules] DROP CONSTRAINT [FK_Workspace_Rules_Workspaces]
GO
ALTER TABLE [dbo].[Workspace_Reviews] DROP CONSTRAINT [FK_Workspace_Reviews_Workspaces]
GO
ALTER TABLE [dbo].[Workspace_Capabilities] DROP CONSTRAINT [FK_Workspace_Capabilities_Workspaces]
GO
ALTER TABLE [dbo].[Workspace_Capabilities] DROP CONSTRAINT [FK_Workspace_Capabilities_CapabilitiesList]
GO
ALTER TABLE [dbo].[Workspace_Available_Services] DROP CONSTRAINT [FK_Workspace_Available_Services_Workspaces]
GO
ALTER TABLE [dbo].[Workspace_Announcements] DROP CONSTRAINT [FK_Workspace_Announcements_Workspaces]
GO
ALTER TABLE [dbo].[Users] DROP CONSTRAINT [FK_Users_Organizations]
GO
ALTER TABLE [dbo].[User_SubscribedServices] DROP CONSTRAINT [FK_User_SubscribedServices_Users]
GO
ALTER TABLE [dbo].[User_SubscribedServices] DROP CONSTRAINT [FK_User_SubscribedServices_ServiceCatalog]
GO
ALTER TABLE [dbo].[User_Settings] DROP CONSTRAINT [FK_User_Settings_Users]
GO
ALTER TABLE [dbo].[User_Session] DROP CONSTRAINT [FK_User_Session_Users]
GO
ALTER TABLE [dbo].[User_Roles] DROP CONSTRAINT [FK_User_Roles_Users]
GO
ALTER TABLE [dbo].[User_Roles] DROP CONSTRAINT [FK_User_Roles_System_Roles]
GO
ALTER TABLE [dbo].[User_Preferences] DROP CONSTRAINT [FK_User_Preferences_Users]
GO
ALTER TABLE [dbo].[User_Calendars] DROP CONSTRAINT [FK_User_Calendars_Users]
GO
ALTER TABLE [dbo].[User_AccessLocations] DROP CONSTRAINT [FK_User_AccessLocations_Users]
GO
ALTER TABLE [dbo].[System_Role_Permissions] DROP CONSTRAINT [FK_System_Role_Permissions_System_Roles]
GO
ALTER TABLE [dbo].[System_Role_Permissions] DROP CONSTRAINT [FK_System_Role_Permissions_System_Permissions]
GO
ALTER TABLE [dbo].[Resource_Yearly_Utilization] DROP CONSTRAINT [FK_Resource_Yearly_Utilization_Workspaces]
GO
ALTER TABLE [dbo].[Resource_Weekly_Utilization] DROP CONSTRAINT [FK_Resource_Weekly_Utilization_Workspaces]
GO
ALTER TABLE [dbo].[Resource_Usage_Status] DROP CONSTRAINT [FK_Resource_Usage_Status_Workspaces]
GO
ALTER TABLE [dbo].[Resource_Daily_Utilization] DROP CONSTRAINT [FK_Resource_Daily_Utilization_Workspaces]
GO
ALTER TABLE [dbo].[PlanAssignments] DROP CONSTRAINT [FK_PlanAssignments_Organizations]
GO
ALTER TABLE [dbo].[PlanAssignments] DROP CONSTRAINT [FK_PlanAssignments_MeeloPlans]
GO
ALTER TABLE [dbo].[Org_Subscribed_Common_Services] DROP CONSTRAINT [FK_Org_Subscribed_Common_Services_ServiceCatalog]
GO
ALTER TABLE [dbo].[Org_Subscribed_Common_Services] DROP CONSTRAINT [FK_Org_Subscribed_Common_Services_Organizations]
GO
ALTER TABLE [dbo].[Org_GlobalSettings] DROP CONSTRAINT [FK_Org_GlobalSettings_Organizations]
GO
ALTER TABLE [dbo].[Org_GlobalEvents] DROP CONSTRAINT [FK_Org_GlobalEvents_Organizations]
GO
ALTER TABLE [dbo].[Org_Buildings] DROP CONSTRAINT [FK_Org_Buildings_Organizations]
GO
ALTER TABLE [dbo].[Org_Building_Floor] DROP CONSTRAINT [FK_Org_Building_Floor_Org_Buildings]
GO
ALTER TABLE [dbo].[LicenseAssignments] DROP CONSTRAINT [FK_LicenseAssignments_Organizations]
GO
ALTER TABLE [dbo].[Global_Announcements] DROP CONSTRAINT [FK_Global_Announcements_Organizations]
GO
ALTER TABLE [dbo].[Event] DROP CONSTRAINT [FK_Event_Workspaces]
GO
ALTER TABLE [dbo].[Attendees] DROP CONSTRAINT [FK_Attendees_Users]
GO
ALTER TABLE [dbo].[Attendees] DROP CONSTRAINT [FK_Attendees_Event]
GO
/****** Object:  Index [PK_Organizations]    Script Date: 6/11/2017 4:38:16 PM ******/
ALTER TABLE [dbo].[Organizations] DROP CONSTRAINT [PK_Organizations]
GO
/****** Object:  Table [dbo].[Worspace_Blackouts]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[Worspace_Blackouts]
GO
/****** Object:  Table [dbo].[Workspaces]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[Workspaces]
GO
/****** Object:  Table [dbo].[Workspace_Rules]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[Workspace_Rules]
GO
/****** Object:  Table [dbo].[Workspace_Reviews]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[Workspace_Reviews]
GO
/****** Object:  Table [dbo].[Workspace_Capabilities]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[Workspace_Capabilities]
GO
/****** Object:  Table [dbo].[Workspace_Available_Services]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[Workspace_Available_Services]
GO
/****** Object:  Table [dbo].[Workspace_Announcements]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[Workspace_Announcements]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[Users]
GO
/****** Object:  Table [dbo].[User_SubscribedServices]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[User_SubscribedServices]
GO
/****** Object:  Table [dbo].[User_Settings]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[User_Settings]
GO
/****** Object:  Table [dbo].[User_Session]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[User_Session]
GO
/****** Object:  Table [dbo].[User_Roles]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[User_Roles]
GO
/****** Object:  Table [dbo].[User_Preferences]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[User_Preferences]
GO
/****** Object:  Table [dbo].[User_Calendars]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[User_Calendars]
GO
/****** Object:  Table [dbo].[User_AccessLocations]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[User_AccessLocations]
GO
/****** Object:  Table [dbo].[System_Roles]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[System_Roles]
GO
/****** Object:  Table [dbo].[System_Role_Permissions]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[System_Role_Permissions]
GO
/****** Object:  Table [dbo].[System_Permissions]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[System_Permissions]
GO
/****** Object:  Table [dbo].[ServiceCatalog]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[ServiceCatalog]
GO
/****** Object:  Table [dbo].[Resource_Yearly_Utilization]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[Resource_Yearly_Utilization]
GO
/****** Object:  Table [dbo].[Resource_Weekly_Utilization]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[Resource_Weekly_Utilization]
GO
/****** Object:  Table [dbo].[Resource_Usage_Status]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[Resource_Usage_Status]
GO
/****** Object:  Table [dbo].[Resource_Daily_Utilization]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[Resource_Daily_Utilization]
GO
/****** Object:  Table [dbo].[PlanAssignments]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[PlanAssignments]
GO
/****** Object:  Index [IX_Organizations]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP INDEX [IX_Organizations] ON [dbo].[Organizations] WITH ( ONLINE = OFF )
GO
/****** Object:  Table [dbo].[Organizations]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[Organizations]
GO
/****** Object:  Table [dbo].[Org_Subscribed_Common_Services]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[Org_Subscribed_Common_Services]
GO
/****** Object:  Table [dbo].[Org_GlobalSettings]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[Org_GlobalSettings]
GO
/****** Object:  Table [dbo].[Org_GlobalEvents]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[Org_GlobalEvents]
GO
/****** Object:  Table [dbo].[Org_Buildings]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[Org_Buildings]
GO
/****** Object:  Table [dbo].[Org_Building_Floor]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[Org_Building_Floor]
GO
/****** Object:  Table [dbo].[MeeloPlans]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[MeeloPlans]
GO
/****** Object:  Table [dbo].[LicenseAssignments]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[LicenseAssignments]
GO
/****** Object:  Table [dbo].[Global_Settings]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[Global_Settings]
GO
/****** Object:  Table [dbo].[Global_Announcements]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[Global_Announcements]
GO
/****** Object:  Table [dbo].[Event]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[Event]
GO
/****** Object:  Table [dbo].[CapabilitiesList]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[CapabilitiesList]
GO
/****** Object:  Table [dbo].[Attendees]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP TABLE [dbo].[Attendees]
GO
/****** Object:  Database [meelodb]    Script Date: 6/11/2017 4:38:16 PM ******/
DROP DATABASE [meelodb]
GO
/****** Object:  Database [meelodb]    Script Date: 6/11/2017 4:38:16 PM ******/
CREATE DATABASE [meelodb]  ;
GO
ALTER DATABASE [meelodb] SET COMPATIBILITY_LEVEL = 130
GO
ALTER DATABASE [meelodb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [meelodb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [meelodb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [meelodb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [meelodb] SET ARITHABORT OFF 
GO
ALTER DATABASE [meelodb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [meelodb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [meelodb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [meelodb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [meelodb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [meelodb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [meelodb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [meelodb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [meelodb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [meelodb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [meelodb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [meelodb] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [meelodb] SET  MULTI_USER 
GO
ALTER DATABASE [meelodb] SET QUERY_STORE = OFF
GO
/*** The scripts of database scoped configurations in Azure should be executed inside the target database connection. ***/
GO
-- ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
-- ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
-- ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
-- ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
-- ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
-- ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
-- ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
-- ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
/****** Object:  Table [dbo].[Attendees]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Attendees](
	[Id] [bigint] NOT NULL,
	[OrgId] [bigint] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[EventId] [bigint] NOT NULL,
	[IsOrganizer] [bit] NOT NULL,
	[AcceptanceType] [smallint] NULL,
	[ReplyLocation] [nchar](10) NULL,
	[LocationString] [nvarchar](255) NULL,
	[Lat] [decimal](18, 0) NULL,
	[Long] [decimal](18, 0) NULL,
	[AttendanceType] [tinyint] NULL,
 CONSTRAINT [PK_Attendees] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[CapabilitiesList]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CapabilitiesList](
	[Id] [bigint] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[IconUri] [varchar](max) NOT NULL,
	[Description] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_CapabilitiesList] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[Event]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Event](
	[Id] [bigint] NOT NULL,
	[OrgId] [bigint] NOT NULL,
	[Subject] [nvarchar](max) NULL,
	[CreatedTime] [datetime] NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NOT NULL,
	[Body] [ntext] NULL,
	[BodyPreview] [ntext] NULL,
	[Categories] [nvarchar](max) NULL,
	[iCalUid] [varchar](255) NULL,
	[RefId] [varchar](255) NULL,
	[Importance] [smallint] NULL,
	[UserId] [bigint] NULL,
	[Sensitivity] [smallint] NULL,
	[EventType] [smallint] NULL,
	[WebLink] [varchar](max) NULL,
	[CheckinTime] [datetime] NULL,
	[CheckoutTime] [datetime] NULL,
	[CheckinPerformedBy] [varchar](50) NULL,
	[CheckoutPerformedBy] [varchar](50) NULL,
	[CalendarId] [bigint] NULL,
	[WorkspaceId] [bigint] NULL,
	[OnlineMeetingUrl] [nvarchar](max) NULL,
 CONSTRAINT [PK_Event] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[Global_Announcements]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Global_Announcements](
	[Id] [bigint] NOT NULL,
	[OrgId] [bigint] NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[ExpirationTime] [datetime] NOT NULL,
	[Severity] [smallint] NOT NULL,
 CONSTRAINT [PK_Global_Announcements] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[Global_Settings]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Global_Settings](
	[Id] [bigint] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Value] [ntext] NOT NULL,
	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NULL,
	[UpdatedBy] [bigint] NOT NULL,
 CONSTRAINT [PK_Global_Settings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[LicenseAssignments]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LicenseAssignments](
	[Id] [bigint] NOT NULL,
	[OrgId] [bigint] NOT NULL,
	[LicenseId] [bigint] NOT NULL,
 CONSTRAINT [PK_LicenseAssignments] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[MeeloPlans]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeeloPlans](
	[Id] [bigint] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_MeeloPlans] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[Org_Building_Floor]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Org_Building_Floor](
	[Id] [bigint] NOT NULL,
	[OrgId] [bigint] NOT NULL,
	[BuildingId] [bigint] NOT NULL,
	[FloorNumber] [int] NOT NULL,
	[Name] [nvarchar](25) NULL,
	[Description] [nvarchar](max) NULL,
	[MapsUri] [varchar](max) NULL,
 CONSTRAINT [PK_Org_Building_Floor] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[Org_Buildings]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Org_Buildings](
	[Id] [bigint] NOT NULL,
	[OrgId] [bigint] NOT NULL,
	[Street] [nvarchar](255) NULL,
	[Building] [nvarchar](255) NULL,
	[City] [nvarchar](50) NULL,
	[Country] [nvarchar](30) NULL,
	[State] [nvarchar](50) NULL,
	[PostalCode] [nvarchar](50) NULL,
	[LocationString] [nvarchar](255) NULL,
	[Lat] [decimal](18, 0) NULL,
	[Long] [decimal](18, 0) NULL,
	[PrimaryContactId] [bigint] NOT NULL,
	[Description] [nvarchar](max) NULL,
	[MapsUri] [varchar](max) NULL,
	[DoFloorsStartAtGround] [bit] NULL,
 CONSTRAINT [PK_Org_Buildings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[Org_GlobalEvents]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Org_GlobalEvents](
	[Id] [bigint] NOT NULL,
	[OrgId] [bigint] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [ntext] NOT NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[IsDayOff] [bit] NULL,
 CONSTRAINT [PK_Org_GlobalEvents] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[Org_GlobalSettings]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Org_GlobalSettings](
	[Id] [bigint] NOT NULL,
	[OrgId] [bigint] NOT NULL,
	[Name] [varchar](50) NULL,
	[Value] [ntext] NULL,
 CONSTRAINT [PK_GlobalSettings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[Org_Subscribed_Common_Services]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Org_Subscribed_Common_Services](
	[Id] [bigint] NOT NULL,
	[OrgId] [bigint] NOT NULL,
	[ServiceId] [bigint] NOT NULL,
 CONSTRAINT [PK_Org_Subscribed_Common_Services] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[Organizations]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Organizations](
	[Id] [bigint] NOT NULL,
	[FriendlyName] [nvarchar](255) NOT NULL,
	[CorporateName] [nvarchar](255) NOT NULL,
	[MeeloLicenseId] [bigint] NOT NULL,
	[Street] [nvarchar](255) NULL,
	[Building] [nvarchar](255) NULL,
	[City] [nvarchar](50) NULL,
	[Country] [nvarchar](30) NULL,
	[State] [nvarchar](50) NULL,
	[PostalCode] [nvarchar](50) NULL,
	[LocationString] [nvarchar](255) NULL,
	[Lat] [decimal](18, 0) NULL,
	[Long] [decimal](18, 0) NULL,
	[PrimaryContactId] [bigint] NULL
)
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Organizations]    Script Date: 6/11/2017 4:38:16 PM ******/
CREATE CLUSTERED INDEX [IX_Organizations] ON [dbo].[Organizations]
(
	[FriendlyName] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
GO
/****** Object:  Table [dbo].[PlanAssignments]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlanAssignments](
	[Id] [bigint] NOT NULL,
	[OrgId] [bigint] NOT NULL,
	[MeeloPlanId] [bigint] NOT NULL
)
GO
/****** Object:  Table [dbo].[Resource_Daily_Utilization]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Resource_Daily_Utilization](
	[Id] [bigint] NOT NULL,
	[ResourceId] [bigint] NULL,
	[Day] [datetime] NULL,
	[Utilization] [decimal](18, 0) NULL,
 CONSTRAINT [PK_Resource_Daily_Utilization] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[Resource_Usage_Status]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Resource_Usage_Status](
	[Id] [bigint] NOT NULL,
	[ResourceId] [bigint] NULL,
	[OrgId] [bigint] NULL,
	[EventId] [bigint] NULL,
	[AttendancePercent] [decimal](18, 0) NULL,
	[WorkspaceStatus] [smallint] NULL,
	[RemoteAttendancePercent] [decimal](18, 0) NULL,
	[InPersonAttendancePercent] [decimal](18, 0) NULL,
	[InPersonAttendanceToCapacityRatio] [decimal](18, 0) NULL,
	[TotalInPersonAttendees] [int] NULL,
	[TotalRemoteAttendees] [int] NULL,
 CONSTRAINT [PK_Resource_Usage_Status] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[Resource_Weekly_Utilization]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Resource_Weekly_Utilization](
	[Id] [bigint] NOT NULL,
	[ResourceId] [bigint] NOT NULL,
	[OrgId] [bigint] NOT NULL,
	[WeekStart] [datetime] NULL,
	[WeekEnd] [datetime] NULL,
	[Utilization] [decimal](18, 0) NULL,
 CONSTRAINT [PK_Resource_Weekly_Utilization] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[Resource_Yearly_Utilization]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Resource_Yearly_Utilization](
	[Id] [bigint] NOT NULL,
	[OrgId] [bigint] NOT NULL,
	[ResourceId] [bigint] NOT NULL,
	[Year] [date] NOT NULL,
	[Utilization] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_Resource_Yearly_Utilization] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[ServiceCatalog]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServiceCatalog](
	[Id] [bigint] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[DisplayName] [nvarchar](50) NULL,
	[Description] [nvarchar](max) NULL,
	[ServiceType] [smallint] NULL,
	[SourceUri] [varchar](max) NULL,
	[NavigationUri] [varchar](max) NULL,
	[Tags] [nvarchar](max) NULL,
	[IsLocationDependent] [bit] NULL,
	[AvailabilityRange_LocationString] [nvarchar](255) NULL,
	[AvailabilityRange_Lat] [decimal](18, 0) NULL,
	[AvailabilityRange_Long] [decimal](18, 0) NULL,
	[IconUri] [varchar](max) NULL,
	[AppliesToAll] [bit] NULL,
 CONSTRAINT [PK_ServiceCatalog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[System_Permissions]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[System_Permissions](
	[Id] [bigint] NOT NULL,
	[Name] [varchar](255) NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_System_Permissions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[System_Role_Permissions]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[System_Role_Permissions](
	[Id] [bigint] NOT NULL,
	[RoleId] [bigint] NOT NULL,
	[PermissionId] [bigint] NOT NULL,
 CONSTRAINT [PK_System_Role_Permissions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[System_Roles]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[System_Roles](
	[Id] [bigint] NOT NULL,
	[Name] [varchar](25) NOT NULL,
	[Description] [varchar](255) NOT NULL,
 CONSTRAINT [PK_System_Roles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[User_AccessLocations]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User_AccessLocations](
	[Id] [bigint] NOT NULL,
	[OrgId] [bigint] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[LocationString] [nvarchar](255) NULL,
	[Lat] [decimal](18, 0) NULL,
	[Long] [decimal](18, 0) NULL,
	[AccessTime] [datetime] NULL,
 CONSTRAINT [PK_User_AccessLocations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[User_Calendars]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User_Calendars](
	[Id] [bigint] NOT NULL,
	[OrgId] [bigint] NOT NULL,
	[Color] [varchar](25) NULL,
	[Name] [nvarchar](50) NULL,
	[UserId] [bigint] NOT NULL,
	[IsPrimary] [bit] NULL,
 CONSTRAINT [PK_User_Calendars] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[User_Preferences]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User_Preferences](
	[Id] [bigint] NOT NULL,
	[OrgId] [bigint] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[DefaultSelectionTimeInMins] [smallint] NULL,
	[StartupScreenPreference] [smallint] NULL,
	[LastLocationString] [nvarchar](255) NULL,
	[LastLat] [decimal](18, 0) NULL,
	[LastLong] [decimal](18, 0) NULL,
 CONSTRAINT [PK_User_Preferences] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[User_Roles]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User_Roles](
	[Id] [bigint] NOT NULL,
	[OrgId] [bigint] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[RoleId] [bigint] NOT NULL,
 CONSTRAINT [PK_User_Roles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[User_Session]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User_Session](
	[Id] [bigint] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[SessionData] [ntext] NULL,
	[SessionToken] [varchar](50) NULL,
	[LastModified] [datetime] NULL,
 CONSTRAINT [PK_User_Session] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[User_Settings]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User_Settings](
	[Id] [bigint] NOT NULL,
	[OrgId] [bigint] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[Value] [ntext] NULL,
 CONSTRAINT [PK_User_Settings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[User_SubscribedServices]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User_SubscribedServices](
	[Id] [bigint] NOT NULL,
	[OrgId] [bigint] NULL,
	[UserId] [bigint] NULL,
	[ServiceId] [bigint] NULL,
 CONSTRAINT [PK_SubscribedServices] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[Users]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [bigint] NOT NULL,
	[OrgId] [bigint] NOT NULL,
	[DisplayName] [nvarchar](50) NULL,
	[FirstName] [nvarchar](50) NULL,
	[PreferredName] [nvarchar](50) NULL,
	[JobTitle] [nvarchar](50) NULL,
	[Mail] [nvarchar](255) NULL,
	[MobilePhone] [nvarchar](15) NULL,
	[BusinessPhones] [nvarchar](60) NULL,
	[OfficeLocationString] [nvarchar](255) NULL,
	[OfficeLocationLat] [decimal](18, 0) NULL,
	[OfficeLocationLong] [decimal](18, 0) NULL,
	[PreferredLanguage] [varchar](10) NULL,
	[Birthday] [datetime] NULL,
	[Surname] [nvarchar](50) NULL,
	[UserPrincipalName] [nvarchar](255) NULL,
	[AccountEnabled] [bit] NULL,
	[MeeloLicenseId] [char](32) NULL,
	[MeeloPlanId] [char](32) NULL,
	[CompanyName] [nvarchar](255) NULL,
	[DepartmentName] [nvarchar](255) NULL,
	[DepartmentId] [nvarchar](255) NULL,
	[HireDate] [datetime] NULL,
	[CollabProviderUniqueId] [nvarchar](255) NULL,
	[ImAddresses] [nvarchar](255) NULL,
	[MailboxSettings_Timezone] [varchar](10) NULL,
	[MailboxSettings_Locale] [varchar](10) NULL,
	[MailboxSettings_Locale_DisplayName] [nvarchar](100) NULL,
	[AutomaticRepliesSetting_ExternalAudience] [varchar](20) NULL,
	[AutomaticRepliesSetting_ExternalReplyMessage] [ntext] NULL,
	[AutomaticRepliesSetting_InternalReplyMessage] [ntext] NULL,
	[AutomaticRepliesSetting_ScheduledStartTime] [datetime] NULL,
	[AutomaticRepliesSetting_ScheduledEndTime] [datetime] NULL,
	[AutomaticRepliesSetting_Status] [varchar](25) NULL,
	[Street] [nvarchar](255) NULL,
	[Building] [nvarchar](255) NULL,
	[Floor] [nvarchar](255) NULL,
	[City] [nvarchar](50) NULL,
	[Country] [nvarchar](30) NULL,
	[State] [nvarchar](50) NULL,
	[PostalCode] [nvarchar](50) NULL,
	[LocationString] [nvarchar](255) NULL,
	[Lat] [decimal](18, 0) NULL,
	[Long] [decimal](18, 0) NULL,
	[UsageLocation_LocationString] [nvarchar](255) NULL,
	[UsageLocation_Lat] [decimal](18, 0) NULL,
	[UsageLocation_Long] [decimal](18, 0) NULL,
	[Role] [varchar](25) NULL,
	[Created] [datetime] NULL,
	[Updated] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
	[Timezone] [varchar](85) NULL,
	[Language] [varchar](10) NULL,
	[LastLogin] [datetime] NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[Workspace_Announcements]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Workspace_Announcements](
	[Id] [bigint] NOT NULL,
	[OrgId] [bigint] NOT NULL,
	[WorkspaceId] [bigint] NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
	[StartTime] [datetime] NULL,
	[ExpirationTime] [datetime] NULL,
	[Severity] [smallint] NULL,
 CONSTRAINT [PK_Workspace_Announcements] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[Workspace_Available_Services]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Workspace_Available_Services](
	[Id] [bigint] NOT NULL,
	[ServiceId] [bigint] NOT NULL,
	[WorkspaceId] [bigint] NOT NULL,
	[OrgId] [bigint] NOT NULL,
 CONSTRAINT [PK_Workspace_Available_Services] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[Workspace_Capabilities]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Workspace_Capabilities](
	[Id] [bigint] NOT NULL,
	[WorkspaceId] [bigint] NOT NULL,
	[CapabilityId] [bigint] NOT NULL,
	[EnablementProcedure] [nvarchar](max) NULL,
	[OrgId] [bigint] NULL,
 CONSTRAINT [PK_Workspace_Capabilities] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[Workspace_Reviews]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Workspace_Reviews](
	[Id] [bigint] NOT NULL,
	[WorkspaceId] [bigint] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[Rating] [int] NULL,
	[Comments] [nvarchar](max) NULL,
	[OrgId] [bigint] NOT NULL,
 CONSTRAINT [PK_Workspace_Reviews] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[Workspace_Rules]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Workspace_Rules](
	[Id] [bigint] NOT NULL,
	[OrgId] [bigint] NOT NULL,
	[WorkspaceId] [bigint] NOT NULL,
	[MaxReservationTimeInMins] [int] NULL,
	[MinReservationTimeInMins] [int] NULL,
	[AccessibleToRoles] [varchar](500) NULL,
	[IsPublic] [bit] NULL,
	[DailyAvailableStartTime] [datetime] NULL,
	[DailyAvailableEndTime] [datetime] NULL,
	[CheckinReminderTimeBeforeInMins] [int] NULL,
	[CheckoutReminderTimeBeforeInMins] [int] NULL,
	[AutocancelAfterNoResponseInMins] [int] NULL,
	[AutocheckoutAfterNoResponseInMins] [int] NULL,
	[NumberOfRoomsUserCanReserveInaTimeslot] [smallint] NULL,
 CONSTRAINT [PK_Workspace_Rules] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[Workspaces]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Workspaces](
	[Id] [bigint] NOT NULL,
	[OrgId] [bigint] NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Description] [nvarchar](max) NULL,
	[FriendlyName] [nvarchar](50) NULL,
	[IdmResourceIdentity] [nvarchar](255) NULL,
	[BuildingId] [bigint] NULL,
	[FloorId] [bigint] NULL,
	[City] [nvarchar](50) NULL,
	[Country] [nvarchar](30) NULL,
	[State] [nvarchar](50) NULL,
	[PostalCode] [nvarchar](50) NULL,
	[LocationString] [nvarchar](255) NULL,
	[Lat] [decimal](18, 0) NULL,
	[Long] [decimal](18, 0) NULL,
	[WebLink] [varchar](max) NULL,
	[PhotoLinks] [varchar](max) NULL,
	[Ratings] [int] NULL,
	[Created] [datetime] NULL,
	[LastUpdated] [datetime] NULL,
	[NextReservationStartTime] [datetime] NULL,
	[NextreservationEndTime] [datetime] NULL,
	[LimitedAccessRoles] [varchar](255) NULL,
	[LimitedAccessUsers] [varchar](max) NULL,
	[Tags] [varchar](max) NULL,
	[MapsUri] [varchar](max) NULL,
	[WorkspaceType] [smallint] NULL,
	[OccupancyStatus] [smallint] NULL,
	[Capacity] [int] NULL,
	[LastRenovated] [datetime] NULL,
	[CommunicationInfo_Email] [nvarchar](65) NULL,
	[CommunicationInfo_PhoneNumber] [varchar](50) NULL,
	[CommunicationInfo_TelephonyDetails] [varchar](max) NULL,
	[CommunicationInfo_AccessDescription] [nvarchar](max) NULL,
	[CommunicationInfo_OtherInfo] [nvarchar](max) NULL,
	[IsAutoCheckinEnabled] [bit] NULL,
	[IsAutoCheckoutEnabled] [bit] NULL,
	[AccessType] [smallint] NULL,
	[MaintenanceOwner] [bigint] NULL,
	[AreaInSqFt] [decimal](18, 0) NULL,
	[DistanceFromFloorOrigin] [text] NULL,
	[HasControllableDoor] [bit] NULL,
	[BeaconUid] [text] NULL,
	[IsBlocked] [bit] NULL,
	[IsHidden] [bit] NULL,
 CONSTRAINT [PK_Workspaces] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Table [dbo].[Worspace_Blackouts]    Script Date: 6/11/2017 4:38:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Worspace_Blackouts](
	[Id] [bigint] NOT NULL,
	[OrgId] [bigint] NOT NULL,
	[WorkspaceId] [bigint] NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NULL,
 CONSTRAINT [PK_Worspace_Blackouts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  Index [PK_Organizations]    Script Date: 6/11/2017 4:38:17 PM ******/
ALTER TABLE [dbo].[Organizations] ADD  CONSTRAINT [PK_Organizations] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)
GO
ALTER TABLE [dbo].[Attendees]  WITH CHECK ADD  CONSTRAINT [FK_Attendees_Event] FOREIGN KEY([EventId])
REFERENCES [dbo].[Event] ([Id])
GO
ALTER TABLE [dbo].[Attendees] CHECK CONSTRAINT [FK_Attendees_Event]
GO
ALTER TABLE [dbo].[Attendees]  WITH CHECK ADD  CONSTRAINT [FK_Attendees_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Attendees] CHECK CONSTRAINT [FK_Attendees_Users]
GO
ALTER TABLE [dbo].[Event]  WITH CHECK ADD  CONSTRAINT [FK_Event_Workspaces] FOREIGN KEY([WorkspaceId])
REFERENCES [dbo].[Workspaces] ([Id])
GO
ALTER TABLE [dbo].[Event] CHECK CONSTRAINT [FK_Event_Workspaces]
GO
ALTER TABLE [dbo].[Global_Announcements]  WITH CHECK ADD  CONSTRAINT [FK_Global_Announcements_Organizations] FOREIGN KEY([Id])
REFERENCES [dbo].[Organizations] ([Id])
GO
ALTER TABLE [dbo].[Global_Announcements] CHECK CONSTRAINT [FK_Global_Announcements_Organizations]
GO
ALTER TABLE [dbo].[LicenseAssignments]  WITH CHECK ADD  CONSTRAINT [FK_LicenseAssignments_Organizations] FOREIGN KEY([OrgId])
REFERENCES [dbo].[Organizations] ([Id])
GO
ALTER TABLE [dbo].[LicenseAssignments] CHECK CONSTRAINT [FK_LicenseAssignments_Organizations]
GO
ALTER TABLE [dbo].[Org_Building_Floor]  WITH CHECK ADD  CONSTRAINT [FK_Org_Building_Floor_Org_Buildings] FOREIGN KEY([BuildingId])
REFERENCES [dbo].[Org_Buildings] ([Id])
GO
ALTER TABLE [dbo].[Org_Building_Floor] CHECK CONSTRAINT [FK_Org_Building_Floor_Org_Buildings]
GO
ALTER TABLE [dbo].[Org_Buildings]  WITH CHECK ADD  CONSTRAINT [FK_Org_Buildings_Organizations] FOREIGN KEY([OrgId])
REFERENCES [dbo].[Organizations] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Org_Buildings] CHECK CONSTRAINT [FK_Org_Buildings_Organizations]
GO
ALTER TABLE [dbo].[Org_GlobalEvents]  WITH CHECK ADD  CONSTRAINT [FK_Org_GlobalEvents_Organizations] FOREIGN KEY([OrgId])
REFERENCES [dbo].[Organizations] ([Id])
GO
ALTER TABLE [dbo].[Org_GlobalEvents] CHECK CONSTRAINT [FK_Org_GlobalEvents_Organizations]
GO
ALTER TABLE [dbo].[Org_GlobalSettings]  WITH CHECK ADD  CONSTRAINT [FK_Org_GlobalSettings_Organizations] FOREIGN KEY([OrgId])
REFERENCES [dbo].[Organizations] ([Id])
GO
ALTER TABLE [dbo].[Org_GlobalSettings] CHECK CONSTRAINT [FK_Org_GlobalSettings_Organizations]
GO
ALTER TABLE [dbo].[Org_Subscribed_Common_Services]  WITH CHECK ADD  CONSTRAINT [FK_Org_Subscribed_Common_Services_Organizations] FOREIGN KEY([OrgId])
REFERENCES [dbo].[Organizations] ([Id])
GO
ALTER TABLE [dbo].[Org_Subscribed_Common_Services] CHECK CONSTRAINT [FK_Org_Subscribed_Common_Services_Organizations]
GO
ALTER TABLE [dbo].[Org_Subscribed_Common_Services]  WITH CHECK ADD  CONSTRAINT [FK_Org_Subscribed_Common_Services_ServiceCatalog] FOREIGN KEY([ServiceId])
REFERENCES [dbo].[ServiceCatalog] ([Id])
GO
ALTER TABLE [dbo].[Org_Subscribed_Common_Services] CHECK CONSTRAINT [FK_Org_Subscribed_Common_Services_ServiceCatalog]
GO
ALTER TABLE [dbo].[PlanAssignments]  WITH CHECK ADD  CONSTRAINT [FK_PlanAssignments_MeeloPlans] FOREIGN KEY([MeeloPlanId])
REFERENCES [dbo].[MeeloPlans] ([Id])
GO
ALTER TABLE [dbo].[PlanAssignments] CHECK CONSTRAINT [FK_PlanAssignments_MeeloPlans]
GO
ALTER TABLE [dbo].[PlanAssignments]  WITH CHECK ADD  CONSTRAINT [FK_PlanAssignments_Organizations] FOREIGN KEY([OrgId])
REFERENCES [dbo].[Organizations] ([Id])
GO
ALTER TABLE [dbo].[PlanAssignments] CHECK CONSTRAINT [FK_PlanAssignments_Organizations]
GO
ALTER TABLE [dbo].[Resource_Daily_Utilization]  WITH CHECK ADD  CONSTRAINT [FK_Resource_Daily_Utilization_Workspaces] FOREIGN KEY([ResourceId])
REFERENCES [dbo].[Workspaces] ([Id])
GO
ALTER TABLE [dbo].[Resource_Daily_Utilization] CHECK CONSTRAINT [FK_Resource_Daily_Utilization_Workspaces]
GO
ALTER TABLE [dbo].[Resource_Usage_Status]  WITH CHECK ADD  CONSTRAINT [FK_Resource_Usage_Status_Workspaces] FOREIGN KEY([ResourceId])
REFERENCES [dbo].[Workspaces] ([Id])
GO
ALTER TABLE [dbo].[Resource_Usage_Status] CHECK CONSTRAINT [FK_Resource_Usage_Status_Workspaces]
GO
ALTER TABLE [dbo].[Resource_Weekly_Utilization]  WITH CHECK ADD  CONSTRAINT [FK_Resource_Weekly_Utilization_Workspaces] FOREIGN KEY([ResourceId])
REFERENCES [dbo].[Workspaces] ([Id])
GO
ALTER TABLE [dbo].[Resource_Weekly_Utilization] CHECK CONSTRAINT [FK_Resource_Weekly_Utilization_Workspaces]
GO
ALTER TABLE [dbo].[Resource_Yearly_Utilization]  WITH CHECK ADD  CONSTRAINT [FK_Resource_Yearly_Utilization_Workspaces] FOREIGN KEY([ResourceId])
REFERENCES [dbo].[Workspaces] ([Id])
GO
ALTER TABLE [dbo].[Resource_Yearly_Utilization] CHECK CONSTRAINT [FK_Resource_Yearly_Utilization_Workspaces]
GO
ALTER TABLE [dbo].[System_Role_Permissions]  WITH CHECK ADD  CONSTRAINT [FK_System_Role_Permissions_System_Permissions] FOREIGN KEY([PermissionId])
REFERENCES [dbo].[System_Permissions] ([Id])
GO
ALTER TABLE [dbo].[System_Role_Permissions] CHECK CONSTRAINT [FK_System_Role_Permissions_System_Permissions]
GO
ALTER TABLE [dbo].[System_Role_Permissions]  WITH CHECK ADD  CONSTRAINT [FK_System_Role_Permissions_System_Roles] FOREIGN KEY([RoleId])
REFERENCES [dbo].[System_Roles] ([Id])
GO
ALTER TABLE [dbo].[System_Role_Permissions] CHECK CONSTRAINT [FK_System_Role_Permissions_System_Roles]
GO
ALTER TABLE [dbo].[User_AccessLocations]  WITH CHECK ADD  CONSTRAINT [FK_User_AccessLocations_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[User_AccessLocations] CHECK CONSTRAINT [FK_User_AccessLocations_Users]
GO
ALTER TABLE [dbo].[User_Calendars]  WITH CHECK ADD  CONSTRAINT [FK_User_Calendars_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[User_Calendars] CHECK CONSTRAINT [FK_User_Calendars_Users]
GO
ALTER TABLE [dbo].[User_Preferences]  WITH CHECK ADD  CONSTRAINT [FK_User_Preferences_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[User_Preferences] CHECK CONSTRAINT [FK_User_Preferences_Users]
GO
ALTER TABLE [dbo].[User_Roles]  WITH CHECK ADD  CONSTRAINT [FK_User_Roles_System_Roles] FOREIGN KEY([RoleId])
REFERENCES [dbo].[System_Roles] ([Id])
GO
ALTER TABLE [dbo].[User_Roles] CHECK CONSTRAINT [FK_User_Roles_System_Roles]
GO
ALTER TABLE [dbo].[User_Roles]  WITH CHECK ADD  CONSTRAINT [FK_User_Roles_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[User_Roles] CHECK CONSTRAINT [FK_User_Roles_Users]
GO
ALTER TABLE [dbo].[User_Session]  WITH CHECK ADD  CONSTRAINT [FK_User_Session_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[User_Session] CHECK CONSTRAINT [FK_User_Session_Users]
GO
ALTER TABLE [dbo].[User_Settings]  WITH CHECK ADD  CONSTRAINT [FK_User_Settings_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[User_Settings] CHECK CONSTRAINT [FK_User_Settings_Users]
GO
ALTER TABLE [dbo].[User_SubscribedServices]  WITH CHECK ADD  CONSTRAINT [FK_User_SubscribedServices_ServiceCatalog] FOREIGN KEY([ServiceId])
REFERENCES [dbo].[ServiceCatalog] ([Id])
GO
ALTER TABLE [dbo].[User_SubscribedServices] CHECK CONSTRAINT [FK_User_SubscribedServices_ServiceCatalog]
GO
ALTER TABLE [dbo].[User_SubscribedServices]  WITH CHECK ADD  CONSTRAINT [FK_User_SubscribedServices_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[User_SubscribedServices] CHECK CONSTRAINT [FK_User_SubscribedServices_Users]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Organizations] FOREIGN KEY([OrgId])
REFERENCES [dbo].[Organizations] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Organizations]
GO
ALTER TABLE [dbo].[Workspace_Announcements]  WITH CHECK ADD  CONSTRAINT [FK_Workspace_Announcements_Workspaces] FOREIGN KEY([WorkspaceId])
REFERENCES [dbo].[Workspaces] ([Id])
GO
ALTER TABLE [dbo].[Workspace_Announcements] CHECK CONSTRAINT [FK_Workspace_Announcements_Workspaces]
GO
ALTER TABLE [dbo].[Workspace_Available_Services]  WITH CHECK ADD  CONSTRAINT [FK_Workspace_Available_Services_Workspaces] FOREIGN KEY([WorkspaceId])
REFERENCES [dbo].[Workspaces] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Workspace_Available_Services] CHECK CONSTRAINT [FK_Workspace_Available_Services_Workspaces]
GO
ALTER TABLE [dbo].[Workspace_Capabilities]  WITH CHECK ADD  CONSTRAINT [FK_Workspace_Capabilities_CapabilitiesList] FOREIGN KEY([CapabilityId])
REFERENCES [dbo].[CapabilitiesList] ([Id])
GO
ALTER TABLE [dbo].[Workspace_Capabilities] CHECK CONSTRAINT [FK_Workspace_Capabilities_CapabilitiesList]
GO
ALTER TABLE [dbo].[Workspace_Capabilities]  WITH CHECK ADD  CONSTRAINT [FK_Workspace_Capabilities_Workspaces] FOREIGN KEY([WorkspaceId])
REFERENCES [dbo].[Workspaces] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Workspace_Capabilities] CHECK CONSTRAINT [FK_Workspace_Capabilities_Workspaces]
GO
ALTER TABLE [dbo].[Workspace_Reviews]  WITH CHECK ADD  CONSTRAINT [FK_Workspace_Reviews_Workspaces] FOREIGN KEY([WorkspaceId])
REFERENCES [dbo].[Workspaces] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Workspace_Reviews] CHECK CONSTRAINT [FK_Workspace_Reviews_Workspaces]
GO
ALTER TABLE [dbo].[Workspace_Rules]  WITH CHECK ADD  CONSTRAINT [FK_Workspace_Rules_Workspaces] FOREIGN KEY([WorkspaceId])
REFERENCES [dbo].[Workspaces] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Workspace_Rules] CHECK CONSTRAINT [FK_Workspace_Rules_Workspaces]
GO
ALTER TABLE [dbo].[Workspaces]  WITH CHECK ADD  CONSTRAINT [FK_Workspaces_Org_Buildings] FOREIGN KEY([BuildingId])
REFERENCES [dbo].[Org_Buildings] ([Id])
GO
ALTER TABLE [dbo].[Workspaces] CHECK CONSTRAINT [FK_Workspaces_Org_Buildings]
GO
ALTER TABLE [dbo].[Workspaces]  WITH CHECK ADD  CONSTRAINT [FK_Workspaces_Organizations] FOREIGN KEY([OrgId])
REFERENCES [dbo].[Organizations] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Workspaces] CHECK CONSTRAINT [FK_Workspaces_Organizations]
GO
ALTER TABLE [dbo].[Worspace_Blackouts]  WITH CHECK ADD  CONSTRAINT [FK_Worspace_Blackouts_Workspaces] FOREIGN KEY([WorkspaceId])
REFERENCES [dbo].[Workspaces] ([Id])
GO
ALTER TABLE [dbo].[Worspace_Blackouts] CHECK CONSTRAINT [FK_Worspace_Blackouts_Workspaces]
GO
ALTER DATABASE [meelodb] SET  READ_WRITE 
GO
