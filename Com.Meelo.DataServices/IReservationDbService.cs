﻿using Com.Meelo.CoreLib;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.DataServices
{
    public interface IReservationDbService
    {
        Task<int> Reserve(Event Input);
        Task<int> CancelReservation(Event Input);
        Task<int> CancelReservation();
        Task<int> CheckOut(Event Input);
        Task<int> CheckIn(Event Input);
        Task<DataTable> GetReservations(DateTimeOffset? StartTime, DateTimeOffset? EndTime,string UserIdFilter,string TenantId);
        Task<int> GetReservationCount(DateTimeOffset? StartTime, DateTimeOffset? EndTime,string UserIdFilter);
    }
}
