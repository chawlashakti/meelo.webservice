﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.DataServices
{
   public class UserDbService:IUserDbService
    {
      public Task<Int64> AddUser(List<CoreLib.User> input)
        {
            var SqlConnection = Utils.Config.DbUtils.GetDbConnection(Utils.Config.DbUtils.GetDbConnectionStringFromConfig("MeeloAzureSQLConnectionString"));
          
            object Id = 0;
            foreach (CoreLib.User user in input)
            {
                    Id = Utils.Sql.SqlHelper.ExecuteScalar(SqlConnection, "Users_Insert",user.OrgId, user.DisplayName, user.FirstName, user.PreferredName, user.JobTitle, user.Mail, user.MobilePhone, user.BusinessPhones.FirstOrDefault(),
                    user.OfficeLocation.Location, user.OfficeLocation.Lat, user.OfficeLocation.Long, user.PreferredLanguage, DateTime.Now, user.Surname, user.UserPrincipalName,user.AccountEnabled, user.MeeloLicense.LicenseId, user.MeeloPlan.PlanId, user.CompanyName, user.DepartmentName, user.DepartmentId, DateTime.Now,
                    user.CollabProviderUniqueId, user.ImAddresses, user.MailboxSettings.TimeZone, user.MailboxSettings.LocaleInfo.Locale,user.MailboxSettings.LocaleInfo.DisplayName, user.MailboxSettings.AutomaticRepliesSetting.ExternalAudience,
                    user.MailboxSettings.AutomaticRepliesSetting.ExternalReplyMessage, user.MailboxSettings.AutomaticRepliesSetting.InternalReplyMessage,user.MailboxSettings.AutomaticRepliesSetting.ScheduledStartDateTime, user.MailboxSettings.AutomaticRepliesSetting.ScheduledEndDateTime,
                    user.MailboxSettings.AutomaticRepliesSetting.Status, user.Address.Street, user.Address.Building, user.Address.Floor, user.Address.City,user.Address.Country, user.Address.State, user.Address.PostalCode, user.OfficeLocation.Location, user.OfficeLocation.Lat, user.OfficeLocation.Long,
                    user.UsageLocation.Location, user.UsageLocation.Lat, user.UsageLocation.Long, user.Role, DateTime.Now, DateTime.Now, true, TimeZone.CurrentTimeZone.ToString(),user.PreferredLanguage, null, user.ObjectId);
            }
            SqlConnection.Close();
            return Task.FromResult<Int64>(Convert.ToInt64(Id));
        }
        public Task<SqlDataReader> GetUser(string ObjectId)
        {
            CoreLib.User user = new CoreLib.User();
            var SqlConnection = Utils.Config.DbUtils.GetDbConnection(Utils.Config.DbUtils.GetDbConnectionStringFromConfig("MeeloAzureSQLConnectionString"));
            var Output = Utils.Sql.SqlHelper.ExecuteReader(SqlConnection, "Users_SelectByObjectId", ObjectId);
            return Task.FromResult<SqlDataReader>(Output); ;
        }

        public Task<DataTable> GetUsers(string TenantId)
        {
            CoreLib.User user = new CoreLib.User();
            var SqlConnection = Utils.Config.DbUtils.GetDbConnection(Utils.Config.DbUtils.GetDbConnectionStringFromConfig("MeeloAzureSQLConnectionString"));
            System.Data.DataSet DataSetObj = new System.Data.DataSet();
            Utils.Sql.SqlHelper.FillDataset(SqlConnection, "Users_Select", DataSetObj, new string[] { "Users" },TenantId);
            return Task.FromResult<DataTable>(DataSetObj.Tables[0]); ;
        }

    }
}
