﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System.Configuration;

namespace meelobotcallback
{
    /// <summary>
    /// Example for proactively pushing a message to the bot from server
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string appdId = ConfigurationManager.AppSettings["MicrosoftAppId"];
                string appPassword = ConfigurationManager.AppSettings["MicrosoftAppPassword"];
                string baseUri = ConfigurationManager.AppSettings["ConnectorClientBaseUri"];

                var client = new ConnectorClient(new Uri(baseUri), appdId, appPassword);
                // var client = new ConnectorClient(new Uri(baseUri));

                //https://slack.botframework.com

                //From GitHub 
                // https://github.com/Microsoft/BotBuilder/issues/1844
                // https://github.com/Microsoft/BotBuilder/issues/1729
                //https://docs.botframework.com/en-us/csharp/builder/sdkreference/routing.html


                MicrosoftAppCredentials.TrustServiceUrl("https://slack.botframework.com");

                var botAccount = new ChannelAccount(name: "meelo", id: "B2PNSP6RG:T2CUWNSAY");
                var userAccount = new ChannelAccount(name: "tredkar", id: "U2CV06WG0:T2CUWNSAY");

                var tal2meeloChannel = new ConversationAccount()
                {
                    Id = "B2PNSP6RG:T2CUWNSAY:C35MFGXSS",
                    Name = "talk2meelo"

                };

                MicrosoftAppCredentials creds = new MicrosoftAppCredentials(appdId, appPassword);
                var connector = new ConnectorClient(new Uri("https://slack.botframework.com"), creds);

                var dcr = CreateDirectConversation(connector, botAccount, userAccount, "Look at this!");

                var members = GetConversationMembers(connector, "B2PNSP6RG:T2CUWNSAY:C35MFGXSS");

                var channelResponse = SendToChannel(connector, botAccount, tal2meeloChannel, "Look at this now!");



            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            Console.ReadLine();
        }

        public static IEnumerable<ChannelAccount> GetConversationMembers(ConnectorClient connector, string conversationId)
        {


            return connector.Conversations.GetConversationMembers(conversationId);
        }

        private static ResourceResponse CreateDirectConversation(ConnectorClient connector, ChannelAccount botAccount, ChannelAccount userAccount, string textMessage)
        {
            Task<ConversationResourceResponse> crs = connector.Conversations.CreateDirectConversationAsync(botAccount, userAccount);

            ConversationResourceResponse crsO = crs.Result;


            Task<ResourceResponse> t = connector.Conversations.SendToConversationAsync(new Activity()
            {
                Type = ActivityTypes.Message,
                From = botAccount,
                Recipient = userAccount,
                Text = textMessage
            }, crsO.Id);

            return t.Result;

        }

        private static ResourceResponse SendToChannel(ConnectorClient connector, ChannelAccount botAccount, ConversationAccount channelInfo, string textMessage)
        {

            Task<ResourceResponse> t1 = connector.Conversations.SendToConversationAsync(new Activity()
            {
                Type = ActivityTypes.Message,
                From = botAccount,
                // Recipient = userAccount,
                Conversation = channelInfo,
                Text = textMessage
            }, channelInfo.Id);


            return t1.Result;
        }


    }
}
