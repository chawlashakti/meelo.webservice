﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.CoreLib
{
   public class MeeloPlan
    {
        public DateTimeOffset AssignedDateTime { get; set; }
        public string PlanName { get; set; }

        public string PlanId { get; set; }

        public string CapabilityStatus { get; set; }

        public IEnumerable<string> ServicesEnabled { get; set; }

        public bool IsPlanEnabled { get; set; }
    }
}
