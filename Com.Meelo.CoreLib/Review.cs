﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.CoreLib
{
   public class Review
    {
        public User EnteredBy { get; set; }
        public string Comment { get; set; }

        public DateTimeOffset Entered { get; set; }

        public int Rating { get; set; }
    }
}
