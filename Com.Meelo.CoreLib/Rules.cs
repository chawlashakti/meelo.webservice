﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.CoreLib
{
    public class Rules
    {
        /// <summary>
        /// Maximum amount of time a resource can be reserved
        /// </summary>
        public int MaxReservationTimeInMins { get; set; }

        /// <summary>
        /// Minimum amount of time a resource can be reserved
        /// </summary>
        public int MinReservationTimeInMins { get; set; }

        /// <summary>
        /// Accessible only to roles
        /// </summary>
        public IEnumerable<string> AccessibleToRoles { get; set; }

        /// <summary>
        /// Is this a public resource
        /// </summary>
        public bool IsPublic { get; set; }

        /// <summary>
        /// Daily available start time
        /// </summary>
        public DateTimeOffset DailyAvailableStartTime { get; set; }

        public DateTimeOffset DailyAvailableEndTime { get; set; }

        /// <summary>
        /// When to send a checkin reminder
        /// </summary>
        public int CheckInReminderTimeBeforeInMins { get; set; }

        /// <summary>
        /// When to send a checkout reminder
        /// </summary>
        public int CheckoutReminderTimeBeforeInMins { get; set; }

        /// <summary>
        /// Auto-cancel the room if user doesn't checkin in the 
        /// </summary>
        public int AutocancelResourceAfterNoResponseInMins { get; set; }

        /// <summary>
        /// Auto-checkout resource if user doesn't checkout
        /// </summary>
        public int AutocheckoutResourceAfterNoResponseInMins {get;set;}

        /// <summary>
        /// Number of same kind of resources user can reserve in the same timeslot
        /// </summary>
        public int NumberOfRoomsUserCanReserveInaTimeslot { get; set; }

        /// <summary>
        /// The time window to make a room available for booking even if it is available
        /// </summary>
        public int MaximumTimeWindowBeforeAvailableRoomCanBeReservedInMins { get; set; }

        public bool ShowUpcomingOpeningsForWorkspaces { get; set; }
    }
}
