﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.CoreLib
{
    /// <summary>
    /// https://developer.microsoft.com/en-us/graph/docs/api-reference/v1.0/resources/datetimetimezone
    /// </summary>
    public class DateTimeTimeZone
    {
        public string DateTimeOffset { get; set; }

      
        public string TimeZone { get; set; } 
    }
}
