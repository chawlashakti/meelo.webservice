﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.CoreLib
{
    public class Photo
    {
        public Uri WebLink { get; set; }
        public string Id { get; set; }

        public int Height { get; set; }

        public int Width { get; set; }
    }
}
