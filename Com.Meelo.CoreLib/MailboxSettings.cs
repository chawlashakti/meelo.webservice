﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.CoreLib
{
   public class MailboxSettings
    {
        public string TimeZone { get; set; }

        public AutomaticRepliesSetting AutomaticRepliesSetting { get; set; }

        public LocaleInfo LocaleInfo { get; set; }
    }
}
