﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.CoreLib
{
    public enum ServiceProviderType
    {
        Document = 1,
        Email = 2,
        Conferencing = 3,
        Weather = 4,
        Food = 5,
        Notes = 6,
        Actions = 7,
        Recording = 8,
        CRM = 9,
        ChatChannel = 10
    }
}
