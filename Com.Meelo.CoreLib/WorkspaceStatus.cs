﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.CoreLib
{
    public enum WorkspaceStatusType
    {
        Free = 1,
        Reserved = 2,
        Occupied = 3,
        Busy=4,
        BusyNotConfirmed = 5
    }
}
