﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.CoreLib
{
    public enum MobileDeviceType
    {
        Android = 1,
        iPhone=2,
        Windows=3,
        Other=4
    }
   public class UserProfile
    {
        public User User { get; set; }

        public IDictionary<string, string> AccessAccountDetails { get; set; }

        public Rules GlobalRules { get; set; }

        public IDictionary<string, string> MobilePermissionSets { get; set; }

      

        public DateTimeOffset LastAccess { get; set; }
    }
}
