﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.CoreLib
{
  

    public interface IService
    {
        string Name { get; set; }

        string Description { get; set; }

        ServiceProviderType ServiceType { get; set; }

        Uri SourceUri { get; set; }

        Uri NavigationUri { get; set; }

        AuthenticationInput AuthInfo { get; set; }

        IDictionary<string, string> Tags { get; set; }

        bool IsLocationDependent { get; set; }

        GpsLocation AvailabilityRange { get; set; }

        Uri IconUri { get; set; }

        bool AppliesToAll { get; set; }
    }
}
