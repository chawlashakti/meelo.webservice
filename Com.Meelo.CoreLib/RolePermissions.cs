﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.CoreLib
{
    public enum RolePermissions
    {
        CanAddWorkspaces = 1,
        CanRemoveWorkspaces = 2,
        CanProvisionServices = 3,
        CanDeprovisionServices = 4,
        CanBlockUser = 5,
        CanUnblockUser = 6,
        CanUpdateLicense = 7,
        CanUpdatePlan = 9
    }
}
