﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.CoreLib
{
    public interface ICapability
    {
         string Id { get; set; }

         string Name { get; set; }

        string EnablementProcedure { get; set; }

         Uri IconUri { get; set; }

        string Description { get; set; }
    }
}
