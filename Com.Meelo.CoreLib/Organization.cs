﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.CoreLib
{
    public class Organization
    {
        public long OrgId { get; set; }

        public string FriendlyName { get; set; }

        public string CorporateName { get; set; }

        public MeeloLicense MeeloLicense { get; set; }

        public Address Address { get; set; }

        public User OrgContact { get; set; }

        public IEnumerable<User> Administrators { get; set; }

        public string TenantId { get; set; }

        public MeeloPlan MeeloPlan { get; set; }
    }
}
