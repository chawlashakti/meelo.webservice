﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.CoreLib
{
   public class Attendee
    {
        public User User { get; set; }

        public AcceptanceType AcceptanceType { get; set; }

        public bool IsOrganizer { get; set; }

        public GpsLocation ReplyLocation { get; set; }

        public DateTimeOffset ReplyTime { get; set; }

        public AttendanceType AttendanceType { get; set; }

    }
}
