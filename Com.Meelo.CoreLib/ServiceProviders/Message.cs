﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.CoreLib.ServiceProviders
{
   public class Message
    {
        public string Id { get; set; }

        public User Sender { get; set; }

        public IEnumerable<User> Recipients { get; set; }

        public IEnumerable<User> BccRecipients { get; set; }

        public IEnumerable<User> CcRecipients { get; set; }

        public MessageBody Body { get; set; }

        public string Subject { get; set; }

        public string ConversationId { get; set; }

        public DateTimeOffset CreatedDateTime { get; set; }

        public bool HasAttachments { get; set; }

        public ImportanceType Importance { get; set; }

       public DateTimeOffset ReceivedDateTime { get; set; }

        public Uri WebLink { get; set; }

        public IEnumerable<Attachment> Attachments { get; set; }
    }
}
