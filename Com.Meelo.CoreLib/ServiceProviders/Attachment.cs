﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.CoreLib.ServiceProviders
{
    public class Attachment
    {
        public string ContentType { get; set; }
        public string Id { get; set; }
        public bool IsInline { get; set; }
        public DateTimeOffset LastModified { get; set; }
        public string Name { get; set; }
        public int Size { get; set; }
    }
}
