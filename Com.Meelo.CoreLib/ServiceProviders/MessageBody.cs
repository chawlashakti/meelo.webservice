﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.CoreLib.ServiceProviders
{
   public class MessageBody
    {
        public string Content { get; set; }

        public string ContentType { get; set; }
    }
}
