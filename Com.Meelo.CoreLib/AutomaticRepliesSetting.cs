﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.CoreLib
{
   public class AutomaticRepliesSetting
    {
        public string ExternalAudience { get; set; }

        public string ExternalReplyMessage { get; set; }

        public string InternalReplyMessage { get; set; }

        public DateTimeTimeZone ScheduledStartDateTime { get; set; }

        public DateTimeTimeZone ScheduledEndDateTime { get; set; }

        //disabled, alwaysEnabled, scheduled.
        public string Status { get; set; }
    }
}
