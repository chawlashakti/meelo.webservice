﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.CoreLib
{
    public enum AcceptanceType
    {
        Accepted = 1,
        Declined = -1,
        Tentative = 0
    }
}
