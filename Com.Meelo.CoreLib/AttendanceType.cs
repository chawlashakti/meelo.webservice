﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.CoreLib
{
    public enum AttendanceType
    {
        Required = 1,
        Optional = 2,
        External = 3
    }
}
