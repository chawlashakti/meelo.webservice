﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.CoreLib
{
   public class CommunicationInfo
    {
        string Email { get; set; }
        public string PhoneNumber { get; set; }

        public string TelephonyDetails { get; set; }

        public string AccessDescription { get; set; }

        public IDictionary<string, string> OtherInfo { get; set; }

    }
}
