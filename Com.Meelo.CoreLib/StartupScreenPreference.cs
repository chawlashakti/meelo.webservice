﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.CoreLib
{
    public enum StartupScreenPreference
    {
        Workspaces = 0,
        Attendees = 1,
        Time=2
    }
}
