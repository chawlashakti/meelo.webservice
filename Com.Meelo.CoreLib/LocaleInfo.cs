﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.CoreLib
{
    /// <summary>
    /// https://developer.microsoft.com/en-us/graph/docs/api-reference/v1.0/resources/localeinfo
    /// </summary>
    public class LocaleInfo
    {
        public string Locale { get; set; }

        public string DisplayName { get; set; }
    }
}
