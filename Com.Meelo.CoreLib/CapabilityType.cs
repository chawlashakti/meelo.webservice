﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.CoreLib
{
    public enum CapabilityType
    {
        None = 0,
        Whiteboard = 1,
        Lock = 2,
        VideoUnit = 3,
        Landline=4,
        Wifi=5,
        VoiceConferenceUnit=6,
        Television=7,
        Display=8,
        CellReception=9,
        Table=10,
        Chairs=11
    }
}
