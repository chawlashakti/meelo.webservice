﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.CoreLib.ViewModel
{
   public class BuildingWorkspaceRequestModel
    {
        public decimal Lat { get; set; }

        public decimal Long { get; set; }

        public string TenantId { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }
        public long BuildingId { get; set; }
        public string SearchText { get; set; }

    }
}
