﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.CoreLib.ViewModel
{
   public class WorkspaceViewModel
    {
        public long Id { get; set; }
        public long OrgId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string FriendlyName { get; set; }

        public string IdmResourceIdentity { get; set; }

        public Address Address { get; set; }

        public Uri WebLink { get; set; }

        public IEnumerable<Photo> Photos { get; set; }

        public int Ratings { get; set; }

        public DateTimeOffset Created { get; set; }

        public DateTimeOffset LastUpdated { get; set; }

        public DateTimeOffset? NextReservationStartTime { get; set; }

        public DateTimeOffset? NextReservationEndTime { get; set; }

        public IEnumerable<RoleType> LimitedAccessRoles { get; set; }

        public IEnumerable<User> LimitedAccessUsers { get; set; }

        public IDictionary<string, string> Tags { get; set; }

        public IEnumerable<Uri> Maps { get; set; }

        public WorkspaceType WorkspaceType { get; set; }

        public WorkspaceStatusType OccupancyStatus { get; set; }

        public int Capacity { get; set; }

        public DateTimeOffset LastRenovated { get; set; }

        public CommunicationInfo CommunicationInfo { get; set; }

        public bool IsAutoCheckinEnabled { get; set; }

        public bool IsAutoCheckoutEnabled { get; set; }

        public WorkspaceAccessType AccessType { get; set; }

        public long MaintenanceOwner { get; set; }

        public decimal AreaInSqFt { get; set; }

        public Point DistanceFromFloorOrigin { get; set; }

        public bool HasControllableDoor { get; set; }

        public string BeaconUid { get; set; }

        public bool IsBlocked { get; set; }

        public bool IsHidden { get; set; }

    }
}
