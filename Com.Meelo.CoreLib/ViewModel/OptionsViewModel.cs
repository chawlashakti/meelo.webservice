﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.CoreLib.ViewModel
{
   public class OptionsViewModel
    {
        public string Value { get; set; }
        public string DisplayText { get; set; }
    }
}
