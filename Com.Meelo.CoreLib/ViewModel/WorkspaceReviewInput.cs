﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.CoreLib.ViewModel
{
   public class WorkspaceReviewInput
    {
        public long WorkspaceId { get; set; }
        public string ObjectId { get; set; }
        public int Rating { get; set; }
        public string Comments { get; set; }
        public long OrgId { get; set; }
    }
}
