﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.BusinessServices.Workspace
{
   public class RateWorkspaceInput : InputBaseClass
    {
        public int Rating { get; set; }

        public string Comment { get; set; }
    }
}
