﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.BusinessServices.Workspace
{
    public class GetWorkspaceStatusInput : InputBaseClass
    {
        public long WorkspaceId { get; set; }

        public bool IsSimple { get; set; }
    }
}
