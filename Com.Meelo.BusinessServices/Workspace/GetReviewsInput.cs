﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.BusinessServices.Workspace
{
   public class GetReviewsInput : InputBaseClass
    {
        public string WorkspaceId { get; set; }
    }
}
