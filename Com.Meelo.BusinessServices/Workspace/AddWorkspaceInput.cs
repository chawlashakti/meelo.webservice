﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.Meelo.CoreLib;

namespace Com.Meelo.BusinessServices.Workspace
{
    public class AddWorkspaceInput : InputBaseClass
    {
       public CoreLib.Workspace Workspace { get; set; }
       public List<CoreLib.Workspace> WorkspaceList { get; set; }
        public long OrgId { get; set; }
    }
}
