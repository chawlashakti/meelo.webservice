﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.Meelo.CoreLib;

namespace Com.Meelo.BusinessServices.Workspace
{
   public class AddWorkspaceOutput : OutputBaseClass
    {
        public Com.Meelo.CoreLib.Workspace Workspace { get; set; }
        public int SuccessRecordsCount { get; set; }
        public int FailureRecordsCount { get; set; }
    }
}
