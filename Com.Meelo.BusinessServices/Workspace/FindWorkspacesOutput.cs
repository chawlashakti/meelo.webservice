﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.Meelo.CoreLib;

namespace Com.Meelo.BusinessServices.Workspace
{
    public class FindWorkspacesOutput : OutputBaseClass
    {
        public IEnumerable<CoreLib.Workspace> Workspaces { get; set; }
    }
}
