﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.Meelo.CoreLib;

namespace Com.Meelo.BusinessServices.Workspace
{
    public class FindWorkspacesInput : InputBaseClass
    {

        public GpsLocation Where { get; set; }
        public DateTimeOffset Start { get; set; }
        public DateTimeOffset End { get; set; }
        public int DurationInMinutes { get; set; }

        public string WorkspaceIdFilter { get; set; }
        public WorkspaceType? TypeFilter { get; set; }

        public string WorkspaceNameFilter { get; set; }

        public string BuildingNameFilter { get; set; }

        public IEnumerable<ICapability> CapabilitiesFilter { get; set; }
    }
}
