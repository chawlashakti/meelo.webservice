﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.Meelo.CoreLib;
using Com.Meelo.Utils;
using System.Data;
using Com.Meelo.DataServices;
using Com.Meelo.CoreLib.ViewModel;
using System.Data.SqlClient;
namespace Com.Meelo.BusinessServices.Workspace
{
    public class WorkspaceService: IWorkspaceService
    {
        public Organization OrganizationObj;
        private IWorkspaceDbService iWorkspaceDbServiceObj;
        #region CRUD
        public async  Task<AddWorkspaceOutput> AddWorkspace(AddWorkspaceInput input)
        {
            iWorkspaceDbServiceObj = new WorkspaceDbService();
            DataTable dt = await iWorkspaceDbServiceObj.AddWorkspace(input.WorkspaceList);
            AddWorkspaceOutput Output = new AddWorkspaceOutput();
            List<CoreLib.Workspace> List = new List<CoreLib.Workspace>();
            Mapper mapper = new Mapper();
            mapper.Map(dt,List);
            Output.Workspace = List?.FirstOrDefault();
            return Output;
        }
        public async Task<AddWorkspaceOutput> AddWorkspaces(AddWorkspaceInput input)
        {
            AddWorkspaceOutput Output = new AddWorkspaceOutput();
            Output.IsCallSuccessful = true;
            iWorkspaceDbServiceObj = new WorkspaceDbService();
            foreach (CoreLib.Workspace obj in input.WorkspaceList)
            {
                DataTable dt = await iWorkspaceDbServiceObj.AddWorkspace(obj);
                if (dt == null || dt.Rows.Count <= 0)
                {
                    Output.FailureRecordsCount++;
                }
                else
                {
                    Output.SuccessRecordsCount++;
                }
            }
            if (Output.FailureRecordsCount > 0)
                Output.IsCallSuccessful = false;
            return Task.FromResult<AddWorkspaceOutput>(Output).Result;
        }
        public async Task<AddWorkspaceOutput> AddWorkspace(DataTable input)
        {
            AddWorkspaceInput WorkspaceInput = new AddWorkspaceInput();
            WorkspaceInput.WorkspaceList = new List<CoreLib.Workspace>();
            var Result = ValidateWorkspaceData(input, WorkspaceInput);
            AddWorkspaceOutput Output = new AddWorkspaceOutput();
            Output.IsCallSuccessful = true;
            if (Result.IsCallSuccessful == false)
            {
                Output.IsCallSuccessful = false;
                Output.Message = Result.Message;
                Output.FailureRecordsCount = input.Rows.Count;
                return Task.FromResult<AddWorkspaceOutput>(Output).Result;
            }
            else
            {
                iWorkspaceDbServiceObj = new WorkspaceDbService();
                Output.FailureRecordsCount = input.Rows.Count - WorkspaceInput.WorkspaceList.Count;
                foreach (CoreLib.Workspace obj in WorkspaceInput.WorkspaceList)
                {
                    DataTable dt = await iWorkspaceDbServiceObj.AddWorkspace(obj);
                    if (dt == null || dt.Rows.Count <= 0)
                    {
                        Output.FailureRecordsCount++;
                    }
                    else
                    {
                        Output.SuccessRecordsCount++;
                    }
                }
                if (Output.FailureRecordsCount > 0)
                    Output.IsCallSuccessful = false;
            }
            return Task.FromResult<AddWorkspaceOutput>(Output).Result;
        }
        public async Task<AddWorkspaceOutput> UpdateWorkspace(AddWorkspaceInput input)
        {
            iWorkspaceDbServiceObj = new WorkspaceDbService();
            DataTable dt = await iWorkspaceDbServiceObj.UpdateWorkspace(input.Workspace);
            AddWorkspaceOutput Output = new AddWorkspaceOutput();
            List<CoreLib.Workspace> List = new List<CoreLib.Workspace>();
            Mapper mapper = new Mapper();
            mapper.Map(dt, List);
            Output.Workspace = List?.FirstOrDefault();
            return Output;
        }
        public OutputBaseClass  ValidateWorkspaceData(DataTable dt, AddWorkspaceInput WorkspaceInput)
        {
            Organization OrganizationObj= System.Web.HttpContext.Current.Session["Organization"] as Organization;
            OutputBaseClass Output = new OutputBaseClass();
            Output.IsCallSuccessful = true;
            string[] formats = {"M/d/yyyy h:mm:ss tt", "M/d/yyyy h:mm tt",
                   "MM/dd/yyyy hh:mm:ss", "M/d/yyyy h:mm:ss",
                   "M/d/yyyy hh:mm tt", "M/d/yyyy hh tt",
                   "M/d/yyyy h:mm", "M/d/yyyy h:mm",
                   "MM/dd/yyyy hh:mm", "M/dd/yyyy hh:mm"};
            DateTime LastUpdated;
            DateTime LastRenovated;
            string Floor = "0";
            try
            {
                DataColumnCollection columns = dt.Columns;
                if (columns.Contains("Building") == false)
                {
                    Output.IsCallSuccessful = false;
                    Output.Message = "Invalid Data format, Building name not found";
                    return Output;
                }
                if (columns.Contains("Name") == false)
                {
                    Output.IsCallSuccessful = false;
                    Output.Message = "Invalid Data format, Name not found";
                    return Output;
                }
                if (columns.Contains("City") == false)
                {
                    Output.IsCallSuccessful = false;
                    Output.Message = "Invalid Data format, City not found";
                    return Output;
                }
                if (columns.Contains("State") == false)
                {
                    Output.IsCallSuccessful = false;
                    Output.Message = "Invalid Data format, Stat not found";
                    return Output;
                }
                if (columns.Contains("PostalCode") == false)
                {
                    Output.IsCallSuccessful = false;
                    Output.Message = "Invalid Data format, PostalCode not found";
                    return Output;
                }

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        CoreLib.Workspace Workspace = new CoreLib.Workspace();
                        //Workspace.Id = OrganizationObj.OrgId;
                        Workspace.OrgId = OrganizationObj.OrgId;
                        Workspace.Name = Convert.ToString(row["Name"]);
                        Workspace.Description = Convert.ToString(row["Description"]);
                        //Workspace.FriendlyName = Convert.ToString(row["FriendlyName"]);
                        //Workspace.IdmResourceIdentity = Convert.ToString(row["IdmResourceIdentity"]);
                        Workspace.Building = new CoreLib.Building
                        {
                            // Id = Convert.ToInt64(row["BuildingId"]),
                            BuildingName = Convert.ToString(row["Building"]),
                        };
                        if (columns.Contains("Floor") != false)
                        {
                            Floor = Convert.ToString(row["Floor"]);
                        }
                        //Workspace.Floor = new CoreLib.Floor { Id = Convert.ToInt64(row["FloorId"]) };
                        Workspace.Address = new CoreLib.Address
                        {
                            Floor = Floor,
                            Country = Convert.ToString(row["Country"]),
                            State = Convert.ToString(row["State"]),
                            City = Convert.ToString(row["City"]),
                            PostalCode = Convert.ToString(row["PostalCode"]),
                            GpsLocation = new CoreLib.GpsLocation
                            {
                                //Location = Convert.ToString(row["LocationString"]),
                                Lat = Convert.ToDecimal(row["Lat"]),
                                Long = Convert.ToDecimal(row["Long"])
                            },

                        };
                        //Workspace.WebLink = new Uri(Utils.UriExtensions.Isvalid(Convert.ToString(row["WebLink"]), Resource.DefaultUrl));
                        //Workspace.Photos = new List<CoreLib.Photo> {
                        //    new CoreLib.Photo{
                        //         WebLink= new Uri(Utils.UriExtensions.Isvalid(Convert.ToString(row["PhotoLinks"]), Resource.DefaultUrl))
                        //    }
                        //};
                        //Workspace.Ratings = Convert.ToInt32(row["Ratings"]);
                        Workspace.Created = DateTime.Now;
                        if (DateTime.TryParseExact(Convert.ToString(row["LastUpdated"]), formats, new System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None,
                              out LastUpdated))
                            Workspace.LastUpdated = LastUpdated;
                        //Workspace.NextReservationStartTime = Convert.ToDateTime(row["NextReservationStartTime"]);
                        //Workspace.NextReservationEndTime = Convert.ToDateTime(row["NextreservationEndTime"]);
                        //Workspace.LimitedAccessRoles = new List<CoreLib.RoleType> { (CoreLib.RoleType)Enum.Parse(typeof(CoreLib.RoleType), (Convert.ToString(row["LimitedAccessRoles"]))) };
                        //Workspace.LimitedAccessUsers = new List<User>{ new User
                        //   {
                        //    Id = Convert.ToInt64(row["LimitedAccessUsers"])
                        //   }
                        //};
                        //Workspace.Tags = new Dictionary<string, string> { { Convert.ToString(row["Tags"]), Convert.ToString(row["Tags"]) } };
                        //Workspace.Maps = new List<Uri> { new Uri(Utils.UriExtensions.Isvalid(Convert.ToString(row["MapsUri"]), Resource.DefaultUrl)) };

                        Workspace.WorkspaceType = (CoreLib.WorkspaceType)Enum.Parse(typeof(CoreLib.WorkspaceType), (Convert.ToString(row["WorkspaceType"])));
                        //Workspace.OccupancyStatus = (CoreLib.WorkspaceStatusType)Enum.Parse(typeof(CoreLib.WorkspaceStatusType), (Convert.ToString(row["OccupancyStatus"])));
                        Workspace.Capacity = Convert.ToInt32(row["Capacity"]);
                        if (DateTime.TryParseExact(Convert.ToString(row["LastRenovated"]), formats, new System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None,
                            out LastRenovated))
                            Workspace.LastRenovated = LastRenovated;
                        Workspace.CommunicationInfo = new CoreLib.CommunicationInfo
                        {
                            PhoneNumber = Convert.ToString(row["CommunicationInfo_PhoneNumber"]),
                            TelephonyDetails = Convert.ToString(row["CommunicationInfo_TelephonyDetails"]),
                            //AccessDescription = Convert.ToString(row["CommunicationInfo_AccessDescription"]),
                            OtherInfo = new Dictionary<string, string> { { Convert.ToString(row["CommunicationInfo_Email"]),
                            Convert.ToString(row["CommunicationInfo_Email"]) } }
                        };
                        //Workspace.IsAutoCheckinEnabled = Convert.ToBoolean(row["IsAutoCheckinEnabled"]);
                        //Workspace.IsAutoCheckoutEnabled = Convert.ToBoolean(row["IsAutoCheckoutEnabled"]);
                        //Workspace.AccessType = (CoreLib.WorkspaceAccessType)Enum.Parse(typeof(CoreLib.WorkspaceAccessType), (Convert.ToString(row["AccessType"])));
                        //Workspace.MaintenanceOwner = new CoreLib.User { Id = Convert.ToInt64(row["MaintenanceOwner"]) };
                        Workspace.AreaInSqFt = Convert.ToDecimal(row["AreaInSqFt"]);
                        //Workspace.DistanceFromFloorOrigin = new Point();
                        Workspace.HasControllableDoor = Convert.ToBoolean(row["HasControllableDoor"]);
                        //Workspace.BeaconUid = Convert.ToString(row["BeaconUid"]);
                        Workspace.IsBlocked = Convert.ToBoolean(row["IsBlocked"]);
                        Workspace.IsHidden = Convert.ToBoolean(row["IsHidden"]);
                        //Workspace.ObjectId = Convert.ToString(row["ObjectId"]);
                        WorkspaceInput.WorkspaceList.Add(Workspace);
                    }
                }
            }
            catch(Exception ex)
            {

            }
            return Output;
        }
        public Task<BlockWorkspaceOutput> BlockWorkspace(BlockWorkspaceInput input)
        {
            return null;
        }

       public Task<bool> RemoveWorkspace(CoreLib.Workspace input)
        {
            return null;
        }

       public Task<bool> HideWorkspace(CoreLib.Workspace input)
        {
            return null;
        }

        #endregion

        
      
        public Task<IEnumerable<WorkspaceType>> GetAvailableWorkspaceTypes(string orgId, User u, GpsLocation location)
        {
            return null;
        }
        public async Task<WorkspaceBuildingOuputViewModel> GetAvailableWorkspaces(BuildingWorkspaceRequestModel Input)
        {
            iWorkspaceDbServiceObj = new WorkspaceDbService();
            var Result = await iWorkspaceDbServiceObj.GetAvailableWorkspaces(Input);
            WorkspaceBuildingOuputViewModel Output = new WorkspaceBuildingOuputViewModel();
            Output.Count = Result.Rows.Count.ToString();
            Output.value = new List<BuildingWorkspacesViewModel>();
            Mapper MapperObj = new Mapper();
            MapperObj.Map(Result, Output.value);
            return Output;
        }
        public Task<List<CoreLib.Workspace>> GetWorkspacesByBuilding(long OrgId,string TenantId,long BuildingId)
        {
            iWorkspaceDbServiceObj = new WorkspaceDbService();
            DataTable dt = new DataTable();
            iWorkspaceDbServiceObj.GetWorkspaces(dt,OrgId, TenantId, BuildingId);
            List<CoreLib.Workspace> Output = new List<CoreLib.Workspace>();
            Mapper mapper = new Mapper();
            mapper.Map(dt, Output);
            return Task.FromResult<List<CoreLib.Workspace>>(Output);
        }
        public Task<WorkspaceOutput> GetWorkspacesByBuilding(WorkspaceInput Input)
        {
            iWorkspaceDbServiceObj = new WorkspaceDbService();
            DataTable dt = new DataTable();
            iWorkspaceDbServiceObj.GetWorkspaces(dt, Input.Workspace.OrgId,null, Input.Workspace.Building.Id);
            WorkspaceOutput Output = new WorkspaceOutput();
            Mapper mapper = new Mapper();
            mapper.Map(dt, Output.WorkspaceList);
            return Task.FromResult<WorkspaceOutput>(Output);
        }

        public Task<WorkspaceOutput> GetWorkspacesById(WorkspaceInput Input)
        {
            iWorkspaceDbServiceObj = new WorkspaceDbService();
            DataTable dt = iWorkspaceDbServiceObj.GetWorkspaces(Input.Workspace.OrgId, Input.Workspace.Id);
            WorkspaceOutput Output = new WorkspaceOutput();
            Output.WorkspaceList = new List<CoreLib.Workspace>();
            Mapper mapper = new Mapper();
            mapper.Map(dt,Output.WorkspaceList);
            return Task.FromResult<WorkspaceOutput>(Output);
        }
        public Task<WorkspaceOutput> GetWorkspacesList(long OrgId, int StartIndex, int PageSize, string Sorting, string FilterbyName)
        {
            iWorkspaceDbServiceObj = new WorkspaceDbService();
            DataTable dt = new DataTable();
            String[] elements = Sorting.Split(new char[] { ' ' });
            if(StartIndex>1)
            StartIndex = StartIndex / 10;
           dt= iWorkspaceDbServiceObj.GetWorkspaces(OrgId, StartIndex, PageSize, elements[0], elements[1], FilterbyName);
            WorkspaceOutput Output = new WorkspaceOutput();
            Output.WorkspaceList = new List<CoreLib.Workspace>();
            if (dt.Rows.Count > 0)
            {
                Mapper mapper = new Mapper();
                mapper.Map(dt, Output.WorkspaceList);
                Output.Count = Convert.ToInt64(dt.Rows[0]["MaxRows"]);
            }
            else
                Output.Count = 0;
            return Task.FromResult<WorkspaceOutput>(Output);
        }
        public Task<IEnumerable<CoreLib.Workspace>> FindWorkspaces(FindWorkspacesInput input)
        {
            return null;
        }

        public async Task<OutputBaseClass> RateWorkspace(WorkspaceReviewInput input)
        {
            iWorkspaceDbServiceObj = new WorkspaceDbService();
            OutputBaseClass Output = new OutputBaseClass();
            int Result=await iWorkspaceDbServiceObj.RateWorkspace(input);

            // if (Result>0)
            //{
                Output.IsCallSuccessful = true;
                Output.Message = Resource.WorkspaceRatingSuccess;
            //}
            //else
            //{
            //    Output.IsCallSuccessful = false;
            //    Output.Message = Resource.WorkspaceRatingException;
            //}
            return Output;
        }

        public Task<GetReviewsOutput> GetReviews(GetReviewsInput input)
        {
            return null;
        }

        public Task<IEnumerable<Announcement>> GetAnnouncements(GetAnnouncementsInput input)
        {
            return null;
        }
        #region Reservations
       public Task<IEnumerable<Event>> GetUpcomingReservations(GetUpcomingReservationsInput input)
        {
            return null;
        }
       public Task<WorkspaceStatusInfo> GetWorkspaceStatus(GetWorkspaceStatusInput input)
        {
            return null;
        }

        
        #endregion
    }
}
