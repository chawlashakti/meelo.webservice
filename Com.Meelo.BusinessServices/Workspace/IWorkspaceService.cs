﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.Meelo.CoreLib;
using Com.Meelo.CoreLib.ViewModel;
using System.Data;

namespace Com.Meelo.BusinessServices.Workspace
{
   public interface IWorkspaceService
    {

        #region CRUD
        Task<AddWorkspaceOutput> AddWorkspace(AddWorkspaceInput input);
        Task<AddWorkspaceOutput> AddWorkspace(DataTable input);
        Task<AddWorkspaceOutput> AddWorkspaces(AddWorkspaceInput input);
        Task<AddWorkspaceOutput> UpdateWorkspace(AddWorkspaceInput input);
        Task<BlockWorkspaceOutput> BlockWorkspace(BlockWorkspaceInput input);

        Task<bool> RemoveWorkspace(CoreLib.Workspace input);

        Task<bool> HideWorkspace(CoreLib.Workspace input);

        #endregion

        #region Search
        Task<IEnumerable<WorkspaceType>> GetAvailableWorkspaceTypes(string orgId, User u, GpsLocation location);
        Task<WorkspaceBuildingOuputViewModel> GetAvailableWorkspaces(BuildingWorkspaceRequestModel Input);

        Task<WorkspaceOutput> GetWorkspacesById(WorkspaceInput Input);
        Task<WorkspaceOutput> GetWorkspacesByBuilding(WorkspaceInput Input);
        Task<List<CoreLib.Workspace>> GetWorkspacesByBuilding(long OrgId,string TenantId,long BuildingId);
        Task<IEnumerable<CoreLib.Workspace>> FindWorkspaces(FindWorkspacesInput input);
        Task<WorkspaceOutput> GetWorkspacesList(long OrgId, int StartIndex, int PageSize, string Sorting, string FilterbyName);
        #endregion

        #region Reservations
        Task<IEnumerable<Event>> GetUpcomingReservations(GetUpcomingReservationsInput input);


        Task<WorkspaceStatusInfo> GetWorkspaceStatus(GetWorkspaceStatusInput input);
        #endregion
        #region Reviews
        Task<OutputBaseClass> RateWorkspace(WorkspaceReviewInput input);


        Task<GetReviewsOutput> GetReviews(GetReviewsInput input);
        #endregion

        OutputBaseClass ValidateWorkspaceData(DataTable dt, AddWorkspaceInput WorkspaceInput);
    }
}
