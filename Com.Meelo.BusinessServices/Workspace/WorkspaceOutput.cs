﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.BusinessServices.Workspace
{
   public class WorkspaceOutput:OutputBaseClass
    {
        public CoreLib.Workspace Workspace { get; set; }
        public List<CoreLib.Workspace> WorkspaceList { get; set; }
        public long Count { get; set; }
    }
}
