﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.BusinessServices.Workspace
{
   public class WorkspaceInput:InputBaseClass
    {
        public CoreLib.Workspace Workspace { get; set; }
    }
}
