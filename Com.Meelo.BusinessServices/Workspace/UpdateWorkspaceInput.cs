﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.Meelo.CoreLib;
namespace Com.Meelo.BusinessServices.Workspace
{
   public class UpdateWorkspaceInput : InputBaseClass
    {
        public Com.Meelo.CoreLib.Workspace OriginalWorkspace { get; set; }

        public Com.Meelo.CoreLib.Workspace UpdatedWorkspace { get; set; }
    }
}
