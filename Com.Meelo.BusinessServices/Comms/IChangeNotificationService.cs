﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.BusinessServices.Comms
{
    public interface IChangeNotificationService
    {
        void TrackResource(long resourceId);
        bool IsTrackedForChanges(long resourceId);
    }
}
