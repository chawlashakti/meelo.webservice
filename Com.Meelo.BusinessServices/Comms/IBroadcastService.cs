﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.BusinessServices.Comms
{
    public interface IBroadcastService
    {
        void BroadcastUpdate(string resourceAddress);
    }
}
