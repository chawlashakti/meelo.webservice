﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.BusinessServices
{
   public class OutputBaseClass
    {
        public bool IsCallSuccessful { get; set; }

        public string Message { get; set; }
    }
}
