﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.BusinessServices.UserProfile
{
   public class GetUserSettingsOutput : OutputBaseClass
    {
        public Com.Meelo.CoreLib.User user { get; set; }
    }
}
