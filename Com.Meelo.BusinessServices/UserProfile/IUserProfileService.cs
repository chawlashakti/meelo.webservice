﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.BusinessServices.UserProfile
{
   public interface IUserProfileService
    {
        Task<long> AddUser(List<CoreLib.User> input);
        Task<CoreLib.User> GetUser(string ObjectId);
        Task<List<CoreLib.User>> GetUsers(string TenantId);
    }
}
