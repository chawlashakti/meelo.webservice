﻿using Com.Meelo.CoreLib;
using Com.Meelo.CoreLib.ViewModel;
using Com.Meelo.DataServices;
using Com.Meelo.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.BusinessServices.Building
{
   public class BuildingService:IBuildingService
    {
        private IBuildingDbService BuildingDBServiceObj;
        public Task<IEnumerable<BuildingViewModel>> GetBuildingsAndWorkspaceCount(string TenantId, GpsLocation location)
        {
            BuildingDBServiceObj = new BuildingDbService();
            var Output= BuildingDBServiceObj.GetBuildingsAndWorkspaceCount(TenantId, location).Result;
            List<BuildingViewModel> Building = new List<BuildingViewModel>();
            Mapper MapperObj = new Mapper();
            MapperObj.Map(Output,Building);
            return Task.FromResult<IEnumerable< BuildingViewModel >>(Building);
        }
       public Task<AddBuildingOutput> AddBuilding(AddBuildingInput input)
        {
            BuildingDBServiceObj = new BuildingDbService();
            var Output = BuildingDBServiceObj.AddBuilding(input.building).Result;
            AddBuildingOutput Bld = new AddBuildingOutput();
            Bld.building = new CoreLib.Building();
            Mapper MapperObj = new Mapper();
            MapperObj.Map(Output, Bld.building);
            return Task.FromResult<AddBuildingOutput>(Bld);
        }
        public Task<AddBuildingOutput> UpdateBuilding(AddBuildingInput input)
        {
            BuildingDBServiceObj = new BuildingDbService();
            var Output = BuildingDBServiceObj.UpdateBuilding(input.building).Result;
            AddBuildingOutput Bld = new AddBuildingOutput();
            Bld.building = new CoreLib.Building();
            Mapper MapperObj = new Mapper();
            MapperObj.Map(Output, Bld.building);
            return Task.FromResult<AddBuildingOutput>(Bld);
        }
        public void GetBuilding(long OrgId, long BuildingId)
        {
            throw new NotImplementedException();
        }

        public Task<BuildingOutput> GetBuilding(long OrgId, int StartIndex, int PageSize, string Sorting, string FilterbyBuilding)
        {
            BuildingDBServiceObj = new BuildingDbService();
            BuildingOutput BldOutput = new BuildingOutput();
            BldOutput.buildings = new List<CoreLib.Building>();
            String[] elements = Sorting.Split(new char[] { ' ' });
            DataTable dt = new DataTable();
          dt= BuildingDBServiceObj.GetBuilding(OrgId, StartIndex, PageSize, elements[0], elements[1], FilterbyBuilding).Result;
            if (dt.Rows.Count > 0)
            {
                Mapper MapperObj = new Mapper();
                MapperObj.Map(dt, BldOutput.buildings);
                BldOutput.Count = Convert.ToInt64(dt.Rows[0]["MaxRows"]);
            }
            else
                BldOutput.Count = 0;
            return Task.FromResult<BuildingOutput>(BldOutput);
        }
        public Task<BuildingOutput> GetBuilding(long OrgId, string FilterbyBuilding)
        {
            BuildingDBServiceObj = new BuildingDbService();
            BuildingOutput BldOutput = new BuildingOutput();
            BldOutput.buildings = new List<CoreLib.Building>();
            DataTable dt = new DataTable();
            dt = BuildingDBServiceObj.GetBuilding(OrgId, FilterbyBuilding).Result;
            Mapper MapperObj = new Mapper();
            MapperObj.Map(dt, BldOutput.buildings);
            BldOutput.Count = Convert.ToInt64(dt.Rows[0]["MaxRows"]);
            return Task.FromResult<BuildingOutput>(BldOutput);
        }
    }
}
