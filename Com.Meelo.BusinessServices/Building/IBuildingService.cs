﻿using Com.Meelo.CoreLib;
using Com.Meelo.CoreLib.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.BusinessServices.Building
{
   public interface IBuildingService
    {
        Task<IEnumerable<BuildingViewModel>> GetBuildingsAndWorkspaceCount(string TenantId, GpsLocation location);
        Task<AddBuildingOutput> AddBuilding(AddBuildingInput input);
        void GetBuilding(long OrgId, long BuildingId);
        Task<BuildingOutput> GetBuilding(long OrgId, int StartIndex, int PageSize, string Sorting, string Filter);
        Task<BuildingOutput> GetBuilding(long OrgId, string FilterbyBuilding);
        Task<AddBuildingOutput> UpdateBuilding(AddBuildingInput input);
    }
}
