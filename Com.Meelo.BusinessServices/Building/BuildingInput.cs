﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.BusinessServices.Building
{
   public class BuildingInput : InputBaseClass
    {
        public CoreLib.Building building { get; set; }
    }
}
