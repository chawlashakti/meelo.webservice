﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.Meelo.CoreLib;
namespace Com.Meelo.BusinessServices.Building
{
    public class AddBuildingInput:InputBaseClass
    {
        public CoreLib.Building building { get; set; }
    }
}
