﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.BusinessServices.Building
{
   public class AddBuildingOutput:OutputBaseClass
    {
        public CoreLib.Building building { get; set; }
    }
}
