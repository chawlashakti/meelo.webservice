﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.BusinessServices.Building
{
   public class BuildingOutput : InputBaseClass
    {
        public CoreLib.Building building { get; set; }
        public List<CoreLib.Building> buildings { get; set; }
        public long Count { get; set; }
    }
}
