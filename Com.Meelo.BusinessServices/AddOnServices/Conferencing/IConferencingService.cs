﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.Meelo.CoreLib;

namespace Com.Meelo.BusinessServices.AddOnServices.Conferencing
{
   public interface IConferencingService
    {
        /*
         * INCOMPLETE. FILL IT WHEN WE IMPLEMENT OUR FIRST CONFERENCING PROVIDER
         * 
         * e.g. https://developer.cisco.com/fileMedia/download/603328ba-f99c-4624-b1af-7302a9150433
         * 
         * */

        void CreateMeeting();

        void ListMeetings();

        void ListMeeting();

        void GetMeeting();

        void DeleteMeeting();

        void GetHostMeetingUrl();

        void GetJoinMeetingUrl();

        void StartMeeting(string roomAddress, string uniqueId, string securityKey);
        bool StartMeetingFromClient(string roomAddress, string uniqueId, string signature);
        void WarnMeeting(string roomAddress, string uniqueId, string securityKey, Func<string, string> buildStartUrl, Func<string, string> buildCancelUrl);
        void CancelMeeting(string roomAddress, string uniqueId, string securityKey);
        void EndMeeting(string roomAddress, string uniqueId, string securityKey);
        void StartNewMeeting(string roomAddress, string securityKey, string title, DateTime endTime);
        void MessageMeeting(string roomAddress, string uniqueId, string securityKey);
        bool CancelMeetingFromClient(string roomAddress, string uniqueId, string signature);
        void SecurityCheck(string roomAddress, string securityKey);
    }
}
