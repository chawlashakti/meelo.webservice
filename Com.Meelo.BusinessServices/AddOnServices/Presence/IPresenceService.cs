﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.BusinessServices.AddOnServices.Presence
{
   public interface IPresenceService
    {
        Task GetUserPresence();
    }
}
