﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.BusinessServices.AddOnServices.IM
{
    public enum InstantMessagePriority
    {
        Emergency,
        NonUrgent,
        Normal,
        Urgent,
    }
    public interface IInstantMessagingService
    {

        Task<bool> SendMessage(SendMessageInput input);

    }
}
