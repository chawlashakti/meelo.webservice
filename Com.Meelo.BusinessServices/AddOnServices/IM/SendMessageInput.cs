﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.BusinessServices.AddOnServices.IM
{
    public class SendMessageInput : InputBaseClass
    {
        public IEnumerable<string> TargetAddresses { get; set; }

        public string Subject { get; set; }

        public string Message { get; set; }

        public InstantMessagePriority Priority { get; set; }
    }
}
