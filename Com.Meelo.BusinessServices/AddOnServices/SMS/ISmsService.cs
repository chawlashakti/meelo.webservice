﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.BusinessServices.AddOnServices.SMS
{
    public interface ISmsService
    {
        Task<bool> SendMessage(SmsMessageInput input);

        Task<IEnumerable<string>> LookupPhoneAddressesFromEmail(LookupPhoneAddressesFromEmailInput input);
    }
}
