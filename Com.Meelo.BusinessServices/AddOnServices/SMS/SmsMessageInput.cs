﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.BusinessServices.AddOnServices.SMS
{
   public class SmsMessageInput : InputBaseClass
    {

        public IEnumerable<string> Numbers { get; set; }

        public string Message { get; set; }
    }
}
