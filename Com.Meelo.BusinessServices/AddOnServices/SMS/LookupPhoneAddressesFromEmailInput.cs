﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.BusinessServices.AddOnServices.SMS
{
   public class LookupPhoneAddressesFromEmailInput : InputBaseClass
    {
        public IEnumerable<string> EmailAddresses { get; set; }
    }
}
