﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.Meelo.CoreLib;

namespace Com.Meelo.BusinessServices.AddOnServices
{
    public class ServiceProviderService : IAddOnService
    {
        public Task<AddServiceOutput> AddService(AddServiceInput input)
        {
            throw new NotImplementedException();
        }

        public Task<GetAvailableServicesOutput> GetAvailableServices(GetAvailableServicesInput input)
        {
            throw new NotImplementedException();
        }

        public Task<GetRedirectUriOutput> GetRedirectUri(GetRedirectUriInput input)
        {
            throw new NotImplementedException();
        }

        public Task<GetServiceDetailsOutput> GetServiceDetails(GetServiceDetailsInput input)
        {
            throw new NotImplementedException();
        }

        public Task<GetServiceSettingsOutput> GetServiceSettings(GetServiceSettingsInput input)
        {
            throw new NotImplementedException();
        }

        public Task<RemoveServiceOutput> RemoveService(RemoveServiceInput input)
        {
            throw new NotImplementedException();
        }

        public Task<UpdateServiceSettingOutput> UpdateServiceSetting(UpdateServiceSettingInput input)
        {
            throw new NotImplementedException();
        }
    }
}
