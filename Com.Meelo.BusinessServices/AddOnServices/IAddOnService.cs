﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.BusinessServices.AddOnServices
{
    public interface IAddOnService
    {

        Task<GetRedirectUriOutput> GetRedirectUri(GetRedirectUriInput input);

        Task<GetServiceDetailsOutput> GetServiceDetails(GetServiceDetailsInput input);


        Task<GetAvailableServicesOutput> GetAvailableServices(GetAvailableServicesInput input);



        Task<AddServiceOutput> AddService(AddServiceInput input);


        Task<RemoveServiceOutput> RemoveService(RemoveServiceInput input);


        Task<GetServiceSettingsOutput> GetServiceSettings(GetServiceSettingsInput input);


        Task<UpdateServiceSettingOutput> UpdateServiceSetting(UpdateServiceSettingInput input);


    }
}
