﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.Meelo.CoreLib;

namespace Com.Meelo.BusinessServices.AddOnServices
{
   public class AddServiceInput : InputBaseClass
    {
        public string MeetingSpaceId { get; set; }

        public IService Service { get; set; }
    }
}
