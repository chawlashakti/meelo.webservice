﻿using Com.Meelo.CoreLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.BusinessServices.Reservation
{
    public interface IReservationService
    {
        Task<ReservationOutput> Reserve(Event Input);
        Task<CancelReservationOutput> CancelReservation(Event Input);
        Task<CancelReservationOutput> CancelReservation();
        Task<CheckinOutput> Checkin(CheckinInput input);
        Task<CheckoutOutput> Checkout(CheckoutInput input);
        Task<GetReservationsOutput> GetReservations(GetReservationsInput input);
        Task<GetReservationsOutput> GetReservationCount(GetReservationsInput input);
    }
}
