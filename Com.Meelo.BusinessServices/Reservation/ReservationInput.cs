﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.Meelo.CoreLib;

namespace Com.Meelo.BusinessServices.Reservation
{
    public class ReservationInput : InputBaseClass
    {
        public Event Reservation { get; set; }

        public GpsLocation ReservedFrom { get; set; }

        public string WorkspaceId { get; set; }

        public DateTimeOffset StartTime { get; set; }

        public DateTimeOffset EndTime { get; set; }
    }
}
