﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.Meelo.CoreLib;
using Com.Meelo.DataServices;
using Com.Meelo.Utils;

namespace Com.Meelo.BusinessServices.Reservation
{
    public class ReservationService : IReservationService
    {
        IReservationDbService iReservationDbService;
        public async Task<ReservationOutput> Reserve(Event Input)
        {
            ReservationOutput Output = new ReservationOutput();
            iReservationDbService = new ReservationDbService();
            int Result = await iReservationDbService.Reserve(Input);
            if (Result ==1)
            {
                Output.IsCallSuccessful = true;
                Output.Message = Resource.ReservationSuccess;

            }
            else if (Result == 2)
            {
                Output.IsCallSuccessful = false;
                Output.Message = Resource.ReservationConflictException;

            }
            else if (Result == 3)
            {
                Output.IsCallSuccessful = false;
                Output.Message = Resource.ReservationBookedConflictException;

            }
            else
            {
                Output.IsCallSuccessful = false;
                Output.Message = Resource.ReservationException;
            }
            return Output;
        }
       public async Task<CancelReservationOutput> CancelReservation(Event Input)
        {
            CancelReservationOutput Output = new CancelReservationOutput();
            iReservationDbService = new ReservationDbService();
            int Result = await iReservationDbService.CancelReservation(Input);
            if(Result>0)
            {
                Output.IsCallSuccessful = true;
                Output.Message = Resource.CancelReservationSuccess;

            }
            else
            {
                Output.IsCallSuccessful = false;
                Output.Message = Resource.CancelReservationException;
            }
            return Output;
        }
        public async Task<CancelReservationOutput> CancelReservation()
        {
            CancelReservationOutput Output = new CancelReservationOutput();
            iReservationDbService = new ReservationDbService();
            int Result = await iReservationDbService.CancelReservation();
            if (Result > 0)
            {
                Output.IsCallSuccessful = true;
                Output.Message =Result+" " +Resource.CancelReservationSuccess;

            }
            else
            {
                Output.IsCallSuccessful = false;
                 Output.Message = Resource.CancelReservationException;
            }
            return Output;
        }
        public Task<CancelReservationOutput> CancelReservation(CancelReservationInput input)
        {
            return null;
        }

        public async Task<CheckinOutput> Checkin(CheckinInput input)
        {
            CheckinOutput Output = new CheckinOutput();
            iReservationDbService = new ReservationDbService();
            int Result = await iReservationDbService.CheckIn(input.Event);
            if (Result > 0)
            {
                Output.IsCallSuccessful = true;
                Output.Message = Resource.CheckInSuccess;

            }
            else
            {
                Output.IsCallSuccessful = false;
                Output.Message = Resource.CheckInException;
            }
            return Output;
        }

        public async Task<CheckoutOutput> Checkout(CheckoutInput input)
        {
            CheckoutOutput Output = new CheckoutOutput();
            iReservationDbService = new ReservationDbService();
            int Result = await iReservationDbService.CheckOut(input.Event);
            if (Result > 0)
            {
                Output.IsCallSuccessful = true;
                Output.Message = Resource.CheckOutSuccess;

            }
            else
            {
                Output.IsCallSuccessful = false;
                Output.Message = Resource.CheckOutException;
            }
            return Output;
        }

        public Task<GetReservationHistoryOutput> GetReservationHistory(GetReservationHistoryInput input)
        {
            return null;
        }

        public async Task<GetReservationsOutput> GetReservations(GetReservationsInput input)
        {
            iReservationDbService = new ReservationDbService();
            var EventObj = await iReservationDbService.GetReservations(input.StartTime,input.EndTime,input.UserIdFilter,input.TenantId);
            List<Event> EventList = new List<Event>();
            Mapper mapper = new Mapper();
            mapper.Map(EventObj, EventList);
            GetReservationsOutput Output = new GetReservationsOutput();
            Output.Reservations = EventList;

                Output.IsCallSuccessful = false;
                Output.Message = Resource.GetReservationSuccess;

            return Output;
        }
        public async Task<GetReservationsOutput> GetReservationCount(GetReservationsInput input)
        {
            iReservationDbService = new ReservationDbService();
            var Obj = await iReservationDbService.GetReservationCount(input.StartTime, input.EndTime, input.UserIdFilter);
            GetReservationsOutput Output = new GetReservationsOutput();
            Output.ReservationCount = Obj;

            Output.IsCallSuccessful = false;
            Output.Message = Resource.GetReservationSuccess;

            return Output;
        }


        public Task<InviteAttendeesOutput> InviteAttendees(InviteAttendeesInput input)
        {
            return null;
        }

        public Task<RemoveAttendeesOutput> RemoveAttendees(RemoveAttendeesInput input)
        {
            return null;
        }

        public Task<GetAttendeesOutput> GetAttendees(GetAttendeesInput input)
        {
            return null;
        }
    }
}
