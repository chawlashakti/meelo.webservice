﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.Meelo.CoreLib;

namespace Com.Meelo.BusinessServices.Reservation
{
    public class GetReservationsInput : InputBaseClass
    {
        public string ReservationIdFilter { get; set; }

        public DateTimeOffset? StartTime { get; set; }

        public DateTimeOffset? EndTime { get; set; }

        public string UserIdFilter { get; set; }

        public string WorkspaceIdFilter { get; set; }
        public string TenantId { get; set; }
    }
}
