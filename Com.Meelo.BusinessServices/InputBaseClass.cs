﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.Meelo.CoreLib;

namespace Com.Meelo.BusinessServices
{
    public class InputBaseClass
    {
        public User u { get; set; }

        public GpsLocation RequestFrom { get; set; }

        public long OrgId { get; set; }
    }
}
