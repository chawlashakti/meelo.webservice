﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.BusinessServices.Tenant
{
    public class AddOrganizationOutput : OutputBaseClass
    {
        public Com.Meelo.CoreLib.Organization Organization { get; set; }

    }
}
