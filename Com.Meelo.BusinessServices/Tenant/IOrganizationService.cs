﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.BusinessServices.Tenant
{
   public interface IOrganizationService
    {
        Task<AddOrganizationOutput> AddOrganization(AddOrganizationInput input);
        Task<OrganizationOutput> GetOrganization(OrganizationInput input);
        Task<AddOrganizationOutput> UpdateOrganization(AddOrganizationInput input);
    }
}
