﻿using Com.Meelo.DataServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.Meelo.CoreLib;
using Com.Meelo.Utils;
namespace Com.Meelo.BusinessServices.Tenant
{
    public class OrganizationService : IOrganizationService
    {
        private IOrganizationDbService iOrganizationDbService;
        public async Task<AddOrganizationOutput> AddOrganization(AddOrganizationInput input)
        {
            iOrganizationDbService = new OrganizationDbService();
            if (input.Organization.MeeloLicense == null)
                input.Organization.MeeloLicense = new CoreLib.MeeloLicense();
            if (input.Organization.OrgContact == null)
                input.Organization.OrgContact = new CoreLib.User();
            if (input.Organization.FriendlyName == null)
                input.Organization.FriendlyName = input.Organization.CorporateName;
            var OrganizationObj = await iOrganizationDbService.AddOrganization(input.Organization);
            Organization Org = new Organization();
            Utils.Mapper mapper = new Mapper();
            mapper.Map(OrganizationObj, Org);
            iOrganizationDbService.CloseConnection();
            AddOrganizationOutput Output = new AddOrganizationOutput();
            Output.Organization = input.Organization;
            Output.Organization.OrgId = Org.OrgId;
            return Output;
        }
        public async Task<AddOrganizationOutput> UpdateOrganization(AddOrganizationInput input)
        {
            iOrganizationDbService = new OrganizationDbService();
            if (input.Organization.MeeloLicense == null)
                input.Organization.MeeloLicense = new CoreLib.MeeloLicense();
            if (input.Organization.OrgContact == null)
                input.Organization.OrgContact = new CoreLib.User();
            if (input.Organization.FriendlyName == null)
                input.Organization.FriendlyName = input.Organization.CorporateName;
            var OrganizationObj = await iOrganizationDbService.UpdateOrganization(input.Organization);
            Organization Org = new Organization();
            Utils.Mapper mapper = new Mapper();
            mapper.Map(OrganizationObj, Org);
            iOrganizationDbService.CloseConnection();
            AddOrganizationOutput Output = new AddOrganizationOutput();
            Output.Organization = input.Organization;
            Output.Organization.OrgId = Org.OrgId;
            return Output;
        }
        public async Task<OrganizationOutput> GetOrganization(OrganizationInput input)
        {
            iOrganizationDbService = new OrganizationDbService();
            System.Data.SqlClient.SqlDataReader DataReader=null;
            if(input.Organization.OrgId>0)
                DataReader = await iOrganizationDbService.GetOrganization(input.Organization.TenantId);
            else
                DataReader = await iOrganizationDbService.GetOrganization(input.Organization.TenantId);
            OrganizationOutput OrgOutput = new OrganizationOutput();
            OrgOutput.Organization = input.Organization;
            Utils.Mapper mapper = new Mapper();
            mapper.Map(DataReader, OrgOutput.Organization);
            iOrganizationDbService.CloseConnection();
            return OrgOutput;
        }
    }
}
