﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tectil.NCommand.Contract;
using Tectil.NCommand;

namespace SearchSetup
{
    /// <summary>
    /// https://github.com/tectil/NCommand
    /// Setup AzureSearch index and data
    /// </summary>
    class Program
    {
        [Command(description: "Create index.")]
        public bool Create(
            [Argument(description: "Name of the index.")] string indexname,
            [Argument(defaultValue: false)] bool overwrite
        )
        {
            // Create the search index
            return true;
        }

        [Command(description: "Delete index.")]
        public bool Delete(
           [Argument(description: "Name of the index.")] string indexname
       )
        {
            // Delete the search index
            return true;
        }

        [Command(description: "Upload documents.")]
        public bool Upload(
          [Argument(description: "Path to the file on local machine.", defaultValue:null)] string path,
          [Argument(description: "Uri to the file.", defaultValue:null)] string uri,
          [Argument(description: "File type (csv or json).", defaultValue:"csv")] string filetype

      )
        {
            // Upload data to the index
            return true;
        }

        [Command(description: "Search documents.")]
        public bool Search(
        [Argument(description: "Query string.", defaultValue: null)] string query

    )
        {
            //Search the index
            return true;
        }
        static void Main(string[] args)
        {
            NCommands commands = new NCommands();
            commands.RunConsole(args);
        }
    }
}
