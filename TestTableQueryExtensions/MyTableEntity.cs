﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage.Table;

namespace TestTableQueryExtensions
{
    public class MyTableEntity : TableEntity
    {
        public MyTableEntity() { }

        public MyTableEntity(string partitionKey, string rowKey)
        {
            this.PartitionKey = partitionKey;
            this.RowKey = rowKey;
        }

        public string MyProperty { get; set; }
    }
}
