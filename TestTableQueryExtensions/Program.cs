﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.WindowsAzure.Storage.Table.Queryable;
using Com.Meelo.Utils.Azure;
using Com.Meelo.Utils.Azure.Data.Wrappers;
using System.Collections.Generic;
using Tectil.NCommand.Contract;
using Tectil.NCommand;

namespace TestTableQueryExtensions
{
    class Program
    {
        public static string TABLE_NAME = "mystoragetable";

        public static string CONNECTION_STRING = null;

        ITableStorage storage = null;

        public Program()
        {
            CONNECTION_STRING = CloudConfigurationManager.GetSetting("StorageConnectionString");
        }

        static void Main(string[] args)
        {
            Program p = new Program();
            try
            {
               
                p.Init(TABLE_NAME);
                p.InsertBatch(TABLE_NAME).Wait();
                p.ExecuteSimpleTableQuery(CancellationToken.None).Wait();
            }catch(Exception ex)
            {
                Console.WriteLine(ex);
            }finally
            {
                p.Dispose();
            }
            Console.ReadLine();
        }

        private async Task ExecuteSimpleTableQuery(CancellationToken ct)
        {
            var table = await GetTable(ct);

            var query = table.CreateQuery<MyTableEntity>()
                .Where(r => !r.MyProperty.Equals(string.Empty))
                .Take(1200)
                .AsTableQuery();

            var results = await query.ExecuteAsync(ct);

            Console.WriteLine($"Results {(results.Any() ? results.Count().ToString() : "not ")}found.");
        }

        private static async Task<CloudTable> GetTable(CancellationToken ct)
        {
            //Retrieve the storage account from the connection string.
            var storageAccount = CloudStorageAccount.Parse(CONNECTION_STRING);

            //Create the table client.
            var tableClient = storageAccount.CreateCloudTableClient();

            //Retrieve a reference to the table.
            var table = tableClient.GetTableReference(TABLE_NAME);

            //Create the table if it doesn't exist.
            await table.CreateIfNotExistsAsync(ct);

            return table;
        }

        [Command(description: "Insert data", Name = "insert data")]

        public async Task InsertBatch(
         [Argument(description: "table name", DefaultValue ="mystoragetable")] string tableName,
                  [Argument(description: "Record count", DefaultValue = 100)] int count=100



            )
        {

            var random = new Random();
            // var count = random.Next(1, 25);
           // var count = 100;
            var partition = Guid.NewGuid().ToString();
            var entities = new List<MyTableEntity>(count);
            for (var i = 0; i < count; i++)
            {
                
                var entity = new MyTableEntity()
                {
                    PartitionKey = partition,
                    RowKey = Guid.NewGuid().ToString(),
                    MyProperty = Com.Meelo.Utils.RandomKeyGenerator.GenerateRandomString(Com.Meelo.Utils.RandomStringGenerationType.SIMPLE_STRING)
                };
                entities.Add(entity);
            }
            await storage.Insert(entities);

            var returned = await storage.QueryByPartition<MyTableEntity>(partition);
            //Assert.IsNotNull(returned);
           Console.WriteLine(string.Format("Returned Count:{0}", returned.Count()));
        }

        [Command(description: "Create table", Name = "createtable")]
        public void Init(
             [Argument(description: "table name", DefaultValue = "mystoragetable")] string tableName
            )
        {
            TABLE_NAME = tableName;
            this.storage = new TableStorage(TABLE_NAME, CONNECTION_STRING);
            storage.CreateIfNotExists().Wait();
        }


        [Command(description: "Delete table", Name = "deletetable")]
        public void Dispose()
        {
            try
            {
                storage.Delete().Wait();
            }catch(Exception ex)
            {
                Console.WriteLine("Error deleting table." + ex.Message);
            }
        }
    }
}
