﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.Meelo.Conferencing.WebEx.Generic;

namespace Com.Meelo.Conferencing.WebEx
{
    public class SetRecordingInfoInput
    {
        public recording Recording { get; set; }

        public bool? IsServiceRecording { get; set; }

        public basic Basic { get; set; }

        public playback Playback { get; set; }

        public fileAccess FileAccess { get; set; }
    }
}
