﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.Conferencing.WebEx
{
   public class WebExMeeting
    {
        public string MeetingKey { get; set; }

        public string GuestToken { get; set; }
    }
}
