﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.Conferencing.WebEx.Meeting
{



    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service", IsNullable = false)]
    public partial class message
    {

        private messageHeader headerField;

        private messageBody bodyField;

        /// <remarks/>
        public messageHeader header
        {
            get
            {
                return this.headerField;
            }
            set
            {
                this.headerField = value;
            }
        }

        /// <remarks/>
        public messageBody body
        {
            get
            {
                return this.bodyField;
            }
            set
            {
                this.bodyField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service")]
    public partial class messageHeader
    {

        private messageHeaderResponse responseField;

        /// <remarks/>
        public messageHeaderResponse response
        {
            get
            {
                return this.responseField;
            }
            set
            {
                this.responseField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service")]
    public partial class messageHeaderResponse
    {

        private string resultField;

        private string reasonField;

        private string gsbStatusField;

        private uint exceptionIDField;

        private messageHeaderResponseSubErrors subErrorsField;

        /// <remarks/>
        public string result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        public string reason
        {
            get
            {
                return this.reasonField;
            }
            set
            {
                this.reasonField = value;
            }
        }

        /// <remarks/>
        public string gsbStatus
        {
            get
            {
                return this.gsbStatusField;
            }
            set
            {
                this.gsbStatusField = value;
            }
        }

        /// <remarks/>
        public uint exceptionID
        {
            get
            {
                return this.exceptionIDField;
            }
            set
            {
                this.exceptionIDField = value;
            }
        }

        /// <remarks/>
        public messageHeaderResponseSubErrors subErrors
        {
            get
            {
                return this.subErrorsField;
            }
            set
            {
                this.subErrorsField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service")]
    public partial class messageHeaderResponseSubErrors
    {

        private messageHeaderResponseSubErrorsSubError subErrorField;

        /// <remarks/>
        public messageHeaderResponseSubErrorsSubError subError
        {
            get
            {
                return this.subErrorField;
            }
            set
            {
                this.subErrorField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service")]
    public partial class messageHeaderResponseSubErrorsSubError
    {

        private string exceptionIDField;

        private string reasonField;

        private string valueField;

        /// <remarks/>
        public string exceptionID
        {
            get
            {
                return this.exceptionIDField;
            }
            set
            {
                this.exceptionIDField = value;
            }
        }

        /// <remarks/>
        public string reason
        {
            get
            {
                return this.reasonField;
            }
            set
            {
                this.reasonField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }


    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service")]
    public partial class messageBody
    {

        private messageBodyBodyContent bodyContentField;

        /// <remarks/>
        public messageBodyBodyContent bodyContent
        {
            get
            {
                return this.bodyContentField;
            }
            set
            {
                this.bodyContentField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service")]
    public partial class messageBodyBodyContent
    {

        private accessControl accessControlField;

        private metaData metaDataField;

        private participants participantsField;

        private enableOptions enableOptionsField;

        private schedule scheduleField;

        private telephony telephonyField;

        private tracking trackingField;

        private repeat repeatField;

        private remind remindField;

        private attendeeOptions attendeeOptionsField;

        private string assistServiceField;

        private string meetingkeyField;

        private string statusField;

        private bool hostJoinedField;

        private bool participantsJoinedField;

        private bool telePresenceField;

        private string hostKeyField;

        private string eventIDField;

        private string guestTokenField;

        private uint hostTypeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public accessControl accessControl
        {
            get
            {
                return this.accessControlField;
            }
            set
            {
                this.accessControlField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public metaData metaData
        {
            get
            {
                return this.metaDataField;
            }
            set
            {
                this.metaDataField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public participants participants
        {
            get
            {
                return this.participantsField;
            }
            set
            {
                this.participantsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public enableOptions enableOptions
        {
            get
            {
                return this.enableOptionsField;
            }
            set
            {
                this.enableOptionsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public schedule schedule
        {
            get
            {
                return this.scheduleField;
            }
            set
            {
                this.scheduleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public telephony telephony
        {
            get
            {
                return this.telephonyField;
            }
            set
            {
                this.telephonyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public tracking tracking
        {
            get
            {
                return this.trackingField;
            }
            set
            {
                this.trackingField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public repeat repeat
        {
            get
            {
                return this.repeatField;
            }
            set
            {
                this.repeatField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public remind remind
        {
            get
            {
                return this.remindField;
            }
            set
            {
                this.remindField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public attendeeOptions attendeeOptions
        {
            get
            {
                return this.attendeeOptionsField;
            }
            set
            {
                this.attendeeOptionsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public string assistService
        {
            get
            {
                return this.assistServiceField;
            }
            set
            {
                this.assistServiceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public string meetingkey
        {
            get
            {
                return this.meetingkeyField;
            }
            set
            {
                this.meetingkeyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public string status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public bool hostJoined
        {
            get
            {
                return this.hostJoinedField;
            }
            set
            {
                this.hostJoinedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public bool participantsJoined
        {
            get
            {
                return this.participantsJoinedField;
            }
            set
            {
                this.participantsJoinedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public bool telePresence
        {
            get
            {
                return this.telePresenceField;
            }
            set
            {
                this.telePresenceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public string hostKey
        {
            get
            {
                return this.hostKeyField;
            }
            set
            {
                this.hostKeyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public string eventID
        {
            get
            {
                return this.eventIDField;
            }
            set
            {
                this.eventIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public string guestToken
        {
            get
            {
                return this.guestTokenField;
            }
            set
            {
                this.guestTokenField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public uint hostType
        {
            get
            {
                return this.hostTypeField;
            }
            set
            {
                this.hostTypeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting", IsNullable = false)]
    public partial class accessControl
    {

        private string sessionPasswordField;

        private string listStatusField;

        private bool registrationField;

        private bool passwordReqField;

        private string meetingPasswordField;

        private bool listToPublicField;

        private bool isPublicField;

        public string meetingPassword
        {
            get
            {
                return this.meetingPasswordField;
            }
            set
            {
                this.meetingPasswordField = value;
            }
        }





        /// <remarks/>
        public bool listToPublic
        {
            get
            {
                return this.listToPublicField;
            }
            set
            {
                this.listToPublicField = value;
            }
        }

        /// <remarks/>
        public bool isPublic
        {
            get
            {
                return this.isPublicField;
            }
            set
            {
                this.isPublicField = value;
            }
        }


        /// <remarks/>
        public string sessionPassword
        {
            get
            {
                return this.sessionPasswordField;
            }
            set
            {
                this.sessionPasswordField = value;
            }
        }

        /// <remarks/>
        public string listStatus
        {
            get
            {
                return this.listStatusField;
            }
            set
            {
                this.listStatusField = value;
            }
        }

        /// <remarks/>
        public bool registration
        {
            get
            {
                return this.registrationField;
            }
            set
            {
                this.registrationField = value;
            }
        }

        /// <remarks/>
        public bool passwordReq
        {
            get
            {
                return this.passwordReqField;
            }
            set
            {
                this.passwordReqField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting", IsNullable = false)]
    public partial class metaData
    {

        private string serviceTypeField;

        private int sessionTypeField;

        private string confNameField;

        private string sessionPasswordField;

        private string listingField;

        private bool isRecurringField;

        private metaDataSessionTemplate sessionTemplateField;

        private int meetingTypeField;

        private string agendaField;

        private string invitationField;

        public string invitation
        {
            get
            {
                return this.invitationField;
            }
            set
            {
                this.invitationField = value;
            }
        }

        public int meetingType
        {
            get
            {
                return this.meetingTypeField;
            }
            set
            {
                this.meetingTypeField = value;
            }
        }

        public string agenda
        {
            get
            {
                return this.agendaField;
            }
            set
            {
                this.agendaField = value;
            }
        }
        /// <remarks/>
        public string serviceType
        {
            get
            {
                return this.serviceTypeField;
            }
            set
            {
                this.serviceTypeField = value;
            }
        }

        /// <remarks/>
        public int sessionType
        {
            get
            {
                return this.sessionTypeField;
            }
            set
            {
                this.sessionTypeField = value;
            }
        }

        /// <remarks/>
        public string confName
        {
            get
            {
                return this.confNameField;
            }
            set
            {
                this.confNameField = value;
            }
        }

        /// <remarks/>
        public string sessionPassword
        {
            get
            {
                return this.sessionPasswordField;
            }
            set
            {
                this.sessionPasswordField = value;
            }
        }

        /// <remarks/>
        public string listing
        {
            get
            {
                return this.listingField;
            }
            set
            {
                this.listingField = value;
            }
        }

        /// <remarks/>
        public metaDataSessionTemplate sessionTemplate
        {
            get
            {
                return this.sessionTemplateField;
            }
            set
            {
                this.sessionTemplateField = value;
            }
        }

        public bool isRecurring
        {
            get
            {
                return this.isRecurringField;
            }
            set
            {
                this.isRecurringField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
    public partial class metaDataSessionTemplate
    {

        private string nameField;

        private string typeField;

        private string valueField;

        /// <remarks/>
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public string type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }



    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting", IsNullable = false)]
    public partial class participants
    {

        private int maxUserNumberField;

        private participantsAttendee[] attendeesField;

        /// <remarks/>
        public int maxUserNumber
        {
            get
            {
                return this.maxUserNumberField;
            }
            set
            {
                this.maxUserNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("attendee", IsNullable = false)]
        public participantsAttendee[] attendees
        {
            get
            {
                return this.attendeesField;
            }
            set
            {
                this.attendeesField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
    public partial class participantsAttendee
    {

        private person personField;

        private string contactIDField;

        private string joinStatusField;

        private string meetingKeyField;

        private string languageField;

        private string roleField;

        private int languageIDField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/attendee")]
        public person person
        {
            get
            {
                return this.personField;
            }
            set
            {
                this.personField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/attendee")]
        public string contactID
        {
            get
            {
                return this.contactIDField;
            }
            set
            {
                this.contactIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/attendee")]
        public string joinStatus
        {
            get
            {
                return this.joinStatusField;
            }
            set
            {
                this.joinStatusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/attendee")]
        public string meetingKey
        {
            get
            {
                return this.meetingKeyField;
            }
            set
            {
                this.meetingKeyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/attendee")]
        public string language
        {
            get
            {
                return this.languageField;
            }
            set
            {
                this.languageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/attendee")]
        public string role
        {
            get
            {
                return this.roleField;
            }
            set
            {
                this.roleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/attendee")]
        public int languageID
        {
            get
            {
                return this.languageIDField;
            }
            set
            {
                this.languageIDField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/attendee")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/attendee", IsNullable = false)]
    public partial class person
    {

        private string nameField;

        private string firstNameField;

        private string lastNameField;

        private address addressField;

        private string phonesField;

        private string emailField;

        private string typeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/common")]
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/common")]
        public string firstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/common")]
        public string lastName
        {
            get
            {
                return this.lastNameField;
            }
            set
            {
                this.lastNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/common")]
        public address address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/common")]
        public string phones
        {
            get
            {
                return this.phonesField;
            }
            set
            {
                this.phonesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/common")]
        public string email
        {
            get
            {
                return this.emailField;
            }
            set
            {
                this.emailField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/common")]
        public string type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/common")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/common", IsNullable = false)]
    public partial class address
    {

        private string addressTypeField;

        private string countryField;

        private string zipCodeField;

        private string stateField;

        private string cityField;

        private string address1Field;

        private string address2Field;

        /// <remarks/>
        public string addressType
        {
            get
            {
                return this.addressTypeField;
            }
            set
            {
                this.addressTypeField = value;
            }
        }

        public string address1
        {
            get
            {
                return this.address1Field;
            }
            set
            {
                this.address1Field = value;
            }
        }

        public string address2
        {
            get
            {
                return this.address2Field;
            }
            set
            {
                this.address2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/common")]
        public string country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/common")]
        public string state
        {
            get
            {
                return this.stateField;
            }
            set
            {
                this.stateField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/common")]
        public string zipCode
        {
            get
            {
                return this.zipCodeField;
            }
            set
            {
                this.zipCodeField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/common")]
        public string city
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }
    }
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting", IsNullable = false)]
    public partial class enableOptions
    {

        private bool chatField;

        private bool pollField;

        private bool audioVideoField;

        private bool attendeeListField;

        private bool fileShareField;

        private bool presentationField;

        private bool applicationShareField;

        private bool desktopShareField;

        private bool webTourField;

        private bool meetingRecordField;

        private bool annotationField;

        private bool importDocumentField;

        private bool saveDocumentField;

        private bool printDocumentField;

        private bool pointerField;

        private bool switchPageField;

        private bool fullScreenField;

        private bool thumbnailField;

        private bool zoomField;

        private bool copyPageField;

        private bool rcAppShareField;

        private bool rcDesktopShareField;

        private bool rcWebTourField;

        private bool javaClientField;

        private bool nativeClientField;

        private bool attendeeRecordMeetingField;

        private bool voipField;

        private bool faxIntoMeetingField;

        private bool enableRegField;

        private bool supportQandAField;

        private bool supportFeedbackField;

        private bool supportBreakoutSessionsField;

        private bool supportPanelistsField;

        private bool supportRemoteComputerField;

        private bool supportShareWebContentField;

        private bool supportUCFWebPagesField;

        private bool supportUCFRichMediaField;

        private bool autoDeleteAfterMeetingEndField;

        private bool viewAnyDocField;

        private bool viewAnyPageField;

        private bool allowContactPrivateField;

        private bool chatHostField;

        private bool chatPresenterField;

        private bool chatAllAttendeesField;

        private bool multiVideoField;

        private bool notesField;

        private bool closedCaptionsField;

        private bool singleNoteField;

        private bool sendFeedbackField;

        private bool displayQuickStartHostField;

        private bool displayQuickStartAttendeesField;

        private bool supportE2EField;

        private bool supportPKIField;

        private bool hQvideoField;

        private bool hDvideoField;

        private bool viewVideoThumbsField;

        /// <remarks/>
        public bool chat
        {
            get
            {
                return this.chatField;
            }
            set
            {
                this.chatField = value;
            }
        }

        /// <remarks/>
        public bool poll
        {
            get
            {
                return this.pollField;
            }
            set
            {
                this.pollField = value;
            }
        }

        /// <remarks/>
        public bool audioVideo
        {
            get
            {
                return this.audioVideoField;
            }
            set
            {
                this.audioVideoField = value;
            }
        }

        /// <remarks/>
        public bool attendeeList
        {
            get
            {
                return this.attendeeListField;
            }
            set
            {
                this.attendeeListField = value;
            }
        }

        /// <remarks/>
        public bool fileShare
        {
            get
            {
                return this.fileShareField;
            }
            set
            {
                this.fileShareField = value;
            }
        }

        /// <remarks/>
        public bool presentation
        {
            get
            {
                return this.presentationField;
            }
            set
            {
                this.presentationField = value;
            }
        }

        /// <remarks/>
        public bool applicationShare
        {
            get
            {
                return this.applicationShareField;
            }
            set
            {
                this.applicationShareField = value;
            }
        }

        /// <remarks/>
        public bool desktopShare
        {
            get
            {
                return this.desktopShareField;
            }
            set
            {
                this.desktopShareField = value;
            }
        }

        /// <remarks/>
        public bool webTour
        {
            get
            {
                return this.webTourField;
            }
            set
            {
                this.webTourField = value;
            }
        }

        /// <remarks/>
        public bool meetingRecord
        {
            get
            {
                return this.meetingRecordField;
            }
            set
            {
                this.meetingRecordField = value;
            }
        }

        /// <remarks/>
        public bool annotation
        {
            get
            {
                return this.annotationField;
            }
            set
            {
                this.annotationField = value;
            }
        }

        /// <remarks/>
        public bool importDocument
        {
            get
            {
                return this.importDocumentField;
            }
            set
            {
                this.importDocumentField = value;
            }
        }

        /// <remarks/>
        public bool saveDocument
        {
            get
            {
                return this.saveDocumentField;
            }
            set
            {
                this.saveDocumentField = value;
            }
        }

        /// <remarks/>
        public bool printDocument
        {
            get
            {
                return this.printDocumentField;
            }
            set
            {
                this.printDocumentField = value;
            }
        }

        /// <remarks/>
        public bool pointer
        {
            get
            {
                return this.pointerField;
            }
            set
            {
                this.pointerField = value;
            }
        }

        /// <remarks/>
        public bool switchPage
        {
            get
            {
                return this.switchPageField;
            }
            set
            {
                this.switchPageField = value;
            }
        }

        /// <remarks/>
        public bool fullScreen
        {
            get
            {
                return this.fullScreenField;
            }
            set
            {
                this.fullScreenField = value;
            }
        }

        /// <remarks/>
        public bool thumbnail
        {
            get
            {
                return this.thumbnailField;
            }
            set
            {
                this.thumbnailField = value;
            }
        }

        /// <remarks/>
        public bool zoom
        {
            get
            {
                return this.zoomField;
            }
            set
            {
                this.zoomField = value;
            }
        }

        /// <remarks/>
        public bool copyPage
        {
            get
            {
                return this.copyPageField;
            }
            set
            {
                this.copyPageField = value;
            }
        }

        /// <remarks/>
        public bool rcAppShare
        {
            get
            {
                return this.rcAppShareField;
            }
            set
            {
                this.rcAppShareField = value;
            }
        }

        /// <remarks/>
        public bool rcDesktopShare
        {
            get
            {
                return this.rcDesktopShareField;
            }
            set
            {
                this.rcDesktopShareField = value;
            }
        }

        /// <remarks/>
        public bool rcWebTour
        {
            get
            {
                return this.rcWebTourField;
            }
            set
            {
                this.rcWebTourField = value;
            }
        }

        /// <remarks/>
        public bool javaClient
        {
            get
            {
                return this.javaClientField;
            }
            set
            {
                this.javaClientField = value;
            }
        }

        /// <remarks/>
        public bool nativeClient
        {
            get
            {
                return this.nativeClientField;
            }
            set
            {
                this.nativeClientField = value;
            }
        }

        /// <remarks/>
        public bool attendeeRecordMeeting
        {
            get
            {
                return this.attendeeRecordMeetingField;
            }
            set
            {
                this.attendeeRecordMeetingField = value;
            }
        }

        /// <remarks/>
        public bool voip
        {
            get
            {
                return this.voipField;
            }
            set
            {
                this.voipField = value;
            }
        }

        /// <remarks/>
        public bool faxIntoMeeting
        {
            get
            {
                return this.faxIntoMeetingField;
            }
            set
            {
                this.faxIntoMeetingField = value;
            }
        }

        /// <remarks/>
        public bool enableReg
        {
            get
            {
                return this.enableRegField;
            }
            set
            {
                this.enableRegField = value;
            }
        }

        /// <remarks/>
        public bool supportQandA
        {
            get
            {
                return this.supportQandAField;
            }
            set
            {
                this.supportQandAField = value;
            }
        }

        /// <remarks/>
        public bool supportFeedback
        {
            get
            {
                return this.supportFeedbackField;
            }
            set
            {
                this.supportFeedbackField = value;
            }
        }

        /// <remarks/>
        public bool supportBreakoutSessions
        {
            get
            {
                return this.supportBreakoutSessionsField;
            }
            set
            {
                this.supportBreakoutSessionsField = value;
            }
        }

        /// <remarks/>
        public bool supportPanelists
        {
            get
            {
                return this.supportPanelistsField;
            }
            set
            {
                this.supportPanelistsField = value;
            }
        }

        /// <remarks/>
        public bool supportRemoteComputer
        {
            get
            {
                return this.supportRemoteComputerField;
            }
            set
            {
                this.supportRemoteComputerField = value;
            }
        }

        /// <remarks/>
        public bool supportShareWebContent
        {
            get
            {
                return this.supportShareWebContentField;
            }
            set
            {
                this.supportShareWebContentField = value;
            }
        }

        /// <remarks/>
        public bool supportUCFWebPages
        {
            get
            {
                return this.supportUCFWebPagesField;
            }
            set
            {
                this.supportUCFWebPagesField = value;
            }
        }

        /// <remarks/>
        public bool supportUCFRichMedia
        {
            get
            {
                return this.supportUCFRichMediaField;
            }
            set
            {
                this.supportUCFRichMediaField = value;
            }
        }

        /// <remarks/>
        public bool autoDeleteAfterMeetingEnd
        {
            get
            {
                return this.autoDeleteAfterMeetingEndField;
            }
            set
            {
                this.autoDeleteAfterMeetingEndField = value;
            }
        }

        /// <remarks/>
        public bool viewAnyDoc
        {
            get
            {
                return this.viewAnyDocField;
            }
            set
            {
                this.viewAnyDocField = value;
            }
        }

        /// <remarks/>
        public bool viewAnyPage
        {
            get
            {
                return this.viewAnyPageField;
            }
            set
            {
                this.viewAnyPageField = value;
            }
        }

        /// <remarks/>
        public bool allowContactPrivate
        {
            get
            {
                return this.allowContactPrivateField;
            }
            set
            {
                this.allowContactPrivateField = value;
            }
        }

        /// <remarks/>
        public bool chatHost
        {
            get
            {
                return this.chatHostField;
            }
            set
            {
                this.chatHostField = value;
            }
        }

        /// <remarks/>
        public bool chatPresenter
        {
            get
            {
                return this.chatPresenterField;
            }
            set
            {
                this.chatPresenterField = value;
            }
        }

        /// <remarks/>
        public bool chatAllAttendees
        {
            get
            {
                return this.chatAllAttendeesField;
            }
            set
            {
                this.chatAllAttendeesField = value;
            }
        }

        /// <remarks/>
        public bool multiVideo
        {
            get
            {
                return this.multiVideoField;
            }
            set
            {
                this.multiVideoField = value;
            }
        }

        /// <remarks/>
        public bool notes
        {
            get
            {
                return this.notesField;
            }
            set
            {
                this.notesField = value;
            }
        }

        /// <remarks/>
        public bool closedCaptions
        {
            get
            {
                return this.closedCaptionsField;
            }
            set
            {
                this.closedCaptionsField = value;
            }
        }

        /// <remarks/>
        public bool singleNote
        {
            get
            {
                return this.singleNoteField;
            }
            set
            {
                this.singleNoteField = value;
            }
        }

        /// <remarks/>
        public bool sendFeedback
        {
            get
            {
                return this.sendFeedbackField;
            }
            set
            {
                this.sendFeedbackField = value;
            }
        }

        /// <remarks/>
        public bool displayQuickStartHost
        {
            get
            {
                return this.displayQuickStartHostField;
            }
            set
            {
                this.displayQuickStartHostField = value;
            }
        }

        /// <remarks/>
        public bool displayQuickStartAttendees
        {
            get
            {
                return this.displayQuickStartAttendeesField;
            }
            set
            {
                this.displayQuickStartAttendeesField = value;
            }
        }

        /// <remarks/>
        public bool supportE2E
        {
            get
            {
                return this.supportE2EField;
            }
            set
            {
                this.supportE2EField = value;
            }
        }

        /// <remarks/>
        public bool supportPKI
        {
            get
            {
                return this.supportPKIField;
            }
            set
            {
                this.supportPKIField = value;
            }
        }

        /// <remarks/>
        public bool HQvideo
        {
            get
            {
                return this.hQvideoField;
            }
            set
            {
                this.hQvideoField = value;
            }
        }

        /// <remarks/>
        public bool HDvideo
        {
            get
            {
                return this.hDvideoField;
            }
            set
            {
                this.hDvideoField = value;
            }
        }

        /// <remarks/>
        public bool viewVideoThumbs
        {
            get
            {
                return this.viewVideoThumbsField;
            }
            set
            {
                this.viewVideoThumbsField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting", IsNullable = false)]
    public partial class schedule
    {

        private string startDateField;

        private int timeZoneIDField;

        private string timeZoneField;

        private int durationField;

        private ushort openTimeField;

        private string hostWebExIDField;

        private bool showFileStartModeField;

        private bool showFileContPlayFlagField;

        private int showFileInterValField;

        private int entryExitToneField;

        private int extNotifyTimeField;

        private bool joinTeleconfBeforeHostField;

        private bool firstAttendeeAsPresenterField;

        private bool allowAnyoneHostMeetingField;

        /// <remarks/>
        public string startDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }

        /// <remarks/>
        public int timeZoneID
        {
            get
            {
                return this.timeZoneIDField;
            }
            set
            {
                this.timeZoneIDField = value;
            }
        }

        /// <remarks/>
        public string timeZone
        {
            get
            {
                return this.timeZoneField;
            }
            set
            {
                this.timeZoneField = value;
            }
        }

        /// <remarks/>
        public int duration
        {
            get
            {
                return this.durationField;
            }
            set
            {
                this.durationField = value;
            }
        }

        /// <remarks/>
        public ushort openTime
        {
            get
            {
                return this.openTimeField;
            }
            set
            {
                this.openTimeField = value;
            }
        }

        /// <remarks/>
        public string hostWebExID
        {
            get
            {
                return this.hostWebExIDField;
            }
            set
            {
                this.hostWebExIDField = value;
            }
        }

        /// <remarks/>
        public bool showFileStartMode
        {
            get
            {
                return this.showFileStartModeField;
            }
            set
            {
                this.showFileStartModeField = value;
            }
        }

        /// <remarks/>
        public bool showFileContPlayFlag
        {
            get
            {
                return this.showFileContPlayFlagField;
            }
            set
            {
                this.showFileContPlayFlagField = value;
            }
        }

        /// <remarks/>
        public int showFileInterVal
        {
            get
            {
                return this.showFileInterValField;
            }
            set
            {
                this.showFileInterValField = value;
            }
        }

        /// <remarks/>
        public int entryExitTone
        {
            get
            {
                return this.entryExitToneField;
            }
            set
            {
                this.entryExitToneField = value;
            }
        }

        /// <remarks/>
        public int extNotifyTime
        {
            get
            {
                return this.extNotifyTimeField;
            }
            set
            {
                this.extNotifyTimeField = value;
            }
        }

        /// <remarks/>
        public bool joinTeleconfBeforeHost
        {
            get
            {
                return this.joinTeleconfBeforeHostField;
            }
            set
            {
                this.joinTeleconfBeforeHostField = value;
            }
        }

        /// <remarks/>
        public bool firstAttendeeAsPresenter
        {
            get
            {
                return this.firstAttendeeAsPresenterField;
            }
            set
            {
                this.firstAttendeeAsPresenterField = value;
            }
        }

        /// <remarks/>
        public bool allowAnyoneHostMeeting
        {
            get
            {
                return this.allowAnyoneHostMeetingField;
            }
            set
            {
                this.allowAnyoneHostMeetingField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting", IsNullable = false)]
    public partial class telephony
    {

        private string telephonySupportField;

        private int? tspAccountIndexField;

        private telephonyAccount accountField;

        private telephonyAccountLabel accountLabelField;

        private string teleconfServiceNameField;

        private bool? intlLocalCallInField;

        private bool? tollfreeField;

        private string entryExitToneField;

        private string extTelephonyDescriptionField;

        public string extTelephonyDescription
        {
            get { return this.extTelephonyDescriptionField; }
            set { extTelephonyDescriptionField = value; }
        }

        private uint? numPhoneLinesField;



        private bool? enableTSPField;


        private string callInNumField;


        public string callInNum
        {
            get
            {
                return this.callInNumField;
            }
            set
            {
                this.callInNumField = value;
            }
        }
        /// <remarks/>
        public string telephonySupport
        {
            get
            {
                return this.telephonySupportField;
            }
            set
            {
                this.telephonySupportField = value;
            }
        }

        /// <remarks/>
        public uint? numPhoneLines
        {
            get
            {
                return this.numPhoneLinesField;
            }
            set
            {
                this.numPhoneLinesField = value;
            }
        }



        /// <remarks/>
        public bool? enableTSP
        {
            get
            {
                return this.enableTSPField;
            }
            set
            {
                this.enableTSPField = value;
            }
        }




        /// <remarks/>
        public int? tspAccountIndex
        {
            get
            {
                return this.tspAccountIndexField;
            }
            set
            {
                this.tspAccountIndexField = value;
            }
        }

        /// <remarks/>
        public telephonyAccount account
        {
            get
            {
                return this.accountField;
            }
            set
            {
                this.accountField = value;
            }
        }

        /// <remarks/>
        public telephonyAccountLabel accountLabel
        {
            get
            {
                return this.accountLabelField;
            }
            set
            {
                this.accountLabelField = value;
            }
        }

        /// <remarks/>
        public string teleconfServiceName
        {
            get
            {
                return this.teleconfServiceNameField;
            }
            set
            {
                this.teleconfServiceNameField = value;
            }
        }

        /// <remarks/>
        public bool? intlLocalCallIn
        {
            get
            {
                return this.intlLocalCallInField;
            }
            set
            {
                this.intlLocalCallInField = value;
            }
        }

        /// <remarks/>
        public bool? tollfree
        {
            get
            {
                return this.tollfreeField;
            }
            set
            {
                this.tollfreeField = value;
            }
        }

        /// <remarks/>
        public string entryExitTone
        {
            get
            {
                return this.entryExitToneField;
            }
            set
            {
                this.entryExitToneField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting", IsNullable = false)]
    public partial class telephonyAccount
    {

        private string tollFreeCallInNumberField;

        private string tollCallInNumberField;

        private string subscriberAccessCodeField;

        private string participantAccessCodeField;

        private string participantLimitedAccessCodeField;

        private telephonyAccountGlobalNum[] globalNumField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollFreeCallInNumber
        {
            get
            {
                return this.tollFreeCallInNumberField;
            }
            set
            {
                this.tollFreeCallInNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollCallInNumber
        {
            get
            {
                return this.tollCallInNumberField;
            }
            set
            {
                this.tollCallInNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string subscriberAccessCode
        {
            get
            {
                return this.subscriberAccessCodeField;
            }
            set
            {
                this.subscriberAccessCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string participantAccessCode
        {
            get
            {
                return this.participantAccessCodeField;
            }
            set
            {
                this.participantAccessCodeField = value;
            }
        }

        /// <remarks/>
        public string participantLimitedAccessCode
        {
            get
            {
                return this.participantLimitedAccessCodeField;
            }
            set
            {
                this.participantLimitedAccessCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("globalNum")]
        public telephonyAccountGlobalNum[] globalNum
        {
            get
            {
                return this.globalNumField;
            }
            set
            {
                this.globalNumField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting", IsNullable = false)]
    public partial class telephonyAccountGlobalNum
    {

        private string countryAliasField;

        private string phoneNumberField;

        private bool? tollFreeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string countryAlias
        {
            get
            {
                return this.countryAliasField;
            }
            set
            {
                this.countryAliasField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string phoneNumber
        {
            get
            {
                return this.phoneNumberField;
            }
            set
            {
                this.phoneNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public bool? tollFree
        {
            get
            {
                return this.tollFreeField;
            }
            set
            {
                this.tollFreeField = value;
            }
        }
    }

    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting", IsNullable = false)]
    public partial class telephonyAccountLabel
    {

        private string tollFreeCallInNumberLabelField;

        private string tollCallInNumberLabelField;

        private string subscriberAccessCodeLabelField;

        private string participantAccessCodeLabelField;

        private string participantLimitedAccessCodeLabelField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollFreeCallInNumberLabel
        {
            get
            {
                return this.tollFreeCallInNumberLabelField;
            }
            set
            {
                this.tollFreeCallInNumberLabelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollCallInNumberLabel
        {
            get
            {
                return this.tollCallInNumberLabelField;
            }
            set
            {
                this.tollCallInNumberLabelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string subscriberAccessCodeLabel
        {
            get
            {
                return this.subscriberAccessCodeLabelField;
            }
            set
            {
                this.subscriberAccessCodeLabelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string participantAccessCodeLabel
        {
            get
            {
                return this.participantAccessCodeLabelField;
            }
            set
            {
                this.participantAccessCodeLabelField = value;
            }
        }

        /// <remarks/>
        public string participantLimitedAccessCodeLabel
        {
            get
            {
                return this.participantLimitedAccessCodeLabelField;
            }
            set
            {
                this.participantLimitedAccessCodeLabelField = value;
            }
        }
    }


    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting", IsNullable = false)]
    public partial class repeat
    {

        private string repeatTypeField;

        private byte intervalField;

        private byte afterMeetingNumberField;

        private string[] dayInWeekField;

        private byte dayInMonthField;

        private byte weekInMonthField;

        private string monthInYearField;

        private string expirationDateField;

        /// <remarks/>
        public string repeatType
        {
            get
            {
                return this.repeatTypeField;
            }
            set
            {
                this.repeatTypeField = value;
            }
        }

        /// <remarks/>
        public byte interval
        {
            get
            {
                return this.intervalField;
            }
            set
            {
                this.intervalField = value;
            }
        }

        /// <remarks/>
        public byte afterMeetingNumber
        {
            get
            {
                return this.afterMeetingNumberField;
            }
            set
            {
                this.afterMeetingNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("day", IsNullable = false)]
        public string[] dayInWeek
        {
            get
            {
                return this.dayInWeekField;
            }
            set
            {
                this.dayInWeekField = value;
            }
        }

        /// <remarks/>
        public byte dayInMonth
        {
            get
            {
                return this.dayInMonthField;
            }
            set
            {
                this.dayInMonthField = value;
            }
        }

        /// <remarks/>
        public byte weekInMonth
        {
            get
            {
                return this.weekInMonthField;
            }
            set
            {
                this.weekInMonthField = value;
            }
        }

        /// <remarks/>
        public string monthInYear
        {
            get
            {
                return this.monthInYearField;
            }
            set
            {
                this.monthInYearField = value;
            }
        }

        /// <remarks/>
        public string expirationDate
        {
            get
            {
                return this.expirationDateField;
            }
            set
            {
                this.expirationDateField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting", IsNullable = false)]
    public partial class attendeeOptions
    {

        private bool requestField;

        private bool registrationField;

        private bool autoField;

        private int participantLimitField;

        private bool excludePasswordField;

        private bool joinRequiresAccountField;

        /// <remarks/>
        public bool request
        {
            get
            {
                return this.requestField;
            }
            set
            {
                this.requestField = value;
            }
        }

        /// <remarks/>
        public bool registration
        {
            get
            {
                return this.registrationField;
            }
            set
            {
                this.registrationField = value;
            }
        }

        /// <remarks/>
        public bool auto
        {
            get
            {
                return this.autoField;
            }
            set
            {
                this.autoField = value;
            }
        }

        /// <remarks/>
        public int participantLimit
        {
            get
            {
                return this.participantLimitField;
            }
            set
            {
                this.participantLimitField = value;
            }
        }

        /// <remarks/>
        public bool excludePassword
        {
            get
            {
                return this.excludePasswordField;
            }
            set
            {
                this.excludePasswordField = value;
            }
        }

        /// <remarks/>
        public bool joinRequiresAccount
        {
            get
            {
                return this.joinRequiresAccountField;
            }
            set
            {
                this.joinRequiresAccountField = value;
            }
        }
    }


    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting", IsNullable = false)]
    public partial class tracking
    {

        private string trackingCode1Field;

        private string trackingCode2Field;

        private string trackingCode3Field;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/common")]
        public string trackingCode1
        {
            get
            {
                return this.trackingCode1Field;
            }
            set
            {
                this.trackingCode1Field = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting", IsNullable = false)]
    public partial class remind
    {

        private bool? enableReminderField;

        private remindEmails emailsField;

        private bool? sendEmailField;

        private bool? sendMobileField;

        private int? daysAheadField;

        private int? hoursAheadField;

        private int? minutesAheadField;

        /// <remarks/>
        public bool? enableReminder
        {
            get
            {
                return this.enableReminderField;
            }
            set
            {
                this.enableReminderField = value;
            }
        }

        /// <remarks/>
        public remindEmails emails
        {
            get
            {
                return this.emailsField;
            }
            set
            {
                this.emailsField = value;
            }
        }

        /// <remarks/>
        public bool? sendEmail
        {
            get
            {
                return this.sendEmailField;
            }
            set
            {
                this.sendEmailField = value;
            }
        }

        /// <remarks/>
        public bool? sendMobile
        {
            get
            {
                return this.sendMobileField;
            }
            set
            {
                this.sendMobileField = value;
            }
        }

        /// <remarks/>
        public int? daysAhead
        {
            get
            {
                return this.daysAheadField;
            }
            set
            {
                this.daysAheadField = value;
            }
        }

        /// <remarks/>
        public int? hoursAhead
        {
            get
            {
                return this.hoursAheadField;
            }
            set
            {
                this.hoursAheadField = value;
            }
        }

        /// <remarks/>
        public int? minutesAhead
        {
            get
            {
                return this.minutesAheadField;
            }
            set
            {
                this.minutesAheadField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
    public partial class remindEmails
    {

        private string emailField;

        /// <remarks/>
        public string email
        {
            get
            {
                return this.emailField;
            }
            set
            {
                this.emailField = value;
            }
        }
    }
}
