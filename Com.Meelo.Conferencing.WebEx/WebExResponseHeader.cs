﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.Conferencing.WebEx
{
    public class WebExResponseHeader
    {
        public string Result { get; set; }

        public string Reason { get; set; }

        public string GsbStatus { get; set; }

        public uint? ExceptionID { get; set; }
    }
}
