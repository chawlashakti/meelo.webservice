﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.Meelo.Conferencing.WebEx.Generic;

namespace Com.Meelo.Conferencing.WebEx
{
    public class CreateMeetingInput
    {
        public accessControl AccessControl { get; set; }

        public metaData Metadata { get; set; }

        public enableOptions EnableOptions { get; set; }

        public IEnumerable<Person> Attendees { get; set; }

        public int? MaxUserNumber { get; set; }

        public schedule Schedule { get; set; }

        public telephony Telephony { get; set; }

        public Repeat Repeat { get; set; }

        public remind Remind { get; set; }

        public attendeeOptions AttendeeOptions { get; set; }
    }


    public class Person
    {
        public Person()
        {
            EmailInvitation = true;
        }
        public string Name { get; set; }

        public string Email { get; set; }

        public bool? EmailInvitation { get; set; }
    }
}
