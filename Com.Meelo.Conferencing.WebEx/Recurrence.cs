﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.Conferencing.WebEx
{
   public class Recurrence
    {

        public static string NoRecurrenceXml()
        {
            return "<repeat><repeatType>NO_REPEAT</repeatType></repeat>";
        }

        /// <summary>
        /// Examples:
        /// Daily
        /// Recur every 2 days and end after 10 meetings
        /// Recur every weekday and end after 02/14/2005
        /// Weekly
        /// Recur every Monday and end after 02/14/2005
        /// Recur every Monday and end after 5 meetings
        /// Monthly
        /// Recur on the 14th day of every month and end after 01/14/2005
        /// Recur on the first Tuesday of every month and end after 8 meetings
        /// Yearly
        /// Recur every December 15th and end after 11/16/2008
        /// Recur on the second Monday of every December and end after 2 meetings
        /// </summary>
        /// <param name="repeat"></param>
        /// <returns></returns>
        public static string GetRecurrenceXml(Repeat repeat)
        {
            StringBuilder strb = new StringBuilder();

            strb.Append("<repeat>");
            strb.AppendFormat("<repeatType>{0}</repeatType>", repeat.RepeatType);

            //end after [Interval] meetings
            if(repeat.Interval != null)
            {
                strb.AppendFormat("<interval>{0}</interval>", repeat.Interval);
            }

            if (repeat.AfterMeetingNumber != null)
            {
                strb.AppendFormat("<afterMeetingNumber>{0}</afterMeetingNumber>", repeat.AfterMeetingNumber);
            }
            if (repeat.DayInWeek != null && repeat.DayInWeek.Count() > 0)
            {
                strb.Append("<dayInWeek>");

                foreach(DayOfWeek dw in repeat.DayInWeek)
                {
                    strb.AppendFormat("<day>{0}</day>", dw.ToString());
                }
                strb.Append("</dayInWeek>");
            }

            //Recur on the 14th day of every month and end after 01/14/2005
            if (repeat.DayInMonth != null)
            {
                strb.AppendFormat("<dayInMonth>{0}</dayInMonth>", repeat.DayInMonth);
            }

            //Recur on the first Tuesday of every month and end after 8 meetings
            if (repeat.WeekInMonth != null)
            {
                strb.AppendFormat("<weekInMonth>{0}</weekInMonth>", repeat.WeekInMonth);
            }

            //Recur every December 15th and end after 11 / 16 / 2008

            if (repeat.MonthInYear != null)
            {
                strb.AppendFormat("<monthInYear>{0}</monthInYear>", repeat.MonthInYear);
            }
            //end after [expirationDate]
            if (repeat.ExpirationDate != null && repeat.ExpirationDate.HasValue)
            {
                strb.AppendFormat("<expirationDate>{0}</expirationDate>", repeat.ExpirationDate.Value.ToWebExReadable());
            }

            strb.Append("</repeat>");

            return strb.ToString();
        }
    }
}
