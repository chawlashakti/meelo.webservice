﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.Conferencing.WebEx
{
    public class WebExUser
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Initials { get; set; }

        public string Title { get; set; }

        public string OfficeTitle { get; set; }

        public string Company { get; set; }

        public string WebExID { get; set; }

        public string Email { get; set; }

        public string AddressType { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public int CountryCode { get; set; }

        public string Country { get; set; }

        public string State { get; set; }

        public string ZipCode { get; set; }

        public string City { get; set; }

        public string Phone { get; set; }

        public string MobilePhone { get; set; }

        public string PersonalUrl { get; set; }

        public int TimeZoneID { get; set; }

        public string TimeZone { get; set; }

        public string TimeZoneWithDST { get; set; }

        public string Service { get; set; }

        public string Language { get; set; }

        public string Locale { get; set; }

        public string PersonalMeetingRoomURL { get; set; }

        public string AccessCode { get; set; }

        public string HostPin { get; set; }

        public string UserId { get; set; }

        public string AvatarUrl { get; set; }

        public string SipURL { get; set; }
    }
}
