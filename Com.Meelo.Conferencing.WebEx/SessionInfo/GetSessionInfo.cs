﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.Conferencing.WebEx.SessionInfo
{


    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service", IsNullable = false)]
    public partial class message
    {

        private messageHeader headerField;

        private messageBody bodyField;

        /// <remarks/>
        public messageHeader header
        {
            get
            {
                return this.headerField;
            }
            set
            {
                this.headerField = value;
            }
        }

        /// <remarks/>
        public messageBody body
        {
            get
            {
                return this.bodyField;
            }
            set
            {
                this.bodyField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service")]
    public partial class messageHeader
    {

        private messageHeaderResponse responseField;

        /// <remarks/>
        public messageHeaderResponse response
        {
            get
            {
                return this.responseField;
            }
            set
            {
                this.responseField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service")]
    public partial class messageHeaderResponse
    {

        private string resultField;

        private string gsbStatusField;

        /// <remarks/>
        public string result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        public string gsbStatus
        {
            get
            {
                return this.gsbStatusField;
            }
            set
            {
                this.gsbStatusField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service")]
    public partial class messageBody
    {

        private messageBodyBodyContent bodyContentField;

        /// <remarks/>
        public messageBodyBodyContent bodyContent
        {
            get
            {
                return this.bodyContentField;
            }
            set
            {
                this.bodyContentField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service")]
    public partial class messageBodyBodyContent
    {

        private string statusField;

        private bool isLockedField;

        private string sessionkeyField;

        private uint confIDField;

        private accessControl accessControlField;

        private metaData metaDataField;

        private telephony telephonyField;

        private host hostField;

        private schedule scheduleField;

        private attendeeOptions attendeeOptionsField;

        private bool isAudioOnlyField;

        private bool telePresenceField;

        private bool isAlternateHostField;

        private bool isCreatorField;

        private string hostKeyField;

        private bool supportE2EField;

        private bool isAllowJBHField;

        private bool isCETMeetingField;

        private bool isPersonalMeetingRoomField;

        private bool isNextUpcomingInstanceField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public string status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public bool isLocked
        {
            get
            {
                return this.isLockedField;
            }
            set
            {
                this.isLockedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public string sessionkey
        {
            get
            {
                return this.sessionkeyField;
            }
            set
            {
                this.sessionkeyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public uint confID
        {
            get
            {
                return this.confIDField;
            }
            set
            {
                this.confIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public accessControl accessControl
        {
            get
            {
                return this.accessControlField;
            }
            set
            {
                this.accessControlField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public metaData metaData
        {
            get
            {
                return this.metaDataField;
            }
            set
            {
                this.metaDataField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public telephony telephony
        {
            get
            {
                return this.telephonyField;
            }
            set
            {
                this.telephonyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public host host
        {
            get
            {
                return this.hostField;
            }
            set
            {
                this.hostField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public schedule schedule
        {
            get
            {
                return this.scheduleField;
            }
            set
            {
                this.scheduleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public attendeeOptions attendeeOptions
        {
            get
            {
                return this.attendeeOptionsField;
            }
            set
            {
                this.attendeeOptionsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public bool isAudioOnly
        {
            get
            {
                return this.isAudioOnlyField;
            }
            set
            {
                this.isAudioOnlyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public bool telePresence
        {
            get
            {
                return this.telePresenceField;
            }
            set
            {
                this.telePresenceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public bool isAlternateHost
        {
            get
            {
                return this.isAlternateHostField;
            }
            set
            {
                this.isAlternateHostField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public bool isCreator
        {
            get
            {
                return this.isCreatorField;
            }
            set
            {
                this.isCreatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public string hostKey
        {
            get
            {
                return this.hostKeyField;
            }
            set
            {
                this.hostKeyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public bool supportE2E
        {
            get
            {
                return this.supportE2EField;
            }
            set
            {
                this.supportE2EField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public bool isAllowJBH
        {
            get
            {
                return this.isAllowJBHField;
            }
            set
            {
                this.isAllowJBHField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public bool isCETMeeting
        {
            get
            {
                return this.isCETMeetingField;
            }
            set
            {
                this.isCETMeetingField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public bool isPersonalMeetingRoom
        {
            get
            {
                return this.isPersonalMeetingRoomField;
            }
            set
            {
                this.isPersonalMeetingRoomField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public bool isNextUpcomingInstance
        {
            get
            {
                return this.isNextUpcomingInstanceField;
            }
            set
            {
                this.isNextUpcomingInstanceField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep", IsNullable = false)]
    public partial class accessControl
    {

        private string sessionPasswordField;

        private string listStatusField;

        private bool registrationField;

        private bool passwordReqField;

        private bool isEnforceAudioPasswordField;

        private bool isEnforceAudioLoginField;

        /// <remarks/>
        public string sessionPassword
        {
            get
            {
                return this.sessionPasswordField;
            }
            set
            {
                this.sessionPasswordField = value;
            }
        }

        /// <remarks/>
        public string listStatus
        {
            get
            {
                return this.listStatusField;
            }
            set
            {
                this.listStatusField = value;
            }
        }

        /// <remarks/>
        public bool registration
        {
            get
            {
                return this.registrationField;
            }
            set
            {
                this.registrationField = value;
            }
        }

        /// <remarks/>
        public bool passwordReq
        {
            get
            {
                return this.passwordReqField;
            }
            set
            {
                this.passwordReqField = value;
            }
        }

        /// <remarks/>
        public bool isEnforceAudioPassword
        {
            get
            {
                return this.isEnforceAudioPasswordField;
            }
            set
            {
                this.isEnforceAudioPasswordField = value;
            }
        }

        /// <remarks/>
        public bool isEnforceAudioLogin
        {
            get
            {
                return this.isEnforceAudioLoginField;
            }
            set
            {
                this.isEnforceAudioLoginField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep", IsNullable = false)]
    public partial class metaData
    {

        private string confNameField;

        private byte sessionTypeField;

        private string serviceTypeField;

        private string agendaField;

        private bool isRecurringField;

        private string agendaFormatField;

        private string descriptionFormatField;

        private bool isExceptionField;

        private byte seriesMeetingKeyField;

        private bool hasExceptionField;

        /// <remarks/>
        public string confName
        {
            get
            {
                return this.confNameField;
            }
            set
            {
                this.confNameField = value;
            }
        }

        /// <remarks/>
        public byte sessionType
        {
            get
            {
                return this.sessionTypeField;
            }
            set
            {
                this.sessionTypeField = value;
            }
        }

        /// <remarks/>
        public string serviceType
        {
            get
            {
                return this.serviceTypeField;
            }
            set
            {
                this.serviceTypeField = value;
            }
        }

        /// <remarks/>
        public string agenda
        {
            get
            {
                return this.agendaField;
            }
            set
            {
                this.agendaField = value;
            }
        }

        /// <remarks/>
        public bool isRecurring
        {
            get
            {
                return this.isRecurringField;
            }
            set
            {
                this.isRecurringField = value;
            }
        }

        /// <remarks/>
        public string agendaFormat
        {
            get
            {
                return this.agendaFormatField;
            }
            set
            {
                this.agendaFormatField = value;
            }
        }

        /// <remarks/>
        public string descriptionFormat
        {
            get
            {
                return this.descriptionFormatField;
            }
            set
            {
                this.descriptionFormatField = value;
            }
        }

        /// <remarks/>
        public bool isException
        {
            get
            {
                return this.isExceptionField;
            }
            set
            {
                this.isExceptionField = value;
            }
        }

        /// <remarks/>
        public byte seriesMeetingKey
        {
            get
            {
                return this.seriesMeetingKeyField;
            }
            set
            {
                this.seriesMeetingKeyField = value;
            }
        }

        /// <remarks/>
        public bool hasException
        {
            get
            {
                return this.hasExceptionField;
            }
            set
            {
                this.hasExceptionField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep", IsNullable = false)]
    public partial class telephony
    {

        private string telephonySupportField;

        private string dialInSequencesField;

        private bool isMPAudioField;

        private bool voipField;

        private bool enableTSPField;

        /// <remarks/>
        public string telephonySupport
        {
            get
            {
                return this.telephonySupportField;
            }
            set
            {
                this.telephonySupportField = value;
            }
        }

        /// <remarks/>
        public string dialInSequences
        {
            get
            {
                return this.dialInSequencesField;
            }
            set
            {
                this.dialInSequencesField = value;
            }
        }

        /// <remarks/>
        public bool isMPAudio
        {
            get
            {
                return this.isMPAudioField;
            }
            set
            {
                this.isMPAudioField = value;
            }
        }

        /// <remarks/>
        public bool voip
        {
            get
            {
                return this.voipField;
            }
            set
            {
                this.voipField = value;
            }
        }

        /// <remarks/>
        public bool enableTSP
        {
            get
            {
                return this.enableTSPField;
            }
            set
            {
                this.enableTSPField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep", IsNullable = false)]
    public partial class host
    {

        private string firstNameField;

        private string lastNameField;

        private string emailField;

        private string webExIdField;

        private bool allowAnyoneHostMeetingField;

        /// <remarks/>
        public string firstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string lastName
        {
            get
            {
                return this.lastNameField;
            }
            set
            {
                this.lastNameField = value;
            }
        }

        /// <remarks/>
        public string email
        {
            get
            {
                return this.emailField;
            }
            set
            {
                this.emailField = value;
            }
        }

        /// <remarks/>
        public string webExId
        {
            get
            {
                return this.webExIdField;
            }
            set
            {
                this.webExIdField = value;
            }
        }

        /// <remarks/>
        public bool allowAnyoneHostMeeting
        {
            get
            {
                return this.allowAnyoneHostMeetingField;
            }
            set
            {
                this.allowAnyoneHostMeetingField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep", IsNullable = false)]
    public partial class schedule
    {

        private string startDateField;

        private byte durationField;

        private string timeZoneField;

        private byte timeZoneIDField;

        private ushort openTimeInSecField;

        /// <remarks/>
        public string startDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }

        /// <remarks/>
        public byte duration
        {
            get
            {
                return this.durationField;
            }
            set
            {
                this.durationField = value;
            }
        }

        /// <remarks/>
        public string timeZone
        {
            get
            {
                return this.timeZoneField;
            }
            set
            {
                this.timeZoneField = value;
            }
        }

        /// <remarks/>
        public byte timeZoneID
        {
            get
            {
                return this.timeZoneIDField;
            }
            set
            {
                this.timeZoneIDField = value;
            }
        }

        /// <remarks/>
        public ushort openTimeInSec
        {
            get
            {
                return this.openTimeInSecField;
            }
            set
            {
                this.openTimeInSecField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep", IsNullable = false)]
    public partial class attendeeOptions
    {

        private bool joinRequiresAccountField;

        /// <remarks/>
        public bool joinRequiresAccount
        {
            get
            {
                return this.joinRequiresAccountField;
            }
            set
            {
                this.joinRequiresAccountField = value;
            }
        }
    }


}
