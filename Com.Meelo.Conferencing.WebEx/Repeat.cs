﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.Conferencing.WebEx
{
    public class Repeat
    {
        public RepeatType RepeatType { get; set; }

        public int? Interval { get; set; }

        public int? AfterMeetingNumber { get; set; }

        public IEnumerable<DayOfWeek> DayInWeek { get; set; }

        public DateTimeOffset? ExpirationDate { get; set; }

        public short? DayInMonth { get; set; }

        public short? WeekInMonth { get; set; }

        public MonthInYear? MonthInYear { get; set; }
    }
}
