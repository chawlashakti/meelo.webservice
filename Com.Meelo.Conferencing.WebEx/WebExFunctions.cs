﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
namespace Com.Meelo.Conferencing.WebEx
{
    /// <summary>
    /// Reference: https://developer.cisco.com/site/webex-developer/develop-test/xml-api/xml-api-reference/
    /// </summary>
    public class WebExFunctions
    {
        public const string XML_START = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"; 

        public WebExFunctions()
        {

        }

        public WebExFunctions(WebExSecurityContext securityContext)
        {
            SecurityContext = securityContext;
        }
        public WebExSecurityContext SecurityContext { get; set; }


        private static readonly XmlSerializer serializer = new XmlSerializer(typeof(Com.Meelo.Conferencing.WebEx.Generic.message));

        private static readonly XmlSerializer meetingSerializer = new XmlSerializer(typeof(Com.Meelo.Conferencing.WebEx.Meeting.message));

        #region XML Generation Functions

        private string BeginDocument()
        {
            return XML_START;
        }
        private string BeginServMessage()
        {
            return "<serv:message xmlns:xsi =\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">";
        }

        private string EndServMessage()
        {
            return "</serv:message>";
        }
        private string GenerateHeader()
        {
            StringBuilder strb = new StringBuilder();
            //strb.Append(XML_START);
            //strb.Append("<serv:message xmlns: xsi =\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">");
            strb.Append("<header><securityContext>");
            strb.AppendFormat("<webExID>{0}</webExID>", SecurityContext.WebExID);
            //if (string.IsNullOrEmpty(Password) && string.IsNullOrEmpty(SessionTicket))
            //    throw new Exception("Both security password and sessionTicket cannot be null.");
            if (!string.IsNullOrEmpty(SecurityContext.Password))
            {
                strb.AppendFormat("<password>{0}</password>", SecurityContext.Password);
            }
            if (!string.IsNullOrEmpty(SecurityContext.SiteName))
            {
                strb.AppendFormat("<siteName>{0}</siteName>", SecurityContext.SiteName);
            }

            if(!string.IsNullOrEmpty(SecurityContext.SiteID))
            {
                strb.AppendFormat("<siteID>{0}</siteID>", SecurityContext.SiteID);
            }

            if (!string.IsNullOrEmpty(SecurityContext.PartnerID))
            {
                strb.AppendFormat("<partnerID>{0}</partnerID>", SecurityContext.PartnerID);
            }

            if (!string.IsNullOrEmpty(SecurityContext.Email))
            {
                strb.AppendFormat("<email>{0}</email>", SecurityContext.Email);
            }
            if (!string.IsNullOrEmpty(SecurityContext.SessionTicket))
            {
                strb.AppendFormat("<sessionTicket>{0}</sessionTicket>", SecurityContext.SessionTicket);
            }

            strb.Append("</securityContext></header>");

            return strb.ToString();
        }

        public string BeginBody(string xsiType)
        {
            StringBuilder strb = new StringBuilder();
            strb.Append("<body>");
            strb.AppendFormat( "<bodyContent xsi:type=\"{0}\">", xsiType);

            return strb.ToString();
        }


        public string EndBody()
        {
            return "</bodyContent></body>";
        }

        #endregion



        #region User Service

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public WebExResponseMessage Authenticate(bool useSSO = true, string samlResponseMsg= "", bool includeRawResponse=false)
        {

            StringBuilder strb = new StringBuilder();
            strb.Append(BeginDocument());
            strb.Append(BeginServMessage());
            strb.Append(GenerateHeader());
            strb.Append(BeginBody("java:com.webex.service.binding.user.AuthenticateUser"));
            if (useSSO)
            {
                strb.AppendFormat("<samlResponse>{0}</samlResponse>", samlResponseMsg);
            }
            strb.Append(EndBody());
            strb.Append(EndServMessage());

          
            string responseFromServer = HttpCall(GetWebExServiceUrl(), strb.ToString());

            WebExResponseMessage wx = new WebExResponseMessage();
            if (includeRawResponse)
            {
                wx.ResponseMessageRaw = responseFromServer;
            }

            if (!string.IsNullOrEmpty(responseFromServer))
            {
                Com.Meelo.Conferencing.WebEx.Generic.message m = serializer.Deserialize(new StringReader(responseFromServer.Replace("xsi:type=\"use:authenticateUserResponse\"", string.Empty))) as Com.Meelo.Conferencing.WebEx.Generic.message;
                wx.ResponseMessage = m;
               

            }

            return wx;
        }

        public WebExResponseMessage GetloginurlUser(string hostWebExID, bool includeRawResponse = false)
        {
            StringBuilder strb = new StringBuilder();
            strb.Append(BeginDocument());
            strb.Append(BeginServMessage());
            strb.Append(GenerateHeader());
            strb.Append(BeginBody("java:com.webex.service.binding.user.GetloginurlUser"));
            strb.AppendFormat("<webExID>{0}</webExID>", hostWebExID);
            strb.Append(EndBody());
            strb.Append(EndServMessage());

            string responseFromServer = HttpCall(GetWebExServiceUrl(), strb.ToString());
            WebExResponseMessage wx = new WebExResponseMessage();
            if (includeRawResponse)
            {
                wx.ResponseMessageRaw = responseFromServer;
            }

            if (!string.IsNullOrEmpty(responseFromServer))
            {
                Com.Meelo.Conferencing.WebEx.Generic.message m = serializer.Deserialize(new StringReader(responseFromServer.Replace("xsi:type=\"use:getloginurlUserResponse\"", string.Empty))) as Com.Meelo.Conferencing.WebEx.Generic.message;

                wx.ResponseMessage = m;

            }

            return wx;
        }

        public WebExResponseMessage GetlogouturlUser(string hostWebExID, bool includeRawResponse = false)
        {
            StringBuilder strb = new StringBuilder();
            strb.Append(BeginDocument());
            strb.Append(BeginServMessage());
            strb.Append(GenerateHeader());
            strb.Append(BeginBody("java:com.webex.service.binding.user.GetlogouturlUser"));
            strb.AppendFormat("<webExID>{0}</webExID>", hostWebExID);
            strb.Append(EndBody());
            strb.Append(EndServMessage());

            string responseFromServer = HttpCall(GetWebExServiceUrl(), strb.ToString());

            WebExResponseMessage wx = new WebExResponseMessage();
            if (includeRawResponse)
            {
                wx.ResponseMessageRaw = responseFromServer;
            }

            if (!string.IsNullOrEmpty(responseFromServer))
            {
                Com.Meelo.Conferencing.WebEx.Generic.message m = serializer.Deserialize(new StringReader(responseFromServer.Replace("xsi:type=\"use:getlogouturlUserResponse\"", string.Empty))) as Com.Meelo.Conferencing.WebEx.Generic.message;

                wx.ResponseMessage = m;

            }

            return wx;
        }

        public WebExResponseMessage GetUser(string hostWebExID, bool includeRawResponse = false)
        {
            StringBuilder strb = new StringBuilder();
            strb.Append(BeginDocument());
            strb.Append(BeginServMessage());
            strb.Append(GenerateHeader());
            strb.Append(BeginBody("java:com.webex.service.binding.user.GetUser"));
            // strb.AppendFormat("<webExID>{0}</webExID>", WebExID);
            strb.AppendFormat("<webExId>{0}</webExId>", hostWebExID);
            strb.Append(EndBody());
            strb.Append(EndServMessage());

            string responseFromServer = HttpCall(GetWebExServiceUrl(), strb.ToString());

            WebExResponseMessage wx = new WebExResponseMessage();
            if (includeRawResponse)
            {
                wx.ResponseMessageRaw = responseFromServer;
            }

            if (!string.IsNullOrEmpty(responseFromServer))
            {
                Com.Meelo.Conferencing.WebEx.Generic.message m = serializer.Deserialize(new StringReader(responseFromServer.Replace("xsi:type=\"use:getUserResponse\"", string.Empty))) as Com.Meelo.Conferencing.WebEx.Generic.message;
                wx.ResponseMessage = m;


            }

            return wx;
        }

        public WebExResponseMessage UploadPMRImage(byte[] imageData, bool includeRawResponse = false)
        {
            StringBuilder strb = new StringBuilder();
            strb.Append(BeginDocument());
            strb.Append(BeginServMessage());
            strb.Append(GenerateHeader());
            strb.Append(BeginBody("java:com.webex.service.binding.user.UploadPMRImage"));
                strb.AppendFormat("<imageFor>Photo</imageFor>");
            strb.AppendFormat("<imageData>{0}</imageData>", Convert.ToBase64String(imageData));
            strb.Append(EndBody());
            strb.Append(EndServMessage());

            string responseFromServer = HttpCall(GetWebExServiceUrl(), strb.ToString());

            WebExResponseMessage wx = new WebExResponseMessage();
            if (includeRawResponse)
            {
                wx.ResponseMessageRaw = responseFromServer;
            }

            if (!string.IsNullOrEmpty(responseFromServer))
            {
                Com.Meelo.Conferencing.WebEx.Generic.message m = serializer.Deserialize(new StringReader(responseFromServer.Replace("xsi:type=\"use:uploadPMRImageResponse\"", string.Empty))) as Com.Meelo.Conferencing.WebEx.Generic.message;

                wx.ResponseMessage = m;

            }

            return wx;
        }
        #endregion

       


        #region General Session Service
        public WebExResponseMessage GetAPIVersion(bool includeRawResponse = false)
        {
            StringBuilder strb = new StringBuilder();
            strb.Append(BeginDocument());
            strb.Append(BeginServMessage());
            strb.Append(GenerateHeader());
            strb.Append(BeginBody("java:com.webex.service.binding.ep.GetAPIVersion"));
            strb.Append(EndBody());
            strb.Append(EndServMessage());

            string responseFromServer = HttpCall(GetWebExServiceUrl(), strb.ToString());
            WebExResponseMessage wx = new WebExResponseMessage();
            if (includeRawResponse)
            {
                wx.ResponseMessageRaw = responseFromServer;
            }
            if (!string.IsNullOrEmpty(responseFromServer))
            {
                Com.Meelo.Conferencing.WebEx.Generic.message m = serializer.Deserialize(new StringReader(responseFromServer.Replace("xsi:type=\"ep:getAPIVersionResponse\"", string.Empty))) as Com.Meelo.Conferencing.WebEx.Generic.message;
                wx.ResponseMessage = m;


            }

            return  wx;
        }


        public WebExResponseMessage DelRecording(string recordingID, bool includeRawResponse = false)
        {

            StringBuilder strb = new StringBuilder();
            strb.Append(BeginDocument());
            strb.Append(BeginServMessage());
            strb.Append(GenerateHeader());
            strb.Append(BeginBody("java:com.webex.service.binding.ep.DelRecording"));
           
                strb.AppendFormat("<recordingID>{0}</recordingID>", recordingID);
            
            strb.Append(EndBody());
            strb.Append(EndServMessage());

            string responseFromServer = HttpCall(GetWebExServiceUrl(), strb.ToString());

            WebExResponseMessage wx = new WebExResponseMessage();
            if (includeRawResponse)
            {
                wx.ResponseMessageRaw = responseFromServer;
            }

            if (!string.IsNullOrEmpty(responseFromServer))
            {
                Com.Meelo.Conferencing.WebEx.Generic.message m = serializer.Deserialize(new StringReader(responseFromServer.Replace("xsi:type=\"ep:delRecordingResponse\"", string.Empty))) as Com.Meelo.Conferencing.WebEx.Generic.message;

                wx.ResponseMessage = m;

            }

            return wx;
        }


        public WebExResponseMessage DelSession(string sessionKey, bool includeRawResponse = false)
        {

            StringBuilder strb = new StringBuilder();
            strb.Append(BeginDocument());
            strb.Append(BeginServMessage());
            strb.Append(GenerateHeader());
            strb.Append(BeginBody("java:com.webex.service.binding.ep.DelSession"));

            strb.AppendFormat("<sessionKey>{0}</sessionKey>", sessionKey);

            strb.Append(EndBody());
            strb.Append(EndServMessage());

            string responseFromServer = HttpCall(GetWebExServiceUrl(), strb.ToString());

            WebExResponseMessage wx = new WebExResponseMessage();
            if (includeRawResponse)
            {
                wx.ResponseMessageRaw = responseFromServer;
            }

            if (!string.IsNullOrEmpty(responseFromServer))
            {
                Com.Meelo.Conferencing.WebEx.Generic.message m = serializer.Deserialize(new StringReader(responseFromServer.Replace("xsi:type=\"ep:DelSessionResponse\"", string.Empty))) as Com.Meelo.Conferencing.WebEx.Generic.message;


                wx.ResponseMessage = m;
            }

            return wx;
        }

        public WebExResponseMessage LstRecording(FilterInput filter, bool returnSessionDetail = true, string hostWebExID = null, string sessionKey = null, bool includeRawResponse = false)
        {
            StringBuilder strb = new StringBuilder();
            strb.Append(BeginDocument());
            strb.Append(BeginServMessage());
            strb.Append(GenerateHeader());
            strb.Append(BeginBody("java:com.webex.service.binding.ep.LstRecording"));

            if (filter == null)
                filter = new FilterInput();

            strb.Append(filter.GetListControlXml());

            if (filter.DateScope != null)
            {
                strb.Append(filter.DateScope.ToCreateTimeScopeXml());
            }

            //<orderAD>DESC</orderAD>
            if (!string.IsNullOrEmpty(sessionKey))
            {
                strb.AppendFormat("<sessionKey>{0}</sessionKey>", sessionKey);
            }

            if (string.IsNullOrEmpty(hostWebExID))
                hostWebExID = SecurityContext.WebExID;

            strb.AppendFormat("<hostWebExID>{0}</hostWebExID>", hostWebExID);
            strb.AppendFormat("<returnSessionDetails>{0}</returnSessionDetails>", returnSessionDetail);


            strb.Append(EndBody());
            strb.Append(EndServMessage());

            string responseFromServer = HttpCall(GetWebExServiceUrl(), strb.ToString());

            WebExResponseMessage wx = new WebExResponseMessage();
            if (includeRawResponse)
            {
                wx.ResponseMessageRaw = responseFromServer;
            }

            if (!string.IsNullOrEmpty(responseFromServer))
            {
                Com.Meelo.Conferencing.WebEx.Generic.message m = serializer.Deserialize(new StringReader(responseFromServer.Replace("xsi:type=\"ep:lstRecordingResponse\"", string.Empty))) as Com.Meelo.Conferencing.WebEx.Generic.message;

                wx.ResponseMessage = m;

            }

            return wx;
        }

        public WebExResponseMessage LstsummarySession(FilterInput filter, int[] sessionTypes=null, string[] serviceTypes=null, bool? invited=null, bool includeRawResponse = false)
        {
            StringBuilder strb = new StringBuilder();
            strb.Append(BeginDocument());
            strb.Append(BeginServMessage());
            strb.Append(GenerateHeader());
            strb.Append(BeginBody("java:com.webex.service.binding.ep.LstsummarySession"));

            if (filter == null)
                filter = new FilterInput();

            strb.Append(filter.GetListControlXml());
            //<orderAD>DESC</orderAD>

            //<orderAD>DESC</orderAD>
            strb.Append(filter.GetResultSortOrderXml());
            

            if (filter.DateScope != null)
            {
                strb.Append(filter.DateScope.ToDateScopeXml());
            }

            if(sessionTypes != null && sessionTypes.Length > 0)
            {
                strb.Append("<sessionTypes>");
                foreach(int s in sessionTypes)
                {
                    strb.AppendFormat("<sessionType>{0}</sessionType>", s);
                }
                strb.Append("</sessionTypes>");

            }

            if (serviceTypes != null && serviceTypes.Length > 0)
            {
                strb.Append("<serviceTypes>");
                foreach (string s in serviceTypes)
                {
                    strb.AppendFormat("<serviceType>{0}</serviceType>", s);
                }
                strb.Append("</serviceTypes>");

            }

            if (invited != null)
            {
                strb.AppendFormat("<invited>{0}</invited>", invited);
            }
            strb.Append(EndBody());
            strb.Append(EndServMessage());

            string responseFromServer = HttpCall(GetWebExServiceUrl(), strb.ToString());
            WebExResponseMessage wx = new WebExResponseMessage();
            if (includeRawResponse)
            {
                wx.ResponseMessageRaw = responseFromServer;
            }
            if (!string.IsNullOrEmpty(responseFromServer))
            {
                Com.Meelo.Conferencing.WebEx.Generic.message m = serializer.Deserialize(new StringReader(responseFromServer.Replace("xsi:type=\"ep:lstsummarySessionResponse\"", string.Empty))) as Com.Meelo.Conferencing.WebEx.Generic.message;

                wx.ResponseMessage = m;

            }

            return wx;
        }

        public WebExResponseMessage GetOneClickSettings(string hostWebExID, bool includeRawResponse = false)
        {

            StringBuilder strb = new StringBuilder();
            strb.Append(BeginDocument());
            strb.Append(BeginServMessage());
            strb.Append(GenerateHeader());
            strb.Append(BeginBody("java:com.webex.service.binding.ep.GetOneClickSettings"));
            if(string.IsNullOrEmpty(hostWebExID))
            {
                hostWebExID = SecurityContext.WebExID;
            }
            strb.AppendFormat("<hostWebExID>{0}</hostWebExID>", hostWebExID);

            strb.Append(EndBody());
            strb.Append(EndServMessage());

            string responseFromServer = HttpCall(GetWebExServiceUrl(), strb.ToString());

            WebExResponseMessage wx = new WebExResponseMessage();
            if (includeRawResponse)
            {
                wx.ResponseMessageRaw = responseFromServer;
            }

            if (!string.IsNullOrEmpty(responseFromServer))
            {
                Com.Meelo.Conferencing.WebEx.Generic.message m = serializer.Deserialize(new StringReader(responseFromServer.Replace("xsi:type=\"ep:getOneClickSettingsResponse\"", string.Empty))) as Com.Meelo.Conferencing.WebEx.Generic.message;


                wx.ResponseMessage = m;
            }

            return wx;
        }

        public WebExResponseMessage SetOneClickSettings(string hostWebExID, string serviceType="MeetingCenter", int sessionType=1, bool includeRawResponse = false)
        {

            StringBuilder strb = new StringBuilder();
            strb.Append(BeginDocument());
            strb.Append(BeginServMessage());
            strb.Append(GenerateHeader());
            strb.Append(BeginBody("java:com.webex.service.binding.ep.SetOneClickSettings"));
            if (string.IsNullOrEmpty(hostWebExID))
            {
                hostWebExID = SecurityContext.WebExID;
            }
            strb.AppendFormat("<hostWebExID>{0}</hostWebExID>", hostWebExID);
            strb.Append("<oneClickMetaData>");
            strb.AppendFormat("<serviceType>{0}</serviceType>",serviceType );
            strb.AppendFormat("<sessionType>{0}</sessionType>", sessionType);
            strb.Append("</oneClickMetaData>");
            strb.Append(EndBody());
            strb.Append(EndServMessage());

            string responseFromServer = HttpCall(GetWebExServiceUrl(), strb.ToString());

            WebExResponseMessage wx = new WebExResponseMessage();
            if (includeRawResponse)
            {
                wx.ResponseMessageRaw = responseFromServer;
            }

            if (!string.IsNullOrEmpty(responseFromServer))
            {
                Com.Meelo.Conferencing.WebEx.Generic.message m = serializer.Deserialize(new StringReader(responseFromServer.Replace("xsi:type=\"ep:setOneClickSettingsResponse\"", string.Empty))) as Com.Meelo.Conferencing.WebEx.Generic.message;

                wx.ResponseMessage = m;

            }

            return wx;
        }

        public WebExResponseMessage GetRecordingInfo(string recordingID, bool isServiceRecording=false, bool includeRawResponse = false)
        {

            StringBuilder strb = new StringBuilder();
            strb.Append(BeginDocument());
            strb.Append(BeginServMessage());
            strb.Append(GenerateHeader());
            strb.Append(BeginBody("java:com.webex.service.binding.ep.GetRecordingInfo"));
         
            strb.AppendFormat("<recordingID>{0}</recordingID>", recordingID);
            strb.AppendFormat("<isServiceRecording>{0}</isServiceRecording>", isServiceRecording);

            strb.Append(EndBody());
            strb.Append(EndServMessage());

            string responseFromServer = HttpCall(GetWebExServiceUrl(), strb.ToString());

            WebExResponseMessage wx = new WebExResponseMessage();
            if (includeRawResponse)
            {
                wx.ResponseMessageRaw = responseFromServer;
            }

            if (!string.IsNullOrEmpty(responseFromServer))
            {
                Com.Meelo.Conferencing.WebEx.Generic.message m = serializer.Deserialize(new StringReader(responseFromServer.Replace("xsi:type=\"ep:getRecordingInfoResponse\"", string.Empty))) as Com.Meelo.Conferencing.WebEx.Generic.message;

                wx.ResponseMessage = m;

            }

            return wx;
        }

        public WebExResponseMessage GetSessionInfo(string sessionPassword=null, string sessionKey=null, bool includeRawResponse = false)
        {

            StringBuilder strb = new StringBuilder();
            strb.Append(BeginDocument());
            strb.Append(BeginServMessage());
            strb.Append(GenerateHeader());
            strb.Append(BeginBody("java:com.webex.service.binding.ep.GetSessionInfo"));
            if(!string.IsNullOrEmpty(sessionPassword))
            {
                strb.AppendFormat("<sessionPassword>{0}</sessionPassword>", sessionPassword);
            }

            if(!string.IsNullOrEmpty(sessionKey))
            {
                strb.AppendFormat("<sessionKey>{0}</sessionKey>", sessionKey);
            }
            
           

            strb.Append(EndBody());
            strb.Append(EndServMessage());

            string responseFromServer = HttpCall(GetWebExServiceUrl(), strb.ToString());

            WebExResponseMessage wx = new WebExResponseMessage();
            if (includeRawResponse)
            {
                wx.ResponseMessageRaw = responseFromServer;
            }

            if (!string.IsNullOrEmpty(responseFromServer))
            {
                Com.Meelo.Conferencing.WebEx.Generic.message m = serializer.Deserialize(new StringReader(responseFromServer.Replace("xsi:type=\"ep:getSessionInfoResponse\"", string.Empty))) as Com.Meelo.Conferencing.WebEx.Generic.message;

                wx.ResponseMessage = m;

            }

            return wx;
        }

        public WebExResponseMessage SetRecordingInfo(SetRecordingInfoInput recordingDetails, bool includeRawResponse = false)
        {

            StringBuilder strb = new StringBuilder();
            strb.Append(BeginDocument());
            strb.Append(BeginServMessage());
            strb.Append(GenerateHeader());
            strb.Append(BeginBody("java:com.webex.service.binding.ep.SetRecordingInfo"));

            strb.Append("<recording>");
            strb.AppendFormat("<recordingID>{0}</recordingID>", recordingDetails.Recording.recordingID);

            if(!string.IsNullOrEmpty(recordingDetails.Recording.description))
            {
                strb.AppendFormat("<description>{0}</description>", recordingDetails.Recording.description);
            }

            if (!string.IsNullOrEmpty(recordingDetails.Recording.password))
            {
                strb.AppendFormat("<password>{0}</password>", recordingDetails.Recording.password);
            }

            strb.Append("</recording>");

            if (recordingDetails.IsServiceRecording != null)
            {
                strb.AppendFormat("<isServiceRecording>{0}</isServiceRecording>", recordingDetails.IsServiceRecording);
            }

            if (recordingDetails.Basic != null)
            {
                strb.Append("<basic>");

                if(!string.IsNullOrEmpty(recordingDetails.Basic.agenda))
                {
                    strb.AppendFormat("<agenda>{0}</agenda>", recordingDetails.Basic.agenda);
                }

                if (!string.IsNullOrEmpty(recordingDetails.Basic.listing))
                {
                    strb.AppendFormat("<listing>{0}</listing>", recordingDetails.Basic.listing);
                }

                if (!string.IsNullOrEmpty(recordingDetails.Basic.presenter))
                {
                    strb.AppendFormat("<presenter>{0}</presenter>", recordingDetails.Basic.presenter);
                }

                if (!string.IsNullOrEmpty(recordingDetails.Basic.topic))
                {
                    strb.AppendFormat("<topic>{0}</topic>", recordingDetails.Basic.topic);
                }

                strb.AppendFormat("<email>{0}</email>", recordingDetails.Basic.email);
                strb.Append("</basic>");

            }

            if (recordingDetails.Playback != null)
            {
                strb.Append("<playback>");
                if (recordingDetails.Playback.chat != null)
                {
                    strb.AppendFormat("<chat>{0}</chat>", recordingDetails.Playback.chat);
                   

                }

                if (recordingDetails.Playback.supportQandA != null)
                {
                    strb.AppendFormat("<supportQandA>{0}</supportQandA>", recordingDetails.Playback.supportQandA);


                }

                if (recordingDetails.Playback.video != null)
                {
                    strb.AppendFormat("<video>{0}</video>", recordingDetails.Playback.video);


                }

                if (recordingDetails.Playback.polling != null)
                {
                    strb.AppendFormat("<polling>{0}</polling>", recordingDetails.Playback.polling);


                }

                if (recordingDetails.Playback.notes != null)
                {
                    strb.AppendFormat("<notes>{0}</notes>", recordingDetails.Playback.notes);


                }

                if (recordingDetails.Playback.fileShare != null)
                {
                    strb.AppendFormat("<fileShare>{0}</fileShare>", recordingDetails.Playback.fileShare);


                }

                if (recordingDetails.Playback.toc != null)
                {
                    strb.AppendFormat("<toc>{0}</toc>", recordingDetails.Playback.toc);


                }

                if (recordingDetails.Playback.attendeeList != null)
                {
                    strb.AppendFormat("<attendeeList>{0}</attendeeList>", recordingDetails.Playback.attendeeList);


                }

                if (!string.IsNullOrEmpty(recordingDetails.Playback.range))
                {
                    strb.AppendFormat("<range>{0}</range>", recordingDetails.Playback.range);
                }

                if (recordingDetails.Playback.partialStart != null)
                {
                    strb.AppendFormat("<partialStart>{0}</partialStart>", recordingDetails.Playback.partialStart);


                }

                if (recordingDetails.Playback.partialEnd != null)
                {
                    strb.AppendFormat("<partialEnd>{0}</partialEnd>", recordingDetails.Playback.partialEnd);


                }
                strb.Append("</playback>");
            }

            if (recordingDetails.FileAccess != null)
            {
                strb.Append("<fileAccess>");


                if (!string.IsNullOrEmpty(recordingDetails.FileAccess.endPlayURL))
                {
                    strb.AppendFormat("<endPlayURL>{0}</endPlayURL>", recordingDetails.FileAccess.endPlayURL);
                }

                if (recordingDetails.FileAccess.registration != null)
                {
                    strb.AppendFormat("<registration>{0}</registration>", recordingDetails.FileAccess.registration);


                }

                if (recordingDetails.FileAccess.attendeeView != null)
                {
                    strb.AppendFormat("<attendeeView>{0}</attendeeView>", recordingDetails.FileAccess.attendeeView);


                }

                if (recordingDetails.FileAccess.attendeeDownload != null)
                {
                    strb.AppendFormat("<attendeeDownload>{0}</attendeeDownload>", recordingDetails.FileAccess.attendeeDownload);


                }

                strb.Append("</fileAccess>");
            }

            strb.Append(EndBody());
            strb.Append(EndServMessage());

            string responseFromServer = HttpCall(GetWebExServiceUrl(), strb.ToString());

            WebExResponseMessage wx = new WebExResponseMessage();
            if (includeRawResponse)
            {
                wx.ResponseMessageRaw = responseFromServer;
            }

            if (!string.IsNullOrEmpty(responseFromServer))
            {
                Com.Meelo.Conferencing.WebEx.Generic.message m = serializer.Deserialize(new StringReader(responseFromServer.Replace("xsi:type=\"ep:setRecordingInfoResponse\"", string.Empty))) as Com.Meelo.Conferencing.WebEx.Generic.message;
                wx.ResponseMessage = m;


            }

            return wx;
        }
        #endregion


        #region Meeting Service

        //Meeting Types
        //FRE //(Free meeting)
        //STD //(Standard meeting)
        //PRO //(Pro meeting)
        //SOS //(Standard subscription office meeting)
        //SOP //(Pro subscription office meeting)
        //PPU //(Pay-per-use meeting)
        //OCS //(OnCall support session)
        //COB //(OnTour Session)
        //ONS //(OnStage meeting)
        //RAS //(Access Anywhere session)
        //TRS //(Training session)
        //SC  //(Support Center Session)
        //SMT //(SMARTtech session)
        //SAC //(Sales session)
        //AUO //(Teleconference Only meeting)


        public WebExResponseMessage LstsummaryMeeting(FilterInput filter, string hostWebExID=null, string meetingKey=null, bool includeRawResponse=false)
        {
            StringBuilder strb = new StringBuilder();
            strb.Append(BeginDocument());
            strb.Append(BeginServMessage());
            strb.Append(GenerateHeader());
            strb.Append(BeginBody("java:com.webex.service.binding.meeting.LstsummaryMeeting"));


            if (!string.IsNullOrEmpty(meetingKey))
            {
                strb.AppendFormat("<meetingKey>{0}</meetingKey>", meetingKey);
            }

            if (!string.IsNullOrEmpty(hostWebExID))
            {
                strb.AppendFormat("<hostWebExID>{0}</hostWebExID>", hostWebExID);
            }

         
            if (filter == null)
                filter = new FilterInput();

            strb.Append(filter.GetListControlXml());
            //<orderAD>DESC</orderAD>

            //<orderAD>DESC</orderAD>
            strb.Append(filter.GetResultSortOrderXml());


            if (filter.DateScope == null)
            {
                filter.DateScope = new DateScope()
                {
                    StartDateStart = DateTimeOffset.Now,
                    StartDateEnd = DateTimeOffset.Now.AddHours(24),
                    EndDateStart = DateTimeOffset.Now,
                    EndDateEnd = DateTimeOffset.Now.AddHours(24)
                };
            }
            strb.Append(filter.DateScope.ToDateScopeXml());

            strb.Append(EndBody());
            strb.Append(EndServMessage());

            string responseFromServer = HttpCall(GetWebExServiceUrl(), strb.ToString());

            WebExResponseMessage wx = new WebExResponseMessage();
            if (includeRawResponse)
            {
                wx.ResponseMessageRaw = responseFromServer;
            }

            if (!string.IsNullOrEmpty(responseFromServer))
            {
                Com.Meelo.Conferencing.WebEx.Generic.message m = serializer.Deserialize(new StringReader(responseFromServer.Replace("xsi:type=\"meet:lstsummaryMeetingResponse\"", string.Empty))) as Com.Meelo.Conferencing.WebEx.Generic.message;

                wx.ResponseMessage = m;



            }

            return wx;
        }


        public WebExResponseMessage CreateMeeting(CreateMeetingInput meetingInput, bool includeRawResponse = false)
        {

            StringBuilder strb = new StringBuilder();
            strb.Append(BeginDocument());
            strb.Append(BeginServMessage());
            strb.Append(GenerateHeader());
            strb.Append(BeginBody("java:com.webex.service.binding.meeting.CreateMeeting"));

            if(meetingInput.AccessControl != null && !string.IsNullOrEmpty(meetingInput.AccessControl.meetingPassword))
            {
                strb.Append("<accessControl>");
                strb.AppendFormat("<meetingPassword>{0}</meetingPassword>", meetingInput.AccessControl.meetingPassword);
                strb.Append("</accessControl>");
            }

            if(meetingInput.Metadata != null)
            {
                strb.Append("<metaData>");
                strb.AppendFormat("<confName>{0}</confName>", meetingInput.Metadata.confName);
                if(meetingInput.Metadata.meetingType != null)
                {
                    strb.AppendFormat("<meetingType>{0}</meetingType>", meetingInput.Metadata.meetingType);
                }

                if(!string.IsNullOrEmpty(meetingInput.Metadata.agenda))
                {
                    strb.AppendFormat("<agenda>{0}</agenda>", meetingInput.Metadata.agenda);
                }
                strb.Append("</metaData>");
            }

           if(meetingInput.Attendees != null && meetingInput.Attendees.Count() > 0)
            {
                strb.Append("<participants>");
                if (meetingInput.MaxUserNumber != null)
                {
                    strb.AppendFormat("<maxUserNumber>{0}</maxUserNumber>", meetingInput.MaxUserNumber);
                }

                strb.Append("<attendees>");
                foreach(Person p in meetingInput.Attendees)
                {
                    strb.Append("<attendee>");
                    strb.Append("<person>");

                    if(!string.IsNullOrEmpty(p.Name))
                    {
                        strb.AppendFormat("<name>{0}</name>", p.Name);
                    }

                    if(!string.IsNullOrEmpty(p.Email))
                    {
                        strb.AppendFormat("<email>{0}</email>", p.Email);
                    }
                    strb.Append("</person>");

                    strb.Append("</attendee>");
                }

                strb.Append("</attendees>");
                strb.Append("</participants>");
            }

           if(meetingInput.EnableOptions != null)
            {
                strb.Append("<enableOptions>");

                if(meetingInput.EnableOptions.chat != null)
                {
                    strb.AppendFormat("<chat>{0}</chat>", meetingInput.EnableOptions.chat);
                }

                if(meetingInput.EnableOptions.audioVideo != null)
                {
                    strb.AppendFormat("<audioVideo>{0}</audioVideo>", meetingInput.EnableOptions.audioVideo);
                }

                if(meetingInput.EnableOptions.poll != null)
                {
                    strb.AppendFormat("<poll>{0}</poll>", meetingInput.EnableOptions.poll);
                }
                strb.Append("</enableOptions>");

            }

           if(meetingInput.Schedule != null)
            {
                strb.Append("<schedule>");

                
                    strb.AppendFormat("<startDate>{0}</startDate>", meetingInput.Schedule.startDate);
                

                if (meetingInput.Schedule.openTime != null)
                {
                    strb.AppendFormat("<openTime>{0}</openTime>", meetingInput.Schedule.openTime);
                }

                if (meetingInput.Schedule.joinTeleconfBeforeHost != null)
                {
                    strb.AppendFormat("<joinTeleconfBeforeHost>{0}</joinTeleconfBeforeHost>", meetingInput.Schedule.joinTeleconfBeforeHost);
                }

                if (meetingInput.Schedule.duration != null)
                {
                    strb.AppendFormat("<duration>{0}</duration>", meetingInput.Schedule.duration);
                }

                if (meetingInput.Schedule.timeZoneID != null)
                {
                    strb.AppendFormat("<timeZoneID>{0}</timeZoneID>", meetingInput.Schedule.timeZoneID);
                }
                strb.Append("</schedule>");
            }

           if(meetingInput.Telephony != null)
            {
                strb.Append("<telephony>");

                if(!string.IsNullOrEmpty(meetingInput.Telephony.telephonySupport))
                {
                    strb.AppendFormat("<telephonySupport>{0}</telephonySupport>", meetingInput.Telephony.telephonySupport);
                }

                if (!string.IsNullOrEmpty(meetingInput.Telephony.extTelephonyDescription))
                {
                    strb.AppendFormat("<extTelephonyDescription>{0}</extTelephonyDescription>", meetingInput.Telephony.extTelephonyDescription);
                }

                strb.Append("</telephony>");
            }

            if (meetingInput.Repeat != null )
            {
                strb.Append(Recurrence.GetRecurrenceXml(meetingInput.Repeat));
            }

            if (meetingInput.AttendeeOptions != null)
            {
                strb.Append("<attendeeOptions>");

                if (meetingInput.AttendeeOptions.auto != null)
                {
                    strb.AppendFormat("<auto>{0}</auto>", meetingInput.AttendeeOptions.auto);
                }

                if (meetingInput.AttendeeOptions.excludePassword != null)
                {
                    strb.AppendFormat("<excludePassword>{0}</excludePassword>", meetingInput.AttendeeOptions.excludePassword);
                }

                if (meetingInput.AttendeeOptions.joinApproval != null)
                {
                    strb.AppendFormat("<joinApproval>{0}</joinApproval>", meetingInput.AttendeeOptions.joinApproval);
                }

                if (meetingInput.AttendeeOptions.joinRequiresAccount != null)
                {
                    strb.AppendFormat("<joinRequiresAccount>{0}</joinRequiresAccount>", meetingInput.AttendeeOptions.joinRequiresAccount);
                }

                if (meetingInput.AttendeeOptions.registration != null)
                {
                    strb.AppendFormat("<registration>{0}</registration>", meetingInput.AttendeeOptions.registration);
                }

                if (meetingInput.AttendeeOptions.request != null)
                {
                    strb.AppendFormat("<request>{0}</request>", meetingInput.AttendeeOptions.request);
                }

                if (meetingInput.AttendeeOptions.emailInvitations != null)
                {
                    strb.AppendFormat("<emailInvitations>{0}</emailInvitations>", meetingInput.AttendeeOptions.emailInvitations);
                }
                strb.Append("</attendeeOptions>");

            }
            strb.Append(EndBody());
            strb.Append(EndServMessage());

            string responseFromServer = HttpCall(GetWebExServiceUrl(), strb.ToString());

            WebExResponseMessage wx = new WebExResponseMessage();
            if (includeRawResponse)
            {
                wx.ResponseMessageRaw = responseFromServer;
            }

            if (!string.IsNullOrEmpty(responseFromServer))
            {
                Com.Meelo.Conferencing.WebEx.Generic.message m = serializer.Deserialize(new StringReader(responseFromServer.Replace("xsi:type=\"meet:createMeetingResponse\"", string.Empty))) as Com.Meelo.Conferencing.WebEx.Generic.message;

                wx.ResponseMessage = m;

            }

            return wx;
        }


        public WebExResponseMessage DeleteMeeting(string meetingKey, bool includeRawResponse = false)
        {
            StringBuilder strb = new StringBuilder();
            strb.Append(BeginDocument());
            strb.Append(BeginServMessage());
            strb.Append(GenerateHeader());
            strb.Append(BeginBody("java:com.webex.service.binding.meeting.DelMeeting"));
         
           
                strb.AppendFormat("<meetingKey>{0}</meetingKey>", meetingKey);
            
         
            strb.Append(EndBody());
            strb.Append(EndServMessage());

            string responseFromServer = HttpCall(GetWebExServiceUrl(), strb.ToString());

            WebExResponseMessage wx = new WebExResponseMessage();
            if (includeRawResponse)
            {
                wx.ResponseMessageRaw = responseFromServer;
            }

            if (!string.IsNullOrEmpty(responseFromServer))
            {
                Com.Meelo.Conferencing.WebEx.Generic.message m = serializer.Deserialize(new StringReader(responseFromServer.Replace("xsi:type=\"meet:delMeetingResponse\"", string.Empty))) as Com.Meelo.Conferencing.WebEx.Generic.message;

                wx.ResponseMessage = m;

            }

            return wx;
        }

        public WebExResponseMessage GethosturlMeeting(string sessionKey, bool includeRawResponse = false)
        {
            StringBuilder strb = new StringBuilder();
            strb.Append(BeginDocument());
            strb.Append(BeginServMessage());
            strb.Append(GenerateHeader());
            strb.Append(BeginBody("java:com.webex.service.binding.meeting.GethosturlMeeting"));


            strb.AppendFormat("<sessionKey>{0}</sessionKey>", sessionKey);


            strb.Append(EndBody());
            strb.Append(EndServMessage());

            string responseFromServer = HttpCall(GetWebExServiceUrl(), strb.ToString());

            WebExResponseMessage wx = new WebExResponseMessage();
            if (includeRawResponse)
            {
                wx.ResponseMessageRaw = responseFromServer;
            }

            if (!string.IsNullOrEmpty(responseFromServer))
            {
                Com.Meelo.Conferencing.WebEx.Generic.message m = serializer.Deserialize(new StringReader(responseFromServer.Replace("xsi:type=\"meet:gethosturlMeetingResponse\"", string.Empty))) as Com.Meelo.Conferencing.WebEx.Generic.message;

                wx.ResponseMessage = m;

            }

            return wx;
        }

        public WebExResponseMessage GetjoinurlMeeting(string sessionKey, string attendeeName, string attendeeEmail=null, string meetingPW=null, string RegID=null, bool includeRawResponse = false)
        {
            StringBuilder strb = new StringBuilder();
            strb.Append(BeginDocument());
            strb.Append(BeginServMessage());
            strb.Append(GenerateHeader());
            strb.Append(BeginBody("java:com.webex.service.binding.meeting.GetjoinurlMeeting"));


            strb.AppendFormat("<sessionKey>{0}</sessionKey>", sessionKey);

            if(!string.IsNullOrEmpty(attendeeName))
            {
                strb.AppendFormat("<attendeeName>{0}</attendeeName>", attendeeName);
            }

            if (!string.IsNullOrEmpty(attendeeEmail))
            {
                strb.AppendFormat("<attendeeEmail>{0}</attendeeEmail>", attendeeEmail);
            }

            if (!string.IsNullOrEmpty(meetingPW))
            {
                strb.AppendFormat("<meetingPW>{0}</meetingPW>", meetingPW);
            }

            if (!string.IsNullOrEmpty(RegID))
            {
                strb.AppendFormat("<RegID>{0}</RegID>", RegID);
            }

            strb.Append(EndBody());
            strb.Append(EndServMessage());

            string responseFromServer = HttpCall(GetWebExServiceUrl(), strb.ToString());

            WebExResponseMessage wx = new WebExResponseMessage();
            if (includeRawResponse)
            {
                wx.ResponseMessageRaw = responseFromServer;
            }

            if (!string.IsNullOrEmpty(responseFromServer))
            {
                Com.Meelo.Conferencing.WebEx.Generic.message m = serializer.Deserialize(new StringReader(responseFromServer.Replace("xsi:type=\"meet:getjoinurlMeetingResponse\"", string.Empty))) as Com.Meelo.Conferencing.WebEx.Generic.message;

                wx.ResponseMessage = m;

            }

            return wx;
        }

        public WebExResponseMessage GetMeeting(string meetingKey, bool includeRawResponse = false)
        {
            StringBuilder strb = new StringBuilder();
            strb.Append(BeginDocument());
            strb.Append(BeginServMessage());
            strb.Append(GenerateHeader());
            strb.Append(BeginBody("java:com.webex.service.binding.meeting.GetMeeting"));


            strb.AppendFormat("<meetingKey>{0}</meetingKey>", meetingKey);


            strb.Append(EndBody());
            strb.Append(EndServMessage());

            string responseFromServer = HttpCall(GetWebExServiceUrl(), strb.ToString());

            WebExResponseMessage wx = new WebExResponseMessage();
            if (includeRawResponse)
            {
                wx.ResponseMessageRaw = responseFromServer;
            }

            if (!string.IsNullOrEmpty(responseFromServer))
            {
               
                Com.Meelo.Conferencing.WebEx.Meeting.message m = meetingSerializer.Deserialize(new StringReader(responseFromServer.Replace("xsi:type=\"meet:getMeetingResponse\"", string.Empty))) as Com.Meelo.Conferencing.WebEx.Meeting.message;

                wx.GetMeetingResponseMessage = m;

            }

            return wx;
        }

        public WebExResponseMessage SetMeeting(string meetingKey, CreateMeetingInput meetingInput, bool includeRawResponse = false)
        {

            StringBuilder strb = new StringBuilder();
            strb.Append(BeginDocument());
            strb.Append(BeginServMessage());
            strb.Append(GenerateHeader());
            strb.Append(BeginBody("java:com.webex.service.binding.meeting.SetMeeting"));

            strb.AppendFormat("<meetingkey>{0}</meetingkey>", meetingKey);

            if (meetingInput.AccessControl != null && !string.IsNullOrEmpty(meetingInput.AccessControl.meetingPassword))
            {
                strb.Append("<accessControl>");
                strb.AppendFormat("<meetingPassword>{0}</meetingPassword>", meetingInput.AccessControl.meetingPassword);
                strb.Append("</accessControl>");
            }

            if (meetingInput.Metadata != null)
            {
                strb.Append("<metaData>");
                strb.AppendFormat("<confName>{0}</confName>", meetingInput.Metadata.confName);
                if (meetingInput.Metadata.meetingType != null)
                {
                    strb.AppendFormat("<meetingType>{0}</meetingType>", meetingInput.Metadata.meetingType);
                }

                if (!string.IsNullOrEmpty(meetingInput.Metadata.agenda))
                {
                    strb.AppendFormat("<agenda>{0}</agenda>", meetingInput.Metadata.agenda);
                }
                strb.Append("</metaData>");
            }

            if (meetingInput.Attendees != null && meetingInput.Attendees.Count() > 0)
            {
                strb.Append("<participants>");
                if (meetingInput.MaxUserNumber != null)
                {
                    strb.AppendFormat("<maxUserNumber>{0}</maxUserNumber>", meetingInput.MaxUserNumber);
                }

                strb.Append("<attendees>");
                foreach (Person p in meetingInput.Attendees)
                {
                    strb.Append("<attendee>");
                    strb.Append("<person>");

                    if (!string.IsNullOrEmpty(p.Name))
                    {
                        strb.AppendFormat("<name>{0}</name>", p.Name);
                    }

                    if (!string.IsNullOrEmpty(p.Email))
                    {
                        strb.AppendFormat("<email>{0}</email>", p.Email);
                    }
                    strb.Append("</person>");

                    strb.Append("</attendee>");
                }

                strb.Append("</attendees>");
                strb.Append("</participants>");
            }

            if (meetingInput.EnableOptions != null)
            {
                strb.Append("<enableOptions>");

                if (meetingInput.EnableOptions.chat != null)
                {
                    strb.AppendFormat("<chat>{0}</chat>", meetingInput.EnableOptions.chat);
                }

                if (meetingInput.EnableOptions.audioVideo != null)
                {
                    strb.AppendFormat("<audioVideo>{0}</audioVideo>", meetingInput.EnableOptions.audioVideo);
                }

                if (meetingInput.EnableOptions.poll != null)
                {
                    strb.AppendFormat("<poll>{0}</poll>", meetingInput.EnableOptions.poll);
                }
                strb.Append("</enableOptions>");

            }

            if (meetingInput.Schedule != null)
            {
                strb.Append("<schedule>");


                strb.AppendFormat("<startDate>{0}</startDate>", meetingInput.Schedule.startDate);


                if (meetingInput.Schedule.openTime != null)
                {
                    strb.AppendFormat("<openTime>{0}</openTime>", meetingInput.Schedule.openTime);
                }

                if (meetingInput.Schedule.joinTeleconfBeforeHost != null)
                {
                    strb.AppendFormat("<joinTeleconfBeforeHost>{0}</joinTeleconfBeforeHost>", meetingInput.Schedule.joinTeleconfBeforeHost);
                }

                if (meetingInput.Schedule.duration != null)
                {
                    strb.AppendFormat("<duration>{0}</duration>", meetingInput.Schedule.duration);
                }

                if (meetingInput.Schedule.timeZoneID != null)
                {
                    strb.AppendFormat("<timeZoneID>{0}</timeZoneID>", meetingInput.Schedule.timeZoneID);
                }
                strb.Append("</schedule>");
            }

            if (meetingInput.Telephony != null)
            {
                strb.Append("<telephony>");

                if (!string.IsNullOrEmpty(meetingInput.Telephony.telephonySupport))
                {
                    strb.AppendFormat("<telephonySupport>{0}</telephonySupport>", meetingInput.Telephony.telephonySupport);
                }

                if (!string.IsNullOrEmpty(meetingInput.Telephony.extTelephonyDescription))
                {
                    strb.AppendFormat("<extTelephonyDescription>{0}</extTelephonyDescription>", meetingInput.Telephony.extTelephonyDescription);
                }

                strb.Append("</telephony>");
            }

            if (meetingInput.Repeat != null)
            {
                strb.Append(Recurrence.GetRecurrenceXml(meetingInput.Repeat));
            }

            if(meetingInput.AttendeeOptions != null)
            {
                strb.Append("<attendeeOptions>");

                if (meetingInput.AttendeeOptions.auto!=null)
                {
                    strb.AppendFormat("<auto>{0}</auto>", meetingInput.AttendeeOptions.auto);
                }

                if (meetingInput.AttendeeOptions.excludePassword != null)
                {
                    strb.AppendFormat("<excludePassword>{0}</excludePassword>", meetingInput.AttendeeOptions.excludePassword);
                }

                if (meetingInput.AttendeeOptions.joinApproval != null)
                {
                    strb.AppendFormat("<joinApproval>{0}</joinApproval>", meetingInput.AttendeeOptions.joinApproval);
                }

                if (meetingInput.AttendeeOptions.joinRequiresAccount != null)
                {
                    strb.AppendFormat("<joinRequiresAccount>{0}</joinRequiresAccount>", meetingInput.AttendeeOptions.joinRequiresAccount);
                }

                if (meetingInput.AttendeeOptions.registration != null)
                {
                    strb.AppendFormat("<registration>{0}</registration>", meetingInput.AttendeeOptions.registration);
                }

                if (meetingInput.AttendeeOptions.request != null)
                {
                    strb.AppendFormat("<request>{0}</request>", meetingInput.AttendeeOptions.request);
                }
                strb.Append("</attendeeOptions>");

            }
            strb.Append(EndBody());
            strb.Append(EndServMessage());

            string responseFromServer = HttpCall(GetWebExServiceUrl(), strb.ToString());

            WebExResponseMessage wx = new WebExResponseMessage();
            if (includeRawResponse)
            {
                wx.ResponseMessageRaw = responseFromServer;
            }

            if (!string.IsNullOrEmpty(responseFromServer))
            {
                Com.Meelo.Conferencing.WebEx.Generic.message m = serializer.Deserialize(new StringReader(responseFromServer.Replace("xsi:type=\"meet:setMeetingResponse\"", string.Empty))) as Com.Meelo.Conferencing.WebEx.Generic.message;
                wx.ResponseMessage = m;


            }

            return wx;
        }

        public WebExResponseMessage GetSite(bool includeRawResponse = false)
        {
            StringBuilder strb = new StringBuilder();
            strb.Append(BeginDocument());
            strb.Append(BeginServMessage());
            strb.Append(GenerateHeader());
            strb.Append(BeginBody("java:com.webex.service.binding.site.GetSite"));


          


            strb.Append(EndBody());
            strb.Append(EndServMessage());

            string responseFromServer = HttpCall(GetWebExServiceUrl(), strb.ToString());

            WebExResponseMessage wx = new WebExResponseMessage();
            if (includeRawResponse)
            {
                wx.ResponseMessageRaw = responseFromServer;
            }

            if (!string.IsNullOrEmpty(responseFromServer))
            {
                Com.Meelo.Conferencing.WebEx.Generic.message m = serializer.Deserialize(new StringReader(responseFromServer.Replace("xsi:type=\"ns1:getSiteResponse\"", string.Empty))) as Com.Meelo.Conferencing.WebEx.Generic.message;
                wx.ResponseMessage = m;


            }

            return wx;
        }
        #endregion

        #region Meeting Attendee Service

        public string CreateMeetingAttendee(string sessionKey, Person p)
        {

            StringBuilder strb = new StringBuilder();
            strb.Append(BeginDocument());
            strb.Append(BeginServMessage());
            strb.Append(GenerateHeader());
            strb.Append(BeginBody("java:com.webex.service.binding.attendee.CreateMeetingAttendee"));

            strb.AppendFormat("<sessionKey>{0}</sessionKey>", sessionKey);



            strb.Append("<person>");

            if (!string.IsNullOrEmpty(p.Name))
            {
                strb.AppendFormat("<name>{0}</name>", p.Name);
            }

            if (!string.IsNullOrEmpty(p.Email))
            {
                strb.AppendFormat("<email>{0}</email>", p.Email);
            }
            strb.Append("</person>");

            if(p.EmailInvitation != null)
            {
                strb.AppendFormat("<emailInvitations>{0}</emailInvitations>", p.EmailInvitation);
            }




            strb.Append(EndBody());
            strb.Append(EndServMessage());

            return HttpCall(GetWebExServiceUrl(), strb.ToString());

            //if (!string.IsNullOrEmpty(responseFromServer))
            //{
            //    return serializer.Deserialize(new StringReader(responseFromServer.Replace("xsi:type=\"att:CreateMeetingAttendeeResponse\"", string.Empty))) as message;



            //}

            //return null;
        }

        public WebExResponseMessage DeleteAttendee(string attendeeID, bool includeRawResponse = false)
        {
            StringBuilder strb = new StringBuilder();
            strb.Append(BeginDocument());
            strb.Append(BeginServMessage());
            strb.Append(GenerateHeader());
            strb.Append(BeginBody("java:com.webex.service.binding.attendee.DelMeetingAttendee"));


            strb.AppendFormat("<attendeeID>{0}</attendeeID>", attendeeID);


            strb.Append(EndBody());
            strb.Append(EndServMessage());

            string responseFromServer = HttpCall(GetWebExServiceUrl(), strb.ToString());

            WebExResponseMessage wx = new WebExResponseMessage();
            if (includeRawResponse)
            {
                wx.ResponseMessageRaw = responseFromServer;
            }

            if (!string.IsNullOrEmpty(responseFromServer))
            {
                Com.Meelo.Conferencing.WebEx.Generic.message m = serializer.Deserialize(new StringReader(responseFromServer.Replace("xsi:type=\"att:delMeetingAttendeeResponse\"", string.Empty))) as Com.Meelo.Conferencing.WebEx.Generic.message;
                wx.ResponseMessage = m;


            }

            return wx;
        }
        #endregion

      

        #region private utility functions
        private string GetWebExServiceUrl(string service= "WBXService/XMLService")
        {
           return string.Format("https://{0}.webex.com/{1}", SecurityContext.SiteName, service);
        }

        private static WebExResponseHeader GetHeader(string result,  string gsbStatus, string reason=null, uint? exceptionId=null)
        {
            return new WebExResponseHeader()
            {
                ExceptionID = exceptionId,
                GsbStatus = gsbStatus,
                Reason = reason,
                Result = result
            };
        }
        private static string HttpCall(string url, string contentToSend, string method = "POST", string contentType = "application/x-www-form-urlencoded")
        {
            try
            {

                WebRequest request = WebRequest.Create(url);
                // Set the Method property of the request to POST.
                if (!string.IsNullOrEmpty(method))
                {
                    request.Method = method;
                }
                // Set the ContentType property of the WebRequest.

                if (!string.IsNullOrEmpty(contentType))
                {
                    request.ContentType = contentType;
                }

                byte[] byteArray = Encoding.UTF8.GetBytes(contentToSend);

                // Set the ContentLength property of the WebRequest.
                request.ContentLength = byteArray.Length;

                // Get the request stream.
                Stream dataStream = request.GetRequestStream();
                // Write the data to the request stream.
                dataStream.Write(byteArray, 0, byteArray.Length);
                // Close the Stream object.
                dataStream.Close();
                // Get the response.
                WebResponse response = request.GetResponse();

                // Get the stream containing content returned by the server.
                dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.
                string responseFromServer = reader.ReadToEnd();
                //// Display the content.
                //Console.WriteLine(responseFromServer);
                // Clean up the streams.
                reader.Close();
                dataStream.Close();
                response.Close();

                return responseFromServer;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
