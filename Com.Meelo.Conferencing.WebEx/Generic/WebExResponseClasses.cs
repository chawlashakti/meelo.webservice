﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.Conferencing.WebEx.Generic
{

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service", IsNullable = false)]
    public partial class message
    {

        private messageHeader headerField;

        private messageBody bodyField;

        /// <remarks/>
        public messageHeader header
        {
            get
            {
                return this.headerField;
            }
            set
            {
                this.headerField = value;
            }
        }

        /// <remarks/>
        public messageBody body
        {
            get
            {
                return this.bodyField;
            }
            set
            {
                this.bodyField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service")]
    public partial class messageHeader
    {

        private messageHeaderResponse responseField;

        /// <remarks/>
        public messageHeaderResponse response
        {
            get
            {
                return this.responseField;
            }
            set
            {
                this.responseField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service")]
    public partial class messageHeaderResponse
    {

        private string resultField;

        private string reasonField;

        private string gsbStatusField;

        private uint exceptionIDField;

        private messageHeaderResponseSubErrors subErrorsField;

        /// <remarks/>
        public string result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        public string reason
        {
            get
            {
                return this.reasonField;
            }
            set
            {
                this.reasonField = value;
            }
        }

        /// <remarks/>
        public string gsbStatus
        {
            get
            {
                return this.gsbStatusField;
            }
            set
            {
                this.gsbStatusField = value;
            }
        }

        /// <remarks/>
        public uint exceptionID
        {
            get
            {
                return this.exceptionIDField;
            }
            set
            {
                this.exceptionIDField = value;
            }
        }

        /// <remarks/>
        public messageHeaderResponseSubErrors subErrors
        {
            get
            {
                return this.subErrorsField;
            }
            set
            {
                this.subErrorsField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service")]
    public partial class messageHeaderResponseSubErrors
    {

        private messageHeaderResponseSubErrorsSubError subErrorField;

        /// <remarks/>
        public messageHeaderResponseSubErrorsSubError subError
        {
            get
            {
                return this.subErrorField;
            }
            set
            {
                this.subErrorField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service")]
    public partial class messageHeaderResponseSubErrorsSubError
    {

        private string exceptionIDField;

        private string reasonField;

        private string valueField;

        /// <remarks/>
        public string exceptionID
        {
            get
            {
                return this.exceptionIDField;
            }
            set
            {
                this.exceptionIDField = value;
            }
        }

        /// <remarks/>
        public string reason
        {
            get
            {
                return this.reasonField;
            }
            set
            {
                this.reasonField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service")]
    public partial class messageBody
    {

        private messageBodyBodyContent bodyContentField;

        /// <remarks/>
        public messageBodyBodyContent bodyContent
        {
            get
            {
                return this.bodyContentField;
            }
            set
            {
                this.bodyContentField = value;
            }
        }


    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service")]
    public partial class messageBodyBodyContent
    {
        #region API
        private string apiVersionField;

        private string releaseField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public string apiVersion
        {
            get
            {
                return this.apiVersionField;
            }
            set
            {
                this.apiVersionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public string release
        {
            get
            {
                return this.releaseField;
            }
            set
            {
                this.releaseField = value;
            }
        }

        #endregion

        #region Meeting Objects
        private meeting[] meetingField;

        private matchingRecords matchingRecordsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("meeting", Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public meeting[] meeting
        {
            get
            {
                return this.meetingField;
            }
            set
            {
                this.meetingField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public matchingRecords matchingRecords
        {
            get
            {
                return this.matchingRecordsField;
            }
            set
            {
                this.matchingRecordsField = value;
            }
        }

        #endregion

        #region Authentication

        private string sessionTicketField;

        private ulong createTimeField;

        private uint timeToLiveField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public string sessionTicket
        {
            get
            {
                return this.sessionTicketField;
            }
            set
            {
                this.sessionTicketField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public ulong createTime
        {
            get
            {
                return this.createTimeField;
            }
            set
            {
                this.createTimeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public uint timeToLive
        {
            get
            {
                return this.timeToLiveField;
            }
            set
            {
                this.timeToLiveField = value;
            }
        }
        #endregion

        #region Login/Logout Session Url
        private string userLoginURLField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public string userLoginURL
        {
            get
            {
                return this.userLoginURLField;
            }
            set
            {
                this.userLoginURLField = value;
            }
        }

        private string userLogoutURLField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public string userLogoutURL
        {
            get
            {
                return this.userLogoutURLField;
            }
            set
            {
                this.userLogoutURLField = value;
            }
        }
        #endregion


        #region GetUser

        private string firstNameField;

        private string lastNameField;

        private int categoryIdField;

        private string companyField;

        private string webExIdField;

        private address addressField;

        private phones phonesField;

        private string emailField;

        private string passwordField;

        private string passwordHintField;

        private string passwordHintAnswerField;

        private string personalUrlField;

        private string expirationDateField;

        private commOptions commOptionsField;

        private meetingTypes meetingTypesField;

        private options optionsField;

        private int timeZoneIDField;

        private string timeZoneField;

        private string timeZoneWithDSTField;

       // private string trackingField;

        private string serviceField;

        private privilege privilegeField;

        private string languageField;

        private string localeField;

        private string activeField;

        private string defaultCallInField;

        private supportedServices supportedServicesField;

        private myWebEx myWebExField;

        private personalTeleconf personalTeleconfField;

        private string videoSystemsField;

        private personalMeetingRoom personalMeetingRoomField;

        private sessionOptions sessionOptionsField;

        private supportCenter supportCenterField;

        private security securityField;

        private int languageIDField;

        private webACDPrefs webACDPrefsField;

        private string registrationDateField;

        private int visitCountField;

        private string userIdField;

        private eventCenter eventCenterField;

        private bool? passwordExpiresField;

        private int passwordDaysLeftField;

        private avatar avatarField;
        //


        private string descriptionField;

        private salesCenter salesCenterField;

        private scheduleFor scheduleForField;

        //new


        private string titleField;


        private tracking trackingField;



        private remoteSupport remoteSupportField;



        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public string title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public remoteSupport remoteSupport
        {
            get
            {
                return this.remoteSupportField;
            }
            set
            {
                this.remoteSupportField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public string firstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public string lastName
        {
            get
            {
                return this.lastNameField;
            }
            set
            {
                this.lastNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public int categoryId
        {
            get
            {
                return this.categoryIdField;
            }
            set
            {
                this.categoryIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public string description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public string company
        {
            get
            {
                return this.companyField;
            }
            set
            {
                this.companyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public string webExId
        {
            get
            {
                return this.webExIdField;
            }
            set
            {
                this.webExIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public address address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public phones phones
        {
            get
            {
                return this.phonesField;
            }
            set
            {
                this.phonesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public string email
        {
            get
            {
                return this.emailField;
            }
            set
            {
                this.emailField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public string password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public string personalUrl
        {
            get
            {
                return this.personalUrlField;
            }
            set
            {
                this.personalUrlField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public string expirationDate
        {
            get
            {
                return this.expirationDateField;
            }
            set
            {
                this.expirationDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public commOptions commOptions
        {
            get
            {
                return this.commOptionsField;
            }
            set
            {
                this.commOptionsField = value;
            }
        }



        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public options options
        {
            get
            {
                return this.optionsField;
            }
            set
            {
                this.optionsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public int timeZoneID
        {
            get
            {
                return this.timeZoneIDField;
            }
            set
            {
                this.timeZoneIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public string timeZone
        {
            get
            {
                return this.timeZoneField;
            }
            set
            {
                this.timeZoneField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public string timeZoneWithDST
        {
            get
            {
                return this.timeZoneWithDSTField;
            }
            set
            {
                this.timeZoneWithDSTField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public tracking tracking
        {
            get
            {
                return this.trackingField;
            }
            set
            {
                this.trackingField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public string service
        {
            get
            {
                return this.serviceField;
            }
            set
            {
                this.serviceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public privilege privilege
        {
            get
            {
                return this.privilegeField;
            }
            set
            {
                this.privilegeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public string language
        {
            get
            {
                return this.languageField;
            }
            set
            {
                this.languageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public string locale
        {
            get
            {
                return this.localeField;
            }
            set
            {
                this.localeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public string active
        {
            get
            {
                return this.activeField;
            }
            set
            {
                this.activeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public supportedServices supportedServices
        {
            get
            {
                return this.supportedServicesField;
            }
            set
            {
                this.supportedServicesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public myWebEx myWebEx
        {
            get
            {
                return this.myWebExField;
            }
            set
            {
                this.myWebExField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public personalTeleconf personalTeleconf
        {
            get
            {
                return this.personalTeleconfField;
            }
            set
            {
                this.personalTeleconfField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public personalMeetingRoom personalMeetingRoom
        {
            get
            {
                return this.personalMeetingRoomField;
            }
            set
            {
                this.personalMeetingRoomField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public sessionOptions sessionOptions
        {
            get
            {
                return this.sessionOptionsField;
            }
            set
            {
                this.sessionOptionsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public supportCenter supportCenter
        {
            get
            {
                return this.supportCenterField;
            }
            set
            {
                this.supportCenterField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public security security
        {
            get
            {
                return this.securityField;
            }
            set
            {
                this.securityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public int languageID
        {
            get
            {
                return this.languageIDField;
            }
            set
            {
                this.languageIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public webACDPrefs webACDPrefs
        {
            get
            {
                return this.webACDPrefsField;
            }
            set
            {
                this.webACDPrefsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public string registrationDate
        {
            get
            {
                return this.registrationDateField;
            }
            set
            {
                this.registrationDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public int visitCount
        {
            get
            {
                return this.visitCountField;
            }
            set
            {
                this.visitCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public string userId
        {
            get
            {
                return this.userIdField;
            }
            set
            {
                this.userIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public salesCenter salesCenter
        {
            get
            {
                return this.salesCenterField;
            }
            set
            {
                this.salesCenterField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public bool? passwordExpires
        {
            get
            {
                return this.passwordExpiresField;
            }
            set
            {
                this.passwordExpiresField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public int passwordDaysLeft
        {
            get
            {
                return this.passwordDaysLeftField;
            }
            set
            {
                this.passwordDaysLeftField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public scheduleFor scheduleFor
        {
            get
            {
                return this.scheduleForField;
            }
            set
            {
                this.scheduleForField = value;
            }
        }



        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public string passwordHint
        {
            get
            {
                return this.passwordHintField;
            }
            set
            {
                this.passwordHintField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public string passwordHintAnswer
        {
            get
            {
                return this.passwordHintAnswerField;
            }
            set
            {
                this.passwordHintAnswerField = value;
            }
        }




        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public meetingTypes meetingTypes
        {
            get
            {
                return this.meetingTypesField;
            }
            set
            {
                this.meetingTypesField = value;
            }
        }




        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public string defaultCallIn
        {
            get
            {
                return this.defaultCallInField;
            }
            set
            {
                this.defaultCallInField = value;
            }
        }



        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public string videoSystems
        {
            get
            {
                return this.videoSystemsField;
            }
            set
            {
                this.videoSystemsField = value;
            }
        }




        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public eventCenter eventCenter
        {
            get
            {
                return this.eventCenterField;
            }
            set
            {
                this.eventCenterField = value;
            }
        }


        /// <remarks/>


        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        public avatar avatar
        {
            get
            {
                return this.avatarField;
            }
            set
            {
                this.avatarField = value;
            }
        }

        #endregion

        #region DelRecording
        private string recordingIDField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public string recordingID
        {
            get
            {
                return this.recordingIDField;
            }
            set
            {
                this.recordingIDField = value;
            }
        }
        #endregion


        #region lstRecording

        private recording[] recordingField;

        [System.Xml.Serialization.XmlElementAttribute("recording", Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public recording[] recording
        {
            get
            {
                return this.recordingField;
            }
            set
            {
                this.recordingField = value;
            }
        }
        #endregion

        #region lstsummarySession

        private session[] sessionField;

        [System.Xml.Serialization.XmlElementAttribute("session", Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public session[] session
        {
            get
            {
                return this.sessionField;
            }
            set
            {
                this.sessionField = value;
            }
        }
        #endregion

        #region GetOneClickSettings
        private metaData metaDataField;

        private trackingCodesTrackingCode[] trackingCodesField;

        private telephony telephonyField;

        private enableOptions enableOptionsField;

        private attendeeOptions attendeeOptionsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public metaData metaData
        {
            get
            {
                return this.metaDataField;
            }
            set
            {
                this.metaDataField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        [System.Xml.Serialization.XmlArrayItemAttribute("trackingCode", IsNullable = false)]
        public trackingCodesTrackingCode[] trackingCodes
        {
            get
            {
                return this.trackingCodesField;
            }
            set
            {
                this.trackingCodesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public telephony telephony
        {
            get
            {
                return this.telephonyField;
            }
            set
            {
                this.telephonyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public enableOptions enableOptions
        {
            get
            {
                return this.enableOptionsField;
            }
            set
            {
                this.enableOptionsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public attendeeOptions attendeeOptions
        {
            get
            {
                return this.attendeeOptionsField;
            }
            set
            {
                this.attendeeOptionsField = value;
            }
        }
        #endregion

        #region GetRecordingInfo
        private basic basicField;

        private playback playbackField;

        private fileAccess fileAccessField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public basic basic
        {
            get
            {
                return this.basicField;
            }
            set
            {
                this.basicField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public playback playback
        {
            get
            {
                return this.playbackField;
            }
            set
            {
                this.playbackField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public fileAccess fileAccess
        {
            get
            {
                return this.fileAccessField;
            }
            set
            {
                this.fileAccessField = value;
            }
        }
        #endregion

        #region GetSessionInfo
        private string statusField;

        private string sessionkeyField;

        private string confIDField;

        private string verifyFlashMediaURLField;

        private string verifyWinMediaURLField;

        private accessControl accessControlField;



        private host hostField;

        private schedule scheduleField;
        private bool isLockedField;


        private bool isAudioOnlyField;

        private bool telePresenceField;

        private bool isAlternateHostField;

        private bool isCreatorField;


        private bool supportE2EField;

        private bool isAllowJBHField;

        private bool isCETMeetingField;

        private bool isPersonalMeetingRoomField;

        private bool isNextUpcomingInstanceField;

        private string sipURLField;


        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public bool isLocked
        {
            get
            {
                return this.isLockedField;
            }
            set
            {
                this.isLockedField = value;
            }
        }





        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public bool isAudioOnly
        {
            get
            {
                return this.isAudioOnlyField;
            }
            set
            {
                this.isAudioOnlyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public bool telePresence
        {
            get
            {
                return this.telePresenceField;
            }
            set
            {
                this.telePresenceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public bool isAlternateHost
        {
            get
            {
                return this.isAlternateHostField;
            }
            set
            {
                this.isAlternateHostField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public bool isCreator
        {
            get
            {
                return this.isCreatorField;
            }
            set
            {
                this.isCreatorField = value;
            }
        }



        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public bool supportE2E
        {
            get
            {
                return this.supportE2EField;
            }
            set
            {
                this.supportE2EField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public bool isAllowJBH
        {
            get
            {
                return this.isAllowJBHField;
            }
            set
            {
                this.isAllowJBHField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public bool isCETMeeting
        {
            get
            {
                return this.isCETMeetingField;
            }
            set
            {
                this.isCETMeetingField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public bool isPersonalMeetingRoom
        {
            get
            {
                return this.isPersonalMeetingRoomField;
            }
            set
            {
                this.isPersonalMeetingRoomField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public bool isNextUpcomingInstance
        {
            get
            {
                return this.isNextUpcomingInstanceField;
            }
            set
            {
                this.isNextUpcomingInstanceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public string sipURL
        {
            get
            {
                return this.sipURLField;
            }
            set
            {
                this.sipURLField = value;
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public string status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public string sessionkey
        {
            get
            {
                return this.sessionkeyField;
            }
            set
            {
                this.sessionkeyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public string confID
        {
            get
            {
                return this.confIDField;
            }
            set
            {
                this.confIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public string verifyFlashMediaURL
        {
            get
            {
                return this.verifyFlashMediaURLField;
            }
            set
            {
                this.verifyFlashMediaURLField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public string verifyWinMediaURL
        {
            get
            {
                return this.verifyWinMediaURLField;
            }
            set
            {
                this.verifyWinMediaURLField = value;
            }
        }

        /// <remarks/>
       // [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public accessControl accessControl
        {
            get
            {
                return this.accessControlField;
            }
            set
            {
                this.accessControlField = value;
            }
        }


        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
        public host host
        {
            get
            {
                return this.hostField;
            }
            set
            {
                this.hostField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public schedule schedule
        {
            get
            {
                return this.scheduleField;
            }
            set
            {
                this.scheduleField = value;
            }
        }

        #endregion

        #region Create Meeting
        private string meetingkeyField;

        private iCalendarURL iCalendarURLField;

        private string guestTokenField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public string meetingkey
        {
            get
            {
                return this.meetingkeyField;
            }
            set
            {
                this.meetingkeyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public iCalendarURL iCalendarURL
        {
            get
            {
                return this.iCalendarURLField;
            }
            set
            {
                this.iCalendarURLField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public string guestToken
        {
            get
            {
                return this.guestTokenField;
            }
            set
            {
                this.guestTokenField = value;
            }
        }
        #endregion

        #region GethostmeetingUrl
        private string hostMeetingURLField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public string hostMeetingURL
        {
            get
            {
                return this.hostMeetingURLField;
            }
            set
            {
                this.hostMeetingURLField = value;
            }
        }
        #endregion

        #region joinmeetingUrl
        private string joinMeetingURLField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public string joinMeetingURL
        {
            get
            {
                return this.joinMeetingURLField;
            }
            set
            {
                this.joinMeetingURLField = value;
            }
        }

        private string inviteMeetingURLField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public string inviteMeetingURL
        {
            get
            {
                return this.inviteMeetingURLField;
            }
            set
            {
                this.inviteMeetingURLField = value;
            }
        }
        #endregion

        #region GetMeeting



        private participants participantsField;



        private repeat repeatField;

        private remind remindField;

        // private string trackingField;

        private bool? hostJoinedField;

        private bool? participantsJoinedField;

        private string hostKeyField;

        private string eventIDField;

        private uint? hostTypeField;

        ///
      

        private string assistServiceField;


        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public participants participants
        {
            get
            {
                return this.participantsField;
            }
            set
            {
                this.participantsField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public string assistService
        {
            get
            {
                return this.assistServiceField;
            }
            set
            {
                this.assistServiceField = value;
            }
        }



        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public repeat repeat
        {
            get
            {
                return this.repeatField;
            }
            set
            {
                this.repeatField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public remind remind
        {
            get
            {
                return this.remindField;
            }
            set
            {
                this.remindField = value;
            }
        }



        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public bool? hostJoined
        {
            get
            {
                return this.hostJoinedField;
            }
            set
            {
                this.hostJoinedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public bool? participantsJoined
        {
            get
            {
                return this.participantsJoinedField;
            }
            set
            {
                this.participantsJoinedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public string hostKey
        {
            get
            {
                return this.hostKeyField;
            }
            set
            {
                this.hostKeyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public string eventID
        {
            get
            {
                return this.eventIDField;
            }
            set
            {
                this.eventIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
        public uint? hostType
        {
            get
            {
                return this.hostTypeField;
            }
            set
            {
                this.hostTypeField = value;
            }
        }
        #endregion

        #region GetSite
        private siteInstance siteInstanceField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
        public siteInstance siteInstance
        {
            get
            {
                return this.siteInstanceField;
            }
            set
            {
                this.siteInstanceField = value;
            }
        }
        #endregion




    }

    

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting", IsNullable = false)]
    public partial class participants
    {

        private int maxUserNumberField;

        private participantsAttendee[] attendeesField;

        /// <remarks/>
        public int maxUserNumber
        {
            get
            {
                return this.maxUserNumberField;
            }
            set
            {
                this.maxUserNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("attendee", IsNullable = false)]
        public participantsAttendee[] attendees
        {
            get
            {
                return this.attendeesField;
            }
            set
            {
                this.attendeesField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
    public partial class participantsAttendee
    {

        private person personField;

        private string contactIDField;

        private string joinStatusField;

        private string meetingKeyField;

        private string languageField;

        private string roleField;

        private int languageIDField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/attendee")]
        public person person
        {
            get
            {
                return this.personField;
            }
            set
            {
                this.personField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/attendee")]
        public string contactID
        {
            get
            {
                return this.contactIDField;
            }
            set
            {
                this.contactIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/attendee")]
        public string joinStatus
        {
            get
            {
                return this.joinStatusField;
            }
            set
            {
                this.joinStatusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/attendee")]
        public string meetingKey
        {
            get
            {
                return this.meetingKeyField;
            }
            set
            {
                this.meetingKeyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/attendee")]
        public string language
        {
            get
            {
                return this.languageField;
            }
            set
            {
                this.languageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/attendee")]
        public string role
        {
            get
            {
                return this.roleField;
            }
            set
            {
                this.roleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/attendee")]
        public int languageID
        {
            get
            {
                return this.languageIDField;
            }
            set
            {
                this.languageIDField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/attendee")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/attendee", IsNullable = false)]
    public partial class person
    {

        private string nameField;

        private string firstNameField;

        private string lastNameField;

        private address addressField;

        private string phonesField;

        private string emailField;

        private string typeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/common")]
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/common")]
        public string firstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/common")]
        public string lastName
        {
            get
            {
                return this.lastNameField;
            }
            set
            {
                this.lastNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/common")]
        public address address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/common")]
        public string phones
        {
            get
            {
                return this.phonesField;
            }
            set
            {
                this.phonesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/common")]
        public string email
        {
            get
            {
                return this.emailField;
            }
            set
            {
                this.emailField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/common")]
        public string type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/common")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/common", IsNullable = false)]
    public partial class address
    {

        private string addressTypeField;

        private string countryField;

        private string zipCodeField;

        private string stateField;

        private string cityField;

        private string address1Field;

        private string address2Field;

        /// <remarks/>
        public string addressType
        {
            get
            {
                return this.addressTypeField;
            }
            set
            {
                this.addressTypeField = value;
            }
        }

        public string address1
        {
            get
            {
                return this.address1Field;
            }
            set
            {
                this.address1Field = value;
            }
        }

        public string address2
        {
            get
            {
                return this.address2Field;
            }
            set
            {
                this.address2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/common")]
        public string country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/common")]
        public string state
        {
            get
            {
                return this.stateField;
            }
            set
            {
                this.stateField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/common")]
        public string zipCode
        {
            get
            {
                return this.zipCodeField;
            }
            set
            {
                this.zipCodeField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/common")]
        public string city
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }
    }

    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting", IsNullable = false)]



    public partial class meeting
    {

        private string meetingKeyField;

        private string confNameField;

        private int meetingTypeField;

        private string hostWebExIDField;

        private string otherHostWebExIDField;

        private int timeZoneIDField;

        private string timeZoneField;

        private string statusField;

        private string startDateField;

        private int durationField;

        private string listStatusField;

        private bool? hostJoinedField;

        private bool? participantsJoinedField;

        private bool? telePresenceField;

        /// <remarks/>
        public string meetingKey
        {
            get
            {
                return this.meetingKeyField;
            }
            set
            {
                this.meetingKeyField = value;
            }
        }

        /// <remarks/>
        public string confName
        {
            get
            {
                return this.confNameField;
            }
            set
            {
                this.confNameField = value;
            }
        }

        /// <remarks/>
        public int meetingType
        {
            get
            {
                return this.meetingTypeField;
            }
            set
            {
                this.meetingTypeField = value;
            }
        }

        /// <remarks/>
        public string hostWebExID
        {
            get
            {
                return this.hostWebExIDField;
            }
            set
            {
                this.hostWebExIDField = value;
            }
        }

        /// <remarks/>
        public string otherHostWebExID
        {
            get
            {
                return this.otherHostWebExIDField;
            }
            set
            {
                this.otherHostWebExIDField = value;
            }
        }

        /// <remarks/>
        public int timeZoneID
        {
            get
            {
                return this.timeZoneIDField;
            }
            set
            {
                this.timeZoneIDField = value;
            }
        }

        /// <remarks/>
        public string timeZone
        {
            get
            {
                return this.timeZoneField;
            }
            set
            {
                this.timeZoneField = value;
            }
        }

        /// <remarks/>
        public string status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        public string startDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }

        /// <remarks/>
        public int duration
        {
            get
            {
                return this.durationField;
            }
            set
            {
                this.durationField = value;
            }
        }

        /// <remarks/>
        public string listStatus
        {
            get
            {
                return this.listStatusField;
            }
            set
            {
                this.listStatusField = value;
            }
        }

        /// <remarks/>
        public bool? hostJoined
        {
            get
            {
                return this.hostJoinedField;
            }
            set
            {
                this.hostJoinedField = value;
            }
        }

        /// <remarks/>
        public bool? participantsJoined
        {
            get
            {
                return this.participantsJoinedField;
            }
            set
            {
                this.participantsJoinedField = value;
            }
        }

        /// <remarks/>
        public bool? telePresence
        {
            get
            {
                return this.telePresenceField;
            }
            set
            {
                this.telePresenceField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting", IsNullable = false)]
    public partial class matchingRecords
    {

        private int totalField;

        private int returnedField;

        private int startFromField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public int total
        {
            get
            {
                return this.totalField;
            }
            set
            {
                this.totalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public int returned
        {
            get
            {
                return this.returnedField;
            }
            set
            {
                this.returnedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public int startFrom
        {
            get
            {
                return this.startFromField;
            }
            set
            {
                this.startFromField = value;
            }
        }
    }

    #region GetUser



    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user", IsNullable = false)]
    public partial class phones
    {

        private string phoneField;

        private string mobilePhoneField;

        private string faxField;

        private string pagerField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/common")]
        public string phone
        {
            get
            {
                return this.phoneField;
            }
            set
            {
                this.phoneField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/common")]
        public string mobilePhone
        {
            get
            {
                return this.mobilePhoneField;
            }
            set
            {
                this.mobilePhoneField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/common")]
        public string fax
        {
            get
            {
                return this.faxField;
            }
            set
            {
                this.faxField = value;
            }
        }

        /// <remarks/>
        public string pager
        {
            get
            {
                return this.pagerField;
            }
            set
            {
                this.pagerField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user", IsNullable = false)]
    public partial class commOptions
    {

        private bool? prodAnnounceField;

        private bool? trainingInfoField;

        private bool? electronicInfoField;

        private bool? promosField;

        private bool? pressField;

        private bool? emailField;

        private bool? faxField;

        private bool? phoneField;

        private bool? mailField;

        /// <remarks/>
        public bool? prodAnnounce
        {
            get
            {
                return this.prodAnnounceField;
            }
            set
            {
                this.prodAnnounceField = value;
            }
        }

        /// <remarks/>
        public bool? trainingInfo
        {
            get
            {
                return this.trainingInfoField;
            }
            set
            {
                this.trainingInfoField = value;
            }
        }

        /// <remarks/>
        public bool? electronicInfo
        {
            get
            {
                return this.electronicInfoField;
            }
            set
            {
                this.electronicInfoField = value;
            }
        }

        /// <remarks/>
        public bool? promos
        {
            get
            {
                return this.promosField;
            }
            set
            {
                this.promosField = value;
            }
        }

        /// <remarks/>
        public bool? press
        {
            get
            {
                return this.pressField;
            }
            set
            {
                this.pressField = value;
            }
        }

        /// <remarks/>
        public bool? email
        {
            get
            {
                return this.emailField;
            }
            set
            {
                this.emailField = value;
            }
        }

        /// <remarks/>
        public bool? fax
        {
            get
            {
                return this.faxField;
            }
            set
            {
                this.faxField = value;
            }
        }

        /// <remarks/>
        public bool? phone
        {
            get
            {
                return this.phoneField;
            }
            set
            {
                this.phoneField = value;
            }
        }

        /// <remarks/>
        public bool? mail
        {
            get
            {
                return this.mailField;
            }
            set
            {
                this.mailField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user", IsNullable = false)]
    public partial class options
    {

        private bool? firstNameVisibleField;

        private bool? lastNameVisibleField;

        private bool? addressVisibleField;

        private bool? workPhoneVisibleField;

        private bool? cellPhoneVisibleField;

        private bool? pagerVisibleField;

        private bool? faxVisibleField;

        private bool? officeUrlVisibleField;

        private bool? pictureVisibleField;

        private bool? notifyOnNewMessageField;

        private bool? notifyOnMeetingField;

        private bool? followMeEnableField;

        private bool? emailVisibleField;

        private bool? listInCategoryField;

        private bool? titleVisibleField;

        private bool? folderReadField;

        private bool? folderWriteField;

        private bool? messageVisibleField;

        private bool? iconSelect1Field;

        private bool? iconSelect2Field;

        private bool? acceptLinkRequestField;

        private bool? holdOnLinkRequestField;

        private bool? notifyOnLinkRequestField;

        private bool? supportVideoField;

        private bool? supportAppField;

        private bool? supportFileShareField;

        private bool? supportDesktopShareField;

        private bool? supportMeetingRecordField;

        private bool? supportAppshareRemoteField;

        private bool? supportWebTourRemoteField;

        private bool? supportDesktopShareRemoteField;

        private bool? subscriptionOfficeField;

        private bool? workPhoneCallbackField;

        private bool? cellPhoneCallbackField;

        private bool? faxCallbackField;

        private bool? pagerCallbackField;



        /// <remarks/>
        public bool? firstNameVisible
        {
            get
            {
                return this.firstNameVisibleField;
            }
            set
            {
                this.firstNameVisibleField = value;
            }
        }

        /// <remarks/>
        public bool? lastNameVisible
        {
            get
            {
                return this.lastNameVisibleField;
            }
            set
            {
                this.lastNameVisibleField = value;
            }
        }

        /// <remarks/>
        public bool? addressVisible
        {
            get
            {
                return this.addressVisibleField;
            }
            set
            {
                this.addressVisibleField = value;
            }
        }

        /// <remarks/>
        public bool? workPhoneVisible
        {
            get
            {
                return this.workPhoneVisibleField;
            }
            set
            {
                this.workPhoneVisibleField = value;
            }
        }

        /// <remarks/>
        public bool? cellPhoneVisible
        {
            get
            {
                return this.cellPhoneVisibleField;
            }
            set
            {
                this.cellPhoneVisibleField = value;
            }
        }

        /// <remarks/>
        public bool? pagerVisible
        {
            get
            {
                return this.pagerVisibleField;
            }
            set
            {
                this.pagerVisibleField = value;
            }
        }

        /// <remarks/>
        public bool? faxVisible
        {
            get
            {
                return this.faxVisibleField;
            }
            set
            {
                this.faxVisibleField = value;
            }
        }

        /// <remarks/>
        public bool? officeUrlVisible
        {
            get
            {
                return this.officeUrlVisibleField;
            }
            set
            {
                this.officeUrlVisibleField = value;
            }
        }

        /// <remarks/>
        public bool? pictureVisible
        {
            get
            {
                return this.pictureVisibleField;
            }
            set
            {
                this.pictureVisibleField = value;
            }
        }

        /// <remarks/>
        public bool? notifyOnNewMessage
        {
            get
            {
                return this.notifyOnNewMessageField;
            }
            set
            {
                this.notifyOnNewMessageField = value;
            }
        }

        /// <remarks/>
        public bool? notifyOnMeeting
        {
            get
            {
                return this.notifyOnMeetingField;
            }
            set
            {
                this.notifyOnMeetingField = value;
            }
        }

        /// <remarks/>
        public bool? followMeEnable
        {
            get
            {
                return this.followMeEnableField;
            }
            set
            {
                this.followMeEnableField = value;
            }
        }

        /// <remarks/>
        public bool? emailVisible
        {
            get
            {
                return this.emailVisibleField;
            }
            set
            {
                this.emailVisibleField = value;
            }
        }

        /// <remarks/>
        public bool? listInCategory
        {
            get
            {
                return this.listInCategoryField;
            }
            set
            {
                this.listInCategoryField = value;
            }
        }

        /// <remarks/>
        public bool? titleVisible
        {
            get
            {
                return this.titleVisibleField;
            }
            set
            {
                this.titleVisibleField = value;
            }
        }

        /// <remarks/>
        public bool? folderRead
        {
            get
            {
                return this.folderReadField;
            }
            set
            {
                this.folderReadField = value;
            }
        }

        /// <remarks/>
        public bool? folderWrite
        {
            get
            {
                return this.folderWriteField;
            }
            set
            {
                this.folderWriteField = value;
            }
        }

        /// <remarks/>
        public bool? messageVisible
        {
            get
            {
                return this.messageVisibleField;
            }
            set
            {
                this.messageVisibleField = value;
            }
        }

        /// <remarks/>
        public bool? iconSelect1
        {
            get
            {
                return this.iconSelect1Field;
            }
            set
            {
                this.iconSelect1Field = value;
            }
        }

        /// <remarks/>
        public bool? iconSelect2
        {
            get
            {
                return this.iconSelect2Field;
            }
            set
            {
                this.iconSelect2Field = value;
            }
        }

        /// <remarks/>
        public bool? acceptLinkRequest
        {
            get
            {
                return this.acceptLinkRequestField;
            }
            set
            {
                this.acceptLinkRequestField = value;
            }
        }

        /// <remarks/>
        public bool? holdOnLinkRequest
        {
            get
            {
                return this.holdOnLinkRequestField;
            }
            set
            {
                this.holdOnLinkRequestField = value;
            }
        }

        /// <remarks/>
        public bool? notifyOnLinkRequest
        {
            get
            {
                return this.notifyOnLinkRequestField;
            }
            set
            {
                this.notifyOnLinkRequestField = value;
            }
        }

        /// <remarks/>
        public bool? supportVideo
        {
            get
            {
                return this.supportVideoField;
            }
            set
            {
                this.supportVideoField = value;
            }
        }

        /// <remarks/>
        public bool? supportApp
        {
            get
            {
                return this.supportAppField;
            }
            set
            {
                this.supportAppField = value;
            }
        }

        /// <remarks/>
        public bool? supportFileShare
        {
            get
            {
                return this.supportFileShareField;
            }
            set
            {
                this.supportFileShareField = value;
            }
        }

        /// <remarks/>
        public bool? supportDesktopShare
        {
            get
            {
                return this.supportDesktopShareField;
            }
            set
            {
                this.supportDesktopShareField = value;
            }
        }

        /// <remarks/>
        public bool? supportMeetingRecord
        {
            get
            {
                return this.supportMeetingRecordField;
            }
            set
            {
                this.supportMeetingRecordField = value;
            }
        }

        /// <remarks/>
        public bool? supportAppshareRemote
        {
            get
            {
                return this.supportAppshareRemoteField;
            }
            set
            {
                this.supportAppshareRemoteField = value;
            }
        }

        /// <remarks/>
        public bool? supportWebTourRemote
        {
            get
            {
                return this.supportWebTourRemoteField;
            }
            set
            {
                this.supportWebTourRemoteField = value;
            }
        }

        /// <remarks/>
        public bool? supportDesktopShareRemote
        {
            get
            {
                return this.supportDesktopShareRemoteField;
            }
            set
            {
                this.supportDesktopShareRemoteField = value;
            }
        }

        /// <remarks/>
        public bool? subscriptionOffice
        {
            get
            {
                return this.subscriptionOfficeField;
            }
            set
            {
                this.subscriptionOfficeField = value;
            }
        }

        public bool? workPhoneCallback
        {
            get
            {
                return this.workPhoneCallbackField;
            }
            set
            {
                this.workPhoneCallbackField = value;
            }
        }

        /// <remarks/>
        public bool? cellPhoneCallback
        {
            get
            {
                return this.cellPhoneCallbackField;
            }
            set
            {
                this.cellPhoneCallbackField = value;
            }
        }

        /// <remarks/>
        public bool? faxCallback
        {
            get
            {
                return this.faxCallbackField;
            }
            set
            {
                this.faxCallbackField = value;
            }
        }

        /// <remarks/>
        public bool? pagerCallback
        {
            get
            {
                return this.pagerCallbackField;
            }
            set
            {
                this.pagerCallbackField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user", IsNullable = false)]
    public partial class privilege
    {

        private bool? hostField;

        private bool? teleConfCallOutField;

        private bool? teleConfCallOutInternationalField;

        private bool? teleConfCallInField;

        private bool? teleConfTollFreeCallInField;

        private bool? siteAdminField;

        private bool? voiceOverIpField;

        private bool? roSiteAdminField;

        private bool? labAdminField;

        private bool? otherTelephonyField;

        private bool? teleConfCallInInternationalField;

        private bool? attendeeOnlyField;

        private bool? recordingEditorField;

        private bool? meetingAssistField;

        private bool hQvideoField;

        private bool hDvideoField;

        private bool isEnableCETField;

        private bool isEnablePMRField;

        private bool teleCLIAuthEnabledField;

        private bool teleCLIPINEnabledField;

        /// <remarks/>
        public bool? host
        {
            get
            {
                return this.hostField;
            }
            set
            {
                this.hostField = value;
            }
        }

        /// <remarks/>
        public bool? teleConfCallOut
        {
            get
            {
                return this.teleConfCallOutField;
            }
            set
            {
                this.teleConfCallOutField = value;
            }
        }

        /// <remarks/>
        public bool? teleConfCallOutInternational
        {
            get
            {
                return this.teleConfCallOutInternationalField;
            }
            set
            {
                this.teleConfCallOutInternationalField = value;
            }
        }

        /// <remarks/>
        public bool? teleConfCallIn
        {
            get
            {
                return this.teleConfCallInField;
            }
            set
            {
                this.teleConfCallInField = value;
            }
        }

        /// <remarks/>
        public bool? teleConfTollFreeCallIn
        {
            get
            {
                return this.teleConfTollFreeCallInField;
            }
            set
            {
                this.teleConfTollFreeCallInField = value;
            }
        }

        /// <remarks/>
        public bool? siteAdmin
        {
            get
            {
                return this.siteAdminField;
            }
            set
            {
                this.siteAdminField = value;
            }
        }

        /// <remarks/>
        public bool? voiceOverIp
        {
            get
            {
                return this.voiceOverIpField;
            }
            set
            {
                this.voiceOverIpField = value;
            }
        }

        /// <remarks/>
        public bool? roSiteAdmin
        {
            get
            {
                return this.roSiteAdminField;
            }
            set
            {
                this.roSiteAdminField = value;
            }
        }

        /// <remarks/>
        public bool? labAdmin
        {
            get
            {
                return this.labAdminField;
            }
            set
            {
                this.labAdminField = value;
            }
        }

        /// <remarks/>
        public bool? otherTelephony
        {
            get
            {
                return this.otherTelephonyField;
            }
            set
            {
                this.otherTelephonyField = value;
            }
        }

        /// <remarks/>
        public bool? teleConfCallInInternational
        {
            get
            {
                return this.teleConfCallInInternationalField;
            }
            set
            {
                this.teleConfCallInInternationalField = value;
            }
        }

        /// <remarks/>
        public bool? attendeeOnly
        {
            get
            {
                return this.attendeeOnlyField;
            }
            set
            {
                this.attendeeOnlyField = value;
            }
        }

        /// <remarks/>
        public bool? recordingEditor
        {
            get
            {
                return this.recordingEditorField;
            }
            set
            {
                this.recordingEditorField = value;
            }
        }

        /// <remarks/>
        public bool? meetingAssist
        {
            get
            {
                return this.meetingAssistField;
            }
            set
            {
                this.meetingAssistField = value;
            }
        }

        /// <remarks/>
        public bool HQvideo
        {
            get
            {
                return this.hQvideoField;
            }
            set
            {
                this.hQvideoField = value;
            }
        }

        /// <remarks/>
        public bool HDvideo
        {
            get
            {
                return this.hDvideoField;
            }
            set
            {
                this.hDvideoField = value;
            }
        }

        /// <remarks/>
        public bool isEnableCET
        {
            get
            {
                return this.isEnableCETField;
            }
            set
            {
                this.isEnableCETField = value;
            }
        }

        /// <remarks/>
        public bool isEnablePMR
        {
            get
            {
                return this.isEnablePMRField;
            }
            set
            {
                this.isEnablePMRField = value;
            }
        }

        /// <remarks/>
        public bool teleCLIAuthEnabled
        {
            get
            {
                return this.teleCLIAuthEnabledField;
            }
            set
            {
                this.teleCLIAuthEnabledField = value;
            }
        }

        /// <remarks/>
        public bool teleCLIPINEnabled
        {
            get
            {
                return this.teleCLIPINEnabledField;
            }
            set
            {
                this.teleCLIPINEnabledField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user", IsNullable = false)]
    public partial class supportedServices
    {

        private bool meetingCenterField;

        private bool trainingCenterField;

        private bool supportCenterField;

        private bool eventCenterField;

        private bool salesCenterField;

        /// <remarks/>
        public bool meetingCenter
        {
            get
            {
                return this.meetingCenterField;
            }
            set
            {
                this.meetingCenterField = value;
            }
        }

        /// <remarks/>
        public bool trainingCenter
        {
            get
            {
                return this.trainingCenterField;
            }
            set
            {
                this.trainingCenterField = value;
            }
        }

        /// <remarks/>
        public bool supportCenter
        {
            get
            {
                return this.supportCenterField;
            }
            set
            {
                this.supportCenterField = value;
            }
        }

        /// <remarks/>
        public bool eventCenter
        {
            get
            {
                return this.eventCenterField;
            }
            set
            {
                this.eventCenterField = value;
            }
        }

        /// <remarks/>
        public bool salesCenter
        {
            get
            {
                return this.salesCenterField;
            }
            set
            {
                this.salesCenterField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user", IsNullable = false)]
    public partial class myWebEx
    {

        private bool? isMyWebExProField;

        private bool? myContactField;

        private bool? myProfileField;

        private bool? myMeetingsField;

        private bool? myFoldersField;

        private bool? trainingRecordingsField;

        private bool? recordedEventsField;

        private uint? totalStorageSizeField;

        private bool? myReportsField;

        private int? myComputerField;

        private bool? personalMeetingRoomField;

        private bool? myPartnerLinksField;

        private bool? myWorkspacesField;



        private int additionalRecordingStorageField;

        /// <remarks/>
        public bool? isMyWebExPro
        {
            get
            {
                return this.isMyWebExProField;
            }
            set
            {
                this.isMyWebExProField = value;
            }
        }

        /// <remarks/>
        public bool? myContact
        {
            get
            {
                return this.myContactField;
            }
            set
            {
                this.myContactField = value;
            }
        }

        /// <remarks/>
        public bool? myProfile
        {
            get
            {
                return this.myProfileField;
            }
            set
            {
                this.myProfileField = value;
            }
        }

        /// <remarks/>
        public bool? myMeetings
        {
            get
            {
                return this.myMeetingsField;
            }
            set
            {
                this.myMeetingsField = value;
            }
        }

        /// <remarks/>
        public bool? myFolders
        {
            get
            {
                return this.myFoldersField;
            }
            set
            {
                this.myFoldersField = value;
            }
        }

        /// <remarks/>
        public bool? trainingRecordings
        {
            get
            {
                return this.trainingRecordingsField;
            }
            set
            {
                this.trainingRecordingsField = value;
            }
        }

        /// <remarks/>
        public bool? recordedEvents
        {
            get
            {
                return this.recordedEventsField;
            }
            set
            {
                this.recordedEventsField = value;
            }
        }

        /// <remarks/>
        public uint? totalStorageSize
        {
            get
            {
                return this.totalStorageSizeField;
            }
            set
            {
                this.totalStorageSizeField = value;
            }
        }

        /// <remarks/>
        public bool? myReports
        {
            get
            {
                return this.myReportsField;
            }
            set
            {
                this.myReportsField = value;
            }
        }

        /// <remarks/>
        public int? myComputer
        {
            get
            {
                return this.myComputerField;
            }
            set
            {
                this.myComputerField = value;
            }
        }

        /// <remarks/>
        public bool? personalMeetingRoom
        {
            get
            {
                return this.personalMeetingRoomField;
            }
            set
            {
                this.personalMeetingRoomField = value;
            }
        }

        /// <remarks/>
        public bool? myPartnerLinks
        {
            get
            {
                return this.myPartnerLinksField;
            }
            set
            {
                this.myPartnerLinksField = value;
            }
        }

        /// <remarks/>
        public bool? myWorkspaces
        {
            get
            {
                return this.myWorkspacesField;
            }
            set
            {
                this.myWorkspacesField = value;
            }
        }

        /// <remarks/>
        public int additionalRecordingStorage
        {
            get
            {
                return this.additionalRecordingStorageField;
            }
            set
            {
                this.additionalRecordingStorageField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user", IsNullable = false)]
    public partial class personalTeleconf
    {

        private personalTeleconfAccount accountField;

        /// <remarks/>
        public personalTeleconfAccount account
        {
            get
            {
                return this.accountField;
            }
            set
            {
                this.accountField = value;
            }
        }

        private bool joinBeforeHostField;

        /// <remarks/>
        public bool joinBeforeHost
        {
            get
            {
                return this.joinBeforeHostField;
            }
            set
            {
                this.joinBeforeHostField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
    public partial class personalTeleconfAccount
    {

        private string subscriberAccessCodeField;

        private string participantFullAccessCodeField;

        private string participantLimitedAccessCodeField;

        private int accountIndexField;

        private bool? defaultFlagField;

        private bool? joinBeforeHostField;

        /// <remarks/>
        public string subscriberAccessCode
        {
            get
            {
                return this.subscriberAccessCodeField;
            }
            set
            {
                this.subscriberAccessCodeField = value;
            }
        }

        /// <remarks/>
        public string participantFullAccessCode
        {
            get
            {
                return this.participantFullAccessCodeField;
            }
            set
            {
                this.participantFullAccessCodeField = value;
            }
        }

        /// <remarks/>
        public string participantLimitedAccessCode
        {
            get
            {
                return this.participantLimitedAccessCodeField;
            }
            set
            {
                this.participantLimitedAccessCodeField = value;
            }
        }

        /// <remarks/>
        public int accountIndex
        {
            get
            {
                return this.accountIndexField;
            }
            set
            {
                this.accountIndexField = value;
            }
        }

        /// <remarks/>
        public bool? defaultFlag
        {
            get
            {
                return this.defaultFlagField;
            }
            set
            {
                this.defaultFlagField = value;
            }
        }

        /// <remarks/>
        public bool? joinBeforeHost
        {
            get
            {
                return this.joinBeforeHostField;
            }
            set
            {
                this.joinBeforeHostField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user", IsNullable = false)]
    public partial class personalMeetingRoom
    {

        private string welcomeMessageField;

        private string photoURLField;

        private string titleField;

        private string personalMeetingRoomURLField;

        private string sipURLField;

        private string accessCodeField;

        private uint hostPINField;

        private bool applyPMRForInstantMeetingField;

        private bool pMRAutoLockField;

        private int pMRAutoLockWaitTimeField;

        private personalMeetingRoomPRNotifications pRNotificationsField;

        private bool pMRAlternateHostField;

        private personalMeetingRoomAlternateHost alternateHostField;


        /// <remarks/>
        public string welcomeMessage
        {
            get
            {
                return this.welcomeMessageField;
            }
            set
            {
                this.welcomeMessageField = value;
            }
        }

        /// <remarks/>
        public string photoURL
        {
            get
            {
                return this.photoURLField;
            }
            set
            {
                this.photoURLField = value;
            }
        }

        /// <remarks/>
        public string title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>
        public string personalMeetingRoomURL
        {
            get
            {
                return this.personalMeetingRoomURLField;
            }
            set
            {
                this.personalMeetingRoomURLField = value;
            }
        }

        /// <remarks/>
        public string sipURL
        {
            get
            {
                return this.sipURLField;
            }
            set
            {
                this.sipURLField = value;
            }
        }

        /// <remarks/>
        public string accessCode
        {
            get
            {
                return this.accessCodeField;
            }
            set
            {
                this.accessCodeField = value;
            }
        }

        /// <remarks/>
        public uint hostPIN
        {
            get
            {
                return this.hostPINField;
            }
            set
            {
                this.hostPINField = value;
            }
        }

        /// <remarks/>
        public bool applyPMRForInstantMeeting
        {
            get
            {
                return this.applyPMRForInstantMeetingField;
            }
            set
            {
                this.applyPMRForInstantMeetingField = value;
            }
        }

        /// <remarks/>
        public bool PMRAutoLock
        {
            get
            {
                return this.pMRAutoLockField;
            }
            set
            {
                this.pMRAutoLockField = value;
            }
        }

        /// <remarks/>
        public int PMRAutoLockWaitTime
        {
            get
            {
                return this.pMRAutoLockWaitTimeField;
            }
            set
            {
                this.pMRAutoLockWaitTimeField = value;
            }
        }

        /// <remarks/>
        public personalMeetingRoomPRNotifications PRNotifications
        {
            get
            {
                return this.pRNotificationsField;
            }
            set
            {
                this.pRNotificationsField = value;
            }
        }

        /// <remarks/>
        public bool PMRAlternateHost
        {
            get
            {
                return this.pMRAlternateHostField;
            }
            set
            {
                this.pMRAlternateHostField = value;
            }
        }

        /// <remarks/>
        public personalMeetingRoomAlternateHost alternateHost
        {
            get
            {
                return this.alternateHostField;
            }
            set
            {
                this.alternateHostField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
    public partial class personalMeetingRoomPRNotifications
    {

        private string modeField;

        /// <remarks/>
        public string mode
        {
            get
            {
                return this.modeField;
            }
            set
            {
                this.modeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
    public partial class personalMeetingRoomAlternateHost
    {

        private bool allowAnyoneHostMyPMRField;

        /// <remarks/>
        public bool allowAnyoneHostMyPMR
        {
            get
            {
                return this.allowAnyoneHostMyPMRField;
            }
            set
            {
                this.allowAnyoneHostMyPMRField = value;
            }
        }
    }
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user", IsNullable = false)]
    public partial class sessionOptions
    {

        private int defaultSessionTypeField;

        private string defaultServiceTypeField;

        private bool? autoDeleteAfterMeetingEndField;

        private bool? displayQuickStartHostField;

        private bool? displayQuickStartAttendeesField;

        /// <remarks/>
        public int defaultSessionType
        {
            get
            {
                return this.defaultSessionTypeField;
            }
            set
            {
                this.defaultSessionTypeField = value;
            }
        }

        /// <remarks/>
        public string defaultServiceType
        {
            get
            {
                return this.defaultServiceTypeField;
            }
            set
            {
                this.defaultServiceTypeField = value;
            }
        }

        /// <remarks/>
        public bool? autoDeleteAfterMeetingEnd
        {
            get
            {
                return this.autoDeleteAfterMeetingEndField;
            }
            set
            {
                this.autoDeleteAfterMeetingEndField = value;
            }
        }

        /// <remarks/>
        public bool? displayQuickStartHost
        {
            get
            {
                return this.displayQuickStartHostField;
            }
            set
            {
                this.displayQuickStartHostField = value;
            }
        }

        /// <remarks/>
        public bool? displayQuickStartAttendees
        {
            get
            {
                return this.displayQuickStartAttendeesField;
            }
            set
            {
                this.displayQuickStartAttendeesField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user", IsNullable = false)]
    public partial class supportCenter
    {

        private string[] orderTabsField;

        private supportCenterServiceDesk serviceDeskField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("tab", IsNullable = false)]
        public string[] orderTabs
        {
            get
            {
                return this.orderTabsField;
            }
            set
            {
                this.orderTabsField = value;
            }
        }

        /// <remarks/>
        public supportCenterServiceDesk serviceDesk
        {
            get
            {
                return this.serviceDeskField;
            }
            set
            {
                this.serviceDeskField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
    public partial class supportCenterServiceDesk
    {

        private bool? enableField;

        /// <remarks/>
        public bool? enable
        {
            get
            {
                return this.enableField;
            }
            set
            {
                this.enableField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user", IsNullable = false)]
    public partial class security
    {

        private bool? forceChangePasswordField;

        private bool? resetPasswordField;

        private bool? lockAccountField;

        /// <remarks/>
        public bool? forceChangePassword
        {
            get
            {
                return this.forceChangePasswordField;
            }
            set
            {
                this.forceChangePasswordField = value;
            }
        }

        /// <remarks/>
        public bool? resetPassword
        {
            get
            {
                return this.resetPasswordField;
            }
            set
            {
                this.resetPasswordField = value;
            }
        }

        /// <remarks/>
        public bool? lockAccount
        {
            get
            {
                return this.lockAccountField;
            }
            set
            {
                this.lockAccountField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user", IsNullable = false)]
    public partial class webACDPrefs
    {

        private bool? isAgentField;

        private bool? isMgrField;

        private int numAgentSessionsField;

        private bool? agentMonitorAllRSQueuesField;

        private bool? managerMonitorAllRSQueuesField;

        private bool? monitorAllRSAgentsField;

        /// <remarks/>
        public bool? isAgent
        {
            get
            {
                return this.isAgentField;
            }
            set
            {
                this.isAgentField = value;
            }
        }

        /// <remarks/>
        public bool? isMgr
        {
            get
            {
                return this.isMgrField;
            }
            set
            {
                this.isMgrField = value;
            }
        }

        /// <remarks/>
        public int numAgentSessions
        {
            get
            {
                return this.numAgentSessionsField;
            }
            set
            {
                this.numAgentSessionsField = value;
            }
        }

        /// <remarks/>
        public bool? agentMonitorAllRSQueues
        {
            get
            {
                return this.agentMonitorAllRSQueuesField;
            }
            set
            {
                this.agentMonitorAllRSQueuesField = value;
            }
        }

        /// <remarks/>
        public bool? managerMonitorAllRSQueues
        {
            get
            {
                return this.managerMonitorAllRSQueuesField;
            }
            set
            {
                this.managerMonitorAllRSQueuesField = value;
            }
        }

        /// <remarks/>
        public bool? monitorAllRSAgents
        {
            get
            {
                return this.monitorAllRSAgentsField;
            }
            set
            {
                this.monitorAllRSAgentsField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user", IsNullable = false)]
    public partial class salesCenter
    {

        private salesCenterRoles rolesField;

        /// <remarks/>
        public salesCenterRoles roles
        {
            get
            {
                return this.rolesField;
            }
            set
            {
                this.rolesField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
    public partial class salesCenterRoles
    {

        private bool? repField;

        private bool? mgrField;

        private bool? asstField;

        private bool? smeField;

        /// <remarks/>
        public bool? rep
        {
            get
            {
                return this.repField;
            }
            set
            {
                this.repField = value;
            }
        }

        /// <remarks/>
        public bool? mgr
        {
            get
            {
                return this.mgrField;
            }
            set
            {
                this.mgrField = value;
            }
        }

        /// <remarks/>
        public bool? asst
        {
            get
            {
                return this.asstField;
            }
            set
            {
                this.asstField = value;
            }
        }

        /// <remarks/>
        public bool? sme
        {
            get
            {
                return this.smeField;
            }
            set
            {
                this.smeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user", IsNullable = false)]
    public partial class scheduleFor
    {

        private string webExIDField;

        /// <remarks/>
        public string webExID
        {
            get
            {
                return this.webExIDField;
            }
            set
            {
                this.webExIDField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user", IsNullable = false)]
    public partial class meetingTypes
    {

        private int[] meetingTypeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("meetingType")]
        public int[] meetingType
        {
            get
            {
                return this.meetingTypeField;
            }
            set
            {
                this.meetingTypeField = value;
            }
        }
    }

    #endregion

    #region lstRecording
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep", IsNullable = false)]
    public partial class recording
    {

        private string recordingIDField;

        private string hostWebExIDField;

        private string nameField;

        private string createTimeField;

        private int timeZoneIDField;

        private decimal sizeField;

        private string streamURLField;

        private string fileURLField;

        private string sessionKeyField;

        private string trackingCodeField;

        private int recordingTypeField;

        private int durationField;

        private string passwordField;

        private string descriptionField;

        /// <remarks/>
        public string password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
            }
        }

        /// <remarks/>
        public string description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }
        /// <remarks/>
        public string recordingID
        {
            get
            {
                return this.recordingIDField;
            }
            set
            {
                this.recordingIDField = value;
            }
        }

        /// <remarks/>
        public string hostWebExID
        {
            get
            {
                return this.hostWebExIDField;
            }
            set
            {
                this.hostWebExIDField = value;
            }
        }

        /// <remarks/>
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public string createTime
        {
            get
            {
                return this.createTimeField;
            }
            set
            {
                this.createTimeField = value;
            }
        }

        /// <remarks/>
        public int timeZoneID
        {
            get
            {
                return this.timeZoneIDField;
            }
            set
            {
                this.timeZoneIDField = value;
            }
        }

        /// <remarks/>
        public decimal size
        {
            get
            {
                return this.sizeField;
            }
            set
            {
                this.sizeField = value;
            }
        }

        /// <remarks/>
        public string streamURL
        {
            get
            {
                return this.streamURLField;
            }
            set
            {
                this.streamURLField = value;
            }
        }

        /// <remarks/>
        public string fileURL
        {
            get
            {
                return this.fileURLField;
            }
            set
            {
                this.fileURLField = value;
            }
        }

        /// <remarks/>
        public string sessionKey
        {
            get
            {
                return this.sessionKeyField;
            }
            set
            {
                this.sessionKeyField = value;
            }
        }

        /// <remarks/>
        public string trackingCode
        {
            get
            {
                return this.trackingCodeField;
            }
            set
            {
                this.trackingCodeField = value;
            }
        }

        /// <remarks/>
        public int recordingType
        {
            get
            {
                return this.recordingTypeField;
            }
            set
            {
                this.recordingTypeField = value;
            }
        }

        /// <remarks/>
        public int duration
        {
            get
            {
                return this.durationField;
            }
            set
            {
                this.durationField = value;
            }
        }
    }
    #endregion

    #region lstsummaryMeeting


    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep", IsNullable = false)]
    public partial class session
    {

        private string sessionKeyField;

        private string confNameField;

        private int sessionTypeField;

        private string serviceTypeField;

        private string hostWebExIDField;

        private string hostFirstNameField;

        private string hostLastNameField;

        private string otherHostWebExIDField;

        private int timeZoneIDField;

        private string timeZoneField;

        private string statusField;

        private string startTimeField;

        private int openTimeField;

        private int durationField;

        private string listStatusField;

        private string hostEmailField;

        private bool? passwordReqField;

        private bool? hostJoinedField;

        private bool? participantsJoinedField;

        private string confIDField;

        private bool? registrationField;

        private bool? isRecurringField;

        private uint hostTypeField;

        /// <remarks/>
        public string sessionKey
        {
            get
            {
                return this.sessionKeyField;
            }
            set
            {
                this.sessionKeyField = value;
            }
        }

        /// <remarks/>
        public string confName
        {
            get
            {
                return this.confNameField;
            }
            set
            {
                this.confNameField = value;
            }
        }

        /// <remarks/>
        public int sessionType
        {
            get
            {
                return this.sessionTypeField;
            }
            set
            {
                this.sessionTypeField = value;
            }
        }

        /// <remarks/>
        public string serviceType
        {
            get
            {
                return this.serviceTypeField;
            }
            set
            {
                this.serviceTypeField = value;
            }
        }

        /// <remarks/>
        public string hostWebExID
        {
            get
            {
                return this.hostWebExIDField;
            }
            set
            {
                this.hostWebExIDField = value;
            }
        }

        /// <remarks/>
        public string hostFirstName
        {
            get
            {
                return this.hostFirstNameField;
            }
            set
            {
                this.hostFirstNameField = value;
            }
        }

        /// <remarks/>
        public string hostLastName
        {
            get
            {
                return this.hostLastNameField;
            }
            set
            {
                this.hostLastNameField = value;
            }
        }

        /// <remarks/>
        public string otherHostWebExID
        {
            get
            {
                return this.otherHostWebExIDField;
            }
            set
            {
                this.otherHostWebExIDField = value;
            }
        }

        /// <remarks/>
        public int timeZoneID
        {
            get
            {
                return this.timeZoneIDField;
            }
            set
            {
                this.timeZoneIDField = value;
            }
        }

        /// <remarks/>
        public string timeZone
        {
            get
            {
                return this.timeZoneField;
            }
            set
            {
                this.timeZoneField = value;
            }
        }

        /// <remarks/>
        public string status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        public string startTime
        {
            get
            {
                return this.startTimeField;
            }
            set
            {
                this.startTimeField = value;
            }
        }

        /// <remarks/>
        public int openTime
        {
            get
            {
                return this.openTimeField;
            }
            set
            {
                this.openTimeField = value;
            }
        }

        /// <remarks/>
        public int duration
        {
            get
            {
                return this.durationField;
            }
            set
            {
                this.durationField = value;
            }
        }

        /// <remarks/>
        public string listStatus
        {
            get
            {
                return this.listStatusField;
            }
            set
            {
                this.listStatusField = value;
            }
        }

        /// <remarks/>
        public string hostEmail
        {
            get
            {
                return this.hostEmailField;
            }
            set
            {
                this.hostEmailField = value;
            }
        }

        /// <remarks/>
        public bool? passwordReq
        {
            get
            {
                return this.passwordReqField;
            }
            set
            {
                this.passwordReqField = value;
            }
        }

        /// <remarks/>
        public bool? hostJoined
        {
            get
            {
                return this.hostJoinedField;
            }
            set
            {
                this.hostJoinedField = value;
            }
        }

        /// <remarks/>
        public bool? participantsJoined
        {
            get
            {
                return this.participantsJoinedField;
            }
            set
            {
                this.participantsJoinedField = value;
            }
        }

        /// <remarks/>
        public string confID
        {
            get
            {
                return this.confIDField;
            }
            set
            {
                this.confIDField = value;
            }
        }

        /// <remarks/>
        public bool? registration
        {
            get
            {
                return this.registrationField;
            }
            set
            {
                this.registrationField = value;
            }
        }

        /// <remarks/>
        public bool? isRecurring
        {
            get
            {
                return this.isRecurringField;
            }
            set
            {
                this.isRecurringField = value;
            }
        }

        /// <remarks/>
        public uint hostType
        {
            get
            {
                return this.hostTypeField;
            }
            set
            {
                this.hostTypeField = value;
            }
        }
    }
    #endregion

    #region GetOneClickSettings

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep", IsNullable = false)]
    public partial class metaData
    {

        private string serviceTypeField;

        private int sessionTypeField;

        private string confNameField;

        private string sessionPasswordField;

        private string listingField;

        private bool isRecurringField;

        private metaDataSessionTemplate sessionTemplateField;

        private int meetingTypeField;

        private string agendaField;

        private string invitationField;

        public string invitation
        {
            get
            {
                return this.invitationField;
            }
            set
            {
                this.invitationField = value;
            }
        }

        public int meetingType
        {
            get
            {
                return this.meetingTypeField;
            }
            set
            {
                this.meetingTypeField = value;
            }
        }

        public string agenda
        {
            get
            {
                return this.agendaField;
            }
            set
            {
                this.agendaField = value;
            }
        }
        /// <remarks/>
        public string serviceType
        {
            get
            {
                return this.serviceTypeField;
            }
            set
            {
                this.serviceTypeField = value;
            }
        }

        /// <remarks/>
        public int sessionType
        {
            get
            {
                return this.sessionTypeField;
            }
            set
            {
                this.sessionTypeField = value;
            }
        }

        /// <remarks/>
        public string confName
        {
            get
            {
                return this.confNameField;
            }
            set
            {
                this.confNameField = value;
            }
        }

        /// <remarks/>
        public string sessionPassword
        {
            get
            {
                return this.sessionPasswordField;
            }
            set
            {
                this.sessionPasswordField = value;
            }
        }

        /// <remarks/>
        public string listing
        {
            get
            {
                return this.listingField;
            }
            set
            {
                this.listingField = value;
            }
        }

        /// <remarks/>
        public metaDataSessionTemplate sessionTemplate
        {
            get
            {
                return this.sessionTemplateField;
            }
            set
            {
                this.sessionTemplateField = value;
            }
        }

        public bool isRecurring
        {
            get
            {
                return this.isRecurringField;
            }
            set
            {
                this.isRecurringField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
    public partial class metaDataSessionTemplate
    {

        private string nameField;

        private string typeField;

        private string valueField;

        /// <remarks/>
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public string type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
    public partial class trackingCodesTrackingCode
    {

        private int indexField;

        private string nameField;

        private string inputModeField;

        private string valueField;

        /// <remarks/>
        public int index
        {
            get
            {
                return this.indexField;
            }
            set
            {
                this.indexField = value;
            }
        }

        /// <remarks/>
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public string inputMode
        {
            get
            {
                return this.inputModeField;
            }
            set
            {
                this.inputModeField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep", IsNullable = false)]
    public partial class telephony
    {

        private string telephonySupportField;

        private int? tspAccountIndexField;

        private telephonyAccount accountField;

        private telephonyAccountLabel accountLabelField;

        private string teleconfServiceNameField;

        private bool? intlLocalCallInField;

        private bool? tollfreeField;

        private string entryExitToneField;

        private string extTelephonyDescriptionField;

        public string extTelephonyDescription
        {
            get { return this.extTelephonyDescriptionField; }
            set { extTelephonyDescriptionField = value; }
        }

        private uint? numPhoneLinesField;



        private bool? enableTSPField;


        private string callInNumField;


        public string callInNum
        {
            get
            {
                return this.callInNumField;
            }
            set
            {
                this.callInNumField = value;
            }
        }
        /// <remarks/>
        public string telephonySupport
        {
            get
            {
                return this.telephonySupportField;
            }
            set
            {
                this.telephonySupportField = value;
            }
        }

        /// <remarks/>
        public uint? numPhoneLines
        {
            get
            {
                return this.numPhoneLinesField;
            }
            set
            {
                this.numPhoneLinesField = value;
            }
        }



        /// <remarks/>
        public bool? enableTSP
        {
            get
            {
                return this.enableTSPField;
            }
            set
            {
                this.enableTSPField = value;
            }
        }




        /// <remarks/>
        public int? tspAccountIndex
        {
            get
            {
                return this.tspAccountIndexField;
            }
            set
            {
                this.tspAccountIndexField = value;
            }
        }

        /// <remarks/>
        public telephonyAccount account
        {
            get
            {
                return this.accountField;
            }
            set
            {
                this.accountField = value;
            }
        }

        /// <remarks/>
        public telephonyAccountLabel accountLabel
        {
            get
            {
                return this.accountLabelField;
            }
            set
            {
                this.accountLabelField = value;
            }
        }

        /// <remarks/>
        public string teleconfServiceName
        {
            get
            {
                return this.teleconfServiceNameField;
            }
            set
            {
                this.teleconfServiceNameField = value;
            }
        }

        /// <remarks/>
        public bool? intlLocalCallIn
        {
            get
            {
                return this.intlLocalCallInField;
            }
            set
            {
                this.intlLocalCallInField = value;
            }
        }

        /// <remarks/>
        public bool? tollfree
        {
            get
            {
                return this.tollfreeField;
            }
            set
            {
                this.tollfreeField = value;
            }
        }

        /// <remarks/>
        public string entryExitTone
        {
            get
            {
                return this.entryExitToneField;
            }
            set
            {
                this.entryExitToneField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
    public partial class telephonyAccount
    {

        private string tollFreeCallInNumberField;

        private string tollCallInNumberField;

        private string subscriberAccessCodeField;

        private string participantAccessCodeField;

        private string participantLimitedAccessCodeField;

        private telephonyAccountGlobalNum[] globalNumField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollFreeCallInNumber
        {
            get
            {
                return this.tollFreeCallInNumberField;
            }
            set
            {
                this.tollFreeCallInNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollCallInNumber
        {
            get
            {
                return this.tollCallInNumberField;
            }
            set
            {
                this.tollCallInNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string subscriberAccessCode
        {
            get
            {
                return this.subscriberAccessCodeField;
            }
            set
            {
                this.subscriberAccessCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string participantAccessCode
        {
            get
            {
                return this.participantAccessCodeField;
            }
            set
            {
                this.participantAccessCodeField = value;
            }
        }

        /// <remarks/>
        public string participantLimitedAccessCode
        {
            get
            {
                return this.participantLimitedAccessCodeField;
            }
            set
            {
                this.participantLimitedAccessCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("globalNum")]
        public telephonyAccountGlobalNum[] globalNum
        {
            get
            {
                return this.globalNumField;
            }
            set
            {
                this.globalNumField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
    public partial class telephonyAccountGlobalNum
    {

        private string countryAliasField;

        private string phoneNumberField;

        private bool? tollFreeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string countryAlias
        {
            get
            {
                return this.countryAliasField;
            }
            set
            {
                this.countryAliasField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string phoneNumber
        {
            get
            {
                return this.phoneNumberField;
            }
            set
            {
                this.phoneNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public bool? tollFree
        {
            get
            {
                return this.tollFreeField;
            }
            set
            {
                this.tollFreeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
    public partial class telephonyAccountLabel
    {

        private string tollFreeCallInNumberLabelField;

        private string tollCallInNumberLabelField;

        private string subscriberAccessCodeLabelField;

        private string participantAccessCodeLabelField;

        private string participantLimitedAccessCodeLabelField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollFreeCallInNumberLabel
        {
            get
            {
                return this.tollFreeCallInNumberLabelField;
            }
            set
            {
                this.tollFreeCallInNumberLabelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollCallInNumberLabel
        {
            get
            {
                return this.tollCallInNumberLabelField;
            }
            set
            {
                this.tollCallInNumberLabelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string subscriberAccessCodeLabel
        {
            get
            {
                return this.subscriberAccessCodeLabelField;
            }
            set
            {
                this.subscriberAccessCodeLabelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string participantAccessCodeLabel
        {
            get
            {
                return this.participantAccessCodeLabelField;
            }
            set
            {
                this.participantAccessCodeLabelField = value;
            }
        }

        /// <remarks/>
        public string participantLimitedAccessCodeLabel
        {
            get
            {
                return this.participantLimitedAccessCodeLabelField;
            }
            set
            {
                this.participantLimitedAccessCodeLabelField = value;
            }
        }
    }





    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting", IsNullable = false)]
    public partial class repeat
    {

        private string repeatTypeField;

        private byte intervalField;

        private byte afterMeetingNumberField;

        private string[] dayInWeekField;

        private byte dayInMonthField;

        private byte weekInMonthField;

        private string monthInYearField;

        private string expirationDateField;

        /// <remarks/>
        public string repeatType
        {
            get
            {
                return this.repeatTypeField;
            }
            set
            {
                this.repeatTypeField = value;
            }
        }

        /// <remarks/>
        public byte interval
        {
            get
            {
                return this.intervalField;
            }
            set
            {
                this.intervalField = value;
            }
        }

        /// <remarks/>
        public byte afterMeetingNumber
        {
            get
            {
                return this.afterMeetingNumberField;
            }
            set
            {
                this.afterMeetingNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("day", IsNullable = false)]
        public string[] dayInWeek
        {
            get
            {
                return this.dayInWeekField;
            }
            set
            {
                this.dayInWeekField = value;
            }
        }

        /// <remarks/>
        public byte dayInMonth
        {
            get
            {
                return this.dayInMonthField;
            }
            set
            {
                this.dayInMonthField = value;
            }
        }

        /// <remarks/>
        public byte weekInMonth
        {
            get
            {
                return this.weekInMonthField;
            }
            set
            {
                this.weekInMonthField = value;
            }
        }

        /// <remarks/>
        public string monthInYear
        {
            get
            {
                return this.monthInYearField;
            }
            set
            {
                this.monthInYearField = value;
            }
        }

        /// <remarks/>
        public string expirationDate
        {
            get
            {
                return this.expirationDateField;
            }
            set
            {
                this.expirationDateField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting", IsNullable = false)]
    public partial class remind
    {

        private bool? enableReminderField;

        private remindEmails emailsField;

        private bool? sendEmailField;

        private bool? sendMobileField;

        private int? daysAheadField;

        private int? hoursAheadField;

        private int? minutesAheadField;

        /// <remarks/>
        public bool? enableReminder
        {
            get
            {
                return this.enableReminderField;
            }
            set
            {
                this.enableReminderField = value;
            }
        }

        /// <remarks/>
        public remindEmails emails
        {
            get
            {
                return this.emailsField;
            }
            set
            {
                this.emailsField = value;
            }
        }

        /// <remarks/>
        public bool? sendEmail
        {
            get
            {
                return this.sendEmailField;
            }
            set
            {
                this.sendEmailField = value;
            }
        }

        /// <remarks/>
        public bool? sendMobile
        {
            get
            {
                return this.sendMobileField;
            }
            set
            {
                this.sendMobileField = value;
            }
        }

        /// <remarks/>
        public int? daysAhead
        {
            get
            {
                return this.daysAheadField;
            }
            set
            {
                this.daysAheadField = value;
            }
        }

        /// <remarks/>
        public int? hoursAhead
        {
            get
            {
                return this.hoursAheadField;
            }
            set
            {
                this.hoursAheadField = value;
            }
        }

        /// <remarks/>
        public int? minutesAhead
        {
            get
            {
                return this.minutesAheadField;
            }
            set
            {
                this.minutesAheadField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
    public partial class remindEmails
    {

        private string emailField;

        /// <remarks/>
        public string email
        {
            get
            {
                return this.emailField;
            }
            set
            {
                this.emailField = value;
            }
        }
    }
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep", IsNullable = false)]
    public partial class enableOptions
    {

        private bool? attendeeListField;

        private bool? fileShareField;

        private bool? presentationField;

        private bool? applicationShareField;

        private bool? desktopShareField;

        private bool? webTourField;

        private bool? meetingRecordField;

        private bool? annotationField;

        private bool? importDocumentField;

        private bool? saveDocumentField;

        private bool? printDocumentField;

        private bool? pointerField;

        private bool? switchPageField;

        private bool? fullScreenField;

        private bool? thumbnailField;

        private bool? zoomField;

        private bool? copyPageField;

        private bool? rcAppShareField;

        private bool? rcDesktopShareField;

        private bool? rcWebTourField;

        private bool? javaClientField;

        private bool? nativeClientField;

        private bool? attendeeRecordMeetingField;


        private bool? faxIntoMeetingField;

        private bool? enableRegField;

        private bool? supportQandAField;

        private bool? supportFeedbackField;

        private bool? supportBreakoutSessionsField;

        private bool? supportPanelistsField;

        private bool? supportRemoteComputerField;

        private bool? supportShareWebContentField;

        private bool? supportUCFWebPagesField;

        private bool? supportUCFRichMediaField;

        private bool? autoDeleteAfterMeetingEndField;

        private bool? viewAnyDocField;

        private bool? viewAnyPageField;

        private bool? allowContactPrivateField;

        private bool? chatHostField;

        private bool? chatPresenterField;

        private bool? chatAllAttendeesField;

        private bool? multiVideoField;

        private bool? notesField;

        private bool? closedCaptionsField;

        private bool? singleNoteField;

        private bool? sendFeedbackField;

        private bool? displayQuickStartHostField;

        private bool? displayQuickStartAttendeesField;

        private bool? supportE2EField;

        private bool? supportPKIField;
        private bool? voipField;

        /// <remarks/>
        public bool? voip
        {
            get
            {
                return this.voipField;
            }
            set
            {
                this.voipField = value;
            }

        }

        private bool? chatField;

        /// <remarks/>
        public bool? chat
        {
            get
            {
                return this.chatField;
            }
            set
            {
                this.chatField = value;
            }
        }

        private bool? pollField;

        /// <remarks/>
        public bool? poll
        {
            get
            {
                return this.pollField;
            }
            set
            {
                this.pollField = value;
            }
        }


        private bool? audioVideoField;

        /// <remarks/>
        public bool? audioVideo
        {
            get
            {
                return this.audioVideoField;
            }
            set
            {
                this.audioVideoField = value;
            }
        }

        /// <remarks/>
        public bool? attendeeList
        {
            get
            {
                return this.attendeeListField;
            }
            set
            {
                this.attendeeListField = value;
            }
        }

        /// <remarks/>
        public bool? fileShare
        {
            get
            {
                return this.fileShareField;
            }
            set
            {
                this.fileShareField = value;
            }
        }

        /// <remarks/>
        public bool? presentation
        {
            get
            {
                return this.presentationField;
            }
            set
            {
                this.presentationField = value;
            }
        }

        /// <remarks/>
        public bool? applicationShare
        {
            get
            {
                return this.applicationShareField;
            }
            set
            {
                this.applicationShareField = value;
            }
        }

        /// <remarks/>
        public bool? desktopShare
        {
            get
            {
                return this.desktopShareField;
            }
            set
            {
                this.desktopShareField = value;
            }
        }

        /// <remarks/>
        public bool? webTour
        {
            get
            {
                return this.webTourField;
            }
            set
            {
                this.webTourField = value;
            }
        }

        /// <remarks/>
        public bool? meetingRecord
        {
            get
            {
                return this.meetingRecordField;
            }
            set
            {
                this.meetingRecordField = value;
            }
        }

        /// <remarks/>
        public bool? annotation
        {
            get
            {
                return this.annotationField;
            }
            set
            {
                this.annotationField = value;
            }
        }

        /// <remarks/>
        public bool? importDocument
        {
            get
            {
                return this.importDocumentField;
            }
            set
            {
                this.importDocumentField = value;
            }
        }

        /// <remarks/>
        public bool? saveDocument
        {
            get
            {
                return this.saveDocumentField;
            }
            set
            {
                this.saveDocumentField = value;
            }
        }

        /// <remarks/>
        public bool? printDocument
        {
            get
            {
                return this.printDocumentField;
            }
            set
            {
                this.printDocumentField = value;
            }
        }

        /// <remarks/>
        public bool? pointer
        {
            get
            {
                return this.pointerField;
            }
            set
            {
                this.pointerField = value;
            }
        }

        /// <remarks/>
        public bool? switchPage
        {
            get
            {
                return this.switchPageField;
            }
            set
            {
                this.switchPageField = value;
            }
        }

        /// <remarks/>
        public bool? fullScreen
        {
            get
            {
                return this.fullScreenField;
            }
            set
            {
                this.fullScreenField = value;
            }
        }

        /// <remarks/>
        public bool? thumbnail
        {
            get
            {
                return this.thumbnailField;
            }
            set
            {
                this.thumbnailField = value;
            }
        }

        /// <remarks/>
        public bool? zoom
        {
            get
            {
                return this.zoomField;
            }
            set
            {
                this.zoomField = value;
            }
        }

        /// <remarks/>
        public bool? copyPage
        {
            get
            {
                return this.copyPageField;
            }
            set
            {
                this.copyPageField = value;
            }
        }

        /// <remarks/>
        public bool? rcAppShare
        {
            get
            {
                return this.rcAppShareField;
            }
            set
            {
                this.rcAppShareField = value;
            }
        }

        /// <remarks/>
        public bool? rcDesktopShare
        {
            get
            {
                return this.rcDesktopShareField;
            }
            set
            {
                this.rcDesktopShareField = value;
            }
        }

        /// <remarks/>
        public bool? rcWebTour
        {
            get
            {
                return this.rcWebTourField;
            }
            set
            {
                this.rcWebTourField = value;
            }
        }

        /// <remarks/>
        public bool? javaClient
        {
            get
            {
                return this.javaClientField;
            }
            set
            {
                this.javaClientField = value;
            }
        }

        /// <remarks/>
        public bool? nativeClient
        {
            get
            {
                return this.nativeClientField;
            }
            set
            {
                this.nativeClientField = value;
            }
        }

        /// <remarks/>
        public bool? attendeeRecordMeeting
        {
            get
            {
                return this.attendeeRecordMeetingField;
            }
            set
            {
                this.attendeeRecordMeetingField = value;
            }
        }



        /// <remarks/>
        public bool? faxIntoMeeting
        {
            get
            {
                return this.faxIntoMeetingField;
            }
            set
            {
                this.faxIntoMeetingField = value;
            }
        }

        /// <remarks/>
        public bool? enableReg
        {
            get
            {
                return this.enableRegField;
            }
            set
            {
                this.enableRegField = value;
            }
        }

        /// <remarks/>
        public bool? supportQandA
        {
            get
            {
                return this.supportQandAField;
            }
            set
            {
                this.supportQandAField = value;
            }
        }

        /// <remarks/>
        public bool? supportFeedback
        {
            get
            {
                return this.supportFeedbackField;
            }
            set
            {
                this.supportFeedbackField = value;
            }
        }

        /// <remarks/>
        public bool? supportBreakoutSessions
        {
            get
            {
                return this.supportBreakoutSessionsField;
            }
            set
            {
                this.supportBreakoutSessionsField = value;
            }
        }

        /// <remarks/>
        public bool? supportPanelists
        {
            get
            {
                return this.supportPanelistsField;
            }
            set
            {
                this.supportPanelistsField = value;
            }
        }

        /// <remarks/>
        public bool? supportRemoteComputer
        {
            get
            {
                return this.supportRemoteComputerField;
            }
            set
            {
                this.supportRemoteComputerField = value;
            }
        }

        /// <remarks/>
        public bool? supportShareWebContent
        {
            get
            {
                return this.supportShareWebContentField;
            }
            set
            {
                this.supportShareWebContentField = value;
            }
        }

        /// <remarks/>
        public bool? supportUCFWebPages
        {
            get
            {
                return this.supportUCFWebPagesField;
            }
            set
            {
                this.supportUCFWebPagesField = value;
            }
        }

        /// <remarks/>
        public bool? supportUCFRichMedia
        {
            get
            {
                return this.supportUCFRichMediaField;
            }
            set
            {
                this.supportUCFRichMediaField = value;
            }
        }

        /// <remarks/>
        public bool? autoDeleteAfterMeetingEnd
        {
            get
            {
                return this.autoDeleteAfterMeetingEndField;
            }
            set
            {
                this.autoDeleteAfterMeetingEndField = value;
            }
        }

        /// <remarks/>
        public bool? viewAnyDoc
        {
            get
            {
                return this.viewAnyDocField;
            }
            set
            {
                this.viewAnyDocField = value;
            }
        }

        /// <remarks/>
        public bool? viewAnyPage
        {
            get
            {
                return this.viewAnyPageField;
            }
            set
            {
                this.viewAnyPageField = value;
            }
        }

        /// <remarks/>
        public bool? allowContactPrivate
        {
            get
            {
                return this.allowContactPrivateField;
            }
            set
            {
                this.allowContactPrivateField = value;
            }
        }

        /// <remarks/>
        public bool? chatHost
        {
            get
            {
                return this.chatHostField;
            }
            set
            {
                this.chatHostField = value;
            }
        }

        /// <remarks/>
        public bool? chatPresenter
        {
            get
            {
                return this.chatPresenterField;
            }
            set
            {
                this.chatPresenterField = value;
            }
        }

        /// <remarks/>
        public bool? chatAllAttendees
        {
            get
            {
                return this.chatAllAttendeesField;
            }
            set
            {
                this.chatAllAttendeesField = value;
            }
        }

        /// <remarks/>
        public bool? multiVideo
        {
            get
            {
                return this.multiVideoField;
            }
            set
            {
                this.multiVideoField = value;
            }
        }

        /// <remarks/>
        public bool? notes
        {
            get
            {
                return this.notesField;
            }
            set
            {
                this.notesField = value;
            }
        }

        /// <remarks/>
        public bool? closedCaptions
        {
            get
            {
                return this.closedCaptionsField;
            }
            set
            {
                this.closedCaptionsField = value;
            }
        }

        /// <remarks/>
        public bool? singleNote
        {
            get
            {
                return this.singleNoteField;
            }
            set
            {
                this.singleNoteField = value;
            }
        }

        /// <remarks/>
        public bool? sendFeedback
        {
            get
            {
                return this.sendFeedbackField;
            }
            set
            {
                this.sendFeedbackField = value;
            }
        }

        /// <remarks/>
        public bool? displayQuickStartHost
        {
            get
            {
                return this.displayQuickStartHostField;
            }
            set
            {
                this.displayQuickStartHostField = value;
            }
        }

        /// <remarks/>
        public bool? displayQuickStartAttendees
        {
            get
            {
                return this.displayQuickStartAttendeesField;
            }
            set
            {
                this.displayQuickStartAttendeesField = value;
            }
        }

        /// <remarks/>
        public bool? supportE2E
        {
            get
            {
                return this.supportE2EField;
            }
            set
            {
                this.supportE2EField = value;
            }
        }

        /// <remarks/>
        public bool? supportPKI
        {
            get
            {
                return this.supportPKIField;
            }
            set
            {
                this.supportPKIField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep", IsNullable = false)]
    public partial class attendeeOptions
    {

        private bool? joinApprovalField;

        /// <remarks/>
        public bool? joinApproval
        {
            get
            {
                return this.joinApprovalField;
            }
            set
            {
                this.joinApprovalField = value;
            }
        }

        private bool? requestField;

        private bool? registrationField;

        private bool? autoField;

        private bool? excludePasswordField;

        private bool? joinRequiresAccountField;

        private bool? emailInvitationsField;

        public bool? emailInvitations
        {
            get
            {
                return this.emailInvitationsField;
            }
            set
            {
                this.emailInvitationsField = value;
            }
        }
        /// <remarks/>
        public bool? request
        {
            get
            {
                return this.requestField;
            }
            set
            {
                this.requestField = value;
            }
        }

        /// <remarks/>
        public bool? registration
        {
            get
            {
                return this.registrationField;
            }
            set
            {
                this.registrationField = value;
            }
        }

        /// <remarks/>
        public bool? auto
        {
            get
            {
                return this.autoField;
            }
            set
            {
                this.autoField = value;
            }
        }

        /// <remarks/>
        public bool? excludePassword
        {
            get
            {
                return this.excludePasswordField;
            }
            set
            {
                this.excludePasswordField = value;
            }
        }

        /// <remarks/>
        public bool? joinRequiresAccount
        {
            get
            {
                return this.joinRequiresAccountField;
            }
            set
            {
                this.joinRequiresAccountField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep", IsNullable = false)]
    public partial class trackingCodes
    {

        private trackingCodesTrackingCode[] trackingCodeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("trackingCode")]
        public trackingCodesTrackingCode[] trackingCode
        {
            get
            {
                return this.trackingCodeField;
            }
            set
            {
                this.trackingCodeField = value;
            }
        }
    }


    #endregion

    #region GetRecordingInfo
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep", IsNullable = false)]
    public partial class basic
    {

        private string topicField;

        private string listingField;

        private string presenterField;

        private string emailField;

        private string agendaField;

        /// <remarks/>
        public string topic
        {
            get
            {
                return this.topicField;
            }
            set
            {
                this.topicField = value;
            }
        }

        /// <remarks/>
        public string listing
        {
            get
            {
                return this.listingField;
            }
            set
            {
                this.listingField = value;
            }
        }

        /// <remarks/>
        public string presenter
        {
            get
            {
                return this.presenterField;
            }
            set
            {
                this.presenterField = value;
            }
        }

        /// <remarks/>
        public string email
        {
            get
            {
                return this.emailField;
            }
            set
            {
                this.emailField = value;
            }
        }

        /// <remarks/>
        public string agenda
        {
            get
            {
                return this.agendaField;
            }
            set
            {
                this.agendaField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep", IsNullable = false)]
    public partial class playback
    {

        private bool? chatField;

        private bool? supportQandAField;

        private bool? videoField;

        private bool? pollingField;

        private bool? notesField;

        private bool? fileShareField;

        private bool? tocField;

        private bool? attendeeListField;

        private bool? includeNBRcontrolsField;

        private string rangeField;

        private int? partialStartField;

        private int? partialEndField;

        /// <remarks/>
        public bool? chat
        {
            get
            {
                return this.chatField;
            }
            set
            {
                this.chatField = value;
            }
        }

        /// <remarks/>
        public bool? supportQandA
        {
            get
            {
                return this.supportQandAField;
            }
            set
            {
                this.supportQandAField = value;
            }
        }

        /// <remarks/>
        public bool? video
        {
            get
            {
                return this.videoField;
            }
            set
            {
                this.videoField = value;
            }
        }

        /// <remarks/>
        public bool? polling
        {
            get
            {
                return this.pollingField;
            }
            set
            {
                this.pollingField = value;
            }
        }

        /// <remarks/>
        public bool? notes
        {
            get
            {
                return this.notesField;
            }
            set
            {
                this.notesField = value;
            }
        }

        /// <remarks/>
        public bool? fileShare
        {
            get
            {
                return this.fileShareField;
            }
            set
            {
                this.fileShareField = value;
            }
        }

        /// <remarks/>
        public bool? toc
        {
            get
            {
                return this.tocField;
            }
            set
            {
                this.tocField = value;
            }
        }

        /// <remarks/>
        public bool? attendeeList
        {
            get
            {
                return this.attendeeListField;
            }
            set
            {
                this.attendeeListField = value;
            }
        }

        /// <remarks/>
        public bool? includeNBRcontrols
        {
            get
            {
                return this.includeNBRcontrolsField;
            }
            set
            {
                this.includeNBRcontrolsField = value;
            }
        }

        /// <remarks/>
        public string range
        {
            get
            {
                return this.rangeField;
            }
            set
            {
                this.rangeField = value;
            }
        }

        /// <remarks/>
        public int? partialStart
        {
            get
            {
                return this.partialStartField;
            }
            set
            {
                this.partialStartField = value;
            }
        }

        /// <remarks/>
        public int? partialEnd
        {
            get
            {
                return this.partialEndField;
            }
            set
            {
                this.partialEndField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep", IsNullable = false)]
    public partial class fileAccess
    {

        private string endPlayURLField;

        private bool? registrationField;

        private bool? attendeeViewField;

        private bool? attendeeDownloadField;

        /// <remarks/>
        public string endPlayURL
        {
            get
            {
                return this.endPlayURLField;
            }
            set
            {
                this.endPlayURLField = value;
            }
        }

        /// <remarks/>
        public bool? registration
        {
            get
            {
                return this.registrationField;
            }
            set
            {
                this.registrationField = value;
            }
        }

        /// <remarks/>
        public bool? attendeeView
        {
            get
            {
                return this.attendeeViewField;
            }
            set
            {
                this.attendeeViewField = value;
            }
        }

        /// <remarks/>
        public bool? attendeeDownload
        {
            get
            {
                return this.attendeeDownloadField;
            }
            set
            {
                this.attendeeDownloadField = value;
            }
        }
    }
    #endregion

    #region GetSessionInfo


    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
   // [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
    // [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep", IsNullable = false)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting", IsNullable = false)]
    public partial class accessControl
    {

        private string sessionPasswordField;

        private string listStatusField;

        private bool registrationField;

        private bool passwordReqField;

        private string meetingPasswordField;

        private bool listToPublicField;

        private bool isPublicField;

        public string meetingPassword
        {
            get
            {
                return this.meetingPasswordField;
            }
            set
            {
                this.meetingPasswordField = value;
            }
        }





        /// <remarks/>
        public bool listToPublic
        {
            get
            {
                return this.listToPublicField;
            }
            set
            {
                this.listToPublicField = value;
            }
        }

        /// <remarks/>
        public bool isPublic
        {
            get
            {
                return this.isPublicField;
            }
            set
            {
                this.isPublicField = value;
            }
        }


        /// <remarks/>
        public string sessionPassword
        {
            get
            {
                return this.sessionPasswordField;
            }
            set
            {
                this.sessionPasswordField = value;
            }
        }

        /// <remarks/>
        public string listStatus
        {
            get
            {
                return this.listStatusField;
            }
            set
            {
                this.listStatusField = value;
            }
        }

        /// <remarks/>
        public bool registration
        {
            get
            {
                return this.registrationField;
            }
            set
            {
                this.registrationField = value;
            }
        }

        /// <remarks/>
        public bool passwordReq
        {
            get
            {
                return this.passwordReqField;
            }
            set
            {
                this.passwordReqField = value;
            }
        }
    }



    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep", IsNullable = false)]
    public partial class host
    {

        private string firstNameField;

        private string lastNameField;

        private string emailField;

        private string webExIdField;

        private bool allowAnyoneHostMeetingField;

        /// <remarks/>
        public string firstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string lastName
        {
            get
            {
                return this.lastNameField;
            }
            set
            {
                this.lastNameField = value;
            }
        }

        /// <remarks/>
        public string email
        {
            get
            {
                return this.emailField;
            }
            set
            {
                this.emailField = value;
            }
        }

        /// <remarks/>
        public string webExId
        {
            get
            {
                return this.webExIdField;
            }
            set
            {
                this.webExIdField = value;
            }
        }

        /// <remarks/>
        public bool allowAnyoneHostMeeting
        {
            get
            {
                return this.allowAnyoneHostMeetingField;
            }
            set
            {
                this.allowAnyoneHostMeetingField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/ep")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/ep", IsNullable = false)]
    public partial class schedule
    {

        private string startDateField;

        private int? durationField;

        private string timeZoneField;

        private int? openTimeField;

        private int? timeZoneIDField;

        private bool? joinTeleconfBeforeHostField;

        private string hostWebExIDField;

        private string templateFilePathField;

        private bool? showFileStartModeField;

        private bool? showFileContPlayFlagField;

        private int? showFileInterValField;

        private int? entryExitToneField;

        private int? extNotifyTimeField;

        private bool? allowAnyoneHostMeetingField;

        /// <remarks/>
        public bool? allowAnyoneHostMeeting
        {
            get
            {
                return this.allowAnyoneHostMeetingField;
            }
            set
            {
                this.allowAnyoneHostMeetingField = value;
            }
        }

        public int? openTime
        {
            get
            {
                return this.openTimeField;
            }
            set
            {
                this.openTimeField = value;
            }
        }

        public int? timeZoneID
        {
            get
            {
                return this.timeZoneIDField;
            }
            set
            {
                this.timeZoneIDField = value;
            }
        }

        public bool? joinTeleconfBeforeHost
        {
            get
            {
                return this.joinTeleconfBeforeHostField;
            }
            set
            {
                this.joinTeleconfBeforeHostField = value;
            }
        }

        /// <remarks/>
        public string startDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }

        /// <remarks/>
        public int? duration
        {
            get
            {
                return this.durationField;
            }
            set
            {
                this.durationField = value;
            }
        }

        /// <remarks/>
        public string timeZone
        {
            get
            {
                return this.timeZoneField;
            }
            set
            {
                this.timeZoneField = value;
            }
        }

        /// <remarks/>
        public string hostWebExID
        {
            get
            {
                return this.hostWebExIDField;
            }
            set
            {
                this.hostWebExIDField = value;
            }
        }

        /// <remarks/>
        public string templateFilePath
        {
            get
            {
                return this.templateFilePathField;
            }
            set
            {
                this.templateFilePathField = value;
            }
        }

        /// <remarks/>
        public bool? showFileStartMode
        {
            get
            {
                return this.showFileStartModeField;
            }
            set
            {
                this.showFileStartModeField = value;
            }
        }

        /// <remarks/>
        public bool? showFileContPlayFlag
        {
            get
            {
                return this.showFileContPlayFlagField;
            }
            set
            {
                this.showFileContPlayFlagField = value;
            }
        }

        /// <remarks/>
        public int? showFileInterVal
        {
            get
            {
                return this.showFileInterValField;
            }
            set
            {
                this.showFileInterValField = value;
            }
        }

        /// <remarks/>
        public int? entryExitTone
        {
            get
            {
                return this.entryExitToneField;
            }
            set
            {
                this.entryExitToneField = value;
            }
        }

        /// <remarks/>
        public int? extNotifyTime
        {
            get
            {
                return this.extNotifyTimeField;
            }
            set
            {
                this.extNotifyTimeField = value;
            }
        }
    }

    #endregion

    #region Create Meeting
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/meeting")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/meeting", IsNullable = false)]
    public partial class iCalendarURL
    {

        private string hostField;

        private string attendeeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string host
        {
            get
            {
                return this.hostField;
            }
            set
            {
                this.hostField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string attendee
        {
            get
            {
                return this.attendeeField;
            }
            set
            {
                this.attendeeField = value;
            }
        }
    }


    #endregion

    #region GetSite

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/site", IsNullable = false)]
    public partial class siteInstance
    {

        private siteInstanceMetaData metaDataField;

        private siteInstanceUcf ucfField;

        private siteInstanceClientPlatforms clientPlatformsField;

        private siteInstanceResourceRestrictions resourceRestrictionsField;

        private siteInstanceSupportAPI supportAPIField;

        private siteInstanceMyWebExConfig myWebExConfigField;

        private siteInstanceTelephonyConfig telephonyConfigField;

        private siteInstanceCommerceAndReporting commerceAndReportingField;

        private siteInstanceTools toolsField;

        private siteInstanceCustCommunications custCommunicationsField;

       // private string trackingCodesField;

        private siteInstanceSupportedServices supportedServicesField;

        private siteInstanceSecurityOptions securityOptionsField;

        private siteInstanceDefaults defaultsField;

        private siteInstanceScheduleMeetingOptions scheduleMeetingOptionsField;

        private siteInstanceNavBarTop navBarTopField;

        private siteInstanceNavMyWebEx navMyWebExField;

        private siteInstanceNavAllServices navAllServicesField;

        private siteInstancePasswordCriteria passwordCriteriaField;

        private siteInstanceAccountPasswordCriteria accountPasswordCriteriaField;

        private siteInstanceProductivityTools productivityToolsField;

        private siteInstanceMeetingPlace meetingPlaceField;

        private siteInstanceEventCenter eventCenterField;

        private siteInstanceSalesCenter salesCenterField;

       // private string connectIntegrationField;

        //New

      

        private siteInstanceTrackingCode[] trackingCodesField;

      

        private siteInstanceRecordingPasswordCriteria recordingPasswordCriteriaField;

       

        //private string meetingPlaceField;

       

        private siteInstanceConnectIntegration connectIntegrationField;

        private siteInstanceVideo videoField;

        private siteInstanceSiteCommonOptions siteCommonOptionsField;

        private siteInstanceSamlSSO samlSSOField;

        private siteInstanceAttendeeLimitation attendeeLimitationField;

      

        /// <remarks/>
        public siteInstanceMetaData metaData
        {
            get
            {
                return this.metaDataField;
            }
            set
            {
                this.metaDataField = value;
            }
        }

        /// <remarks/>
        public siteInstanceUcf ucf
        {
            get
            {
                return this.ucfField;
            }
            set
            {
                this.ucfField = value;
            }
        }

        /// <remarks/>
        public siteInstanceClientPlatforms clientPlatforms
        {
            get
            {
                return this.clientPlatformsField;
            }
            set
            {
                this.clientPlatformsField = value;
            }
        }

        /// <remarks/>
        public siteInstanceResourceRestrictions resourceRestrictions
        {
            get
            {
                return this.resourceRestrictionsField;
            }
            set
            {
                this.resourceRestrictionsField = value;
            }
        }

        /// <remarks/>
        public siteInstanceSupportAPI supportAPI
        {
            get
            {
                return this.supportAPIField;
            }
            set
            {
                this.supportAPIField = value;
            }
        }

        /// <remarks/>
        public siteInstanceMyWebExConfig myWebExConfig
        {
            get
            {
                return this.myWebExConfigField;
            }
            set
            {
                this.myWebExConfigField = value;
            }
        }

        /// <remarks/>
        public siteInstanceTelephonyConfig telephonyConfig
        {
            get
            {
                return this.telephonyConfigField;
            }
            set
            {
                this.telephonyConfigField = value;
            }
        }

        /// <remarks/>
        public siteInstanceCommerceAndReporting commerceAndReporting
        {
            get
            {
                return this.commerceAndReportingField;
            }
            set
            {
                this.commerceAndReportingField = value;
            }
        }

        /// <remarks/>
        public siteInstanceTools tools
        {
            get
            {
                return this.toolsField;
            }
            set
            {
                this.toolsField = value;
            }
        }

        /// <remarks/>
        public siteInstanceCustCommunications custCommunications
        {
            get
            {
                return this.custCommunicationsField;
            }
            set
            {
                this.custCommunicationsField = value;
            }
        }


        /// <remarks/>
        public siteInstanceSupportedServices supportedServices
        {
            get
            {
                return this.supportedServicesField;
            }
            set
            {
                this.supportedServicesField = value;
            }
        }

        /// <remarks/>
        public siteInstanceSecurityOptions securityOptions
        {
            get
            {
                return this.securityOptionsField;
            }
            set
            {
                this.securityOptionsField = value;
            }
        }

        /// <remarks/>
        public siteInstanceDefaults defaults
        {
            get
            {
                return this.defaultsField;
            }
            set
            {
                this.defaultsField = value;
            }
        }

        /// <remarks/>
        public siteInstanceScheduleMeetingOptions scheduleMeetingOptions
        {
            get
            {
                return this.scheduleMeetingOptionsField;
            }
            set
            {
                this.scheduleMeetingOptionsField = value;
            }
        }

        /// <remarks/>
        public siteInstanceNavBarTop navBarTop
        {
            get
            {
                return this.navBarTopField;
            }
            set
            {
                this.navBarTopField = value;
            }
        }

        /// <remarks/>
        public siteInstanceNavMyWebEx navMyWebEx
        {
            get
            {
                return this.navMyWebExField;
            }
            set
            {
                this.navMyWebExField = value;
            }
        }

        /// <remarks/>
        public siteInstanceNavAllServices navAllServices
        {
            get
            {
                return this.navAllServicesField;
            }
            set
            {
                this.navAllServicesField = value;
            }
        }

        /// <remarks/>
        public siteInstancePasswordCriteria passwordCriteria
        {
            get
            {
                return this.passwordCriteriaField;
            }
            set
            {
                this.passwordCriteriaField = value;
            }
        }

        /// <remarks/>
        public siteInstanceAccountPasswordCriteria accountPasswordCriteria
        {
            get
            {
                return this.accountPasswordCriteriaField;
            }
            set
            {
                this.accountPasswordCriteriaField = value;
            }
        }

        /// <remarks/>
        public siteInstanceProductivityTools productivityTools
        {
            get
            {
                return this.productivityToolsField;
            }
            set
            {
                this.productivityToolsField = value;
            }
        }

        /// <remarks/>
        public siteInstanceMeetingPlace meetingPlace
        {
            get
            {
                return this.meetingPlaceField;
            }
            set
            {
                this.meetingPlaceField = value;
            }
        }

        /// <remarks/>
        public siteInstanceEventCenter eventCenter
        {
            get
            {
                return this.eventCenterField;
            }
            set
            {
                this.eventCenterField = value;
            }
        }

        /// <remarks/>
        public siteInstanceSalesCenter salesCenter
        {
            get
            {
                return this.salesCenterField;
            }
            set
            {
                this.salesCenterField = value;
            }
        }

       ///

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("trackingCode", IsNullable = false)]
        public siteInstanceTrackingCode[] trackingCodes
        {
            get
            {
                return this.trackingCodesField;
            }
            set
            {
                this.trackingCodesField = value;
            }
        }

      

        /// <remarks/>
        public siteInstanceRecordingPasswordCriteria recordingPasswordCriteria
        {
            get
            {
                return this.recordingPasswordCriteriaField;
            }
            set
            {
                this.recordingPasswordCriteriaField = value;
            }
        }

      

        /// <remarks/>
        public siteInstanceConnectIntegration connectIntegration
        {
            get
            {
                return this.connectIntegrationField;
            }
            set
            {
                this.connectIntegrationField = value;
            }
        }

        /// <remarks/>
        public siteInstanceVideo video
        {
            get
            {
                return this.videoField;
            }
            set
            {
                this.videoField = value;
            }
        }

        /// <remarks/>
        public siteInstanceSiteCommonOptions siteCommonOptions
        {
            get
            {
                return this.siteCommonOptionsField;
            }
            set
            {
                this.siteCommonOptionsField = value;
            }
        }

        /// <remarks/>
        public siteInstanceSamlSSO samlSSO
        {
            get
            {
                return this.samlSSOField;
            }
            set
            {
                this.samlSSOField = value;
            }
        }

        /// <remarks/>
        public siteInstanceAttendeeLimitation attendeeLimitation
        {
            get
            {
                return this.attendeeLimitationField;
            }
            set
            {
                this.attendeeLimitationField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTrackingCode
    {

        private byte indexField;

        private string nameField;

        private string inputModeField;

        private string hostProfileField;

        private siteInstanceTrackingCodeSchedulingPage[] schedulingPageField;

        private siteInstanceTrackingCodeListValue[] listValueField;

        /// <remarks/>
        public byte index
        {
            get
            {
                return this.indexField;
            }
            set
            {
                this.indexField = value;
            }
        }

        /// <remarks/>
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public string inputMode
        {
            get
            {
                return this.inputModeField;
            }
            set
            {
                this.inputModeField = value;
            }
        }

        /// <remarks/>
        public string hostProfile
        {
            get
            {
                return this.hostProfileField;
            }
            set
            {
                this.hostProfileField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("schedulingPage")]
        public siteInstanceTrackingCodeSchedulingPage[] schedulingPage
        {
            get
            {
                return this.schedulingPageField;
            }
            set
            {
                this.schedulingPageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("listValue")]
        public siteInstanceTrackingCodeListValue[] listValue
        {
            get
            {
                return this.listValueField;
            }
            set
            {
                this.listValueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTrackingCodeSchedulingPage
    {

        private string serviceField;

        private string schedulingField;

        /// <remarks/>
        public string service
        {
            get
            {
                return this.serviceField;
            }
            set
            {
                this.serviceField = value;
            }
        }

        /// <remarks/>
        public string scheduling
        {
            get
            {
                return this.schedulingField;
            }
            set
            {
                this.schedulingField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTrackingCodeListValue
    {

        private byte indexField;

        private string valueField;

        private bool activeField;

        /// <remarks/>
        public byte index
        {
            get
            {
                return this.indexField;
            }
            set
            {
                this.indexField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }

        /// <remarks/>
        public bool active
        {
            get
            {
                return this.activeField;
            }
            set
            {
                this.activeField = value;
            }
        }
    }

   

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceRecordingPasswordCriteria
    {

        private bool mixedCaseField;

        private byte minLengthField;

        private byte minAlphaField;

        private byte minNumericField;

        private byte minSpecialField;

        private bool disallowWebTextSessionsField;

        private bool disallowListField;

        private string[] disallowValueField;

        /// <remarks/>
        public bool mixedCase
        {
            get
            {
                return this.mixedCaseField;
            }
            set
            {
                this.mixedCaseField = value;
            }
        }

        /// <remarks/>
        public byte minLength
        {
            get
            {
                return this.minLengthField;
            }
            set
            {
                this.minLengthField = value;
            }
        }

        /// <remarks/>
        public byte minAlpha
        {
            get
            {
                return this.minAlphaField;
            }
            set
            {
                this.minAlphaField = value;
            }
        }

        /// <remarks/>
        public byte minNumeric
        {
            get
            {
                return this.minNumericField;
            }
            set
            {
                this.minNumericField = value;
            }
        }

        /// <remarks/>
        public byte minSpecial
        {
            get
            {
                return this.minSpecialField;
            }
            set
            {
                this.minSpecialField = value;
            }
        }

        /// <remarks/>
        public bool disallowWebTextSessions
        {
            get
            {
                return this.disallowWebTextSessionsField;
            }
            set
            {
                this.disallowWebTextSessionsField = value;
            }
        }

        /// <remarks/>
        public bool disallowList
        {
            get
            {
                return this.disallowListField;
            }
            set
            {
                this.disallowListField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("disallowValue")]
        public string[] disallowValue
        {
            get
            {
                return this.disallowValueField;
            }
            set
            {
                this.disallowValueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceConnectIntegration
    {

        private bool integratedWebEx11Field;

        /// <remarks/>
        public bool integratedWebEx11
        {
            get
            {
                return this.integratedWebEx11Field;
            }
            set
            {
                this.integratedWebEx11Field = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceVideo
    {

        private bool hQvideoField;

        private string maxBandwidthField;

        private bool hDvideoField;

        /// <remarks/>
        public bool HQvideo
        {
            get
            {
                return this.hQvideoField;
            }
            set
            {
                this.hQvideoField = value;
            }
        }

        /// <remarks/>
        public string maxBandwidth
        {
            get
            {
                return this.maxBandwidthField;
            }
            set
            {
                this.maxBandwidthField = value;
            }
        }

        /// <remarks/>
        public bool HDvideo
        {
            get
            {
                return this.hDvideoField;
            }
            set
            {
                this.hDvideoField = value;
            }
        }
    }


    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceSiteCommonOptions
    {

        private bool supportCustomDialRestrictionField;

        private bool supportTelePresenceField;

        private bool supportTelePresencePlusField;

        private bool enableCloudTelepresenceField;

        private bool enablePersonalMeetingRoomField;

        private bool supportAlternateHostField;

        private bool supportAnyoneHostMeetingsField;

        /// <remarks/>
        public bool SupportCustomDialRestriction
        {
            get
            {
                return this.supportCustomDialRestrictionField;
            }
            set
            {
                this.supportCustomDialRestrictionField = value;
            }
        }

        /// <remarks/>
        public bool SupportTelePresence
        {
            get
            {
                return this.supportTelePresenceField;
            }
            set
            {
                this.supportTelePresenceField = value;
            }
        }

        /// <remarks/>
        public bool SupportTelePresencePlus
        {
            get
            {
                return this.supportTelePresencePlusField;
            }
            set
            {
                this.supportTelePresencePlusField = value;
            }
        }

        /// <remarks/>
        public bool EnableCloudTelepresence
        {
            get
            {
                return this.enableCloudTelepresenceField;
            }
            set
            {
                this.enableCloudTelepresenceField = value;
            }
        }

        /// <remarks/>
        public bool enablePersonalMeetingRoom
        {
            get
            {
                return this.enablePersonalMeetingRoomField;
            }
            set
            {
                this.enablePersonalMeetingRoomField = value;
            }
        }

        /// <remarks/>
        public bool SupportAlternateHost
        {
            get
            {
                return this.supportAlternateHostField;
            }
            set
            {
                this.supportAlternateHostField = value;
            }
        }

        /// <remarks/>
        public bool SupportAnyoneHostMeetings
        {
            get
            {
                return this.supportAnyoneHostMeetingsField;
            }
            set
            {
                this.supportAnyoneHostMeetingsField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceSamlSSO
    {

        private bool enableSSOField;

        private bool autoAccountCreationField;

        /// <remarks/>
        public bool enableSSO
        {
            get
            {
                return this.enableSSOField;
            }
            set
            {
                this.enableSSOField = value;
            }
        }

        /// <remarks/>
        public bool autoAccountCreation
        {
            get
            {
                return this.autoAccountCreationField;
            }
            set
            {
                this.autoAccountCreationField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceAttendeeLimitation
    {

        private ushort maxInviteesNumberForMCField;

        private ushort maxRegistrantsNumberForMCField;

        private ushort maxInviteesNumberForTCField;

        private ushort maxRegistrantsNumberForTCField;

        private ushort maxInviteesNumberForECField;

        private ushort maxRegistrantsNumberForECField;

        /// <remarks/>
        public ushort maxInviteesNumberForMC
        {
            get
            {
                return this.maxInviteesNumberForMCField;
            }
            set
            {
                this.maxInviteesNumberForMCField = value;
            }
        }

        /// <remarks/>
        public ushort maxRegistrantsNumberForMC
        {
            get
            {
                return this.maxRegistrantsNumberForMCField;
            }
            set
            {
                this.maxRegistrantsNumberForMCField = value;
            }
        }

        /// <remarks/>
        public ushort maxInviteesNumberForTC
        {
            get
            {
                return this.maxInviteesNumberForTCField;
            }
            set
            {
                this.maxInviteesNumberForTCField = value;
            }
        }

        /// <remarks/>
        public ushort maxRegistrantsNumberForTC
        {
            get
            {
                return this.maxRegistrantsNumberForTCField;
            }
            set
            {
                this.maxRegistrantsNumberForTCField = value;
            }
        }

        /// <remarks/>
        public ushort maxInviteesNumberForEC
        {
            get
            {
                return this.maxInviteesNumberForECField;
            }
            set
            {
                this.maxInviteesNumberForECField = value;
            }
        }

        /// <remarks/>
        public ushort maxRegistrantsNumberForEC
        {
            get
            {
                return this.maxRegistrantsNumberForECField;
            }
            set
            {
                this.maxRegistrantsNumberForECField = value;
            }
        }
    }


    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceMetaData
    {

        private bool isEnterpriseField;

        private string[] serviceTypeField;

        private siteInstanceMetaDataMeetingTypes[] meetingTypesField;

        private string siteNameField;

        private string[] brandNameField;

        private string regionField;

        private string currencyField;

        private byte timeZoneIDField;

        private string timeZoneField;

        private string parterIDField;

        private string webDomainField;

        private string meetingDomainField;

        private string telephonyDomainField;

        private string pageVersionField;

        private string clientVersionField;

        private string pageLanguageField;

        private bool activateStatusField;

        private string webPageTypeField;

        private bool iCalendarField;

        private string myWebExDefaultPageField;

        private string componentVersionField;

        private ushort accountNumLimitField;

        private byte activeUserCountField;

        private uint auoAccountNumLimitField;

        private byte auoActiveUserCountField;

        private bool displayMeetingActualTimeField;

        private bool displayOffsetField;

        private bool supportWebEx11Field;

        /// <remarks/>
        public bool isEnterprise
        {
            get
            {
                return this.isEnterpriseField;
            }
            set
            {
                this.isEnterpriseField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("serviceType")]
        public string[] serviceType
        {
            get
            {
                return this.serviceTypeField;
            }
            set
            {
                this.serviceTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("meetingTypes")]
        public siteInstanceMetaDataMeetingTypes[] meetingTypes
        {
            get
            {
                return this.meetingTypesField;
            }
            set
            {
                this.meetingTypesField = value;
            }
        }

        /// <remarks/>
        public string siteName
        {
            get
            {
                return this.siteNameField;
            }
            set
            {
                this.siteNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("brandName")]
        public string[] brandName
        {
            get
            {
                return this.brandNameField;
            }
            set
            {
                this.brandNameField = value;
            }
        }

        /// <remarks/>
        public string region
        {
            get
            {
                return this.regionField;
            }
            set
            {
                this.regionField = value;
            }
        }

        /// <remarks/>
        public string currency
        {
            get
            {
                return this.currencyField;
            }
            set
            {
                this.currencyField = value;
            }
        }

        /// <remarks/>
        public byte timeZoneID
        {
            get
            {
                return this.timeZoneIDField;
            }
            set
            {
                this.timeZoneIDField = value;
            }
        }

        /// <remarks/>
        public string timeZone
        {
            get
            {
                return this.timeZoneField;
            }
            set
            {
                this.timeZoneField = value;
            }
        }

        /// <remarks/>
        public string parterID
        {
            get
            {
                return this.parterIDField;
            }
            set
            {
                this.parterIDField = value;
            }
        }

        /// <remarks/>
        public string webDomain
        {
            get
            {
                return this.webDomainField;
            }
            set
            {
                this.webDomainField = value;
            }
        }

        /// <remarks/>
        public string meetingDomain
        {
            get
            {
                return this.meetingDomainField;
            }
            set
            {
                this.meetingDomainField = value;
            }
        }

        /// <remarks/>
        public string telephonyDomain
        {
            get
            {
                return this.telephonyDomainField;
            }
            set
            {
                this.telephonyDomainField = value;
            }
        }

        /// <remarks/>
        public string pageVersion
        {
            get
            {
                return this.pageVersionField;
            }
            set
            {
                this.pageVersionField = value;
            }
        }

        /// <remarks/>
        public string clientVersion
        {
            get
            {
                return this.clientVersionField;
            }
            set
            {
                this.clientVersionField = value;
            }
        }

        /// <remarks/>
        public string pageLanguage
        {
            get
            {
                return this.pageLanguageField;
            }
            set
            {
                this.pageLanguageField = value;
            }
        }

        /// <remarks/>
        public bool activateStatus
        {
            get
            {
                return this.activateStatusField;
            }
            set
            {
                this.activateStatusField = value;
            }
        }

        /// <remarks/>
        public string webPageType
        {
            get
            {
                return this.webPageTypeField;
            }
            set
            {
                this.webPageTypeField = value;
            }
        }

        /// <remarks/>
        public bool iCalendar
        {
            get
            {
                return this.iCalendarField;
            }
            set
            {
                this.iCalendarField = value;
            }
        }

        /// <remarks/>
        public string myWebExDefaultPage
        {
            get
            {
                return this.myWebExDefaultPageField;
            }
            set
            {
                this.myWebExDefaultPageField = value;
            }
        }

        /// <remarks/>
        public string componentVersion
        {
            get
            {
                return this.componentVersionField;
            }
            set
            {
                this.componentVersionField = value;
            }
        }

        /// <remarks/>
        public ushort accountNumLimit
        {
            get
            {
                return this.accountNumLimitField;
            }
            set
            {
                this.accountNumLimitField = value;
            }
        }

        /// <remarks/>
        public byte activeUserCount
        {
            get
            {
                return this.activeUserCountField;
            }
            set
            {
                this.activeUserCountField = value;
            }
        }

        /// <remarks/>
        public uint auoAccountNumLimit
        {
            get
            {
                return this.auoAccountNumLimitField;
            }
            set
            {
                this.auoAccountNumLimitField = value;
            }
        }

        /// <remarks/>
        public byte auoActiveUserCount
        {
            get
            {
                return this.auoActiveUserCountField;
            }
            set
            {
                this.auoActiveUserCountField = value;
            }
        }

        /// <remarks/>
        public bool displayMeetingActualTime
        {
            get
            {
                return this.displayMeetingActualTimeField;
            }
            set
            {
                this.displayMeetingActualTimeField = value;
            }
        }

        /// <remarks/>
        public bool displayOffset
        {
            get
            {
                return this.displayOffsetField;
            }
            set
            {
                this.displayOffsetField = value;
            }
        }

        /// <remarks/>
        public bool supportWebEx11
        {
            get
            {
                return this.supportWebEx11Field;
            }
            set
            {
                this.supportWebEx11Field = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceMetaDataMeetingTypes
    {

        private ushort meetingTypeIDField;

        private string meetingTypeNameField;

        private bool hideInSchedulerField;

        private bool hideInSchedulerFieldSpecified;

        /// <remarks/>
        public ushort meetingTypeID
        {
            get
            {
                return this.meetingTypeIDField;
            }
            set
            {
                this.meetingTypeIDField = value;
            }
        }

        /// <remarks/>
        public string meetingTypeName
        {
            get
            {
                return this.meetingTypeNameField;
            }
            set
            {
                this.meetingTypeNameField = value;
            }
        }

        /// <remarks/>
        public bool hideInScheduler
        {
            get
            {
                return this.hideInSchedulerField;
            }
            set
            {
                this.hideInSchedulerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool hideInSchedulerSpecified
        {
            get
            {
                return this.hideInSchedulerFieldSpecified;
            }
            set
            {
                this.hideInSchedulerFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceUcf
    {

        private string ucfConfigurationField;

        /// <remarks/>
        public string ucfConfiguration
        {
            get
            {
                return this.ucfConfigurationField;
            }
            set
            {
                this.ucfConfigurationField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceClientPlatforms
    {

        private bool msWindowsField;

        private bool macOS9Field;

        private bool macOSXField;

        private bool sunSolarisField;

        private bool linuxField;

        private bool hpUnixField;

        private bool javaField;

        private bool palmField;

        /// <remarks/>
        public bool msWindows
        {
            get
            {
                return this.msWindowsField;
            }
            set
            {
                this.msWindowsField = value;
            }
        }

        /// <remarks/>
        public bool macOS9
        {
            get
            {
                return this.macOS9Field;
            }
            set
            {
                this.macOS9Field = value;
            }
        }

        /// <remarks/>
        public bool macOSX
        {
            get
            {
                return this.macOSXField;
            }
            set
            {
                this.macOSXField = value;
            }
        }

        /// <remarks/>
        public bool sunSolaris
        {
            get
            {
                return this.sunSolarisField;
            }
            set
            {
                this.sunSolarisField = value;
            }
        }

        /// <remarks/>
        public bool linux
        {
            get
            {
                return this.linuxField;
            }
            set
            {
                this.linuxField = value;
            }
        }

        /// <remarks/>
        public bool hpUnix
        {
            get
            {
                return this.hpUnixField;
            }
            set
            {
                this.hpUnixField = value;
            }
        }

        /// <remarks/>
        public bool java
        {
            get
            {
                return this.javaField;
            }
            set
            {
                this.javaField = value;
            }
        }

        /// <remarks/>
        public bool palm
        {
            get
            {
                return this.palmField;
            }
            set
            {
                this.palmField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceResourceRestrictions
    {

        private bool isLicenseManagerField;

        private int concurrentLicenseField;

        private uint fileFolderCapacityField;

        private int maxConcurrentEventsField;

        private uint archiveStorageLimitField;

        /// <remarks/>
        public bool isLicenseManager
        {
            get
            {
                return this.isLicenseManagerField;
            }
            set
            {
                this.isLicenseManagerField = value;
            }
        }

        /// <remarks/>
        public int concurrentLicense
        {
            get
            {
                return this.concurrentLicenseField;
            }
            set
            {
                this.concurrentLicenseField = value;
            }
        }

        /// <remarks/>
        public uint fileFolderCapacity
        {
            get
            {
                return this.fileFolderCapacityField;
            }
            set
            {
                this.fileFolderCapacityField = value;
            }
        }

        /// <remarks/>
        public int maxConcurrentEvents
        {
            get
            {
                return this.maxConcurrentEventsField;
            }
            set
            {
                this.maxConcurrentEventsField = value;
            }
        }

        /// <remarks/>
        public uint archiveStorageLimit
        {
            get
            {
                return this.archiveStorageLimitField;
            }
            set
            {
                this.archiveStorageLimitField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceSupportAPI
    {

        private bool autoLoginField;

        private bool aspAndPHPAPIField;

        private bool backwardAPIField;

        private bool xmlAPIField;

        private bool cAPIField;

        private bool scormField;

        /// <remarks/>
        public bool autoLogin
        {
            get
            {
                return this.autoLoginField;
            }
            set
            {
                this.autoLoginField = value;
            }
        }

        /// <remarks/>
        public bool aspAndPHPAPI
        {
            get
            {
                return this.aspAndPHPAPIField;
            }
            set
            {
                this.aspAndPHPAPIField = value;
            }
        }

        /// <remarks/>
        public bool backwardAPI
        {
            get
            {
                return this.backwardAPIField;
            }
            set
            {
                this.backwardAPIField = value;
            }
        }

        /// <remarks/>
        public bool xmlAPI
        {
            get
            {
                return this.xmlAPIField;
            }
            set
            {
                this.xmlAPIField = value;
            }
        }

        /// <remarks/>
        public bool cAPI
        {
            get
            {
                return this.cAPIField;
            }
            set
            {
                this.cAPIField = value;
            }
        }

        /// <remarks/>
        public bool scorm
        {
            get
            {
                return this.scormField;
            }
            set
            {
                this.scormField = value;
            }
        }
    }


    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceMyWebExConfig
    {

        private bool myContactsField;

        private bool myProfileField;

        private bool myMeetingsField;

        private bool trainingRecordingsField;

        private bool foldersField;

        private bool eventDocumentField;

        private bool myReportField;

        private bool myComputerField;

        private bool personalMeetingPageField;

        private byte myFilesStorageField;

        private byte myComputerNumbersField;

        private bool enableMyWebExProField;

        private uint myWebExProMaxHostsField;

        private bool restrictAccessAnyAppsField;

        private byte restrictAccessAnyAppsNumField;

        private string addlAccessAnyComputersLimitField;

        private byte addlAccessAnyComputersField;

        private string addlStorageLimitField;

        private byte addlStorageField;

        private bool myContactsProField;

        private bool myProfileProField;

        private bool myMeetingsProField;

        private bool trainingRecordingsProField;

        private bool foldersProField;

        private bool eventDocumentProField;

        private bool myReportProField;

        private bool myComputerProField;

        private bool personalMeetingPageProField;

        private ushort myFilesStorageProField;

        private byte myComputerNumbersProField;

        private bool pMRheaderBrandingField;

        /// <remarks/>
        public bool myContacts
        {
            get
            {
                return this.myContactsField;
            }
            set
            {
                this.myContactsField = value;
            }
        }

        /// <remarks/>
        public bool myProfile
        {
            get
            {
                return this.myProfileField;
            }
            set
            {
                this.myProfileField = value;
            }
        }

        /// <remarks/>
        public bool myMeetings
        {
            get
            {
                return this.myMeetingsField;
            }
            set
            {
                this.myMeetingsField = value;
            }
        }

        /// <remarks/>
        public bool trainingRecordings
        {
            get
            {
                return this.trainingRecordingsField;
            }
            set
            {
                this.trainingRecordingsField = value;
            }
        }

        /// <remarks/>
        public bool folders
        {
            get
            {
                return this.foldersField;
            }
            set
            {
                this.foldersField = value;
            }
        }

        /// <remarks/>
        public bool eventDocument
        {
            get
            {
                return this.eventDocumentField;
            }
            set
            {
                this.eventDocumentField = value;
            }
        }

        /// <remarks/>
        public bool myReport
        {
            get
            {
                return this.myReportField;
            }
            set
            {
                this.myReportField = value;
            }
        }

        /// <remarks/>
        public bool myComputer
        {
            get
            {
                return this.myComputerField;
            }
            set
            {
                this.myComputerField = value;
            }
        }

        /// <remarks/>
        public bool personalMeetingPage
        {
            get
            {
                return this.personalMeetingPageField;
            }
            set
            {
                this.personalMeetingPageField = value;
            }
        }

        /// <remarks/>
        public byte myFilesStorage
        {
            get
            {
                return this.myFilesStorageField;
            }
            set
            {
                this.myFilesStorageField = value;
            }
        }

        /// <remarks/>
        public byte myComputerNumbers
        {
            get
            {
                return this.myComputerNumbersField;
            }
            set
            {
                this.myComputerNumbersField = value;
            }
        }

        /// <remarks/>
        public bool enableMyWebExPro
        {
            get
            {
                return this.enableMyWebExProField;
            }
            set
            {
                this.enableMyWebExProField = value;
            }
        }

        /// <remarks/>
        public uint myWebExProMaxHosts
        {
            get
            {
                return this.myWebExProMaxHostsField;
            }
            set
            {
                this.myWebExProMaxHostsField = value;
            }
        }

        /// <remarks/>
        public bool restrictAccessAnyApps
        {
            get
            {
                return this.restrictAccessAnyAppsField;
            }
            set
            {
                this.restrictAccessAnyAppsField = value;
            }
        }

        /// <remarks/>
        public byte restrictAccessAnyAppsNum
        {
            get
            {
                return this.restrictAccessAnyAppsNumField;
            }
            set
            {
                this.restrictAccessAnyAppsNumField = value;
            }
        }

        /// <remarks/>
        public string addlAccessAnyComputersLimit
        {
            get
            {
                return this.addlAccessAnyComputersLimitField;
            }
            set
            {
                this.addlAccessAnyComputersLimitField = value;
            }
        }

        /// <remarks/>
        public byte addlAccessAnyComputers
        {
            get
            {
                return this.addlAccessAnyComputersField;
            }
            set
            {
                this.addlAccessAnyComputersField = value;
            }
        }

        /// <remarks/>
        public string addlStorageLimit
        {
            get
            {
                return this.addlStorageLimitField;
            }
            set
            {
                this.addlStorageLimitField = value;
            }
        }

        /// <remarks/>
        public byte addlStorage
        {
            get
            {
                return this.addlStorageField;
            }
            set
            {
                this.addlStorageField = value;
            }
        }

        /// <remarks/>
        public bool myContactsPro
        {
            get
            {
                return this.myContactsProField;
            }
            set
            {
                this.myContactsProField = value;
            }
        }

        /// <remarks/>
        public bool myProfilePro
        {
            get
            {
                return this.myProfileProField;
            }
            set
            {
                this.myProfileProField = value;
            }
        }

        /// <remarks/>
        public bool myMeetingsPro
        {
            get
            {
                return this.myMeetingsProField;
            }
            set
            {
                this.myMeetingsProField = value;
            }
        }

        /// <remarks/>
        public bool trainingRecordingsPro
        {
            get
            {
                return this.trainingRecordingsProField;
            }
            set
            {
                this.trainingRecordingsProField = value;
            }
        }

        /// <remarks/>
        public bool foldersPro
        {
            get
            {
                return this.foldersProField;
            }
            set
            {
                this.foldersProField = value;
            }
        }

        /// <remarks/>
        public bool eventDocumentPro
        {
            get
            {
                return this.eventDocumentProField;
            }
            set
            {
                this.eventDocumentProField = value;
            }
        }

        /// <remarks/>
        public bool myReportPro
        {
            get
            {
                return this.myReportProField;
            }
            set
            {
                this.myReportProField = value;
            }
        }

        /// <remarks/>
        public bool myComputerPro
        {
            get
            {
                return this.myComputerProField;
            }
            set
            {
                this.myComputerProField = value;
            }
        }

        /// <remarks/>
        public bool personalMeetingPagePro
        {
            get
            {
                return this.personalMeetingPageProField;
            }
            set
            {
                this.personalMeetingPageProField = value;
            }
        }

        /// <remarks/>
        public ushort myFilesStoragePro
        {
            get
            {
                return this.myFilesStorageProField;
            }
            set
            {
                this.myFilesStorageProField = value;
            }
        }

        /// <remarks/>
        public byte myComputerNumbersPro
        {
            get
            {
                return this.myComputerNumbersProField;
            }
            set
            {
                this.myComputerNumbersProField = value;
            }
        }

        /// <remarks/>
        public bool PMRheaderBranding
        {
            get
            {
                return this.pMRheaderBrandingField;
            }
            set
            {
                this.pMRheaderBrandingField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTelephonyConfig
    {

        private bool isTSPUsingTelephonyAPIField;

        private string serviceNameField;

        private string participantAccessCodeLabelField;

        private string subscriberAccessCodeLabelField;

        private string attendeeIDLabelField;

        private bool internetPhoneField;

        private bool supportCallInTypeTeleconfField;

        private bool callInTeleconferencingField;

        private bool tollFreeCallinTeleconferencingField;

        private bool intlCallInTeleconferencingField;

        private bool callBackTeleconferencingField;

        private byte callInNumberField;

        private string defaultTeleServerSubjectField;

        private string subscribeNameField;

        private string subscribePasswordField;

        private byte defaultPhoneLinesField;

        private byte defaultSpeakingLinesField;

        private byte majorCountryCodeField;

        private ushort majorAreaCodeField;

        private string publicNameField;

        private bool hybridTeleconferenceField;

        private bool instantHelpField;

        private bool customerManageField;

        private byte maxCallersNumberField;

        private bool isSpecified1Field;

        private bool isContinueField;

        private bool intlCallBackTeleconferencingField;

        private siteInstanceTelephonyConfigPersonalTeleconf personalTeleconfField;

        private bool multiMediaPlatformField;

        private string multiMediaHostNameField;

        private bool broadcastAudioStreamField;

        private siteInstanceTelephonyConfigTspAdaptorSettings tspAdaptorSettingsField;

        private siteInstanceTelephonyConfigMeetingPlace meetingPlaceField;

        private bool supportOtherTypeTeleconfField;

        private string otherTeleServiceNameField;

        private bool supportAdapterlessTSPField;

        private bool displayAttendeeIDField;

        private bool provisionTeleAccountField;

        private bool choosePCNField;

        private bool audioOnlyField;

        private bool configTollAndTollFreeNumField;

        private bool configPrimaryTSField;

        private bool teleCLIAuthEnabledField;

        private bool teleCLIPINEnabledField;

        /// <remarks/>
        public bool isTSPUsingTelephonyAPI
        {
            get
            {
                return this.isTSPUsingTelephonyAPIField;
            }
            set
            {
                this.isTSPUsingTelephonyAPIField = value;
            }
        }

        /// <remarks/>
        public string serviceName
        {
            get
            {
                return this.serviceNameField;
            }
            set
            {
                this.serviceNameField = value;
            }
        }

        /// <remarks/>
        public string participantAccessCodeLabel
        {
            get
            {
                return this.participantAccessCodeLabelField;
            }
            set
            {
                this.participantAccessCodeLabelField = value;
            }
        }

        /// <remarks/>
        public string subscriberAccessCodeLabel
        {
            get
            {
                return this.subscriberAccessCodeLabelField;
            }
            set
            {
                this.subscriberAccessCodeLabelField = value;
            }
        }

        /// <remarks/>
        public string attendeeIDLabel
        {
            get
            {
                return this.attendeeIDLabelField;
            }
            set
            {
                this.attendeeIDLabelField = value;
            }
        }

        /// <remarks/>
        public bool internetPhone
        {
            get
            {
                return this.internetPhoneField;
            }
            set
            {
                this.internetPhoneField = value;
            }
        }

        /// <remarks/>
        public bool supportCallInTypeTeleconf
        {
            get
            {
                return this.supportCallInTypeTeleconfField;
            }
            set
            {
                this.supportCallInTypeTeleconfField = value;
            }
        }

        /// <remarks/>
        public bool callInTeleconferencing
        {
            get
            {
                return this.callInTeleconferencingField;
            }
            set
            {
                this.callInTeleconferencingField = value;
            }
        }

        /// <remarks/>
        public bool tollFreeCallinTeleconferencing
        {
            get
            {
                return this.tollFreeCallinTeleconferencingField;
            }
            set
            {
                this.tollFreeCallinTeleconferencingField = value;
            }
        }

        /// <remarks/>
        public bool intlCallInTeleconferencing
        {
            get
            {
                return this.intlCallInTeleconferencingField;
            }
            set
            {
                this.intlCallInTeleconferencingField = value;
            }
        }

        /// <remarks/>
        public bool callBackTeleconferencing
        {
            get
            {
                return this.callBackTeleconferencingField;
            }
            set
            {
                this.callBackTeleconferencingField = value;
            }
        }

        /// <remarks/>
        public byte callInNumber
        {
            get
            {
                return this.callInNumberField;
            }
            set
            {
                this.callInNumberField = value;
            }
        }

        /// <remarks/>
        public string defaultTeleServerSubject
        {
            get
            {
                return this.defaultTeleServerSubjectField;
            }
            set
            {
                this.defaultTeleServerSubjectField = value;
            }
        }

        /// <remarks/>
        public string subscribeName
        {
            get
            {
                return this.subscribeNameField;
            }
            set
            {
                this.subscribeNameField = value;
            }
        }

        /// <remarks/>
        public string subscribePassword
        {
            get
            {
                return this.subscribePasswordField;
            }
            set
            {
                this.subscribePasswordField = value;
            }
        }

        /// <remarks/>
        public byte defaultPhoneLines
        {
            get
            {
                return this.defaultPhoneLinesField;
            }
            set
            {
                this.defaultPhoneLinesField = value;
            }
        }

        /// <remarks/>
        public byte defaultSpeakingLines
        {
            get
            {
                return this.defaultSpeakingLinesField;
            }
            set
            {
                this.defaultSpeakingLinesField = value;
            }
        }

        /// <remarks/>
        public byte majorCountryCode
        {
            get
            {
                return this.majorCountryCodeField;
            }
            set
            {
                this.majorCountryCodeField = value;
            }
        }

        /// <remarks/>
        public ushort majorAreaCode
        {
            get
            {
                return this.majorAreaCodeField;
            }
            set
            {
                this.majorAreaCodeField = value;
            }
        }

        /// <remarks/>
        public string publicName
        {
            get
            {
                return this.publicNameField;
            }
            set
            {
                this.publicNameField = value;
            }
        }

        /// <remarks/>
        public bool hybridTeleconference
        {
            get
            {
                return this.hybridTeleconferenceField;
            }
            set
            {
                this.hybridTeleconferenceField = value;
            }
        }

        /// <remarks/>
        public bool instantHelp
        {
            get
            {
                return this.instantHelpField;
            }
            set
            {
                this.instantHelpField = value;
            }
        }

        /// <remarks/>
        public bool customerManage
        {
            get
            {
                return this.customerManageField;
            }
            set
            {
                this.customerManageField = value;
            }
        }

        /// <remarks/>
        public byte maxCallersNumber
        {
            get
            {
                return this.maxCallersNumberField;
            }
            set
            {
                this.maxCallersNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("isSpecified")]
        public bool isSpecified1
        {
            get
            {
                return this.isSpecified1Field;
            }
            set
            {
                this.isSpecified1Field = value;
            }
        }

        /// <remarks/>
        public bool isContinue
        {
            get
            {
                return this.isContinueField;
            }
            set
            {
                this.isContinueField = value;
            }
        }

        /// <remarks/>
        public bool intlCallBackTeleconferencing
        {
            get
            {
                return this.intlCallBackTeleconferencingField;
            }
            set
            {
                this.intlCallBackTeleconferencingField = value;
            }
        }

        /// <remarks/>
        public siteInstanceTelephonyConfigPersonalTeleconf personalTeleconf
        {
            get
            {
                return this.personalTeleconfField;
            }
            set
            {
                this.personalTeleconfField = value;
            }
        }

        /// <remarks/>
        public bool multiMediaPlatform
        {
            get
            {
                return this.multiMediaPlatformField;
            }
            set
            {
                this.multiMediaPlatformField = value;
            }
        }

        /// <remarks/>
        public string multiMediaHostName
        {
            get
            {
                return this.multiMediaHostNameField;
            }
            set
            {
                this.multiMediaHostNameField = value;
            }
        }

        /// <remarks/>
        public bool broadcastAudioStream
        {
            get
            {
                return this.broadcastAudioStreamField;
            }
            set
            {
                this.broadcastAudioStreamField = value;
            }
        }

        /// <remarks/>
        public siteInstanceTelephonyConfigTspAdaptorSettings tspAdaptorSettings
        {
            get
            {
                return this.tspAdaptorSettingsField;
            }
            set
            {
                this.tspAdaptorSettingsField = value;
            }
        }

        /// <remarks/>
        public siteInstanceTelephonyConfigMeetingPlace meetingPlace
        {
            get
            {
                return this.meetingPlaceField;
            }
            set
            {
                this.meetingPlaceField = value;
            }
        }

        /// <remarks/>
        public bool supportOtherTypeTeleconf
        {
            get
            {
                return this.supportOtherTypeTeleconfField;
            }
            set
            {
                this.supportOtherTypeTeleconfField = value;
            }
        }

        /// <remarks/>
        public string otherTeleServiceName
        {
            get
            {
                return this.otherTeleServiceNameField;
            }
            set
            {
                this.otherTeleServiceNameField = value;
            }
        }

        /// <remarks/>
        public bool supportAdapterlessTSP
        {
            get
            {
                return this.supportAdapterlessTSPField;
            }
            set
            {
                this.supportAdapterlessTSPField = value;
            }
        }

        /// <remarks/>
        public bool displayAttendeeID
        {
            get
            {
                return this.displayAttendeeIDField;
            }
            set
            {
                this.displayAttendeeIDField = value;
            }
        }

        /// <remarks/>
        public bool provisionTeleAccount
        {
            get
            {
                return this.provisionTeleAccountField;
            }
            set
            {
                this.provisionTeleAccountField = value;
            }
        }

        /// <remarks/>
        public bool choosePCN
        {
            get
            {
                return this.choosePCNField;
            }
            set
            {
                this.choosePCNField = value;
            }
        }

        /// <remarks/>
        public bool audioOnly
        {
            get
            {
                return this.audioOnlyField;
            }
            set
            {
                this.audioOnlyField = value;
            }
        }

        /// <remarks/>
        public bool configTollAndTollFreeNum
        {
            get
            {
                return this.configTollAndTollFreeNumField;
            }
            set
            {
                this.configTollAndTollFreeNumField = value;
            }
        }

        /// <remarks/>
        public bool configPrimaryTS
        {
            get
            {
                return this.configPrimaryTSField;
            }
            set
            {
                this.configPrimaryTSField = value;
            }
        }

        /// <remarks/>
        public bool teleCLIAuthEnabled
        {
            get
            {
                return this.teleCLIAuthEnabledField;
            }
            set
            {
                this.teleCLIAuthEnabledField = value;
            }
        }

        /// <remarks/>
        public bool teleCLIPINEnabled
        {
            get
            {
                return this.teleCLIPINEnabledField;
            }
            set
            {
                this.teleCLIPINEnabledField = value;
            }
        }
    }



    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTelephonyConfigPersonalTeleconf
    {

        private siteInstanceTelephonyConfigPersonalTeleconfPrimaryLargeServer primaryLargeServerField;

        private siteInstanceTelephonyConfigPersonalTeleconfBackup1LargeServer backup1LargeServerField;

        private siteInstanceTelephonyConfigPersonalTeleconfBackup2LargeServer backup2LargeServerField;

        private siteInstanceTelephonyConfigPersonalTeleconfPrimarySmallServer primarySmallServerField;

        private siteInstanceTelephonyConfigPersonalTeleconfBackup1SmallServer backup1SmallServerField;

        private siteInstanceTelephonyConfigPersonalTeleconfBackup2SmallServer backup2SmallServerField;

        private bool joinBeforeHostField;

        /// <remarks/>
        public siteInstanceTelephonyConfigPersonalTeleconfPrimaryLargeServer primaryLargeServer
        {
            get
            {
                return this.primaryLargeServerField;
            }
            set
            {
                this.primaryLargeServerField = value;
            }
        }

        /// <remarks/>
        public siteInstanceTelephonyConfigPersonalTeleconfBackup1LargeServer backup1LargeServer
        {
            get
            {
                return this.backup1LargeServerField;
            }
            set
            {
                this.backup1LargeServerField = value;
            }
        }

        /// <remarks/>
        public siteInstanceTelephonyConfigPersonalTeleconfBackup2LargeServer backup2LargeServer
        {
            get
            {
                return this.backup2LargeServerField;
            }
            set
            {
                this.backup2LargeServerField = value;
            }
        }

        /// <remarks/>
        public siteInstanceTelephonyConfigPersonalTeleconfPrimarySmallServer primarySmallServer
        {
            get
            {
                return this.primarySmallServerField;
            }
            set
            {
                this.primarySmallServerField = value;
            }
        }

        /// <remarks/>
        public siteInstanceTelephonyConfigPersonalTeleconfBackup1SmallServer backup1SmallServer
        {
            get
            {
                return this.backup1SmallServerField;
            }
            set
            {
                this.backup1SmallServerField = value;
            }
        }

        /// <remarks/>
        public siteInstanceTelephonyConfigPersonalTeleconfBackup2SmallServer backup2SmallServer
        {
            get
            {
                return this.backup2SmallServerField;
            }
            set
            {
                this.backup2SmallServerField = value;
            }
        }

        /// <remarks/>
        public bool joinBeforeHost
        {
            get
            {
                return this.joinBeforeHostField;
            }
            set
            {
                this.joinBeforeHostField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTelephonyConfigPersonalTeleconfPrimaryLargeServer
    {

        private string tollNumField;

        private string tollFreeNumField;

        private globalNum[] globalNumField;

        private bool enableServerField;

        private string tollLabelField;

        private string tollFreeLabelField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollNum
        {
            get
            {
                return this.tollNumField;
            }
            set
            {
                this.tollNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollFreeNum
        {
            get
            {
                return this.tollFreeNumField;
            }
            set
            {
                this.tollFreeNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("globalNum", Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public globalNum[] globalNum
        {
            get
            {
                return this.globalNumField;
            }
            set
            {
                this.globalNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public bool enableServer
        {
            get
            {
                return this.enableServerField;
            }
            set
            {
                this.enableServerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollLabel
        {
            get
            {
                return this.tollLabelField;
            }
            set
            {
                this.tollLabelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollFreeLabel
        {
            get
            {
                return this.tollFreeLabelField;
            }
            set
            {
                this.tollFreeLabelField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service", IsNullable = false)]
    public partial class globalNum
    {

        private string countryAliasField;

        private string phoneNumberField;

        private bool tollFreeField;

        private bool defaultField;

        /// <remarks/>
        public string countryAlias
        {
            get
            {
                return this.countryAliasField;
            }
            set
            {
                this.countryAliasField = value;
            }
        }

        /// <remarks/>
        public string phoneNumber
        {
            get
            {
                return this.phoneNumberField;
            }
            set
            {
                this.phoneNumberField = value;
            }
        }

        /// <remarks/>
        public bool tollFree
        {
            get
            {
                return this.tollFreeField;
            }
            set
            {
                this.tollFreeField = value;
            }
        }

        /// <remarks/>
        public bool @default
        {
            get
            {
                return this.defaultField;
            }
            set
            {
                this.defaultField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTelephonyConfigPersonalTeleconfBackup1LargeServer
    {

        private string tollNumField;

        private string tollFreeNumField;

        private globalNum[] globalNumField;

        private bool enableServerField;

        private string tollLabelField;

        private string tollFreeLabelField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollNum
        {
            get
            {
                return this.tollNumField;
            }
            set
            {
                this.tollNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollFreeNum
        {
            get
            {
                return this.tollFreeNumField;
            }
            set
            {
                this.tollFreeNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("globalNum", Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public globalNum[] globalNum
        {
            get
            {
                return this.globalNumField;
            }
            set
            {
                this.globalNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public bool enableServer
        {
            get
            {
                return this.enableServerField;
            }
            set
            {
                this.enableServerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollLabel
        {
            get
            {
                return this.tollLabelField;
            }
            set
            {
                this.tollLabelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollFreeLabel
        {
            get
            {
                return this.tollFreeLabelField;
            }
            set
            {
                this.tollFreeLabelField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTelephonyConfigPersonalTeleconfBackup2LargeServer
    {

        private string tollNumField;

        private string tollFreeNumField;

        private globalNum[] globalNumField;

        private bool enableServerField;

        private string tollLabelField;

        private string tollFreeLabelField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollNum
        {
            get
            {
                return this.tollNumField;
            }
            set
            {
                this.tollNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollFreeNum
        {
            get
            {
                return this.tollFreeNumField;
            }
            set
            {
                this.tollFreeNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("globalNum", Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public globalNum[] globalNum
        {
            get
            {
                return this.globalNumField;
            }
            set
            {
                this.globalNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public bool enableServer
        {
            get
            {
                return this.enableServerField;
            }
            set
            {
                this.enableServerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollLabel
        {
            get
            {
                return this.tollLabelField;
            }
            set
            {
                this.tollLabelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollFreeLabel
        {
            get
            {
                return this.tollFreeLabelField;
            }
            set
            {
                this.tollFreeLabelField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTelephonyConfigPersonalTeleconfPrimarySmallServer
    {

        private string tollNumField;

        private string tollFreeNumField;

        private globalNum[] globalNumField;

        private bool enableServerField;

        private string tollLabelField;

        private string tollFreeLabelField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollNum
        {
            get
            {
                return this.tollNumField;
            }
            set
            {
                this.tollNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollFreeNum
        {
            get
            {
                return this.tollFreeNumField;
            }
            set
            {
                this.tollFreeNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("globalNum", Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public globalNum[] globalNum
        {
            get
            {
                return this.globalNumField;
            }
            set
            {
                this.globalNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public bool enableServer
        {
            get
            {
                return this.enableServerField;
            }
            set
            {
                this.enableServerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollLabel
        {
            get
            {
                return this.tollLabelField;
            }
            set
            {
                this.tollLabelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollFreeLabel
        {
            get
            {
                return this.tollFreeLabelField;
            }
            set
            {
                this.tollFreeLabelField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTelephonyConfigPersonalTeleconfBackup1SmallServer
    {

        private string tollNumField;

        private string tollFreeNumField;

        private globalNum[] globalNumField;

        private bool enableServerField;

        private string tollLabelField;

        private string tollFreeLabelField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollNum
        {
            get
            {
                return this.tollNumField;
            }
            set
            {
                this.tollNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollFreeNum
        {
            get
            {
                return this.tollFreeNumField;
            }
            set
            {
                this.tollFreeNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("globalNum", Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public globalNum[] globalNum
        {
            get
            {
                return this.globalNumField;
            }
            set
            {
                this.globalNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public bool enableServer
        {
            get
            {
                return this.enableServerField;
            }
            set
            {
                this.enableServerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollLabel
        {
            get
            {
                return this.tollLabelField;
            }
            set
            {
                this.tollLabelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollFreeLabel
        {
            get
            {
                return this.tollFreeLabelField;
            }
            set
            {
                this.tollFreeLabelField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTelephonyConfigPersonalTeleconfBackup2SmallServer
    {

        private string tollNumField;

        private string tollFreeNumField;

        private globalNum[] globalNumField;

        private bool enableServerField;

        private string tollLabelField;

        private string tollFreeLabelField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollNum
        {
            get
            {
                return this.tollNumField;
            }
            set
            {
                this.tollNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollFreeNum
        {
            get
            {
                return this.tollFreeNumField;
            }
            set
            {
                this.tollFreeNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("globalNum", Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public globalNum[] globalNum
        {
            get
            {
                return this.globalNumField;
            }
            set
            {
                this.globalNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public bool enableServer
        {
            get
            {
                return this.enableServerField;
            }
            set
            {
                this.enableServerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollLabel
        {
            get
            {
                return this.tollLabelField;
            }
            set
            {
                this.tollLabelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollFreeLabel
        {
            get
            {
                return this.tollFreeLabelField;
            }
            set
            {
                this.tollFreeLabelField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTelephonyConfigTspAdaptorSettings
    {

        private siteInstanceTelephonyConfigTspAdaptorSettingsPrimaryLarge primaryLargeField;

        private siteInstanceTelephonyConfigTspAdaptorSettingsBackup1Large backup1LargeField;

        private siteInstanceTelephonyConfigTspAdaptorSettingsBackup2Large backup2LargeField;

        /// <remarks/>
        public siteInstanceTelephonyConfigTspAdaptorSettingsPrimaryLarge primaryLarge
        {
            get
            {
                return this.primaryLargeField;
            }
            set
            {
                this.primaryLargeField = value;
            }
        }

        /// <remarks/>
        public siteInstanceTelephonyConfigTspAdaptorSettingsBackup1Large backup1Large
        {
            get
            {
                return this.backup1LargeField;
            }
            set
            {
                this.backup1LargeField = value;
            }
        }

        /// <remarks/>
        public siteInstanceTelephonyConfigTspAdaptorSettingsBackup2Large backup2Large
        {
            get
            {
                return this.backup2LargeField;
            }
            set
            {
                this.backup2LargeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTelephonyConfigTspAdaptorSettingsPrimaryLarge
    {

        private bool enableAdaptorField;

        private object serverIPField;

        private siteInstanceTelephonyConfigTspAdaptorSettingsPrimaryLargeMpAudio[] mpAudioField;

        /// <remarks/>
        public bool enableAdaptor
        {
            get
            {
                return this.enableAdaptorField;
            }
            set
            {
                this.enableAdaptorField = value;
            }
        }

        /// <remarks/>
        public object serverIP
        {
            get
            {
                return this.serverIPField;
            }
            set
            {
                this.serverIPField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("mpAudio")]
        public siteInstanceTelephonyConfigTspAdaptorSettingsPrimaryLargeMpAudio[] mpAudio
        {
            get
            {
                return this.mpAudioField;
            }
            set
            {
                this.mpAudioField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTelephonyConfigTspAdaptorSettingsPrimaryLargeMpAudio
    {

        private string labelField;

        /// <remarks/>
        public string label
        {
            get
            {
                return this.labelField;
            }
            set
            {
                this.labelField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTelephonyConfigTspAdaptorSettingsBackup1Large
    {

        private bool enableAdaptorField;

        private object serverIPField;

        private siteInstanceTelephonyConfigTspAdaptorSettingsBackup1LargeMpAudio[] mpAudioField;

        /// <remarks/>
        public bool enableAdaptor
        {
            get
            {
                return this.enableAdaptorField;
            }
            set
            {
                this.enableAdaptorField = value;
            }
        }

        /// <remarks/>
        public object serverIP
        {
            get
            {
                return this.serverIPField;
            }
            set
            {
                this.serverIPField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("mpAudio")]
        public siteInstanceTelephonyConfigTspAdaptorSettingsBackup1LargeMpAudio[] mpAudio
        {
            get
            {
                return this.mpAudioField;
            }
            set
            {
                this.mpAudioField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTelephonyConfigTspAdaptorSettingsBackup1LargeMpAudio
    {

        private string labelField;

        /// <remarks/>
        public string label
        {
            get
            {
                return this.labelField;
            }
            set
            {
                this.labelField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTelephonyConfigTspAdaptorSettingsBackup2Large
    {

        private bool enableAdaptorField;

        private object serverIPField;

        private siteInstanceTelephonyConfigTspAdaptorSettingsBackup2LargeMpAudio[] mpAudioField;

        /// <remarks/>
        public bool enableAdaptor
        {
            get
            {
                return this.enableAdaptorField;
            }
            set
            {
                this.enableAdaptorField = value;
            }
        }

        /// <remarks/>
        public object serverIP
        {
            get
            {
                return this.serverIPField;
            }
            set
            {
                this.serverIPField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("mpAudio")]
        public siteInstanceTelephonyConfigTspAdaptorSettingsBackup2LargeMpAudio[] mpAudio
        {
            get
            {
                return this.mpAudioField;
            }
            set
            {
                this.mpAudioField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTelephonyConfigTspAdaptorSettingsBackup2LargeMpAudio
    {

        private string labelField;

        /// <remarks/>
        public string label
        {
            get
            {
                return this.labelField;
            }
            set
            {
                this.labelField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTelephonyConfigMeetingPlace
    {

        private bool persistentTSPField;

        private string mpAudioConferencingField;

        /// <remarks/>
        public bool persistentTSP
        {
            get
            {
                return this.persistentTSPField;
            }
            set
            {
                this.persistentTSPField = value;
            }
        }

        /// <remarks/>
        public string mpAudioConferencing
        {
            get
            {
                return this.mpAudioConferencingField;
            }
            set
            {
                this.mpAudioConferencingField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceCommerceAndReporting
    {

        private bool trackingCodeField;

        private bool siteAdminReportField;

        private bool subScriptionServiceField;

        private bool isECommmerceField;

        private bool customereCommerceField;

        private bool isLocalTaxField;

        private string localTaxNameField;

        private decimal localTaxtRateField;

        private int holReportField;



        /// <remarks/>
        public bool trackingCode
        {
            get
            {
                return this.trackingCodeField;
            }
            set
            {
                this.trackingCodeField = value;
            }
        }

        /// <remarks/>
        public bool siteAdminReport
        {
            get
            {
                return this.siteAdminReportField;
            }
            set
            {
                this.siteAdminReportField = value;
            }
        }

        /// <remarks/>
        public bool subScriptionService
        {
            get
            {
                return this.subScriptionServiceField;
            }
            set
            {
                this.subScriptionServiceField = value;
            }
        }

        /// <remarks/>
        public bool isECommmerce
        {
            get
            {
                return this.isECommmerceField;
            }
            set
            {
                this.isECommmerceField = value;
            }
        }

        /// <remarks/>
        public bool customereCommerce
        {
            get
            {
                return this.customereCommerceField;
            }
            set
            {
                this.customereCommerceField = value;
            }
        }

        /// <remarks/>
        public bool isLocalTax
        {
            get
            {
                return this.isLocalTaxField;
            }
            set
            {
                this.isLocalTaxField = value;
            }
        }

        /// <remarks/>
        public string localTaxName
        {
            get
            {
                return this.localTaxNameField;
            }
            set
            {
                this.localTaxNameField = value;
            }
        }

        /// <remarks/>
        public decimal localTaxtRate
        {
            get
            {
                return this.localTaxtRateField;
            }
            set
            {
                this.localTaxtRateField = value;
            }
        }

        /// <remarks/>
        public int holReport
        {
            get
            {
                return this.holReportField;
            }
            set
            {
                this.holReportField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTools
    {

        private bool businessDirectoryField;

        private bool officeCalendarField;

        private bool meetingCalendarField;

        private bool displayOnCallAssistLinkField;

        private bool displayProfileLinkField;

        private bool recordingAndPlaybackField;

        private bool recordingEditorField;

        private bool publishRecordingsField;

        private bool instantMeetingField;

        private bool emailsField;

        private bool outlookIntegrationField;

        private bool wirelessAccessField;

        private bool allowPublicAccessField;

        private bool sslField;

        private bool handsOnLabField;

        private uint holMaxLabsField;

        private uint holMaxComputersField;

        private bool userLockDownField;

        private bool meetingAssistField;

        private bool smsField;

        private string encryptionField;

        private bool internalMeetingField;

        private bool enableTPField;

        private bool enableTPplusField;

        /// <remarks/>
        public bool businessDirectory
        {
            get
            {
                return this.businessDirectoryField;
            }
            set
            {
                this.businessDirectoryField = value;
            }
        }

        /// <remarks/>
        public bool officeCalendar
        {
            get
            {
                return this.officeCalendarField;
            }
            set
            {
                this.officeCalendarField = value;
            }
        }

        /// <remarks/>
        public bool meetingCalendar
        {
            get
            {
                return this.meetingCalendarField;
            }
            set
            {
                this.meetingCalendarField = value;
            }
        }

        /// <remarks/>
        public bool displayOnCallAssistLink
        {
            get
            {
                return this.displayOnCallAssistLinkField;
            }
            set
            {
                this.displayOnCallAssistLinkField = value;
            }
        }

        /// <remarks/>
        public bool displayProfileLink
        {
            get
            {
                return this.displayProfileLinkField;
            }
            set
            {
                this.displayProfileLinkField = value;
            }
        }

        /// <remarks/>
        public bool recordingAndPlayback
        {
            get
            {
                return this.recordingAndPlaybackField;
            }
            set
            {
                this.recordingAndPlaybackField = value;
            }
        }

        /// <remarks/>
        public bool recordingEditor
        {
            get
            {
                return this.recordingEditorField;
            }
            set
            {
                this.recordingEditorField = value;
            }
        }

        /// <remarks/>
        public bool publishRecordings
        {
            get
            {
                return this.publishRecordingsField;
            }
            set
            {
                this.publishRecordingsField = value;
            }
        }

        /// <remarks/>
        public bool instantMeeting
        {
            get
            {
                return this.instantMeetingField;
            }
            set
            {
                this.instantMeetingField = value;
            }
        }

        /// <remarks/>
        public bool emails
        {
            get
            {
                return this.emailsField;
            }
            set
            {
                this.emailsField = value;
            }
        }

        /// <remarks/>
        public bool outlookIntegration
        {
            get
            {
                return this.outlookIntegrationField;
            }
            set
            {
                this.outlookIntegrationField = value;
            }
        }

        /// <remarks/>
        public bool wirelessAccess
        {
            get
            {
                return this.wirelessAccessField;
            }
            set
            {
                this.wirelessAccessField = value;
            }
        }

        /// <remarks/>
        public bool allowPublicAccess
        {
            get
            {
                return this.allowPublicAccessField;
            }
            set
            {
                this.allowPublicAccessField = value;
            }
        }

        /// <remarks/>
        public bool ssl
        {
            get
            {
                return this.sslField;
            }
            set
            {
                this.sslField = value;
            }
        }

        /// <remarks/>
        public bool handsOnLab
        {
            get
            {
                return this.handsOnLabField;
            }
            set
            {
                this.handsOnLabField = value;
            }
        }

        /// <remarks/>
        public uint holMaxLabs
        {
            get
            {
                return this.holMaxLabsField;
            }
            set
            {
                this.holMaxLabsField = value;
            }
        }

        /// <remarks/>
        public uint holMaxComputers
        {
            get
            {
                return this.holMaxComputersField;
            }
            set
            {
                this.holMaxComputersField = value;
            }
        }

        /// <remarks/>
        public bool userLockDown
        {
            get
            {
                return this.userLockDownField;
            }
            set
            {
                this.userLockDownField = value;
            }
        }

        /// <remarks/>
        public bool meetingAssist
        {
            get
            {
                return this.meetingAssistField;
            }
            set
            {
                this.meetingAssistField = value;
            }
        }

        /// <remarks/>
        public bool sms
        {
            get
            {
                return this.smsField;
            }
            set
            {
                this.smsField = value;
            }
        }

        /// <remarks/>
        public string encryption
        {
            get
            {
                return this.encryptionField;
            }
            set
            {
                this.encryptionField = value;
            }
        }

        /// <remarks/>
        public bool internalMeeting
        {
            get
            {
                return this.internalMeetingField;
            }
            set
            {
                this.internalMeetingField = value;
            }
        }

        /// <remarks/>
        public bool enableTP
        {
            get
            {
                return this.enableTPField;
            }
            set
            {
                this.enableTPField = value;
            }
        }

        /// <remarks/>
        public bool enableTPplus
        {
            get
            {
                return this.enableTPplusField;
            }
            set
            {
                this.enableTPplusField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceCustCommunications
    {

        private siteInstanceCustCommunicationsDisplayType displayTypeField;

        private siteInstanceCustCommunicationsDisplayMethod displayMethodField;

        /// <remarks/>
        public siteInstanceCustCommunicationsDisplayType displayType
        {
            get
            {
                return this.displayTypeField;
            }
            set
            {
                this.displayTypeField = value;
            }
        }

        /// <remarks/>
        public siteInstanceCustCommunicationsDisplayMethod displayMethod
        {
            get
            {
                return this.displayMethodField;
            }
            set
            {
                this.displayMethodField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceCustCommunicationsDisplayType
    {

        private bool prodSvcAnnounceField;

        private bool trainingInfoField;

        private bool eNewslettersField;

        private bool promotionsOffersField;

        private bool pressReleasesField;

        /// <remarks/>
        public bool prodSvcAnnounce
        {
            get
            {
                return this.prodSvcAnnounceField;
            }
            set
            {
                this.prodSvcAnnounceField = value;
            }
        }

        /// <remarks/>
        public bool trainingInfo
        {
            get
            {
                return this.trainingInfoField;
            }
            set
            {
                this.trainingInfoField = value;
            }
        }

        /// <remarks/>
        public bool eNewsletters
        {
            get
            {
                return this.eNewslettersField;
            }
            set
            {
                this.eNewslettersField = value;
            }
        }

        /// <remarks/>
        public bool promotionsOffers
        {
            get
            {
                return this.promotionsOffersField;
            }
            set
            {
                this.promotionsOffersField = value;
            }
        }

        /// <remarks/>
        public bool pressReleases
        {
            get
            {
                return this.pressReleasesField;
            }
            set
            {
                this.pressReleasesField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceCustCommunicationsDisplayMethod
    {

        private bool emailField;

        private bool faxField;

        private bool phoneField;

        private bool mailField;

        /// <remarks/>
        public bool email
        {
            get
            {
                return this.emailField;
            }
            set
            {
                this.emailField = value;
            }
        }

        /// <remarks/>
        public bool fax
        {
            get
            {
                return this.faxField;
            }
            set
            {
                this.faxField = value;
            }
        }

        /// <remarks/>
        public bool phone
        {
            get
            {
                return this.phoneField;
            }
            set
            {
                this.phoneField = value;
            }
        }

        /// <remarks/>
        public bool mail
        {
            get
            {
                return this.mailField;
            }
            set
            {
                this.mailField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceSupportedServices
    {

        private siteInstanceSupportedServicesMeetingCenter meetingCenterField;

        private siteInstanceSupportedServicesTrainingCenter trainingCenterField;

        private siteInstanceSupportedServicesSupportCenter supportCenterField;

        private siteInstanceSupportedServicesEventCenter eventCenterField;

        private siteInstanceSupportedServicesSalesCenter salesCenterField;

        /// <remarks/>
        public siteInstanceSupportedServicesMeetingCenter meetingCenter
        {
            get
            {
                return this.meetingCenterField;
            }
            set
            {
                this.meetingCenterField = value;
            }
        }

        /// <remarks/>
        public siteInstanceSupportedServicesTrainingCenter trainingCenter
        {
            get
            {
                return this.trainingCenterField;
            }
            set
            {
                this.trainingCenterField = value;
            }
        }

        /// <remarks/>
        public siteInstanceSupportedServicesSupportCenter supportCenter
        {
            get
            {
                return this.supportCenterField;
            }
            set
            {
                this.supportCenterField = value;
            }
        }

        /// <remarks/>
        public siteInstanceSupportedServicesEventCenter eventCenter
        {
            get
            {
                return this.eventCenterField;
            }
            set
            {
                this.eventCenterField = value;
            }
        }

        /// <remarks/>
        public siteInstanceSupportedServicesSalesCenter salesCenter
        {
            get
            {
                return this.salesCenterField;
            }
            set
            {
                this.salesCenterField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceSupportedServicesMeetingCenter
    {

        private bool enabledField;

        private string pageVersionField;

        private string clientVersionField;

        /// <remarks/>
        public bool enabled
        {
            get
            {
                return this.enabledField;
            }
            set
            {
                this.enabledField = value;
            }
        }

        /// <remarks/>
        public string pageVersion
        {
            get
            {
                return this.pageVersionField;
            }
            set
            {
                this.pageVersionField = value;
            }
        }

        /// <remarks/>
        public string clientVersion
        {
            get
            {
                return this.clientVersionField;
            }
            set
            {
                this.clientVersionField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceSupportedServicesTrainingCenter
    {

        private bool enabledField;

        private string pageVersionField;

        private string clientVersionField;

        /// <remarks/>
        public bool enabled
        {
            get
            {
                return this.enabledField;
            }
            set
            {
                this.enabledField = value;
            }
        }

        /// <remarks/>
        public string pageVersion
        {
            get
            {
                return this.pageVersionField;
            }
            set
            {
                this.pageVersionField = value;
            }
        }

        /// <remarks/>
        public string clientVersion
        {
            get
            {
                return this.clientVersionField;
            }
            set
            {
                this.clientVersionField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceSupportedServicesSupportCenter
    {

        private bool enabledField;

        private string pageVersionField;

        private string clientVersionField;

        private bool webACDField;

        /// <remarks/>
        public bool enabled
        {
            get
            {
                return this.enabledField;
            }
            set
            {
                this.enabledField = value;
            }
        }

        /// <remarks/>
        public string pageVersion
        {
            get
            {
                return this.pageVersionField;
            }
            set
            {
                this.pageVersionField = value;
            }
        }

        /// <remarks/>
        public string clientVersion
        {
            get
            {
                return this.clientVersionField;
            }
            set
            {
                this.clientVersionField = value;
            }
        }

        /// <remarks/>
        public bool webACD
        {
            get
            {
                return this.webACDField;
            }
            set
            {
                this.webACDField = value;
            }
        }
    }

    /// <remarks/>
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceSupportedServicesEventCenter
    {

        private bool enabledField;

        private string pageVersionField;

        private string clientVersionField;

        private bool marketingAddOnField;

        private bool optimizeAttendeeBandwidthUsageField;

        /// <remarks/>
        public bool enabled
        {
            get
            {
                return this.enabledField;
            }
            set
            {
                this.enabledField = value;
            }
        }

        /// <remarks/>
        public string pageVersion
        {
            get
            {
                return this.pageVersionField;
            }
            set
            {
                this.pageVersionField = value;
            }
        }

        /// <remarks/>
        public string clientVersion
        {
            get
            {
                return this.clientVersionField;
            }
            set
            {
                this.clientVersionField = value;
            }
        }

        /// <remarks/>
        public bool marketingAddOn
        {
            get
            {
                return this.marketingAddOnField;
            }
            set
            {
                this.marketingAddOnField = value;
            }
        }

        /// <remarks/>
        public bool optimizeAttendeeBandwidthUsage
        {
            get
            {
                return this.optimizeAttendeeBandwidthUsageField;
            }
            set
            {
                this.optimizeAttendeeBandwidthUsageField = value;
            }
        }
    }


    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceSupportedServicesSalesCenter
    {

        private bool enabledField;

        private string pageVersionField;

        private string clientVersionField;

        /// <remarks/>
        public bool enabled
        {
            get
            {
                return this.enabledField;
            }
            set
            {
                this.enabledField = value;
            }
        }

        /// <remarks/>
        public string pageVersion
        {
            get
            {
                return this.pageVersionField;
            }
            set
            {
                this.pageVersionField = value;
            }
        }

        /// <remarks/>
        public string clientVersion
        {
            get
            {
                return this.clientVersionField;
            }
            set
            {
                this.clientVersionField = value;
            }
        }
    }


    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceSecurityOptions
    {

        private bool passwordExpiresField;

        private byte passwordLifetimeField;

        private bool allMeetingsUnlistedField;

        private bool allMeetingsPasswordField;

        private bool joinBeforeHostField;

        private bool audioBeforeHostField;

        private bool audioBeforeHostECField;

        private bool audioBeforeHostTCField;

        private bool changePersonalURLField;

        private bool changeUserNameField;

        private siteInstanceSecurityOptionsMeetings meetingsField;

        private bool strictUserPasswordField;

        private bool accountNotifyField;

        private bool requireLoginBeforeSiteAccessField;

        private bool changePWDWhenAutoLoginField;

        private bool enforceBaselineField;

        private bool passwordChangeIntervalOptField;

        private byte passwordChangeIntervalField;

        private bool firstAttendeeAsPresenterField;

        private bool isEnableUUIDLinkField;

        private bool isEnableUUIDLinkForSACField;

        private bool enforceRecordingPwdForMCField;

        private bool enforceRecordingPwdForECField;

        private bool enforceRecordingPwdForTCField;

        private bool enforceRecordingPwdForMiscField;

        /// <remarks/>
        public bool passwordExpires
        {
            get
            {
                return this.passwordExpiresField;
            }
            set
            {
                this.passwordExpiresField = value;
            }
        }

        /// <remarks/>
        public byte passwordLifetime
        {
            get
            {
                return this.passwordLifetimeField;
            }
            set
            {
                this.passwordLifetimeField = value;
            }
        }

        /// <remarks/>
        public bool allMeetingsUnlisted
        {
            get
            {
                return this.allMeetingsUnlistedField;
            }
            set
            {
                this.allMeetingsUnlistedField = value;
            }
        }

        /// <remarks/>
        public bool allMeetingsPassword
        {
            get
            {
                return this.allMeetingsPasswordField;
            }
            set
            {
                this.allMeetingsPasswordField = value;
            }
        }

        /// <remarks/>
        public bool joinBeforeHost
        {
            get
            {
                return this.joinBeforeHostField;
            }
            set
            {
                this.joinBeforeHostField = value;
            }
        }

        /// <remarks/>
        public bool audioBeforeHost
        {
            get
            {
                return this.audioBeforeHostField;
            }
            set
            {
                this.audioBeforeHostField = value;
            }
        }

        /// <remarks/>
        public bool audioBeforeHostEC
        {
            get
            {
                return this.audioBeforeHostECField;
            }
            set
            {
                this.audioBeforeHostECField = value;
            }
        }

        /// <remarks/>
        public bool audioBeforeHostTC
        {
            get
            {
                return this.audioBeforeHostTCField;
            }
            set
            {
                this.audioBeforeHostTCField = value;
            }
        }

        /// <remarks/>
        public bool changePersonalURL
        {
            get
            {
                return this.changePersonalURLField;
            }
            set
            {
                this.changePersonalURLField = value;
            }
        }

        /// <remarks/>
        public bool changeUserName
        {
            get
            {
                return this.changeUserNameField;
            }
            set
            {
                this.changeUserNameField = value;
            }
        }

        /// <remarks/>
        public siteInstanceSecurityOptionsMeetings meetings
        {
            get
            {
                return this.meetingsField;
            }
            set
            {
                this.meetingsField = value;
            }
        }

        /// <remarks/>
        public bool strictUserPassword
        {
            get
            {
                return this.strictUserPasswordField;
            }
            set
            {
                this.strictUserPasswordField = value;
            }
        }

        /// <remarks/>
        public bool accountNotify
        {
            get
            {
                return this.accountNotifyField;
            }
            set
            {
                this.accountNotifyField = value;
            }
        }

        /// <remarks/>
        public bool requireLoginBeforeSiteAccess
        {
            get
            {
                return this.requireLoginBeforeSiteAccessField;
            }
            set
            {
                this.requireLoginBeforeSiteAccessField = value;
            }
        }

        /// <remarks/>
        public bool changePWDWhenAutoLogin
        {
            get
            {
                return this.changePWDWhenAutoLoginField;
            }
            set
            {
                this.changePWDWhenAutoLoginField = value;
            }
        }

        /// <remarks/>
        public bool enforceBaseline
        {
            get
            {
                return this.enforceBaselineField;
            }
            set
            {
                this.enforceBaselineField = value;
            }
        }

        /// <remarks/>
        public bool passwordChangeIntervalOpt
        {
            get
            {
                return this.passwordChangeIntervalOptField;
            }
            set
            {
                this.passwordChangeIntervalOptField = value;
            }
        }

        /// <remarks/>
        public byte passwordChangeInterval
        {
            get
            {
                return this.passwordChangeIntervalField;
            }
            set
            {
                this.passwordChangeIntervalField = value;
            }
        }

        /// <remarks/>
        public bool firstAttendeeAsPresenter
        {
            get
            {
                return this.firstAttendeeAsPresenterField;
            }
            set
            {
                this.firstAttendeeAsPresenterField = value;
            }
        }

        /// <remarks/>
        public bool isEnableUUIDLink
        {
            get
            {
                return this.isEnableUUIDLinkField;
            }
            set
            {
                this.isEnableUUIDLinkField = value;
            }
        }

        /// <remarks/>
        public bool isEnableUUIDLinkForSAC
        {
            get
            {
                return this.isEnableUUIDLinkForSACField;
            }
            set
            {
                this.isEnableUUIDLinkForSACField = value;
            }
        }

        /// <remarks/>
        public bool enforceRecordingPwdForMC
        {
            get
            {
                return this.enforceRecordingPwdForMCField;
            }
            set
            {
                this.enforceRecordingPwdForMCField = value;
            }
        }

        /// <remarks/>
        public bool enforceRecordingPwdForEC
        {
            get
            {
                return this.enforceRecordingPwdForECField;
            }
            set
            {
                this.enforceRecordingPwdForECField = value;
            }
        }

        /// <remarks/>
        public bool enforceRecordingPwdForTC
        {
            get
            {
                return this.enforceRecordingPwdForTCField;
            }
            set
            {
                this.enforceRecordingPwdForTCField = value;
            }
        }

        /// <remarks/>
        public bool enforceRecordingPwdForMisc
        {
            get
            {
                return this.enforceRecordingPwdForMiscField;
            }
            set
            {
                this.enforceRecordingPwdForMiscField = value;
            }
        }
    }
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceSecurityOptionsMeetings
    {

        private bool strictPasswordsField;

        /// <remarks/>
        public bool strictPasswords
        {
            get
            {
                return this.strictPasswordsField;
            }
            set
            {
                this.strictPasswordsField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceDefaults
    {

        private bool emailRemindersField;

        private string entryExitToneField;

        private bool voipField;

        private siteInstanceDefaultsTeleconference teleconferenceField;

        private bool joinTeleconfNotPress1Field;

        private bool updateTSPAccountField;

        /// <remarks/>
        public bool emailReminders
        {
            get
            {
                return this.emailRemindersField;
            }
            set
            {
                this.emailRemindersField = value;
            }
        }

        /// <remarks/>
        public string entryExitTone
        {
            get
            {
                return this.entryExitToneField;
            }
            set
            {
                this.entryExitToneField = value;
            }
        }

        /// <remarks/>
        public bool voip
        {
            get
            {
                return this.voipField;
            }
            set
            {
                this.voipField = value;
            }
        }

        /// <remarks/>
        public siteInstanceDefaultsTeleconference teleconference
        {
            get
            {
                return this.teleconferenceField;
            }
            set
            {
                this.teleconferenceField = value;
            }
        }

        /// <remarks/>
        public bool joinTeleconfNotPress1
        {
            get
            {
                return this.joinTeleconfNotPress1Field;
            }
            set
            {
                this.joinTeleconfNotPress1Field = value;
            }
        }

        /// <remarks/>
        public bool updateTSPAccount
        {
            get
            {
                return this.updateTSPAccountField;
            }
            set
            {
                this.updateTSPAccountField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceDefaultsTeleconference
    {

        private string telephonySupportField;

        private bool tollFreeField;

        private bool intlLocalCallInField;

        /// <remarks/>
        public string telephonySupport
        {
            get
            {
                return this.telephonySupportField;
            }
            set
            {
                this.telephonySupportField = value;
            }
        }

        /// <remarks/>
        public bool tollFree
        {
            get
            {
                return this.tollFreeField;
            }
            set
            {
                this.tollFreeField = value;
            }
        }

        /// <remarks/>
        public bool intlLocalCallIn
        {
            get
            {
                return this.intlLocalCallInField;
            }
            set
            {
                this.intlLocalCallInField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceScheduleMeetingOptions
    {

        private bool scheduleOnBehalfField;

        private bool saveSessionTemplateField;

        /// <remarks/>
        public bool scheduleOnBehalf
        {
            get
            {
                return this.scheduleOnBehalfField;
            }
            set
            {
                this.scheduleOnBehalfField = value;
            }
        }

        /// <remarks/>
        public bool saveSessionTemplate
        {
            get
            {
                return this.saveSessionTemplateField;
            }
            set
            {
                this.saveSessionTemplateField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceNavBarTop
    {

        private siteInstanceNavBarTopButton[] buttonField;

        private bool displayDisabledServiceField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("button")]
        public siteInstanceNavBarTopButton[] button
        {
            get
            {
                return this.buttonField;
            }
            set
            {
                this.buttonField = value;
            }
        }

        /// <remarks/>
        public bool displayDisabledService
        {
            get
            {
                return this.displayDisabledServiceField;
            }
            set
            {
                this.displayDisabledServiceField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceNavBarTopButton
    {

        private int orderField;

        private bool enabledField;

        private bool enabledFieldSpecified;

        private string serviceNameField;

        /// <remarks/>
        public int order
        {
            get
            {
                return this.orderField;
            }
            set
            {
                this.orderField = value;
            }
        }

        /// <remarks/>
        public bool enabled
        {
            get
            {
                return this.enabledField;
            }
            set
            {
                this.enabledField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool enabledSpecified
        {
            get
            {
                return this.enabledFieldSpecified;
            }
            set
            {
                this.enabledFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string serviceName
        {
            get
            {
                return this.serviceNameField;
            }
            set
            {
                this.serviceNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceNavMyWebEx
    {

        private siteInstanceNavMyWebExCustomLink[] customLinksField;

        private siteInstanceNavMyWebExPartnerLink[] partnerLinksField;

        private bool partnerIntegrationField;

        private siteInstanceNavMyWebExSupport supportField;

        private siteInstanceNavMyWebExTraining trainingField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("customLink", IsNullable = false)]
        public siteInstanceNavMyWebExCustomLink[] customLinks
        {
            get
            {
                return this.customLinksField;
            }
            set
            {
                this.customLinksField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("partnerLink", IsNullable = false)]
        public siteInstanceNavMyWebExPartnerLink[] partnerLinks
        {
            get
            {
                return this.partnerLinksField;
            }
            set
            {
                this.partnerLinksField = value;
            }
        }

        /// <remarks/>
        public bool partnerIntegration
        {
            get
            {
                return this.partnerIntegrationField;
            }
            set
            {
                this.partnerIntegrationField = value;
            }
        }

        /// <remarks/>
        public siteInstanceNavMyWebExSupport support
        {
            get
            {
                return this.supportField;
            }
            set
            {
                this.supportField = value;
            }
        }

        /// <remarks/>
        public siteInstanceNavMyWebExTraining training
        {
            get
            {
                return this.trainingField;
            }
            set
            {
                this.trainingField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceNavMyWebExCustomLink
    {

        private string targetField;

        /// <remarks/>
        public string target
        {
            get
            {
                return this.targetField;
            }
            set
            {
                this.targetField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceNavMyWebExPartnerLink
    {

        private string targetField;

        /// <remarks/>
        public string target
        {
            get
            {
                return this.targetField;
            }
            set
            {
                this.targetField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceNavMyWebExSupport
    {

        private string targetField;

        /// <remarks/>
        public string target
        {
            get
            {
                return this.targetField;
            }
            set
            {
                this.targetField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceNavMyWebExTraining
    {

        private string targetField;

        /// <remarks/>
        public string target
        {
            get
            {
                return this.targetField;
            }
            set
            {
                this.targetField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceNavAllServices
    {

        private siteInstanceNavAllServicesCustomLink[] customLinksField;

        private siteInstanceNavAllServicesSupport supportField;

        private siteInstanceNavAllServicesTraining trainingField;

        private siteInstanceNavAllServicesSupportMenu supportMenuField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("customLink", IsNullable = false)]
        public siteInstanceNavAllServicesCustomLink[] customLinks
        {
            get
            {
                return this.customLinksField;
            }
            set
            {
                this.customLinksField = value;
            }
        }

        /// <remarks/>
        public siteInstanceNavAllServicesSupport support
        {
            get
            {
                return this.supportField;
            }
            set
            {
                this.supportField = value;
            }
        }

        /// <remarks/>
        public siteInstanceNavAllServicesTraining training
        {
            get
            {
                return this.trainingField;
            }
            set
            {
                this.trainingField = value;
            }
        }

        /// <remarks/>
        public siteInstanceNavAllServicesSupportMenu supportMenu
        {
            get
            {
                return this.supportMenuField;
            }
            set
            {
                this.supportMenuField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceNavAllServicesCustomLink
    {

        private string targetField;

        /// <remarks/>
        public string target
        {
            get
            {
                return this.targetField;
            }
            set
            {
                this.targetField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceNavAllServicesSupport
    {

        private string nameField;

        private string targetField;

        /// <remarks/>
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public string target
        {
            get
            {
                return this.targetField;
            }
            set
            {
                this.targetField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceNavAllServicesTraining
    {

        private string nameField;

        private string targetField;

        /// <remarks/>
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public string target
        {
            get
            {
                return this.targetField;
            }
            set
            {
                this.targetField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceNavAllServicesSupportMenu
    {

        private siteInstanceNavAllServicesSupportMenuUserGuides userGuidesField;

        private siteInstanceNavAllServicesSupportMenuDownloads downloadsField;

        private siteInstanceNavAllServicesSupportMenuTraining trainingField;

        private siteInstanceNavAllServicesSupportMenuContactUs contactUsField;

        private bool supportMyResourcesField;

        /// <remarks/>
        public siteInstanceNavAllServicesSupportMenuUserGuides userGuides
        {
            get
            {
                return this.userGuidesField;
            }
            set
            {
                this.userGuidesField = value;
            }
        }

        /// <remarks/>
        public siteInstanceNavAllServicesSupportMenuDownloads downloads
        {
            get
            {
                return this.downloadsField;
            }
            set
            {
                this.downloadsField = value;
            }
        }

        /// <remarks/>
        public siteInstanceNavAllServicesSupportMenuTraining training
        {
            get
            {
                return this.trainingField;
            }
            set
            {
                this.trainingField = value;
            }
        }

        /// <remarks/>
        public siteInstanceNavAllServicesSupportMenuContactUs contactUs
        {
            get
            {
                return this.contactUsField;
            }
            set
            {
                this.contactUsField = value;
            }
        }

        /// <remarks/>
        public bool supportMyResources
        {
            get
            {
                return this.supportMyResourcesField;
            }
            set
            {
                this.supportMyResourcesField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceNavAllServicesSupportMenuUserGuides
    {

        private string targetField;

        /// <remarks/>
        public string target
        {
            get
            {
                return this.targetField;
            }
            set
            {
                this.targetField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceNavAllServicesSupportMenuDownloads
    {

        private string targetField;

        /// <remarks/>
        public string target
        {
            get
            {
                return this.targetField;
            }
            set
            {
                this.targetField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceNavAllServicesSupportMenuTraining
    {

        private string targetField;

        /// <remarks/>
        public string target
        {
            get
            {
                return this.targetField;
            }
            set
            {
                this.targetField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceNavAllServicesSupportMenuContactUs
    {

        private string targetField;

        /// <remarks/>
        public string target
        {
            get
            {
                return this.targetField;
            }
            set
            {
                this.targetField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstancePasswordCriteria
    {

        private bool mixedCaseField;

        private int minLengthField;

        private int minAlphaField;

        private int minNumericField;

        private int minSpecialField;

        private bool disallowWebTextSessionsField;

        private bool disallowWebTextAccountsField;

        private bool disallowListField;

        private string[] disallowValueField;

        /// <remarks/>
        public bool mixedCase
        {
            get
            {
                return this.mixedCaseField;
            }
            set
            {
                this.mixedCaseField = value;
            }
        }

        /// <remarks/>
        public int minLength
        {
            get
            {
                return this.minLengthField;
            }
            set
            {
                this.minLengthField = value;
            }
        }

        /// <remarks/>
        public int minAlpha
        {
            get
            {
                return this.minAlphaField;
            }
            set
            {
                this.minAlphaField = value;
            }
        }

        /// <remarks/>
        public int minNumeric
        {
            get
            {
                return this.minNumericField;
            }
            set
            {
                this.minNumericField = value;
            }
        }

        /// <remarks/>
        public int minSpecial
        {
            get
            {
                return this.minSpecialField;
            }
            set
            {
                this.minSpecialField = value;
            }
        }

        /// <remarks/>
        public bool disallowWebTextSessions
        {
            get
            {
                return this.disallowWebTextSessionsField;
            }
            set
            {
                this.disallowWebTextSessionsField = value;
            }
        }

        /// <remarks/>
        public bool disallowWebTextAccounts
        {
            get
            {
                return this.disallowWebTextAccountsField;
            }
            set
            {
                this.disallowWebTextAccountsField = value;
            }
        }

        /// <remarks/>
        public bool disallowList
        {
            get
            {
                return this.disallowListField;
            }
            set
            {
                this.disallowListField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("disallowValue")]
        public string[] disallowValue
        {
            get
            {
                return this.disallowValueField;
            }
            set
            {
                this.disallowValueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceAccountPasswordCriteria
    {

        private bool mixedCaseField;

        private int minLengthField;

        private int minNumericField;

        private int minAlphaField;

        private int minSpecialField;

        private bool disallow3XRepeatedCharField;

        private bool disallowWebTextAccountsField;

        private bool disallowListField;

        private string[] disallowValueField;

        /// <remarks/>
        public bool mixedCase
        {
            get
            {
                return this.mixedCaseField;
            }
            set
            {
                this.mixedCaseField = value;
            }
        }

        /// <remarks/>
        public int minLength
        {
            get
            {
                return this.minLengthField;
            }
            set
            {
                this.minLengthField = value;
            }
        }

        /// <remarks/>
        public int minNumeric
        {
            get
            {
                return this.minNumericField;
            }
            set
            {
                this.minNumericField = value;
            }
        }

        /// <remarks/>
        public int minAlpha
        {
            get
            {
                return this.minAlphaField;
            }
            set
            {
                this.minAlphaField = value;
            }
        }

        /// <remarks/>
        public int minSpecial
        {
            get
            {
                return this.minSpecialField;
            }
            set
            {
                this.minSpecialField = value;
            }
        }

        /// <remarks/>
        public bool disallow3XRepeatedChar
        {
            get
            {
                return this.disallow3XRepeatedCharField;
            }
            set
            {
                this.disallow3XRepeatedCharField = value;
            }
        }

        /// <remarks/>
        public bool disallowWebTextAccounts
        {
            get
            {
                return this.disallowWebTextAccountsField;
            }
            set
            {
                this.disallowWebTextAccountsField = value;
            }
        }

        /// <remarks/>
        public bool disallowList
        {
            get
            {
                return this.disallowListField;
            }
            set
            {
                this.disallowListField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("disallowValue")]
        public string[] disallowValue
        {
            get
            {
                return this.disallowValueField;
            }
            set
            {
                this.disallowValueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceProductivityTools
    {

        private bool enableField;

        private siteInstanceProductivityToolsInstallOpts installOptsField;

        private siteInstanceProductivityToolsIntegrations integrationsField;

        private siteInstanceProductivityToolsOneClick oneClickField;

        private siteInstanceProductivityToolsTemplates templatesField;

        private siteInstanceProductivityToolsLockDownPT lockDownPTField;

        private siteInstanceProductivityToolsImSettings imSettingsField;

        /// <remarks/>
        public bool enable
        {
            get
            {
                return this.enableField;
            }
            set
            {
                this.enableField = value;
            }
        }

        /// <remarks/>
        public siteInstanceProductivityToolsInstallOpts installOpts
        {
            get
            {
                return this.installOptsField;
            }
            set
            {
                this.installOptsField = value;
            }
        }

        /// <remarks/>
        public siteInstanceProductivityToolsIntegrations integrations
        {
            get
            {
                return this.integrationsField;
            }
            set
            {
                this.integrationsField = value;
            }
        }

        /// <remarks/>
        public siteInstanceProductivityToolsOneClick oneClick
        {
            get
            {
                return this.oneClickField;
            }
            set
            {
                this.oneClickField = value;
            }
        }

        /// <remarks/>
        public siteInstanceProductivityToolsTemplates templates
        {
            get
            {
                return this.templatesField;
            }
            set
            {
                this.templatesField = value;
            }
        }

        /// <remarks/>
        public siteInstanceProductivityToolsLockDownPT lockDownPT
        {
            get
            {
                return this.lockDownPTField;
            }
            set
            {
                this.lockDownPTField = value;
            }
        }

        /// <remarks/>
        public siteInstanceProductivityToolsImSettings imSettings
        {
            get
            {
                return this.imSettingsField;
            }
            set
            {
                this.imSettingsField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceProductivityToolsInstallOpts
    {

        private bool autoUpdateField;

        /// <remarks/>
        public bool autoUpdate
        {
            get
            {
                return this.autoUpdateField;
            }
            set
            {
                this.autoUpdateField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceProductivityToolsIntegrations
    {

        private bool outlookField;

        private bool outlookForMacField;

        private bool lotusNotesField;

        private bool oneClickField;

        private bool showSysTrayIconField;

        private bool officeField;

        private bool excelField;

        private bool powerPointField;

        private bool wordField;

        private bool ieField;

        private bool firefoxField;

        private bool explorerRightClickField;

        private bool instantMessengerField;

        private bool aolMessengerField;

        private bool googleTalkField;

        private bool lotusSametimeField;

        private bool skypeField;

        private bool windowsMessengerField;

        private bool yahooMessengerField;

        private bool ciscoIPPhoneField;

        private bool pcNowField;

        private bool iGoogleField;

        private bool iPhoneDustingField;

        /// <remarks/>
        public bool outlook
        {
            get
            {
                return this.outlookField;
            }
            set
            {
                this.outlookField = value;
            }
        }

        /// <remarks/>
        public bool outlookForMac
        {
            get
            {
                return this.outlookForMacField;
            }
            set
            {
                this.outlookForMacField = value;
            }
        }

        /// <remarks/>
        public bool lotusNotes
        {
            get
            {
                return this.lotusNotesField;
            }
            set
            {
                this.lotusNotesField = value;
            }
        }

        /// <remarks/>
        public bool oneClick
        {
            get
            {
                return this.oneClickField;
            }
            set
            {
                this.oneClickField = value;
            }
        }

        /// <remarks/>
        public bool showSysTrayIcon
        {
            get
            {
                return this.showSysTrayIconField;
            }
            set
            {
                this.showSysTrayIconField = value;
            }
        }

        /// <remarks/>
        public bool office
        {
            get
            {
                return this.officeField;
            }
            set
            {
                this.officeField = value;
            }
        }

        /// <remarks/>
        public bool excel
        {
            get
            {
                return this.excelField;
            }
            set
            {
                this.excelField = value;
            }
        }

        /// <remarks/>
        public bool powerPoint
        {
            get
            {
                return this.powerPointField;
            }
            set
            {
                this.powerPointField = value;
            }
        }

        /// <remarks/>
        public bool word
        {
            get
            {
                return this.wordField;
            }
            set
            {
                this.wordField = value;
            }
        }

        /// <remarks/>
        public bool IE
        {
            get
            {
                return this.ieField;
            }
            set
            {
                this.ieField = value;
            }
        }

        /// <remarks/>
        public bool firefox
        {
            get
            {
                return this.firefoxField;
            }
            set
            {
                this.firefoxField = value;
            }
        }

        /// <remarks/>
        public bool explorerRightClick
        {
            get
            {
                return this.explorerRightClickField;
            }
            set
            {
                this.explorerRightClickField = value;
            }
        }

        /// <remarks/>
        public bool instantMessenger
        {
            get
            {
                return this.instantMessengerField;
            }
            set
            {
                this.instantMessengerField = value;
            }
        }

        /// <remarks/>
        public bool aolMessenger
        {
            get
            {
                return this.aolMessengerField;
            }
            set
            {
                this.aolMessengerField = value;
            }
        }

        /// <remarks/>
        public bool googleTalk
        {
            get
            {
                return this.googleTalkField;
            }
            set
            {
                this.googleTalkField = value;
            }
        }

        /// <remarks/>
        public bool lotusSametime
        {
            get
            {
                return this.lotusSametimeField;
            }
            set
            {
                this.lotusSametimeField = value;
            }
        }

        /// <remarks/>
        public bool skype
        {
            get
            {
                return this.skypeField;
            }
            set
            {
                this.skypeField = value;
            }
        }

        /// <remarks/>
        public bool windowsMessenger
        {
            get
            {
                return this.windowsMessengerField;
            }
            set
            {
                this.windowsMessengerField = value;
            }
        }

        /// <remarks/>
        public bool yahooMessenger
        {
            get
            {
                return this.yahooMessengerField;
            }
            set
            {
                this.yahooMessengerField = value;
            }
        }

        /// <remarks/>
        public bool ciscoIPPhone
        {
            get
            {
                return this.ciscoIPPhoneField;
            }
            set
            {
                this.ciscoIPPhoneField = value;
            }
        }

        /// <remarks/>
        public bool pcNow
        {
            get
            {
                return this.pcNowField;
            }
            set
            {
                this.pcNowField = value;
            }
        }

        /// <remarks/>
        public bool iGoogle
        {
            get
            {
                return this.iGoogleField;
            }
            set
            {
                this.iGoogleField = value;
            }
        }

        /// <remarks/>
        public bool iPhoneDusting
        {
            get
            {
                return this.iPhoneDustingField;
            }
            set
            {
                this.iPhoneDustingField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceProductivityToolsOneClick
    {

        private bool allowJoinUnlistMeetingField;

        private bool requireApproveJoinField;

        /// <remarks/>
        public bool allowJoinUnlistMeeting
        {
            get
            {
                return this.allowJoinUnlistMeetingField;
            }
            set
            {
                this.allowJoinUnlistMeetingField = value;
            }
        }

        /// <remarks/>
        public bool requireApproveJoin
        {
            get
            {
                return this.requireApproveJoinField;
            }
            set
            {
                this.requireApproveJoinField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceProductivityToolsTemplates
    {

        private bool useTemplateField;

        /// <remarks/>
        public bool useTemplate
        {
            get
            {
                return this.useTemplateField;
            }
            set
            {
                this.useTemplateField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceProductivityToolsLockDownPT
    {

        private bool lockDownField;

        /// <remarks/>
        public bool lockDown
        {
            get
            {
                return this.lockDownField;
            }
            set
            {
                this.lockDownField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceProductivityToolsImSettings
    {

        private bool attendeeInviteOtherField;

        /// <remarks/>
        public bool attendeeInviteOther
        {
            get
            {
                return this.attendeeInviteOtherField;
            }
            set
            {
                this.attendeeInviteOtherField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceMeetingPlace
    {

        private string mpProfileURLField;

        private string mpLogoutURLField;

        private string mpInternalMeetingLinkField;

        private string nbrProfileNumberField;

        private string nbrProfilePasswordField;

        /// <remarks/>
        public string mpProfileURL
        {
            get
            {
                return this.mpProfileURLField;
            }
            set
            {
                this.mpProfileURLField = value;
            }
        }

        /// <remarks/>
        public string mpLogoutURL
        {
            get
            {
                return this.mpLogoutURLField;
            }
            set
            {
                this.mpLogoutURLField = value;
            }
        }

        /// <remarks/>
        public string mpInternalMeetingLink
        {
            get
            {
                return this.mpInternalMeetingLinkField;
            }
            set
            {
                this.mpInternalMeetingLinkField = value;
            }
        }

        /// <remarks/>
        public string nbrProfileNumber
        {
            get
            {
                return this.nbrProfileNumberField;
            }
            set
            {
                this.nbrProfileNumberField = value;
            }
        }

        /// <remarks/>
        public string nbrProfilePassword
        {
            get
            {
                return this.nbrProfilePasswordField;
            }
            set
            {
                this.nbrProfilePasswordField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceEventCenter
    {

        private siteInstanceEventCenterStandardFields standardFieldsField;

        private string customFieldsField;

        /// <remarks/>
        public siteInstanceEventCenterStandardFields standardFields
        {
            get
            {
                return this.standardFieldsField;
            }
            set
            {
                this.standardFieldsField = value;
            }
        }

        /// <remarks/>
        public string customFields
        {
            get
            {
                return this.customFieldsField;
            }
            set
            {
                this.customFieldsField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceEventCenterStandardFields
    {

        private phone phoneField;

        private company companyField;

        private title titleField;

        private numEmployees numEmployeesField;

        private futureInfo futureInfoField;

        private address1 address1Field;

        private address2 address2Field;

        private city cityField;

        private state stateField;

        private postalCode postalCodeField;

        private country countryField;

        private siteInstanceEventCenterStandardFieldsFirstName firstNameField;

        private siteInstanceEventCenterStandardFieldsLastName lastNameField;

        private siteInstanceEventCenterStandardFieldsEmailAddress emailAddressField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/event")]
        public phone phone
        {
            get
            {
                return this.phoneField;
            }
            set
            {
                this.phoneField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/event")]
        public company company
        {
            get
            {
                return this.companyField;
            }
            set
            {
                this.companyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/event")]
        public title title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/event")]
        public numEmployees numEmployees
        {
            get
            {
                return this.numEmployeesField;
            }
            set
            {
                this.numEmployeesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/event")]
        public futureInfo futureInfo
        {
            get
            {
                return this.futureInfoField;
            }
            set
            {
                this.futureInfoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/event")]
        public address1 address1
        {
            get
            {
                return this.address1Field;
            }
            set
            {
                this.address1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/event")]
        public address2 address2
        {
            get
            {
                return this.address2Field;
            }
            set
            {
                this.address2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/event")]
        public city city
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/event")]
        public state state
        {
            get
            {
                return this.stateField;
            }
            set
            {
                this.stateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/event")]
        public postalCode postalCode
        {
            get
            {
                return this.postalCodeField;
            }
            set
            {
                this.postalCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/event")]
        public country country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>
        public siteInstanceEventCenterStandardFieldsFirstName firstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public siteInstanceEventCenterStandardFieldsLastName lastName
        {
            get
            {
                return this.lastNameField;
            }
            set
            {
                this.lastNameField = value;
            }
        }

        /// <remarks/>
        public siteInstanceEventCenterStandardFieldsEmailAddress emailAddress
        {
            get
            {
                return this.emailAddressField;
            }
            set
            {
                this.emailAddressField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/event")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/event", IsNullable = false)]
    public partial class phone
    {

        private bool reqField;

        /// <remarks/>
        public bool req
        {
            get
            {
                return this.reqField;
            }
            set
            {
                this.reqField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/event")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/event", IsNullable = false)]
    public partial class company
    {

        private bool reqField;

        /// <remarks/>
        public bool req
        {
            get
            {
                return this.reqField;
            }
            set
            {
                this.reqField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/event")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/event", IsNullable = false)]
    public partial class title
    {

        private bool reqField;

        /// <remarks/>
        public bool req
        {
            get
            {
                return this.reqField;
            }
            set
            {
                this.reqField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/event")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/event", IsNullable = false)]
    public partial class numEmployees
    {

        private bool reqField;

        /// <remarks/>
        public bool req
        {
            get
            {
                return this.reqField;
            }
            set
            {
                this.reqField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/event")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/event", IsNullable = false)]
    public partial class futureInfo
    {

        private bool reqField;

        /// <remarks/>
        public bool req
        {
            get
            {
                return this.reqField;
            }
            set
            {
                this.reqField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/event")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/event", IsNullable = false)]
    public partial class address1
    {

        private bool reqField;

        /// <remarks/>
        public bool req
        {
            get
            {
                return this.reqField;
            }
            set
            {
                this.reqField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/event")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/event", IsNullable = false)]
    public partial class address2
    {

        private bool reqField;

        /// <remarks/>
        public bool req
        {
            get
            {
                return this.reqField;
            }
            set
            {
                this.reqField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/event")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/event", IsNullable = false)]
    public partial class city
    {

        private bool reqField;

        /// <remarks/>
        public bool req
        {
            get
            {
                return this.reqField;
            }
            set
            {
                this.reqField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/event")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/event", IsNullable = false)]
    public partial class state
    {

        private bool reqField;

        /// <remarks/>
        public bool req
        {
            get
            {
                return this.reqField;
            }
            set
            {
                this.reqField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/event")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/event", IsNullable = false)]
    public partial class postalCode
    {

        private bool reqField;

        /// <remarks/>
        public bool req
        {
            get
            {
                return this.reqField;
            }
            set
            {
                this.reqField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/event")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/event", IsNullable = false)]
    public partial class country
    {

        private bool reqField;

        /// <remarks/>
        public bool req
        {
            get
            {
                return this.reqField;
            }
            set
            {
                this.reqField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceEventCenterStandardFieldsFirstName
    {

        private bool reqField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/event")]
        public bool req
        {
            get
            {
                return this.reqField;
            }
            set
            {
                this.reqField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceEventCenterStandardFieldsLastName
    {

        private bool reqField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/event")]
        public bool req
        {
            get
            {
                return this.reqField;
            }
            set
            {
                this.reqField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceEventCenterStandardFieldsEmailAddress
    {

        private bool reqField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/event")]
        public bool req
        {
            get
            {
                return this.reqField;
            }
            set
            {
                this.reqField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceSalesCenter
    {

        private bool allowJoinWithoutLoginField;

        /// <remarks/>
        public bool allowJoinWithoutLogin
        {
            get
            {
                return this.allowJoinWithoutLoginField;
            }
            set
            {
                this.allowJoinWithoutLoginField = value;
            }
        }
    }


    #endregion

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user", IsNullable = false)]
    public partial class eventCenter
    {

        private bool optimizeAttendeeBandwidthUsageField;

        /// <remarks/>
        public bool optimizeAttendeeBandwidthUsage
        {
            get
            {
                return this.optimizeAttendeeBandwidthUsageField;
            }
            set
            {
                this.optimizeAttendeeBandwidthUsageField = value;
            }
        }
    }



    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user", IsNullable = false)]
    public partial class avatar
    {

        private string urlField;

        private ulong lastModifiedTimeField;

        private string initialsField;

        private bool isUploadedField;

        /// <remarks/>
        public string url
        {
            get
            {
                return this.urlField;
            }
            set
            {
                this.urlField = value;
            }
        }

        /// <remarks/>
        public ulong lastModifiedTime
        {
            get
            {
                return this.lastModifiedTimeField;
            }
            set
            {
                this.lastModifiedTimeField = value;
            }
        }

        /// <remarks/>
        public string initials
        {
            get
            {
                return this.initialsField;
            }
            set
            {
                this.initialsField = value;
            }
        }

        /// <remarks/>
        public bool isUploaded
        {
            get
            {
                return this.isUploadedField;
            }
            set
            {
                this.isUploadedField = value;
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user", IsNullable = false)]
        public partial class tracking
        {

            private string trackingCode1Field;

            private string trackingCode2Field;

            private string trackingCode3Field;

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/common")]
            public string trackingCode1
            {
                get
                {
                    return this.trackingCode1Field;
                }
                set
                {
                    this.trackingCode1Field = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/common")]
            public string trackingCode2
            {
                get
                {
                    return this.trackingCode2Field;
                }
                set
                {
                    this.trackingCode2Field = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/common")]
            public string trackingCode3
            {
                get
                {
                    return this.trackingCode3Field;
                }
                set
                {
                    this.trackingCode3Field = value;
                }
            }
        }

    }


    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user", IsNullable = false)]
    public partial class remoteSupport
    {

        private string sharingViewField;

        private string sharingColorField;

        private remoteSupportRecording recordingField;

        private remoteSupportAutoRequest autoRequestField;

        private remoteSupportDefaultClient defaultClientField;

        /// <remarks/>
        public string sharingView
        {
            get
            {
                return this.sharingViewField;
            }
            set
            {
                this.sharingViewField = value;
            }
        }

        /// <remarks/>
        public string sharingColor
        {
            get
            {
                return this.sharingColorField;
            }
            set
            {
                this.sharingColorField = value;
            }
        }

        /// <remarks/>
        public remoteSupportRecording recording
        {
            get
            {
                return this.recordingField;
            }
            set
            {
                this.recordingField = value;
            }
        }

        /// <remarks/>
        public remoteSupportAutoRequest autoRequest
        {
            get
            {
                return this.autoRequestField;
            }
            set
            {
                this.autoRequestField = value;
            }
        }

        /// <remarks/>
        public remoteSupportDefaultClient defaultClient
        {
            get
            {
                return this.defaultClientField;
            }
            set
            {
                this.defaultClientField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
    public partial class remoteSupportRecording
    {

        private bool enforceField;

        /// <remarks/>
        public bool enforce
        {
            get
            {
                return this.enforceField;
            }
            set
            {
                this.enforceField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
    public partial class remoteSupportAutoRequest
    {

        private bool enableField;

        /// <remarks/>
        public bool enable
        {
            get
            {
                return this.enableField;
            }
            set
            {
                this.enableField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
    public partial class remoteSupportDefaultClient
    {

        private string typeField;

        private string singleSessClientField;

        /// <remarks/>
        public string type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        public string singleSessClient
        {
            get
            {
                return this.singleSessClientField;
            }
            set
            {
                this.singleSessClientField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/user")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/user", IsNullable = false)]
    public partial class tracking
    {

        private string trackingCode1Field;

        private string trackingCode2Field;

        private string trackingCode3Field;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/common")]
        public string trackingCode1
        {
            get
            {
                return this.trackingCode1Field;
            }
            set
            {
                this.trackingCode1Field = value;
            }
        }
    }

}

