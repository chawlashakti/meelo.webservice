﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.Conferencing.WebEx.Site
{


    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service", IsNullable = false)]
    public partial class message
    {

        private messageHeader headerField;

        private messageBody bodyField;

        /// <remarks/>
        public messageHeader header
        {
            get
            {
                return this.headerField;
            }
            set
            {
                this.headerField = value;
            }
        }

        /// <remarks/>
        public messageBody body
        {
            get
            {
                return this.bodyField;
            }
            set
            {
                this.bodyField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service")]
    public partial class messageHeader
    {

        private messageHeaderResponse responseField;

        /// <remarks/>
        public messageHeaderResponse response
        {
            get
            {
                return this.responseField;
            }
            set
            {
                this.responseField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service")]
    public partial class messageHeaderResponse
    {

        private string resultField;

        private string reasonField;

        private string gsbStatusField;

        private uint exceptionIDField;

        private messageHeaderResponseSubErrors subErrorsField;

        /// <remarks/>
        public string result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        public string reason
        {
            get
            {
                return this.reasonField;
            }
            set
            {
                this.reasonField = value;
            }
        }

        /// <remarks/>
        public string gsbStatus
        {
            get
            {
                return this.gsbStatusField;
            }
            set
            {
                this.gsbStatusField = value;
            }
        }

        /// <remarks/>
        public uint exceptionID
        {
            get
            {
                return this.exceptionIDField;
            }
            set
            {
                this.exceptionIDField = value;
            }
        }

        /// <remarks/>
        public messageHeaderResponseSubErrors subErrors
        {
            get
            {
                return this.subErrorsField;
            }
            set
            {
                this.subErrorsField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service")]
    public partial class messageHeaderResponseSubErrors
    {

        private messageHeaderResponseSubErrorsSubError subErrorField;

        /// <remarks/>
        public messageHeaderResponseSubErrorsSubError subError
        {
            get
            {
                return this.subErrorField;
            }
            set
            {
                this.subErrorField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service")]
    public partial class messageHeaderResponseSubErrorsSubError
    {

        private string exceptionIDField;

        private string reasonField;

        private string valueField;

        /// <remarks/>
        public string exceptionID
        {
            get
            {
                return this.exceptionIDField;
            }
            set
            {
                this.exceptionIDField = value;
            }
        }

        /// <remarks/>
        public string reason
        {
            get
            {
                return this.reasonField;
            }
            set
            {
                this.reasonField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }


    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service")]
    public partial class messageBody
    {

        private messageBodyBodyContent bodyContentField;

        /// <remarks/>
        public messageBodyBodyContent bodyContent
        {
            get
            {
                return this.bodyContentField;
            }
            set
            {
                this.bodyContentField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service")]
    public partial class messageBodyBodyContent
    {

        private siteInstance siteInstanceField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
        public siteInstance siteInstance
        {
            get
            {
                return this.siteInstanceField;
            }
            set
            {
                this.siteInstanceField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service/site", IsNullable = false)]
    public partial class siteInstance
    {

        private siteInstanceMetaData metaDataField;

        private siteInstanceUcf ucfField;

        private siteInstanceClientPlatforms clientPlatformsField;

        private siteInstanceResourceRestrictions resourceRestrictionsField;

        private siteInstanceSupportAPI supportAPIField;

        private siteInstanceMyWebExConfig myWebExConfigField;

        private siteInstanceTelephonyConfig telephonyConfigField;

        private siteInstanceCommerceAndReporting commerceAndReportingField;

        private siteInstanceTools toolsField;

        private siteInstanceCustCommunications custCommunicationsField;

        private siteInstanceTrackingCode[] trackingCodesField;

        private siteInstanceSupportedServices supportedServicesField;

        private siteInstanceSecurityOptions securityOptionsField;

        private siteInstanceDefaults defaultsField;

        private siteInstanceScheduleMeetingOptions scheduleMeetingOptionsField;

        private siteInstanceNavBarTop navBarTopField;

        private siteInstanceNavMyWebEx navMyWebExField;

        private siteInstanceNavAllServices navAllServicesField;

        private siteInstancePasswordCriteria passwordCriteriaField;

        private siteInstanceRecordingPasswordCriteria recordingPasswordCriteriaField;

        private siteInstanceAccountPasswordCriteria accountPasswordCriteriaField;

        private siteInstanceProductivityTools productivityToolsField;

        private string meetingPlaceField;

        private siteInstanceSalesCenter salesCenterField;

        private siteInstanceConnectIntegration connectIntegrationField;

        private siteInstanceVideo videoField;

        private siteInstanceSiteCommonOptions siteCommonOptionsField;

        private siteInstanceSamlSSO samlSSOField;

        private siteInstanceAttendeeLimitation attendeeLimitationField;

        /// <remarks/>
        public siteInstanceMetaData metaData
        {
            get
            {
                return this.metaDataField;
            }
            set
            {
                this.metaDataField = value;
            }
        }

        /// <remarks/>
        public siteInstanceUcf ucf
        {
            get
            {
                return this.ucfField;
            }
            set
            {
                this.ucfField = value;
            }
        }

        /// <remarks/>
        public siteInstanceClientPlatforms clientPlatforms
        {
            get
            {
                return this.clientPlatformsField;
            }
            set
            {
                this.clientPlatformsField = value;
            }
        }

        /// <remarks/>
        public siteInstanceResourceRestrictions resourceRestrictions
        {
            get
            {
                return this.resourceRestrictionsField;
            }
            set
            {
                this.resourceRestrictionsField = value;
            }
        }

        /// <remarks/>
        public siteInstanceSupportAPI supportAPI
        {
            get
            {
                return this.supportAPIField;
            }
            set
            {
                this.supportAPIField = value;
            }
        }

        /// <remarks/>
        public siteInstanceMyWebExConfig myWebExConfig
        {
            get
            {
                return this.myWebExConfigField;
            }
            set
            {
                this.myWebExConfigField = value;
            }
        }

        /// <remarks/>
        public siteInstanceTelephonyConfig telephonyConfig
        {
            get
            {
                return this.telephonyConfigField;
            }
            set
            {
                this.telephonyConfigField = value;
            }
        }

        /// <remarks/>
        public siteInstanceCommerceAndReporting commerceAndReporting
        {
            get
            {
                return this.commerceAndReportingField;
            }
            set
            {
                this.commerceAndReportingField = value;
            }
        }

        /// <remarks/>
        public siteInstanceTools tools
        {
            get
            {
                return this.toolsField;
            }
            set
            {
                this.toolsField = value;
            }
        }

        /// <remarks/>
        public siteInstanceCustCommunications custCommunications
        {
            get
            {
                return this.custCommunicationsField;
            }
            set
            {
                this.custCommunicationsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("trackingCode", IsNullable = false)]
        public siteInstanceTrackingCode[] trackingCodes
        {
            get
            {
                return this.trackingCodesField;
            }
            set
            {
                this.trackingCodesField = value;
            }
        }

        /// <remarks/>
        public siteInstanceSupportedServices supportedServices
        {
            get
            {
                return this.supportedServicesField;
            }
            set
            {
                this.supportedServicesField = value;
            }
        }

        /// <remarks/>
        public siteInstanceSecurityOptions securityOptions
        {
            get
            {
                return this.securityOptionsField;
            }
            set
            {
                this.securityOptionsField = value;
            }
        }

        /// <remarks/>
        public siteInstanceDefaults defaults
        {
            get
            {
                return this.defaultsField;
            }
            set
            {
                this.defaultsField = value;
            }
        }

        /// <remarks/>
        public siteInstanceScheduleMeetingOptions scheduleMeetingOptions
        {
            get
            {
                return this.scheduleMeetingOptionsField;
            }
            set
            {
                this.scheduleMeetingOptionsField = value;
            }
        }

        /// <remarks/>
        public siteInstanceNavBarTop navBarTop
        {
            get
            {
                return this.navBarTopField;
            }
            set
            {
                this.navBarTopField = value;
            }
        }

        /// <remarks/>
        public siteInstanceNavMyWebEx navMyWebEx
        {
            get
            {
                return this.navMyWebExField;
            }
            set
            {
                this.navMyWebExField = value;
            }
        }

        /// <remarks/>
        public siteInstanceNavAllServices navAllServices
        {
            get
            {
                return this.navAllServicesField;
            }
            set
            {
                this.navAllServicesField = value;
            }
        }

        /// <remarks/>
        public siteInstancePasswordCriteria passwordCriteria
        {
            get
            {
                return this.passwordCriteriaField;
            }
            set
            {
                this.passwordCriteriaField = value;
            }
        }

        /// <remarks/>
        public siteInstanceRecordingPasswordCriteria recordingPasswordCriteria
        {
            get
            {
                return this.recordingPasswordCriteriaField;
            }
            set
            {
                this.recordingPasswordCriteriaField = value;
            }
        }

        /// <remarks/>
        public siteInstanceAccountPasswordCriteria accountPasswordCriteria
        {
            get
            {
                return this.accountPasswordCriteriaField;
            }
            set
            {
                this.accountPasswordCriteriaField = value;
            }
        }

        /// <remarks/>
        public siteInstanceProductivityTools productivityTools
        {
            get
            {
                return this.productivityToolsField;
            }
            set
            {
                this.productivityToolsField = value;
            }
        }

        /// <remarks/>
        public string meetingPlace
        {
            get
            {
                return this.meetingPlaceField;
            }
            set
            {
                this.meetingPlaceField = value;
            }
        }

        /// <remarks/>
        public siteInstanceSalesCenter salesCenter
        {
            get
            {
                return this.salesCenterField;
            }
            set
            {
                this.salesCenterField = value;
            }
        }

        /// <remarks/>
        public siteInstanceConnectIntegration connectIntegration
        {
            get
            {
                return this.connectIntegrationField;
            }
            set
            {
                this.connectIntegrationField = value;
            }
        }

        /// <remarks/>
        public siteInstanceVideo video
        {
            get
            {
                return this.videoField;
            }
            set
            {
                this.videoField = value;
            }
        }

        /// <remarks/>
        public siteInstanceSiteCommonOptions siteCommonOptions
        {
            get
            {
                return this.siteCommonOptionsField;
            }
            set
            {
                this.siteCommonOptionsField = value;
            }
        }

        /// <remarks/>
        public siteInstanceSamlSSO samlSSO
        {
            get
            {
                return this.samlSSOField;
            }
            set
            {
                this.samlSSOField = value;
            }
        }

        /// <remarks/>
        public siteInstanceAttendeeLimitation attendeeLimitation
        {
            get
            {
                return this.attendeeLimitationField;
            }
            set
            {
                this.attendeeLimitationField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceMetaData
    {

        private bool isEnterpriseField;

        private string[] serviceTypeField;

        private siteInstanceMetaDataMeetingTypes[] meetingTypesField;

        private string siteNameField;

        private string[] brandNameField;

        private string regionField;

        private string currencyField;

        private byte timeZoneIDField;

        private string timeZoneField;

        private string parterIDField;

        private string webDomainField;

        private string meetingDomainField;

        private string telephonyDomainField;

        private string pageVersionField;

        private string clientVersionField;

        private string pageLanguageField;

        private bool activateStatusField;

        private string webPageTypeField;

        private bool iCalendarField;

        private string myWebExDefaultPageField;

        private string componentVersionField;

        private ushort accountNumLimitField;

        private byte activeUserCountField;

        private uint auoAccountNumLimitField;

        private byte auoActiveUserCountField;

        private bool displayMeetingActualTimeField;

        private bool displayOffsetField;

        private bool supportWebEx11Field;

        /// <remarks/>
        public bool isEnterprise
        {
            get
            {
                return this.isEnterpriseField;
            }
            set
            {
                this.isEnterpriseField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("serviceType")]
        public string[] serviceType
        {
            get
            {
                return this.serviceTypeField;
            }
            set
            {
                this.serviceTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("meetingTypes")]
        public siteInstanceMetaDataMeetingTypes[] meetingTypes
        {
            get
            {
                return this.meetingTypesField;
            }
            set
            {
                this.meetingTypesField = value;
            }
        }

        /// <remarks/>
        public string siteName
        {
            get
            {
                return this.siteNameField;
            }
            set
            {
                this.siteNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("brandName")]
        public string[] brandName
        {
            get
            {
                return this.brandNameField;
            }
            set
            {
                this.brandNameField = value;
            }
        }

        /// <remarks/>
        public string region
        {
            get
            {
                return this.regionField;
            }
            set
            {
                this.regionField = value;
            }
        }

        /// <remarks/>
        public string currency
        {
            get
            {
                return this.currencyField;
            }
            set
            {
                this.currencyField = value;
            }
        }

        /// <remarks/>
        public byte timeZoneID
        {
            get
            {
                return this.timeZoneIDField;
            }
            set
            {
                this.timeZoneIDField = value;
            }
        }

        /// <remarks/>
        public string timeZone
        {
            get
            {
                return this.timeZoneField;
            }
            set
            {
                this.timeZoneField = value;
            }
        }

        /// <remarks/>
        public string parterID
        {
            get
            {
                return this.parterIDField;
            }
            set
            {
                this.parterIDField = value;
            }
        }

        /// <remarks/>
        public string webDomain
        {
            get
            {
                return this.webDomainField;
            }
            set
            {
                this.webDomainField = value;
            }
        }

        /// <remarks/>
        public string meetingDomain
        {
            get
            {
                return this.meetingDomainField;
            }
            set
            {
                this.meetingDomainField = value;
            }
        }

        /// <remarks/>
        public string telephonyDomain
        {
            get
            {
                return this.telephonyDomainField;
            }
            set
            {
                this.telephonyDomainField = value;
            }
        }

        /// <remarks/>
        public string pageVersion
        {
            get
            {
                return this.pageVersionField;
            }
            set
            {
                this.pageVersionField = value;
            }
        }

        /// <remarks/>
        public string clientVersion
        {
            get
            {
                return this.clientVersionField;
            }
            set
            {
                this.clientVersionField = value;
            }
        }

        /// <remarks/>
        public string pageLanguage
        {
            get
            {
                return this.pageLanguageField;
            }
            set
            {
                this.pageLanguageField = value;
            }
        }

        /// <remarks/>
        public bool activateStatus
        {
            get
            {
                return this.activateStatusField;
            }
            set
            {
                this.activateStatusField = value;
            }
        }

        /// <remarks/>
        public string webPageType
        {
            get
            {
                return this.webPageTypeField;
            }
            set
            {
                this.webPageTypeField = value;
            }
        }

        /// <remarks/>
        public bool iCalendar
        {
            get
            {
                return this.iCalendarField;
            }
            set
            {
                this.iCalendarField = value;
            }
        }

        /// <remarks/>
        public string myWebExDefaultPage
        {
            get
            {
                return this.myWebExDefaultPageField;
            }
            set
            {
                this.myWebExDefaultPageField = value;
            }
        }

        /// <remarks/>
        public string componentVersion
        {
            get
            {
                return this.componentVersionField;
            }
            set
            {
                this.componentVersionField = value;
            }
        }

        /// <remarks/>
        public ushort accountNumLimit
        {
            get
            {
                return this.accountNumLimitField;
            }
            set
            {
                this.accountNumLimitField = value;
            }
        }

        /// <remarks/>
        public byte activeUserCount
        {
            get
            {
                return this.activeUserCountField;
            }
            set
            {
                this.activeUserCountField = value;
            }
        }

        /// <remarks/>
        public uint auoAccountNumLimit
        {
            get
            {
                return this.auoAccountNumLimitField;
            }
            set
            {
                this.auoAccountNumLimitField = value;
            }
        }

        /// <remarks/>
        public byte auoActiveUserCount
        {
            get
            {
                return this.auoActiveUserCountField;
            }
            set
            {
                this.auoActiveUserCountField = value;
            }
        }

        /// <remarks/>
        public bool displayMeetingActualTime
        {
            get
            {
                return this.displayMeetingActualTimeField;
            }
            set
            {
                this.displayMeetingActualTimeField = value;
            }
        }

        /// <remarks/>
        public bool displayOffset
        {
            get
            {
                return this.displayOffsetField;
            }
            set
            {
                this.displayOffsetField = value;
            }
        }

        /// <remarks/>
        public bool supportWebEx11
        {
            get
            {
                return this.supportWebEx11Field;
            }
            set
            {
                this.supportWebEx11Field = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceMetaDataMeetingTypes
    {

        private ushort meetingTypeIDField;

        private string meetingTypeNameField;

        private bool hideInSchedulerField;

        private bool hideInSchedulerFieldSpecified;

        /// <remarks/>
        public ushort meetingTypeID
        {
            get
            {
                return this.meetingTypeIDField;
            }
            set
            {
                this.meetingTypeIDField = value;
            }
        }

        /// <remarks/>
        public string meetingTypeName
        {
            get
            {
                return this.meetingTypeNameField;
            }
            set
            {
                this.meetingTypeNameField = value;
            }
        }

        /// <remarks/>
        public bool hideInScheduler
        {
            get
            {
                return this.hideInSchedulerField;
            }
            set
            {
                this.hideInSchedulerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool hideInSchedulerSpecified
        {
            get
            {
                return this.hideInSchedulerFieldSpecified;
            }
            set
            {
                this.hideInSchedulerFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceUcf
    {

        private string ucfConfigurationField;

        /// <remarks/>
        public string ucfConfiguration
        {
            get
            {
                return this.ucfConfigurationField;
            }
            set
            {
                this.ucfConfigurationField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceClientPlatforms
    {

        private bool msWindowsField;

        private bool macOS9Field;

        private bool macOSXField;

        private bool sunSolarisField;

        private bool linuxField;

        private bool hpUnixField;

        private bool javaField;

        private bool palmField;

        /// <remarks/>
        public bool msWindows
        {
            get
            {
                return this.msWindowsField;
            }
            set
            {
                this.msWindowsField = value;
            }
        }

        /// <remarks/>
        public bool macOS9
        {
            get
            {
                return this.macOS9Field;
            }
            set
            {
                this.macOS9Field = value;
            }
        }

        /// <remarks/>
        public bool macOSX
        {
            get
            {
                return this.macOSXField;
            }
            set
            {
                this.macOSXField = value;
            }
        }

        /// <remarks/>
        public bool sunSolaris
        {
            get
            {
                return this.sunSolarisField;
            }
            set
            {
                this.sunSolarisField = value;
            }
        }

        /// <remarks/>
        public bool linux
        {
            get
            {
                return this.linuxField;
            }
            set
            {
                this.linuxField = value;
            }
        }

        /// <remarks/>
        public bool hpUnix
        {
            get
            {
                return this.hpUnixField;
            }
            set
            {
                this.hpUnixField = value;
            }
        }

        /// <remarks/>
        public bool java
        {
            get
            {
                return this.javaField;
            }
            set
            {
                this.javaField = value;
            }
        }

        /// <remarks/>
        public bool palm
        {
            get
            {
                return this.palmField;
            }
            set
            {
                this.palmField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceResourceRestrictions
    {

        private bool isLicenseManagerField;

        private byte concurrentLicenseField;

        private uint fileFolderCapacityField;

        private byte maxConcurrentEventsField;

        private byte archiveStorageLimitField;

        /// <remarks/>
        public bool isLicenseManager
        {
            get
            {
                return this.isLicenseManagerField;
            }
            set
            {
                this.isLicenseManagerField = value;
            }
        }

        /// <remarks/>
        public byte concurrentLicense
        {
            get
            {
                return this.concurrentLicenseField;
            }
            set
            {
                this.concurrentLicenseField = value;
            }
        }

        /// <remarks/>
        public uint fileFolderCapacity
        {
            get
            {
                return this.fileFolderCapacityField;
            }
            set
            {
                this.fileFolderCapacityField = value;
            }
        }

        /// <remarks/>
        public byte maxConcurrentEvents
        {
            get
            {
                return this.maxConcurrentEventsField;
            }
            set
            {
                this.maxConcurrentEventsField = value;
            }
        }

        /// <remarks/>
        public byte archiveStorageLimit
        {
            get
            {
                return this.archiveStorageLimitField;
            }
            set
            {
                this.archiveStorageLimitField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceSupportAPI
    {

        private bool autoLoginField;

        private bool aspAndPHPAPIField;

        private bool backwardAPIField;

        private bool xmlAPIField;

        private bool cAPIField;

        private bool scormField;

        /// <remarks/>
        public bool autoLogin
        {
            get
            {
                return this.autoLoginField;
            }
            set
            {
                this.autoLoginField = value;
            }
        }

        /// <remarks/>
        public bool aspAndPHPAPI
        {
            get
            {
                return this.aspAndPHPAPIField;
            }
            set
            {
                this.aspAndPHPAPIField = value;
            }
        }

        /// <remarks/>
        public bool backwardAPI
        {
            get
            {
                return this.backwardAPIField;
            }
            set
            {
                this.backwardAPIField = value;
            }
        }

        /// <remarks/>
        public bool xmlAPI
        {
            get
            {
                return this.xmlAPIField;
            }
            set
            {
                this.xmlAPIField = value;
            }
        }

        /// <remarks/>
        public bool cAPI
        {
            get
            {
                return this.cAPIField;
            }
            set
            {
                this.cAPIField = value;
            }
        }

        /// <remarks/>
        public bool scorm
        {
            get
            {
                return this.scormField;
            }
            set
            {
                this.scormField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceMyWebExConfig
    {

        private bool myContactsField;

        private bool myProfileField;

        private bool myMeetingsField;

        private bool trainingRecordingsField;

        private bool foldersField;

        private bool eventDocumentField;

        private bool myReportField;

        private bool myComputerField;

        private bool personalMeetingPageField;

        private byte myFilesStorageField;

        private byte myComputerNumbersField;

        private bool enableMyWebExProField;

        private uint myWebExProMaxHostsField;

        private bool restrictAccessAnyAppsField;

        private byte restrictAccessAnyAppsNumField;

        private string addlAccessAnyComputersLimitField;

        private byte addlAccessAnyComputersField;

        private string addlStorageLimitField;

        private byte addlStorageField;

        private bool myContactsProField;

        private bool myProfileProField;

        private bool myMeetingsProField;

        private bool trainingRecordingsProField;

        private bool foldersProField;

        private bool eventDocumentProField;

        private bool myReportProField;

        private bool myComputerProField;

        private bool personalMeetingPageProField;

        private ushort myFilesStorageProField;

        private byte myComputerNumbersProField;

        private bool pMRheaderBrandingField;

        /// <remarks/>
        public bool myContacts
        {
            get
            {
                return this.myContactsField;
            }
            set
            {
                this.myContactsField = value;
            }
        }

        /// <remarks/>
        public bool myProfile
        {
            get
            {
                return this.myProfileField;
            }
            set
            {
                this.myProfileField = value;
            }
        }

        /// <remarks/>
        public bool myMeetings
        {
            get
            {
                return this.myMeetingsField;
            }
            set
            {
                this.myMeetingsField = value;
            }
        }

        /// <remarks/>
        public bool trainingRecordings
        {
            get
            {
                return this.trainingRecordingsField;
            }
            set
            {
                this.trainingRecordingsField = value;
            }
        }

        /// <remarks/>
        public bool folders
        {
            get
            {
                return this.foldersField;
            }
            set
            {
                this.foldersField = value;
            }
        }

        /// <remarks/>
        public bool eventDocument
        {
            get
            {
                return this.eventDocumentField;
            }
            set
            {
                this.eventDocumentField = value;
            }
        }

        /// <remarks/>
        public bool myReport
        {
            get
            {
                return this.myReportField;
            }
            set
            {
                this.myReportField = value;
            }
        }

        /// <remarks/>
        public bool myComputer
        {
            get
            {
                return this.myComputerField;
            }
            set
            {
                this.myComputerField = value;
            }
        }

        /// <remarks/>
        public bool personalMeetingPage
        {
            get
            {
                return this.personalMeetingPageField;
            }
            set
            {
                this.personalMeetingPageField = value;
            }
        }

        /// <remarks/>
        public byte myFilesStorage
        {
            get
            {
                return this.myFilesStorageField;
            }
            set
            {
                this.myFilesStorageField = value;
            }
        }

        /// <remarks/>
        public byte myComputerNumbers
        {
            get
            {
                return this.myComputerNumbersField;
            }
            set
            {
                this.myComputerNumbersField = value;
            }
        }

        /// <remarks/>
        public bool enableMyWebExPro
        {
            get
            {
                return this.enableMyWebExProField;
            }
            set
            {
                this.enableMyWebExProField = value;
            }
        }

        /// <remarks/>
        public uint myWebExProMaxHosts
        {
            get
            {
                return this.myWebExProMaxHostsField;
            }
            set
            {
                this.myWebExProMaxHostsField = value;
            }
        }

        /// <remarks/>
        public bool restrictAccessAnyApps
        {
            get
            {
                return this.restrictAccessAnyAppsField;
            }
            set
            {
                this.restrictAccessAnyAppsField = value;
            }
        }

        /// <remarks/>
        public byte restrictAccessAnyAppsNum
        {
            get
            {
                return this.restrictAccessAnyAppsNumField;
            }
            set
            {
                this.restrictAccessAnyAppsNumField = value;
            }
        }

        /// <remarks/>
        public string addlAccessAnyComputersLimit
        {
            get
            {
                return this.addlAccessAnyComputersLimitField;
            }
            set
            {
                this.addlAccessAnyComputersLimitField = value;
            }
        }

        /// <remarks/>
        public byte addlAccessAnyComputers
        {
            get
            {
                return this.addlAccessAnyComputersField;
            }
            set
            {
                this.addlAccessAnyComputersField = value;
            }
        }

        /// <remarks/>
        public string addlStorageLimit
        {
            get
            {
                return this.addlStorageLimitField;
            }
            set
            {
                this.addlStorageLimitField = value;
            }
        }

        /// <remarks/>
        public byte addlStorage
        {
            get
            {
                return this.addlStorageField;
            }
            set
            {
                this.addlStorageField = value;
            }
        }

        /// <remarks/>
        public bool myContactsPro
        {
            get
            {
                return this.myContactsProField;
            }
            set
            {
                this.myContactsProField = value;
            }
        }

        /// <remarks/>
        public bool myProfilePro
        {
            get
            {
                return this.myProfileProField;
            }
            set
            {
                this.myProfileProField = value;
            }
        }

        /// <remarks/>
        public bool myMeetingsPro
        {
            get
            {
                return this.myMeetingsProField;
            }
            set
            {
                this.myMeetingsProField = value;
            }
        }

        /// <remarks/>
        public bool trainingRecordingsPro
        {
            get
            {
                return this.trainingRecordingsProField;
            }
            set
            {
                this.trainingRecordingsProField = value;
            }
        }

        /// <remarks/>
        public bool foldersPro
        {
            get
            {
                return this.foldersProField;
            }
            set
            {
                this.foldersProField = value;
            }
        }

        /// <remarks/>
        public bool eventDocumentPro
        {
            get
            {
                return this.eventDocumentProField;
            }
            set
            {
                this.eventDocumentProField = value;
            }
        }

        /// <remarks/>
        public bool myReportPro
        {
            get
            {
                return this.myReportProField;
            }
            set
            {
                this.myReportProField = value;
            }
        }

        /// <remarks/>
        public bool myComputerPro
        {
            get
            {
                return this.myComputerProField;
            }
            set
            {
                this.myComputerProField = value;
            }
        }

        /// <remarks/>
        public bool personalMeetingPagePro
        {
            get
            {
                return this.personalMeetingPageProField;
            }
            set
            {
                this.personalMeetingPageProField = value;
            }
        }

        /// <remarks/>
        public ushort myFilesStoragePro
        {
            get
            {
                return this.myFilesStorageProField;
            }
            set
            {
                this.myFilesStorageProField = value;
            }
        }

        /// <remarks/>
        public byte myComputerNumbersPro
        {
            get
            {
                return this.myComputerNumbersProField;
            }
            set
            {
                this.myComputerNumbersProField = value;
            }
        }

        /// <remarks/>
        public bool PMRheaderBranding
        {
            get
            {
                return this.pMRheaderBrandingField;
            }
            set
            {
                this.pMRheaderBrandingField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTelephonyConfig
    {

        private bool isTSPUsingTelephonyAPIField;

        private string serviceNameField;

        private string participantAccessCodeLabelField;

        private string subscriberAccessCodeLabelField;

        private string attendeeIDLabelField;

        private bool internetPhoneField;

        private bool supportCallInTypeTeleconfField;

        private bool callInTeleconferencingField;

        private bool tollFreeCallinTeleconferencingField;

        private bool intlCallInTeleconferencingField;

        private bool callBackTeleconferencingField;

        private byte callInNumberField;

        private string defaultTeleServerSubjectField;

        private string subscribeNameField;

        private string subscribePasswordField;

        private byte defaultPhoneLinesField;

        private byte defaultSpeakingLinesField;

        private byte majorCountryCodeField;

        private ushort majorAreaCodeField;

        private string publicNameField;

        private bool hybridTeleconferenceField;

        private bool instantHelpField;

        private bool customerManageField;

        private byte maxCallersNumberField;

        private bool isSpecified1Field;

        private bool isContinueField;

        private bool intlCallBackTeleconferencingField;

        private siteInstanceTelephonyConfigPersonalTeleconf personalTeleconfField;

        private bool multiMediaPlatformField;

        private string multiMediaHostNameField;

        private bool broadcastAudioStreamField;

        private siteInstanceTelephonyConfigTspAdaptorSettings tspAdaptorSettingsField;

        private siteInstanceTelephonyConfigMeetingPlace meetingPlaceField;

        private bool supportOtherTypeTeleconfField;

        private string otherTeleServiceNameField;

        private bool supportAdapterlessTSPField;

        private bool displayAttendeeIDField;

        private bool provisionTeleAccountField;

        private bool choosePCNField;

        private bool audioOnlyField;

        private bool configTollAndTollFreeNumField;

        private bool configPrimaryTSField;

        private bool teleCLIAuthEnabledField;

        private bool teleCLIPINEnabledField;

        /// <remarks/>
        public bool isTSPUsingTelephonyAPI
        {
            get
            {
                return this.isTSPUsingTelephonyAPIField;
            }
            set
            {
                this.isTSPUsingTelephonyAPIField = value;
            }
        }

        /// <remarks/>
        public string serviceName
        {
            get
            {
                return this.serviceNameField;
            }
            set
            {
                this.serviceNameField = value;
            }
        }

        /// <remarks/>
        public string participantAccessCodeLabel
        {
            get
            {
                return this.participantAccessCodeLabelField;
            }
            set
            {
                this.participantAccessCodeLabelField = value;
            }
        }

        /// <remarks/>
        public string subscriberAccessCodeLabel
        {
            get
            {
                return this.subscriberAccessCodeLabelField;
            }
            set
            {
                this.subscriberAccessCodeLabelField = value;
            }
        }

        /// <remarks/>
        public string attendeeIDLabel
        {
            get
            {
                return this.attendeeIDLabelField;
            }
            set
            {
                this.attendeeIDLabelField = value;
            }
        }

        /// <remarks/>
        public bool internetPhone
        {
            get
            {
                return this.internetPhoneField;
            }
            set
            {
                this.internetPhoneField = value;
            }
        }

        /// <remarks/>
        public bool supportCallInTypeTeleconf
        {
            get
            {
                return this.supportCallInTypeTeleconfField;
            }
            set
            {
                this.supportCallInTypeTeleconfField = value;
            }
        }

        /// <remarks/>
        public bool callInTeleconferencing
        {
            get
            {
                return this.callInTeleconferencingField;
            }
            set
            {
                this.callInTeleconferencingField = value;
            }
        }

        /// <remarks/>
        public bool tollFreeCallinTeleconferencing
        {
            get
            {
                return this.tollFreeCallinTeleconferencingField;
            }
            set
            {
                this.tollFreeCallinTeleconferencingField = value;
            }
        }

        /// <remarks/>
        public bool intlCallInTeleconferencing
        {
            get
            {
                return this.intlCallInTeleconferencingField;
            }
            set
            {
                this.intlCallInTeleconferencingField = value;
            }
        }

        /// <remarks/>
        public bool callBackTeleconferencing
        {
            get
            {
                return this.callBackTeleconferencingField;
            }
            set
            {
                this.callBackTeleconferencingField = value;
            }
        }

        /// <remarks/>
        public byte callInNumber
        {
            get
            {
                return this.callInNumberField;
            }
            set
            {
                this.callInNumberField = value;
            }
        }

        /// <remarks/>
        public string defaultTeleServerSubject
        {
            get
            {
                return this.defaultTeleServerSubjectField;
            }
            set
            {
                this.defaultTeleServerSubjectField = value;
            }
        }

        /// <remarks/>
        public string subscribeName
        {
            get
            {
                return this.subscribeNameField;
            }
            set
            {
                this.subscribeNameField = value;
            }
        }

        /// <remarks/>
        public string subscribePassword
        {
            get
            {
                return this.subscribePasswordField;
            }
            set
            {
                this.subscribePasswordField = value;
            }
        }

        /// <remarks/>
        public byte defaultPhoneLines
        {
            get
            {
                return this.defaultPhoneLinesField;
            }
            set
            {
                this.defaultPhoneLinesField = value;
            }
        }

        /// <remarks/>
        public byte defaultSpeakingLines
        {
            get
            {
                return this.defaultSpeakingLinesField;
            }
            set
            {
                this.defaultSpeakingLinesField = value;
            }
        }

        /// <remarks/>
        public byte majorCountryCode
        {
            get
            {
                return this.majorCountryCodeField;
            }
            set
            {
                this.majorCountryCodeField = value;
            }
        }

        /// <remarks/>
        public ushort majorAreaCode
        {
            get
            {
                return this.majorAreaCodeField;
            }
            set
            {
                this.majorAreaCodeField = value;
            }
        }

        /// <remarks/>
        public string publicName
        {
            get
            {
                return this.publicNameField;
            }
            set
            {
                this.publicNameField = value;
            }
        }

        /// <remarks/>
        public bool hybridTeleconference
        {
            get
            {
                return this.hybridTeleconferenceField;
            }
            set
            {
                this.hybridTeleconferenceField = value;
            }
        }

        /// <remarks/>
        public bool instantHelp
        {
            get
            {
                return this.instantHelpField;
            }
            set
            {
                this.instantHelpField = value;
            }
        }

        /// <remarks/>
        public bool customerManage
        {
            get
            {
                return this.customerManageField;
            }
            set
            {
                this.customerManageField = value;
            }
        }

        /// <remarks/>
        public byte maxCallersNumber
        {
            get
            {
                return this.maxCallersNumberField;
            }
            set
            {
                this.maxCallersNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("isSpecified")]
        public bool isSpecified1
        {
            get
            {
                return this.isSpecified1Field;
            }
            set
            {
                this.isSpecified1Field = value;
            }
        }

        /// <remarks/>
        public bool isContinue
        {
            get
            {
                return this.isContinueField;
            }
            set
            {
                this.isContinueField = value;
            }
        }

        /// <remarks/>
        public bool intlCallBackTeleconferencing
        {
            get
            {
                return this.intlCallBackTeleconferencingField;
            }
            set
            {
                this.intlCallBackTeleconferencingField = value;
            }
        }

        /// <remarks/>
        public siteInstanceTelephonyConfigPersonalTeleconf personalTeleconf
        {
            get
            {
                return this.personalTeleconfField;
            }
            set
            {
                this.personalTeleconfField = value;
            }
        }

        /// <remarks/>
        public bool multiMediaPlatform
        {
            get
            {
                return this.multiMediaPlatformField;
            }
            set
            {
                this.multiMediaPlatformField = value;
            }
        }

        /// <remarks/>
        public string multiMediaHostName
        {
            get
            {
                return this.multiMediaHostNameField;
            }
            set
            {
                this.multiMediaHostNameField = value;
            }
        }

        /// <remarks/>
        public bool broadcastAudioStream
        {
            get
            {
                return this.broadcastAudioStreamField;
            }
            set
            {
                this.broadcastAudioStreamField = value;
            }
        }

        /// <remarks/>
        public siteInstanceTelephonyConfigTspAdaptorSettings tspAdaptorSettings
        {
            get
            {
                return this.tspAdaptorSettingsField;
            }
            set
            {
                this.tspAdaptorSettingsField = value;
            }
        }

        /// <remarks/>
        public siteInstanceTelephonyConfigMeetingPlace meetingPlace
        {
            get
            {
                return this.meetingPlaceField;
            }
            set
            {
                this.meetingPlaceField = value;
            }
        }

        /// <remarks/>
        public bool supportOtherTypeTeleconf
        {
            get
            {
                return this.supportOtherTypeTeleconfField;
            }
            set
            {
                this.supportOtherTypeTeleconfField = value;
            }
        }

        /// <remarks/>
        public string otherTeleServiceName
        {
            get
            {
                return this.otherTeleServiceNameField;
            }
            set
            {
                this.otherTeleServiceNameField = value;
            }
        }

        /// <remarks/>
        public bool supportAdapterlessTSP
        {
            get
            {
                return this.supportAdapterlessTSPField;
            }
            set
            {
                this.supportAdapterlessTSPField = value;
            }
        }

        /// <remarks/>
        public bool displayAttendeeID
        {
            get
            {
                return this.displayAttendeeIDField;
            }
            set
            {
                this.displayAttendeeIDField = value;
            }
        }

        /// <remarks/>
        public bool provisionTeleAccount
        {
            get
            {
                return this.provisionTeleAccountField;
            }
            set
            {
                this.provisionTeleAccountField = value;
            }
        }

        /// <remarks/>
        public bool choosePCN
        {
            get
            {
                return this.choosePCNField;
            }
            set
            {
                this.choosePCNField = value;
            }
        }

        /// <remarks/>
        public bool audioOnly
        {
            get
            {
                return this.audioOnlyField;
            }
            set
            {
                this.audioOnlyField = value;
            }
        }

        /// <remarks/>
        public bool configTollAndTollFreeNum
        {
            get
            {
                return this.configTollAndTollFreeNumField;
            }
            set
            {
                this.configTollAndTollFreeNumField = value;
            }
        }

        /// <remarks/>
        public bool configPrimaryTS
        {
            get
            {
                return this.configPrimaryTSField;
            }
            set
            {
                this.configPrimaryTSField = value;
            }
        }

        /// <remarks/>
        public bool teleCLIAuthEnabled
        {
            get
            {
                return this.teleCLIAuthEnabledField;
            }
            set
            {
                this.teleCLIAuthEnabledField = value;
            }
        }

        /// <remarks/>
        public bool teleCLIPINEnabled
        {
            get
            {
                return this.teleCLIPINEnabledField;
            }
            set
            {
                this.teleCLIPINEnabledField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTelephonyConfigPersonalTeleconf
    {

        private siteInstanceTelephonyConfigPersonalTeleconfPrimaryLargeServer primaryLargeServerField;

        private siteInstanceTelephonyConfigPersonalTeleconfBackup1LargeServer backup1LargeServerField;

        private siteInstanceTelephonyConfigPersonalTeleconfBackup2LargeServer backup2LargeServerField;

        private siteInstanceTelephonyConfigPersonalTeleconfPrimarySmallServer primarySmallServerField;

        private siteInstanceTelephonyConfigPersonalTeleconfBackup1SmallServer backup1SmallServerField;

        private siteInstanceTelephonyConfigPersonalTeleconfBackup2SmallServer backup2SmallServerField;

        private bool joinBeforeHostField;

        /// <remarks/>
        public siteInstanceTelephonyConfigPersonalTeleconfPrimaryLargeServer primaryLargeServer
        {
            get
            {
                return this.primaryLargeServerField;
            }
            set
            {
                this.primaryLargeServerField = value;
            }
        }

        /// <remarks/>
        public siteInstanceTelephonyConfigPersonalTeleconfBackup1LargeServer backup1LargeServer
        {
            get
            {
                return this.backup1LargeServerField;
            }
            set
            {
                this.backup1LargeServerField = value;
            }
        }

        /// <remarks/>
        public siteInstanceTelephonyConfigPersonalTeleconfBackup2LargeServer backup2LargeServer
        {
            get
            {
                return this.backup2LargeServerField;
            }
            set
            {
                this.backup2LargeServerField = value;
            }
        }

        /// <remarks/>
        public siteInstanceTelephonyConfigPersonalTeleconfPrimarySmallServer primarySmallServer
        {
            get
            {
                return this.primarySmallServerField;
            }
            set
            {
                this.primarySmallServerField = value;
            }
        }

        /// <remarks/>
        public siteInstanceTelephonyConfigPersonalTeleconfBackup1SmallServer backup1SmallServer
        {
            get
            {
                return this.backup1SmallServerField;
            }
            set
            {
                this.backup1SmallServerField = value;
            }
        }

        /// <remarks/>
        public siteInstanceTelephonyConfigPersonalTeleconfBackup2SmallServer backup2SmallServer
        {
            get
            {
                return this.backup2SmallServerField;
            }
            set
            {
                this.backup2SmallServerField = value;
            }
        }

        /// <remarks/>
        public bool joinBeforeHost
        {
            get
            {
                return this.joinBeforeHostField;
            }
            set
            {
                this.joinBeforeHostField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTelephonyConfigPersonalTeleconfPrimaryLargeServer
    {

        private string tollNumField;

        private string tollFreeNumField;

        private globalNum[] globalNumField;

        private bool enableServerField;

        private string tollLabelField;

        private string tollFreeLabelField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollNum
        {
            get
            {
                return this.tollNumField;
            }
            set
            {
                this.tollNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollFreeNum
        {
            get
            {
                return this.tollFreeNumField;
            }
            set
            {
                this.tollFreeNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("globalNum", Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public globalNum[] globalNum
        {
            get
            {
                return this.globalNumField;
            }
            set
            {
                this.globalNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public bool enableServer
        {
            get
            {
                return this.enableServerField;
            }
            set
            {
                this.enableServerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollLabel
        {
            get
            {
                return this.tollLabelField;
            }
            set
            {
                this.tollLabelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollFreeLabel
        {
            get
            {
                return this.tollFreeLabelField;
            }
            set
            {
                this.tollFreeLabelField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service", IsNullable = false)]
    public partial class globalNum
    {

        private string countryAliasField;

        private string phoneNumberField;

        private bool tollFreeField;

        private bool defaultField;

        /// <remarks/>
        public string countryAlias
        {
            get
            {
                return this.countryAliasField;
            }
            set
            {
                this.countryAliasField = value;
            }
        }

        /// <remarks/>
        public string phoneNumber
        {
            get
            {
                return this.phoneNumberField;
            }
            set
            {
                this.phoneNumberField = value;
            }
        }

        /// <remarks/>
        public bool tollFree
        {
            get
            {
                return this.tollFreeField;
            }
            set
            {
                this.tollFreeField = value;
            }
        }

        /// <remarks/>
        public bool @default
        {
            get
            {
                return this.defaultField;
            }
            set
            {
                this.defaultField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTelephonyConfigPersonalTeleconfBackup1LargeServer
    {

        private string tollNumField;

        private string tollFreeNumField;

        private globalNum[] globalNumField;

        private bool enableServerField;

        private string tollLabelField;

        private string tollFreeLabelField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollNum
        {
            get
            {
                return this.tollNumField;
            }
            set
            {
                this.tollNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollFreeNum
        {
            get
            {
                return this.tollFreeNumField;
            }
            set
            {
                this.tollFreeNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("globalNum", Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public globalNum[] globalNum
        {
            get
            {
                return this.globalNumField;
            }
            set
            {
                this.globalNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public bool enableServer
        {
            get
            {
                return this.enableServerField;
            }
            set
            {
                this.enableServerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollLabel
        {
            get
            {
                return this.tollLabelField;
            }
            set
            {
                this.tollLabelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollFreeLabel
        {
            get
            {
                return this.tollFreeLabelField;
            }
            set
            {
                this.tollFreeLabelField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTelephonyConfigPersonalTeleconfBackup2LargeServer
    {

        private string tollNumField;

        private string tollFreeNumField;

        private globalNum[] globalNumField;

        private bool enableServerField;

        private string tollLabelField;

        private string tollFreeLabelField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollNum
        {
            get
            {
                return this.tollNumField;
            }
            set
            {
                this.tollNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollFreeNum
        {
            get
            {
                return this.tollFreeNumField;
            }
            set
            {
                this.tollFreeNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("globalNum", Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public globalNum[] globalNum
        {
            get
            {
                return this.globalNumField;
            }
            set
            {
                this.globalNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public bool enableServer
        {
            get
            {
                return this.enableServerField;
            }
            set
            {
                this.enableServerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollLabel
        {
            get
            {
                return this.tollLabelField;
            }
            set
            {
                this.tollLabelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollFreeLabel
        {
            get
            {
                return this.tollFreeLabelField;
            }
            set
            {
                this.tollFreeLabelField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTelephonyConfigPersonalTeleconfPrimarySmallServer
    {

        private string tollNumField;

        private string tollFreeNumField;

        private globalNum[] globalNumField;

        private bool enableServerField;

        private string tollLabelField;

        private string tollFreeLabelField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollNum
        {
            get
            {
                return this.tollNumField;
            }
            set
            {
                this.tollNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollFreeNum
        {
            get
            {
                return this.tollFreeNumField;
            }
            set
            {
                this.tollFreeNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("globalNum", Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public globalNum[] globalNum
        {
            get
            {
                return this.globalNumField;
            }
            set
            {
                this.globalNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public bool enableServer
        {
            get
            {
                return this.enableServerField;
            }
            set
            {
                this.enableServerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollLabel
        {
            get
            {
                return this.tollLabelField;
            }
            set
            {
                this.tollLabelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollFreeLabel
        {
            get
            {
                return this.tollFreeLabelField;
            }
            set
            {
                this.tollFreeLabelField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTelephonyConfigPersonalTeleconfBackup1SmallServer
    {

        private string tollNumField;

        private string tollFreeNumField;

        private globalNum[] globalNumField;

        private bool enableServerField;

        private string tollLabelField;

        private string tollFreeLabelField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollNum
        {
            get
            {
                return this.tollNumField;
            }
            set
            {
                this.tollNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollFreeNum
        {
            get
            {
                return this.tollFreeNumField;
            }
            set
            {
                this.tollFreeNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("globalNum", Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public globalNum[] globalNum
        {
            get
            {
                return this.globalNumField;
            }
            set
            {
                this.globalNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public bool enableServer
        {
            get
            {
                return this.enableServerField;
            }
            set
            {
                this.enableServerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollLabel
        {
            get
            {
                return this.tollLabelField;
            }
            set
            {
                this.tollLabelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollFreeLabel
        {
            get
            {
                return this.tollFreeLabelField;
            }
            set
            {
                this.tollFreeLabelField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTelephonyConfigPersonalTeleconfBackup2SmallServer
    {

        private string tollNumField;

        private string tollFreeNumField;

        private globalNum[] globalNumField;

        private bool enableServerField;

        private string tollLabelField;

        private string tollFreeLabelField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollNum
        {
            get
            {
                return this.tollNumField;
            }
            set
            {
                this.tollNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollFreeNum
        {
            get
            {
                return this.tollFreeNumField;
            }
            set
            {
                this.tollFreeNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("globalNum", Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public globalNum[] globalNum
        {
            get
            {
                return this.globalNumField;
            }
            set
            {
                this.globalNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public bool enableServer
        {
            get
            {
                return this.enableServerField;
            }
            set
            {
                this.enableServerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollLabel
        {
            get
            {
                return this.tollLabelField;
            }
            set
            {
                this.tollLabelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.webex.com/schemas/2002/06/service")]
        public string tollFreeLabel
        {
            get
            {
                return this.tollFreeLabelField;
            }
            set
            {
                this.tollFreeLabelField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTelephonyConfigTspAdaptorSettings
    {

        private siteInstanceTelephonyConfigTspAdaptorSettingsPrimaryLarge primaryLargeField;

        private siteInstanceTelephonyConfigTspAdaptorSettingsBackup1Large backup1LargeField;

        private siteInstanceTelephonyConfigTspAdaptorSettingsBackup2Large backup2LargeField;

        /// <remarks/>
        public siteInstanceTelephonyConfigTspAdaptorSettingsPrimaryLarge primaryLarge
        {
            get
            {
                return this.primaryLargeField;
            }
            set
            {
                this.primaryLargeField = value;
            }
        }

        /// <remarks/>
        public siteInstanceTelephonyConfigTspAdaptorSettingsBackup1Large backup1Large
        {
            get
            {
                return this.backup1LargeField;
            }
            set
            {
                this.backup1LargeField = value;
            }
        }

        /// <remarks/>
        public siteInstanceTelephonyConfigTspAdaptorSettingsBackup2Large backup2Large
        {
            get
            {
                return this.backup2LargeField;
            }
            set
            {
                this.backup2LargeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTelephonyConfigTspAdaptorSettingsPrimaryLarge
    {

        private bool enableAdaptorField;

        private object serverIPField;

        private siteInstanceTelephonyConfigTspAdaptorSettingsPrimaryLargeMpAudio[] mpAudioField;

        /// <remarks/>
        public bool enableAdaptor
        {
            get
            {
                return this.enableAdaptorField;
            }
            set
            {
                this.enableAdaptorField = value;
            }
        }

        /// <remarks/>
        public object serverIP
        {
            get
            {
                return this.serverIPField;
            }
            set
            {
                this.serverIPField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("mpAudio")]
        public siteInstanceTelephonyConfigTspAdaptorSettingsPrimaryLargeMpAudio[] mpAudio
        {
            get
            {
                return this.mpAudioField;
            }
            set
            {
                this.mpAudioField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTelephonyConfigTspAdaptorSettingsPrimaryLargeMpAudio
    {

        private string labelField;

        /// <remarks/>
        public string label
        {
            get
            {
                return this.labelField;
            }
            set
            {
                this.labelField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTelephonyConfigTspAdaptorSettingsBackup1Large
    {

        private bool enableAdaptorField;

        private object serverIPField;

        private siteInstanceTelephonyConfigTspAdaptorSettingsBackup1LargeMpAudio[] mpAudioField;

        /// <remarks/>
        public bool enableAdaptor
        {
            get
            {
                return this.enableAdaptorField;
            }
            set
            {
                this.enableAdaptorField = value;
            }
        }

        /// <remarks/>
        public object serverIP
        {
            get
            {
                return this.serverIPField;
            }
            set
            {
                this.serverIPField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("mpAudio")]
        public siteInstanceTelephonyConfigTspAdaptorSettingsBackup1LargeMpAudio[] mpAudio
        {
            get
            {
                return this.mpAudioField;
            }
            set
            {
                this.mpAudioField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTelephonyConfigTspAdaptorSettingsBackup1LargeMpAudio
    {

        private string labelField;

        /// <remarks/>
        public string label
        {
            get
            {
                return this.labelField;
            }
            set
            {
                this.labelField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTelephonyConfigTspAdaptorSettingsBackup2Large
    {

        private bool enableAdaptorField;

        private object serverIPField;

        private siteInstanceTelephonyConfigTspAdaptorSettingsBackup2LargeMpAudio[] mpAudioField;

        /// <remarks/>
        public bool enableAdaptor
        {
            get
            {
                return this.enableAdaptorField;
            }
            set
            {
                this.enableAdaptorField = value;
            }
        }

        /// <remarks/>
        public object serverIP
        {
            get
            {
                return this.serverIPField;
            }
            set
            {
                this.serverIPField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("mpAudio")]
        public siteInstanceTelephonyConfigTspAdaptorSettingsBackup2LargeMpAudio[] mpAudio
        {
            get
            {
                return this.mpAudioField;
            }
            set
            {
                this.mpAudioField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTelephonyConfigTspAdaptorSettingsBackup2LargeMpAudio
    {

        private string labelField;

        /// <remarks/>
        public string label
        {
            get
            {
                return this.labelField;
            }
            set
            {
                this.labelField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTelephonyConfigMeetingPlace
    {

        private bool persistentTSPField;

        private string mpAudioConferencingField;

        /// <remarks/>
        public bool persistentTSP
        {
            get
            {
                return this.persistentTSPField;
            }
            set
            {
                this.persistentTSPField = value;
            }
        }

        /// <remarks/>
        public string mpAudioConferencing
        {
            get
            {
                return this.mpAudioConferencingField;
            }
            set
            {
                this.mpAudioConferencingField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceCommerceAndReporting
    {

        private bool trackingCodeField;

        private bool siteAdminReportField;

        private bool subScriptionServiceField;

        private bool isECommmerceField;

        private bool customereCommerceField;

        private bool isLocalTaxField;

        private string localTaxNameField;

        private decimal localTaxtRateField;

        private byte holReportField;

        /// <remarks/>
        public bool trackingCode
        {
            get
            {
                return this.trackingCodeField;
            }
            set
            {
                this.trackingCodeField = value;
            }
        }

        /// <remarks/>
        public bool siteAdminReport
        {
            get
            {
                return this.siteAdminReportField;
            }
            set
            {
                this.siteAdminReportField = value;
            }
        }

        /// <remarks/>
        public bool subScriptionService
        {
            get
            {
                return this.subScriptionServiceField;
            }
            set
            {
                this.subScriptionServiceField = value;
            }
        }

        /// <remarks/>
        public bool isECommmerce
        {
            get
            {
                return this.isECommmerceField;
            }
            set
            {
                this.isECommmerceField = value;
            }
        }

        /// <remarks/>
        public bool customereCommerce
        {
            get
            {
                return this.customereCommerceField;
            }
            set
            {
                this.customereCommerceField = value;
            }
        }

        /// <remarks/>
        public bool isLocalTax
        {
            get
            {
                return this.isLocalTaxField;
            }
            set
            {
                this.isLocalTaxField = value;
            }
        }

        /// <remarks/>
        public string localTaxName
        {
            get
            {
                return this.localTaxNameField;
            }
            set
            {
                this.localTaxNameField = value;
            }
        }

        /// <remarks/>
        public decimal localTaxtRate
        {
            get
            {
                return this.localTaxtRateField;
            }
            set
            {
                this.localTaxtRateField = value;
            }
        }

        /// <remarks/>
        public byte holReport
        {
            get
            {
                return this.holReportField;
            }
            set
            {
                this.holReportField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTools
    {

        private bool businessDirectoryField;

        private bool officeCalendarField;

        private bool meetingCalendarField;

        private bool displayOnCallAssistLinkField;

        private bool displayProfileLinkField;

        private bool recordingAndPlaybackField;

        private bool recordingEditorField;

        private bool publishRecordingsField;

        private bool instantMeetingField;

        private bool emailsField;

        private bool outlookIntegrationField;

        private bool wirelessAccessField;

        private bool allowPublicAccessField;

        private bool sslField;

        private bool handsOnLabField;

        private uint holMaxLabsField;

        private uint holMaxComputersField;

        private bool userLockDownField;

        private bool meetingAssistField;

        private bool smsField;

        private string encryptionField;

        private bool internalMeetingField;

        private bool enableTPField;

        private bool enableTPplusField;

        /// <remarks/>
        public bool businessDirectory
        {
            get
            {
                return this.businessDirectoryField;
            }
            set
            {
                this.businessDirectoryField = value;
            }
        }

        /// <remarks/>
        public bool officeCalendar
        {
            get
            {
                return this.officeCalendarField;
            }
            set
            {
                this.officeCalendarField = value;
            }
        }

        /// <remarks/>
        public bool meetingCalendar
        {
            get
            {
                return this.meetingCalendarField;
            }
            set
            {
                this.meetingCalendarField = value;
            }
        }

        /// <remarks/>
        public bool displayOnCallAssistLink
        {
            get
            {
                return this.displayOnCallAssistLinkField;
            }
            set
            {
                this.displayOnCallAssistLinkField = value;
            }
        }

        /// <remarks/>
        public bool displayProfileLink
        {
            get
            {
                return this.displayProfileLinkField;
            }
            set
            {
                this.displayProfileLinkField = value;
            }
        }

        /// <remarks/>
        public bool recordingAndPlayback
        {
            get
            {
                return this.recordingAndPlaybackField;
            }
            set
            {
                this.recordingAndPlaybackField = value;
            }
        }

        /// <remarks/>
        public bool recordingEditor
        {
            get
            {
                return this.recordingEditorField;
            }
            set
            {
                this.recordingEditorField = value;
            }
        }

        /// <remarks/>
        public bool publishRecordings
        {
            get
            {
                return this.publishRecordingsField;
            }
            set
            {
                this.publishRecordingsField = value;
            }
        }

        /// <remarks/>
        public bool instantMeeting
        {
            get
            {
                return this.instantMeetingField;
            }
            set
            {
                this.instantMeetingField = value;
            }
        }

        /// <remarks/>
        public bool emails
        {
            get
            {
                return this.emailsField;
            }
            set
            {
                this.emailsField = value;
            }
        }

        /// <remarks/>
        public bool outlookIntegration
        {
            get
            {
                return this.outlookIntegrationField;
            }
            set
            {
                this.outlookIntegrationField = value;
            }
        }

        /// <remarks/>
        public bool wirelessAccess
        {
            get
            {
                return this.wirelessAccessField;
            }
            set
            {
                this.wirelessAccessField = value;
            }
        }

        /// <remarks/>
        public bool allowPublicAccess
        {
            get
            {
                return this.allowPublicAccessField;
            }
            set
            {
                this.allowPublicAccessField = value;
            }
        }

        /// <remarks/>
        public bool ssl
        {
            get
            {
                return this.sslField;
            }
            set
            {
                this.sslField = value;
            }
        }

        /// <remarks/>
        public bool handsOnLab
        {
            get
            {
                return this.handsOnLabField;
            }
            set
            {
                this.handsOnLabField = value;
            }
        }

        /// <remarks/>
        public uint holMaxLabs
        {
            get
            {
                return this.holMaxLabsField;
            }
            set
            {
                this.holMaxLabsField = value;
            }
        }

        /// <remarks/>
        public uint holMaxComputers
        {
            get
            {
                return this.holMaxComputersField;
            }
            set
            {
                this.holMaxComputersField = value;
            }
        }

        /// <remarks/>
        public bool userLockDown
        {
            get
            {
                return this.userLockDownField;
            }
            set
            {
                this.userLockDownField = value;
            }
        }

        /// <remarks/>
        public bool meetingAssist
        {
            get
            {
                return this.meetingAssistField;
            }
            set
            {
                this.meetingAssistField = value;
            }
        }

        /// <remarks/>
        public bool sms
        {
            get
            {
                return this.smsField;
            }
            set
            {
                this.smsField = value;
            }
        }

        /// <remarks/>
        public string encryption
        {
            get
            {
                return this.encryptionField;
            }
            set
            {
                this.encryptionField = value;
            }
        }

        /// <remarks/>
        public bool internalMeeting
        {
            get
            {
                return this.internalMeetingField;
            }
            set
            {
                this.internalMeetingField = value;
            }
        }

        /// <remarks/>
        public bool enableTP
        {
            get
            {
                return this.enableTPField;
            }
            set
            {
                this.enableTPField = value;
            }
        }

        /// <remarks/>
        public bool enableTPplus
        {
            get
            {
                return this.enableTPplusField;
            }
            set
            {
                this.enableTPplusField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceCustCommunications
    {

        private siteInstanceCustCommunicationsDisplayType displayTypeField;

        private siteInstanceCustCommunicationsDisplayMethod displayMethodField;

        /// <remarks/>
        public siteInstanceCustCommunicationsDisplayType displayType
        {
            get
            {
                return this.displayTypeField;
            }
            set
            {
                this.displayTypeField = value;
            }
        }

        /// <remarks/>
        public siteInstanceCustCommunicationsDisplayMethod displayMethod
        {
            get
            {
                return this.displayMethodField;
            }
            set
            {
                this.displayMethodField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceCustCommunicationsDisplayType
    {

        private bool prodSvcAnnounceField;

        private bool trainingInfoField;

        private bool eNewslettersField;

        private bool promotionsOffersField;

        private bool pressReleasesField;

        /// <remarks/>
        public bool prodSvcAnnounce
        {
            get
            {
                return this.prodSvcAnnounceField;
            }
            set
            {
                this.prodSvcAnnounceField = value;
            }
        }

        /// <remarks/>
        public bool trainingInfo
        {
            get
            {
                return this.trainingInfoField;
            }
            set
            {
                this.trainingInfoField = value;
            }
        }

        /// <remarks/>
        public bool eNewsletters
        {
            get
            {
                return this.eNewslettersField;
            }
            set
            {
                this.eNewslettersField = value;
            }
        }

        /// <remarks/>
        public bool promotionsOffers
        {
            get
            {
                return this.promotionsOffersField;
            }
            set
            {
                this.promotionsOffersField = value;
            }
        }

        /// <remarks/>
        public bool pressReleases
        {
            get
            {
                return this.pressReleasesField;
            }
            set
            {
                this.pressReleasesField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceCustCommunicationsDisplayMethod
    {

        private bool emailField;

        private bool faxField;

        private bool phoneField;

        private bool mailField;

        /// <remarks/>
        public bool email
        {
            get
            {
                return this.emailField;
            }
            set
            {
                this.emailField = value;
            }
        }

        /// <remarks/>
        public bool fax
        {
            get
            {
                return this.faxField;
            }
            set
            {
                this.faxField = value;
            }
        }

        /// <remarks/>
        public bool phone
        {
            get
            {
                return this.phoneField;
            }
            set
            {
                this.phoneField = value;
            }
        }

        /// <remarks/>
        public bool mail
        {
            get
            {
                return this.mailField;
            }
            set
            {
                this.mailField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTrackingCode
    {

        private byte indexField;

        private string nameField;

        private string inputModeField;

        private string hostProfileField;

        private siteInstanceTrackingCodeSchedulingPage[] schedulingPageField;

        private siteInstanceTrackingCodeListValue[] listValueField;

        /// <remarks/>
        public byte index
        {
            get
            {
                return this.indexField;
            }
            set
            {
                this.indexField = value;
            }
        }

        /// <remarks/>
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public string inputMode
        {
            get
            {
                return this.inputModeField;
            }
            set
            {
                this.inputModeField = value;
            }
        }

        /// <remarks/>
        public string hostProfile
        {
            get
            {
                return this.hostProfileField;
            }
            set
            {
                this.hostProfileField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("schedulingPage")]
        public siteInstanceTrackingCodeSchedulingPage[] schedulingPage
        {
            get
            {
                return this.schedulingPageField;
            }
            set
            {
                this.schedulingPageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("listValue")]
        public siteInstanceTrackingCodeListValue[] listValue
        {
            get
            {
                return this.listValueField;
            }
            set
            {
                this.listValueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTrackingCodeSchedulingPage
    {

        private string serviceField;

        private string schedulingField;

        /// <remarks/>
        public string service
        {
            get
            {
                return this.serviceField;
            }
            set
            {
                this.serviceField = value;
            }
        }

        /// <remarks/>
        public string scheduling
        {
            get
            {
                return this.schedulingField;
            }
            set
            {
                this.schedulingField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceTrackingCodeListValue
    {

        private byte indexField;

        private string valueField;

        private bool activeField;

        /// <remarks/>
        public byte index
        {
            get
            {
                return this.indexField;
            }
            set
            {
                this.indexField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }

        /// <remarks/>
        public bool active
        {
            get
            {
                return this.activeField;
            }
            set
            {
                this.activeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceSupportedServices
    {

        private siteInstanceSupportedServicesMeetingCenter meetingCenterField;

        private siteInstanceSupportedServicesTrainingCenter trainingCenterField;

        private siteInstanceSupportedServicesSupportCenter supportCenterField;

        private siteInstanceSupportedServicesEventCenter eventCenterField;

        private siteInstanceSupportedServicesSalesCenter salesCenterField;

        /// <remarks/>
        public siteInstanceSupportedServicesMeetingCenter meetingCenter
        {
            get
            {
                return this.meetingCenterField;
            }
            set
            {
                this.meetingCenterField = value;
            }
        }

        /// <remarks/>
        public siteInstanceSupportedServicesTrainingCenter trainingCenter
        {
            get
            {
                return this.trainingCenterField;
            }
            set
            {
                this.trainingCenterField = value;
            }
        }

        /// <remarks/>
        public siteInstanceSupportedServicesSupportCenter supportCenter
        {
            get
            {
                return this.supportCenterField;
            }
            set
            {
                this.supportCenterField = value;
            }
        }

        /// <remarks/>
        public siteInstanceSupportedServicesEventCenter eventCenter
        {
            get
            {
                return this.eventCenterField;
            }
            set
            {
                this.eventCenterField = value;
            }
        }

        /// <remarks/>
        public siteInstanceSupportedServicesSalesCenter salesCenter
        {
            get
            {
                return this.salesCenterField;
            }
            set
            {
                this.salesCenterField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceSupportedServicesMeetingCenter
    {

        private bool enabledField;

        private string pageVersionField;

        private string clientVersionField;

        /// <remarks/>
        public bool enabled
        {
            get
            {
                return this.enabledField;
            }
            set
            {
                this.enabledField = value;
            }
        }

        /// <remarks/>
        public string pageVersion
        {
            get
            {
                return this.pageVersionField;
            }
            set
            {
                this.pageVersionField = value;
            }
        }

        /// <remarks/>
        public string clientVersion
        {
            get
            {
                return this.clientVersionField;
            }
            set
            {
                this.clientVersionField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceSupportedServicesTrainingCenter
    {

        private bool enabledField;

        private string pageVersionField;

        private string clientVersionField;

        /// <remarks/>
        public bool enabled
        {
            get
            {
                return this.enabledField;
            }
            set
            {
                this.enabledField = value;
            }
        }

        /// <remarks/>
        public string pageVersion
        {
            get
            {
                return this.pageVersionField;
            }
            set
            {
                this.pageVersionField = value;
            }
        }

        /// <remarks/>
        public string clientVersion
        {
            get
            {
                return this.clientVersionField;
            }
            set
            {
                this.clientVersionField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceSupportedServicesSupportCenter
    {

        private bool enabledField;

        private string pageVersionField;

        private string clientVersionField;

        private bool webACDField;

        /// <remarks/>
        public bool enabled
        {
            get
            {
                return this.enabledField;
            }
            set
            {
                this.enabledField = value;
            }
        }

        /// <remarks/>
        public string pageVersion
        {
            get
            {
                return this.pageVersionField;
            }
            set
            {
                this.pageVersionField = value;
            }
        }

        /// <remarks/>
        public string clientVersion
        {
            get
            {
                return this.clientVersionField;
            }
            set
            {
                this.clientVersionField = value;
            }
        }

        /// <remarks/>
        public bool webACD
        {
            get
            {
                return this.webACDField;
            }
            set
            {
                this.webACDField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceSupportedServicesEventCenter
    {

        private bool enabledField;

        private string pageVersionField;

        private string clientVersionField;

        private bool marketingAddOnField;

        private bool optimizeAttendeeBandwidthUsageField;

        /// <remarks/>
        public bool enabled
        {
            get
            {
                return this.enabledField;
            }
            set
            {
                this.enabledField = value;
            }
        }

        /// <remarks/>
        public string pageVersion
        {
            get
            {
                return this.pageVersionField;
            }
            set
            {
                this.pageVersionField = value;
            }
        }

        /// <remarks/>
        public string clientVersion
        {
            get
            {
                return this.clientVersionField;
            }
            set
            {
                this.clientVersionField = value;
            }
        }

        /// <remarks/>
        public bool marketingAddOn
        {
            get
            {
                return this.marketingAddOnField;
            }
            set
            {
                this.marketingAddOnField = value;
            }
        }

        /// <remarks/>
        public bool optimizeAttendeeBandwidthUsage
        {
            get
            {
                return this.optimizeAttendeeBandwidthUsageField;
            }
            set
            {
                this.optimizeAttendeeBandwidthUsageField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceSupportedServicesSalesCenter
    {

        private bool enabledField;

        private string pageVersionField;

        private string clientVersionField;

        /// <remarks/>
        public bool enabled
        {
            get
            {
                return this.enabledField;
            }
            set
            {
                this.enabledField = value;
            }
        }

        /// <remarks/>
        public string pageVersion
        {
            get
            {
                return this.pageVersionField;
            }
            set
            {
                this.pageVersionField = value;
            }
        }

        /// <remarks/>
        public string clientVersion
        {
            get
            {
                return this.clientVersionField;
            }
            set
            {
                this.clientVersionField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceSecurityOptions
    {

        private bool passwordExpiresField;

        private byte passwordLifetimeField;

        private bool allMeetingsUnlistedField;

        private bool allMeetingsPasswordField;

        private bool joinBeforeHostField;

        private bool audioBeforeHostField;

        private bool audioBeforeHostECField;

        private bool audioBeforeHostTCField;

        private bool changePersonalURLField;

        private bool changeUserNameField;

        private siteInstanceSecurityOptionsMeetings meetingsField;

        private bool strictUserPasswordField;

        private bool accountNotifyField;

        private bool requireLoginBeforeSiteAccessField;

        private bool changePWDWhenAutoLoginField;

        private bool enforceBaselineField;

        private bool passwordChangeIntervalOptField;

        private byte passwordChangeIntervalField;

        private bool firstAttendeeAsPresenterField;

        private bool isEnableUUIDLinkField;

        private bool isEnableUUIDLinkForSACField;

        private bool enforceRecordingPwdForMCField;

        private bool enforceRecordingPwdForECField;

        private bool enforceRecordingPwdForTCField;

        private bool enforceRecordingPwdForMiscField;

        /// <remarks/>
        public bool passwordExpires
        {
            get
            {
                return this.passwordExpiresField;
            }
            set
            {
                this.passwordExpiresField = value;
            }
        }

        /// <remarks/>
        public byte passwordLifetime
        {
            get
            {
                return this.passwordLifetimeField;
            }
            set
            {
                this.passwordLifetimeField = value;
            }
        }

        /// <remarks/>
        public bool allMeetingsUnlisted
        {
            get
            {
                return this.allMeetingsUnlistedField;
            }
            set
            {
                this.allMeetingsUnlistedField = value;
            }
        }

        /// <remarks/>
        public bool allMeetingsPassword
        {
            get
            {
                return this.allMeetingsPasswordField;
            }
            set
            {
                this.allMeetingsPasswordField = value;
            }
        }

        /// <remarks/>
        public bool joinBeforeHost
        {
            get
            {
                return this.joinBeforeHostField;
            }
            set
            {
                this.joinBeforeHostField = value;
            }
        }

        /// <remarks/>
        public bool audioBeforeHost
        {
            get
            {
                return this.audioBeforeHostField;
            }
            set
            {
                this.audioBeforeHostField = value;
            }
        }

        /// <remarks/>
        public bool audioBeforeHostEC
        {
            get
            {
                return this.audioBeforeHostECField;
            }
            set
            {
                this.audioBeforeHostECField = value;
            }
        }

        /// <remarks/>
        public bool audioBeforeHostTC
        {
            get
            {
                return this.audioBeforeHostTCField;
            }
            set
            {
                this.audioBeforeHostTCField = value;
            }
        }

        /// <remarks/>
        public bool changePersonalURL
        {
            get
            {
                return this.changePersonalURLField;
            }
            set
            {
                this.changePersonalURLField = value;
            }
        }

        /// <remarks/>
        public bool changeUserName
        {
            get
            {
                return this.changeUserNameField;
            }
            set
            {
                this.changeUserNameField = value;
            }
        }

        /// <remarks/>
        public siteInstanceSecurityOptionsMeetings meetings
        {
            get
            {
                return this.meetingsField;
            }
            set
            {
                this.meetingsField = value;
            }
        }

        /// <remarks/>
        public bool strictUserPassword
        {
            get
            {
                return this.strictUserPasswordField;
            }
            set
            {
                this.strictUserPasswordField = value;
            }
        }

        /// <remarks/>
        public bool accountNotify
        {
            get
            {
                return this.accountNotifyField;
            }
            set
            {
                this.accountNotifyField = value;
            }
        }

        /// <remarks/>
        public bool requireLoginBeforeSiteAccess
        {
            get
            {
                return this.requireLoginBeforeSiteAccessField;
            }
            set
            {
                this.requireLoginBeforeSiteAccessField = value;
            }
        }

        /// <remarks/>
        public bool changePWDWhenAutoLogin
        {
            get
            {
                return this.changePWDWhenAutoLoginField;
            }
            set
            {
                this.changePWDWhenAutoLoginField = value;
            }
        }

        /// <remarks/>
        public bool enforceBaseline
        {
            get
            {
                return this.enforceBaselineField;
            }
            set
            {
                this.enforceBaselineField = value;
            }
        }

        /// <remarks/>
        public bool passwordChangeIntervalOpt
        {
            get
            {
                return this.passwordChangeIntervalOptField;
            }
            set
            {
                this.passwordChangeIntervalOptField = value;
            }
        }

        /// <remarks/>
        public byte passwordChangeInterval
        {
            get
            {
                return this.passwordChangeIntervalField;
            }
            set
            {
                this.passwordChangeIntervalField = value;
            }
        }

        /// <remarks/>
        public bool firstAttendeeAsPresenter
        {
            get
            {
                return this.firstAttendeeAsPresenterField;
            }
            set
            {
                this.firstAttendeeAsPresenterField = value;
            }
        }

        /// <remarks/>
        public bool isEnableUUIDLink
        {
            get
            {
                return this.isEnableUUIDLinkField;
            }
            set
            {
                this.isEnableUUIDLinkField = value;
            }
        }

        /// <remarks/>
        public bool isEnableUUIDLinkForSAC
        {
            get
            {
                return this.isEnableUUIDLinkForSACField;
            }
            set
            {
                this.isEnableUUIDLinkForSACField = value;
            }
        }

        /// <remarks/>
        public bool enforceRecordingPwdForMC
        {
            get
            {
                return this.enforceRecordingPwdForMCField;
            }
            set
            {
                this.enforceRecordingPwdForMCField = value;
            }
        }

        /// <remarks/>
        public bool enforceRecordingPwdForEC
        {
            get
            {
                return this.enforceRecordingPwdForECField;
            }
            set
            {
                this.enforceRecordingPwdForECField = value;
            }
        }

        /// <remarks/>
        public bool enforceRecordingPwdForTC
        {
            get
            {
                return this.enforceRecordingPwdForTCField;
            }
            set
            {
                this.enforceRecordingPwdForTCField = value;
            }
        }

        /// <remarks/>
        public bool enforceRecordingPwdForMisc
        {
            get
            {
                return this.enforceRecordingPwdForMiscField;
            }
            set
            {
                this.enforceRecordingPwdForMiscField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceSecurityOptionsMeetings
    {

        private bool strictPasswordsField;

        /// <remarks/>
        public bool strictPasswords
        {
            get
            {
                return this.strictPasswordsField;
            }
            set
            {
                this.strictPasswordsField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceDefaults
    {

        private bool emailRemindersField;

        private string entryExitToneField;

        private bool voipField;

        private siteInstanceDefaultsTeleconference teleconferenceField;

        private bool joinTeleconfNotPress1Field;

        private bool updateTSPAccountField;

        /// <remarks/>
        public bool emailReminders
        {
            get
            {
                return this.emailRemindersField;
            }
            set
            {
                this.emailRemindersField = value;
            }
        }

        /// <remarks/>
        public string entryExitTone
        {
            get
            {
                return this.entryExitToneField;
            }
            set
            {
                this.entryExitToneField = value;
            }
        }

        /// <remarks/>
        public bool voip
        {
            get
            {
                return this.voipField;
            }
            set
            {
                this.voipField = value;
            }
        }

        /// <remarks/>
        public siteInstanceDefaultsTeleconference teleconference
        {
            get
            {
                return this.teleconferenceField;
            }
            set
            {
                this.teleconferenceField = value;
            }
        }

        /// <remarks/>
        public bool joinTeleconfNotPress1
        {
            get
            {
                return this.joinTeleconfNotPress1Field;
            }
            set
            {
                this.joinTeleconfNotPress1Field = value;
            }
        }

        /// <remarks/>
        public bool updateTSPAccount
        {
            get
            {
                return this.updateTSPAccountField;
            }
            set
            {
                this.updateTSPAccountField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceDefaultsTeleconference
    {

        private string telephonySupportField;

        private bool tollFreeField;

        private bool intlLocalCallInField;

        /// <remarks/>
        public string telephonySupport
        {
            get
            {
                return this.telephonySupportField;
            }
            set
            {
                this.telephonySupportField = value;
            }
        }

        /// <remarks/>
        public bool tollFree
        {
            get
            {
                return this.tollFreeField;
            }
            set
            {
                this.tollFreeField = value;
            }
        }

        /// <remarks/>
        public bool intlLocalCallIn
        {
            get
            {
                return this.intlLocalCallInField;
            }
            set
            {
                this.intlLocalCallInField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceScheduleMeetingOptions
    {

        private bool scheduleOnBehalfField;

        private bool saveSessionTemplateField;

        /// <remarks/>
        public bool scheduleOnBehalf
        {
            get
            {
                return this.scheduleOnBehalfField;
            }
            set
            {
                this.scheduleOnBehalfField = value;
            }
        }

        /// <remarks/>
        public bool saveSessionTemplate
        {
            get
            {
                return this.saveSessionTemplateField;
            }
            set
            {
                this.saveSessionTemplateField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceNavBarTop
    {

        private siteInstanceNavBarTopButton[] buttonField;

        private bool displayDisabledServiceField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("button")]
        public siteInstanceNavBarTopButton[] button
        {
            get
            {
                return this.buttonField;
            }
            set
            {
                this.buttonField = value;
            }
        }

        /// <remarks/>
        public bool displayDisabledService
        {
            get
            {
                return this.displayDisabledServiceField;
            }
            set
            {
                this.displayDisabledServiceField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceNavBarTopButton
    {

        private byte orderField;

        private bool enabledField;

        private bool enabledFieldSpecified;

        private string serviceNameField;

        /// <remarks/>
        public byte order
        {
            get
            {
                return this.orderField;
            }
            set
            {
                this.orderField = value;
            }
        }

        /// <remarks/>
        public bool enabled
        {
            get
            {
                return this.enabledField;
            }
            set
            {
                this.enabledField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool enabledSpecified
        {
            get
            {
                return this.enabledFieldSpecified;
            }
            set
            {
                this.enabledFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string serviceName
        {
            get
            {
                return this.serviceNameField;
            }
            set
            {
                this.serviceNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceNavMyWebEx
    {

        private siteInstanceNavMyWebExCustomLink[] customLinksField;

        private siteInstanceNavMyWebExPartnerLink[] partnerLinksField;

        private bool partnerIntegrationField;

        private siteInstanceNavMyWebExSupport supportField;

        private siteInstanceNavMyWebExTraining trainingField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("customLink", IsNullable = false)]
        public siteInstanceNavMyWebExCustomLink[] customLinks
        {
            get
            {
                return this.customLinksField;
            }
            set
            {
                this.customLinksField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("partnerLink", IsNullable = false)]
        public siteInstanceNavMyWebExPartnerLink[] partnerLinks
        {
            get
            {
                return this.partnerLinksField;
            }
            set
            {
                this.partnerLinksField = value;
            }
        }

        /// <remarks/>
        public bool partnerIntegration
        {
            get
            {
                return this.partnerIntegrationField;
            }
            set
            {
                this.partnerIntegrationField = value;
            }
        }

        /// <remarks/>
        public siteInstanceNavMyWebExSupport support
        {
            get
            {
                return this.supportField;
            }
            set
            {
                this.supportField = value;
            }
        }

        /// <remarks/>
        public siteInstanceNavMyWebExTraining training
        {
            get
            {
                return this.trainingField;
            }
            set
            {
                this.trainingField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceNavMyWebExCustomLink
    {

        private string targetField;

        /// <remarks/>
        public string target
        {
            get
            {
                return this.targetField;
            }
            set
            {
                this.targetField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceNavMyWebExPartnerLink
    {

        private string targetField;

        /// <remarks/>
        public string target
        {
            get
            {
                return this.targetField;
            }
            set
            {
                this.targetField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceNavMyWebExSupport
    {

        private string targetField;

        /// <remarks/>
        public string target
        {
            get
            {
                return this.targetField;
            }
            set
            {
                this.targetField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceNavMyWebExTraining
    {

        private string targetField;

        /// <remarks/>
        public string target
        {
            get
            {
                return this.targetField;
            }
            set
            {
                this.targetField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceNavAllServices
    {

        private siteInstanceNavAllServicesCustomLink[] customLinksField;

        private siteInstanceNavAllServicesSupport supportField;

        private siteInstanceNavAllServicesTraining trainingField;

        private siteInstanceNavAllServicesSupportMenu supportMenuField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("customLink", IsNullable = false)]
        public siteInstanceNavAllServicesCustomLink[] customLinks
        {
            get
            {
                return this.customLinksField;
            }
            set
            {
                this.customLinksField = value;
            }
        }

        /// <remarks/>
        public siteInstanceNavAllServicesSupport support
        {
            get
            {
                return this.supportField;
            }
            set
            {
                this.supportField = value;
            }
        }

        /// <remarks/>
        public siteInstanceNavAllServicesTraining training
        {
            get
            {
                return this.trainingField;
            }
            set
            {
                this.trainingField = value;
            }
        }

        /// <remarks/>
        public siteInstanceNavAllServicesSupportMenu supportMenu
        {
            get
            {
                return this.supportMenuField;
            }
            set
            {
                this.supportMenuField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceNavAllServicesCustomLink
    {

        private string targetField;

        /// <remarks/>
        public string target
        {
            get
            {
                return this.targetField;
            }
            set
            {
                this.targetField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceNavAllServicesSupport
    {

        private string nameField;

        private string targetField;

        /// <remarks/>
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public string target
        {
            get
            {
                return this.targetField;
            }
            set
            {
                this.targetField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceNavAllServicesTraining
    {

        private string nameField;

        private string targetField;

        /// <remarks/>
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public string target
        {
            get
            {
                return this.targetField;
            }
            set
            {
                this.targetField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceNavAllServicesSupportMenu
    {

        private siteInstanceNavAllServicesSupportMenuUserGuides userGuidesField;

        private siteInstanceNavAllServicesSupportMenuDownloads downloadsField;

        private siteInstanceNavAllServicesSupportMenuTraining trainingField;

        private siteInstanceNavAllServicesSupportMenuContactUs contactUsField;

        private bool supportMyResourcesField;

        /// <remarks/>
        public siteInstanceNavAllServicesSupportMenuUserGuides userGuides
        {
            get
            {
                return this.userGuidesField;
            }
            set
            {
                this.userGuidesField = value;
            }
        }

        /// <remarks/>
        public siteInstanceNavAllServicesSupportMenuDownloads downloads
        {
            get
            {
                return this.downloadsField;
            }
            set
            {
                this.downloadsField = value;
            }
        }

        /// <remarks/>
        public siteInstanceNavAllServicesSupportMenuTraining training
        {
            get
            {
                return this.trainingField;
            }
            set
            {
                this.trainingField = value;
            }
        }

        /// <remarks/>
        public siteInstanceNavAllServicesSupportMenuContactUs contactUs
        {
            get
            {
                return this.contactUsField;
            }
            set
            {
                this.contactUsField = value;
            }
        }

        /// <remarks/>
        public bool supportMyResources
        {
            get
            {
                return this.supportMyResourcesField;
            }
            set
            {
                this.supportMyResourcesField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceNavAllServicesSupportMenuUserGuides
    {

        private string targetField;

        /// <remarks/>
        public string target
        {
            get
            {
                return this.targetField;
            }
            set
            {
                this.targetField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceNavAllServicesSupportMenuDownloads
    {

        private string targetField;

        /// <remarks/>
        public string target
        {
            get
            {
                return this.targetField;
            }
            set
            {
                this.targetField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceNavAllServicesSupportMenuTraining
    {

        private string targetField;

        /// <remarks/>
        public string target
        {
            get
            {
                return this.targetField;
            }
            set
            {
                this.targetField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceNavAllServicesSupportMenuContactUs
    {

        private string targetField;

        /// <remarks/>
        public string target
        {
            get
            {
                return this.targetField;
            }
            set
            {
                this.targetField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstancePasswordCriteria
    {

        private bool mixedCaseField;

        private byte minLengthField;

        private byte minAlphaField;

        private byte minNumericField;

        private byte minSpecialField;

        private bool disallowWebTextSessionsField;

        private bool disallowWebTextAccountsField;

        private bool disallowListField;

        private string[] disallowValueField;

        /// <remarks/>
        public bool mixedCase
        {
            get
            {
                return this.mixedCaseField;
            }
            set
            {
                this.mixedCaseField = value;
            }
        }

        /// <remarks/>
        public byte minLength
        {
            get
            {
                return this.minLengthField;
            }
            set
            {
                this.minLengthField = value;
            }
        }

        /// <remarks/>
        public byte minAlpha
        {
            get
            {
                return this.minAlphaField;
            }
            set
            {
                this.minAlphaField = value;
            }
        }

        /// <remarks/>
        public byte minNumeric
        {
            get
            {
                return this.minNumericField;
            }
            set
            {
                this.minNumericField = value;
            }
        }

        /// <remarks/>
        public byte minSpecial
        {
            get
            {
                return this.minSpecialField;
            }
            set
            {
                this.minSpecialField = value;
            }
        }

        /// <remarks/>
        public bool disallowWebTextSessions
        {
            get
            {
                return this.disallowWebTextSessionsField;
            }
            set
            {
                this.disallowWebTextSessionsField = value;
            }
        }

        /// <remarks/>
        public bool disallowWebTextAccounts
        {
            get
            {
                return this.disallowWebTextAccountsField;
            }
            set
            {
                this.disallowWebTextAccountsField = value;
            }
        }

        /// <remarks/>
        public bool disallowList
        {
            get
            {
                return this.disallowListField;
            }
            set
            {
                this.disallowListField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("disallowValue")]
        public string[] disallowValue
        {
            get
            {
                return this.disallowValueField;
            }
            set
            {
                this.disallowValueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceRecordingPasswordCriteria
    {

        private bool mixedCaseField;

        private byte minLengthField;

        private byte minAlphaField;

        private byte minNumericField;

        private byte minSpecialField;

        private bool disallowWebTextSessionsField;

        private bool disallowListField;

        private string[] disallowValueField;

        /// <remarks/>
        public bool mixedCase
        {
            get
            {
                return this.mixedCaseField;
            }
            set
            {
                this.mixedCaseField = value;
            }
        }

        /// <remarks/>
        public byte minLength
        {
            get
            {
                return this.minLengthField;
            }
            set
            {
                this.minLengthField = value;
            }
        }

        /// <remarks/>
        public byte minAlpha
        {
            get
            {
                return this.minAlphaField;
            }
            set
            {
                this.minAlphaField = value;
            }
        }

        /// <remarks/>
        public byte minNumeric
        {
            get
            {
                return this.minNumericField;
            }
            set
            {
                this.minNumericField = value;
            }
        }

        /// <remarks/>
        public byte minSpecial
        {
            get
            {
                return this.minSpecialField;
            }
            set
            {
                this.minSpecialField = value;
            }
        }

        /// <remarks/>
        public bool disallowWebTextSessions
        {
            get
            {
                return this.disallowWebTextSessionsField;
            }
            set
            {
                this.disallowWebTextSessionsField = value;
            }
        }

        /// <remarks/>
        public bool disallowList
        {
            get
            {
                return this.disallowListField;
            }
            set
            {
                this.disallowListField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("disallowValue")]
        public string[] disallowValue
        {
            get
            {
                return this.disallowValueField;
            }
            set
            {
                this.disallowValueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceAccountPasswordCriteria
    {

        private bool mixedCaseField;

        private byte minLengthField;

        private byte minNumericField;

        private byte minAlphaField;

        private byte minSpecialField;

        private bool disallow3XRepeatedCharField;

        private bool disallowWebTextAccountsField;

        private bool disallowListField;

        private string[] disallowValueField;

        /// <remarks/>
        public bool mixedCase
        {
            get
            {
                return this.mixedCaseField;
            }
            set
            {
                this.mixedCaseField = value;
            }
        }

        /// <remarks/>
        public byte minLength
        {
            get
            {
                return this.minLengthField;
            }
            set
            {
                this.minLengthField = value;
            }
        }

        /// <remarks/>
        public byte minNumeric
        {
            get
            {
                return this.minNumericField;
            }
            set
            {
                this.minNumericField = value;
            }
        }

        /// <remarks/>
        public byte minAlpha
        {
            get
            {
                return this.minAlphaField;
            }
            set
            {
                this.minAlphaField = value;
            }
        }

        /// <remarks/>
        public byte minSpecial
        {
            get
            {
                return this.minSpecialField;
            }
            set
            {
                this.minSpecialField = value;
            }
        }

        /// <remarks/>
        public bool disallow3XRepeatedChar
        {
            get
            {
                return this.disallow3XRepeatedCharField;
            }
            set
            {
                this.disallow3XRepeatedCharField = value;
            }
        }

        /// <remarks/>
        public bool disallowWebTextAccounts
        {
            get
            {
                return this.disallowWebTextAccountsField;
            }
            set
            {
                this.disallowWebTextAccountsField = value;
            }
        }

        /// <remarks/>
        public bool disallowList
        {
            get
            {
                return this.disallowListField;
            }
            set
            {
                this.disallowListField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("disallowValue")]
        public string[] disallowValue
        {
            get
            {
                return this.disallowValueField;
            }
            set
            {
                this.disallowValueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceProductivityTools
    {

        private bool enableField;

        private siteInstanceProductivityToolsInstallOpts installOptsField;

        private siteInstanceProductivityToolsIntegrations integrationsField;

        private siteInstanceProductivityToolsOneClick oneClickField;

        private siteInstanceProductivityToolsTemplates templatesField;

        private siteInstanceProductivityToolsLockDownPT lockDownPTField;

        private siteInstanceProductivityToolsImSettings imSettingsField;

        /// <remarks/>
        public bool enable
        {
            get
            {
                return this.enableField;
            }
            set
            {
                this.enableField = value;
            }
        }

        /// <remarks/>
        public siteInstanceProductivityToolsInstallOpts installOpts
        {
            get
            {
                return this.installOptsField;
            }
            set
            {
                this.installOptsField = value;
            }
        }

        /// <remarks/>
        public siteInstanceProductivityToolsIntegrations integrations
        {
            get
            {
                return this.integrationsField;
            }
            set
            {
                this.integrationsField = value;
            }
        }

        /// <remarks/>
        public siteInstanceProductivityToolsOneClick oneClick
        {
            get
            {
                return this.oneClickField;
            }
            set
            {
                this.oneClickField = value;
            }
        }

        /// <remarks/>
        public siteInstanceProductivityToolsTemplates templates
        {
            get
            {
                return this.templatesField;
            }
            set
            {
                this.templatesField = value;
            }
        }

        /// <remarks/>
        public siteInstanceProductivityToolsLockDownPT lockDownPT
        {
            get
            {
                return this.lockDownPTField;
            }
            set
            {
                this.lockDownPTField = value;
            }
        }

        /// <remarks/>
        public siteInstanceProductivityToolsImSettings imSettings
        {
            get
            {
                return this.imSettingsField;
            }
            set
            {
                this.imSettingsField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceProductivityToolsInstallOpts
    {

        private bool autoUpdateField;

        /// <remarks/>
        public bool autoUpdate
        {
            get
            {
                return this.autoUpdateField;
            }
            set
            {
                this.autoUpdateField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceProductivityToolsIntegrations
    {

        private bool outlookField;

        private bool outlookForMacField;

        private bool lotusNotesField;

        private bool oneClickField;

        private bool showSysTrayIconField;

        private bool officeField;

        private bool excelField;

        private bool powerPointField;

        private bool wordField;

        private bool ieField;

        private bool firefoxField;

        private bool explorerRightClickField;

        private bool instantMessengerField;

        private bool aolMessengerField;

        private bool googleTalkField;

        private bool lotusSametimeField;

        private bool skypeField;

        private bool windowsMessengerField;

        private bool yahooMessengerField;

        private bool ciscoIPPhoneField;

        private bool pcNowField;

        private bool iGoogleField;

        private bool iPhoneDustingField;

        /// <remarks/>
        public bool outlook
        {
            get
            {
                return this.outlookField;
            }
            set
            {
                this.outlookField = value;
            }
        }

        /// <remarks/>
        public bool outlookForMac
        {
            get
            {
                return this.outlookForMacField;
            }
            set
            {
                this.outlookForMacField = value;
            }
        }

        /// <remarks/>
        public bool lotusNotes
        {
            get
            {
                return this.lotusNotesField;
            }
            set
            {
                this.lotusNotesField = value;
            }
        }

        /// <remarks/>
        public bool oneClick
        {
            get
            {
                return this.oneClickField;
            }
            set
            {
                this.oneClickField = value;
            }
        }

        /// <remarks/>
        public bool showSysTrayIcon
        {
            get
            {
                return this.showSysTrayIconField;
            }
            set
            {
                this.showSysTrayIconField = value;
            }
        }

        /// <remarks/>
        public bool office
        {
            get
            {
                return this.officeField;
            }
            set
            {
                this.officeField = value;
            }
        }

        /// <remarks/>
        public bool excel
        {
            get
            {
                return this.excelField;
            }
            set
            {
                this.excelField = value;
            }
        }

        /// <remarks/>
        public bool powerPoint
        {
            get
            {
                return this.powerPointField;
            }
            set
            {
                this.powerPointField = value;
            }
        }

        /// <remarks/>
        public bool word
        {
            get
            {
                return this.wordField;
            }
            set
            {
                this.wordField = value;
            }
        }

        /// <remarks/>
        public bool IE
        {
            get
            {
                return this.ieField;
            }
            set
            {
                this.ieField = value;
            }
        }

        /// <remarks/>
        public bool firefox
        {
            get
            {
                return this.firefoxField;
            }
            set
            {
                this.firefoxField = value;
            }
        }

        /// <remarks/>
        public bool explorerRightClick
        {
            get
            {
                return this.explorerRightClickField;
            }
            set
            {
                this.explorerRightClickField = value;
            }
        }

        /// <remarks/>
        public bool instantMessenger
        {
            get
            {
                return this.instantMessengerField;
            }
            set
            {
                this.instantMessengerField = value;
            }
        }

        /// <remarks/>
        public bool aolMessenger
        {
            get
            {
                return this.aolMessengerField;
            }
            set
            {
                this.aolMessengerField = value;
            }
        }

        /// <remarks/>
        public bool googleTalk
        {
            get
            {
                return this.googleTalkField;
            }
            set
            {
                this.googleTalkField = value;
            }
        }

        /// <remarks/>
        public bool lotusSametime
        {
            get
            {
                return this.lotusSametimeField;
            }
            set
            {
                this.lotusSametimeField = value;
            }
        }

        /// <remarks/>
        public bool skype
        {
            get
            {
                return this.skypeField;
            }
            set
            {
                this.skypeField = value;
            }
        }

        /// <remarks/>
        public bool windowsMessenger
        {
            get
            {
                return this.windowsMessengerField;
            }
            set
            {
                this.windowsMessengerField = value;
            }
        }

        /// <remarks/>
        public bool yahooMessenger
        {
            get
            {
                return this.yahooMessengerField;
            }
            set
            {
                this.yahooMessengerField = value;
            }
        }

        /// <remarks/>
        public bool ciscoIPPhone
        {
            get
            {
                return this.ciscoIPPhoneField;
            }
            set
            {
                this.ciscoIPPhoneField = value;
            }
        }

        /// <remarks/>
        public bool pcNow
        {
            get
            {
                return this.pcNowField;
            }
            set
            {
                this.pcNowField = value;
            }
        }

        /// <remarks/>
        public bool iGoogle
        {
            get
            {
                return this.iGoogleField;
            }
            set
            {
                this.iGoogleField = value;
            }
        }

        /// <remarks/>
        public bool iPhoneDusting
        {
            get
            {
                return this.iPhoneDustingField;
            }
            set
            {
                this.iPhoneDustingField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceProductivityToolsOneClick
    {

        private bool allowJoinUnlistMeetingField;

        private bool requireApproveJoinField;

        /// <remarks/>
        public bool allowJoinUnlistMeeting
        {
            get
            {
                return this.allowJoinUnlistMeetingField;
            }
            set
            {
                this.allowJoinUnlistMeetingField = value;
            }
        }

        /// <remarks/>
        public bool requireApproveJoin
        {
            get
            {
                return this.requireApproveJoinField;
            }
            set
            {
                this.requireApproveJoinField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceProductivityToolsTemplates
    {

        private bool useTemplateField;

        /// <remarks/>
        public bool useTemplate
        {
            get
            {
                return this.useTemplateField;
            }
            set
            {
                this.useTemplateField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceProductivityToolsLockDownPT
    {

        private bool lockDownField;

        /// <remarks/>
        public bool lockDown
        {
            get
            {
                return this.lockDownField;
            }
            set
            {
                this.lockDownField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceProductivityToolsImSettings
    {

        private bool attendeeInviteOtherField;

        /// <remarks/>
        public bool attendeeInviteOther
        {
            get
            {
                return this.attendeeInviteOtherField;
            }
            set
            {
                this.attendeeInviteOtherField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceSalesCenter
    {

        private bool allowJoinWithoutLoginField;

        /// <remarks/>
        public bool allowJoinWithoutLogin
        {
            get
            {
                return this.allowJoinWithoutLoginField;
            }
            set
            {
                this.allowJoinWithoutLoginField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceConnectIntegration
    {

        private bool integratedWebEx11Field;

        /// <remarks/>
        public bool integratedWebEx11
        {
            get
            {
                return this.integratedWebEx11Field;
            }
            set
            {
                this.integratedWebEx11Field = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceVideo
    {

        private bool hQvideoField;

        private string maxBandwidthField;

        private bool hDvideoField;

        /// <remarks/>
        public bool HQvideo
        {
            get
            {
                return this.hQvideoField;
            }
            set
            {
                this.hQvideoField = value;
            }
        }

        /// <remarks/>
        public string maxBandwidth
        {
            get
            {
                return this.maxBandwidthField;
            }
            set
            {
                this.maxBandwidthField = value;
            }
        }

        /// <remarks/>
        public bool HDvideo
        {
            get
            {
                return this.hDvideoField;
            }
            set
            {
                this.hDvideoField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceSiteCommonOptions
    {

        private bool supportCustomDialRestrictionField;

        private bool supportTelePresenceField;

        private bool supportTelePresencePlusField;

        private bool enableCloudTelepresenceField;

        private bool enablePersonalMeetingRoomField;

        private bool supportAlternateHostField;

        private bool supportAnyoneHostMeetingsField;

        /// <remarks/>
        public bool SupportCustomDialRestriction
        {
            get
            {
                return this.supportCustomDialRestrictionField;
            }
            set
            {
                this.supportCustomDialRestrictionField = value;
            }
        }

        /// <remarks/>
        public bool SupportTelePresence
        {
            get
            {
                return this.supportTelePresenceField;
            }
            set
            {
                this.supportTelePresenceField = value;
            }
        }

        /// <remarks/>
        public bool SupportTelePresencePlus
        {
            get
            {
                return this.supportTelePresencePlusField;
            }
            set
            {
                this.supportTelePresencePlusField = value;
            }
        }

        /// <remarks/>
        public bool EnableCloudTelepresence
        {
            get
            {
                return this.enableCloudTelepresenceField;
            }
            set
            {
                this.enableCloudTelepresenceField = value;
            }
        }

        /// <remarks/>
        public bool enablePersonalMeetingRoom
        {
            get
            {
                return this.enablePersonalMeetingRoomField;
            }
            set
            {
                this.enablePersonalMeetingRoomField = value;
            }
        }

        /// <remarks/>
        public bool SupportAlternateHost
        {
            get
            {
                return this.supportAlternateHostField;
            }
            set
            {
                this.supportAlternateHostField = value;
            }
        }

        /// <remarks/>
        public bool SupportAnyoneHostMeetings
        {
            get
            {
                return this.supportAnyoneHostMeetingsField;
            }
            set
            {
                this.supportAnyoneHostMeetingsField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceSamlSSO
    {

        private bool enableSSOField;

        private bool autoAccountCreationField;

        /// <remarks/>
        public bool enableSSO
        {
            get
            {
                return this.enableSSOField;
            }
            set
            {
                this.enableSSOField = value;
            }
        }

        /// <remarks/>
        public bool autoAccountCreation
        {
            get
            {
                return this.autoAccountCreationField;
            }
            set
            {
                this.autoAccountCreationField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.webex.com/schemas/2002/06/service/site")]
    public partial class siteInstanceAttendeeLimitation
    {

        private ushort maxInviteesNumberForMCField;

        private ushort maxRegistrantsNumberForMCField;

        private ushort maxInviteesNumberForTCField;

        private ushort maxRegistrantsNumberForTCField;

        private ushort maxInviteesNumberForECField;

        private ushort maxRegistrantsNumberForECField;

        /// <remarks/>
        public ushort maxInviteesNumberForMC
        {
            get
            {
                return this.maxInviteesNumberForMCField;
            }
            set
            {
                this.maxInviteesNumberForMCField = value;
            }
        }

        /// <remarks/>
        public ushort maxRegistrantsNumberForMC
        {
            get
            {
                return this.maxRegistrantsNumberForMCField;
            }
            set
            {
                this.maxRegistrantsNumberForMCField = value;
            }
        }

        /// <remarks/>
        public ushort maxInviteesNumberForTC
        {
            get
            {
                return this.maxInviteesNumberForTCField;
            }
            set
            {
                this.maxInviteesNumberForTCField = value;
            }
        }

        /// <remarks/>
        public ushort maxRegistrantsNumberForTC
        {
            get
            {
                return this.maxRegistrantsNumberForTCField;
            }
            set
            {
                this.maxRegistrantsNumberForTCField = value;
            }
        }

        /// <remarks/>
        public ushort maxInviteesNumberForEC
        {
            get
            {
                return this.maxInviteesNumberForECField;
            }
            set
            {
                this.maxInviteesNumberForECField = value;
            }
        }

        /// <remarks/>
        public ushort maxRegistrantsNumberForEC
        {
            get
            {
                return this.maxRegistrantsNumberForECField;
            }
            set
            {
                this.maxRegistrantsNumberForECField = value;
            }
        }
    }


}
