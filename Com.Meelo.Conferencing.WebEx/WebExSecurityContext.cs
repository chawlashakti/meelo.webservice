﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.Conferencing.WebEx
{
    public class WebExSecurityContext
    {
        public string WebExID { get; set; }

        public string Password { get; set; }

        public string SiteName { get; set; }

        public string SiteID { get; set; }

        public string PartnerID { get; set; }

        public string Email { get; set; }

        public string SessionTicket { get; set; }
    }
}
