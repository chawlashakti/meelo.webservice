﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.Conferencing.WebEx
{
    public static class DateTimeExtension
    {
        public static string ToIsoReadable(this DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-dd HH':'mm':'ss");
        }

        public static string ToIsoReadable(this DateTimeOffset dateTime)
        {
            return dateTime.ToString("yyyy-MM-dd HH':'mm':'ss");
        }

        public static string ToWebExReadable(this DateTime dateTime)
        {
            return dateTime.ToString("MM/dd/yyyy HH':'mm':'ss");
        }

        public static string ToWebExReadable(this DateTimeOffset dateTime)
        {
            return dateTime.ToString("MM/dd/yyyy HH':'mm':'ss");
        }
    }
}
