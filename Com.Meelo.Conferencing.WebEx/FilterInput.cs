﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.Conferencing.WebEx
{
    public enum LstMethod
    {
        AND,
        OR
    }

    public enum OrderAD
    {
        ASC,
        DESC

    }
    public enum OrderBy
    {
        HOSTWEBEXID,
        CONFNAME,
        STARTTIME
    }
   public class FilterInput
    {
        public FilterInput()
        {
            StartFrom = 0;
            MaximumNum = 50;
            ResultSortOrder = new Dictionary<OrderBy, OrderAD?>();
        }
        public LstMethod? ListMethod { get; set; }

        public int? StartFrom { get; set; }

        public int? MaximumNum { get; set; }

        public DateScope DateScope { get; set; }

        public IDictionary<OrderBy, OrderAD?> ResultSortOrder { get; set; }

        public string GetListControlXml()
        {
            StringBuilder strb = new StringBuilder();
            strb.Append("<listControl>");
            if (StartFrom == null) StartFrom = 0;

            strb.AppendFormat("<startFrom>{0}</startFrom>", StartFrom);

            if (MaximumNum == null) MaximumNum = 20;

            strb.AppendFormat("<maximumNum>{0}</maximumNum>", MaximumNum);

            if (ListMethod != null)
                strb.AppendFormat("<listMethod>{0}</listMethod>", ListMethod.Value.ToString());
            strb.Append("</listControl>");

            return strb.ToString();
            
        }

        public string GetResultSortOrderXml()
        {
            if (ResultSortOrder != null && ResultSortOrder.Count > 0)
            {
                StringBuilder strb = new StringBuilder();
                strb.Append("<order>");

                foreach (KeyValuePair<OrderBy, OrderAD?> kv in ResultSortOrder)
                {
                    strb.AppendFormat("<orderBy>{0}</orderBy>", kv.Key.ToString());
                    if (kv.Value.HasValue && kv.Value != null)
                        strb.AppendFormat("<orderAD>{0}</orderAD>", kv.Value.ToString());
                }

                strb.Append("</order>");

                return strb.ToString();
            }
            return string.Empty;
        }
    }
}
