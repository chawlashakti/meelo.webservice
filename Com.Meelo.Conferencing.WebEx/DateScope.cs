﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.Conferencing.WebEx
{
    public class DateScope
    {
        public DateTimeOffset? StartDateStart { get; set; }

        public DateTimeOffset? StartDateEnd { get; set; }

        public DateTimeOffset? EndDateStart { get; set; }

        public DateTimeOffset? EndDateEnd { get; set; }

        public int? TimezoneId { get; set; }

        public string ToDateScopeXml()
        {
            StringBuilder strb = new StringBuilder();
            strb.Append("<dateScope>");

            if (StartDateStart != null)
                strb.AppendFormat("<startDateStart>{0}</startDateStart>", StartDateStart.Value.ToWebExReadable());
            if (StartDateEnd != null)
                strb.AppendFormat("<startDateEnd>{0}</startDateEnd>", StartDateEnd.Value.ToWebExReadable());
            if (EndDateStart != null)
                strb.AppendFormat("<endDateStart>{0}</endDateStart>", EndDateStart.Value.ToWebExReadable());
            if (EndDateEnd != null)
                strb.AppendFormat("<endDateEnd>{0}</endDateEnd>", EndDateEnd.Value.ToWebExReadable());

            if (TimezoneId!=null)
            {
                strb.AppendFormat("<timeZoneID>{0}</timeZoneID>", TimezoneId);
            }
            strb.Append("</dateScope>");

            return strb.ToString();
        }

        public string ToCreateTimeScopeXml()
        {
            StringBuilder strb = new StringBuilder();
            strb.Append("<createTimeScope>");

            if (StartDateStart != null)
                strb.AppendFormat("<createTimeStart>{0}</createTimeStart>", StartDateStart.Value.ToWebExReadable());
            if (StartDateEnd != null)
                strb.AppendFormat("<createTimeEnd>{0}</createTimeEnd>", StartDateEnd.Value.ToWebExReadable());
          

            if (TimezoneId != null)
            {
                strb.AppendFormat("<timeZoneID>{0}</timeZoneID>", TimezoneId);
            }
            strb.Append("</createTimeScope>");

            return strb.ToString();
        }
    }
}
