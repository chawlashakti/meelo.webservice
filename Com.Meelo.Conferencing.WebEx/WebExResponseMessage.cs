﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.Meelo.Conferencing.WebEx.Generic;

namespace Com.Meelo.Conferencing.WebEx
{
    public class WebExResponseMessage
    {
       public message ResponseMessage { get; set; }

        public Com.Meelo.Conferencing.WebEx.Meeting.message GetMeetingResponseMessage { get; set; }

        public string ResponseMessageRaw { get; set; }

        public WebExResponseHeader Header { get; set; }
    }
}
