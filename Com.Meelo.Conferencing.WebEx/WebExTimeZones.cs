﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.Conferencing.WebEx
{
   public static class WebExTimeZones
    {

        public static readonly IDictionary<int, string> TimeZoneIds = new Dictionary<int, string>();
        

        static WebExTimeZones()
        {
            TimeZoneIds.Add(0, "GMT-12:00, Dateline(Eniwetok)");
            TimeZoneIds.Add(1, "GMT-11:00, Samoa (Samoa)");
            TimeZoneIds.Add(2, "GMT-10:00, Hawaii (Honolulu)");
            TimeZoneIds.Add(3, "GMT-09:00, Alaska (Anchorage)");
            TimeZoneIds.Add(4, "GMT-08:00, Pacific (San Jose)");
            TimeZoneIds.Add(5, "GMT-07:00, Mountain (Arizona)");
            TimeZoneIds.Add(6, "GMT-07:00, Mountain (Denver)");
            TimeZoneIds.Add(7, "GMT-06:00, Central (Chicago)");
            TimeZoneIds.Add(8, "GMT-06:00, Mexico (Mexico City, Tegucigalpa)");
            TimeZoneIds.Add(9, "GMT-06:00, Central (Regina)");
            TimeZoneIds.Add(10, "GMT-05:00, S. America Pacific (Bogota)");
            TimeZoneIds.Add(11, "GMT-05:00, Eastern (New York)");
            TimeZoneIds.Add(12, "GMT-05:00, Eastern (Indiana)");
            TimeZoneIds.Add(13, "GMT-04:00, Atlantic (Halifax)");
            TimeZoneIds.Add(14, "GMT-04:00, S. America Western (Caracas)");
            TimeZoneIds.Add(15, "GMT-03:30, Newfoundland (Newfoundland)");
            TimeZoneIds.Add(16, "GMT-03:00, S. America Eastern (Brasilia)");
            TimeZoneIds.Add(17, "GMT-03:00, S. America Eastern (Buenos Aires)");
            TimeZoneIds.Add(18, "GMT-02:00, Mid-Atlantic (Mid-Atlantic)");
            TimeZoneIds.Add(19, "GMT-01:00, Azores (Azores)");
            TimeZoneIds.Add(20, "GMT+00:00, Greenwich (Casablanca)");
            TimeZoneIds.Add(21, "GMT+00:00, GMT (London)");
            TimeZoneIds.Add(22, "GMT+01:00, Europe (Amsterdam)");
            TimeZoneIds.Add(23, "GMT+01:00, Europe (Paris)");
            TimeZoneIds.Add(24, "Deprecated. Will change to timezone 23 instead.");
            TimeZoneIds.Add(25, "GMT+01:00, Europe (Berlin)");
            TimeZoneIds.Add(26, "GMT+02:00, Greece (Athens)");
            TimeZoneIds.Add(27, "Deprecated. Will change to timezone 26 instead.");
            TimeZoneIds.Add(28, "GMT+02:00, Egypt (Cairo)");
            TimeZoneIds.Add(29, "GMT+02:00, South Africa (Pretoria)");
            TimeZoneIds.Add(30, "GMT+02:00, Northern Europe (Helsinki)");
            TimeZoneIds.Add(31, "GMT+02:00, Israel (Tel Aviv)");
            TimeZoneIds.Add(32, "GMT+03:00, Saudi Arabia (Baghdad)");
            TimeZoneIds.Add(33, "GMT+03:00, Russian (Moscow)");
            TimeZoneIds.Add(34, "GMT+03:00, Nairobi (Nairobi)");
            TimeZoneIds.Add(35, "GMT+03:30, Iran (Tehran)");
            TimeZoneIds.Add(36, "GMT+04:00, Arabian (Abu Dhabi, Muscat)");
            TimeZoneIds.Add(37, "GMT+04:00, Baku (Baku)");
            TimeZoneIds.Add(38, "GMT+04:30, Afghanistan (Kabul)");
            TimeZoneIds.Add(39, "GMT+05:00, West Asia (Ekaterinburg)");
            TimeZoneIds.Add(40, "GMT+05:00, West Asia (Islamabad)");
            TimeZoneIds.Add(41, "GMT+05:30, India (Bombay)");
            TimeZoneIds.Add(42, "GMT+06:00, Columbo (Columbo)");
            TimeZoneIds.Add(43, "GMT+06:00, Central Asia (Almaty)");
            TimeZoneIds.Add(44, "GMT+07:00, Bangkok (Bangkok)");
            TimeZoneIds.Add(45, "GMT+08:00, China (Beijing)");
            TimeZoneIds.Add(46, "GMT+08:00, Australia Western (Perth)");
            TimeZoneIds.Add(47, "GMT+08:00, Singapore (Singapore)");
            TimeZoneIds.Add(48, "GMT+08:00, Taipei (Hong Kong)");
            TimeZoneIds.Add(49, "GMT+09:00, Tokyo (Tokyo)");
            TimeZoneIds.Add(50, "GMT+09:00, Korea (Seoul)");
            TimeZoneIds.Add(51, "GMT+09:00, Yakutsk (Yakutsk)");
            TimeZoneIds.Add(52, "GMT+09:30, Australia Central (Adelaide)");
            TimeZoneIds.Add(53, "GMT+09:30, Australia Central (Darwin)");
            TimeZoneIds.Add(54, "GMT+10:00, Australia Eastern (Brisbane)");
            TimeZoneIds.Add(55, "GMT+10:00, Australia Eastern (Sydney)");
            TimeZoneIds.Add(56, "GMT+10:00, West Pacific (Guam)");
            TimeZoneIds.Add(57, "GMT+10:00, Tasmania (Hobart)");
            TimeZoneIds.Add(58, "GMT+10:00, Vladivostok (Vladivostok)");
            TimeZoneIds.Add(59, "GMT+11:00, Central Pacific (Solomon Is)");
            TimeZoneIds.Add(60, "GMT+12:00, New Zealand (Wellington)");
            TimeZoneIds.Add(61, "GMT+12:00, Fiji (Fiji)");

         

        }
       
    }
}
