﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoogleMaps.LocationServices;

namespace Com.Meelo.Geolocation
{
    public class GeolocationService
    {
        private static readonly GoogleLocationService LocationService = new GoogleLocationService();
        public static MapPoint GetLatLongFromAddress(string address)
        {
         
           
            return LocationService.GetLatLongFromAddress(address);
          

        }

        public static MapPoint GetLatLongFromAddress(AddressData address)
        {
            return LocationService.GetLatLongFromAddress(address);
        }

        public static string[] GetAddressesListFromAddress(string address)
        {


            return LocationService.GetAddressesListFromAddress(address);


        }

        public static string[] GetAddressesListFromAddress(AddressData address)
        {
            return LocationService.GetAddressesListFromAddress(address);
        }
        public static AddressData GetAddressFromLatLang(double latitude, double longitude)
        {
            return LocationService.GetAddressFromLatLang(latitude, longitude);
        }
        public static Directions GetDirections(double latitude, double longitude)
        {
            return LocationService.GetDirections(latitude, longitude);
        }
        public static Directions GetDirections(AddressData originAddress, AddressData destinationAddress)
        {
            return LocationService.GetDirections(originAddress, destinationAddress);
        }
     
      
        public static Region GetRegionFromLatLong(double latitude, double longitude)
        {
            return LocationService.GetRegionFromLatLong(latitude, longitude);
        }

      
    }
}
