﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Botf.Framework
{
    public class BotType
    {
        public string Name { get; set; }

        public string Id { get; set; }

        public string Description { get; set; }

        public string IntroductionMessage { get; set; }

        public string LogoUri { get; set; }

       

        
    }
}
