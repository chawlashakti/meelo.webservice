﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.Bot.Connector;
using Newtonsoft.Json;
using Com.Meelo.Utils;
using Com.Meelo.Utils.Azure.Data.Wrappers;
using System.Configuration;

namespace Com.Botf.Framework
{
    public class BotConversationsTableStorage 
    {
        private const string TABLE_NAME = "botconversationstable";
        private ITableStorage storage;

        public string ConnectionString
        {
            get;set;
        }

        public BotConversationsTableStorage():this(null)
        {
           
        }

        public BotConversationsTableStorage (string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                ConnectionString = ConfigurationManager.AppSettings["botconversationchannelstableconnectionstring"];
            }else
            {
                ConnectionString = connectionString;
            }
            storage = new TableStorage(TABLE_NAME, ConnectionString);

            storage.CreateIfNotExists().Wait();
        }

        public async Task<TableResult> InsertOrReplace(BotConversationChannels conversation)
        {
            return await storage.InsertOrReplace(conversation);
        }

        public async Task Delete(IActivity activity)
        {
             await storage.DeleteByPartition(BotConversationChannels.GeneratePartitionKey(activity.ChannelId, activity.Conversation.Id));
        }

        public async Task<IEnumerable<BotConversationChannels>> QueryByPartition(IActivity activity)
        {
            return await storage.QueryByPartition<BotConversationChannels>(BotConversationChannels.GeneratePartitionKey(activity.ChannelId, activity.Conversation.Id));
        }

        public async Task<IEnumerable<BotConversationChannels>> QueryByPartition(string channelId, string conversationId)
        {
            return await storage.QueryByPartition<BotConversationChannels>(BotConversationChannels.GeneratePartitionKey(channelId, conversationId));
        }
        public class BotConversationChannels : TableEntity
        {
            /// <summary>
            /// Empty constructor.
            /// </summary>
            public BotConversationChannels()
            { }

            /// <summary>
            /// Construct from an IActivity.
            /// </summary>
            /// <param name="activity"></param>
            public BotConversationChannels(IActivity activity)
            {
                PartitionKey = GeneratePartitionKey(activity.ChannelId, activity.Conversation.Id);
                RowKey = GenerateRowKey(activity.Timestamp.Value);
                From = activity.From.Id;
                Recipient = activity.Recipient.Id;
                Activity = activity;
                Version = 3.0;
            }

            /// <summary>
            /// Version number for the underlying activity.
            /// </summary>
            public double Version { get; set; }

            /// <summary>
            /// Channel identifier for sender.
            /// </summary>
            public string From { get; set; }

            /// <summary>
            /// Channel identifier for receiver.
            /// </summary>
            public string Recipient { get; set; }

            /// <summary>
            /// Logged activity.
            /// </summary>
            [IgnoreProperty]
            public IActivity Activity { get; set; }

            private const int FieldLimit = 1 << 16;

            /// <summary>
            /// Write out entity with distributed activity.
            /// </summary>
            /// <param name="operationContext"></param>
            /// <returns></returns>
            public override IDictionary<string, EntityProperty> WriteEntity(OperationContext operationContext)
            {

                var props = base.WriteEntity(operationContext);
                var buffer = JsonConvert.SerializeObject(Activity).Compress();
                var start = 0;
                var blockid = 0;
                while (start < buffer.Length)
                {
                    var blockSize = Math.Min(buffer.Length - start, FieldLimit);
                    var block = new byte[blockSize];
                    Array.Copy(buffer, start, block, 0, blockSize);
                    props[$"Activity{blockid++}"] = new EntityProperty(block);
                    start += blockSize;
                }
                return props;
            }

            /// <summary>
            /// Read entity with distributed activity.
            /// </summary>
            /// <param name="properties"></param>
            /// <param name="operationContext"></param>
            public override void ReadEntity(IDictionary<string, EntityProperty> properties, OperationContext operationContext)
            {
                base.ReadEntity(properties, operationContext);
                var blocks = 0;
                var size = 0;
                EntityProperty entityBlock;
                while (properties.TryGetValue($"Activity{blocks}", out entityBlock))
                {
                    ++blocks;
                    size += entityBlock.BinaryValue.Length;
                }
                var buffer = new byte[size];
                for (var blockid = 0; blockid < blocks; ++blockid)
                {
                    var block = properties[$"Activity{blockid}"].BinaryValue;
                    Array.Copy(block, 0, buffer, blockid * FieldLimit, block.Length);
                }
                Activity = JsonConvert.DeserializeObject<Activity>(buffer.Decompress());
            }

            /// <summary>
            /// Generate a partition key given <paramref name="channelId"/> and <paramref name="conversationId"/>.
            /// </summary>
            /// <param name="channelId">Channel where activity happened.</param>
            /// <param name="conversationId">Conversation where activity happened.</param>
            /// <returns>Partition key.</returns>
            public static string GeneratePartitionKey(string channelId, string conversationId)
            {
                return $"{channelId}|{conversationId}";
            }

            /// <summary>
            /// Generate row key for ascending <paramref name="timestamp"/>.
            /// </summary>
            /// <param name="timestamp">Timestamp of activity.</param>
            /// <returns></returns>
            public static string GenerateRowKey(DateTime timestamp)
            {
                return $"{timestamp.Ticks:D19}";
            }
        }
    }//outer class
}
