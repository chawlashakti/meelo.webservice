﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Bot.Connector;

namespace Com.Botf.Framework
{
   public interface IChannel
    {
        ResourceResponse SendToChannel(ConnectorClient connector, ChannelAccount botAccount, ConversationAccount channelInfo, string textMessage);

        ResourceResponse SendDirectConversation(ConnectorClient connector, ChannelAccount botAccount, ChannelAccount userAccount, string textMessage);
    }
}
