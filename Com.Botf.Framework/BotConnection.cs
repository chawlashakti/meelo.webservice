﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Botf.Framework
{
    public enum ConnectionType
    {
        Direct,
        Conversation
    }
    /// <summary>
    /// Instance of a bot in a conversation provider
    /// </summary>
    public class BotConnection
    {
        public string Name { get; set; }

        public string Id { get; set; }

        public string Appid { get; set; }

        public string AppPassword { get; set; }

        public string BaseUri { get; set; }

        public ConnectionType ConnectionType { get; set; }
    }
}
