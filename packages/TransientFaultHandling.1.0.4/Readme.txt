﻿/* Available extension methods */

using (var conn = new SqlConnection("connection string")) {

    conn.OpenWithRetry();

    using (var cmd = new SqlCommand("sql command", conn)) {

        cmd.ExecuteNonQueryWithRetry();

        using (var reader = cmd.ExecuteReaderWithRetry()) { }

        // returns Fasle when return value is zero, True otherwise
        cmd.ExecuteReturnWithRetry<boo>();

        // scalar unboxing to type provided as a generic parameter
        cmd.ExecuteScalarWithRetry<int>();

    }
}

/* Available async extension methods */

using (var conn = new SqlConnection("connection string")) {

    await conn.OpenWithRetryAsync();

    using (var cmd = new SqlCommand("sql command", conn)) {

        await cmd.ExecuteNonQueryWithRetryAsync();

        using (var reader = await cmd.ExecuteReaderWithRetryAsync()) { }

        // returns Fasle when return value is zero, True otherwise
        await cmd.ExecuteReturnWithRetryAsync<boo>();

        // scalar unboxing to type provided as a generic parameter
        await cmd.ExecuteScalarWithRetryAsync<int>();

    }
}