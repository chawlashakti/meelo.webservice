﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.Meelo.CoreLib;
using Microsoft.Azure.Search;
using Microsoft.Azure.Search.Models;
using Microsoft.Spatial;
using Newtonsoft.Json;

namespace Com.Meelo.Normalizers
{
    // The SerializePropertyNamesAsCamelCase attribute is defined in the Azure Search .NET SDK.
    // It ensures that Pascal-case property names in the model class are mapped to camel-case
    // field names in the index.
    [SerializePropertyNamesAsCamelCase]
    public class WorkspaceIndexObj
    {
        [System.ComponentModel.DataAnnotations.Key]
        [IsFilterable]
        public string Id { get; set; }

        [IsSearchable, IsFilterable, IsSortable]
        public string Name { get; set; }

        [IsSearchable]
        public string Description { get; set; }
        [IsSearchable, IsFilterable, IsSortable]
        public string FriendlyName { get; set; }

        [IsSearchable]
        public string Street { get; set; }

        [IsSearchable, IsFilterable]
        public string Building { get; set; }
        [IsSearchable, IsFilterable]
        public string Floor { get; set; }

        [IsFilterable, IsSortable]
        public GeographyPoint Location { get; set; }

        [IsSearchable, IsFilterable, IsSortable]
        public string City { get; set; }

        [IsSearchable, IsFilterable, IsSortable]
        public string Country { get; set; }

        [IsSearchable, IsFilterable, IsSortable]
        public string State { get; set; }

        [IsFilterable, IsSearchable]
        public string PostalCode { get; set; }

        [IsFilterable, IsSortable, IsFacetable]
        public int? Ratings { get; set; }


        [IsSearchable, IsFilterable, IsFacetable]
        public string[] Tags { get; set; }

        [IsFilterable, IsFacetable]
        public string WorkspaceType { get; set; }

        [IsFilterable, IsFacetable]
        public string OccupancyStatus { get; set; }


        [IsFilterable, IsSortable, IsFacetable]
        public int? Capacity { get; set; }

        [IsSearchable, IsFilterable, IsFacetable]
        public string[] Capabilities { get; set; }


        [IsFilterable, IsFacetable]
        public string AccessType { get; set; }


        [IsFilterable, IsSortable, IsFacetable]
        public decimal? AreaInSqFt { get; set; }

       
    }
}
