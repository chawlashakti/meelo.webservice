﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Meelo.iOS;
using Meelo;
using Meelo.CustomControls;

[assembly: ExportRenderer(typeof(GestureFrame), typeof(GestureFrameRenderer))]
namespace Meelo.iOS
{
    public class GestureFrameRenderer : ViewRenderer
    {
        UISwipeGestureRecognizer swipeDown;
        UISwipeGestureRecognizer swipeUp;
        UISwipeGestureRecognizer swipeLeft;
        UISwipeGestureRecognizer swipeRight;

        public GestureFrameRenderer()
        {
        }
         
        protected override void OnElementChanged(ElementChangedEventArgs<View> e)
        {
            base.OnElementChanged(e);

            swipeDown = new UISwipeGestureRecognizer(
                () =>
                {
                    GestureFrame _gi = (GestureFrame)this.Element;
                    _gi.OnSwipeDown();
                }
            )
            {
                Direction = UISwipeGestureRecognizerDirection.Down,
            };

            swipeUp = new UISwipeGestureRecognizer(
                () =>
                {
                    GestureFrame _gi = (GestureFrame)this.Element;
                    _gi.OnSwipeTop();
                }
            )
            {
                Direction = UISwipeGestureRecognizerDirection.Up,
            };

            swipeLeft = new UISwipeGestureRecognizer(
                () =>
                {
                    GestureFrame _gi = (GestureFrame)this.Element;
                    _gi.OnSwipeLeft();
                }
            )
            {
                Direction = UISwipeGestureRecognizerDirection.Left,
            };

            swipeRight = new UISwipeGestureRecognizer(
                () =>
                {
                    GestureFrame _gi = (GestureFrame)this.Element;
                    _gi.OnSwipeRight();
                }
            )
            {
                Direction = UISwipeGestureRecognizerDirection.Right,
            };

            if (e.NewElement == null)
            {
                if (swipeDown != null)
                {
                    this.RemoveGestureRecognizer(swipeDown);
                }
                if (swipeUp != null)
                {
                    this.RemoveGestureRecognizer(swipeUp);
                }
                if (swipeLeft != null)
                {
                    this.RemoveGestureRecognizer(swipeLeft);
                }
                if (swipeRight != null)
                {
                    this.RemoveGestureRecognizer(swipeRight);
                }
            }

            if (e.OldElement == null)
            {
                this.AddGestureRecognizer(swipeDown);
                this.AddGestureRecognizer(swipeUp);
                this.AddGestureRecognizer(swipeLeft);
                this.AddGestureRecognizer(swipeRight);
            }
        }
    }
}