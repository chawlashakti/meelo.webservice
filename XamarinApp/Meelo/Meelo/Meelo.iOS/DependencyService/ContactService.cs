﻿using Contacts;
using Foundation;
using Meelo.Interfaces;
using Meelo.iOS.DependencyService;
using Meelo.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
[assembly: Dependency(typeof(ContactService))]
namespace Meelo.iOS.DependencyService
{
    public class ContactService : IUserContactsService
    {
        public IEnumerable<PhoneContact> GetAllContacts()
        {
            var keysToFetch = new[] { CNContactKey.GivenName, CNContactKey.FamilyName, CNContactKey.PhoneNumbers,CNContactKey.EmailAddresses,CNContactKey.ThumbnailImageData };
            NSError error;
            //var containerId = new CNContactStore().DefaultContainerIdentifier;
            // using the container id of null to get all containers.
            // If you want to get contacts for only a single container type, you can specify that here
            var contactList = new List<CNContact>();

            using (var store = new CNContactStore())
            {
                var allContainers = store.GetContainers(null, out error);
                foreach (var container in allContainers)
                {
                    try
                    {
                        using (var predicate = CNContact.GetPredicateForContactsInContainer(container.Identifier))
                        {
                            var containerResults = store.GetUnifiedContacts(predicate, keysToFetch, out error);
                            contactList.AddRange(containerResults);
                        }
                    }
                    catch
                    {
                        // ignore missed contacts from errors
                    }
                }
            }
            var contacts = new List<PhoneContact>();

            foreach (var item in contactList)
            {
                //var numbers = item.PhoneNumbers;
                var emails = item.EmailAddresses;
                //var photo = UIKit.UIImage.LoadFromData(item.ThumbnailImageData);
                //if (numbers != null)
                //{
                //    foreach (var item2 in numbers)
                //    {
                //        contacts.Add(new PhoneContact
                //        {
                //            FirstName = item.GivenName,
                //            LastName = item.FamilyName,
                //            PhoneNumber = item2.Value.StringValue,
                //        });
                //    }
                //}
                if (emails!=null)
                {
                    foreach (var item3 in emails)
                    {
                        contacts.Add(new PhoneContact
                        {
                            FirstName = item.GivenName,
                            LastName = item.FamilyName,
                            EmailAddress = item3.Value.ToString()
                        });
                    }
                }
            }
            return contacts;
        }
    }
}
