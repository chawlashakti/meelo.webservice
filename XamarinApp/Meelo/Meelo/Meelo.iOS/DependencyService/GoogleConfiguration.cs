﻿using Meelo.Interfaces;
using Meelo.iOS.DependencyService;
using Meelo.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
[assembly: Dependency(typeof(GoogleConfig))]
namespace Meelo.iOS.DependencyService
{
   public class GoogleConfig : IGoogleAuthenticationConfiguration
    {
        public Services.GoogleConfiguration GetConfiguration()
        {
            return new Services.GoogleConfiguration
            {
                ClientId = "27210327045-9v06b4g37k7nrt2l0h9397clpi4ont4t.apps.googleusercontent.com",
                RedirectUrl = "com.googleusercontent.apps.27210327045-9v06b4g37k7nrt2l0h9397clpi4ont4t:/oauth2redirect"
            };
        }
    }
}
