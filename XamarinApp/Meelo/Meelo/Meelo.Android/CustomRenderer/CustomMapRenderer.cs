﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using Meelo;
using Meelo.Droid.CustomRenderer;
using Xamarin.Forms.Maps.Android;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Meelo.CustomControls;
using Xamarin.Forms.Maps;
using Android.Graphics;

[assembly: ExportRenderer(typeof(CustomMap), typeof(CustomMapRenderer))]
namespace Meelo.Droid.CustomRenderer
{
    public class CustomMapRenderer : MapRenderer
    {
        System.Collections.ObjectModel.ObservableCollection<CustomPin> customPins;
        string Count = "0";
        public CustomMapRenderer(Context context) : base(context)
        {
        }
        protected override void OnElementChanged(Xamarin.Forms.Platform.Android.ElementChangedEventArgs<Xamarin.Forms.Maps.Map> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                var formsMap = (CustomMap)e.NewElement;
                customPins = formsMap.CustomPins;
                Control.GetMapAsync(this);
            }
        }

        protected override void OnMapReady(GoogleMap map)
        {
            base.OnMapReady(map);
            map.MarkerClick += Pin_ClickedCustom;
        }
        protected override MarkerOptions CreateMarker(Pin pin)
        {
            var marker = new MarkerOptions();
            marker.SetPosition(new LatLng(pin.Position.Latitude, pin.Position.Longitude));
            marker.SetTitle(pin.Label);
            marker.SetSnippet(pin.Address);
            //if(App.Locator.MainPageViewModelInstance!=null)
            //Count = App.Locator.MapPageViewModelInstance.WorkspaceRecord.Count.ToString();
            if (pin.Position.Latitude == App.position.Latitude && pin.Position.Longitude == App.position.Longitude)
            {
                pin.Type = PinType.Place;
                pin.Label = "";
                pin.Address = "";
                marker.SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.UserMapIcon));
            }
            else
            {
                marker.SetIcon(GetCustomBitmapDescriptor(pin.Label));
            }
            return marker;
        }

        CustomPin GetCustomPin(Marker annotation)
        {
            var position = new Position(annotation.Position.Latitude, annotation.Position.Longitude);
            foreach (var pin in customPins)
            {
                if (pin.Position == position)
                {
                    return pin;
                }
            }
            return null;
        }
        void Pin_ClickedCustom(object sender, GoogleMap.MarkerClickEventArgs e)
        {
            try
            {
                var customPin = e.Marker.Position;
                for (int i = 0; i < App.Locator.MapPageViewModelInstance.WorkspaceRecord.Count; i++)
                {
                    if (customPin.Latitude == App.Locator.MapPageViewModelInstance.WorkspaceRecord[i].Lat && customPin.Longitude == App.Locator.MapPageViewModelInstance.WorkspaceRecord[i].Long)
                    {
                        Meelo.App.Locator.MapPageViewModelInstance.LoadListOnPinTap(customPin.Latitude, customPin.Longitude, App.Locator.MapPageViewModelInstance.WorkspaceRecord[i].BuildingId);
                        break;
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        private BitmapDescriptor GetCustomBitmapDescriptor(string text)
        {
            using (Paint paint = new Paint(PaintFlags.AntiAlias))
            {
                    using (Rect bounds = new Rect())
                    {
                        using (Bitmap baseBitmap = BitmapFactory.DecodeResource(Resources, Resource.Drawable.map_pin1))
                        {
                            Bitmap bitmap = baseBitmap.Copy(Bitmap.Config.Argb8888, true);

                            paint.GetTextBounds(text, 0, text.Length, bounds);
                            paint.Color = Android.Graphics.Color.Argb(255, 255, 255, 255);
                            paint.TextSize = 22.0f;
                            //float x = bitmap.Width / 2.0f;
                            float x = bitmap.Width - 30;
                            float y = 22; //(bitmap.Height - bounds.Height()) /2.0f - bounds.Top;

                            Canvas canvas = new Canvas(bitmap);
       
                            canvas.DrawText(text, x, y, paint);

                            BitmapDescriptor icon = BitmapDescriptorFactory.FromBitmap(bitmap);

                            return (icon);
                        }
                    }
            }
        }
    }
}