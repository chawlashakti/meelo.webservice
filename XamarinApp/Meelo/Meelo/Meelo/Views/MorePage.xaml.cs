﻿using Meelo.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Meelo.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MorePage : ContentPage
	{
        MorePageViewModel viewModel;
        public MorePage ()
		{
			InitializeComponent ();
            viewModel = App.Locator.MorePageViewModelInstance;
            BindingContext = viewModel;

        }
	}
}