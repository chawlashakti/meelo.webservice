﻿using GalaSoft.MvvmLight.Views;
using Meelo.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Meelo.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginOptionsPage : ContentPage
	{
        LoginOptionsViewModel viewModel;
        public LoginOptionsPage ()
		{
            NavigationPage.SetHasNavigationBar(this, false);
            viewModel = App.Locator.LoginOptionsViewModelInstance;
            BindingContext = viewModel;
			InitializeComponent ();
        }

    }
}