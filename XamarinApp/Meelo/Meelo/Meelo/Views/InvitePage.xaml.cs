﻿using CommonServiceLocator;
using GalaSoft.MvvmLight.Views;
using Meelo.Interfaces;
using Meelo.ViewModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Meelo.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class InvitePage : ContentPage
	{
        InvitePageViewModel viewModel;
        private INavigationService _navigationService;
        IEnumerable<PhoneContact> Contacts;
       

        public int TotalNumberOfTypings { get; private set; }
        public IEnumerable OriginSuggestions { get; private set; }
        private IEnumerable _Suggestions;
        public IEnumerable Suggestions
        {
            get { return _Suggestions; }
            set
            {
                OnSuggestionsChanged(this, _Suggestions, value);
                _Suggestions = value;
            }
        }
        private string _SearchText;
        public string SearchText
        {
            get { return _SearchText; }
            set
            {
                _SearchText = value;
                Search(_SearchText);
            }
        }
        private bool _busy;
        public bool Busy
        {
            get { return _busy; }
            set
            {
                _busy = value;
            }
        }
        public string Placeholder { get; set; }
        public int MaximumVisibleSuggestionItems { get; set; }
        public DataTemplate SuggestionItemTemplate { get; set; }
        public string DisplayPropertyName { get; set; }
        public InvitePage ()
		{
			InitializeComponent ();
            viewModel = App.Locator.InvitePageViewModelInstance;
            BindingContext = this;
            _navigationService = App.NavInitiate;
            NavigationPage.SetHasNavigationBar(this,false);
            Contacts= DependencyService.Get<IUserContactsService>().GetAllContacts();
            Suggestions = Contacts;
            Backbtn.Command = new Command(OnBack);
            ContactAddedListGrid.IsVisible = false;
            SearchList.ItemSelected += SearchList_ItemSelected;
            SearchEntry.Focused += SearchEntry_Focused;
            lblDone.Command = new Command(viewModel.OnDone);
            viewModel.InvitePageObj = this;
            viewModel.SelectedContacts = new List<PhoneContact>();
        }

        public void DeleteClicked(object sender, EventArgs e)
        {
            var item = (Xamarin.Forms.Button)sender;
           var Selected= item.CommandParameter;
            if(Selected!=null)
            {
                PhoneContact itm = viewModel.SelectedContacts.Where(m => m.EmailAddress.Equals(Selected)).FirstOrDefault();
                List<PhoneContact> list = Contacts.ToList();
                list.Add(itm);
                viewModel.SelectedContacts.Remove(itm);
                Contacts = list;
                Suggestions = Contacts;
                SearchAddedList.ItemsSource = null;
                SearchAddedList.ItemsSource = viewModel.SelectedContacts;
            }

        }
        private void SearchEntry_Focused(object sender, FocusEventArgs e)
        {
            ContactAddedListGrid.IsVisible = false;
            SearchListGrid.IsVisible = true;
        }

        private void SearchList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            PhoneContact Item = null;
            
            if (e.SelectedItem != null)
            {
                ContactAddedListGrid.IsVisible = true;
                SearchListGrid.IsVisible = false;
                Item = (PhoneContact)e.SelectedItem;
                
               List<PhoneContact> list= Contacts.ToList();
                list.Remove(Item);
                viewModel.SelectedContacts.Add(Item);
                SearchAddedList.ItemsSource = null;
                SearchAddedList.ItemsSource = viewModel.SelectedContacts;
                Contacts = list;
                Suggestions = Contacts;
                SearchAddedList.Focus();
            }
            
            

        }

        void OnBack()
        {
            _navigationService.GoBack();
        }
        private void SearchEntry_TextChanged(object sender, TextChangedEventArgs e)
        {
            TotalNumberOfTypings++;
            Device.StartTimer(TimeSpan.FromMilliseconds(700), () => {
                TotalNumberOfTypings--;
                if (TotalNumberOfTypings == 0)
                {
                    SearchText = e.NewTextValue;
                }
                return false;
            });
        }
        public void Search(string keyword)
        {
            List<PhoneContact> List = new List<PhoneContact>();
            try
            {
                foreach (var result in Contacts)
                {
                    if (result.EmailAddress != null && result.EmailAddress.IndexOf(keyword, StringComparison.CurrentCultureIgnoreCase) != -1)
                    {
                        List.Add(new PhoneContact
                        {
                            EmailAddress = result.EmailAddress,
                            FirstName=result.FirstName,
                            LastName=result.LastName
                        });
                    }
                }
                this.Suggestions = List;
            }
            catch (Exception ex)
            { }
        }

        private static void OnSuggestionsChanged(object bindable, object oldValue, object newValue)
        {
            var autoCompleteView = bindable as InvitePage;

            var suggestions = (IEnumerable)newValue;
            autoCompleteView.OriginSuggestions = suggestions;

            autoCompleteView.SearchList.ItemsSource = suggestions;
        }
        private static void OnSearchTextChanged(object bindable, object oldValue, object newValue)
        {
            var autoCompleteView = bindable as InvitePage;

            var suggestions = autoCompleteView.OriginSuggestions;
            if (newValue != null && newValue.ToString().Length > 0)
            {
                //suggestions = autoCompleteView.FilterSuggestions(suggestions, autoCompleteView.SearchText);
                // assign when initializing with data
                if (autoCompleteView.SearchEntry.Text != autoCompleteView.SearchText)
                {
                    autoCompleteView.SearchEntry.Text = autoCompleteView.SearchText;
                }
            }
            autoCompleteView.SearchList.ItemsSource = suggestions;
        }
    }
}