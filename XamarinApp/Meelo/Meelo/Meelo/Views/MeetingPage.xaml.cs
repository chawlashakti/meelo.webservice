﻿using Meelo.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Meelo.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MeetingPage : ContentPage
	{
        MeetingPageViewModel viewModel;
		public MeetingPage ()
		{
			InitializeComponent ();
            viewModel = App.Locator.MeetingPageViewModelInstance;
            this.BindingContext = viewModel;
            
            viewModel.MeetingsListContainer = MeetingsList;
        }
	}
}