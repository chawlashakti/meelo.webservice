﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Meelo.CustomControls
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DateTimeFilterView : StackLayout
	{
        public static readonly BindableProperty DatePickerFocusChangedCommandProperty = BindableProperty.Create(nameof(DatePickerFocusChangedCommand), typeof(ICommand), typeof(DateTimeFilterView), null);
        public ICommand DatePickerFocusChangedCommand
        {
            get { return (ICommand)GetValue(DatePickerFocusChangedCommandProperty); }
            set { SetValue(DatePickerFocusChangedCommandProperty, value); }
        }
        public static readonly BindableProperty DatePickerTextProperty = BindableProperty.Create(nameof(DatePickerText), typeof(DateTime), typeof(DateTimeFilterView), DateTime.Now,propertyChanged: DatePickerTextPropertyChanged);
        public DateTime DatePickerText
        {
            get { return (DateTime)GetValue(DatePickerTextProperty); }
            set { SetValue(DatePickerTextProperty, value); }
        }
        static void DatePickerTextPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if (bindable is DateTimeFilterView)
            {
                var control = (DateTimeFilterView)bindable;
                control.Datepicker.Date = Convert.ToDateTime(newValue);
            }
        }
        public DateTimeFilterView ()
		{
			InitializeComponent ();
            Datepicker.SetBinding(DatePickerTextProperty, new Binding(path: "DatePickerText", source: this));
        }
        private void DatePicker_Unfocused(object sender, FocusEventArgs e)
        {
            if (this.DatePickerFocusChangedCommand != null && this.DatePickerFocusChangedCommand.CanExecute(null))
            {
                this.DatePickerFocusChangedCommand.Execute(null);
            }
        }

    }
}