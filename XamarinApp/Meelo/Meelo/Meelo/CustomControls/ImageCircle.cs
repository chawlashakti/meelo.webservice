﻿using System;
using Xamarin.Forms;
using System.Linq;
namespace Meelo.CustomControls
{
    public class ImageCircle : Image
    {
        public static BindableProperty IsCircleProperty = BindableProperty.Create<ImageCircle, bool>(o => o.IsCircle, false, BindingMode.TwoWay);

        public bool IsCircle
        {
            get { return (bool)GetValue(IsCircleProperty); }
            set { SetValue(IsCircleProperty, value); }
        }
    }

}
