﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
namespace Meelo.CustomControls
{
    public class RangeSliderView : AbsoluteLayout
    {
        #region Private Constants

        private const double PADDING = 35.0;
        private const double SLIDER_PATH_HEIGHT = 28.0;
        private const double BUTTON_SIZE = 30.0;
        private const double BUTTON_START_X = PADDING - (BUTTON_SIZE / 2.0);
        private const double BUBBLE_WIDTH = 64.0;
        private const double BUBBLE_HEIGHT = 45.0;

        #endregion

        // ------------------------------------------------------------

        #region Private Members
        private Image _nextButton;
        private Image _PreButton;
        private bool IsLeftDisabled;
        private bool IsRightDisabled;
        private double[] TimeBlock = new double[6];
        private double pixelPos;
        private Size _viewSize;
        private BoxView _activeBar;
        private BoxView _inActiveLeftBar;
        private BoxView _inActiveRightBar;
        private RangeSliderThumb _leftButton;
        private RangeSliderThumb _rightButton;
        private RangeSliderBubble _leftBubble;
        private RangeSliderBubble _rightBubble;
        private double _leftButtonPanGesturePosX = 0.0;
        private double _rightButtonPanGesturePosX = 0.0;
        public DateTime StartTime;
        private DateTime CurrentTime;
        private Label lblHour1;
        private Label lblHour2;
        private Label lblHour3;
        private Label lblHour4;
        private Label lblHour5;
        private Label lblHour6;
        private Label lblHHour1;
        private Label lblHHour2;
        private Label lblHHour3;
        private Label lblHHour4;
        private Label lblHHour5;
        private Label lblHHour6;
        private Label lblAmPm1;
        private Label lblAmPm2;
        private Label lblAmPm3;
        private Label lblAmPm4;
        private Label lblAmPm5;
        private Label lblAmPm6;
        #endregion

        // ------------------------------------------------------------

        #region Constructors

        public RangeSliderView()
        {
            // Left arrow button
            _PreButton = new Image { HorizontalOptions = LayoutOptions.Start, VerticalOptions = LayoutOptions.End };

            AbsoluteLayout.SetLayoutFlags(_PreButton, AbsoluteLayoutFlags.None);

            var preButtonPanGesture = new TapGestureRecognizer();

            preButtonPanGesture.Tapped += PreButtonPanGesture_Tapped;

            _PreButton.GestureRecognizers.Add(preButtonPanGesture);

            _PreButton.SetBinding(Image.SourceProperty, new Binding(path: "PrevImageSource", source: this));
          
            this.Children.Add(_PreButton);

            // Active Bar
            _activeBar = new BoxView();

            _activeBar.SetBinding(BoxView.ColorProperty, new Binding(path: "ActiveBarColor", source: this));
            _activeBar.SetBinding(BoxView.OpacityProperty, new Binding(path: "ActiveBarOpacity", source: this));
            AbsoluteLayout.SetLayoutFlags(_activeBar, AbsoluteLayoutFlags.None);

            this.Children.Add(_activeBar);

            // Inactive Left Bar
            _inActiveLeftBar = new BoxView();

            _inActiveLeftBar.SetBinding(BoxView.ColorProperty, new Binding(path: "InactiveBarColor", source: this));
            _inActiveLeftBar.SetBinding(BoxView.OpacityProperty, new Binding(path: "InactiveBarOpacity", source: this));

            AbsoluteLayout.SetLayoutFlags(_inActiveLeftBar, AbsoluteLayoutFlags.None);

            this.Children.Add(_inActiveLeftBar);

            // Inactive Right Bar
            _inActiveRightBar = new BoxView();

            _inActiveRightBar.SetBinding(BoxView.ColorProperty, new Binding(path: "InactiveBarColor", source: this));
            _inActiveRightBar.SetBinding(BoxView.OpacityProperty, new Binding(path: "InactiveBarOpacity", source: this));
            AbsoluteLayout.SetLayoutFlags(_inActiveRightBar, AbsoluteLayoutFlags.None);

            this.Children.Add(_inActiveRightBar);

            //RangeStepNumbers

            lblHour1 = new Label { VerticalOptions = LayoutOptions.End, TextColor = Color.FromHex("#000000"), FontSize = 14, FontFamily = "HelveticaNeue", HorizontalOptions = LayoutOptions.Start };
            lblHour2 = new Label { VerticalOptions = LayoutOptions.End, TextColor = Color.FromHex("#000000"), FontSize = 14, FontFamily = "HelveticaNeue", HorizontalOptions = LayoutOptions.Start };
            lblHour3 = new Label { VerticalOptions = LayoutOptions.End, TextColor = Color.FromHex("#000000"), FontSize = 14, FontFamily = "HelveticaNeue", HorizontalOptions = LayoutOptions.Start };
            lblHour4 = new Label { VerticalOptions = LayoutOptions.End, TextColor = Color.FromHex("#000000"), FontSize = 14, FontFamily = "HelveticaNeue", HorizontalOptions = LayoutOptions.Start };
            lblHour5 = new Label { VerticalOptions = LayoutOptions.End, TextColor = Color.FromHex("#000000"), FontSize = 14, FontFamily = "HelveticaNeue", HorizontalOptions = LayoutOptions.Start };
            lblHour6 = new Label { VerticalOptions = LayoutOptions.End, TextColor = Color.FromHex("#000000"), FontSize = 14, FontFamily = "HelveticaNeue", HorizontalOptions = LayoutOptions.Start };
            lblHHour1 = new Label { Text = ".", VerticalOptions = LayoutOptions.Start, TextColor = Color.FromHex("#000000"), FontSize = 20, FontFamily = "HelveticaNeue", FontAttributes = FontAttributes.Bold };
            lblHHour2 = new Label { Text = ".", VerticalOptions = LayoutOptions.Start, TextColor = Color.FromHex("#000000"), FontSize = 20, FontFamily = "HelveticaNeue", FontAttributes = FontAttributes.Bold };
            lblHHour3 = new Label { Text = ".", VerticalOptions = LayoutOptions.Start, TextColor = Color.FromHex("#000000"), FontSize = 20, FontFamily = "HelveticaNeue", FontAttributes = FontAttributes.Bold };
            lblHHour4 = new Label { Text = ".", VerticalOptions = LayoutOptions.Start, TextColor = Color.FromHex("#000000"), FontSize = 20, FontFamily = "HelveticaNeue", FontAttributes = FontAttributes.Bold };
            lblHHour5 = new Label { Text = ".", VerticalOptions = LayoutOptions.Start, TextColor = Color.FromHex("#000000"), FontSize = 20, FontFamily = "HelveticaNeue", FontAttributes = FontAttributes.Bold };
            lblHHour6 = new Label { Text = ".", VerticalOptions = LayoutOptions.Start, TextColor = Color.FromHex("#000000"), FontSize = 20, FontFamily = "HelveticaNeue", FontAttributes = FontAttributes.Bold };
            lblAmPm1 = new Label { VerticalOptions = LayoutOptions.End, TextColor = Color.FromHex("#000000"), FontSize = 12, FontFamily = "HelveticaNeue" };
            lblAmPm2 = new Label { VerticalOptions = LayoutOptions.End, TextColor = Color.FromHex("#000000"), FontSize = 12, FontFamily = "HelveticaNeue" };
            lblAmPm3 = new Label { VerticalOptions = LayoutOptions.End, TextColor = Color.FromHex("#000000"), FontSize = 12, FontFamily = "HelveticaNeue" };
            lblAmPm4 = new Label { VerticalOptions = LayoutOptions.End, TextColor = Color.FromHex("#000000"), FontSize = 12, FontFamily = "HelveticaNeue" };
            lblAmPm5 = new Label { VerticalOptions = LayoutOptions.End, TextColor = Color.FromHex("#000000"), FontSize = 12, FontFamily = "HelveticaNeue" };
            lblAmPm6 = new Label { VerticalOptions = LayoutOptions.End, TextColor = Color.FromHex("#000000"), FontSize = 12, FontFamily = "HelveticaNeue" };
            this.Children.Add(lblHour1);
            this.Children.Add(lblHour2);
            this.Children.Add(lblHour3);
            this.Children.Add(lblHour4);
            this.Children.Add(lblHour5);
            this.Children.Add(lblHour6);

            this.Children.Add(lblHHour1);
            this.Children.Add(lblHHour2);
            this.Children.Add(lblHHour3);
            this.Children.Add(lblHHour4);
            this.Children.Add(lblHHour5);
            this.Children.Add(lblHHour6);

            this.Children.Add(lblAmPm1);
            this.Children.Add(lblAmPm2);
            this.Children.Add(lblAmPm3);
            this.Children.Add(lblAmPm4);
            this.Children.Add(lblAmPm5);
            this.Children.Add(lblAmPm6);

            // Left Button
            _leftButton = new RangeSliderThumb() { Text = this.LeftValue.ToString("00") };

            AbsoluteLayout.SetLayoutFlags(_leftButton, AbsoluteLayoutFlags.None);

            var leftButtonPanGesture = new PanGestureRecognizer();

            leftButtonPanGesture.PanUpdated += LeftButtonPanGesture;

            _leftButton.GestureRecognizers.Add(leftButtonPanGesture);
            _leftButton.SetBinding(RangeSliderThumb.SourceProperty, new Binding(path: "HandleImageSource", source: this));
            _leftButton.SetBinding(RangeSliderThumb.OpacityProperty, new Binding(path: "ThumbImageOpacity", source: this));
            _leftButton.SetBinding(RangeSliderThumb.FontFamilyProperty, new Binding(path: "ThumbFontFamily", source: this));
            _leftButton.SetBinding(RangeSliderThumb.FontSizeProperty, new Binding(path: "ThumbFontSize", source: this));
            _leftButton.SetBinding(RangeSliderThumb.TextColorProperty, new Binding(path: "ThumbTextColor", source: this));
            this.Children.Add(_leftButton);

            // Right Button
            _rightButton = new RangeSliderThumb() { Text = this.RightValue.ToString("00") };

            AbsoluteLayout.SetLayoutFlags(_rightButton, AbsoluteLayoutFlags.None);

            var rightButtonPanGesture = new PanGestureRecognizer();

            rightButtonPanGesture.PanUpdated += RightButtonPanGesture;

            _rightButton.GestureRecognizers.Add(rightButtonPanGesture);

            _rightButton.SetBinding(RangeSliderThumb.SourceProperty, new Binding(path: "HandleImageSource", source: this));
            _rightButton.SetBinding(RangeSliderThumb.OpacityProperty, new Binding(path: "ThumbImageOpacity", source: this));
            _rightButton.SetBinding(RangeSliderThumb.FontFamilyProperty, new Binding(path: "ThumbFontFamily", source: this));
            _rightButton.SetBinding(RangeSliderThumb.FontSizeProperty, new Binding(path: "ThumbFontSize", source: this));
            _rightButton.SetBinding(RangeSliderThumb.TextColorProperty, new Binding(path: "ThumbTextColor", source: this));
            this.Children.Add(_rightButton);

            // Left Bubble
            _leftBubble = new RangeSliderBubble() { IsVisible = this.ShowBubbles, Text = this.LeftValue.ToString("#0") };

            AbsoluteLayout.SetLayoutFlags(_leftBubble, AbsoluteLayoutFlags.None);

            _leftBubble.SetBinding(RangeSliderBubble.SourceProperty, new Binding(path: "BubbleImageSource", source: this));
            _leftBubble.SetBinding(RangeSliderBubble.FontFamilyProperty, new Binding(path: "FontFamily", source: this));
            _leftBubble.SetBinding(RangeSliderBubble.FontSizeProperty, new Binding(path: "FontSize", source: this));
            _leftBubble.SetBinding(RangeSliderBubble.TextColorProperty, new Binding(path: "TextColor", source: this));

            this.Children.Add(_leftBubble);

            // Right Bubble
            _rightBubble = new RangeSliderBubble() { IsVisible = this.ShowBubbles, Text = this.RightValue.ToString("#0") };

            AbsoluteLayout.SetLayoutFlags(_rightBubble, AbsoluteLayoutFlags.None);

            _rightBubble.SetBinding(RangeSliderBubble.SourceProperty, new Binding(path: "BubbleImageSource", source: this));
            _rightBubble.SetBinding(RangeSliderBubble.FontFamilyProperty, new Binding(path: "FontFamily", source: this));
            _rightBubble.SetBinding(RangeSliderBubble.FontSizeProperty, new Binding(path: "FontSize", source: this));
            _rightBubble.SetBinding(RangeSliderBubble.TextColorProperty, new Binding(path: "TextColor", source: this));

            this.Children.Add(_rightBubble);
            // Right arrow button
            _nextButton = new Image { HorizontalOptions = LayoutOptions.End, VerticalOptions = LayoutOptions.End };
            _nextButton.SetBinding(Image.SourceProperty, new Binding(path: "NextImageSource", source: this));
            var nextButtonPanGesture = new TapGestureRecognizer();

            nextButtonPanGesture.Tapped += NextButtonPanGesture_Tapped;

            _nextButton.GestureRecognizers.Add(nextButtonPanGesture);
            this.Children.Add(_nextButton);
        }





        #endregion

        // ------------------------------------------------------------

        #region Overrides

        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);

            _viewSize = new Size(width, height);

            UpdateViews();

        }

        protected override void OnPropertyChanged(string propertyName)
        {
            base.OnPropertyChanged(propertyName);

            if (propertyName == RangeSliderView.MinValueProperty.PropertyName ||
                propertyName == RangeSliderView.MaxValueProperty.PropertyName ||
                propertyName == RangeSliderView.LeftValueProperty.PropertyName ||
                propertyName == RangeSliderView.RightValueProperty.PropertyName ||
                propertyName == RangeSliderView.StepProperty.PropertyName)
            {
                UpdateViews();
            }

            if (propertyName == RangeSliderView.LeftValueProperty.PropertyName)
            {
                NotifyLeftValueChanged();
            }

            if (propertyName == RangeSliderView.RightValueProperty.PropertyName)
            {
                NotifyRightValueChanged();
            }

            if (propertyName == RangeSliderView.ShowBubblesProperty.PropertyName)
            {
                _leftBubble.IsVisible = this.ShowBubbles;
                _rightBubble.IsVisible = this.ShowBubbles;
            }

        }


        #endregion

        // ------------------------------------------------------------

        #region Events

        public event EventHandler<EventArgs<double>> LeftValueChanged;
        public event EventHandler<EventArgs<double>> RightValueChanged;
        public event EventHandler<EventArgs> ValueChangeCompleted;

        #endregion

        // ------------------------------------------------------------

        #region Commands

        public static readonly BindableProperty LeftValueChangedCommandProperty = BindableProperty.Create(nameof(LeftValueChangedCommand), typeof(ICommand), typeof(RangeSliderView), null);
        public ICommand LeftValueChangedCommand
        {
            get { return (ICommand)GetValue(LeftValueChangedCommandProperty); }
            set { SetValue(LeftValueChangedCommandProperty, value); }
        }

        public static readonly BindableProperty RightValueChangedCommandProperty = BindableProperty.Create(nameof(RightValueChangedCommand), typeof(ICommand), typeof(RangeSliderView), null);
        public ICommand RightValueChangedCommand
        {
            get { return (ICommand)GetValue(RightValueChangedCommandProperty); }
            set { SetValue(RightValueChangedCommandProperty, value); }
        }

        public static readonly BindableProperty ValueChangeCompletedCommandProperty = BindableProperty.Create(nameof(ValueChangeCompletedCommand), typeof(ICommand), typeof(RangeSliderView), null);
        public ICommand ValueChangeCompletedCommand
        {
            get { return (ICommand)GetValue(ValueChangeCompletedCommandProperty); }
            set { SetValue(ValueChangeCompletedCommandProperty, value); }
        }

        #endregion

        // ------------------------------------------------------------

        #region Public Properties

        [TypeConverter(typeof(ImageSourceConverter))]
        public static readonly BindableProperty BubbleImageSourceProperty = BindableProperty.Create(nameof(BubbleImageSource), typeof(ImageSource), typeof(RangeSliderView), null);
        public ImageSource BubbleImageSource
        {
            get { return (ImageSource)GetValue(BubbleImageSourceProperty); }
            set { SetValue(BubbleImageSourceProperty, value); }
        }

        [TypeConverter(typeof(ImageSourceConverter))]
        public static readonly BindableProperty HandleImageSourceProperty = BindableProperty.Create(nameof(HandleImageSource), typeof(ImageSource), typeof(RangeSliderView), null);
        public ImageSource HandleImageSource
        {
            get { return (ImageSource)GetValue(HandleImageSourceProperty); }
            set { SetValue(HandleImageSourceProperty, value); }
        }
        [TypeConverter(typeof(ImageSourceConverter))]
        public static readonly BindableProperty NextImageSourceProperty = BindableProperty.Create(nameof(NextImageSource), typeof(ImageSource), typeof(RangeSliderView), null);
        public ImageSource NextImageSource
        {
            get { return (ImageSource)GetValue(NextImageSourceProperty); }
            set { SetValue(NextImageSourceProperty, value); }
        }
        [TypeConverter(typeof(ImageSourceConverter))]
        public static readonly BindableProperty PreImageSourceProperty = BindableProperty.Create(nameof(PrevImageSource), typeof(ImageSource), typeof(RangeSliderView), null);
        public ImageSource PrevImageSource
        {
            get { return (ImageSource)GetValue(PreImageSourceProperty); }
            set { SetValue(PreImageSourceProperty, value); }
        }
        public static readonly BindableProperty ActiveBarColorProperty = BindableProperty.Create(nameof(ActiveBarColor), typeof(Color), typeof(RangeSliderView), Color.Black);
        public Color ActiveBarColor
        {
            get { return (Color)GetValue(ActiveBarColorProperty); }
            set { SetValue(ActiveBarColorProperty, value); }
        }
        public static readonly BindableProperty ActiveBarOpacityProperty = BindableProperty.Create(nameof(ActiveBarOpacity), typeof(double), typeof(RangeSliderView), 1.0);
        public double ActiveBarOpacity
        {
            get { return (double)GetValue(ActiveBarOpacityProperty); }
            set { SetValue(ActiveBarOpacityProperty, value); }
        }

        public static readonly BindableProperty InactiveBarColorProperty = BindableProperty.Create(nameof(InactiveBarColor), typeof(Color), typeof(RangeSliderView), Color.Black);
        public Color InactiveBarColor
        {
            get { return (Color)GetValue(InactiveBarColorProperty); }
            set { SetValue(InactiveBarColorProperty, value); }
        }
        public static readonly BindableProperty InactiveBarOpacityProperty = BindableProperty.Create(nameof(InactiveBarOpacity), typeof(double), typeof(RangeSliderView), 1.0);
        public double InactiveBarOpacity
        {
            get { return (double)GetValue(InactiveBarOpacityProperty); }
            set { SetValue(InactiveBarOpacityProperty, value); }
        }

        public static readonly BindableProperty FontFamilyProperty = BindableProperty.Create(nameof(FontFamily), typeof(string), typeof(RangeSliderView), string.Empty);
        public string FontFamily
        {
            get { return (string)GetValue(FontFamilyProperty); }
            set { SetValue(FontFamilyProperty, value); }
        }

        public static BindableProperty FontSizeProperty = BindableProperty.Create(nameof(FontSize), typeof(double), typeof(RangeSliderView), 10.0);
        public double FontSize
        {
            get { return (double)GetValue(FontSizeProperty); }
            set { SetValue(FontSizeProperty, value); }
        }

        public static BindableProperty TextColorProperty = BindableProperty.Create(nameof(TextColor), typeof(Color), typeof(RangeSliderView), Color.Black);
        public Color TextColor
        {
            get { return (Color)GetValue(TextColorProperty); }
            set { SetValue(TextColorProperty, value); }
        }

        public static readonly BindableProperty MinValueProperty = BindableProperty.Create(nameof(MinValue), typeof(double), typeof(RangeSliderView), 0.0);
        public double MinValue
        {
            get { return (double)GetValue(MinValueProperty); }
            set { SetValue(MinValueProperty, value); }
        }

        public static readonly BindableProperty MaxValueProperty = BindableProperty.Create(nameof(MaxValue), typeof(double), typeof(RangeSliderView), 0.0);
        public double MaxValue
        {
            get { return (double)GetValue(MaxValueProperty); }
            set { SetValue(MaxValueProperty, value); }
        }

        public static readonly BindableProperty LeftValueProperty = BindableProperty.Create(nameof(LeftValue), typeof(double), typeof(RangeSliderView), 0.0);
        public double LeftValue
        {
            get { return (double)GetValue(LeftValueProperty); }
            set { SetValue(LeftValueProperty, value); }
        }

        public static readonly BindableProperty RightValueProperty = BindableProperty.Create(nameof(RightValue), typeof(double), typeof(RangeSliderView), 0.0);
        public double RightValue
        {
            get { return (double)GetValue(RightValueProperty); }
            set { SetValue(RightValueProperty, value); }
        }

        public static readonly BindableProperty StepProperty = BindableProperty.Create(nameof(Step), typeof(double), typeof(RangeSliderView), 0.0);
        public double Step
        {
            get { return (double)GetValue(StepProperty); }
            set { SetValue(StepProperty, value); }
        }

        public static readonly BindableProperty ShowBubblesProperty = BindableProperty.Create(nameof(ShowBubbles), typeof(bool), typeof(RangeSliderView), false);
        public bool ShowBubbles
        {
            get { return (bool)GetValue(ShowBubblesProperty); }
            set { SetValue(ShowBubblesProperty, value); }
        }

        public static readonly BindableProperty ThumbImageOpacityProperty = BindableProperty.Create(nameof(ThumbImageOpacity), typeof(double), typeof(RangeSliderView), 1.0);
        public double ThumbImageOpacity
        {
            get { return (double)GetValue(ThumbImageOpacityProperty); }
            set { SetValue(ThumbImageOpacityProperty, value); }
        }
        public static readonly BindableProperty ThumbFontFamilyProperty = BindableProperty.Create(nameof(ThumbFontFamily), typeof(string), typeof(RangeSliderView), string.Empty);
        public string ThumbFontFamily
        {
            get { return (string)GetValue(ThumbFontFamilyProperty); }
            set { SetValue(ThumbFontFamilyProperty, value); }
        }

        public static BindableProperty ThumbFontSizeProperty = BindableProperty.Create(nameof(ThumbFontSize), typeof(double), typeof(RangeSliderView), 10.0);
        public double ThumbFontSize
        {
            get { return (double)GetValue(ThumbFontSizeProperty); }
            set { SetValue(ThumbFontSizeProperty, value); }
        }

        public static BindableProperty ThumbTextColorProperty = BindableProperty.Create(nameof(ThumbTextColor), typeof(Color), typeof(RangeSliderView), Color.Black);
        public Color ThumbTextColor
        {
            get { return (Color)GetValue(ThumbTextColorProperty); }
            set { SetValue(ThumbTextColorProperty, value); }
        }
        #endregion

        // ------------------------------------------------------------

        #region Private Methods

        private void NotifyLeftValueChanged()
        {
            _leftBubble.Text = this.LeftValue.ToString("0#"); //TODO: Make formatting configurable?
            _leftButton._label.FontSize = 14;
            _leftButton._label.FontAttributes = FontAttributes.None;
            if (this.LeftValue % 1 == 0)
            {
                _leftButton.Text = TimeBlock[Convert.ToInt32(this.LeftValue)].ToString("00");// this.LeftValue.ToString("0.##");

                AbsoluteLayout.SetLayoutFlags(_leftButton._label, AbsoluteLayoutFlags.All);
                AbsoluteLayout.SetLayoutBounds(_leftButton._label, new Rectangle(0.5f, 0.25f, 0.8f, 0.5f));
            }
            else
            {
                _leftButton.Text = ".";

                AbsoluteLayout.SetLayoutFlags(_leftButton._label, AbsoluteLayoutFlags.All);
                AbsoluteLayout.SetLayoutBounds(_leftButton._label, new Rectangle(0.5f, -1f, 1.2f, 0.8f));
                _leftButton._label.FontSize = 20;
                _leftButton._label.FontAttributes = FontAttributes.Bold;
            }
            if (this.LeftValueChanged != null)
            {
                this.LeftValueChanged(this, new EventArgs<double>(this.LeftValue));
            }

            if (this.LeftValueChangedCommand != null && this.LeftValueChangedCommand.CanExecute(this.LeftValue))
            {
                this.LeftValueChangedCommand.Execute(this.LeftValue);
            }
        }

        private void NotifyRightValueChanged()
        {
            _rightBubble.Text = this.RightValue.ToString("#0"); //TODO: Make formatting configurable?

            if (this.RightValue % 1 == 0)
            {
                _rightButton.Text = TimeBlock[Convert.ToInt32(this.RightValue)].ToString("00");// this.RightValue.ToString("0.##");
                _rightButton._label.FontSize = 14;
                _rightButton._label.FontAttributes = FontAttributes.None;
                AbsoluteLayout.SetLayoutFlags(_rightButton._label, AbsoluteLayoutFlags.All);
                AbsoluteLayout.SetLayoutBounds(_rightButton._label, new Rectangle(0.5f, 0.25f, 0.8f, 0.5f));
            }
            else
            {
                _rightButton.Text = ".";
                _rightButton._label.FontSize = 20;
                _rightButton._label.FontAttributes = FontAttributes.Bold;
                AbsoluteLayout.SetLayoutFlags(_rightButton._label, AbsoluteLayoutFlags.All);
                AbsoluteLayout.SetLayoutBounds(_rightButton._label, new Rectangle(0.5f, -1f, 1.2f, 0.8f));
            }
            if (this.RightValueChanged != null)
            {
                this.RightValueChanged(this, new EventArgs<double>(this.RightValue));
            }

            if (this.RightValueChangedCommand != null && this.RightValueChangedCommand.CanExecute(this.RightValue))
            {
                this.RightValueChangedCommand.Execute(this.RightValue);
            }
        }

        private void ChangeValueFinished()
        {
            if (this.ValueChangeCompleted != null)
            {
                this.ValueChangeCompleted(this, new EventArgs());
            }

            if (this.ValueChangeCompletedCommand != null && this.ValueChangeCompletedCommand.CanExecute(null))
            {
                this.ValueChangeCompletedCommand.Execute(null);
            }
        }

        private void UpdateViews()
        {
            // Validation...
            if (_viewSize.Width < 1 || _viewSize.Height < 1 || this.Step < 0 || this.Step > (this.MaxValue - this.MinValue) || this.MinValue == this.MaxValue)
            {
                return;
            }
            // Calculate Total Bar Width
            var totalBarWidth = _viewSize.Width - (PADDING * 2.0);

            // Calculate Steps
            var pixelsPerStep = GetPixelsPerStep();

            // Position Left Arrow Button
            _PreButton.Layout(new Rectangle(0, ((_viewSize.Height - SLIDER_PATH_HEIGHT) + 5) / 2.0, 24, 24));
            // Position Right Arrow Button
            _nextButton.Layout(new Rectangle(_viewSize.Width - 24, ((_viewSize.Height - SLIDER_PATH_HEIGHT) + 5) / 2.0, 24, 24));

            // Position Left Button
            var leftButtonX = BUTTON_START_X + (((this.LeftValue) / this.Step) * pixelsPerStep);

            _leftButton.Layout(new Rectangle(leftButtonX, (_viewSize.Height - BUTTON_SIZE) / 2.0, BUTTON_SIZE, BUTTON_SIZE));

            // Position Right Button
            var rightButtonX = BUTTON_START_X + (((this.RightValue) / this.Step) * pixelsPerStep);

            _rightButton.Layout(new Rectangle(rightButtonX, (_viewSize.Height - BUTTON_SIZE) / 2.0, BUTTON_SIZE, BUTTON_SIZE));

            // Position Left Bubble
            var bubbleY = ((_viewSize.Height - BUTTON_SIZE) / 2.0) + BUTTON_SIZE + 5.0;

            _leftBubble.Layout(new Rectangle(leftButtonX + (BUTTON_SIZE / 2.0) - (BUBBLE_WIDTH / 2.0), bubbleY, BUBBLE_WIDTH, BUBBLE_HEIGHT));

            // Position Right Bubble
            _rightBubble.Layout(new Rectangle(rightButtonX + (BUTTON_SIZE / 2.0) - (BUBBLE_WIDTH / 2.0), bubbleY, BUBBLE_WIDTH, BUBBLE_HEIGHT));

            // Position Bars

            _activeBar.Layout(new Rectangle(leftButtonX + (BUTTON_SIZE / 2.0), (_viewSize.Height - SLIDER_PATH_HEIGHT) / 2.0, rightButtonX - leftButtonX, SLIDER_PATH_HEIGHT));
            _inActiveLeftBar.Layout(new Rectangle(PADDING, (_viewSize.Height - SLIDER_PATH_HEIGHT) / 2.0, leftButtonX - PADDING, SLIDER_PATH_HEIGHT));
            _inActiveRightBar.Layout(new Rectangle(rightButtonX + (BUTTON_SIZE / 2.0), (_viewSize.Height - SLIDER_PATH_HEIGHT) / 2.0, totalBarWidth - rightButtonX + (BUTTON_SIZE / 2.0), SLIDER_PATH_HEIGHT));

            //Position RangeStepNumbers 

            lblHour1.Layout(new Rectangle(BUTTON_START_X + 6, ((_viewSize.Height - BUTTON_SIZE) / 2.0) + 4, 20, 40));
            lblHour2.Layout(new Rectangle(BUTTON_START_X + 6 + (((1) / this.Step) * pixelsPerStep), ((_viewSize.Height - BUTTON_SIZE) / 2.0) + 4, 20, 40));
            lblHour3.Layout(new Rectangle(BUTTON_START_X + 6 + (((2) / this.Step) * pixelsPerStep), ((_viewSize.Height - BUTTON_SIZE) / 2.0) + 4, 20, 40));
            lblHour4.Layout(new Rectangle(BUTTON_START_X + 6 + (((3) / this.Step) * pixelsPerStep), ((_viewSize.Height - BUTTON_SIZE) / 2.0) + 4, 20, 40));
            lblHour5.Layout(new Rectangle(BUTTON_START_X + 6 + (((4) / this.Step) * pixelsPerStep), ((_viewSize.Height - BUTTON_SIZE) / 2.0) + 4, 20, 40));
            lblHour6.Layout(new Rectangle(BUTTON_START_X + 6 + (((5) / this.Step) * pixelsPerStep), ((_viewSize.Height - BUTTON_SIZE) / 2.0) + 4, 20, 40));

            lblHHour1.Layout(new Rectangle(BUTTON_START_X + 6 + (((0.5) / this.Step) * pixelsPerStep) + 5, ((_viewSize.Height - BUTTON_SIZE) / 2.0) - 5, 10, 40));
            lblHHour2.Layout(new Rectangle(BUTTON_START_X + 6 + (((1.5) / this.Step) * pixelsPerStep) + 5, ((_viewSize.Height - BUTTON_SIZE) / 2.0) - 5, 10, 40));
            lblHHour3.Layout(new Rectangle(BUTTON_START_X + 6 + (((2.5) / this.Step) * pixelsPerStep) + 5, ((_viewSize.Height - BUTTON_SIZE) / 2.0) - 5, 10, 40));
            lblHHour4.Layout(new Rectangle(BUTTON_START_X + 6 + (((3.5) / this.Step) * pixelsPerStep) + 5, ((_viewSize.Height - BUTTON_SIZE) / 2.0) - 5, 10, 40));
            lblHHour5.Layout(new Rectangle(BUTTON_START_X + 6 + (((4.5) / this.Step) * pixelsPerStep) + 5, ((_viewSize.Height - BUTTON_SIZE) / 2.0) - 5, 10, 40));
            lblHHour6.Layout(new Rectangle(BUTTON_START_X + 6 + (((5.5) / this.Step) * pixelsPerStep) + 5, ((_viewSize.Height - BUTTON_SIZE) / 2.0) - 5, 10, 40));

            lblAmPm1.Layout(new Rectangle(BUTTON_START_X + 6, ((_viewSize.Height - BUTTON_SIZE) / 2.0) + 28, 20, 40));
            lblAmPm2.Layout(new Rectangle(BUTTON_START_X + 6 + (((1) / this.Step) * pixelsPerStep), ((_viewSize.Height - BUTTON_SIZE) / 2.0) + 28, 20, 40));
            lblAmPm3.Layout(new Rectangle(BUTTON_START_X + 6 + (((2) / this.Step) * pixelsPerStep), ((_viewSize.Height - BUTTON_SIZE) / 2.0) + 28, 20, 40));
            lblAmPm4.Layout(new Rectangle(BUTTON_START_X + 6 + (((3) / this.Step) * pixelsPerStep), ((_viewSize.Height - BUTTON_SIZE) / 2.0) + 28, 20, 40));
            lblAmPm5.Layout(new Rectangle(BUTTON_START_X + 6 + (((4) / this.Step) * pixelsPerStep), ((_viewSize.Height - BUTTON_SIZE) / 2.0) + 28, 20, 40));
            lblAmPm6.Layout(new Rectangle(BUTTON_START_X + 6 + (((5) / this.Step) * pixelsPerStep), ((_viewSize.Height - BUTTON_SIZE) / 2.0) + 28, 20, 40));

            lblHour1.TextColor = lblAmPm1.TextColor;
            lblHour2.TextColor = Color.Black;
            lblHour3.TextColor = Color.Black;
            lblHour4.TextColor = Color.Black;
            lblHour5.TextColor = Color.Black;
            lblHour6.TextColor = Color.Black;
            lblHHour1.TextColor = Color.FromHex("#aaaaaa");
            lblHHour2.TextColor = Color.FromHex("#aaaaaa");
            lblHHour3.TextColor = Color.FromHex("#aaaaaa");
            lblHHour4.TextColor = Color.FromHex("#aaaaaa");
            lblHHour5.TextColor = Color.FromHex("#aaaaaa");
            if (IsRightDisabled)
                lblHHour6.TextColor = Color.FromHex("#cccccc");
            else
                lblHHour6.TextColor = Color.FromHex("#aaaaaa");


            for (double i = this.LeftValue; i <= this.RightValue; i = i + 0.5)
            {
                switch (i)
                {
                    case 0:
                        lblHour1.TextColor = Color.White;
                        break;
                    case 0.5:
                        lblHHour1.TextColor = Color.White;
                        break;
                    case 1:
                        lblHour2.TextColor = Color.White;
                        break;
                    case 1.5:
                        lblHHour2.TextColor = Color.White;
                        break;
                    case 2:
                        lblHour3.TextColor = Color.White;
                        break;
                    case 2.5:
                        lblHHour3.TextColor = Color.White;
                        break;
                    case 3:
                        lblHour4.TextColor = Color.White;
                        break;
                    case 3.5:
                        lblHHour4.TextColor = Color.White;
                        break;
                    case 4:
                        lblHour5.TextColor = Color.White;
                        break;
                    case 4.5:
                        lblHHour5.TextColor = Color.White;
                        break;
                    case 5:
                        lblHour6.TextColor = Color.White;
                        break;
                    case 5.5:
                        lblHHour6.TextColor = Color.White;
                        break;

                }
            }
        }

        private void PreButtonPanGesture_Tapped(object sender, EventArgs e)
        {
            if (this.IsLeftDisabled == false)
                SetStepsInHours(StartTime.AddHours(-1));
        }
        private void NextButtonPanGesture_Tapped(object sender, EventArgs e)
        {
            if (this.IsRightDisabled == false)
                SetStepsInHours(StartTime.AddHours(1));
        }
        private void LeftButtonPanGesture(object sender, PanUpdatedEventArgs e)
        {
            switch (e.StatusType)
            {
                case GestureStatus.Started:
                    _leftButtonPanGesturePosX = _leftButton.X;
                    break;

                case GestureStatus.Running:
                    if (Device.RuntimePlatform == Device.Android)
                    {
                        _leftButtonPanGesturePosX = _leftButton.X;
                    }

                    this.LeftValue = AdjustLeftValue(((_leftButtonPanGesturePosX + e.TotalX - BUTTON_START_X) / GetPixelsPerStep()) * this.Step);
                    break;

                case GestureStatus.Completed:
                    _leftButtonPanGesturePosX = 0.0;
                    ChangeValueFinished();
                    break;
            }
        }

        private void RightButtonPanGesture(object sender, PanUpdatedEventArgs e)
        {
            switch (e.StatusType)
            {
                case GestureStatus.Started:
                    _rightButtonPanGesturePosX = _rightButton.X;
                    break;

                case GestureStatus.Running:
                    if (Device.RuntimePlatform == Device.Android)
                    {
                        _rightButtonPanGesturePosX = _rightButton.X;
                    }

                    this.RightValue = AdjustRightValue(((_rightButtonPanGesturePosX + e.TotalX - BUTTON_START_X) / GetPixelsPerStep()) * this.Step);
                    break;

                case GestureStatus.Completed:
                    _rightButtonPanGesturePosX = 0.0;
                    ChangeValueFinished();
                    break;
            }
        }

        private double GetPixelsPerStep()
        {
            return (_viewSize.Width - (PADDING * 2.0)) / (((this.MaxValue + pixelPos) - this.MinValue) / this.Step);
        }

        private double AdjustLeftValue(double leftValue)
        {
            var adjustedValue = leftValue;

            if (leftValue < this.MinValue)
            {
                adjustedValue = this.MinValue;
            }

            if (leftValue >= this.RightValue - this.Step)
            {
                adjustedValue = this.RightValue - this.Step;
            }
            return Math.Round(adjustedValue / this.Step) * this.Step;
        }

        private double AdjustRightValue(double rightValue)
        {
            var adjustedValue = rightValue;

            if (rightValue > this.MaxValue)
            {
                adjustedValue = this.MaxValue;
            }

            if (rightValue < this.LeftValue + this.Step)
            {
                adjustedValue = this.LeftValue + this.Step;
            }

            return Math.Round(adjustedValue / this.Step) * this.Step;
        }

        #endregion
        #region Public Methods
        public static DateTime RoundUp(DateTime dt, TimeSpan d)
        {
            return new DateTime(((dt.Ticks + d.Ticks - 1) / d.Ticks) * d.Ticks);
        }
        public void SetStepsInHours(DateTime Time)
        {
            this.MinValue = 0;
            this.MaxValue = 5.5;
            this.Step = 0.5;
            this.LeftValue = 0;
            this.RightValue = 1;
            this.CurrentTime = RoundUp(DateTime.Now, TimeSpan.FromMinutes(30));
            this.StartTime = RoundUp(Time, TimeSpan.FromMinutes(30));
            DateTime Endtime = StartTime.AddHours(5).AddMinutes(30);
            TimeSpan differenceInHour = this.StartTime - this.CurrentTime;
            DateTime TempHour = StartTime.AddHours(5).AddMinutes(30);
            if (StartTime.Date == this.CurrentTime.Date && StartTime.Minute > 0 && differenceInHour.Hours <= 0)
            {
                IsLeftDisabled = true;
                lblHour1.TextColor = Color.FromHex("#cccccc");
                lblAmPm1.TextColor = Color.FromHex("#cccccc");
            }
            else if (StartTime.Date == this.CurrentTime.Date && differenceInHour.Hours <= 0)
            {
                IsLeftDisabled = true;
            }
            else
            {
                IsLeftDisabled = false;
                lblAmPm1.TextColor = Color.Black;
                lblAmPm1.TextColor = Color.Black;
            }
            if (TempHour.Date > StartTime.Date && TempHour.Minute > 0)
            {
                IsRightDisabled = true;
                lblHour6.TextColor = Color.Black;

            }
            else
            {
                IsRightDisabled = false;
                lblHour6.TextColor = Color.FromHex("#cccccc");
            }
            if (StartTime == this.CurrentTime)
            {
                if (StartTime.Minute > 0)
                {
                    this.MinValue = 0.5;
                    this.LeftValue = this.MinValue;
                    this.RightValue = this.MinValue + 1;
                    this.pixelPos = 0.5;
                }
                else
                {
                    this.LeftValue = 0;
                    this.RightValue = 1;
                    this.MinValue = 0;
                    this.MaxValue = 5.5;
                    this.pixelPos = 0;
                }
            }
            else
            {
                this.MinValue = 0;
                this.pixelPos = 0;
            }
            //if (this.StartTime.AddHours(this.MaxValue) > Endtime)
            //{
            //    IsRightDisabled = true;
            //    lblHHour1.TextColor= Color.FromHex("#cccccc");
            //    this.MaxValue = 5;
            //    this.pixelPos = this.pixelPos+ 0.5;
            //}
            //else
            //{
            //    IsRightDisabled = false;
            //    lblHHour6.TextColor = Color.FromHex("#aaaaaa");
            //    this.MaxValue = 5.5;
            //}

            int IndexIncrementer = 0;
            TimeBlock = new double[Convert.ToInt32(this.MaxValue * 2)];
            for (double i = 0; i <= this.MaxValue; i++)
            {
                TimeBlock[IndexIncrementer] = ((StartTime.AddHours(IndexIncrementer).Hour + 11) % 12) + 1;
                TempHour = TempHour.AddHours(IndexIncrementer);
                if (i == 0 && StartTime.AddHours(IndexIncrementer).Hour == 0)
                {
                    IsLeftDisabled = true;
                    //lblHour1.TextColor = Color.FromHex("#cccccc");
                    //lblAmPm1.TextColor = Color.FromHex("#cccccc");
                }
                else if (StartTime.AddHours(IndexIncrementer).Hour == 0)
                {
                    IsRightDisabled = true;
                    lblHHour6.TextColor = Color.FromHex("#cccccc");
                    this.MaxValue = 5;
                    this.pixelPos = this.pixelPos + 0.5;
                }
                IndexIncrementer++;
            }
            if (this.LeftValue % 1 == 0)
            {
                _leftButton.Text = TimeBlock[Convert.ToInt32(this.LeftValue)].ToString("00");
            }
            else
            {
                _leftButton.Text = ".";
            }
            if (this.RightValue % 1 == 0)
            {
                _rightButton.Text = TimeBlock[Convert.ToInt32(this.RightValue)].ToString("00");
            }
            else
            {
                _rightButton.Text = ".";
            }


            lblHour1.Text = TimeBlock[0].ToString("00");
            lblHour2.Text = TimeBlock[1].ToString("00");
            lblHour3.Text = TimeBlock[2].ToString("00");
            lblHour4.Text = TimeBlock[3].ToString("00");
            lblHour5.Text = TimeBlock[4].ToString("00");
            lblHour6.Text = TimeBlock[5].ToString("00");
            lblAmPm1.Text = StartTime.AddHours(0).ToString("tt");
            lblAmPm2.Text = StartTime.AddHours(1).ToString("tt");
            lblAmPm3.Text = StartTime.AddHours(2).ToString("tt");
            lblAmPm4.Text = StartTime.AddHours(3).ToString("tt");
            lblAmPm5.Text = StartTime.AddHours(4).ToString("tt");
            lblAmPm6.Text = StartTime.AddHours(5).ToString("tt");

            ChangeValueFinished();

        }
        //public void SetStepsInHours(DateTime StartTime)
        //{
        //    this.StartTime = StartTime;
        //    DateTime Endtime = StartTime.Date.AddDays(1);
        //    TimeSpan differenceInHour = this.StartTime - this.CurrentTime;
        //    if (StartTime.Date == this.CurrentTime.Date && StartTime.Minute > 0 && differenceInHour.Hours <= 0)
        //    {
        //        Leftlimit(false);
        //        //IsLeftDisabled = true;
        //        //lblHour1.TextColor = Color.FromHex("#cccccc");
        //        //lblAmPm1.TextColor = Color.FromHex("#cccccc");
        //    }
        //    else if (StartTime.Date == this.CurrentTime.Date && differenceInHour.Hours <= 0)
        //    {
        //        IsLeftDisabled = true;
        //    }
        //    else
        //    {
        //        Leftlimit(true);
        //        //IsLeftDisabled = false;
        //        //lblAmPm1.TextColor = Color.Black;
        //    }

        //    if (StartTime == this.CurrentTime)
        //    {
        //        if (StartTime.Minute > 0)
        //        {
        //            this.MinValue = 0.5;
        //            this.LeftValue = this.MinValue;
        //            this.RightValue = this.MinValue + 1;
        //            this.pixelPos = 0.5;
        //        }
        //        else
        //        {
        //            this.LeftValue = 0;
        //            this.RightValue = 1;
        //            this.MinValue = 0;
        //            this.MaxValue = 5.5;
        //            this.pixelPos = 0;
        //        }
        //    }
        //    else 
        //    {
        //        this.MinValue = 0;
        //        this.pixelPos = 0;
        //    }


        //    //if (this.StartTime.AddHours(this.MaxValue) > Endtime)
        //    //{
        //    //   IsRightDisabled = true;
        //    //    lblHHour1.TextColor = Color.FromHex("#cccccc");
        //    //    this.MaxValue = 5;
        //    //    this.pixelPos = this.pixelPos + 0.5;
        //    //}
        //    //else
        //    //{
        //     //   IsRightDisabled = false;
        //    //    lblHHour6.TextColor = Color.FromHex("#aaaaaa");
        //    //    this.MaxValue = 5.5;
        //    //}

        //    int IndexIncrementer = 0;
        //    TimeBlock = new double[Convert.ToInt32(this.MaxValue * 2)];
        //    for (double i = 0; i <= this.MaxValue; i++)
        //    {
        //        TimeBlock[IndexIncrementer] = ((StartTime.AddHours(IndexIncrementer).Hour + 11) % 12) + 1;
        //        IndexIncrementer++;
        //    }
        //    if (this.LeftValue % 1 == 0)
        //    {
        //        _leftButton.Text = TimeBlock[Convert.ToInt32(this.LeftValue)].ToString("00");
        //    }
        //    else
        //    {
        //        _leftButton.Text = ".";
        //    }
        //    if (this.RightValue % 1 == 0)
        //    {
        //        _rightButton.Text = TimeBlock[Convert.ToInt32(this.RightValue)].ToString("00");
        //    }
        //    else
        //    {
        //        _rightButton.Text = ".";
        //    }


        //    lblHour1.Text = TimeBlock[0].ToString("00");
        //    lblHour2.Text = TimeBlock[1].ToString("00");
        //    lblHour3.Text = TimeBlock[2].ToString("00");
        //    lblHour4.Text = TimeBlock[3].ToString("00");
        //    lblHour5.Text = TimeBlock[4].ToString("00");
        //    lblHour6.Text = TimeBlock[5].ToString("00");
        //    lblAmPm1.Text = StartTime.AddHours(0).ToString("tt");
        //    lblAmPm2.Text = StartTime.AddHours(1).ToString("tt");
        //    lblAmPm3.Text = StartTime.AddHours(2).ToString("tt");
        //    lblAmPm4.Text = StartTime.AddHours(3).ToString("tt");
        //    lblAmPm5.Text = StartTime.AddHours(4).ToString("tt");
        //    lblAmPm6.Text = StartTime.AddHours(5).ToString("tt");



        //}

        #endregion Public Methods
    }

    public class RangeSliderBubble : AbsoluteLayout
    {
        private Image _image;
        private Label _label;

        public RangeSliderBubble()
        {
            // Init
            this.BackgroundColor = Color.Transparent;

            // Image
            _image = new Image();

            AbsoluteLayout.SetLayoutFlags(_image, AbsoluteLayoutFlags.All);
            AbsoluteLayout.SetLayoutBounds(_image, new Rectangle(0f, 0f, 1f, 1f));

            _image.SetBinding(Image.SourceProperty, new Binding(path: "Source", source: this));

            this.Children.Add(_image);

            // Label
            _label = new Label() { VerticalTextAlignment = TextAlignment.Center, HorizontalTextAlignment = TextAlignment.Center };

            AbsoluteLayout.SetLayoutFlags(_label, AbsoluteLayoutFlags.All);
            AbsoluteLayout.SetLayoutBounds(_label, new Rectangle(0.5f, 0.75f, 0.8f, 0.5f));

            _label.SetBinding(Label.TextProperty, new Binding(path: "Text", source: this));
            _label.SetBinding(Label.FontFamilyProperty, new Binding(path: "FontFamily", source: this));
            _label.SetBinding(Label.FontSizeProperty, new Binding(path: "FontSize", source: this));
            _label.SetBinding(Label.TextColorProperty, new Binding(path: "TextColor", source: this));

            this.Children.Add(_label);
        }

        [TypeConverter(typeof(ImageSourceConverter))]
        public static readonly BindableProperty SourceProperty = BindableProperty.Create(nameof(Source), typeof(ImageSource), typeof(RangeSliderBubble), null);
        public ImageSource Source
        {
            get { return (ImageSource)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }

        public static readonly BindableProperty TextProperty = BindableProperty.Create(nameof(Text), typeof(string), typeof(RangeSliderBubble), string.Empty);
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly BindableProperty FontFamilyProperty = BindableProperty.Create(nameof(FontFamily), typeof(string), typeof(RangeSliderBubble), string.Empty);
        public string FontFamily
        {
            get { return (string)GetValue(FontFamilyProperty); }
            set { SetValue(FontFamilyProperty, value); }
        }

        public static BindableProperty FontSizeProperty = BindableProperty.Create(nameof(FontSize), typeof(double), typeof(RangeSliderBubble), 10.0);
        public double FontSize
        {
            get { return (double)GetValue(FontSizeProperty); }
            set { SetValue(FontSizeProperty, value); }
        }

        public static BindableProperty TextColorProperty = BindableProperty.Create(nameof(TextColor), typeof(Color), typeof(RangeSliderBubble), Color.Black);
        public Color TextColor
        {
            get { return (Color)GetValue(TextColorProperty); }
            set { SetValue(TextColorProperty, value); }
        }
    }

    public class RangeSliderThumb : AbsoluteLayout
    {
        private Image _image;
        public Label _label;

        public RangeSliderThumb()
        {
            // Init
            this.BackgroundColor = Color.Transparent;

            // Image
            _image = new Image();

            AbsoluteLayout.SetLayoutFlags(_image, AbsoluteLayoutFlags.All);
            AbsoluteLayout.SetLayoutBounds(_image, new Rectangle(0f, 0f, 1f, 1f));

            _image.SetBinding(Image.SourceProperty, new Binding(path: "Source", source: this));
            _image.SetBinding(Image.OpacityProperty, new Binding(path: "ThumbImageOpacity", source: this));

            this.Children.Add(_image);

            // Label
            _label = new Label() { VerticalTextAlignment = TextAlignment.Center, HorizontalTextAlignment = TextAlignment.Center };

            AbsoluteLayout.SetLayoutFlags(_label, AbsoluteLayoutFlags.All);
            AbsoluteLayout.SetLayoutBounds(_label, new Rectangle(0.5f, 0.25f, 0.8f, 0.5f));

            _label.SetBinding(Label.TextProperty, new Binding(path: "Text", source: this));
            _label.SetBinding(Label.FontFamilyProperty, new Binding(path: "ThumbFontFamily", source: this));
            _label.SetBinding(Label.FontSizeProperty, new Binding(path: "ThumbFontSize", source: this));
            _label.SetBinding(Label.TextColorProperty, new Binding(path: "ThumbTextColor", source: this));
            this.Children.Add(_label);
        }

        [TypeConverter(typeof(ImageSourceConverter))]
        public static readonly BindableProperty SourceProperty = BindableProperty.Create(nameof(Source), typeof(ImageSource), typeof(RangeSliderThumb), null);
        public ImageSource Source
        {
            get { return (ImageSource)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }

        public static readonly BindableProperty TextProperty = BindableProperty.Create(nameof(Text), typeof(string), typeof(RangeSliderThumb), string.Empty);
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly BindableProperty FontFamilyProperty = BindableProperty.Create(nameof(ThumbFontFamily), typeof(string), typeof(RangeSliderThumb), string.Empty);
        public string ThumbFontFamily
        {
            get { return (string)GetValue(FontFamilyProperty); }
            set { SetValue(FontFamilyProperty, value); }
        }

        public static BindableProperty FontSizeProperty = BindableProperty.Create(nameof(ThumbFontSize), typeof(double), typeof(RangeSliderThumb), 10.0);
        public double ThumbFontSize
        {
            get { return (double)GetValue(FontSizeProperty); }
            set { SetValue(FontSizeProperty, value); }
        }

        public static BindableProperty TextColorProperty = BindableProperty.Create(nameof(ThumbTextColor), typeof(Color), typeof(RangeSliderThumb), Color.Black);
        public Color ThumbTextColor
        {
            get { return (Color)GetValue(TextColorProperty); }
            set { SetValue(TextColorProperty, value); }
        }
    }
}
