﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.Maps;

namespace Meelo.CustomControls
{
    public class CustomPin:Pin
    {
        public string Url { get; set; }

        public int WorkspaceCount { get; set; }

        public string BuildingId { get; set; }

    }
}
