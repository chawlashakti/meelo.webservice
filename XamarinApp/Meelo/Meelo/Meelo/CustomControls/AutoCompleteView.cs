﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Meelo.CustomControls
{
    public class AutoCompleteView : ContentView
    {
        public static readonly BindableProperty SuggestionsProperty = BindableProperty.Create(nameof(Suggestions), typeof(IEnumerable), typeof(AutoCompleteView), null, BindingMode.OneWay, null, OnSuggestionsChanged);
        public static readonly BindableProperty SearchTextProperty = BindableProperty.Create(nameof(SearchText), typeof(string), typeof(AutoCompleteView), null, BindingMode.TwoWay, null, OnSearchTextChanged);
        public static readonly BindableProperty PlaceholderProperty = BindableProperty.Create(nameof(Placeholder), typeof(string), typeof(AutoCompleteView), null, BindingMode.OneWay, null, OnPlaceholderChanged);
        public static readonly BindableProperty MaximumVisibleSuggestionItemsProperty = BindableProperty.Create(nameof(MaximumVisibleSuggestionItems), typeof(int), typeof(AutoCompleteView), 4);
        public static readonly BindableProperty SuggestionItemTemplateProperty = BindableProperty.Create(nameof(SuggestionItemTemplate), typeof(DataTemplate), typeof(AutoCompleteView), null, BindingMode.OneWay, null, OnSuggestionItemTemplateChanged);
        public static readonly BindableProperty DisplayPropertyNameProperty = BindableProperty.Create(nameof(DisplayPropertyName), typeof(string), typeof(AutoCompleteView));
        public event EventHandler<EventArgs> OnFocusChanged;
        public IEnumerable Suggestions
        {
            get { return (IEnumerable)GetValue(SuggestionsProperty); }
            set { SetValue(SuggestionsProperty, value); }
        }

        public string SearchText
        {
            get { return (string)GetValue(SearchTextProperty); }
            set { SetValue(SearchTextProperty, value); }
        }

        public string Placeholder
        {
            get { return (string)GetValue(PlaceholderProperty); }
            set { SetValue(PlaceholderProperty, value); }
        }

        public int MaximumVisibleSuggestionItems
        {
            get { return (int)GetValue(MaximumVisibleSuggestionItemsProperty); }
            set { SetValue(MaximumVisibleSuggestionItemsProperty, value); }
        }

        public DataTemplate SuggestionItemTemplate
        {
            get { return (DataTemplate)GetValue(SuggestionItemTemplateProperty); }
            set { SetValue(SuggestionItemTemplateProperty, value); }
        }

        public string DisplayPropertyName
        {
            get { return (string)GetValue(DisplayPropertyNameProperty); }
            set { SetValue(DisplayPropertyNameProperty, value); }
        }

        public ItemsStack SuggestionsListView { get; private set; }
        public CustomEntry SearchEntry { get; private set; }
        public IEnumerable OriginSuggestions { get; private set; }
        public ScrollView SuggestionWrapper { get; private set; }
        public Grid EntryContainer { get; private set; }
        public ImageButton ClearIconImage { get; private set; }
        public bool IsSelected { get; private set; }
        public int TotalNumberOfTypings { get; private set; }
        public ICommand OnClearSearchClicked { get; set; }
        private static void OnSuggestionsChanged(object bindable, object oldValue, object newValue)
        {
            var autoCompleteView = bindable as AutoCompleteView;

            var suggestions = (IEnumerable)newValue;
            autoCompleteView.OriginSuggestions = suggestions;

            suggestions = autoCompleteView.FilterSuggestions(suggestions, autoCompleteView.SearchText);
            autoCompleteView.SuggestionsListView.ItemsSource = suggestions;
        }

        private static void OnSearchTextChanged(object bindable, object oldValue, object newValue)
        {
            var autoCompleteView = bindable as AutoCompleteView;

            var suggestions = autoCompleteView.OriginSuggestions;
            if (newValue != null && newValue.ToString().Length>0)
            {
                suggestions = autoCompleteView.FilterSuggestions(suggestions, autoCompleteView.SearchText);
                // assign when initializing with data
                if (autoCompleteView.SearchEntry.Text != autoCompleteView.SearchText)
                {
                    autoCompleteView.SearchEntry.Text = autoCompleteView.SearchText;
                }
            }
            autoCompleteView.SuggestionsListView.ItemsSource = suggestions;

            if (Device.RuntimePlatform == Device.Android)
            {
                // update the layout -> only do this when user is typing instead of selection an item from suggestions list 
                // -> prevent duplicated update layout
                if (!autoCompleteView.IsSelected)
                {
                    autoCompleteView.UpdateLayout();
                }
                else
                {
                    autoCompleteView.IsSelected = false;
                }
            }
        }

        private static void OnSuggestionItemTemplateChanged(object bindable, object oldValue, object newValue)
        {
            var autoCompleteView = bindable as AutoCompleteView;

            if (autoCompleteView.SuggestionsListView != null)
            {
                autoCompleteView.SuggestionsListView.ItemTemplate = autoCompleteView.SuggestionItemTemplate;
            }
        }

        public IEnumerable FilterSuggestions(IEnumerable suggestions, string keyword)
        {
            if (string.IsNullOrEmpty(keyword) || suggestions == null) return suggestions;

            //var searchWords = keyword.ConvertToNonMark().ToLower().Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            //var result = suggestions.Cast<object>();
            //foreach (var item in searchWords)
            //{
            //    if (!string.IsNullOrEmpty(DisplayPropertyName))
            //    {
            //        result = result.Where(x => x.GetType().GetRuntimeProperty(DisplayPropertyName).GetValue(x).ToString().ConvertToNonMark().ToLower().Contains(item)).ToList();
            //    }
            //    else
            //    {
            //        result = result.Where(x => x.ToString().ConvertToNonMark().ToLower().Contains(item)).ToList();
            //    }
            //}

            return suggestions;
        }

        private static void OnPlaceholderChanged(object bindable, object oldValue, object newValue)
        {
            var autoCompleteView = bindable as AutoCompleteView;
            autoCompleteView.SearchEntry.Placeholder = newValue?.ToString();
        }

        public void UpdateLayout()
        {
            var expectedHeight = this.getExpectedHeight();
            //Container.HeightRequest = expectedHeight;
            //Container.ForceLayout();
        }

        private void SearchEntry_TextChanged(object sender, TextChangedEventArgs e)
        {
            TotalNumberOfTypings++;
            Device.StartTimer(TimeSpan.FromMilliseconds(1000), () => {
                TotalNumberOfTypings--;
                if (TotalNumberOfTypings == 0)
                {
                    SearchText = e.NewTextValue;
                }
                return false;
            });
        }

        private void SearchEntry_Focused(object sender, FocusEventArgs e)
        {
            UpdateLayout();
            IsSelected = false;
            ClearIconImage.IsVisible = true;
            if (this.OnFocusChanged != null)
            {
                this.OnFocusChanged(sender, e);
            }
        }

        private void SearchEntry_Unfocused(object sender, FocusEventArgs e)
        {
            //Container.HeightRequest = 50;
            //Container.ForceLayout();
            //ClearIconImage.IsVisible = false;
        }

        private void SuggestionsListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            IsSelected = true;
            SearchEntry.Text = !string.IsNullOrEmpty(DisplayPropertyName) ? e.SelectedItem?.GetType()?.GetRuntimeProperty(DisplayPropertyName)?.GetValue(e.SelectedItem)?.ToString() : e.SelectedItem?.ToString();
            //Container.HeightRequest = 50;
            //Container.ForceLayout();
        }

        private void OverlapContentView_Tapped(object sender, TappedEventArgs e)
        {
            UpdateLayout();
            IsSelected = false;

        }

        private int getExpectedHeight()
        {
            var items = SuggestionsListView.ItemsSource as IList;
            int wrapperHeightRequest = items != null ?
                (items.Count >= MaximumVisibleSuggestionItems ? MaximumVisibleSuggestionItems * 40 : items.Count * 40) : 0;
            if (Device.RuntimePlatform == Device.Android)
            {
                return wrapperHeightRequest + 50;
            }
            return MaximumVisibleSuggestionItems * 40 + 50;
        }

        public AutoCompleteView()
        {
            StackLayout StackContainer = new StackLayout();
            EntryContainer = new Grid();
            EntryContainer.BackgroundColor = Color.FromHex("#f8f8f8");
            SearchEntry = new CustomEntry();
            SuggestionsListView = new ItemsStack();
            SuggestionWrapper = new ScrollView();

            // init Grid Layout
            EntryContainer.RowSpacing = 0;
            EntryContainer.ColumnDefinitions.Add(new ColumnDefinition() { Width =new GridLength(18, GridUnitType.Star) });
            EntryContainer.ColumnDefinitions.Add(new ColumnDefinition() { Width =new GridLength(82, GridUnitType.Star) });
            EntryContainer.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });

            //Search Icon Stack
            StackLayout IconLayout = new StackLayout();
            IconLayout.Padding = new Thickness(15, 10, 0, 10);
            Image iconImage = new Image {
                Aspect = Aspect.AspectFill,
                BackgroundColor = Color.Transparent,
                HorizontalOptions = LayoutOptions.Start,
                VerticalOptions = LayoutOptions.Center,
                Source = ImageSource.FromFile("icon_searchfield.png")
            };
            IconLayout.Children.Add(iconImage);
            //Search Entry Stack
            StackLayout SearchEntryLayout = new StackLayout();
            SearchEntryLayout.Padding = new Thickness(0, 10, 15, 10);
            SearchEntryLayout.HorizontalOptions = LayoutOptions.FillAndExpand;
            SearchEntryLayout.VerticalOptions = LayoutOptions.FillAndExpand;
            SearchEntryLayout.Orientation =StackOrientation.Horizontal;
            // init Search Entry
            SearchEntry.HorizontalOptions = LayoutOptions.FillAndExpand;
            SearchEntry.VerticalOptions = LayoutOptions.FillAndExpand;
            SearchEntry.TextChanged += SearchEntry_TextChanged;
            SearchEntry.Unfocused += SearchEntry_Unfocused;
            SearchEntry.Focused += SearchEntry_Focused;
            SearchEntry.BackgroundColor = Color.Transparent;
            SearchEntry.FontSize = 14;
            SearchEntry.TextColor = Color.Black;

            SearchEntryLayout.Children.Add(SearchEntry);
            StackLayout ClearIconLayout = new StackLayout();
            ClearIconImage = new ImageButton
            {
                Aspect = Aspect.AspectFill,
                HorizontalOptions = LayoutOptions.End,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                Source = ImageSource.FromFile("icon_clearsearch.png"),
                IsVisible = false,
                Command = new Command(OnClear)
            };
            ClearIconLayout.Children.Add(ClearIconImage);
            SearchEntryLayout.Children.Add(ClearIconLayout);
            // init Suggestions ListView
            SuggestionsListView.BackgroundColor = Color.White;
            //SuggestionsListView.ItemTapped += SuggestionsListView_ItemSelected;
            SuggestionsListView.SelectedItemChanged += SuggestionsListView_ItemSelected;
            SuggestionsListView.VerticalOptions = LayoutOptions.End;
            SuggestionsListView.Spacing = 1;
            // suggestions Listview's wrapper
            SuggestionWrapper.VerticalOptions = LayoutOptions.Fill;
            SuggestionWrapper.Orientation = ScrollOrientation.Vertical;
            SuggestionWrapper.BackgroundColor = Color.White;
            SuggestionWrapper.Content = SuggestionsListView;
            EntryContainer.Children.Add(IconLayout, 0, 0);
            EntryContainer.Children.Add(SearchEntryLayout, 1, 0);

            StackContainer.Children.Add(EntryContainer);
            StackContainer.Children.Add(SuggestionWrapper);

            this.Content = StackContainer;
        }
        void OnClear()
        {
            Suggestions = null;
        }
    }
}
