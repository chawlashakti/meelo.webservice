﻿using Microsoft.Graph;
using Microsoft.Identity.Client;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Meelo.Helper
{
    public static class Authentication
    {
        public static string TokenForUser = null;
        public static DateTimeOffset expiration;

        private static GraphServiceClient graphClient = null;

        // Get an access token for the given context and resourceId. An attempt is first made to 
        // acquire the token silently. If that fails, then we try to acquire the token by prompting the user.
        public static GraphServiceClient GetAuthenticatedClient()
        {
            if (graphClient == null)
            {
                // Create Microsoft Graph client.
                try
                {
                    //graphClient = new GraphServiceClient(
                    // "https://graph.microsoft.com/v1.0",
                    // new DelegateAuthenticationProvider(
                    // async(requestMessage) = &gt;
                    //{
                    //    var token = await GetTokenForUserAsync();
                    //    requestMessage.Headers.Authorization = new AuthenticationHeaderValue("bearer", token);
                    //}));
                    //return graphClient;
                    graphClient = new GraphServiceClient(
                        "https://graph.microsoft.com/v1.0",
                        new DelegateAuthenticationProvider(
                            async (requestMessage) =>
                            {

                                var token = await GetTokenForUserAsync();
                                requestMessage.Headers.Authorization = new AuthenticationHeaderValue("bearer", token);
                                requestMessage.Headers.Add("SampleID", "xamarin-csharp-meetingmanager-sample");

                            }));
                    return graphClient;
                }

                catch (Exception ex)
                {
                    Debug.WriteLine(StringResources.ClientCreateFailed + ex.Message);
                }
            }

            return graphClient;
        }


        /// <summary>
        /// Get Token for User.
        /// </summary>
        /// <returns>Token for user.</returns>
        public static async Task<string> GetTokenForUserAsync()
        {
            try
            {
                if (TokenForUser == null || expiration <= DateTimeOffset.UtcNow.AddMinutes(5))
                {
                    //string ClientId = AppResources.ClientAppId;
                    //string Authority = AppResources.AuthorityLink;
                    //string ReturnUri = AppResources.ReturnURILink;
                    //string GraphResourceUri = AppResources.GraphURILink;
                    //var auth = DependencyService.Get<IAzureActiveDirectoryAuthenticator>();
                    //string adAccessToken = await auth.Authenticate(Authority, GraphResourceUri, ClientId, ReturnUri);
                    //TokenForUser = adAccessToken;
                    //expiration = App.ExpiresOn;





                    //AuthenticationResult authResult = await App.IdentityClientApp.AcquireTokenAsync(App.Scopes);
                    //AuthenticationResult authResult = await App.IdentityClientApp.AcquireTokenAsync(App.Scopes, App.UiParent);
                    AuthenticationResult authResult =
    await App.PCA.AcquireTokenSilentAsync(Constants.Scopes, App.PCA.Users.FirstOrDefault());
                    TokenForUser = authResult.AccessToken;
                    expiration = authResult.ExpiresOn;

                }
                return TokenForUser;

            }
            catch (Exception e)
            {
                Debug.WriteLine(StringResources.ClientCreateFailed + e.Message);

                return null;
            }

        }


        /// <summary>
        /// Signs the user out of the service.
        /// </summary>
        public static void SignOut()
        {
            //foreach (var user in App.IdentityClientApp.Users)
            //{
            //    user.SignOut();
            //}
            //graphClient = null;
            //TokenForUser = null;

        }

    }
}
