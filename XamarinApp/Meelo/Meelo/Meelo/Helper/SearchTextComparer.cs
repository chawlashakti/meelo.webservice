﻿using Meelo.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meelo.Helper
{
   public class SearchTextComparer: IEqualityComparer<AzureSearchResponseModel>
    {
        public bool Equals(AzureSearchResponseModel x, AzureSearchResponseModel y)
        {
            if (Object.ReferenceEquals(x, y)) return true;

            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            return x.SearchText == y.SearchText;
        }
        public int GetHashCode(AzureSearchResponseModel obj)
        {
            if (Object.ReferenceEquals(obj, null)) return 0;
            int hashText = obj.SearchText == null ? 0 : obj.SearchText.GetHashCode();
            //int hashProductCode = obj.Model.GetHashCode();
            return hashText;
        }
    }
}
