﻿using System;
using CommonServiceLocator;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using Meelo.ViewModels;
using Meelo.Views;

namespace Meelo.Helper
{
	public class ViewModelLocator
	{
		public ViewModelLocator()
		{
			//ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
			var navigationService = this.CreateNavigationService();
			SimpleIoc.Default.Register<INavigationService>(() => navigationService);
            SimpleIoc.Default.Register<IDialogService, DialogService>();
            SimpleIoc.Default.Register<LoginOptionsViewModel>();
            SimpleIoc.Default.Register<MapPageViewModel>();
            SimpleIoc.Default.Register<ListViewPageViewModel>();
            SimpleIoc.Default.Register<MeetingPageViewModel>();
            SimpleIoc.Default.Register<MorePageViewModel>();
            SimpleIoc.Default.Register<MainPageViewModel>();
            SimpleIoc.Default.Register<InvitePageViewModel>();

        }
        public LoginOptionsViewModel LoginOptionsViewModelInstance
        {
            get
            {
                return SimpleIoc.Default.GetInstance<LoginOptionsViewModel>();
            }
        }
        public MainPageViewModel MainPageViewModelInstance
        {
            get
            {
                return SimpleIoc.Default.GetInstance<MainPageViewModel>();
            }
        }
        public MapPageViewModel MapPageViewModelInstance
        {
            get
            {
                return SimpleIoc.Default.GetInstance<MapPageViewModel>();
            }
        }
        public ListViewPageViewModel ListViewPageViewModelInstance
        {
            get
            {
                return SimpleIoc.Default.GetInstance<ListViewPageViewModel>();
            }
        }
        public MeetingPageViewModel MeetingPageViewModelInstance
        {
            get
            {
                return SimpleIoc.Default.GetInstance<MeetingPageViewModel>();
            }
        }
        public MorePageViewModel MorePageViewModelInstance
        {
            get
            {
                return SimpleIoc.Default.GetInstance<MorePageViewModel>();
            }
        }
        public InvitePageViewModel InvitePageViewModelInstance
        {
            get
            {
                return SimpleIoc.Default.GetInstance<InvitePageViewModel>();
            }
        }
        /// <summary>
        /// Creates and configures the navigation service
        /// </summary>
        /// <returns>The navigation service.</returns>
        private INavigationService CreateNavigationService()
		{
			var navigationService = new NavigationService();

            navigationService.Configure(Constants.LoginOptions, typeof(LoginOptionsPage));
            navigationService.Configure(Constants.MainTabbedPage, typeof(MainPage));
            navigationService.Configure(Constants.MapPage, typeof(MapPage));
            navigationService.Configure(Constants.ListViewPage, typeof(ListViewPage));
            navigationService.Configure(Constants.MeetingPage, typeof(MeetingPage));
            navigationService.Configure(Constants.AppsPage, typeof(AppsPage));
            navigationService.Configure(Constants.MorePage, typeof(MorePage));
            navigationService.Configure(Constants.InvitePage, typeof(InvitePage));
            return navigationService;
		}
	}
}

