﻿using System;
using Xamarin.Forms;

namespace Meelo.Helper
{
	public static class Colors
	{

        public static Color MeeloBlue = Color.FromHex("#039be5");
        public static Color MeeloGray = Color.FromHex("#aaaaaa");
        public static Color MeeloSky = Color.FromHex("#4fc3fc");

    }
}