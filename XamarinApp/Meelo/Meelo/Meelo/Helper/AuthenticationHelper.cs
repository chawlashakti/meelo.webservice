﻿using Microsoft.Graph;
using Microsoft.Identity.Client;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Meelo.Helper
{
   public class AuthenticationHelper
    {
        public static string TokenForUser = null;
        public static DateTimeOffset expiration;

        private static GraphServiceClient graphClient = null;
        public static GraphServiceClient GetAuthenticatedClient()
        {
            if (graphClient == null)
            {
                // Create Microsoft Graph client.
                try
                {
                    graphClient = new GraphServiceClient(
                        "https://graph.microsoft.com/v1.0",
                        new DelegateAuthenticationProvider(
                            async (requestMessage) =>
                            {
                                var token = await GetTokenForUserSilentAsync();
                                requestMessage.Headers.Authorization = new AuthenticationHeaderValue("bearer", token);
                                // This header has been added to identify our sample in the Microsoft Graph service.  If extracting this code for your project please remove.
                                //requestMessage.Headers.Add("SampleID", "xamarin-csharp-connect-sample");

                            }));
                    return graphClient;
                }

                catch (Exception ex)
                {
                    Debug.WriteLine("Could not create a graph client: " + ex.Message);
                }
            }

            return graphClient;
        }
        public static async Task<string> GetTokenForUserAsync()
        {
            try
            {
                if (TokenForUser == null || expiration <= DateTimeOffset.UtcNow.AddMinutes(5))
                {
                    AuthenticationResult authResult = await App.PCA.AcquireTokenAsync(Constants.Scopes,App.UiParent);
                    App.AccessToken = authResult.AccessToken;
                    App.TenantId = authResult.TenantId;
                    App.UniqueId = authResult.UniqueId;
                    App.UserName = authResult.User.Name;
                    App.ExpiresOn = authResult.ExpiresOn;
                    TokenForUser = authResult.AccessToken;
                    expiration = authResult.ExpiresOn;
                    
                }
            }
            catch(Exception ex)
            { }
            return TokenForUser;
        }
        public static async Task<string> GetTokenForUserSilentAsync()
        {
            if (TokenForUser == null || expiration <= DateTimeOffset.UtcNow.AddMinutes(5))
            {
                AuthenticationResult authResult = await App.PCA.AcquireTokenSilentAsync(Constants.Scopes, App.PCA.Users.FirstOrDefault());

                TokenForUser = authResult.AccessToken;
                expiration = authResult.ExpiresOn;
            }

            return TokenForUser;
        }
        public static void SignOut()
        {
            foreach (var user in App.PCA.Users)
            {
                App.PCA.Remove(user);
            }
            graphClient = null;
            TokenForUser = null;

        }

    }
}
