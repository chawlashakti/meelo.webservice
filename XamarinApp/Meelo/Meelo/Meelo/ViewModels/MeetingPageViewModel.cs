﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using Meelo.CustomControls;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Meelo.Webservices;
using Meelo.Models;
using System.Threading.Tasks;

namespace Meelo.ViewModels
{
    public class MeetingPageViewModel : ViewModelBase
    {
        #region Members
        private readonly INavigationService _navigationService;
        private IDialogService _dialogService;
        private IUserWorkspace _workspaceService;
        public StackLayout MeetingsListContainer;
        public Event checkInEvent = new Event();
        private IReserveWorkspace _reserveService;

        private IMeetingCheckInCheckOut _checkInCheckOutService;
        #endregion Members
        #region Properties
        private bool _isEmptyMeetingsMsgVisible;
        public bool IsEmptyMeetingsMsgVisible
        {
            get { return _isEmptyMeetingsMsgVisible; }
            set
            {
                _isEmptyMeetingsMsgVisible = value;
                RaisePropertyChanged("IsEmptyMeetingsMsgVisible");
            }
        }
        private bool _isMeetingsListNotEmpty;
        public bool IsMeetingsListNotEmpty
        {
            get { return _isMeetingsListNotEmpty; }
            set
            {
                _isMeetingsListNotEmpty = value;
                RaisePropertyChanged("IsMeetingsListNotEmpty");
            }
        }
        private List<Event> _meetingsRecord = new List<Event>();
        public List<Event> MeetingRecord
        {
            get { return _meetingsRecord; }
            set
            {
                _meetingsRecord = value;
                RaisePropertyChanged("MeetingRecord");
            }
        }
        private string _checkInMeetingName;
        public string CheckInMeetingName
        {
            get { return _checkInMeetingName; }
            set
            {
                _checkInMeetingName = value;
                RaisePropertyChanged("CheckInMeetingName");
            }
        }
        private string _workspaceImage;
        public string WorkspaceImage
        {
            get { return _workspaceImage; }
            set
            {
                _workspaceImage = value;
                RaisePropertyChanged("WorkspaceImage");
            }
        }
        private string _checkInMeetingTime;
        public string CheckInMeetingTime
        {
            get { return _checkInMeetingTime; }
            set
            {
                _checkInMeetingTime = value;
                RaisePropertyChanged("CheckInMeetingTime");
            }
        }
        private bool _IsBusy;
        public bool IsBusy
        {
            get { return _IsBusy; }
            set
            {
                _IsBusy = value;
                RaisePropertyChanged("IsBusy");
            }
        }
        #endregion Properties
        #region Constructor
        public MeetingPageViewModel(INavigationService navigationService, IDialogService dialogService)
        {
            if (navigationService == null)
                throw new ArgumentNullException("navigationService");
            _navigationService = navigationService;

            if (dialogService == null)
                throw new ArgumentNullException("navigationService");
            _dialogService = dialogService;
            
        }
        #endregion Constructor
        #region Methods
        public void LoadMeeting()
        {
            _workspaceService = new UserWorksapceApi();
            GetReservedMeetingsCount();

            try
            {
                DateTimeOffset startTime = new DateTimeOffset(DateTime.UtcNow.Date);
                DateTimeOffset endTime = DateTimeOffset.UtcNow.AddDays(30);

                var meetingData = _workspaceService.GetMeetingList(startTime.ToString("O"), endTime.ToString("O")).Result;
                MeetingRecord = meetingData.Reservations.ToList();
                ResetOnMeetingsList();
                //_navigationService.NavigateTo(App.Meeting);
            }
            catch (Exception ex)
            {

            }
        }
        public void GetReservedMeetingsCount()
        { }
        public void ResetOnMeetingsList()
        {
            try
            {
                if (MeetingRecord.Count == 0)
                {
                    IsMeetingsListNotEmpty = false;
                    IsEmptyMeetingsMsgVisible = true;
                }
                else
                {
                    IsMeetingsListNotEmpty = true;
                    IsEmptyMeetingsMsgVisible = false;
                    CreateMeetingsList();
                }
            }
            catch (Exception ex)
            {

            }

        }
        public void CreateMeetingsList()
        {
            try
            {

                MeetingsListContainer.Children.Clear();
                List<List<Event>> groupedMeetingList = new List<List<Event>>();
                if (MeetingRecord.Count > 0)
                {
                    var records = MeetingRecord.OrderBy(mt => mt.StartTime.Date);
                    groupedMeetingList = records.GroupBy(u => u.StartTime.Date).Select(grp => grp.ToList()).ToList();
                }
                if (groupedMeetingList.Count > 0)
                    for (int groupCount = 0; groupCount < groupedMeetingList.Count; groupCount++)
                    {
                        var meetingsWorkspaceLayout = new StackLayout
                        {
                            VerticalOptions = LayoutOptions.StartAndExpand,
                            HorizontalOptions = LayoutOptions.StartAndExpand,
                            Orientation = StackOrientation.Vertical,
                            Padding = new Thickness(0, 5, 0, 0)
                        };
                        var workspaceDateLayout = new StackLayout
                        {
                            VerticalOptions = LayoutOptions.StartAndExpand,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            Orientation = StackOrientation.Horizontal,
                            Padding = new Thickness(15, 8, 0, 8),
                            BackgroundColor = Color.FromHex("#f8f8f8")
                        };
                        var workspaceDate = new Label
                        {
                            VerticalOptions = LayoutOptions.Center,
                            HorizontalOptions = LayoutOptions.StartAndExpand,
                            BackgroundColor = Color.FromHex("#f8f8f8"),
                            TextColor = Color.FromHex("#767676"),
                            FontSize = 12,
                            FontFamily = "HelveticaNeue",
                        };
                        workspaceDate.Text = groupedMeetingList[groupCount][0].StartTime.ToLocalTime().ToString("ddd,MMM dd");
                        //workspaceDate.Text = MeetingRecord[0].StartTime.ToLocalTime().ToString("ddd,MMM dd");
                        workspaceDateLayout.Children.Add(workspaceDate);
                        meetingsWorkspaceLayout.Children.Add(workspaceDateLayout);
                        List<Event> groupOfMeeting = new List<Event>();
                        groupOfMeeting = groupedMeetingList[groupCount];
                        Grid ComponentGrid = new Grid
                        {
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            VerticalOptions = LayoutOptions.FillAndExpand,
                        };
                        ComponentGrid.RowSpacing = 0;
                        for (int i = 0; i < groupOfMeeting.Count; i++)
                        {
                            ComponentGrid.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
                        }
                        for (int workspaceCount = 0; workspaceCount < groupOfMeeting.Count; workspaceCount++)
                        {
                            var wholeBoxLayout = new StackLayout
                            {
                                VerticalOptions = LayoutOptions.FillAndExpand,
                                HorizontalOptions = LayoutOptions.FillAndExpand,
                                Padding = new Thickness(0, 0, 0, 0),
                                Orientation = StackOrientation.Vertical,
                                HeightRequest = 130
                            };
                            var wholeLayout = new StackLayout
                            {
                                VerticalOptions = LayoutOptions.FillAndExpand,
                                HorizontalOptions = LayoutOptions.FillAndExpand,
                                Padding = new Thickness(0, 0, 0, -5),
                                Orientation = StackOrientation.Horizontal
                            };
                            var workspaceView = new ContentView
                            {
                                VerticalOptions = LayoutOptions.FillAndExpand,
                                HorizontalOptions = LayoutOptions.FillAndExpand,
                                Padding = new Thickness(0, 0, 0, 10),
                                //MinimumWidthRequest = 270
                                //StyleId = groupOfMeeting[workspaceCount].Workspace.Id.ToString()
                                StyleId = groupOfMeeting[workspaceCount].Id.ToString()
                            };
                            Grid workspaceGrid = new Grid
                            {
                                HorizontalOptions = LayoutOptions.FillAndExpand,
                                VerticalOptions = LayoutOptions.FillAndExpand,
                                InputTransparent = true,
                            };
                            workspaceGrid.RowSpacing = 0;
                            workspaceGrid.ColumnSpacing = 0;
                            workspaceGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });
                            workspaceGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });
                            var confImageLayout = new StackLayout
                            {
                                VerticalOptions = LayoutOptions.FillAndExpand,
                                HorizontalOptions = LayoutOptions.FillAndExpand,
                                Padding = new Thickness(15, 10, 20, 20),
                                WidthRequest = 100
                            };
                            var confImage = new ImageCircle
                            {
                                HorizontalOptions = LayoutOptions.Center,
                                VerticalOptions = LayoutOptions.Center,
                                Aspect = Aspect.AspectFill,
                                WidthRequest = 100,
                                HeightRequest = 100
                            };
                            confImage.IsCircle = false;
                            confImage.Source = groupOfMeeting[workspaceCount].Workspace.PhotoLinks;
                            //confImage.Source = MeetingRecord[workspaceCount].Workspace.PhotoLinks;
                            confImageLayout.Children.Add(confImage);
                            workspaceGrid.Children.Add(confImageLayout, 0, 0);
                            Grid GridRow2ndColum = new Grid
                            {
                                HorizontalOptions = LayoutOptions.FillAndExpand,
                                VerticalOptions = LayoutOptions.FillAndExpand,
                                Padding = new Thickness(10, 10, 20, 0),
                                //MinimumWidthRequest = 160
                            };
                            GridRow2ndColum.RowSpacing = 0;
                            GridRow2ndColum.ColumnSpacing = 0;
                            GridRow2ndColum.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
                            GridRow2ndColum.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
                            GridRow2ndColum.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
                            GridRow2ndColum.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
                            var workspaceNameLayout = new StackLayout
                            {
                                VerticalOptions = LayoutOptions.StartAndExpand,
                                HorizontalOptions = LayoutOptions.StartAndExpand,
                                Orientation = StackOrientation.Horizontal,
                                Padding = new Thickness(0, 0, 0, 0)
                            };
                            var workspaceName = new Label
                            {
                                VerticalOptions = LayoutOptions.Center,
                                HorizontalOptions = LayoutOptions.StartAndExpand,
                                TextColor = Color.FromHex("#767676"),
                                FontSize = 14,
                            };
                            workspaceName.Text = groupOfMeeting[workspaceCount].Workspace.Name;
                            workspaceNameLayout.Children.Add(workspaceName);
                            GridRow2ndColum.Children.Add(workspaceNameLayout, 0, 0);
                            var capabilitiesLayout = new StackLayout
                            {
                                VerticalOptions = LayoutOptions.CenterAndExpand,
                                HorizontalOptions = LayoutOptions.StartAndExpand,
                                Orientation = StackOrientation.Horizontal,
                                Padding = new Thickness(0, 8, 0, 0)
                            };
                            if (groupOfMeeting[workspaceCount].Workspace.IconUri != null)
                            {
                                for (int capabilitiesCount = 0; capabilitiesCount < groupOfMeeting[workspaceCount].Workspace.IconUri.Count; capabilitiesCount++)
                                {
                                    var capabilty = new Image
                                    {
                                        HorizontalOptions = LayoutOptions.Start,
                                        VerticalOptions = LayoutOptions.Center,
                                        Aspect = Aspect.Fill,
                                        HeightRequest = 15,
                                        WidthRequest = 15
                                    };
                                    capabilty.Source = groupOfMeeting[workspaceCount].Workspace.IconUri[capabilitiesCount].IconUri;
                                    capabilitiesLayout.Children.Add(capabilty);
                                }
                            }
                            GridRow2ndColum.Children.Add(capabilitiesLayout, 0, 1);
                            var invitedLayout = new StackLayout
                            {
                                VerticalOptions = LayoutOptions.CenterAndExpand,
                                HorizontalOptions = LayoutOptions.StartAndExpand,
                                Orientation = StackOrientation.Horizontal,
                                Padding = new Thickness(0, 8, 0, 0)
                            };
                            var people = new Image
                            {
                                HorizontalOptions = LayoutOptions.Start,
                                VerticalOptions = LayoutOptions.Center,
                                Aspect = Aspect.Fill
                            };
                            invitedLayout.Children.Add(people);
                            var invitedLabel = new Label
                            {
                                VerticalOptions = LayoutOptions.Center,
                                HorizontalOptions = LayoutOptions.StartAndExpand,
                                TextColor = Color.FromHex("#767676"),
                                FontSize = 12,
                                FontFamily = "HelveticaNeue",
                                //Text = "Invited 0 people"
                            };
                            invitedLayout.Children.Add(invitedLabel);
                            GridRow2ndColum.Children.Add(invitedLayout, 0, 2);
                            var timeLayout = new StackLayout
                            {
                                VerticalOptions = LayoutOptions.CenterAndExpand,
                                HorizontalOptions = LayoutOptions.StartAndExpand,
                                Orientation = StackOrientation.Horizontal,
                                Padding = new Thickness(0, 8, 0, 0)
                            };
                            var time = new Image
                            {
                                HorizontalOptions = LayoutOptions.Start,
                                VerticalOptions = LayoutOptions.Center,
                                Aspect = Aspect.Fill
                            };
                            time.Source = "icon_distance.png";
                            timeLayout.Children.Add(time);
                            var timeLabel = new Label
                            {
                                VerticalOptions = LayoutOptions.Center,
                                HorizontalOptions = LayoutOptions.StartAndExpand,
                                TextColor = Color.FromHex("#767676"),
                                FontSize = 12,
                                FontFamily = "HelveticaNeue"
                            };
                            DateTimeOffset convertedDate = DateTime.SpecifyKind(
                            DateTime.Parse(groupOfMeeting[workspaceCount].StartTime.ToString()), DateTimeKind.Local);
                            timeLabel.Text = convertedDate.ToString("ddd,MMM dd,yyyy hh:mm tt");
                            timeLayout.Children.Add(timeLabel);
                            GridRow2ndColum.Children.Add(timeLayout, 0, 3);
                            workspaceGrid.Children.Add(GridRow2ndColum, 1, 0);
                            workspaceView.Content = workspaceGrid;

                            var gi = new GestureFrame
                            {
                                VerticalOptions = LayoutOptions.FillAndExpand,
                                HorizontalOptions = LayoutOptions.FillAndExpand,
                                Padding = new Thickness(0, 0, 0, 0),
                                BackgroundColor = Color.White,
                                Spacing = -45,
                                Orientation = StackOrientation.Horizontal
                            };
                            var tapGestureForCheckIn = new TapGestureRecognizer();
                            if (Device.RuntimePlatform == Device.iOS)
                            {
                                tapGestureForCheckIn.Tapped += async (s, e) =>
                                {
                                    try
                                    {
                                        await Task.Run(() =>
                                        {
                                            IsBusy = true;
                                        });
                                        await Task.Delay(200);
                                        var layout = s as ContentView;
                                        var eventID = layout.StyleId;
                                        foreach (var item in MeetingRecord)
                                        {
                                            if (item.Id == Convert.ToInt64(eventID))
                                            {
                                                checkInEvent = item;
                                                checkInEvent.PrimaryOwner.ObjectId = App.UniqueId;
                                                CheckInMeetingName = item.Workspace.Name;
                                                WorkspaceImage = item.Workspace.PhotoLinks;
                                                CheckInMeetingTime = item.StartTime.ToString("hh:mm tt") + "-" + item.EndTime.ToString("hh:mm tt");
                                                break;
                                            }
                                        }
                                        var timeDifference = checkInEvent.StartTime.Subtract(DateTime.Now).TotalMinutes;
                                        if (checkInEvent.CheckinPerformedBy == 0 && checkInEvent.CheckoutPerformedBy == 0)
                                        {
                                            if (timeDifference <= 0 && timeDifference >= -5)
                                            {
                                                ////WorkspaceEndTime = DateTime.Now.AddMinutes(65).TimeOfDay;
                                                ////WorkspaceStartTime = DateTime.Now.AddMinutes(5).TimeOfDay;
                                                //ResetTimeRangeValues();
                                                //IsMapViewVisible = true;
                                                //IsListViewVisible = false;
                                                //IsListIconVisible = true;
                                                //IsMapIconVisible = false;
                                                //switchToMapClicked = true;
                                                //if (meeloMapObj.CustomPins != null)
                                                //{
                                                //    meeloMapObj.CustomPins.Clear();
                                                //}
                                                //if (meeloMapObj.Pins != null)
                                                //{
                                                //    meeloMapObj.Pins.Clear();
                                                //}
                                                _navigationService.NavigateTo(Helper.Constants.MapPage);
                                                //LoadPage();
                                                //PositionChanged();
                                                //SearchTextColor = Colors.MeeloBlue;

                                                //IsVicinityCheckIn = true;
                                                //GetReservedMeetingsCount();
                                                //InitialiseSearchTab();
                                            }
                                            else
                                                await _dialogService.ShowMessage("This workspace is currently unavailable for check in.", "Failed!");
                                        }
                                        else
                                            await _dialogService.ShowMessage("This workspace is already checked in.", "Failed!");

                                        await Task.Run(() =>
                                        {
                                            IsBusy = false;
                                        });
                                    }
                                    catch (Exception ex)
                                    {
                                    }
                                };
                                tapGestureForCheckIn.NumberOfTapsRequired = 1;
                                workspaceView.GestureRecognizers.Add(tapGestureForCheckIn);
                            }

                            gi.Children.Add(workspaceView);



                            var cancelView = new ContentView
                            {
                                VerticalOptions = LayoutOptions.FillAndExpand,
                                HorizontalOptions = LayoutOptions.EndAndExpand,
                                Padding = new Thickness(0, 0, 0, 0),
                                IsVisible = false,
                                StyleId = groupOfMeeting[workspaceCount].Workspace.Id.ToString(),
                                ClassId = groupOfMeeting[workspaceCount].Id.ToString()
                            };

                            var cancelLayout = new StackLayout
                            {
                                VerticalOptions = LayoutOptions.FillAndExpand,
                                HorizontalOptions = LayoutOptions.CenterAndExpand,
                                Padding = new Thickness(20, 0, 20, 0),
                                BackgroundColor = Color.FromHex("#f44336"),
                                MinimumWidthRequest = 100,
                                InputTransparent = true,
                                StyleId = groupOfMeeting[workspaceCount].Workspace.Id.ToString()
                            };
                            var cancelLabel = new Label
                            {
                                VerticalOptions = LayoutOptions.FillAndExpand,
                                HorizontalOptions = LayoutOptions.CenterAndExpand,
                                FontSize = 14,
                                TextColor = Color.White,
                                //InputTransparent = true,
                                BackgroundColor = Color.FromHex("#f44336"),
                                Text = "CANCEL",
                                VerticalTextAlignment = TextAlignment.Center,
                                HorizontalTextAlignment = TextAlignment.Center,
                                InputTransparent = true,
                                StyleId = groupOfMeeting[workspaceCount].Workspace.Id.ToString()
                            };

                            cancelLayout.Children.Add(cancelLabel);

                            var tapGestureOnReserveLabel = new TapGestureRecognizer();
                            tapGestureOnReserveLabel.Tapped += async (s, e) =>
                            {
                                try
                                {
                                    await Task.Run(() =>
                                    {
                                        IsBusy = true;
                                    });
                                    await Task.Delay(200);
                                    var layout = s as Label;
                                    var eventID = layout.StyleId;
                                    Event tempItem = new Event();
                                    foreach (var item in MeetingRecord)
                                    {
                                        if (item.Workspace.Id == Convert.ToInt64(eventID))
                                        {
                                            tempItem = item;
                                            break;
                                        }
                                    }
                                    Event cancelingEvent = new Event();
                                    cancelingEvent.Id = tempItem.Id;
                                    //cancelingEvent.Id = Convert.ToInt64(eventID);
                                    cancelingEvent.CalenderEventId = tempItem.CalenderEventId;
                                    cancelingEvent.StartTime = tempItem.StartTime;
                                    cancelingEvent.EndTime = tempItem.EndTime;
                                    var returnedEvent = await _reserveService.CancelSelectedWorkspace(cancelingEvent);

                                    DateTimeOffset startTime = new DateTimeOffset(DateTime.UtcNow.Date);
                                    DateTimeOffset endTime = DateTimeOffset.UtcNow.AddDays(30);
                                    var meetingData = _workspaceService.GetMeetingList(startTime.ToString("O"), endTime.ToString("O")).Result;
                                    MeetingRecord = meetingData.Reservations.ToList();

                                    ResetOnMeetingsList();
                                    GetReservedMeetingsCount();
                                    await Task.Run(() =>
                                    {
                                        IsBusy = false;
                                    });

                                }
                                catch (Exception ex)
                                {

                                }

                            };
                            tapGestureOnReserveLabel.NumberOfTapsRequired = 1;
                            cancelLabel.GestureRecognizers.Add(tapGestureOnReserveLabel);


                            var tapGestureOnReserve = new TapGestureRecognizer();
                            tapGestureOnReserve.Tapped += async (s, e) =>
                            {
                                try
                                {
                                    await Task.Run(() =>
                                    {
                                        IsBusy = true;
                                    });
                                    await Task.Delay(200);

                                    var layout = s as StackLayout;
                                    var eventID = layout.StyleId;
                                    Event tempItem = new Event();
                                    foreach (var item in MeetingRecord)
                                    {
                                        if (item.Id == Convert.ToInt64(eventID))
                                        {
                                            tempItem = item;
                                            break;
                                        }
                                    }
                                    Event cancelingEvent = new Event();
                                    cancelingEvent.Id = tempItem.Id;
                                    //cancelingEvent.Id = Convert.ToInt64(eventID);
                                    cancelingEvent.CalenderEventId = tempItem.CalenderEventId;
                                    cancelingEvent.StartTime = tempItem.StartTime;
                                    cancelingEvent.EndTime = tempItem.EndTime;
                                    var returnedEvent = await _reserveService.CancelSelectedWorkspace(cancelingEvent);
                                    DateTimeOffset startTime = new DateTimeOffset(DateTime.UtcNow.Date);
                                    DateTimeOffset endTime = DateTimeOffset.UtcNow.AddDays(30);
                                    var meetingData = _workspaceService.GetMeetingList(startTime.ToString("O"), endTime.ToString("O")).Result;
                                    MeetingRecord = meetingData.Reservations.ToList();

                                    ResetOnMeetingsList();
                                    GetReservedMeetingsCount();
                                    await Task.Run(() =>
                                    {
                                        IsBusy = false;
                                    });
                                }
                                catch (Exception ex)
                                {

                                }


                            };
                            tapGestureOnReserve.NumberOfTapsRequired = 1;
                            cancelLayout.GestureRecognizers.Add(tapGestureOnReserve);

                            cancelView.Content = cancelLayout;
                            var tap = new TapGestureRecognizer();
                            tap.Tapped += async (s, e) =>
                            {
                                try
                                {
                                    await Task.Run(() =>
                                    {
                                        IsBusy = true;
                                    });
                                    await Task.Delay(1000);
                                    var layout = s as ContentView;
                                    var eventID = layout.StyleId;
                                    Event tempItem = new Event();
                                    foreach (var item in MeetingRecord)
                                    {
                                        if (item.Workspace.Id == Convert.ToInt64(eventID))
                                        {
                                            tempItem = item;
                                            break;
                                        }
                                    }
                                    _reserveService = new ReserveWorkspaceApi();
                                    Event cancelingEvent = new Event();
                                    cancelingEvent.Id = tempItem.Id;
                                    //cancelingEvent.Id = Convert.ToInt64(eventID);
                                    cancelingEvent.CalenderEventId = tempItem.CalenderEventId;
                                    cancelingEvent.StartTime = tempItem.StartTime;
                                    cancelingEvent.EndTime = tempItem.EndTime;
                                    var returnedEvent = await _reserveService.CancelSelectedWorkspace(cancelingEvent);


                                    DateTimeOffset startTime = new DateTimeOffset(DateTime.UtcNow.Date);
                                    DateTimeOffset endTime = DateTimeOffset.UtcNow.AddDays(30);
                                    var meetingData = _workspaceService.GetMeetingList(startTime.ToString("O"), endTime.ToString("O")).Result;
                                    MeetingRecord = meetingData.Reservations.ToList();

                                    ResetOnMeetingsList();
                                    GetReservedMeetingsCount();
                                    await Task.Run(() =>
                                    {
                                        IsBusy = false;
                                    });
                                }
                                catch (Exception ex)
                                {

                                }

                            };
                            tap.NumberOfTapsRequired = 1;
                            cancelView.GestureRecognizers.Add(tap);

                            gi.Children.Add(cancelView);

                            gi.SwipeLeft += async (s, e) =>
                            {

                                try
                                {
                                    await Task.Run(() =>
                                    {
                                        //IsLoading = true;
                                    });
                                    await Task.Delay(1000);
                                    var stackSwiped = (StackLayout)s;
                                    var reserveCont = (ContentView)stackSwiped.Children[1];
                                    var layoutCont = (ContentView)stackSwiped.Children[0];

                                    for (int j = 0; j < MeetingsListContainer.Children.Count; j++)
                                    {
                                        StackLayout stObj = (StackLayout)MeetingsListContainer.Children[j];
                                        Grid grid = (Grid)stObj.Children[1];
                                        for (int i = 0; i < grid.Children.Count; i++)
                                        {
                                            StackLayout outer = (StackLayout)grid.Children[i];
                                            StackLayout inner = (StackLayout)outer.Children[0];
                                            StackLayout view = (StackLayout)inner.Children[0];

                                            ContentView contentView2 = (ContentView)view.Children[1];
                                            long id = Convert.ToInt64(contentView2.ClassId);
                                            var meeting = MeetingRecord.Where(x => x.Id == id).ToList();
                                            if (contentView2.IsVisible && contentView2.StyleId != reserveCont.StyleId)
                                            {

                                                ContentView contentView1 = (ContentView)view.Children[0];
                                                await contentView1.TranslateTo(0, 0, 50);
                                                contentView2.IsVisible = false;

                                            }
                                            else if (contentView2.StyleId == reserveCont.StyleId && meeting[0].CheckoutPerformedBy == 0)
                                            {
                                                await layoutCont.TranslateTo(-100, 0, 50);
                                                reserveCont.IsVisible = true;

                                            }
                                            else if (contentView2.StyleId == reserveCont.StyleId && meeting[0].CheckoutPerformedBy == 1)
                                            {
                                                await _dialogService.ShowMessage("This workspace is currently checked out.", "Failed!");

                                            }
                                        }
                                    }


                                }
                                catch (Exception ex)
                                {

                                }


                            };

                            gi.SwipeRight += async (s, e) =>
                            {
                                try
                                {
                                    var stack = (StackLayout)s;
                                    var reserveCont = (ContentView)stack.Children[1];
                                    var layoutCont = (ContentView)stack.Children[0];
                                    if (reserveCont.IsVisible)
                                    {
                                        await layoutCont.TranslateTo(0, 0, 50);
                                        reserveCont.IsVisible = false;
                                    }
                                }
                                catch (Exception ex)
                                { }

                            };
                            if (Device.RuntimePlatform == Device.Android)
                            {
                                //tapGestureForCheckIn.Tapped += async (s, e) =>
                                gi.Tapped += async (s, e) =>
                                {
                                    try
                                    {
                                        await Task.Run(() =>
                                        {
                                            IsBusy = true;
                                        });
                                        await Task.Delay(1000);
                                        //var layout = s as ContentView;
                                        //var eventID = layout.StyleId;
                                        var stackSwiped = (StackLayout)s;
                                        var layoutCont = (ContentView)stackSwiped.Children[0];
                                        var eventID = layoutCont.StyleId;

                                        foreach (var item in MeetingRecord)
                                        {
                                            if (item.Id == Convert.ToInt64(eventID))
                                            {
                                                checkInEvent = item;
                                                checkInEvent.PrimaryOwner.ObjectId = App.UniqueId;
                                                CheckInMeetingName = item.Workspace.Name;
                                                WorkspaceImage = item.Workspace.PhotoLinks;
                                                CheckInMeetingTime = item.StartTime.ToString("hh:mm tt") + "-" + item.EndTime.ToString("hh:mm tt");
                                                break;
                                            }
                                        }
                                        var timeDifference = checkInEvent.StartTime.Subtract(DateTime.Now).TotalMinutes;
                                        if (checkInEvent.CheckinPerformedBy == 0 && checkInEvent.CheckoutPerformedBy == 0)
                                        {
                                            if (timeDifference <= 0 && timeDifference >= -5)
                                            {
                                                ////WorkspaceEndTime = DateTime.Now.AddMinutes(65).TimeOfDay;
                                                ////WorkspaceStartTime = DateTime.Now.AddMinutes(5).TimeOfDay;
                                                //ResetTimeRangeValues();
                                                //IsMapViewVisible = true;
                                                //IsListViewVisible = false;
                                                //IsListIconVisible = true;
                                                //IsMapIconVisible = false;
                                                //switchToMapClicked = true;
                                                //if (meeloMapObj.CustomPins != null)
                                                //{
                                                //    meeloMapObj.CustomPins.Clear();
                                                //}
                                                //if (meeloMapObj.Pins != null)
                                                //{
                                                //    meeloMapObj.Pins.Clear();
                                                //}
                                                _navigationService.NavigateTo(Helper.Constants.MapPage);
                                                //LoadPage();
                                                //PositionChanged();
                                                //SearchTextColor = Colors.MeeloBlue;

                                                //IsVicinityCheckIn = true;
                                                GetReservedMeetingsCount();
                                                //InitialiseSearchTab();
                                                //IsBottomTabsVisible = false;
                                            }
                                            else
                                                await _dialogService.ShowMessage("This workspace is currently unavailable for check in.", "Failed!");
                                        }
                                        else
                                            await _dialogService.ShowMessage("This workspace is already checked in.", "Failed!");

                                        await Task.Run(() =>
                                        {
                                            IsBusy = false;
                                        });
                                    }
                                    catch (Exception ex)
                                    {
                                    }
                                };
                            }
                            wholeLayout.Children.Add(gi);

                            wholeBoxLayout.Children.Add(wholeLayout);
                            var box = new BoxView
                            {
                                HeightRequest = 1,
                                HorizontalOptions = LayoutOptions.FillAndExpand,
                                VerticalOptions = LayoutOptions.Start,
                                BackgroundColor = Color.FromHex("#f0f0f0"),

                            };
                            wholeBoxLayout.Children.Add(box);

                            ComponentGrid.Children.Add(wholeBoxLayout, 0, workspaceCount);
                        }
                        meetingsWorkspaceLayout.Children.Add(ComponentGrid);

                        //}

                        MeetingsListContainer.Children.Add(meetingsWorkspaceLayout);


                    }
            }
            catch (Exception ex)
            {

            }
        }
        #endregion Methods
    }
}
