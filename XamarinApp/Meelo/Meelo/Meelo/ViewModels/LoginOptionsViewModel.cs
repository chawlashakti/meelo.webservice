﻿//using CoreLocation;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using Microsoft.Identity.Client;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Newtonsoft.Json;
using System.Diagnostics;
using Meelo.Helper;
using Meelo.Interfaces;
using Meelo.Authentication;
using Meelo.Services;

namespace Meelo.ViewModels
{
    public class LoginOptionsViewModel : ViewModelBase, IGoogleAuthenticationDelegate
    {
        public ICommand OnSignInO365Click { get; set; }
        public ICommand OnSignInGoogleClick { get; set; }
        public ICommand OnSignInFacebookClick { get; set; }

        public ICommand OnRemeberMeClick { get; set; }

        private readonly INavigationService _navigationService;
        private IDialogService _dialogService;

        private bool _isbusy;
        public bool IsBusy
        {
            get { return _isbusy; }      
            set
            {
                _isbusy = value;
                RaisePropertyChanged("IsBusy");
            }
        }

        

        public LoginOptionsViewModel(INavigationService navigationService, IDialogService dialogService)
        {
            if (navigationService == null)
                throw new ArgumentNullException("navigationService");
            _navigationService = navigationService;

            if (dialogService == null)
                throw new ArgumentNullException("navigationService");
            _dialogService = dialogService;

            OnSignInGoogleClick = new Command(GoogleSignIn);
            OnSignInO365Click = new Command(Office365SignIn);
            IsBusy = false;
        }

        public async void OnAuthenticationCompleted(GoogleOAuthToken token)
        {
            App.AccessToken = token.AccessToken;
            var googleService = new GoogleService();
            var user = await googleService.GetUserInfoAsync(token.TokenType, token.AccessToken);
            App.UserName = user.name;
            _navigationService.NavigateTo(Constants.MainTabbedPage);
            IsBusy = false;
        }

        public void OnAuthenticationFailed(string message, Exception exception)
        {
            throw new NotImplementedException();
        }

        public void OnAuthenticationCanceled()
        {
            throw new NotImplementedException();
        }

        #region Command Events
       async void GoogleSignIn()
        {
            await Task.Run(() =>
            {
                IsBusy = true;
            });
            
            App.LoggedInPlatform = Constants.LoginPlatform.Google;
            var Configuration = DependencyService.Get<IGoogleAuthenticationConfiguration>().GetConfiguration();
            App.Auth = new GoogleAuthenticator(Configuration.ClientId,Helper.Constants.GoogleScope, Configuration.RedirectUrl, this);
            var authenticator = App.Auth.GetAuthenticator();
            var presenter = new Xamarin.Auth.Presenters.OAuthLoginPresenter();
            presenter.Login(authenticator);
        }
        async void Office365SignIn()
        {
            IsBusy = true;
            App.LoggedInPlatform = Constants.LoginPlatform.Office365;
            string Token = await AuthenticationHelper.GetTokenForUserAsync();
            IsBusy = false;
            if (Token != null)
            {
                _navigationService.NavigateTo(Constants.MainTabbedPage);
            }
        }
        
        #endregion Commmand Events
    }
}
