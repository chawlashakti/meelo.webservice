﻿using GalaSoft.MvvmLight.Views;
using Meelo.Interfaces;
using Meelo.Models;
using Meelo.Views;
using Meelo.Webservices;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Meelo.ViewModels
{
    public class InvitePageViewModel
    {
        private readonly INavigationService _navigationService;
        private IDialogService _dialogService;
        private IReserveWorkspace _reserveService;
        public List<PhoneContact> SelectedContacts;
        public string workdpaceID { get; set; }
        public InvitePage InvitePageObj;
        public InvitePageViewModel(INavigationService navigationService, IDialogService dialogService)
        {
            if (navigationService == null)
                throw new ArgumentNullException("navigationService");
            _navigationService = navigationService;
            if (dialogService == null)
                throw new ArgumentNullException("navigationService");
            _dialogService = dialogService;
        }
       public async void OnDone()
        {
            //await Task.Run(() =>
            //{
                InvitePageObj.Busy = true;
            //});
            
            int SelectedWorkspaceId = 0;
            DateTime StartDate = App.Locator.MapPageViewModelInstance.WorkspaceSearchDate;
            TimeSpan EndTime = App.Locator.MapPageViewModelInstance.WorkspaceEndTime;
            TimeSpan StartTime = App.Locator.MapPageViewModelInstance.WorkspaceStartTime;
            try
            {
                if (workdpaceID == null)
                    return;
                else
                    SelectedWorkspaceId = Convert.ToInt32(workdpaceID);


                WorkspaceModel selectedWorkspace = new WorkspaceModel();
                selectedWorkspace = App.Locator.MapPageViewModelInstance.WorkspaceRecord.Where(m => m.WorkspaceId == SelectedWorkspaceId).FirstOrDefault();
                if (selectedWorkspace == null)
                    return;
                Event reservingEvent = new Event();
                reservingEvent.Workspace = new Workspace();
                reservingEvent.EventCalendar = new Models.Calendar();
                reservingEvent.PrimaryOwner = new User();
                reservingEvent.Workspace.Id = selectedWorkspace.WorkspaceId;
                reservingEvent.Workspace.OrgId = selectedWorkspace.OrgId;
                reservingEvent.CreatedDateTime = DateTime.UtcNow;
                var currentstartTime = new DateTime(StartDate.Year, StartDate.Month, StartDate.Day, StartTime.Hours, StartTime.Minutes, 0);
                reservingEvent.StartTime = currentstartTime.ToUniversalTime();
                var currentEndTime = new DateTime(StartDate.Year, StartDate.Month, StartDate.Day, EndTime.Hours, EndTime.Minutes, 0);
                reservingEvent.EndTime = currentEndTime.ToUniversalTime();
                reservingEvent.Id = 1;
                reservingEvent.Importance = ImportanceType.Normal;
                reservingEvent.PrimaryOwner.Id = 1;
                reservingEvent.WebLink = new Uri("http://meelo.io");
                reservingEvent.CheckinTime = DateTime.UtcNow;
                reservingEvent.CheckoutTime = DateTime.UtcNow;
                reservingEvent.OnlineMeetingUrl = "http://meelo.io";
                reservingEvent.EventCalendar.Id = 1;
                reservingEvent.Subject = selectedWorkspace.WorkspaceName;
                reservingEvent.PrimaryOwner.ObjectId = App.UniqueId;
                StringBuilder Attendees = new StringBuilder();
                for(int i=0;i< SelectedContacts.Count;i++)
                {
                    Attendees.Append(SelectedContacts[i].EmailAddress);
                    Attendees.Append(",");
                }
                _reserveService = new ReserveWorkspaceApi();
                var returnedEvent = await _reserveService.ReserveSelectedWorkspace(reservingEvent,Attendees.ToString());
                if (returnedEvent != null && returnedEvent.IsCallSuccessful)
                {
                    await _dialogService.ShowMessage("Workspace " + reservingEvent.Subject + " Reserved", "Success!");
                }
                else
                {
                    await _dialogService.ShowMessage(returnedEvent.Message, "Failed!");
                }

                await Task.Run(() =>
                {
                    InvitePageObj.Busy = false;
                });

            }
            catch (Exception ex)
            {
                //return false;
            }
        }
    }
}
