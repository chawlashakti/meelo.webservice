﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meelo.ViewModels
{
    public class PhoneContact
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public string Name { get => $"{FirstName} {LastName}"; }
        public Xamarin.Forms.Image Photo { get; set; }

    }
}
