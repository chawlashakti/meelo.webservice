﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using Meelo.CustomControls;
using Meelo.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Plugin.Geolocator.Abstractions;
using Plugin.Geolocator;
using Xamarin.Forms.Maps;
using System.Diagnostics;
using Meelo.Views;
using System.Globalization;
using Meelo.Models;
using Microsoft.Azure.Search.Models;
using Microsoft.Azure.Search;
using Meelo.Webservices;

namespace Meelo.ViewModels
{
    public class MapPageViewModel : ViewModelBase
    {
        #region Commands
        public ICommand OnSwitchToListViewClick { get; set; }
        public ICommand OnSwitchToMapClick { get; set; }
        public ICommand OnTimeSelectClick { get; set; }
        public ICommand OnCalendarLabelClick { get; set; }
        public ICommand OnClearSearchClicked { get; set; }
        public ICommand OnCancelSearchClick { get; set; }
        public ICommand OnSearchClick { get; set; }
        #endregion
        #region Fields
        private readonly INavigationService _navigationService;
        private IDialogService _dialogService;
        private readonly IGeolocator locator = CrossGeolocator.Current;
        public CustomMap meeloMapObj;
        public RangeSliderView RangeSlider;
        public MapPage MapPageObj { get; set; }
        SearchIndexClient indexClient;
        private IUserWorkspace _WorkspaceService;
        private IReserveWorkspace _reserveService;
        public Constants.PageView LastPage;
        public Constants.PageView CurrentPage;
        public StackLayout WorkspaceListContainer;
        #endregion end fields
        #region Visiblity Properties
        private bool _mapViewVisible = true;
        public bool mapViewVisible
        {
            get { return _mapViewVisible; }
            set
            {
                _mapViewVisible = value;
                RaisePropertyChanged("mapViewVisible");
            }

        }
        private bool _isListViewVisible;
        public bool IsListViewVisible
        {
            get { return _isListViewVisible; }
            set
            {
                _isListViewVisible = value;
                RaisePropertyChanged("IsListViewVisible");
            }
        }
        private bool _isEmptyMsgVisible;
        public bool IsEmptyMsgVisible
        {
            get { return _isEmptyMsgVisible; }
            set
            {
                _isEmptyMsgVisible = value;
                RaisePropertyChanged("IsEmptyMsgVisible");
            }
        }
        private bool _isListNotEmpty;
        public bool IsListNotEmpty
        {
            get { return _isListNotEmpty; }
            set
            {
                _isListNotEmpty = value;
                RaisePropertyChanged("IsListNotEmpty");
            }
        }
        private bool _isListIconVisible = true;
        public bool IsListIconVisible
        {
            get { return _isListIconVisible; }
            set
            {
                _isListIconVisible = value;
                RaisePropertyChanged("IsListIconVisible");
            }
        }
        private bool _SearchHeaderVisible;
        public bool SearchHeaderVisible
        {
            get { return _SearchHeaderVisible; }
            set
            {
                _SearchHeaderVisible = value;
                RaisePropertyChanged("SearchHeaderVisible");
            }

        }
        private bool _ConfRoomHeaderVisible = true;
        public bool ConfRoomHeaderVisible
        {
            get { return _ConfRoomHeaderVisible; }
            set
            {
                _ConfRoomHeaderVisible = value;
                RaisePropertyChanged("ConfRoomHeaderVisible");
            }

        }
        private bool _isSearchFiltersVisible = true;
        public bool IsSearchFiltersVisible
        {
            get { return _isSearchFiltersVisible; }
            set
            {
                _isSearchFiltersVisible = value;
                RaisePropertyChanged("IsSearchFiltersVisible");
            }
        }
        private bool _isVisibleStackTimeRangeslider = true;
        public bool IsVisibleStackTimeRangeslider
        {
            get { return _isVisibleStackTimeRangeslider; }
            set
            {
                _isVisibleStackTimeRangeslider = value;
                RaisePropertyChanged("IsVisibleStackTimeRangeslider");
            }
        }
        private bool _ClearSearchIconVisible;
        public bool ClearSearchIconVisible
        {
            get { return _ClearSearchIconVisible; }
            set
            {
                _ClearSearchIconVisible = value;
                RaisePropertyChanged("ClearSearchIconVisible");
            }
        }
        private bool _isVisibleCalendar;
        public bool isVisibleCalendar
        {
            get { return _isVisibleCalendar; }
            set
            {
                _isVisibleCalendar = value;
                RaisePropertyChanged("isVisibleCalendar");
            }
        }
        private bool _SearchListVisible;
        public bool SearchListVisible
        {
            get { return _SearchListVisible; }
            set
            {
                _SearchListVisible = value;
                RaisePropertyChanged("SearchListVisible");
            }

        }
        private bool _isbusy;
        public bool IsBusy
        {
            get { return _isbusy; }
            set
            {
                _isbusy = value;
                RaisePropertyChanged("IsBusy");
            }
        }
        private bool _isCurrentLocation=true;
        public bool IsCurrentLocation
        {
            get { return _isCurrentLocation; }
            set
            {
                _isCurrentLocation = value;
                RaisePropertyChanged("IsCurrentLocation");
            }
        }
        #endregion Visiblity Properties
        #region Properties
        private Plugin.Geolocator.Abstractions.Position _position;
        public Plugin.Geolocator.Abstractions.Position MyPosition
        {
            get { return _position; }
            set
            {
                _position = value;
                App.position = _position;
                GetAvailableWorkspaces();
                
            }
        }
        private string _confRoomAvailableCount = "0 available";
        public string ConfRoomAvailableCount
        {
            get { return _confRoomAvailableCount; }
            set
            {
                _confRoomAvailableCount = value;
                RaisePropertyChanged("ConfRoomAvailableCount");
            }
        }
        private DateTime _workspaceSearchDate = DateTime.Now;
        public DateTime WorkspaceSearchDate
        {
            get { return _workspaceSearchDate; }
            set
            {
                _workspaceSearchDate = value;
                DateTime NewTime = _workspaceSearchDate;
                if (NewTime.ToString("dd/MM/yyyy") == DateTime.Now.ToString("dd/MM/yyyy"))
                    NewTime = DateTime.Now;
                else
                    NewTime = _workspaceSearchDate.AddHours(DateTime.Now.Hour).AddMinutes(DateTime.Now.Minute);

                TimeSlot(NewTime);
                RaisePropertyChanged("WorkspaceSearchDate");
            }
        }
        private string _meetingTime;
        public string MeetingTime
        {
            get { return _meetingTime; }
            set
            {
                _meetingTime = value;
                RaisePropertyChanged("MeetingTime");
            }
        }
        private long _SearchedBuildingId;
        public long SearchedBuildingId
        {
            get { return _SearchedBuildingId; }
            set
            {
                _SearchedBuildingId = value;
                RaisePropertyChanged("SearchedBuildingId");
            }
        }
        private string _CalendarLabSelecteddate;
        public string CalendarLabSelecteddate
        {
            get { return _CalendarLabSelecteddate; }
            set
            {
                _CalendarLabSelecteddate = value;
                RaisePropertyChanged("CalendarLabSelecteddate");
            }
        }
        private string _SearchText;
        public string SearchText
        {
            get { return _SearchText; }
            set
            {
                _SearchText = value;
                Search(_SearchText);
                RaisePropertyChanged("SearchText");
            }

        }
        //private AzureSearchModel _SelectedSearchText;
        //public AzureSearchModel SelectedSearchText
        //{
        //    get { return _SelectedSearchText; }
        //    set
        //    {
        //        _SelectedSearchText = value;
        //        RaisePropertyChanged("SelectedSearchText");
        //    }

        //}
        private TimeSpan _workspaceStartTime;
        public TimeSpan WorkspaceStartTime
        {
            get { return _workspaceStartTime; }
            set
            {
                _workspaceStartTime = value;
                RaisePropertyChanged("WorkspaceStartTime");
            }
        }
        private TimeSpan _workspaceEndTime;
        public TimeSpan WorkspaceEndTime
        {
            get { return _workspaceEndTime; }
            set
            {
                _workspaceEndTime = value;
                RaisePropertyChanged("WorkspaceEndTime");
            }
        }
        private List<WorkspaceModel> _workspaceRecord = new List<WorkspaceModel>();
        public List<WorkspaceModel> WorkspaceRecord
        {
            get { return _workspaceRecord; }
            set
            {
                _workspaceRecord = value;
                RaisePropertyChanged("WorkspaceRecord");
            }
        }
        private List<AzureSearchResponseModel> _AzureSearchList;
        public List<AzureSearchResponseModel> AzureSearchList
        {
            get { return _AzureSearchList; }
            set
            {
                _AzureSearchList = value;
                RaisePropertyChanged("AzureSearchList");
            }

        }
        #endregion
        #region Constructor
        public MapPageViewModel(INavigationService navigationService, IDialogService dialogService)
        {
            if (navigationService == null)
                throw new ArgumentNullException("navigationService");
            _navigationService = navigationService;
            if (dialogService == null)
                throw new ArgumentNullException("navigationService");
            _dialogService = dialogService;
           
            #region Commands
            OnTimeSelectClick = new Command(OnTimeSelect);
            OnCalendarLabelClick = new RelayCommand(() =>
            {
                isVisibleCalendar = true;
            });
            OnSwitchToListViewClick = new Command(OnSwitchToListView);
            OnClearSearchClicked = new Command(OnClearSearch);
            OnCancelSearchClick = new Command(OnCancelSearch);
            OnSearchClick = new Command(OnSearch);
            #endregion Commands
            LastPage = CurrentPage = Constants.PageView.Map;
            VisibilitySetting(Constants.PageView.Map);
            indexClient = new SearchIndexClient("meelosearchbasic", "workspacesql-index", new SearchCredentials("D16F28ACA1F70AF47BEA15F5D5E0CF21"));
        }
        #endregion End Constructor
        #region Functions
        void OnTimeSelect()
        {
            if (IsVisibleStackTimeRangeslider)
                IsVisibleStackTimeRangeslider = false;
            else
                IsVisibleStackTimeRangeslider = true;
        }
        void OnSwitchToListView()
        {
            if (IsListIconVisible)
            {
                //IsListIconVisible = false;
                VisibilitySetting(Constants.PageView.List);
                if (WorkspaceRecord.Count == 0)
                {
                    IsListNotEmpty = false;
                    IsEmptyMsgVisible = true;
                }
                else
                {
                    IsListNotEmpty = true;
                    IsEmptyMsgVisible = false;
                    CreateListView();
                }
            }
            else
            {
                IsListIconVisible = true;
                VisibilitySetting(Constants.PageView.Map);
                var Buildings = WorkspaceRecord.Distinct(new DistinctItemComparer()).ToList();
               
                LoadMapData(Buildings);
            }
                
        }
        void OnClearSearch()
        {
            AzureSearchList = null;
            SearchText = "";
        }
        void OnCancelSearch()
        {
            VisibilitySetting(LastPage);
        }
        void OnSearch()
        {
            VisibilitySetting(Constants.PageView.Search);
        }
        public void VisibilitySetting(Constants.PageView page)
        {
            switch(page)
            {
                case Constants.PageView.Map:
                    ConfRoomHeaderVisible = true;
                    IsSearchFiltersVisible = true;
                    IsVisibleStackTimeRangeslider = true;
                    mapViewVisible = true;
                    SearchHeaderVisible = false;
                    SearchListVisible = false;
                    ClearSearchIconVisible = false;
                    IsListViewVisible = false;
                    break;
                case Constants.PageView.List:
                    ConfRoomHeaderVisible = true;
                    IsSearchFiltersVisible = true;
                    IsVisibleStackTimeRangeslider = true;
                    mapViewVisible = false;
                    SearchHeaderVisible = false;
                    SearchListVisible = false;
                    ClearSearchIconVisible = false;
                    IsListViewVisible = true;
                    IsListIconVisible = false;
                    break;
                case Constants.PageView.Search:
                    ConfRoomHeaderVisible = false;
                    IsSearchFiltersVisible = false;
                    IsVisibleStackTimeRangeslider = false;
                    mapViewVisible = false;
                    SearchHeaderVisible = true;
                    SearchListVisible = false;
                    ClearSearchIconVisible = true;
                    IsListViewVisible = false;
                    break;

            }
            LastPage = CurrentPage;
            CurrentPage = page;
        }
        public void ClearMapPins()
        {
             if (meeloMapObj.CustomPins != null)
                {
                    meeloMapObj.CustomPins.Clear();
                }
                if (meeloMapObj.Pins != null)
                {
                    meeloMapObj.Pins.Clear();
                }
        }
        public void TimeSlot(DateTime Time)
        {
            RangeSlider.SetStepsInHours(Time);
        }
        public void ResetTimeSlot(DateTime time)
        {
            WorkspaceSearchDate = time;
            string Selectedday = time.ToString("ddd", CultureInfo.InvariantCulture);
            CalendarLabSelecteddate = Selectedday + ", " + time.ToString("MMM") + " " + DateTime.Now.Day.ToString();

        }
        public void CalculateStartAndEndTime()
        {
            if(MapPageObj!=null)
            MapPageObj.CalculateStartandEndTime();
        }
        public void AddPinsOnMap(Plugin.Geolocator.Abstractions.Position p, bool UserLocation,string Count="0")
        {
            
            string PinId = "WorkspaceLocation";
            if (UserLocation)
                PinId = "UserLocation";
            try
            {
                var pin = new CustomPin
                {
                    Type = PinType.Place,
                    Position = new Xamarin.Forms.Maps.Position(p.Latitude, p.Longitude),
                    Label = Count,
                    Id = PinId,
                    WorkspaceCount = 0
                };
                meeloMapObj.CustomPins = new System.Collections.ObjectModel.ObservableCollection<CustomPin> { pin };
                meeloMapObj.Pins.Add(pin);
                if(UserLocation)
                meeloMapObj.MoveToRegion(MapSpan.FromCenterAndRadius(new Xamarin.Forms.Maps.Position(p.Latitude, p.Longitude), Distance.FromKilometers(5.0)));

            }
            catch (Exception ex)
            {

            }

        }
        public async void Search(string keyword)
        {
            List<AzureSearchResponseModel> List = new List<AzureSearchResponseModel>();
            var parameters = new SuggestParameters()
            {
                UseFuzzyMatching = false,
                MinimumCoverage = 100,
                Top = 10,
                Filter = "TenantId eq '"+App.TenantId+"'"
            };
            try
            {
                var suggestionResults = await indexClient.Documents.SuggestAsync<AzureSearchResponseModel>(keyword, "workspacesql-suggester", parameters);
                foreach (var result in suggestionResults.Results)
                {
                    List.Add(new AzureSearchResponseModel
                    {
                        SearchText = result.Text,
                        WorkspaceBldId = result.Document.WorkspaceBldId
                    });
                }
                this.AzureSearchList = List.Distinct(new Helper.SearchTextComparer()).ToList();
            }
            catch (Exception ex)
            { }
        }
        public async void GetAvailableWorkspaces(long BuildingId=0, string SearchText=null)
        {
            IsBusy = true;
            if (App.position != null)
            {
                _WorkspaceService = new UserWorksapceApi();
                var StartTime = (new DateTime(WorkspaceSearchDate.Year, WorkspaceSearchDate.Month, WorkspaceSearchDate.Day, WorkspaceStartTime.Hours, WorkspaceStartTime.Minutes, 0)).ToUniversalTime();
                var EndTime = (new DateTime(WorkspaceSearchDate.Year, WorkspaceSearchDate.Month, WorkspaceSearchDate.Day, WorkspaceEndTime.Hours, WorkspaceEndTime.Minutes, 0)).ToUniversalTime();
                WorkspaceBuilding workspaceData = null;
                if (IsCurrentLocation)
                    workspaceData = await _WorkspaceService.GetListViewWorkspaces(App.position.Latitude, App.position.Longitude, StartTime, EndTime, BuildingId, SearchText);
                else
                    workspaceData = await _WorkspaceService.GetListViewWorkspaces(App.position.Latitude, App.position.Longitude, StartTime, EndTime, SearchedBuildingId, this.SearchText);

                ConfRoomAvailableCount = workspaceData.DataCount + " available";
                WorkspaceRecord = workspaceData.value;
                if (workspaceData != null && Convert.ToInt32(workspaceData.DataCount) > 0)
                {
                    var Buildings = workspaceData.value.Distinct(new DistinctItemComparer()).ToList();


                    LoadMapData(Buildings);
                }

            }
            IsBusy = false;

        }
        public void LoadMapData(List<WorkspaceModel> Buildings)
        {
            meeloMapObj.CustomPins = new System.Collections.ObjectModel.ObservableCollection<CustomPin>();
            meeloMapObj.Pins.Clear();
            try
            {

                if (Buildings != null || Buildings.Count > 0)
                {
                    for (int i = 0; i < Buildings.Count; i++)
                    {
                        var pin = new CustomPin
                        {
                            Type = PinType.Place,
                            Position = new Xamarin.Forms.Maps.Position(Buildings[i].Lat, Buildings[i].Long),
                            Label = Buildings[i].Name.ToString(),
                            Id = "WorkspaceLocation",
                            WorkspaceCount = Buildings[i].WorkspaceCount,
                            BuildingId = Buildings[i].BuildingId
                        };

                        meeloMapObj.CustomPins.Add(pin);

                        meeloMapObj.Pins.Add(pin);
                    }

                }
                var Userpin = new CustomPin
                {
                    Type = PinType.Place,
                    Position = new Xamarin.Forms.Maps.Position(App.position.Latitude, App.position.Longitude),
                    Label = "User's Location",
                    Id = "UserLocation",
                    WorkspaceCount = 0
                };
                meeloMapObj.CustomPins.Add(Userpin);
                meeloMapObj.Pins.Add(Userpin);
                if(IsCurrentLocation)
                meeloMapObj.MoveToRegion(MapSpan.FromCenterAndRadius(new Xamarin.Forms.Maps.Position(App.position.Latitude, App.position.Longitude), Distance.FromKilometers(5.0)));
                else
                meeloMapObj.MoveToRegion(MapSpan.FromCenterAndRadius(new Xamarin.Forms.Maps.Position(Buildings[0].Lat, Buildings[0].Long), Distance.FromKilometers(5.0)));

            }
            catch (Exception ex)
            {

            }
        }
        public void LoadListOnPinTap(double latitude, double longitude, string buildingID)         {             try             {                 IsBusy = true;                 
                    List<WorkspaceModel> uniqueList = new List<WorkspaceModel>();

                for (int i = 0; i < WorkspaceRecord.Count; i++)
                {
                    if (WorkspaceRecord[i].BuildingId == buildingID)
                    {
                        uniqueList.Add(WorkspaceRecord[i]);
                        //break;
                    }
                }


                    foreach (var item in uniqueList)
                    {
                        item.DisplayRatingCount = "(" + item.RatingCount.ToString() + ")";
                        item.DisplayRatings = Math.Round(Convert.ToDecimal(item.Ratings), 1);
                    }
                    //WorkspaceRecord = uniqueList;
                    if (WorkspaceRecord != null && WorkspaceRecord.Count > 0)
                    {
                        SearchText = WorkspaceRecord[0].Building;
                        //tappedBuildingId = buildingID;
                        ConfRoomAvailableCount = WorkspaceRecord.Count + " available";
                    }
                    else
                        ConfRoomAvailableCount = "0 available";
                    VisibilitySetting(Constants.PageView.List);
                    if (uniqueList.Count == 0)
                    {
                        IsListNotEmpty = false;
                        IsEmptyMsgVisible = true;
                    }
                    else
                    {
                        IsListNotEmpty = true;
                        IsEmptyMsgVisible = false;
                    //await Task.Run(() =>
                    //{
                        CreateListView(uniqueList);
                    //});
                }
                    

                                IsBusy = false;              }             catch (Exception ex) { }         }
        #region ListView Operations
        public void CreateListView(List<WorkspaceModel> WorkspaceData = null)
        {
            if (WorkspaceData == null)
                WorkspaceData = WorkspaceRecord;
            try
            {
                WorkspaceListContainer.Children.Clear();

                Grid ComponentGrid = new Grid
                {
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                };
                ComponentGrid.RowSpacing = 0;
                for (int i = 0; i < WorkspaceData.Count; i++)
                {
                    ComponentGrid.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
                }

                for (int workspaceCount = 0; workspaceCount < WorkspaceData.Count; workspaceCount++)
                {
                    var wholeBoxLayout = new StackLayout
                    {
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        Padding = new Thickness(0, 0, 0, 0),
                        Orientation = StackOrientation.Vertical,
                        HeightRequest = 130
                    };

                    var wholeLayout = new StackLayout
                    {
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        Padding = new Thickness(0, 0, 0, -5),
                        Orientation = StackOrientation.Horizontal,
                        StyleId = WorkspaceData[workspaceCount].WorkspaceId.ToString()
                    };

                    var workspaceView = new ContentView
                    {
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        Padding = new Thickness(0, 0, 0, 0),
                        //StyleId = WorkspaceRecord[workspaceCount].WorkspaceId.ToString()
                    };

                    Grid workspaceGrid = new Grid
                    {
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        InputTransparent = true,
                    };
                    workspaceGrid.RowSpacing = 0;
                    workspaceGrid.ColumnSpacing = 0;

                    workspaceGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });
                    workspaceGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });

                    var confImageLayout = new StackLayout
                    {
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        Padding = new Thickness(15, 10, 10, 0),
                        //WidthRequest = 100
                    };

                    var confImage = new ImageCircle
                    {
                        HorizontalOptions = LayoutOptions.Center,
                        VerticalOptions = LayoutOptions.Center,
                        Aspect = Aspect.AspectFill,
                        WidthRequest = 100,
                        HeightRequest = 100
                    };
                    confImage.IsCircle = false;
                    confImage.Source = WorkspaceData[workspaceCount].PhotoLinks;

                    confImageLayout.Children.Add(confImage);
                    workspaceGrid.Children.Add(confImageLayout, 0, 0);

                    Grid GridRow2ndColum = new Grid
                    {
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        Padding = new Thickness(0, 10, 0, 10),
                        MinimumWidthRequest = 160
                    };
                    GridRow2ndColum.RowSpacing = 0;
                    GridRow2ndColum.ColumnSpacing = 0;

                    GridRow2ndColum.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
                    GridRow2ndColum.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
                    GridRow2ndColum.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
                    GridRow2ndColum.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });

                    var workspaceNameLayout = new StackLayout
                    {
                        VerticalOptions = LayoutOptions.StartAndExpand,
                        HorizontalOptions = LayoutOptions.StartAndExpand,
                        Orientation = StackOrientation.Horizontal,
                        Padding = new Thickness(0, 0, 0, 0)
                    };

                    var workspaceName = new Label
                    {
                        VerticalOptions = LayoutOptions.Center,
                        HorizontalOptions = LayoutOptions.StartAndExpand,
                        TextColor = Color.FromHex("#767676"),
                        FontSize = 14,
                    };
                    workspaceName.Text = WorkspaceData[workspaceCount].WorkspaceName;

                    workspaceNameLayout.Children.Add(workspaceName);
                    GridRow2ndColum.Children.Add(workspaceNameLayout, 0, 0);

                    var capabilitiesLayout = new StackLayout
                    {
                        VerticalOptions = LayoutOptions.Center,
                        HorizontalOptions = LayoutOptions.StartAndExpand,
                        Orientation = StackOrientation.Horizontal,
                        Padding = new Thickness(0, 8, 0, 0)
                    };

                    if (WorkspaceData[workspaceCount].IconUri != null)
                    {
                        for (int capabilitiesCount = 0; capabilitiesCount < WorkspaceData[workspaceCount].IconUri.Count; capabilitiesCount++)
                        {
                            var capabilty = new Image
                            {
                                HorizontalOptions = LayoutOptions.Start,
                                VerticalOptions = LayoutOptions.Center,
                                Aspect = Aspect.Fill,
                                HeightRequest = 15,
                                WidthRequest = 15
                            };
                            capabilty.Source = WorkspaceData[workspaceCount].IconUri[capabilitiesCount].IconUri;

                            capabilitiesLayout.Children.Add(capabilty);
                        }
                    }

                    var boxView = new BoxView
                    {
                        WidthRequest = 1,
                        HeightRequest = 15,
                        BackgroundColor = Color.Black,
                    };
                    capabilitiesLayout.Children.Add(boxView);

                    var capacityIcon = new Image
                    {
                        HorizontalOptions = LayoutOptions.Start,
                        VerticalOptions = LayoutOptions.Center,
                        Aspect = Aspect.Fill,
                        HeightRequest = 15,
                        WidthRequest = 15
                    };
                    capacityIcon.Source = "icon_invited.png";

                    capabilitiesLayout.Children.Add(capacityIcon);

                    var capacityLabel = new Label
                    {
                        VerticalOptions = LayoutOptions.Center,
                        HorizontalOptions = LayoutOptions.StartAndExpand,
                        TextColor = Color.FromHex("#767676"),
                        FontSize = 12
                    };
                    capacityLabel.Text = WorkspaceData[workspaceCount].Capacity.ToString();

                    capabilitiesLayout.Children.Add(capacityLabel);

                    GridRow2ndColum.Children.Add(capabilitiesLayout, 0, 1);


                    var ratingsLayout = new StackLayout
                    {
                        VerticalOptions = LayoutOptions.StartAndExpand,
                        HorizontalOptions = LayoutOptions.Start,
                        Orientation = StackOrientation.Horizontal,
                        Padding = new Thickness(0, 8, 0, 0)
                    };

                    var ratingLabel = new Label
                    {
                        VerticalOptions = LayoutOptions.Center,
                        FontSize = 10,
                        HorizontalOptions = LayoutOptions.StartAndExpand,
                        TextColor = Color.FromHex("#333333"),
                        //Text = "5"
                    };
                    ratingLabel.Text = WorkspaceData[workspaceCount].Ratings.ToString();
                    ratingsLayout.Children.Add(ratingLabel);


                    var starratingsLayout = new StackLayout
                    {
                        VerticalOptions = LayoutOptions.StartAndExpand,
                        HorizontalOptions = LayoutOptions.Start,
                        Orientation = StackOrientation.Horizontal,
                        Padding = new Thickness(0, 0, 0, 0)
                    };
                    for (int starRatingCount = 0; starRatingCount < WorkspaceData[workspaceCount].Ratings; starRatingCount++)
                    {
                        var ratingIcon = new Image
                        {
                            HorizontalOptions = LayoutOptions.Start,
                            VerticalOptions = LayoutOptions.Center,
                            Aspect = Aspect.Fill
                        };
                        ratingIcon.Source = "icon_starrating.png";

                        starratingsLayout.Children.Add(ratingIcon);
                    }
                    for (int emptyStarRatingCount = 0; emptyStarRatingCount < (5 - WorkspaceData[workspaceCount].Ratings); emptyStarRatingCount++)
                    {
                        var emptyRatingIcon = new Image
                        {
                            HorizontalOptions = LayoutOptions.Start,
                            VerticalOptions = LayoutOptions.Center,
                            Aspect = Aspect.Fill
                        };
                        emptyRatingIcon.Source = "icon_starratingempty.png";

                        starratingsLayout.Children.Add(emptyRatingIcon);
                    }

                    ratingsLayout.Children.Add(starratingsLayout);

                    var ratingCountLabel = new Label
                    {
                        VerticalOptions = LayoutOptions.Center,
                        HorizontalOptions = LayoutOptions.StartAndExpand,
                        FontSize = 10,
                        TextColor = Color.FromHex("#767676"),
                    };
                    ratingCountLabel.Text = WorkspaceData[workspaceCount].DisplayRatingCount;
                    ratingsLayout.Children.Add(ratingCountLabel);

                    GridRow2ndColum.Children.Add(ratingsLayout, 0, 2);

                    var buildingLayout = new StackLayout
                    {
                        VerticalOptions = LayoutOptions.Center,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        Orientation = StackOrientation.Horizontal,
                        Padding = new Thickness(0, 8, 0, 0)
                    };

                    var buildingLabel = new Label
                    {
                        VerticalOptions = LayoutOptions.Center,
                        HorizontalOptions = LayoutOptions.StartAndExpand,
                        FontSize = 12,
                        TextColor = Color.FromHex("#767676"),
                    };
                    buildingLabel.Text = WorkspaceData[workspaceCount].Building;
                    buildingLayout.Children.Add(buildingLabel);

                    var distanceIcon = new Image
                    {
                        HorizontalOptions = LayoutOptions.Start,
                        VerticalOptions = LayoutOptions.Center,
                        Aspect = Aspect.Fill
                    };
                    distanceIcon.Source = "icon_distance.png";

                    buildingLayout.Children.Add(distanceIcon);

                    var distanceLabel = new Label
                    {
                        VerticalOptions = LayoutOptions.Center,
                        HorizontalOptions = LayoutOptions.StartAndExpand,
                        FontSize = 12,
                        TextColor = Color.FromHex("#767676"),
                    };
                    distanceLabel.Text = Math.Round((WorkspaceData[workspaceCount].Distance), 2).ToString() + " mile";
                    buildingLayout.Children.Add(distanceLabel);

                    GridRow2ndColum.Children.Add(buildingLayout, 0, 3);

                    workspaceGrid.Children.Add(GridRow2ndColum, 1, 0);

                    workspaceView.Content = workspaceGrid;

                    var gi = new GestureFrame
                    {
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        Padding = new Thickness(0, 0, 0, 0),
                        BackgroundColor = Color.White,
                        Spacing = -40,
                        Orientation = StackOrientation.Horizontal
                    };

                    gi.Children.Add(workspaceView);

                    var reserveView = new ContentView
                    {
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        HorizontalOptions = LayoutOptions.EndAndExpand,
                        Padding = new Thickness(0, 0, 0, 0),
                        IsVisible = false,
                        StyleId = WorkspaceData[workspaceCount].WorkspaceId.ToString()
                    };

                    var reserveLayout = new StackLayout
                    {
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        HorizontalOptions = LayoutOptions.CenterAndExpand,
                        Padding = new Thickness(20, 0, 20, 0),
                        BackgroundColor = Color.FromHex("#03a9f4"),
                        MinimumWidthRequest = 120,
                        InputTransparent = true,
                        StyleId = WorkspaceData[workspaceCount].WorkspaceId.ToString()
                    };
                    var reserveLabel = new Label
                    {
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        HorizontalOptions = LayoutOptions.CenterAndExpand,
                        FontSize = 14,
                        TextColor = Color.White,
                        BackgroundColor = Color.FromHex("#03a9f4"),
                        Text = "RESERVE",
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalTextAlignment = TextAlignment.Center,
                        StyleId = WorkspaceData[workspaceCount].WorkspaceId.ToString()
                    };

                    reserveLayout.Children.Add(reserveLabel);

                    var tapGestureOnReserveLabel = new TapGestureRecognizer();
                    tapGestureOnReserveLabel.Tapped += async (s, e) =>
                    {
                        try
                        {
                            await Task.Run(() =>
                            {
                                IsBusy = true;
                            });
                            await Task.Delay(200);

                            var layout = s as Label;
                            var workdpaceID = layout.StyleId;
                            ReserveWorkspace(workdpaceID);
                            //RefreshListView();
                            //GetReservedMeetingsCount();

                            await Task.Run(() =>
                            {
                                IsBusy = false;
                            });
                        }
                        catch (Exception ex)
                        {

                        }


                    };
                    tapGestureOnReserveLabel.NumberOfTapsRequired = 1;
                    reserveLabel.GestureRecognizers.Add(tapGestureOnReserveLabel);

                    var tapGestureOnReserve = new TapGestureRecognizer();
                    tapGestureOnReserve.Tapped += async (s, e) =>
                    {
                        try
                        {
                            await Task.Run(() =>
                            {
                                IsBusy = true;
                            });
                            await Task.Delay(200);
                            var layout = s as StackLayout;
                            var workdpaceID = layout.StyleId;
                            ReserveWorkspace(workdpaceID);
                            //RefreshListView();
                            //GetReservedMeetingsCount();
                            await Task.Run(() =>
                            {
                                IsBusy = false;
                            });
                        }
                        catch (Exception ex)
                        {

                        }


                    };
                    tapGestureOnReserve.NumberOfTapsRequired = 1;
                    reserveLayout.GestureRecognizers.Add(tapGestureOnReserve);

                    reserveView.Content = reserveLayout;
                    var tap = new TapGestureRecognizer();
                    tap.Tapped += async (s, e) =>
                    {
                        try
                        {
                            await Task.Run(() =>
                            {
                                IsBusy = true;
                            });
                            await Task.Delay(200);
                            var layout = s as ContentView;
                            var workdpaceID = layout.StyleId;
                            ReserveWorkspace(workdpaceID);
                            //RefreshListView();
                            //GetReservedMeetingsCount();
                            //LoadPage();
                            await Task.Run(() =>
                            {
                                IsBusy = false;
                            });
                        }
                        catch (Exception ex)
                        {

                        }

                    };
                    tap.NumberOfTapsRequired = 1;
                    reserveView.GestureRecognizers.Add(tap);

                    gi.Children.Add(reserveView);

                    gi.SwipeLeft += async (s, e) =>
                    {
                        try
                        {
                            var stackSwiped = (StackLayout)s;
                            var reserveCont = (ContentView)stackSwiped.Children[1];
                            var layoutCont = (ContentView)stackSwiped.Children[0];

                            Grid grid = (Grid)WorkspaceListContainer.Children[0];
                            for (int i = 0; i < grid.Children.Count; i++)
                            {
                                StackLayout outer = (StackLayout)grid.Children[i];
                                StackLayout inner = (StackLayout)outer.Children[0];
                                StackLayout view = (StackLayout)inner.Children[0];

                                ContentView contentView2 = (ContentView)view.Children[1];
                                if (contentView2.IsVisible && contentView2.StyleId != reserveCont.StyleId)
                                {

                                    ContentView contentView1 = (ContentView)view.Children[0];
                                    await contentView1.TranslateTo(0, 0, 50);
                                    contentView2.IsVisible = false;

                                }
                                else if (contentView2.StyleId == reserveCont.StyleId)
                                {
                                    await layoutCont.TranslateTo(-100, 0, 50);
                                    reserveCont.IsVisible = true;

                                }
                            }
                        }
                        catch (Exception ex)
                        {

                        }

                    };

                    gi.SwipeRight += async (s, e) =>
                    {
                        try
                        {
                            var stack = (StackLayout)s;

                            var reserveCont = (ContentView)stack.Children[1];
                            var layoutCont = (ContentView)stack.Children[0];
                            if (reserveCont.IsVisible)
                            {
                                await layoutCont.TranslateTo(0, 0, 50);
                                reserveCont.IsVisible = false;
                            }

                        }
                        catch (Exception ex)
                        {

                        }

                    };

                    wholeLayout.Children.Add(gi);

                    wholeBoxLayout.Children.Add(wholeLayout);
                    var box = new BoxView
                    {
                        HeightRequest = 1,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        VerticalOptions = LayoutOptions.Start,
                        BackgroundColor = Color.FromHex("#f0f0f0"),
                    };

                    wholeBoxLayout.Children.Add(box);


                    ComponentGrid.Children.Add(wholeBoxLayout, 0, workspaceCount);
                }
                WorkspaceListContainer.Children.Add(ComponentGrid);

            }
            catch (Exception ex)
            {

            }

        }
        public void ReserveWorkspace(string workdpaceID)
        {
            _navigationService.NavigateTo(Constants.InvitePage);
            App.Locator.InvitePageViewModelInstance.workdpaceID = workdpaceID;
            //try
            //{
            //    WorkspaceModel selectedWorkspace = new WorkspaceModel();
            //    int id = 0;
            //    for (int i = 0; i < WorkspaceRecord.Count; i++)
            //    {
            //        if (WorkspaceRecord[i].WorkspaceId == Convert.ToInt32(workdpaceID))
            //        {
            //            id = i;
            //            break;
            //        }
            //    }
            //    Event reservingEvent = new Event();
            //    reservingEvent.Workspace = new Workspace();
            //    reservingEvent.EventCalendar = new Models.Calendar();
            //    reservingEvent.PrimaryOwner = new User();
            //    reservingEvent.Workspace.Id = WorkspaceRecord[id].WorkspaceId;
            //    reservingEvent.Workspace.OrgId = WorkspaceRecord[id].OrgId;
            //    reservingEvent.CreatedDateTime = DateTime.UtcNow;
            //    var currentstartTime = new DateTime(WorkspaceSearchDate.Year, WorkspaceSearchDate.Month, WorkspaceSearchDate.Day, WorkspaceStartTime.Hours, WorkspaceStartTime.Minutes, 0);
            //    reservingEvent.StartTime = currentstartTime.ToUniversalTime();
            //    var currentEndTime = new DateTime(WorkspaceSearchDate.Year, WorkspaceSearchDate.Month, WorkspaceSearchDate.Day, WorkspaceEndTime.Hours, WorkspaceEndTime.Minutes, 0);
            //    reservingEvent.EndTime = currentEndTime.ToUniversalTime();
            //    reservingEvent.Id = 1;
            //    reservingEvent.Importance = ImportanceType.Normal;
            //    reservingEvent.PrimaryOwner.Id = 1;
            //    reservingEvent.WebLink = new Uri("http://meelo.io");
            //    reservingEvent.CheckinTime = DateTime.UtcNow;
            //    reservingEvent.CheckoutTime = DateTime.UtcNow;
            //    reservingEvent.OnlineMeetingUrl = "http://meelo.io";
            //    reservingEvent.EventCalendar.Id = 1;
            //    reservingEvent.Subject = WorkspaceRecord[id].WorkspaceName;
            //    reservingEvent.PrimaryOwner.ObjectId = App.UniqueId;
            //    _reserveService = new ReserveWorkspaceApi();
            //    var returnedEvent = await _reserveService.ReserveSelectedWorkspace(reservingEvent);
            //    if (returnedEvent != null && returnedEvent.IsCallSuccessful)
            //    {
            //        await _dialogService.ShowMessage("Workspace " + reservingEvent.Subject + " Reserved", "Success!");
            //    }
            //    else
            //    {
            //        await _dialogService.ShowMessage(returnedEvent.Message, "Failed!");
            //    }
            //    return true;
            //}
            //catch (Exception ex)
            //{
            //    return false;
            //}
        }
        #endregion Listview Operations
        #endregion Functions

    }
}
