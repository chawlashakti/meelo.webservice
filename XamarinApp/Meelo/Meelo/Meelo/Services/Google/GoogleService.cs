﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Meelo.Services
{
    public class GoogleService
    {
        public async Task<string> GetEmailAsync(string tokenType, string accessToken)
        {
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);
            var json = await httpClient.GetStringAsync("https://www.googleapis.com/userinfo/email?alt=json");
            var email = JsonConvert.DeserializeObject<GoogleEmail>(json);
            return email.Data.Email;
        }
        public async Task<GoogleUserInfo> GetUserInfoAsync(string tokenType, string accessToken)
        {
            GoogleUserInfo user = null;
            try
            {
                var httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);
                var json = await httpClient.GetStringAsync("https://www.googleapis.com/oauth2/v1/userinfo?alt=json");
                user = JsonConvert.DeserializeObject<GoogleUserInfo>(json);
            }
            catch(Exception ex)
            { }
            return user;
        }
    }
}
