﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meelo.Services
{
    public class GoogleEmail
    {
        public GoogleEmailData Data { get; set; }
    }

    public class GoogleEmailData
    {
        public string Email { get; set; }
    }
    public class GoogleUserInfo
    {
        public string id { get; set; }
        public string email { get; set; }
        public string verified_email { get; set; }
        public string name { get; set; }
        public string given_name { get; set; }
        public string family_name { get; set; }
        public string picture { get; set; }
        public string locale { get; set; }
    }
    public class GoogleConfiguration
    {
        public string ClientId { get; set; }
        public string RedirectUrl{get;set;}
        //public const string ClientIdiOS = "27210327045-9v06b4g37k7nrt2l0h9397clpi4ont4t.apps.googleusercontent.com";
        //public const string ClientIdDroid = "27210327045-2h6ntvdrvfc4pi4idsupqma9mpc4qnqh.apps.googleusercontent.com";
        public const string Scope = "profile";
        //public const string RedirectUrl = "com.googleusercontent.apps.27210327045-9v06b4g37k7nrt2l0h9397clpi4ont4t:/oauth2redirect";
    }
}
