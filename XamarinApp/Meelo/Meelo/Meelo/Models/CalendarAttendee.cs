﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Meelo.Models
{
    public class CalendarAttendee
    {
        [DataMember(Name = "EmailAddress")]
        public CalendarEmailAddress EmailAddress { get; set; }
        [DataMember(Name = "Type")]
        public string Type { get; set; }
    }
    [DataContract]
    public class CalendarEmailAddress
    {
        [DataMember(Name = "Address")]
        public string Address { get; set; }
        [DataMember(Name = "Name")]
        public string Name { get; set; }
    }
}
