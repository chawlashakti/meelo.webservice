﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meelo.Models
{
    public class GetReservationsOutput
    {
        public IEnumerable<Event> Reservations { get; set; }

        public int ReservationCount { get; set; }
    }
}
