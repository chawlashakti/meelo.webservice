﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meelo.Models
{
    public class Attendee
    {
        public User User { get; set; }

        public AcceptanceType AcceptanceType { get; set; }

        public bool IsOrganizer { get; set; }

        public GpsLocation ReplyLocation { get; set; }

        public DateTimeOffset ReplyTime { get; set; }

        public AttendanceType AttendanceType { get; set; }

    }

    public enum AcceptanceType
    {
        Accepted = 1,
        Declined = -1,
        Tentative = 0
    }
    public enum AttendanceType
    {
        Required = 1,
        Optional = 2,
        External = 3
    }
}
