﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meelo.Models
{
    public class GpsLocation
    {
        public string Location { get; set; }

        public decimal? Lat { get; set; }

        public decimal? Long { get; set; }
    }
}
