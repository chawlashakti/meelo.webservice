﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meelo.Models
{
    public class OutputBaseClass
    {
        public bool IsCallSuccessful { get; set; }

        public string Message { get; set; }
    }
}
