﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meelo.Models
{
    public class Capabilities
    {
        public int CapabilitiesId { get; set; }
        public int CapabilityId { get; set; }
        public string EnablementProcedure { get; set; }
        public int CapabilitiesListId { get; set; }
        public string Name { get; set; }
        public string IconUri { get; set; }
        public string Description { get; set; }
    }
}
