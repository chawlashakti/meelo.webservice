﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meelo.Models
{
    public class BuildingViewModel
    {
        public long Id { get; set; }

        public string Building { get; set; }

        public long OrgId { get; set; }

        public string PostalCode { get; set; }

        public GpsLocation gpsLocation { get; set; }

        public int WorkspaceCount { get; set; }
    }
}
