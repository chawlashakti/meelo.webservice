﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meelo.Models
{
    public interface IService
    {
        string Name { get; set; }

        string Description { get; set; }

        ServiceProviderType ServiceType { get; set; }

        Uri SourceUri { get; set; }

        Uri NavigationUri { get; set; }

        AuthenticationInput AuthInfo { get; set; }

        IDictionary<string, string> Tags { get; set; }

        bool IsLocationDependent { get; set; }

        GpsLocation AvailabilityRange { get; set; }

        Uri IconUri { get; set; }

        bool AppliesToAll { get; set; }
    }

    public enum ServiceProviderType
    {
        Document = 1,
        Email = 2,
        Conferencing = 3,
        Weather = 4,
        Food = 5,
        Notes = 6,
        Actions = 7,
        Recording = 8,
        CRM = 9,
        ChatChannel = 10
    }
    public class AuthenticationInput
    {

    }
}
