﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meelo.Models
{
    public class Review
    {
        public int RatingCount { get; set; }
        public int WorkspaceId { get; set; }
    }
}
