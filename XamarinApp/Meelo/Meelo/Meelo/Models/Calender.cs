﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meelo.Models
{
    public class Calendar
    {

        public string Color { get; set; }
        public long Id { get; set; }
        public string Name { get; set; }

        public IEnumerable<Event> Events { get; set; }
    }
}
