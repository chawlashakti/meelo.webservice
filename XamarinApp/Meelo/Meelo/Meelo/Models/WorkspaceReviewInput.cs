﻿using System;
namespace Meelo.Models
{
    public class WorkspaceReviewInput
    {
        public long WorkspaceId { get; set; }
        public string ObjectId { get; set; }
        public int Rating { get; set; }
        public string Comments { get; set; }
        public long OrgId { get; set; }
    }
}
