﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Meelo.Models
{
    [DataContract]
    public class CalendarBody
    {

        [DataMember(Name = "ContentType")]
        public string ContentType { get; set; }

        [DataMember(Name = "Content")]
        public string Content { get; set; }
    }
}
