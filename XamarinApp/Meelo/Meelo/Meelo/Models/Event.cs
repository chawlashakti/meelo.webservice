﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meelo.Models
{
    public class Event
    {
        public string CalenderEventId { get; set; }

        public Workspace Workspace { get; set; }

        public string Subject { get; set; }

        public DateTime CreatedDateTime { get; set; }
        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public string Body { get; set; }

        public string BodyPreview { get; set; }

        public IEnumerable<string> Categories { get; set; }

        public string iCalUId { get; set; }

        public long Id { get; set; }

        public string RefId { get; set; }

        /// <summary>
        /// Low, Normal, High.
        /// </summary>
        public ImportanceType Importance { get; set; }

        public User PrimaryOwner { get; set; }
        public bool IsOrganizer { get; set; }

        public PatterenedRecurrrence PatterenedRecurrence { get; set; }

        /// <summary>
        /// Normal, Personal, Private, Confidential.
        /// </summary>
        public SensitivityType Sensitivity { get; set; }

        /// <summary>
        /// The event type: SingleInstance = 0, Occurrence = 1, Exception = 2, SeriesMaster = 3. Possible values are: SingleInstance, Occurrence, Exception, SeriesMaster.
        /// </summary>
        public EventType Type { get; set; }

        public Uri WebLink { get; set; }

        public IEnumerable<Attendee> Attendees { get; set; }

        public IEnumerable<IService> Services { get; set; }

        public IDictionary<string, string> Tags { get; set; }

        public DateTime? CheckinTime { get; set; }

        public DateTime? CheckoutTime { get; set; }

        public long CheckinPerformedBy { get; set; }

        public long CheckoutPerformedBy { get; set; }

        public string OnlineMeetingUrl { get; set; }
        public Calendar EventCalendar { get; set; }
    }

    public enum ImportanceType
    {
        Low = 0,
        Normal = 1,
        High = 2
    }

    public enum SensitivityType
    {
        Normal = 0,
        Personal = 1,
        Private = 2,
        Confidential = 3
    }

    public enum EventType
    {
        SingleInstance = 0, Occurrence = 1, Exception = 2, SeriesMaster = 3
    }
}
