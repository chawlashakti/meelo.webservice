﻿using System;
namespace Meelo.Models
{
    public class GoogleProfile
    {
        public string Kind { get; set; }
        public string Etag { get; set; }
        public string Occupation { get; set; }
        public string Gender { get; set; }
        public string ObjectType { get; set; }
        public string Id { get; set; }
        public string DisplayName { get; set; }
        public Name Name { get; set; }
        public string Tagline { get; set; }
        public string Url { get; set; }
       
    }

    public class Name
    {
        public string FamilyName { get; set; }
        public string GivenName { get; set; }
    }
}
