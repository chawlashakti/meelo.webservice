﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meelo.Models
{
    public class Workspace
    {
        public long OrgId { get; set; }

        public long Id { get; set; }

        public string Name { get; set; }

        public string PhotoLinks { get; set; }

        public List<Capabilites> IconUri { get; set; }
    }
}
