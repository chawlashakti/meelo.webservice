﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meelo.Models
{
    public class WorkspaceModel
    {
        public decimal Distance { get; set; }

        public List<Capabilites> IconUri { get; set; }

        public int RowId { get; set; }

        public bool ReserveWorkspaceVisible { get; set; }

        public int? RatingCount { get; set; }
        public string DisplayRatingCount { get; set; }

        public int WorkspaceCount { get; set; }
        public string BuildingId { get; set; }
        public long OrgId { get; set; }
        public string Street { get; set; }
        public string Building { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string LocationString { get; set; }
        public double Lat { get; set; }
        public double Long { get; set; }
        public int? PrimaryContactId { get; set; }
        public string Description { get; set; }
        public string MapsUri { get; set; }
        public bool DoFloorsStartAtGround { get; set; }
        public long WorkspaceId { get; set; }
        public string WorkspaceName { get; set; }
        public string WsDescription { get; set; }
        public string FriendlyName { get; set; }
        public string IdmResourceIdentity { get; set; }
        public int? FloorId { get; set; }
        public string WsCity { get; set; }
        public string WsCountry { get; set; }
        public string WsState { get; set; }
        public string WsPostalCode { get; set; }
        public object WsLocationString { get; set; }
        public string WsLat { get; set; }
        public string WsLong { get; set; }
        public string WebLink { get; set; }
        public string PhotoLinks { get; set; }
        public int? Ratings { get; set; }
        public decimal DisplayRatings { get; set; }


        public DateTime? Created { get; set; }
        public DateTime? LastUpdated { get; set; }
        public DateTime? NextReservationStartTime { get; set; }
        public DateTime? NextreservationEndTime { get; set; }
        public string LimitedAccessRoles { get; set; }
        public string LimitedAccessUsers { get; set; }
        public string Tags { get; set; }
        public string WorkspaceMapsUri { get; set; }
        public int? WorkspaceType { get; set; }
        public int? OccupancyStatus { get; set; }
        public int? Capacity { get; set; }
        public DateTime? LastRenovated { get; set; }
        public string CommunicationInfo_Email { get; set; }
        public string CommunicationInfo_PhoneNumber { get; set; }
        public string CommunicationInfo_TelephonyDetails { get; set; }
        public string CommunicationInfo_AccessDescription { get; set; }
        public string CommunicationInfo_OtherInfo { get; set; }
        public bool IsAutoCheckinEnabled { get; set; }
        public bool IsAutoCheckoutEnabled { get; set; }
        public int? AccessType { get; set; }
        public long MaintenanceOwner { get; set; }
        public string AreaInSqFt { get; set; }
        public string DistanceFromFloorOrigin { get; set; }
        public bool HasControllableDoor { get; set; }
        public string BeaconUid { get; set; }
        public bool IsBlocked { get; set; }
        public bool IsHidden { get; set; }
        public string ObjectId { get; set; }
        public object CapabilitiesId { get; set; }
        public object CapabilityId { get; set; }
        public object EnablementProcedure { get; set; }
        public object Name { get; set; }
        //public object IconUri { get; set; }
        public object CapabilityDescription { get; set; }
        public string TenantId { get; set; }
    
        public Location GeoLocation { get; set; }
    }
    public class WorkspaceBuilding
    {
        

        public List<WorkspaceModel> value { get; set; }

        [JsonProperty("@odata.count")]
        public string DataCount;

        private string Count { get; set; }
        public string count
        {
            get
            {
                return Count;
            }

            set
            {
                if (String.IsNullOrEmpty(DataCount))
                    DataCount = value;
            }
        }
    }

    public class Location
    {
        public string type { get; set; }
        public List<double> coordinates { get; set; }
    }
}
