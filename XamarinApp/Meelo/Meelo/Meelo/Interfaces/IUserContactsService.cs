﻿using Meelo.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Meelo.Interfaces
{
    public interface IUserContactsService { IEnumerable<PhoneContact> GetAllContacts(); }
}
