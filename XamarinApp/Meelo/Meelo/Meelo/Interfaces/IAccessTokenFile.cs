﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meelo.Interfaces
{
    public interface IAccessTokenFile
    {
        void CreateLogin(string fileName, string[] textLines);
        string[] LoadText(string filename);
        void DeleteData(string fileName);
    }
}
