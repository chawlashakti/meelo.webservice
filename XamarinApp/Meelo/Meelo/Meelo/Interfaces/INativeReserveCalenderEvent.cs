﻿using Meelo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Meelo.Interfaces
{
    public interface INativeReserveCalenderEvent
    {
        Task<ServiceInfo> GetServiceInfo(string calender);
        Task<HttpResponseMessage> SendRequestAsync(string accessToken, string resourceId, HttpClient client, Func<HttpRequestMessage> requestCreator);

    }
}
