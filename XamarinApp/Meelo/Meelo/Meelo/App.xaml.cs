﻿using CommonServiceLocator;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using Meelo.Authentication;
using Meelo.Helper;
using Meelo.Views;
using Microsoft.Identity.Client;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using static Meelo.Helper.Constants;

namespace Meelo
{
	public partial class App : Application
	{
        
        public static LoginPlatform LoggedInPlatform;
        public static PublicClientApplication PCA = null;
        public static GoogleAuthenticator Auth;
        public static UIParent UiParent = null;
        public static Position position;
        public static string AccessToken { get; set; }
        public static string UniqueId { get; set; }
        public static DateTimeOffset ExpiresOn { get; set; }
        public static string TenantId { get; set; }
        public static string Email { get; set; }
        public static string UserName { get; set; }
        public static bool isCheckInCancelClicked;
        
        private static ViewModelLocator _locator;
        public static ViewModelLocator Locator
        {
            get
            {
                return _locator ?? (_locator = new ViewModelLocator());
            }
        }
        public static INavigationService NavInitiate;
        public static IDialogService DialgInitiate;
        public App ()
		{
			InitializeComponent();
            GetGPSCordinates();
            PCA = new PublicClientApplication(Helper.Constants.ClientAppId)
            {
                RedirectUri = $"msal{Helper.Constants.ClientAppId}://auth"
            };
            
            _locator = Locator;
            if (PCA.Users.Count() > 0)
                GetTokenSilently();
            var navigationService = (NavigationService)SimpleIoc.Default.GetInstance<INavigationService>();
            var dialogService = (DialogService)SimpleIoc.Default.GetInstance<IDialogService>();
            NavInitiate = (NavigationService)SimpleIoc.Default.GetInstance<INavigationService>();
            DialgInitiate = (DialogService)SimpleIoc.Default.GetInstance<IDialogService>();
            NavigationPage firstPage = null;
            if (App.AccessToken == null)
                firstPage = new NavigationPage(new LoginOptionsPage());
            else
                firstPage = new NavigationPage(new MainPage());

            navigationService.Initialize(firstPage);
            dialogService.Initialize(firstPage);
            MainPage = firstPage;

		}
        
        private async void GetTokenSilently()
        {
            try
            {
                Microsoft.Identity.Client.AuthenticationResult authResult = await App.PCA.AcquireTokenSilentAsync(Helper.Constants.Scopes, App.PCA.Users.FirstOrDefault());
                App.AccessToken = authResult.AccessToken;
                App.TenantId = authResult.TenantId;
                App.UniqueId = authResult.UniqueId;
                App.UserName = authResult.User.Name;
                App.ExpiresOn = authResult.ExpiresOn;
            }
            catch (Exception ex)
            { }

        }
        public static async Task<Position> GetCurrentPosition()
        {
            Position position = null;
            try
            {
                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 100;

                position = await locator.GetLastKnownLocationAsync();

                if (position != null)
                {
                    //got a cahched position, so let's use it.
                    return position;
                }

                if (!locator.IsGeolocationAvailable || !locator.IsGeolocationEnabled)
                {
                    //not available or enabled
                    return null;
                }

                position = await locator.GetPositionAsync(TimeSpan.FromSeconds(20), null, true);

            }
            catch (Exception ex)
            {
                //Debug.WriteLine("Unable to get location: " + ex);
            }

            if (position == null)
                return null;

            //var output = string.Format("Time: {0} \nLat: {1} \nLong: {2} \nAltitude: {3} \nAltitude Accuracy: {4} \nAccuracy: {5} \nHeading: {6} \nSpeed: {7}",
            //        position.Timestamp, position.Latitude, position.Longitude,
            //        position.Altitude, position.AltitudeAccuracy, position.Accuracy, position.Heading, position.Speed);

            //Debug.WriteLine(output);

            return position;
        }
        public async void GetGPSCordinates()
        {
            position = await GetCurrentPosition();
            //position = new Position {Latitude= 18.5741873,Longitude= 73.8793412 }; 
            //await locator.GetPositionAsync(TimeSpan.FromSeconds(20));
        }
        protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
        
        
    }
}
