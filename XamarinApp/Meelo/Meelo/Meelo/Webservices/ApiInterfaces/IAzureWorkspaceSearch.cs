﻿using Meelo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meelo.Webservices
{
    public interface IAzureWorkspaceSearch
    {
        Task<List<AzureSearchModel>> SearchWorkspaceFromAzure(string searchedString);

        Task<List<AzureSearchModel>> ExecSuggest(string q);
    }
}
