﻿using Meelo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meelo.Webservices
{
    public interface IUserWorkspace
    {
        Task<WorkspaceBuilding> GetListViewWorkspaces(double latitude,double longitude,DateTime currentFilterStartTime, DateTime currentFilterEndTime, long buildingId, string searchText);

        Task<WorkspaceBuilding> SearchWorkspacesListDetails(int rowId);

        Task<GetReservationsOutput> GetMeetingList(string startTime,string endTime);

        Task<GetReservationsOutput> GetMeetingCount(string startTime, string endTime);
    }
}
