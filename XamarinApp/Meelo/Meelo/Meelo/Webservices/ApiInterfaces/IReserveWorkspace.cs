﻿using Meelo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meelo.Webservices
{
    public interface IReserveWorkspace
    {
        Task<ReservationOutput> ReserveSelectedWorkspace(Event reserveWorkspace,string Attendees);

        Task<CancelReservationOutput> CancelSelectedWorkspace(Event reserveWorkspace);

    }
}
