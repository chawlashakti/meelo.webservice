﻿using Meelo.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Meelo.Webservices
{
    public class AzureWorkspaceSearchApi : IAzureWorkspaceSearch
    {
        public async Task<List<AzureSearchModel>> SearchWorkspaceFromAzure(string searchedString)
        {
            HttpClient client = new HttpClient();
            client.MaxResponseContentBufferSize = 999999;
            List<AzureSearchModel> searchResults = new List<AzureSearchModel>();
            try
            {
                //string restURL = "https://meelosearchbasic.search.windows.net/indexes/workspacesearchindex/docs/suggest?api-version=2016-09-01";
                string restURL = "https://meelosearchbasic.search.windows.net/indexes/indworkspacesearch/docs/suggest?api-version=2016-09-01";

                client.DefaultRequestHeaders.TryAddWithoutValidation("api-key", "D16F28ACA1F70AF47BEA15F5D5E0CF21");

                var uri = new Uri(string.Format(restURL, string.Empty));

                string body = "{\"search\":\"" + searchedString + "\",\"top\":\"5\",\"suggesterName\":\"sgworkspacesearch\",\"filter\":\"TenantId eq '" + App.TenantId + "'\"}";

                var content = new StringContent(body, Encoding.UTF8, "application/json");

                //var content = new StringContent("{\"Location\":\"null\",\"Lat\":18.577312,\"Long\":73.874196}", Encoding.UTF8, "application/json");
                HttpResponseMessage response = null;

                response = client.PostAsync(uri, content).Result;
                if (response.IsSuccessStatusCode)
                {
                    var contentResponse = await response.Content.ReadAsStringAsync();

                    var obj = JsonConvert.DeserializeObject<SearchResult>(contentResponse);
                    searchResults = obj.value;
                }

            }
            catch (Exception ex)
            {
            }
            finally
            {
                client.Dispose();
            }
            return searchResults;

        }
        //public static string AzureSuggestUrl = "/indexes/listings/docs/suggest?suggesterName=sg&$top=15&search=";
        //private static Uri _serviceUri = new Uri("https://meelosearchbasic.search.windows.net/indexes/indworkspacesearch/docs/suggest?api-version=2016-09-01");
        // Below is a query key as well as a sample search service that you are free to use for demo or learning purposes

        private static HttpClient _httpClient;
        public async Task<List<AzureSearchModel>> ExecSuggest(string q)
        {
            try
            {
                // Execute /suggest API call to Azure Search and parse results
                //string url = _serviceUri + AzureSuggestUrl + q;
                //string url = "https://meelosearchbasic.search.windows.net/indexes/indworkspacesearch/docs/suggest?api-version=2016-09-01&suggesterName=sgworkspacesearch&$top=5&$filter=TenantId eq '" + App.userTenantId + "'&search=" + q;
                string url = "https://meelosearchbasic.search.windows.net/indexes/workspacesql-index/docs/suggest?api-version=2016-09-01&suggesterName=workspacesql-suggester&$top=10&$filter=TenantId eq '" + App.TenantId + "'&search=" + q;
                _httpClient = new HttpClient();
                Uri uri = new Uri(url);
                List<AzureSearchModel> Suggestions = new List<AzureSearchModel>();

                HttpResponseMessage response = AzureSearchHelper.SendSearchRequest(_httpClient, HttpMethod.Get, uri);
                AzureSearchHelper.EnsureSuccessfulSearchResponse(response);

                var obj = JsonConvert.DeserializeObject<AzureSearchResult>(response.Content.ReadAsStringAsync().Result);
                //Suggestions = obj.value;
                //for(int i=0; i< obj.value.Count;i++)
                //{
                //    string [] arrStr = Regex.Replace(obj.value[i].SearchText, @"(\[|\]|\"")", "").Split(',');
                //    for(int j=0;j<arrStr.Length;j++)
                //    {
                //        Suggestions.Add(new AzureSearchModel { BuildingId = obj.value[i].BuildingId, SearchText = arrStr[j] });
                //    }
                //}
                for (int i = 0; i < obj.value.Count; i++)
                {
                    string[] arrStr = obj.value[i].WorkspaceBldId.ToString().Split('-');

                    Suggestions.Add(new AzureSearchModel { BuildingId = Convert.ToInt32(arrStr[1]), SearchText = obj.value[i].SearchText });
                }
                return Suggestions.Distinct().OrderBy(x => x.SearchText).ToList();
                //return Suggestions;
            }
            catch (Exception e)
            {
                return null;
            }
        }
        //public async Task<List<AzureSearchModel>> ExecSuggest(string q)
        //{
        //    try
        //    {
        //        // Execute /suggest API call to Azure Search and parse results
        //        //string url = _serviceUri + AzureSuggestUrl + q;
        //        string url = "https://meelosearchbasic.search.windows.net/indexes/indworkspacesearch/docs/suggest?api-version=2016-09-01&suggesterName=sgworkspacesearch&$top=5&$filter=TenantId eq '" + App.userTenantId + "'&search=" + q;
        //        _httpClient = new HttpClient();
        //        Uri uri = new Uri(url);
        //        List<AzureSearchModel> Suggestions = new List<AzureSearchModel>();

        //        HttpResponseMessage response = AzureSearchHelper.SendSearchRequest(_httpClient, HttpMethod.Get, uri);
        //        AzureSearchHelper.EnsureSuccessfulSearchResponse(response);

        //        var obj = JsonConvert.DeserializeObject<SearchResult>(response.Content.ReadAsStringAsync().Result);
        //        Suggestions = obj.value;

        //        return Suggestions.Distinct().OrderBy(x => x.SearchText).ToList();
        //    }
        //    catch (Exception e)
        //    {
        //        return null;
        //    }
        //}
    }
}
