﻿using GalaSoft.MvvmLight.Views;
using Meelo.Helper;
using Meelo.Interfaces;
using Meelo.Models;
using Microsoft.Identity.Client;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Meelo.Webservices
{
    public class ReserveWorkspaceApi : IReserveWorkspace
    {
        private IDialogService _dialogueService;

        internal static string discoveryServiceResourceId = "https://api.office.com/discovery/";

        public static string tenantAuthority = "https://login.microsoftonline.com/{0}";
        public static string Authority
        {
            get
            {
                return string.Format(tenantAuthority, App.TenantId);
            }
        }

        public async Task<ReservationOutput> ReserveSelectedWorkspace(Event reserveWorkspace, string Attendees)
        {
            //reserveWorkspace.StartTime = DateTime.UtcNow.AddMinutes(4);
            //reserveWorkspace.EndTime = DateTime.UtcNow.AddMinutes(6);
            reserveWorkspace.StartTime = reserveWorkspace.StartTime.AddSeconds(-reserveWorkspace.StartTime.Second);
            reserveWorkspace.EndTime = reserveWorkspace.EndTime.AddSeconds(-reserveWorkspace.EndTime.Second);
            HttpClient httpClient = new HttpClient();
            httpClient.MaxResponseContentBufferSize = 999999;
            _dialogueService = App.DialgInitiate;
            Event taskModels = new Event();
            ReservationOutput outputObj = new ReservationOutput();

            try
            {
                CalendarEvent calender = new CalendarEvent();
                calender.Body = new CalendarBody();
                string[] emails = new string[10];

                calender.Attendees = null;
                calender.Start = reserveWorkspace.StartTime.ToString("O");
                calender.End = reserveWorkspace.EndTime.ToString("O");
                calender.Body = new CalendarBody();
                calender.Body = new CalendarBody
                {
                    Content = "Worspace reserved",
                    ContentType = "HTML"
                };
                //calender.Subject = "Meelo Event";
                calender.Subject = reserveWorkspace.Subject + " Reserved";

                DateTime startDateTime;
                DateTime endDateTime;
                bool IsAllDayMeeting = false;
                if (IsAllDayMeeting)
                {
                    startDateTime = reserveWorkspace.StartTime;
                    endDateTime = reserveWorkspace.EndTime.AddDays(1);
                }
                else
                {
                    startDateTime = (reserveWorkspace.StartTime).ToUniversalTime();
                    endDateTime = (reserveWorkspace.EndTime).ToUniversalTime();
                }
                var eventId = await CalendarHelper.CreateEventAsync(startDateTime, endDateTime, "Worspace reserved", "Microsoft", calender.Subject, Attendees, IsAllDayMeeting);

                if (eventId != null)
                {
                    reserveWorkspace.CalenderEventId = eventId;

                    string restURL = Constants.ApiBaseUrl + "/Workspace/Reserve";

                    var json = JsonConvert.SerializeObject(reserveWorkspace);
                    HttpContent httpContent = new StringContent(json);
                    httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    //httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + App.userAccessToken);
                    HttpResponseMessage result = httpClient.PostAsync(restURL, httpContent).Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var contents = await result.Content.ReadAsStringAsync();
                        outputObj = JsonConvert.DeserializeObject<ReservationOutput>(contents);
                        taskModels = outputObj.Reservation;

                        if (outputObj.IsCallSuccessful)
                        {
                            //var eventId = await CalendarHelper.CreateEventAsync(startDateTime, endDateTime, "Worspace reserved", "Microsoft", calender.Subject, "", IsAllDayMeeting);
                        }
                        else
                        {
                            var deletedId = await CalendarHelper.DeleteEventAsync(reserveWorkspace.CalenderEventId);
                        }

                    }
                }
                return outputObj;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                httpClient.Dispose();
            }
        }
        public static async Task<CalendarEvent> AddCalendarEvent(CalendarEvent calendarEvent)
        {
            var auth = DependencyService.Get<INativeReserveCalenderEvent>();
            var serviceInfo = await auth.GetServiceInfo("Calendar");

            //var serviceInfo = await AuthenticationHelper.GetServiceInfo("Calendar");

            var requestUrl = String.Format(CultureInfo.InvariantCulture,
                "{0}/me/events", serviceInfo.ApiEndpoint);

            string postData = JsonConvert.SerializeObject(calendarEvent);

            postData = postData.Replace("null", string.Empty);

            Func<HttpRequestMessage> requestCreator = () =>
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, requestUrl);
                //request.Headers.Add("Accept", "application/json");
                request.Headers.Add("Accept", "application/json;odata.metadata=minimal");

                request.Content = new StringContent(postData, Encoding.UTF8, "application/json");

                return request;
            };

            var obj = DependencyService.Get<INativeReserveCalenderEvent>();
            var httpClient = new HttpClient();



            //var response = await obj.SendRequestAsync(serviceInfo.AccessToken,
            //  serviceInfo.ResourceId, httpClient, requestCreator);


            using (var request = requestCreator.Invoke())
            {
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", serviceInfo.AccessToken);
                request.Headers.Add("X-ClientService-ClientTag", "Office 365 API Tools 1.1.0612");
                var resp = await httpClient.SendAsync(request);

                // Check if the server responded with "Unauthorized". If so, it might be a real authorization issue, or 
                //     it might be due to an expired access token. To be sure, renew the token and try one more time:
                if (resp.StatusCode == HttpStatusCode.Unauthorized)
                {
                    string authority = string.Format(CultureInfo.InvariantCulture, tenantAuthority, Settings.TenantId);
                    var authContext = new AuthenticationContext(authority);
                    //var token = await GetAccessToken(serviceInfo.ResourceId);

                    // Create and send a new request:
                    using (HttpRequestMessage retryRequest = requestCreator.Invoke())
                    {
                        retryRequest.Headers.Authorization = new AuthenticationHeaderValue("Bearer", serviceInfo.AccessToken);
                        retryRequest.Headers.Add("X-ClientService-ClientTag", "Office 365 API Tools 1.1.0612");
                        resp = await httpClient.SendAsync(retryRequest);
                    }
                }

                // Return either the original response, or the response from the second attempt:
                //return resp;




                string responseString = await resp.Content.ReadAsStringAsync();

                var c = JsonConvert.DeserializeObject<CalendarEvent>(responseString);

                return c;
            }
        }

        public async Task<CancelReservationOutput> CancelSelectedWorkspace(Event reserveWorkspace)
        {
            HttpClient httpClient = new HttpClient();
            httpClient.MaxResponseContentBufferSize = 999999;
            _dialogueService = App.DialgInitiate;
            CancelReservationOutput taskModels = new CancelReservationOutput();

            try
            {
                string restURL = Helper.Constants.ApiBaseUrl + "/Workspace/Cancel";

                var json = JsonConvert.SerializeObject(reserveWorkspace);
                HttpContent httpContent = new StringContent(json);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                //httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + App.userAccessToken);
                HttpResponseMessage result = httpClient.PostAsync(restURL, httpContent).Result;
                if (result.IsSuccessStatusCode)
                {
                    var contents = await result.Content.ReadAsStringAsync();

                    taskModels = JsonConvert.DeserializeObject<CancelReservationOutput>(contents);

                    if (taskModels.IsCallSuccessful)
                    {
                        var eventId = await CalendarHelper.DeleteEventAsync(reserveWorkspace.CalenderEventId);
                        if (!App.isCheckInCancelClicked)
                            await _dialogueService.ShowMessage(taskModels.Message, "Success!");

                    }
                    else
                        await _dialogueService.ShowMessage(taskModels.Message, "Error!");

                }
                return taskModels;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                httpClient.Dispose();
            }
        }

    }
}
