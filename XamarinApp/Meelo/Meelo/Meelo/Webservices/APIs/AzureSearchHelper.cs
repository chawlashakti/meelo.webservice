﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Meelo.Webservices
{
    public class AzureSearchHelper
    {
        // This sample uses the prototype API.  Please review the documenation for details on this API version - http://msdn.microsoft.com/en-us/library/azure/dn864560.aspx
        //api-version=2016-09-01
        //public const string ApiVersionString = "api-version=2015-02-28-Preview";
        public const string ApiVersionString = "api-version=2016-09-01";


        private static readonly JsonSerializerSettings _jsonSettings;

        static AzureSearchHelper()
        {
            _jsonSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented, // for readability, change to None for compactness
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                DateTimeZoneHandling = DateTimeZoneHandling.Utc
            };

            _jsonSettings.Converters.Add(new StringEnumConverter());
        }

        public static string SerializeJson(object value)
        {
            return JsonConvert.SerializeObject(value, _jsonSettings);
        }

        public static T DeserializeJson<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json, _jsonSettings);
        }

        public static HttpResponseMessage SendSearchRequest(HttpClient client, HttpMethod method, Uri uri, string json = null)
        {
            try
            {
                UriBuilder builder = new UriBuilder(uri);
                //string separator = string.IsNullOrWhiteSpace(builder.Query) ? string.Empty : "&";
                //builder.Query = builder.Query.TrimStart('?') + separator + ApiVersionString;

                var request = new HttpRequestMessage(method, builder.Uri);
                request.Headers.Add("api-key", "3DC7DA69B7821AF751BA770E2F985491");
                //3DC7DA69B7821AF751BA770E2F985491
                //D16F28ACA1F70AF47BEA15F5D5E0CF21
                if (json != null)
                {
                    request.Content = new StringContent(json, Encoding.UTF8, "application/json");
                }

                return client.SendAsync(request).Result;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static void EnsureSuccessfulSearchResponse(HttpResponseMessage response)
        {
            if (!response.IsSuccessStatusCode)
            {
                string error = response.Content == null ? null : response.Content.ReadAsStringAsync().Result;
                throw new Exception("Search request failed: " + error);
            }
        }
    }
}
