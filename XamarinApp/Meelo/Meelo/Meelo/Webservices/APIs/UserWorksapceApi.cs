﻿using Meelo.Helper;
using Meelo.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Meelo.Webservices
{
    public class UserWorksapceApi : IUserWorkspace
    {

        public async Task<WorkspaceBuilding> GetListViewWorkspaces(double latitude, double longitude, DateTime currentFilterStartTime, DateTime currentFilterEndTime, long buildingId, string searchText)
        {
            HttpClient client = new HttpClient();
            client.MaxResponseContentBufferSize = 999999;
            List<WorkspaceModel> workspaceList = new List<WorkspaceModel>();

            WorkspaceBuilding buildingData = new WorkspaceBuilding();

            try
            {
                string restURL = Constants.ApiBaseUrl + "/Available/Workspaces";

                //string restURL = "http://192.168.1.56:88/Available/Workspaces";
                //client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                var uri = new Uri(string.Format(restURL, string.Empty));

                //string body = "{\"TenantId\":\"" + App.userTenantId + "\",\"Lat\":18.515603,\"Long\":73.781905,\"StartTime\":\"2018-02-09 13:02:00.00\",\"EndTime\":\"2018-02-09 14:02:00.00\",\"BuildingId\":0,\"SearchText\":\"\"}";
                string body = "{\"TenantId\":\"" + App.TenantId + "\",\"Lat\":" + latitude + ",\"Long\":" + longitude + ",\"StartTime\":\"" + currentFilterStartTime.ToString("yyyy-MM-dd HH:mm:ss") + "\",\"EndTime\":\"" + currentFilterEndTime.ToString("yyyy-MM-dd HH:mm:ss") + "\",\"BuildingId\":" + buildingId + ",\"SearchText\":\"" + searchText + "\"}";
                //string body= "{\"TenantId\":\"a52683c3-dd28-454d-af9f-6a08bc1dcf44\",\"Lat\":18.5761510857513,\"Long\":73.8759450203706,\"StartTime\":\"2017-09-29 13:02:00.00\",\"EndTime\":\"2017-09-29 14:02:00.00\",\"BuildingId\":0,\"SearchText\":\"\"}";


                var content = new StringContent(body, Encoding.UTF8, "application/json");

                HttpResponseMessage response = null;

                response = client.PostAsync(uri, content).Result;
                if (response.IsSuccessStatusCode)
                {
                    var contentResponse = await response.Content.ReadAsStringAsync();
                    buildingData = JsonConvert.DeserializeObject<WorkspaceBuilding>(contentResponse);

                    workspaceList = buildingData.value;
                }
                return buildingData;

            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                //client.Dispose();
            }

        }
        //public async Task<WorkspaceBuilding> GetListViewWorkspaces()
        //{
        //    HttpClient client = new HttpClient();
        //    client.MaxResponseContentBufferSize = 999999;
        //    List<WorkspaceModel> workspaceList = new List<WorkspaceModel>();

        //    WorkspaceBuilding buildingData = new WorkspaceBuilding ();

        //    try
        //    {
        //        string restURL = "https://meelosearchbasic.search.windows.net/indexes/idxworkspace/docs/search?api-version=2016-09-01";
        //        client.DefaultRequestHeaders.TryAddWithoutValidation("api-key", "D16F28ACA1F70AF47BEA15F5D5E0CF21");
        //        client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

        //        var uri = new Uri(string.Format(restURL, string.Empty));

        //        string body = "{\"count\":\"true\",\"filter\":\"TenantId eq '" + App.userTenantId + "' and geo.distance(GeoLocation, geography'POINT("+App.position.Longitude+" "+ App.position.Latitude+")') le 2\",\"search\":\"*\"}";

        //        var content = new StringContent(body, Encoding.UTF8, "application/json");

        //        HttpResponseMessage response = null;

        //        response = client.PostAsync(uri, content).Result;
        //        if (response.IsSuccessStatusCode)
        //        {
        //            var contentResponse = await response.Content.ReadAsStringAsync();
        //            buildingData = JsonConvert.DeserializeObject<WorkspaceBuilding>(contentResponse);

        //            workspaceList = buildingData.value;
        //        }
        //        //HttpResponseMessage response = null;

        //        //response = client.GetAsync(uri).Result;

        //        //if (response.IsSuccessStatusCode)
        //        //{
        //        //    var contentResponse = await response.Content.ReadAsStringAsync();
        //        //    workspaceList = JsonConvert.DeserializeObject<List<WorkspaceModel>>(contentResponse);
        //        //}
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    finally
        //    {
        //        client.Dispose();
        //    }
        //    return buildingData;

        //}

        public async Task<WorkspaceBuilding> SearchWorkspacesListDetails(int rowId)
        {
            HttpClient client = new HttpClient();
            client.MaxResponseContentBufferSize = 999999;
            List<WorkspaceModel> workspaceList = new List<WorkspaceModel>();

            WorkspaceBuilding buildingData = new WorkspaceBuilding();

            try
            {
                string restURL = "https://meelosearchbasic.search.windows.net/indexes/idxworkspace/docs/search?api-version=2016-09-01";
                client.DefaultRequestHeaders.TryAddWithoutValidation("api-key", "D16F28ACA1F70AF47BEA15F5D5E0CF21");
                client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                var uri = new Uri(string.Format(restURL, string.Empty));


                string body = "{\"count\":\"true\",\"filter\":\"TenantId eq '" + App.TenantId + "' and BuildingId eq " + rowId + "\",\"search\":\"*\"}";

                var content = new StringContent(body, Encoding.UTF8, "application/json");

                HttpResponseMessage response = null;

                response = client.PostAsync(uri, content).Result;
                if (response.IsSuccessStatusCode)
                {
                    var contentResponse = await response.Content.ReadAsStringAsync();
                    buildingData = JsonConvert.DeserializeObject<WorkspaceBuilding>(contentResponse);
                    workspaceList = buildingData.value;
                }

            }
            catch (Exception ex)
            {
            }
            finally
            {
                client.Dispose();
            }
            return buildingData;

        }


        public async Task<GetReservationsOutput> GetMeetingList(string startTime, string endTime)
        {
            HttpClient client = new HttpClient();
            client.MaxResponseContentBufferSize = 999999;

            GetReservationsOutput meetingData = new GetReservationsOutput();

            try
            {
                string restURL = Constants.ApiBaseUrl + "/Workspace/GetReservations";
                client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                var uri = new Uri(string.Format(restURL, string.Empty));


                string body = "{\"ReservationIdFilter\":\"null\",\"StartTime\":\"" + startTime + "\",\"EndTime\":\"" + endTime + "\",\"UserIdFilter\":\"" + App.UniqueId + "\",\"WorkspaceIdFilter\":\"null\",\"u\":\"null\",\"RequestFrom\":\"null\",\"OrgId\":\"null\",\"TenantId\":\"" + App.TenantId + "\" }";

                var content = new StringContent(body, Encoding.UTF8, "application/json");

                HttpResponseMessage response = null;

                response = client.PostAsync(uri, content).Result;
                if (response.IsSuccessStatusCode)
                {
                    var contentResponse = await response.Content.ReadAsStringAsync();
                    contentResponse = contentResponse.Replace("\\", "");
                    contentResponse = contentResponse.Replace("\"{", "{");
                    contentResponse = contentResponse.Replace("}\"", "}");
                    meetingData = JsonConvert.DeserializeObject<GetReservationsOutput>(contentResponse);
                }

            }
            catch (Exception ex)
            {
            }
            finally
            {
                client.Dispose();
            }
            return meetingData;

        }

        public async Task<GetReservationsOutput> GetMeetingCount(string startTime, string endTime)
        {
            HttpClient client = new HttpClient();
            client.MaxResponseContentBufferSize = 999999;
            GetReservationsOutput meetingCount = new GetReservationsOutput();

            try
            {
                string restURL = Constants.ApiBaseUrl + "/Workspace/GetReservationCount";
                client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                var uri = new Uri(string.Format(restURL, string.Empty));

                string body = "{\"ReservationIdFilter\":\"null\",\"StartTime\":\"" + startTime + "\",\"EndTime\":\"" + endTime + "\",\"UserIdFilter\":\"" + App.UniqueId + "\",\"WorkspaceIdFilter\":\"null\",\"u\":\"null\",\"RequestFrom\":\"null\",\"OrgId\":\"null\" }";

                var content = new StringContent(body, Encoding.UTF8, "application/json");

                HttpResponseMessage response = null;

                response = client.PostAsync(uri, content).Result;
                if (response.IsSuccessStatusCode)
                {
                    var contentResponse = await response.Content.ReadAsStringAsync();
                    //contentResponse = contentResponse.Replace("\\", "");
                    //contentResponse = contentResponse.Replace("\"{", "{");
                    //contentResponse = contentResponse.Replace("}\"", "}");
                    meetingCount = JsonConvert.DeserializeObject<GetReservationsOutput>(contentResponse);
                }

            }
            catch (Exception ex)
            {
            }
            finally
            {
                client.Dispose();
            }
            return meetingCount;

        }
    }
}
