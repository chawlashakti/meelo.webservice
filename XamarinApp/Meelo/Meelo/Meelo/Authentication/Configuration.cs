﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meelo.Authentication
{
    public static class Configuration
    {
        public const string ClientIdiOS = "27210327045-9v06b4g37k7nrt2l0h9397clpi4ont4t.apps.googleusercontent.com";
        public const string ClientIdDroid = "27210327045-2h6ntvdrvfc4pi4idsupqma9mpc4qnqh.apps.googleusercontent.com";
        
        public const string RedirectUrl = "com.googleusercontent.apps.27210327045-9v06b4g37k7nrt2l0h9397clpi4ont4t:/oauth2redirect";
    }
}
