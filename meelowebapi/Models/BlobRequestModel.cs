﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace meelowebapi.Models
{
    public class BlobRequestModel
    {
        string ContainerName { get; set; }
        string ContainerUrl { get; set; }
        string DirectoryName { get; set; }
        string DirectoryUrl { get; set; }
    }
}