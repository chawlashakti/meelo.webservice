﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace meelowebapi.Models
{
    public class WorkspaceImageRequestModel
    {
        public string OrgId { get; set; }
        public string WorkspaceId { get; set; }
        public string Url { get; set; }
    }
}