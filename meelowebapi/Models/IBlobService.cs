﻿using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace meelowebapi.Models
{
    public interface IBlobService
    {
        Task<List<BlobUploadModel>> UploadBlobs(HttpContent httpContent);
        Task<BlobDownloadModel> DownloadBlob(WorkspaceImageRequestModel Input);
    }

    public class BlobService : IBlobService
    {
        public async Task<List<BlobUploadModel>> UploadBlobs(HttpContent httpContent)
        {
            var blobUploadProvider = new BlobStorageUploadProvider();

            var list = await httpContent.ReadAsMultipartAsync(blobUploadProvider)
                .ContinueWith(task =>
                {
                    if (task.IsFaulted || task.IsCanceled)
                    {
                        throw task.Exception;
                    }

                    var provider = task.Result;
                    return provider.Uploads.ToList();
                });

            // TODO: Use data in the list to store blob info in your
            // database so that you can always retrieve it later.

            return list;
        }
        public async Task<BlobDownloadModel> DownloadBlob(WorkspaceImageRequestModel Input)
        {

           
            string ContainerName = "org-" + Input.OrgId;
            string DirectoryName = "Workspace-" +Input.WorkspaceId;
            string urlMatch = "";
            if (Input.Url.EndsWith("/"))
                urlMatch = Input.Url + "img-1";
            else
                urlMatch = Input.Url + "/img-1";


            if (!String.IsNullOrEmpty(Input.OrgId))
            {
                var container = BlobHelper.GetBlobContainer(ContainerName);
                var directory = container.GetDirectoryReference("WorkspaceImages");//.GetDirectoryReference(DirectoryName);
                // Loop over items within the container.

                foreach (CloudBlockBlob blob in container.ListBlobs(directory.Prefix,true))
                {
                    string extension = System.IO.Path.GetExtension(blob.StorageUri.PrimaryUri.ToString());
                    if (blob.StorageUri.PrimaryUri.ToString() == urlMatch+extension)
                    {
                        var ms = new MemoryStream();
                        await blob.DownloadToStreamAsync(ms);

                        var lastPos = blob.Name.LastIndexOf('/');
                        var fileName = blob.Name.Substring(lastPos + 1, blob.Name.Length - lastPos - 1);

                        // Build and return the download model with the blob stream and its relevant info
                        var download = new BlobDownloadModel
                        {
                            BlobStream = ms,
                            BlobFileName = fileName,
                            BlobLength = blob.Properties.Length,
                            BlobContentType = blob.Properties.ContentType
                        };

                        return download;
                    }
                }
                return null;
            }

            // Otherwise
            return null;
        }
        #region Commented code
        //public async Task<BlobDownloadModel> DownloadBlob(string ContainerName)
        //{
        //    // TODO: You must implement this helper method. It should retrieve blob info
        //    // from your database, based on the blobId. The record should contain the
        //    // blobName, which you should return as the result of this helper method.
        //    var blobName = "Images/Workspace-1.jpg";// GetBlobName(blobId);

        //    if (!String.IsNullOrEmpty(blobName))
        //    {
        //        var container = BlobHelper.GetBlobContainer(ContainerName);
        //        var directory = container.GetDirectoryReference("WorkspaceImages");

        //        // Loop over items within the container.
        //        foreach (CloudBlockBlob item in directory.ListBlobs())//container.ListBlobs(null, false))
        //        {
        //            if (item.GetType() == typeof(CloudBlockBlob))
        //            {
        //                CloudBlockBlob blob = (CloudBlockBlob)item;
        //                //var ms1= new MemoryStream();
        //                //await blob1.DownloadToStreamAsync(ms1);
        //                //ms1.Position = 0;
        //                //System.IO.File.WriteAllBytes(@"D:\\filename", ms1.ToArray());
        //                // var blob = container.GetBlockBlobReference(blobName);

        //                // Download the blob into a memory stream. Notice that we're not putting the memory
        //                // stream in a using statement. This is because we need the stream to be open for the
        //                // API controller in order for the file to actually be downloadable. The closing and
        //                // disposing of the stream is handled by the Web API framework.
        //                var ms = new MemoryStream();
        //                await blob.DownloadToStreamAsync(ms);

        //                // Strip off any folder structure so the file name is just the file name
        //                var lastPos = blob.Name.LastIndexOf('/');
        //                var fileName = blob.Name.Substring(lastPos + 1, blob.Name.Length - lastPos - 1);

        //                // Build and return the download model with the blob stream and its relevant info
        //                var download = new BlobDownloadModel
        //                {
        //                    BlobStream = ms,
        //                    BlobFileName = fileName,
        //                    BlobLength = blob.Properties.Length,
        //                    BlobContentType = blob.Properties.ContentType
        //                };

        //                return download;
        //            }
        //        }
        //        return null;
        //    }

        //    // Otherwise
        //    return null;
        //}
        #endregion
    }
}
