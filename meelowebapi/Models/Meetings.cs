﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace meelowebapi.Models
{
    public class Capabilities
    {
        public int CapabilitiesId { get; set; }
        public int CapabilityId { get; set; }
        public string EnablementProcedure { get; set; }
        public int CapabilitiesListId { get; set; }
        public string Name { get; set; }
        public string IconUri { get; set; }
        public string Description { get; set; }
    }

    public class Review
    {
        public int RatingCount { get; set; }
        public int WorkspaceId { get; set; }
    }
    public class GeoLocation
    {
        public string type { get; set; }
        public List<double> coordinates { get; set; }
    }
    public class Value
    {
    public string EventId { get; set; }
    public int OrgId { get; set; }
    public object Subject { get; set; }
    public DateTime CreatedTime { get; set; }
    public DateTime StartTime { get; set; }
    public DateTime EndTime { get; set; }
    public object Body { get; set; }
    public object BodyPreview { get; set; }
    public object Categories { get; set; }
    public object iCalUid { get; set; }
    public object RefId { get; set; }
    public int Importance { get; set; }
    public int UserId { get; set; }
    public int Sensitivity { get; set; }
    public int EventType { get; set; }
    public string WebLink { get; set; }
    public object CheckinTime { get; set; }
    public object CheckoutTime { get; set; }
    public string CheckinPerformedBy { get; set; }
    public string CheckoutPerformedBy { get; set; }
    public int CalendarId { get; set; }
    public string OnlineMeetingUrl { get; set; }
    public GeoLocation GeoLocation { get; set; }
    public int BuildingId { get; set; }
    public string Street { get; set; }
    public string Building { get; set; }
    public string City { get; set; }
    public string Country { get; set; }
    public string State { get; set; }
    public string PostalCode { get; set; }
    public string LocationString { get; set; }
    public string Lat { get; set; }
    public string Long { get; set; }
    public int PrimaryContactId { get; set; }
    public string Description { get; set; }
    public string MapsUri { get; set; }
    public bool DoFloorsStartAtGround { get; set; }
    public int WorkspaceId { get; set; }
    public string WorkspaceName { get; set; }
    public string WsDescription { get; set; }
    public string FriendlyName { get; set; }
    public string IdmResourceIdentity { get; set; }
    public int Floor { get; set; }
    public string WsCity { get; set; }
    public string WsCountry { get; set; }
    public string WsState { get; set; }
    public string WsPostalCode { get; set; }
    public object WsLocationString { get; set; }
    public string WsLat { get; set; }
    public string WsLong { get; set; }
    public string WsWebLink { get; set; }
    public string PhotoLinks { get; set; }
    public int Ratings { get; set; }
    public DateTime Created { get; set; }
    public DateTime LastUpdated { get; set; }
    public DateTime NextReservationStartTime { get; set; }
    public DateTime NextreservationEndTime { get; set; }
    public string LimitedAccessRoles { get; set; }
    public string LimitedAccessUsers { get; set; }
    public string Tags { get; set; }
    public string WorkspaceMapsUri { get; set; }
    public int WorkspaceType { get; set; }
    public int OccupancyStatus { get; set; }
    public int Capacity { get; set; }
    public DateTime LastRenovated { get; set; }
    public string CommunicationInfo_Email { get; set; }
    public string CommunicationInfo_PhoneNumber { get; set; }
    public string CommunicationInfo_TelephonyDetails { get; set; }
    public string CommunicationInfo_AccessDescription { get; set; }
    public string CommunicationInfo_OtherInfo { get; set; }
    public bool IsAutoCheckinEnabled { get; set; }
    public bool IsAutoCheckoutEnabled { get; set; }
    public int AccessType { get; set; }
    public int MaintenanceOwner { get; set; }
    public string AreaInSqFt { get; set; }
    public string DistanceFromFloorOrigin { get; set; }
    public bool HasControllableDoor { get; set; }
    public string BeaconUid { get; set; }
    public bool IsBlocked { get; set; }
    public bool IsHidden { get; set; }
    public string ObjectId { get; set; }
    public List<Capabilities> Capabilities { get; set; }
    public List<Review> Review { get; set; }
}

public class RootObject
{

    public List<Value> value { get; set; }
}
}