﻿using Com.Meelo.BusinessServices.Reservation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace meelowebapi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Thread thread = new Thread(new ThreadStart(ThreadFunc));
            thread.IsBackground = true;
            thread.Name = "ThreadFunc";
            thread.Start();
        }

        protected void ThreadFunc()
        {
            System.Timers.Timer t = new System.Timers.Timer();

            t.Elapsed += new System.Timers.ElapsedEventHandler(TimerWorker);
            t.Interval = 60000;
            t.Enabled = true;
            t.AutoReset = true;
            t.Start();
        }
        protected void TimerWorker(object sender, System.Timers.ElapsedEventArgs e)
        {
            IReservationService iReservationService = new ReservationService();
            CancelReservationOutput Output = new CancelReservationOutput();
            try
            {

                Output = iReservationService.CancelReservation().Result;

            }
            catch (Exception ex)
            {
                Output.IsCallSuccessful = false;
                Output.Message = Com.Meelo.BusinessServices.Resource.CancelReservationException + " \n" + ex.Message;
            }
        }
    }
}
