﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using meelowebapi.code;
using System.Net.Http.Headers;
using System.Net.Http.Formatting;
using System.Web.Http.Routing;
using System.Net.Http;

namespace meelowebapi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            config.Routes.MapHttpRoute(
            "PostBlobUpload",
            "blobs/upload",
            new { controller = "Workspace", action = "PostBlobUpload" },
            new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) }
        );

           
            //GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;

            ////  GlobalConfiguration.Configuration.Filters.Add(new CustomHttpsAttribute());


            //GlobalConfiguration.Configuration.EnsureInitialized();

            ////To produce JSON format add this line of code
            //config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            var json = config.Formatters.JsonFormatter;
            json.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects;
            config.Formatters.Remove(config.Formatters.XmlFormatter);

//            GlobalConfiguration.Configuration.Formatters.JsonFormatter.MediaTypeMappings
//.Add(new RequestHeaderMapping("Accept",
//                              "text/html",
//                              StringComparison.InvariantCultureIgnoreCase,
//                              true,
//                              "application/json"));
        }
    }
}
