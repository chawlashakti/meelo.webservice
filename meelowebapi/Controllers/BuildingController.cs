﻿using Com.Meelo.BusinessServices.Building;
using Com.Meelo.CoreLib;
using Com.Meelo.CoreLib.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace meelowebapi.Controllers
{
    public class BuildingController : ApiController
    {
        IBuildingService BuildingServiceObj;
        [HttpPost]
        public IEnumerable<BuildingViewModel> Index(string TenantId, GpsLocation location)
        {
            BuildingServiceObj = new BuildingService();
            IEnumerable<BuildingViewModel> BuildingObj = BuildingServiceObj.GetBuildingsAndWorkspaceCount(TenantId, location).Result;
            return BuildingObj.ToList();
        }
    }
}
