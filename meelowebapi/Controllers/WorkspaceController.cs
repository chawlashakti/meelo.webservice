﻿using Com.Meelo.BusinessServices.Workspace;
using Com.Meelo.CoreLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Newtonsoft.Json;
using Com.Meelo.CoreLib.ViewModel;
using Microsoft.WindowsAzure.Storage;
using meelowebapi.Models;
using System.Web.Http.Description;
using System.Net.Http.Headers;
using System.Web;
using Microsoft.WindowsAzure.Storage.Blob;
using System.IO;
using Newtonsoft.Json.Linq;
using Com.Meelo.BusinessServices.Reservation;
using Com.Meelo.BusinessServices;

namespace meelowebapi.Controllers
{
    public class WorkspaceController : ApiController
    {
        IWorkspaceService WorkspaceServiceObj;
        IReservationService iReservationService;
        private readonly IBlobService _service = new BlobService();
        [HttpGet]
        [Route("Get/All")]
        public IEnumerable<Workspace> Index()
        {
            WorkspaceReviewInput input = new WorkspaceReviewInput();
            string inputjson = JsonConvert.SerializeObject(input);
            var path = HttpContext.Current.Server.MapPath("~/Containers");
            using (StreamReader r = new StreamReader(path+"/json.json"))
            {
                string json = r.ReadToEnd();

                json = json.Replace("\\", "");
                json = json.Replace("\"{", "{");
                json = json.Replace("}\"", "}");
                RootObject items = JsonConvert.DeserializeObject<RootObject>(json);
            }
            WorkspaceService service = new WorkspaceService();
          //  IEnumerable<Workspace> WorkspaceObj = service.GetAvailableWorkspaces("1", new Com.Meelo.CoreLib.User { Address = new Com.Meelo.CoreLib.Address { Country = "USA", State = "WA", City = "Redmond" } }, new Com.Meelo.CoreLib.GpsLocation { }).Result;
            return null;
        }
        [HttpPost]
        public IEnumerable<Workspace> GetWorksapceByBuilding(string TenantId,long BuildingId)
        {
            WorkspaceServiceObj = new WorkspaceService();
            IEnumerable<Workspace> WorkspaceList = WorkspaceServiceObj.GetWorkspacesByBuilding(0,TenantId,BuildingId).Result;
            return WorkspaceList.ToList();
        }
        [HttpPost]
        [Route("Available/Workspaces")]
        public WorkspaceBuildingOuputViewModel GetAvailableWorkspaces(BuildingWorkspaceRequestModel Input)
        {
            try
            {
                WorkspaceServiceObj = new WorkspaceService();
                var Output = WorkspaceServiceObj.GetAvailableWorkspaces(Input).Result;
                return Output;
            }
            catch(Exception ex)
            {
                WorkspaceBuildingOuputViewModel  ws= new WorkspaceBuildingOuputViewModel();
                ws.Message = ex.ToString();
                return ws;
            }
        }
        [HttpPost]
        [Route("Rate/Workspace")]
        public OutputBaseClass Rating(WorkspaceReviewInput input)
        {
            WorkspaceServiceObj = new WorkspaceService();
            var Output = WorkspaceServiceObj.RateWorkspace(input).Result;
            return Output;
        }

        [HttpPost]
        [Route("Workspace/Reserve")]
        public ReservationOutput Reserve(Event Input)
        {
            iReservationService  = new ReservationService();
            ReservationOutput Output = new ReservationOutput();
            try
            {
                if (Input.StartTime.DateTime > DateTime.UtcNow.AddMinutes(-5))
                {
                    Output = iReservationService.Reserve(Input).Result;
                }
                else
                {
                    Output.IsCallSuccessful = false;
                    Output.Message = Com.Meelo.BusinessServices.Resource.ReservationDateTimeValidationFailure;
                }
            }
            catch(Exception ex)
            {
                Output.IsCallSuccessful = false;
                Output.Message = Com.Meelo.BusinessServices.Resource.ReservationException + " \n" + ex.Message;
            }
            return Output;
        }
        [HttpPost]
        [Route("Workspace/GetReservations")]
        public GetReservationsOutput GetReservations(GetReservationsInput Input)
        {
            iReservationService  = new ReservationService();
            GetReservationsOutput Output = new GetReservationsOutput();
            try
            {
                Output = iReservationService.GetReservations(Input).Result;
            }
            catch (Exception ex)
            {
                Output.IsCallSuccessful = false;
                Output.Message = Com.Meelo.BusinessServices.Resource.GetReservationException + " \n" + ex.Message;
            }
            return Output;
        }
        [HttpPost]
        [Route("Workspace/GetReservationCount")]
        public GetReservationsOutput GetReservationCount(GetReservationsInput Input)
        {
            iReservationService = new ReservationService();
            GetReservationsOutput Output = new GetReservationsOutput();
            try
            {
                Output = iReservationService.GetReservationCount(Input).Result;
            }
            catch (Exception ex)
            {
                Output.IsCallSuccessful = false;
                Output.Message = ex.Message;
            }
            return Output;
        }
        [HttpPost]
        [Route("Workspace/Cancel")]
        public CancelReservationOutput CancelReservation(Event Input)
        {
            iReservationService = new ReservationService();
            CancelReservationOutput Output = new CancelReservationOutput();
            try
            {
                //if (Input.StartTime.DateTime > DateTime.UtcNow.AddMinutes(-1))
                //{
                    Output = iReservationService.CancelReservation(Input).Result;
                //}
                //else
                //{
                //    Output.IsCallSuccessful = false;
                //    Output.Message = Com.Meelo.BusinessServices.Resource.CancelReservationDateTimeValidationFailure;
                //}
            }
            catch (Exception ex)
            {
                Output.IsCallSuccessful = false;
                Output.Message =Com.Meelo.BusinessServices.Resource.CancelReservationException+" \n"+ex.Message;
            }
            return Output;
        }

        [HttpPost]
        [Route("Workspace/CheckIn")]
        public CheckinOutput CheckIn(Event Input)
        {
            iReservationService = new ReservationService();
            CheckinOutput Output = new CheckinOutput();

            try
            {
                CheckinInput Checkin = new CheckinInput();
                Checkin.Event = Input;
                Output = iReservationService.Checkin(Checkin).Result;
            }
            catch (Exception ex)
            {
                Output.IsCallSuccessful = false;
                Output.Message = Com.Meelo.BusinessServices.Resource.CheckInException + " \n" + ex.Message;
            }
            return Output;
        }
        [HttpPost]
        [Route("Workspace/CheckOut")]
        public CheckoutOutput CheckOut(Event Input)
        {
            iReservationService = new ReservationService();
            CheckoutOutput Output = new CheckoutOutput();

                try
                {
                    CheckoutInput Checkout = new CheckoutInput();
                    Checkout.Event = Input;
                    Output = iReservationService.Checkout(Checkout).Result;
                }
                catch (Exception ex)
                {
                    Output.IsCallSuccessful = false;
                    Output.Message = Com.Meelo.BusinessServices.Resource.CheckOutException + " \n" + ex.Message;
                }

            return Output;
        }
        [ResponseType(typeof(List<BlobUploadModel>))]
        public async Task<IHttpActionResult> PostBlobUpload()
        {
            try
            {
                // This endpoint only supports multipart form data
                if (!Request.Content.IsMimeMultipartContent("form-data"))
                {
                    return StatusCode(HttpStatusCode.UnsupportedMediaType);
                }

                // Call service to perform upload, then check result to return as content
                var result = await _service.UploadBlobs(Request.Content);
                if (result != null && result.Count > 0)
                {
                    return Ok(result);
                }

                // Otherwise
                return BadRequest();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        [HttpPost]
        [Route("workspace/Thumbimage")]
        public async Task<HttpResponseMessage> GetBlobDownload(WorkspaceImageRequestModel Input)
        {
            try
            {
                var result = await _service.DownloadBlob(Input);
                if (result == null)
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound);
                }

                // Reset the stream position; otherwise, download will not work
                result.BlobStream.Position = 0;

                // Create response message with blob stream as its content
                var message = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StreamContent(result.BlobStream)
                };

                // Set content headers
                message.Content.Headers.ContentLength = result.BlobLength;
                message.Content.Headers.ContentType = new MediaTypeHeaderValue(result.BlobContentType);
                message.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = HttpUtility.UrlDecode(result.BlobFileName),
                    Size = result.BlobLength
                };

                return message;
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Content = new StringContent(ex.Message)
                };
            }
        }
    }
}
