﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WikipediaNET;
using WikipediaNET.Enums;
using WikipediaNET.Objects;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Connector;

namespace meelo.Utils
{
    public static class WikipediaSearch
    {
        private static readonly Wikipedia wikipedia = new Wikipedia();

        
        public static IList<Attachment> WikipediaToAttachments(string searchText)
        {
            try
            {
                //Use HTTPS instead of HTTP
                wikipedia.UseTLS = true;

                //We would like 5 results
                wikipedia.Limit = 5;

                //We would like to search inside the articles
                wikipedia.What = What.Text;


                QueryResult results = wikipedia.Search(searchText);

                List<Attachment> wkUrls = new List<Attachment>();
                foreach (Search s in results.Search)
                {
                    //CardAction ci = new CardAction()
                    //{
                    //    Value = s.Url.ToString(),
                    //    Type = "openUrl",
                    //    Title = s.Title
                    //};
                    //// wkUrls.Add(DialogUtils.GetHeroCard(string.Empty, string.Empty, s.TitleSnippet, null, ci));
                    //wkUrls.Add(DialogUtils.GetThumbnailCard(string.Empty, string.Empty, s.TitleSnippet, null, ci));

                    wkUrls.Add(new Attachment()
                    {
                        ContentUrl = s.Url.ToString(),
                        ContentType = "text/html",
                        Name = s.Title,
                        Content = string.Empty //s.Snippet
                    });
                }


                return wkUrls;
             

            }
            catch (Exception)
            {

            }

            return null;

        }
    }
}