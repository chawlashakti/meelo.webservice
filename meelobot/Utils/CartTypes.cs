﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace meelo.Utils
{
    public enum CardType
    {
        HeroCard,
        ThumbnailCard,
        ReceiptCard,
        SignInCard
    }
}