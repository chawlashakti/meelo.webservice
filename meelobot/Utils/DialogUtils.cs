﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;

namespace meelo.Utils
{
    public static class DialogUtils
    {
        //https://www.sitepoint.com/web-foundations/mime-types-complete-list/

        public static Attachment GetHeroCard(string title, string subtitle, string text, CardImage cardImage, CardAction cardAction)
        {
            var heroCard = new HeroCard
            {
                Title = title,
                Subtitle = subtitle,
                Text = text,
              
            };

            if (cardImage != null && string.IsNullOrEmpty(text))
            {
                heroCard.Images = new List<CardImage>() { cardImage };
            }

            if(cardAction != null)
                heroCard.Buttons = new List<CardAction>() { cardAction };

            return heroCard.ToAttachment();

       
           
        }

        public static Attachment GetThumbnailCard(string title, string subtitle, string text, CardImage cardImage, CardAction cardAction)
        {
            var heroCard = new ThumbnailCard
            {
                Title = title,
                Subtitle = subtitle,
                Text = text,

            };

            if (cardImage != null && string.IsNullOrEmpty(text))
            {
                heroCard.Images = new List<CardImage>() { cardImage };
            }

            if (cardAction != null)
                heroCard.Buttons = new List<CardAction>() { cardAction };

            return heroCard.ToAttachment();

        }



        public static Attachment GetSigninCard(string text, string signInUrl, string signInText= "Sign-in")
        {
            var signinCard = new SigninCard
            {
                Text = text,
                Buttons = new List<CardAction> { new CardAction(ActionTypes.Signin, signInText, value: signInUrl) }
            };

            return signinCard.ToAttachment();
        }


        public static Attachment GetEmbeddedAttachment(string embeddedContent, string name, string contentType = "text/html")
        {
            return GetAttachment(embeddedContent, contentType, null, name);
        }
        public static Attachment GetUrlAttachment(string url, string name, string contentType=null)
        {
            return GetAttachment(null,contentType, url, name);
        }

        public static Attachment GetAttachment(object content, string contentType, string contentUrl, string name, string thumbnailUrl=null)
        {
            return new Attachment
            {
                 Content = content,
                  ContentType = contentType,
                   ContentUrl = contentUrl,
                    Name = name,
                     ThumbnailUrl= thumbnailUrl

            };

           
        }

    }
}