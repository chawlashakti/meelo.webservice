﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.Bot.Connector;
using Newtonsoft.Json;
using System.Web.Http.Tracing;
using Microsoft.Bot.Builder.Dialogs;
using meelobot.Dialogs;
using SimpleExpressionEvaluator;
using Com.Meelo.Utils.Text;
using System.Text.RegularExpressions;

namespace meelobot
{
    //https://codepen.io/lilgreenland/pen/pyVvqB

    [BotAuthentication]
    public class MessagesController : ApiController
    {
        private const string MENTION_REGEX = @"(\@\w+)";
        private const string HASHTAG_REGEX = @"(?<=#)\w+";

       // private static readonly Regex Regex = new Regex(MENTION_REGEX, RegexOptions.Compiled);
        /// <summary>
        /// POST: api/Messages
        /// Receive a message from a user and reply to it
        /// </summary>
        public async Task<HttpResponseMessage> Post([FromBody]Activity activity)
        {
            if (activity.Type == ActivityTypes.Message)
            {
                bool respond = false;
                bool mentionsExist = false;
                //Regex to extract mentions "(?<=@)([\\w-]+)", "(?<=[^\s])@\w+(?=[,\s])"
                //Allows Email: (?<!\w)@(\w+)/
                // await EvaluateMath(activity);

                //Mention Works 1: (@)((?:[A-Za-z0-9-_]*))
                //Mention Works 1: (\@\w+)
                //Hashtag 1: "(?<=#)\w+"

                //Hashtag 2 for length greater than 1 \B#\w\w+
                //Hashtag 3: (?<=\s|^)#(\w*[A-Za-z_]+\w*)

                //Parse URL: (http(s)?://)?([\w-]+\.)+[\w-]+(/\S\w[\w- ;,./?%&=]\S*)?
                try
                {

                   
                    //is this a group conversation?
                    ConversationAccount ca = activity.Conversation;
                    if (string.IsNullOrEmpty(ca.Name) || ca.IsGroup == false)
                    {
                        //if this is a private conversation. Respond
                        respond = true;
                    }
                    else
                    {

                        //For public conversation, check if @meelo was mentioned
                        Mention[] mentions = activity.GetMentions();
                        if (mentions != null && mentions.Length > 0)
                        {
                            mentionsExist = true;
                            foreach (Mention m in mentions)
                            {
                                string who = m.Mentioned.Name;
                                string id = m.Mentioned.Id;

                                if (id == activity.Recipient.Id)
                                {
                                    respond = true;
                                }
                            }
                        }
                    }

                    if (respond)
                    {
                        if (mentionsExist)
                        {
                            //Strip out all mentions
                           // activity.Text = RegExUtils.Replace(activity.Text, MENTION_REGEX, string.Empty);

                            //Strip out only @meelo
                            activity.Text = activity.Text.Replace(string.Format("@{0}", activity.Recipient.Name), string.Empty);
                        }
                        // Call NumberGuesserDialog that comes from the examples.
                        // await Conversation.SendAsync(activity, () => new NumberGuesserDialog());

                        //Math
                        //  await Conversation.SendAsync(activity, () => new MathSolverDialog());

                        await Conversation.SendAsync(activity, () => new WolframAlphaDialog());

                        //  await Conversation.SendAsync(activity, () => new CardsDialog());

                        // await Conversation.SendAsync(activity, () => new CarouselCardsDialog());
                    }
                }
                catch (Exception ex)
                {
                    LogError(ex);
                }


            }
            else
            {
               await HandleSystemMessage(activity);
            }
            var response = Request.CreateResponse(HttpStatusCode.OK);
            return response;
        }


        private async Task EvaluateMath(Activity activity)
        {
            ConnectorClient connector = new ConnectorClient(new Uri(activity.ServiceUrl));
            ExpressionEvaluator engine = new ExpressionEvaluator();
            string resultStr = string.Empty;
            try
            {

                decimal result = engine.Evaluate(activity.Text.Trim());

                resultStr = result.ToString();


            }
            catch (ArgumentException ax)
            {

                resultStr = Resources.Resource.mathsolverArgumentException;

            }
            finally
            {
                Activity reply = activity.CreateReply(resultStr);
                await connector.Conversations.ReplyToActivityAsync(reply);

            }
        }
        private async Task< Activity> HandleSystemMessage(Activity message)
        {
            if (message.Type == ActivityTypes.DeleteUserData)
            {
                // Implement user deletion here
                // If we handle user deletion, return a real message
                try
                {
                  
                    ConnectorClient connector = new ConnectorClient(new Uri(message.ServiceUrl));
                    Activity reply = message.CreateReply(string.Format(Resources.Resource.deleteuserdata, message.From.Name));
                    await connector.Conversations.ReplyToActivityAsync(reply);

                }catch(Exception ex)
                {
                    LogError(ex);
                }
            }
            else if (message.Type == ActivityTypes.ConversationUpdate)
            {
                // Handle conversation state changes, like members being added and removed
                // Use Activity.MembersAdded and Activity.MembersRemoved and Activity.Action for info
                // Not available in all channels
                try
                {
                    IConversationUpdateActivity update = message;
                    //var client = new ConnectorClient(new Uri(message.ServiceUrl), new MicrosoftAppCredentials());
                    var client = new ConnectorClient(new Uri(message.ServiceUrl));
                    if (update.MembersAdded != null && update.MembersAdded.Any())
                    {
                        foreach (var newMember in update.MembersAdded)
                        {
                            if (newMember.Id != message.Recipient.Id)
                            {
                                var reply = message.CreateReply();
                                reply.Text =string.Format(Resources.Resource.newmemberadded, newMember.Name);
                                await client.Conversations.ReplyToActivityAsync(reply);
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    LogError(ex);
                }
            }
            else if (message.Type == ActivityTypes.ContactRelationUpdate)
            {
                // Handle add/remove from contact lists
                // Activity.From + Activity.Action represent what happened

                try
                {
                    IContactRelationUpdateActivity update = message;
                    //var client = new ConnectorClient(new Uri(message.ServiceUrl), new MicrosoftAppCredentials());
                    ConnectorClient connector = new ConnectorClient(new Uri(message.ServiceUrl));
                    Activity reply = message.CreateReply(string.Format(Resources.Resource.contactrelationupdate, update.From.Name, update.Action));
                    await connector.Conversations.ReplyToActivityAsync(reply);

                }
                catch (Exception ex)
                {
                    LogError(ex);
                }
            }
            else if (message.Type == ActivityTypes.Typing)
            {
                // Handle knowing tha the user is typing
                // LogDebug("Typing...");

            }
            else if (message.Type == ActivityTypes.Ping)
            {
            }

            return null;
        }




        #region Logging
        private void LogError(Exception ex)
        {
            Configuration.Services.GetTraceWriter().Error(
           Request, "MessagesController", ex, ex.Message);
        }

        private void LogDebug(string message)
        {
            Configuration.Services.GetTraceWriter().Debug(
           Request, "MessagesController", message);
        }

        #endregion
    }
}