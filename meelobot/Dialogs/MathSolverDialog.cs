﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.Web;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using SimpleExpressionEvaluator;

namespace meelobot.Dialogs
{
    [Serializable]
    public class MathSolverDialog : IDialog<object>
    {
        private const string expr = "(5+3-5*4)-";
        protected long numberOfExpressions;

        public async Task StartAsync(IDialogContext context)
        {
            try
            {
                // Set Attempts
                this.numberOfExpressions = 0;
                Random random = new Random();
                string add = expr + random.Next(1, 10).ToString();
                ExpressionEvaluator engine = new ExpressionEvaluator();
               
                await context.PostAsync(string.Format(Resources.Resource.mathsolverStart, add, engine.Evaluate(add)));
            }
            finally
            {
                // Start the Game
                context.Wait(MessageReceivedAsync);
            }
        }

        public virtual async Task MessageReceivedAsync(IDialogContext context,
            IAwaitable<IMessageActivity> argument)
        {
            string resultStr = string.Empty;
            try
            {
                // Get the text passed
                var message = await argument;


               
                ExpressionEvaluator engine = new ExpressionEvaluator();


                try
                {
                    decimal result = engine.Evaluate(message.Text);

                    resultStr = result.ToString();

                    // await context.PostAsync(result.ToString());
                    // await context.FlushAsync(CancellationToken.None);

                }
                catch (ArgumentException ax)
                {
                    if (numberOfExpressions == 0)
                    {
                        resultStr = string.Format(Resources.Resource.mathsolverFirstArgumentExpression, message.Text);
                    }
                    else
                    {
                        resultStr = Resources.Resource.mathsolverArgumentException;
                    }

                }
                finally
                {
                    numberOfExpressions++;
                    await context.PostAsync(resultStr.ToString());
                }




            }
            catch (Exception ex)
            {
                //send s global message
                await context.PostAsync(Resources.Resource.generalError);
            }
            finally
            {

                context.Wait(MessageReceivedAsync);

            }
        }

        
    }
}