﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using WolframAlphaNET;
using WolframAlphaNET.Misc;
using WolframAlphaNET.Objects;
using System.Configuration;
using System.Text;
using meelo.Utils;

namespace meelobot.Dialogs
{
    [Serializable]
    public class WolframAlphaDialog : IDialog<object>
    {
        private string AppId = string.Empty;
        protected long numberOfExpressions;

        public async Task StartAsync(IDialogContext context)
        {
            try
            {
                // Set Attempts
                this.numberOfExpressions = 0;
                AppId = ConfigurationManager.AppSettings["WolframAlphaAppID"];
               // await context.PostAsync(string.Format(Resources.Resource.wolframStart));
            }
            finally
            {
                // Start the Game
              //  context.Wait(MessageReceivedAsync);

                context.Wait(MessageReceivedAsyncWithCards);
            }
        }

        public virtual async Task MessageReceivedAsync(IDialogContext context,
           IAwaitable<IMessageActivity> argument)
        {
            string resultStr = string.Empty;
            try
            {
                // Get the text passed
                var message = await argument;


                


                try
                {
                    string errorMessage;
                    IList<WolframResult> qr = CallWolframApi(message.Text, out errorMessage);

                    if (qr != null && qr.Count > 0)
                    {
                        resultStr = qr[0].PlainText;
                    }
                    else if (!string.IsNullOrEmpty(errorMessage))
                    {
                        resultStr = errorMessage;
                    }
                    else
                    {

                    }

                    // await context.PostAsync(result.ToString());
                    // await context.FlushAsync(CancellationToken.None);

                }
                catch (Exception ax)
                {

                    resultStr = Resources.Resource.wolframError;


                }
                finally
                {
                    numberOfExpressions++;
                    await context.PostAsync(resultStr.ToString());
                }




            }
            catch (Exception ex)
            {
                //send s global message
                await context.PostAsync(Resources.Resource.generalError);
            }
            finally
            {

                context.Wait(MessageReceivedAsync);

            }
        }

        public virtual async Task MessageReceivedAsyncWithCards(IDialogContext context,
            IAwaitable<IMessageActivity> argument)
        {
           
            try
            {
                // Get the text passed
                var message = await argument;



                var reply = context.MakeMessage();

                try
                {
                    string errorMessage;
   
                    reply.Attachments = CallWolframApiAndReturnCards(message.Text, out errorMessage);

                  

                    if (reply.Attachments.Count > 1)
                    {
                      
                       // reply.Attachments.Add(BotAds.GetAdAsAttachment());
                        reply.AttachmentLayout = AttachmentLayoutTypes.List;

                    }
                    else if (!string.IsNullOrEmpty(errorMessage))
                    {
                        reply.Text = errorMessage;
                        
                    }
                    else if (reply.Attachments.Count == 0)
                    {
                        reply.Text = Resources.Resource.wolframError;
                    }
                  


                 

                }
                catch (Exception ax)
                {

                    reply.Text = Resources.Resource.wolframError;


                }
                finally
                {
                    numberOfExpressions++;
                    await context.PostAsync(reply);
                }




            }
            catch (Exception ex)
            {
                //send s global message
                await context.PostAsync(Resources.Resource.generalError);
            }
            finally
            {

                context.Wait(MessageReceivedAsyncWithCards);

            }
        }

        #region WolframAlpha Utiliti Function

        private IList<WolframResult> CallWolframApi(string query, out string errorMessage)
        {
            errorMessage = string.Empty;
            WolframAlpha wolfram = new WolframAlpha(AppId);
            wolfram.ScanTimeout = 0.1f; //We set ScanTimeout really low to get a quick answer. See RecalculateResults() below.
            wolfram.UseTLS = true; //Use encryption

            //We search for something. Notice that we spelled it wrong.
            QueryResult results = wolfram.Query(query.Trim());

            //This fetches the pods that did not complete. It is only here to show how to use it.
            //This returns the pods, but also adds them to the original QueryResults.
            results.RecalculateResults();

            //Here we output the Wolfram|Alpha results.
            if (results.Error != null)
            {
                errorMessage = Resources.Resource.wolframError;
                return null;
            }

            if (results.DidYouMean.HasElements())
            {
                StringBuilder strb = new StringBuilder();

                foreach (DidYouMean didYouMean in results.DidYouMean)
                {
                    strb.AppendLine(string.Format(Resources.Resource.didyoumean, didYouMean.Value));
                    
                }
            }

       
            //Results are split into "pods" that contain information. Those pods can also have subpods.
            Pod primaryPod = results.GetPrimaryPod();
            IList<WolframResult> queryResult = new List<WolframResult>();
            if (primaryPod != null)
            {
                //Console.WriteLine(primaryPod.Title);
                if (primaryPod.SubPods.HasElements())
                {
                   
                    //Can compose the output cards here
                    foreach (SubPod subPod in primaryPod.SubPods)
                    {
                        //Console.WriteLine(subPod.Title);
                        //Console.WriteLine(subPod.Plaintext);
                      
                        queryResult.Add(new WolframResult() { PlainText = subPod.Plaintext.Replace("Wolfram|Alpha", "Meelo") });
                        break;
                        //if (subPod.Image != null && !string.IsNullOrEmpty(subPod.Image.Src))
                        //{
                        //    System.Net.WebClient client = new System.Net.WebClient();
                        //    if (subPod.Image.Src.IndexOf("image/gif") > 0)
                        //    {
                        //        client.DownloadFile(new Uri(subPod.Image.Src), System.Guid.NewGuid().ToString("N") + ".gif");
                        //    }
                        //}
                    }
                }
            }

            if(queryResult.Count == 0)
            {
                queryResult.Add(new WolframResult() { PlainText = Resources.Resource.bespecific });
            }
            return queryResult;
            //if (results.Warnings != null)
            //{
            //    if (results.Warnings.Translation != null)
            //        Console.WriteLine("Translation: " + results.Warnings.Translation.Text);

            //    if (results.Warnings.SpellCheck != null)
            //        Console.WriteLine("Spellcheck: " + results.Warnings.SpellCheck.Text);
            //}

        }


        private IList<Attachment> CallWolframApiAndReturnCards(string query, out string errorMessage)
        {
            errorMessage = string.Empty;
            WolframAlpha wolfram = new WolframAlpha(AppId);
            wolfram.ScanTimeout = 0.1f; //We set ScanTimeout really low to get a quick answer. See RecalculateResults() below.
            wolfram.UseTLS = true; //Use encryption

            //We search for something. Notice that we spelled it wrong.
            QueryResult results = wolfram.Query(query.Trim());
            IList<Attachment> queryResult = new List<Attachment>();
            //This fetches the pods that did not complete. It is only here to show how to use it.
            //This returns the pods, but also adds them to the original QueryResults.
            // results.RecalculateResults();
            bool callWiki = false;
            //Here we output the Wolfram|Alpha results.
            if (results.Error != null)
            {
                //Get it from Wikipedia
                callWiki = true;
            }
            else
            {

                //if (results.DidYouMean.HasElements())
                //{
                //    StringBuilder strb = new StringBuilder();

                //    foreach (DidYouMean didYouMean in results.DidYouMean)
                //    {
                //        strb.AppendLine(string.Format(Resources.Resource.didyoumean, didYouMean.Value));

                //    }
                //}


                //Results are split into "pods" that contain information. Those pods can also have subpods.
                Pod primaryPod = results.GetPrimaryPod();

                if (primaryPod != null)
                {
                    //Console.WriteLine(primaryPod.Title);
                    if (primaryPod.SubPods.HasElements())
                    {

                        //Can compose the output cards here
                        foreach (SubPod subPod in primaryPod.SubPods)
                        {


                            queryResult.Add(SubPodToCard(primaryPod, subPod, CardType.HeroCard));


                        }
                    }
                }
                else
                {
                    results.RecalculateResults();
                    //get each pod and then print its subpods

                    //var nonPrimaryPods = from npods in results.Pods
                    //                     where npods.Primary == false
                    //                     select npods;

                    foreach (Pod p in results.Pods)
                    {


                        foreach (SubPod subPod in p.SubPods)
                        {
                            queryResult.Add(SubPodToCard(p, subPod, CardType.HeroCard));
                        }

                    }
                }//else

                if(queryResult.Count == 0)
                {
                    callWiki = true;
                }
            }

            if(callWiki)
            {
                queryResult = WikipediaSearch.WikipediaToAttachments(query);

                if (queryResult == null || queryResult.Count == 0 || (queryResult.Count == 1 && string.IsNullOrEmpty(queryResult[0].Name)))
                {
                    errorMessage = Resources.Resource.wolframError;
                    return null;
                }
            }
            return queryResult;
            //if (results.Warnings != null)
            //{
            //    if (results.Warnings.Translation != null)
            //        Console.WriteLine("Translation: " + results.Warnings.Translation.Text);

            //    if (results.Warnings.SpellCheck != null)
            //        Console.WriteLine("Spellcheck: " + results.Warnings.SpellCheck.Text);
            //}

        }

        private static Attachment SubPodToCard(Pod pod, SubPod subPod, CardType cT = CardType.ThumbnailCard)
        {
            try
            {
                CardImage ci = null;
                string plainText = subPod.Plaintext.Replace(Resources.Resource.wolframName, Resources.Resource.myname);

                //if (string.IsNullOrEmpty(subPod.Plaintext))
                //{
                    if (subPod.Image != null && !string.IsNullOrEmpty(subPod.Image.Src))
                    {

                        ci = new CardImage()
                        {
                            Url = subPod.Image.Src,
                             Alt = "image"
                        };
                    }

                //  }

                // CardAction act = new CardAction(ActionTypes.OpenUrl, "More...", value:subPod.Image.Src);
                CardAction act = null;

                string podTitle = !(pod.Primary) ? pod.Title : string.Empty;

                switch (cT)
                {
                    case CardType.HeroCard:

                        return DialogUtils.GetHeroCard(podTitle, subPod.Title, plainText, ci, act);
                    case CardType.ThumbnailCard:

                        return DialogUtils.GetThumbnailCard(podTitle, subPod.Title, plainText, ci, act);

                    default:

                        return DialogUtils.GetThumbnailCard(podTitle, subPod.Title, plainText, ci, act);

                }
            

            }catch(Exception)
            {
              
            }

            return null;
           
        }


      

    }

    #endregion
   


    [Serializable]
    public class WolframResult
    {
        public string PlainText { get; set; }


    }
}