﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tectil.NCommand.Contract;
using Tectil.NCommand;
using System.Net;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using Com.Meelo.Conferencing.WebEx;
using Newtonsoft.Json;
using System.Drawing;
using Com.Meelo.Conferencing.WebEx.Generic;


namespace conf
{
    class Program
    {
        static readonly XmlSerializer serializer = new XmlSerializer(typeof(message));

        [Command(description: "Get Webex API version", Name = "WebexAPIVersion")]
        public string WebexAPIVersion(
          [Argument(description: "Name of the webex site (e.g. go, cisco, etc.)")] string sitename


      )
        {

            try
            {
                
                WebExFunctions wf = new WebExFunctions()
                {
                     SecurityContext = new WebExSecurityContext() { SiteName = sitename }
                };
                //string responseFromServer = wf.GetAPIVersion();
               WebExResponseMessage msg = wf.GetAPIVersion();
                // File.WriteAllText(string.Format("webex.GetAPIVersion-{0}.xml", System.Guid.NewGuid().ToString("N")), responseFromServer);

                return msg.ResponseMessage.body.bodyContent.apiVersion;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }


        }

        [Command(description: "LstsummaryMeeting", Name = "LstsummaryMeeting")]
        public string LstsummaryMeeting(
          [Argument(description: "Webex Site Name")] string sitename,
           [Argument(description: "Webex SiteID")] string siteid,
            [Argument(description: "Webex Partner Id")] string partnerid,
            [Argument(description: "Webex User Id")] string webexid,
            [Argument(description: "Webex User Password")] string password,
             [Argument(description: "Host WebEx ID")] string hostwebexid,
              [Argument(description: "Meeting Key")] string meetingkey,
              [Argument(description: "Write to file", DefaultValue = true)] bool writetofile
      )
        {



            try
            {


                WebExFunctions wf = new WebExFunctions()
                {
                    SecurityContext = new WebExSecurityContext()
                    {
                        SiteName = sitename,
                        SiteID = siteid,
                        WebExID = webexid,
                        Password = password,
                        PartnerID = partnerid
                    }
                };

                FilterInput filter = new FilterInput();

                DateScope ds = new DateScope()
                {
                    StartDateStart = DateTimeOffset.Now.AddDays(-30),
                    EndDateStart = DateTimeOffset.Now.AddDays(30)
                };

                filter.DateScope = ds;
                filter.MaximumNum = 50;
                
                WebExResponseMessage msg = wf.LstsummaryMeeting(filter, hostWebExID:hostwebexid, meetingKey:meetingkey, includeRawResponse: writetofile);

                if (writetofile)
                {
                    File.WriteAllText(string.Format("webex.LstsummaryMeeting-{0}.xml", System.Guid.NewGuid().ToString("N")), msg.ResponseMessageRaw);
                }

                if (msg.ResponseMessage.header.response.result == "SUCCESS")
                {
                    //string s = JsonConvert.SerializeObject(msg);
                    //  Console.WriteLine(s);
                    StringBuilder strb = new StringBuilder();
                    serializer.Serialize(new StringWriter(strb), msg.ResponseMessage);
                    return strb.ToString();
                }
                else
                {
                    return msg.ResponseMessage.header.response.reason;
                }


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        [Command(description: "Authenticate WebEx User", Name = "authenticateWebexUser")]
        public string AuthenticateWebexUser(
            [Argument(description: "Webex Site Name")] string sitename,
           [Argument(description: "Webex SiteID")] string siteid,
            [Argument(description: "Webex Partner Id")] string partnerid,
            [Argument(description: "Webex User Id")] string webexid,
            [Argument(description: "Webex User Password")] string password
     )
        {

           
            try
            {


                WebExFunctions wf = new WebExFunctions()
                {
                    SecurityContext = new WebExSecurityContext()
                    {
                        SiteName = sitename,
                        SiteID = siteid,
                        WebExID = webexid,
                        Password = password,
                        PartnerID = partnerid
                    }
                };

               WebExResponseMessage msg = wf.Authenticate();
                // File.WriteAllText(string.Format("webex.GetAPIVersion-{0}.xml", System.Guid.NewGuid().ToString("N")), responseFromServer);
                if (msg.ResponseMessage.header.response.result == "SUCCESS")
                {
                    //string s = JsonConvert.SerializeObject(msg);
                    //  Console.WriteLine(s);
                    StringBuilder strb = new StringBuilder();
                    serializer.Serialize(new StringWriter(strb), msg.ResponseMessage);
                    return strb.ToString();
                }
                else
                {
                    return msg.ResponseMessage.header.response.reason;
                }


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }

        }

        [Command(description: "Authenticate WebEx User with CI", Name = "authenticateWebexUserCi")]
        public string AuthenticateWebexUserCi(
       [Argument(description: "Webex Site Name")] string sitename,
           [Argument(description: "Webex SiteID")] string siteid,
            [Argument(description: "Webex Partner Id")] string partnerid,
            [Argument(description: "Webex User Id")] string webexid,
            [Argument(description: "Webex User Password")] string password
    )
        {



            try
            {


                WebExFunctions wf = new WebExFunctions()
                {
                    SecurityContext = new WebExSecurityContext()
                    {
                        SiteName = sitename,
                        SiteID = siteid,
                        WebExID = webexid,
                        Password = password,
                        PartnerID = partnerid
                    }
                };

                WebExResponseMessage msg = wf.Authenticate(useSSO: false);
                // File.WriteAllText(string.Format("webex.GetAPIVersion-{0}.xml", System.Guid.NewGuid().ToString("N")), responseFromServer);
                if (msg.ResponseMessage.header.response.result == "SUCCESS")
                {
                    //string s = JsonConvert.SerializeObject(msg);
                    //  Console.WriteLine(s);
                    StringBuilder strb = new StringBuilder();
                    serializer.Serialize(new StringWriter(strb), msg.ResponseMessage);
                    return strb.ToString();
                }
                else
                {
                    return msg.ResponseMessage.header.response.reason;
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        [Command(description: "Get user login url", Name = "GetloginurlUser")]
        public string GetloginurlUser(
     [Argument(description: "Webex Site Name")] string sitename,
         [Argument(description: "Webex SiteID")] string siteid,
          [Argument(description: "Webex Partner Id")] string partnerid,
          [Argument(description: "Webex User Id")] string webexid,
          [Argument(description: "Webex User Password")] string password
  )
        {



            try
            {


                WebExFunctions wf = new WebExFunctions()
                {
                    SecurityContext = new WebExSecurityContext()
                    {
                        SiteName = sitename,
                        SiteID = siteid,
                        WebExID = webexid,
                        Password = password,
                        PartnerID = partnerid
                    }
                };

               WebExResponseMessage msg = wf.GetloginurlUser(webexid);
                // File.WriteAllText(string.Format("webex.GetAPIVersion-{0}.xml", System.Guid.NewGuid().ToString("N")), responseFromServer);
                if (msg.ResponseMessage.header.response.result == "SUCCESS")
                {
                    //string s = JsonConvert.SerializeObject(msg);
                    //  Console.WriteLine(s);
                    StringBuilder strb = new StringBuilder();
                    serializer.Serialize(new StringWriter(strb), msg.ResponseMessage);
                    return strb.ToString();
                }
                else
                {
                    return msg.ResponseMessage.header.response.reason;
                }


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        [Command(description: "Get user logout url", Name = "GetlogouturlUser")]
        public string GetlogouturlUser(
   [Argument(description: "Webex Site Name")] string sitename,
       [Argument(description: "Webex SiteID")] string siteid,
        [Argument(description: "Webex Partner Id")] string partnerid,
        [Argument(description: "Webex User Id")] string webexid,
        [Argument(description: "Webex User Password")] string password
)
        {



            try
            {


                WebExFunctions wf = new WebExFunctions()
                {
                    SecurityContext = new WebExSecurityContext()
                    {
                        SiteName = sitename,
                        SiteID = siteid,
                        WebExID = webexid,
                        Password = password,
                        PartnerID = partnerid
                    }
                };

                WebExResponseMessage msg = wf.GetlogouturlUser(webexid);
                // File.WriteAllText(string.Format("webex.GetAPIVersion-{0}.xml", System.Guid.NewGuid().ToString("N")), responseFromServer);
                if (msg.ResponseMessage.header.response.result == "SUCCESS")
                {
                    //string s = JsonConvert.SerializeObject(msg);
                    //  Console.WriteLine(s);
                    StringBuilder strb = new StringBuilder();
                    serializer.Serialize(new StringWriter(strb), msg.ResponseMessage);
                    return strb.ToString();
                }
                else
                {
                    return msg.ResponseMessage.header.response.reason;
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        [Command(description: "Get user information", Name = "GetUser")]
        public string GetUser(
  [Argument(description: "Webex Site Name")] string sitename,
      [Argument(description: "Webex SiteID")] string siteid,
       [Argument(description: "Webex Partner Id")] string partnerid,
       [Argument(description: "Webex User Id")] string webexid,
       [Argument(description: "Webex User Password")] string password,
         [Argument(description: "Write to file", DefaultValue = true)] bool writetofile
)
        {



            try
            {


                WebExFunctions wf = new WebExFunctions()
                {
                    SecurityContext = new WebExSecurityContext()
                    {
                        SiteName = sitename,
                        SiteID = siteid,
                        WebExID = webexid,
                        Password = password,
                        PartnerID = partnerid
                    }
                };

                WebExResponseMessage msg = wf.GetUser(webexid, includeRawResponse:writetofile);
                if(writetofile)
                 File.WriteAllText(string.Format("webex.GetUser-{0}.xml", System.Guid.NewGuid().ToString("N")), msg.ResponseMessageRaw);
                if (msg.ResponseMessage.header.response.result == "SUCCESS")
                {
                    //string s = JsonConvert.SerializeObject(msg);
                    //  Console.WriteLine(s);
                    StringBuilder strb = new StringBuilder();
                    serializer.Serialize(new StringWriter(strb), msg.ResponseMessage);
                    return strb.ToString();
                }
                else
                {
                    return msg.ResponseMessage.header.response.reason;
                }


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        [Command(description: "Upload user photo for personal meetings", Name = "UploadPMRImage")]
        public string UploadPMRImage(
[Argument(description: "Webex Site Name")] string sitename,
    [Argument(description: "Webex SiteID")] string siteid,
     [Argument(description: "Webex Partner Id")] string partnerid,
     [Argument(description: "Webex User Id")] string webexid,
     [Argument(description: "Webex User Password")] string password,
      [Argument(description: "Path to photo on local machine")] string imagepath
)
        {



            try
            {
                if (!string.IsNullOrEmpty(imagepath))
                {

                    WebExFunctions wf = new WebExFunctions()
                    {
                        SecurityContext = new WebExSecurityContext()
                        {
                            SiteName = sitename,
                            SiteID = siteid,
                            WebExID = webexid,
                            Password = password,
                            PartnerID = partnerid
                        }
                    };

                   WebExResponseMessage msg = wf.UploadPMRImage(SerializeImage(imagepath));
                    // File.WriteAllText(string.Format("webex.GetAPIVersion-{0}.xml", System.Guid.NewGuid().ToString("N")), responseFromServer);
                    if (msg.ResponseMessage.header.response.result == "SUCCESS")
                    {
                        //string s = JsonConvert.SerializeObject(msg);
                        //  Console.WriteLine(s);
                        StringBuilder strb = new StringBuilder();
                        serializer.Serialize(new StringWriter(strb), msg.ResponseMessage);
                        return strb.ToString();
                    }
                    else
                    {
                        return msg.ResponseMessage.header.response.reason;
                    }

                }
                else
                {
                    return "Please specify path to the image";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        [Command(description: "Get user one click settings", Name = "GetOneClickSettings")]
        public string GetOneClickSettings(
 [Argument(description: "Webex Site Name")] string sitename,
     [Argument(description: "Webex SiteID")] string siteid,
      [Argument(description: "Webex Partner Id")] string partnerid,
      [Argument(description: "Webex User Id")] string webexid,
      [Argument(description: "Webex User Password")] string password,
       [Argument(description: "Host Webex User Id")] string hostwebexid,
              [Argument(description: "Write to file", DefaultValue = true)] bool writetofile

)
        {



            try
            {


                WebExFunctions wf = new WebExFunctions()
                {
                    SecurityContext = new WebExSecurityContext()
                    {
                        SiteName = sitename,
                        SiteID = siteid,
                        WebExID = webexid,
                        Password = password,
                        PartnerID = partnerid
                    }
                };

                if (string.IsNullOrEmpty(hostwebexid)) hostwebexid = webexid;

                WebExResponseMessage msg = wf.GetOneClickSettings(hostwebexid, includeRawResponse:writetofile);
                if(writetofile)
                  File.WriteAllText(string.Format("webex.GetOneClickSettings-{0}.xml", System.Guid.NewGuid().ToString("N")), msg.ResponseMessageRaw);
                if (msg.ResponseMessage.header.response.result == "SUCCESS")
                {
                    //string s = JsonConvert.SerializeObject(msg);
                    //  Console.WriteLine(s);
                    StringBuilder strb = new StringBuilder();
                    serializer.Serialize(new StringWriter(strb), msg.ResponseMessage);
                    return strb.ToString();
                }
                else
                {
                    return msg.ResponseMessage.header.response.reason;
                }


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        [Command(description: "List recordings", Name = "lstRecording")]
        public string LstRecording(
        [Argument(description: "Webex Site Name")] string sitename,
         [Argument(description: "Webex SiteID")] string siteid,
          [Argument(description: "Webex Partner Id")] string partnerid,
          [Argument(description: "Webex User Id")] string webexid,
          [Argument(description: "Webex User Password")] string password,
           [Argument(description: "Write to file", DefaultValue = true)] bool writetofile
    )
        {



            try
            {


                WebExFunctions wf = new WebExFunctions()
                {
                    SecurityContext = new WebExSecurityContext()
                    {
                        SiteName = sitename,
                        SiteID = siteid,
                        WebExID = webexid,
                        Password = password,
                        PartnerID = partnerid
                    }
                };

                FilterInput filter = new FilterInput();

                DateScope ds = new DateScope()
                {
                    StartDateStart = DateTimeOffset.Now.AddDays(-30),
                    StartDateEnd = DateTimeOffset.Now.AddDays(30)
                };
                filter.MaximumNum = 50;
                filter.DateScope = ds;

               WebExResponseMessage msg = wf.LstRecording(filter, includeRawResponse:writetofile);

                if(writetofile)
                File.WriteAllText(string.Format("webex.LstRecording-{0}.xml", System.Guid.NewGuid().ToString("N")), msg.ResponseMessageRaw);

                if (msg.ResponseMessage.header.response.result == "SUCCESS")
                {
                    //string s = JsonConvert.SerializeObject(msg);
                    //  Console.WriteLine(s);
                    StringBuilder strb = new StringBuilder();
                    serializer.Serialize(new StringWriter(strb), msg.ResponseMessage);
                    return strb.ToString();
                }
                else
                {
                    return msg.ResponseMessage.header.response.reason;
                }


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        [Command(description: "Get session info", Name = "GetSessionInfo")]
        public string GetSessionInfo(
 [Argument(description: "Webex Site Name")] string sitename,
     [Argument(description: "Webex SiteID")] string siteid,
      [Argument(description: "Webex Partner Id")] string partnerid,
      [Argument(description: "Webex User Id")] string webexid,
      [Argument(description: "Webex User Password")] string password,
       [Argument(description: "Session Key")] string sessionkey,
         [Argument(description: "Session Password")] string sessionpassword,
     [Argument(description: "Write to file", DefaultValue = true)] bool writetofile
)
        {



            try
            {


                WebExFunctions wf = new WebExFunctions()
                {
                    SecurityContext = new WebExSecurityContext()
                    {
                        SiteName = sitename,
                        SiteID = siteid,
                        WebExID = webexid,
                        Password = password,
                        PartnerID = partnerid
                    }
                };


                WebExResponseMessage msg = wf.GetSessionInfo(sessionpassword, sessionkey, includeRawResponse:writetofile);

                if(writetofile)
                 File.WriteAllText(string.Format("webex.GetSessionInfo-{0}.xml", System.Guid.NewGuid().ToString("N")), msg.ResponseMessageRaw);
                if (msg.ResponseMessage.header.response.result == "SUCCESS")
                {
                    //string s = JsonConvert.SerializeObject(msg);
                    //  Console.WriteLine(s);
                    StringBuilder strb = new StringBuilder();
                    serializer.Serialize(new StringWriter(strb), msg.ResponseMessage);
                    return strb.ToString();
                }
                else
                {
                    return msg.ResponseMessage.header.response.reason;
                }


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }


        #region Meeting Service

        [Command(description: "Create a meeting", Name = "CreateMeeting")]
        public string CreateMeeting(
[Argument(description: "Webex Site Name")] string sitename,
    [Argument(description: "Webex SiteID")] string siteid,
     [Argument(description: "Webex Partner Id")] string partnerid,
     [Argument(description: "Webex User Id")] string webexid,
     [Argument(description: "Webex User Password")] string password,
       [Argument(description: "Conference name")] string confname,
         //  [Argument(description: "Generate random meeting password", DefaultValue = true)] bool generatemeetingpassword,
         [Argument(description: "Enable chat, av, poll, voip", DefaultValue = true)] bool enableoptions,
          [Argument(description: "Add attendees", DefaultValue = true)] bool addattendees,
           [Argument(description: "Use telephony", DefaultValue = false)] bool usetelephony,
            [Argument(description: "Repeat daily")] bool repeatdaily

)
        {


            try
            {


                WebExFunctions wf = new WebExFunctions()
                {
                    SecurityContext = new WebExSecurityContext()
                    {
                        SiteName = sitename,
                        SiteID = siteid,
                        WebExID = webexid,
                        Password = password,
                        PartnerID = partnerid
                    }
                };

                CreateMeetingInput cmi = new CreateMeetingInput();

                //if (generatemeetingpassword)
                //{
                accessControl ac = new accessControl()
                {
                    meetingPassword = "abcdef"
                };

                cmi.AccessControl = ac;
                // }

                metaData md = new metaData()
                {
                    confName = confname,
                    agenda = "Meeting agenda here..."
                };
                cmi.Metadata = md;

                if (enableoptions)
                {
                    enableOptions eo = new enableOptions()
                    {
                        audioVideo = true,
                        chat = true,
                        poll = true,
                        voip = true
                    };

                    cmi.EnableOptions = eo;
                }

                if (addattendees)
                {
                    IList<Person> attendees = new List<Person>();
                   
                    attendees.Add(new Person() { Name = "Tejaswi Outlook", Email = "tejaswi@outlook.com" });

                    attendees.Add(new Person() { Name = "Tej Gmail", Email = "tredkar@gmail.com" });


                    cmi.Attendees = attendees;
                }

                schedule sc = new schedule()
                {
                    startDate = DateTimeOffset.Now.AddHours(1).ToWebExReadable(),
                    duration = 90,
                    joinTeleconfBeforeHost = false
                };

                cmi.Schedule = sc;

                if (usetelephony)
                {
                    telephony tp = new telephony()
                    {
                        telephonySupport = "CALLIN",
                        extTelephonyDescription = "Call 1-800-555-1234, Passcode 98765"
                    };

                    cmi.Telephony = tp;
                }

                attendeeOptions ao = new attendeeOptions()
                {
                    emailInvitations = true
                };

                cmi.AttendeeOptions = ao;
           
                if(repeatdaily)
                {
                    Repeat rp = new Repeat()
                    {
                        RepeatType = RepeatType.DAILY,
                         Interval = 1,
                         AfterMeetingNumber = 10

                    };

                    cmi.Repeat = rp;
                }

                WebExResponseMessage msg = wf.CreateMeeting(cmi);
                // File.WriteAllText(string.Format("webex.GetAPIVersion-{0}.xml", System.Guid.NewGuid().ToString("N")), responseFromServer);
                if (msg.ResponseMessage.header.response.result == "SUCCESS")
                {
                    //string s = JsonConvert.SerializeObject(msg);
                    //  Console.WriteLine(s);
                    StringBuilder strb = new StringBuilder();
                    serializer.Serialize(new StringWriter(strb), msg.ResponseMessage);
                    return strb.ToString();
                }
                else
                {
                    return msg.ResponseMessage.header.response.reason;
                }


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        [Command(description: "Delete meeting", Name = "deletemeeting")]
        public string DeleteMeeting(
[Argument(description: "Webex Site Name")] string sitename,
   [Argument(description: "Webex SiteID")] string siteid,
    [Argument(description: "Webex Partner Id")] string partnerid,
    [Argument(description: "Webex User Id")] string webexid,
    [Argument(description: "Webex User Password")] string password,
     [Argument(description: "Meeting key")] string meetingkey
)
        {



            try
            {


                WebExFunctions wf = new WebExFunctions()
                {
                    SecurityContext = new WebExSecurityContext()
                    {
                        SiteName = sitename,
                        SiteID = siteid,
                        WebExID = webexid,
                        Password = password,
                        PartnerID = partnerid
                    }
                };

                if (!string.IsNullOrEmpty(meetingkey))
                {



                    WebExResponseMessage msg = wf.DeleteMeeting(meetingkey);

                    if (msg.ResponseMessage.header.response.result == "SUCCESS")
                    {
                        //string s = JsonConvert.SerializeObject(msg);
                        //  Console.WriteLine(s);
                        StringBuilder strb = new StringBuilder();
                        serializer.Serialize(new StringWriter(strb), msg.ResponseMessage);
                        return strb.ToString();
                    }
                    else
                    {
                        return msg.ResponseMessage.header.response.reason;
                    }

                }
                else
                {
                    return "Please provide a meeting key";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        [Command(description: "Get host url for meeting", Name = "Gethosturlmeeting")]
        public string GethosturlMeeting(
[Argument(description: "Webex Site Name")] string sitename,
 [Argument(description: "Webex SiteID")] string siteid,
  [Argument(description: "Webex Partner Id")] string partnerid,
  [Argument(description: "Webex User Id")] string webexid,
  [Argument(description: "Webex User Password")] string password,
   [Argument(description: "Session key")] string sessionkey
)
        {



            try
            {


                WebExFunctions wf = new WebExFunctions()
                {
                    SecurityContext = new WebExSecurityContext()
                    {
                        SiteName = sitename,
                        SiteID = siteid,
                        WebExID = webexid,
                        Password = password,
                        PartnerID = partnerid
                    }
                };

                if (!string.IsNullOrEmpty(sessionkey))
                {



                   WebExResponseMessage msg = wf.GethosturlMeeting(sessionkey);

                    if (msg.ResponseMessage.header.response.result == "SUCCESS")
                    {
                        //string s = JsonConvert.SerializeObject(msg);
                        //  Console.WriteLine(s);
                        StringBuilder strb = new StringBuilder();
                        serializer.Serialize(new StringWriter(strb), msg.ResponseMessage);
                        return strb.ToString();
                    }
                    else
                    {
                        return msg.ResponseMessage.header.response.reason;
                    }

                }
                else
                {
                    return "Please provide a meeting key";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        [Command(description: "Get attendee url for meeting", Name = "GetjoinurlMeeting")]
        public string GetjoinurlMeeting(
[Argument(description: "Webex Site Name")] string sitename,
[Argument(description: "Webex SiteID")] string siteid,
[Argument(description: "Webex Partner Id")] string partnerid,
[Argument(description: "Webex User Id")] string webexid,
[Argument(description: "Webex User Password")] string password,
 [Argument(description: "Session key")] string sessionkey,
  [Argument(description: "Attendee Name")] string attendeename
)
        {

            try
            {


                WebExFunctions wf = new WebExFunctions()
                {
                    SecurityContext = new WebExSecurityContext()
                    {
                        SiteName = sitename,
                        SiteID = siteid,
                        WebExID = webexid,
                        Password = password,
                        PartnerID = partnerid
                    }
                };

                if (!string.IsNullOrEmpty(sessionkey))
                {



                    WebExResponseMessage msg = wf.GetjoinurlMeeting(sessionkey, attendeename);

                    if (msg.ResponseMessage.header.response.result == "SUCCESS")
                    {
                        //string s = JsonConvert.SerializeObject(msg);
                        //  Console.WriteLine(s);
                        StringBuilder strb = new StringBuilder();
                        serializer.Serialize(new StringWriter(strb), msg.ResponseMessage);
                        return strb.ToString();
                    }
                    else
                    {
                        return msg.ResponseMessage.header.response.reason;
                    }

                }
                else
                {
                    return "Please provide a meeting key";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        [Command(description: "Get meeting", Name = "Getmeeting")]
        public string GetMeeting(
[Argument(description: "Webex Site Name")] string sitename,
  [Argument(description: "Webex SiteID")] string siteid,
   [Argument(description: "Webex Partner Id")] string partnerid,
   [Argument(description: "Webex User Id")] string webexid,
   [Argument(description: "Webex User Password")] string password,
    [Argument(description: "Meeting key")] string meetingkey,
     [Argument(description: "Write to file", DefaultValue = true)] bool writetofile
)
        {



            try
            {


                WebExFunctions wf = new WebExFunctions()
                {
                    SecurityContext = new WebExSecurityContext()
                    {
                        SiteName = sitename,
                        SiteID = siteid,
                        WebExID = webexid,
                        Password = password,
                        PartnerID = partnerid
                    }
                };

                if (!string.IsNullOrEmpty(meetingkey))
                {



                    WebExResponseMessage msg = wf.GetMeeting(meetingkey, includeRawResponse: writetofile);
                    if (writetofile)
                        File.WriteAllText(string.Format("webex.GetMeeting-{0}.xml", System.Guid.NewGuid().ToString("N")), msg.ResponseMessageRaw);

                    if (msg.GetMeetingResponseMessage.header.response.result == "SUCCESS")
                    {
                        //string s = JsonConvert.SerializeObject(msg);
                        //  Console.WriteLine(s);
                        StringBuilder strb = new StringBuilder();
                         XmlSerializer mserializer = new XmlSerializer(typeof(Com.Meelo.Conferencing.WebEx.Meeting.message));
                        mserializer.Serialize(new StringWriter(strb), msg.GetMeetingResponseMessage);
                        return strb.ToString();
                    }
                    else
                    {
                        return msg.GetMeetingResponseMessage.header.response.reason;
                    }

                }
                else
                {
                    return "Please provide a meeting key";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        [Command(description: "Update a meeting", Name = "Setmeeting")]
        public string SetMeeting(
[Argument(description: "Webex Site Name")] string sitename,
 [Argument(description: "Webex SiteID")] string siteid,
  [Argument(description: "Webex Partner Id")] string partnerid,
  [Argument(description: "Webex User Id")] string webexid,
  [Argument(description: "Webex User Password")] string password,
   [Argument(description: "Meeting key")] string meetingkey,
    [Argument(description: "Conference name")] string confname,
      [Argument(description: "Enable chat, av, poll, voip", DefaultValue = false)] bool enableoptions,
       [Argument(description: "Add attendees", DefaultValue = false)] bool addattendees,
        [Argument(description: "Use telephony", DefaultValue = false)] bool usetelephony
)
        {
            if (!string.IsNullOrEmpty(meetingkey))
            {

                try
                {



                    WebExFunctions wf = new WebExFunctions()
                    {
                        SecurityContext = new WebExSecurityContext()
                        {
                            SiteName = sitename,
                            SiteID = siteid,
                            WebExID = webexid,
                            Password = password,
                            PartnerID = partnerid
                        }
                    };

                    CreateMeetingInput cmi = new CreateMeetingInput();

                    //if (generatemeetingpassword)
                    //{
                    accessControl ac = new accessControl()
                    {
                        meetingPassword = "abcdef"
                    };

                    cmi.AccessControl = ac;
                    // }

                    metaData md = new metaData()
                    {
                        confName = confname,
                        agenda = "Meeting agenda here..."
                    };
                    cmi.Metadata = md;

                    if (enableoptions)
                    {
                        enableOptions eo = new enableOptions()
                        {
                            audioVideo = true,
                            chat = true,
                            poll = true,
                            voip = true
                        };

                        cmi.EnableOptions = eo;
                    }

                    if (addattendees)
                    {
                        IList<Person> attendees = new List<Person>();
                        for (int i = 0; i < 10; i++)
                        {
                            string name = string.Format("attendee{0}", i);
                            attendees.Add(new Person() { Name = name, Email = string.Format("{0}@meelo.io", name) });
                        }

                        cmi.Attendees = attendees;
                    }

                    schedule sc = new schedule()
                    {
                        startDate = DateTimeOffset.Now.ToWebExReadable(),
                        duration = 90,
                        joinTeleconfBeforeHost = false
                    };

                    cmi.Schedule = sc;

                    if (usetelephony)
                    {
                        telephony tp = new telephony()
                        {
                            telephonySupport = "CALLIN",
                            extTelephonyDescription = "Call 1-800-555-1234, Passcode 98765"
                        };

                        cmi.Telephony = tp;
                    }

                    cmi.AttendeeOptions = new attendeeOptions()
                    {
                        auto = true
                    };

                  
                    
                    WebExResponseMessage msg = wf.SetMeeting(meetingkey, cmi);
                    // File.WriteAllText(string.Format("webex.GetAPIVersion-{0}.xml", System.Guid.NewGuid().ToString("N")), responseFromServer);
                    if (msg.ResponseMessage.header.response.result == "SUCCESS")
                    {
                        //string s = JsonConvert.SerializeObject(msg);
                        //  Console.WriteLine(s);
                        StringBuilder strb = new StringBuilder();
                        serializer.Serialize(new StringWriter(strb), msg.ResponseMessage);
                        return strb.ToString();
                    }
                    else
                    {
                        return msg.ResponseMessage.header.response.reason;
                    }



                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    throw ex;
                }

            }
            else
            {
                return "Please provide a meeting key";
            }
        }
        #endregion

        #region Site service


        [Command(description: "Get site", Name = "GetSite")]
        public string GetSite(
[Argument(description: "Webex Site Name")] string sitename,
 [Argument(description: "Webex SiteID")] string siteid,
  [Argument(description: "Webex Partner Id")] string partnerid,
  [Argument(description: "Webex User Id")] string webexid,
  [Argument(description: "Webex User Password")] string password,
     [Argument(description: "Write to file", DefaultValue = true)] bool writetofile

)
        {



            try
            {


                WebExFunctions wf = new WebExFunctions()
                {
                    SecurityContext = new WebExSecurityContext()
                    {
                        SiteName = sitename,
                        SiteID = siteid,
                        WebExID = webexid,
                        Password = password,
                        PartnerID = partnerid
                    }
                };

              


                    WebExResponseMessage msg = wf.GetSite(includeRawResponse:writetofile);
                if (writetofile)
                    File.WriteAllText(string.Format("webex.GetSite-{0}.xml", System.Guid.NewGuid().ToString("N")), msg.ResponseMessageRaw);

                if (msg.ResponseMessage.header.response.result == "SUCCESS")
                    {
                        //string s = JsonConvert.SerializeObject(msg);
                        //  Console.WriteLine(s);
                        StringBuilder strb = new StringBuilder();
                        serializer.Serialize(new StringWriter(strb), msg.ResponseMessage);
                        return strb.ToString();
                    }
                    else
                    {
                        return msg.ResponseMessage.header.response.reason;
                    }

             
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }
        #endregion

        #region Meeting Attendee Service
        [Command(description: "Create a meeting attendee", Name = "CreateMeetingAttendee")]
        public string CreateMeetingAttendee(
[Argument(description: "Webex Site Name")] string sitename,
  [Argument(description: "Webex SiteID")] string siteid,
   [Argument(description: "Webex Partner Id")] string partnerid,
   [Argument(description: "Webex User Id")] string webexid,
   [Argument(description: "Webex User Password")] string password,
   [Argument(description: "Session key")] string meetingkey,
   [Argument(description: "Attendee name")] string attendeename,
   [Argument(description: "Attendee email")] string attendeeemail
)
        {


            try
            {


                WebExFunctions wf = new WebExFunctions()
                {
                    SecurityContext = new WebExSecurityContext()
                    {
                        SiteName = sitename,
                        SiteID = siteid,
                        WebExID = webexid,
                        Password = password,
                        PartnerID = partnerid
                    }
                };

               

                return wf.CreateMeetingAttendee(meetingkey, new Person()
                {
                    Name = attendeename,
                    Email = attendeeemail
                });
                // File.WriteAllText(string.Format("webex.GetAPIVersion-{0}.xml", System.Guid.NewGuid().ToString("N")), responseFromServer);
                //if (msg.header.response.result == "SUCCESS")
                //{
                //    //string s = JsonConvert.SerializeObject(msg);
                //    //  Console.WriteLine(s);
                //    StringBuilder strb = new StringBuilder();
                //    serializer.Serialize(new StringWriter(strb), msg);
                //    return strb.ToString();
                //}
                //else
                //{
                //    return msg.header.response.reason;
                //}


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        [Command(description: "Delete attendee", Name = "deleteattendee")]
        public string DeleteAttendee(
[Argument(description: "Webex Site Name")] string sitename,
[Argument(description: "Webex SiteID")] string siteid,
 [Argument(description: "Webex Partner Id")] string partnerid,
 [Argument(description: "Webex User Id")] string webexid,
 [Argument(description: "Webex User Password")] string password,
  [Argument(description: "Attendee Id")] string attendeeid
)
        {



            try
            {


                WebExFunctions wf = new WebExFunctions()
                {
                    SecurityContext = new WebExSecurityContext()
                    {
                        SiteName = sitename,
                        SiteID = siteid,
                        WebExID = webexid,
                        Password = password,
                        PartnerID = partnerid
                    }
                };

                if (!string.IsNullOrEmpty(attendeeid))
                {



                    WebExResponseMessage msg = wf.DeleteAttendee(attendeeid);

                    if (msg.ResponseMessage.header.response.result == "SUCCESS")
                    {
                        //string s = JsonConvert.SerializeObject(msg);
                        //  Console.WriteLine(s);
                        StringBuilder strb = new StringBuilder();
                        serializer.Serialize(new StringWriter(strb), msg.ResponseMessage);
                        return strb.ToString();
                    }
                    else
                    {
                        return msg.ResponseMessage.header.response.reason;
                    }

                }
                else
                {
                    return "Please provide a meeting key";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }
        #endregion

        #region Utility Methods
        public static byte[] SerializeImage(string pathToImage)
        {
            MemoryStream m;


            byte[] imageBytes;
            using (Image image = Image.FromFile(pathToImage))
            {

                using (m = new MemoryStream())
                {

                    image.Save(m, image.RawFormat);
                    imageBytes = new byte[m.Length];
                    //Very Important    
                    imageBytes = m.ToArray();

                }//end using
            }//end using

            return imageBytes;
        }//SerializeImage
        #endregion


        static void Main(string[] args)
        {
            NCommands commands = new NCommands();
            commands.RunConsole(args);
        }
    }
}
