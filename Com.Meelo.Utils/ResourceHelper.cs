﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.Utils
{
   public static class ResourceHelper
    {
        public static string GetResources(string Key)
        {
           return Com.Meelo.Utils.Resources.Resource.ResourceManager.GetString(Key);
        }
    }
}
