﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Com.Meelo.Utils.Config
{
    public static class ConfigUtils
    {
        #region Configuration

        public static string GetDbConnectionString(string configName)
        {
            string dbs = null;
            try
            {
              
                    dbs = ConfigurationManager.ConnectionStrings[configName].ConnectionString;
              
            }
            catch (Exception)
            {

                return ConfigurationManager.ConnectionStrings[configName].ConnectionString;
            }
            return dbs;
        }

        /// <summary>
        /// Gets the value from the config value from ServiceConfiguration if it exists otherwise gets it from web.config or app.config
        /// </summary>
        /// <param name="configName"></param>
        /// <returns></returns>
        public static string GetConfigValueAsString(string configName)
        {
            return ConfigurationManager.AppSettings[configName];

        }

        public static string GetStringConfigurationValue(string configName)
        {

            try
            {

                return GetConfigValueAsString(configName);


            }
            catch (Exception ex)
            {

                throw ex;

            }


        }

        public static bool GetBooleanConfigurationValue(string configName)
        {

            try
            {
                bool ret;
                if (bool.TryParse(GetConfigValueAsString(configName), out ret))
                {
                    return ret;
                }
                else
                {

                    throw new Exception(String.Format("Could not parse value for configuration setting {0}", configName));


                }


            }
            catch (Exception ex)
            {

                throw ex;

            }


        }

        public static int GetIntConfigurationValue(string configName)
        {

            try
            {
                int ret;
                if (int.TryParse(GetConfigValueAsString(configName), out ret))
                {
                    return ret;
                }
                else
                {

                    throw new Exception(String.Format("Could not parse value for configuration setting {0}", configName));


                }


            }
            catch (Exception ex)
            {

                throw ex;

            }


        }

        public static double GetDoubleConfigurationValue(string configName)
        {

            try
            {
                double ret;
                if (double.TryParse(GetConfigValueAsString(configName), out ret))
                {
                    return ret;
                }
                else
                {

                    throw new Exception(String.Format("Could not parse value for configuration setting {0}", configName));


                }


            }
            catch (Exception ex)
            {

                throw ex;

            }


        }



        #endregion
    }
}
