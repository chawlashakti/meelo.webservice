﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.Meelo.Utils.Sql;
using System.Data.SqlClient;

namespace Com.Meelo.Utils.Config
{
    public class DbUtils
    {
        #region DB Init
        public static string GetDbConnectionStringFromConfig(string dbConnectionStringConfigName)
        {

            return ConfigUtils.GetDbConnectionString(dbConnectionStringConfigName);

        }

        public static SqlConnection GetDbConnection(string dbConnectionString)
        {
            return new SqlRetryConnectionManager().OpenConnection(dbConnectionString);

        }




        #endregion
    }
}
