﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.Utils
{
    public static class UriExtensions
    {
        #region Class Methods

        public static string GetUrlKeyString(this Uri uri, UriComponents uriSensitivity)
        {
            // Get complete url
            string completeUrl = uri.ToString().ToUpperInvariant();

            // Get sensitive part
            string sensitiveUrlPart = uri.GetComponents(uriSensitivity, UriFormat.Unescaped);

            if (sensitiveUrlPart.IsNullOrEmpty())
            {
                return completeUrl;
            }

            return completeUrl.Replace(sensitiveUrlPart.ToUpperInvariant(), sensitiveUrlPart);
        }

        /// <summary>
        /// Checks if url is the same as the base url
        /// </summary>
        /// <param name="uriBase">Base Uri</param>
        /// <param name="uri">url to check</param>
        /// <returns>Returns true if url is not same as base url, else false</returns>
        public static bool IsHostMatch(this Uri uriBase, Uri uri)
        {


            if (uri == null)
            {
                return false;
            }

            bool ret = !uriBase.Host.Equals(uri.Host, StringComparison.OrdinalIgnoreCase);

            if (ret)
            {
                //check for subdomains
                string[] urlBaseStr = uriBase.Host.Split('.');

                if (urlBaseStr != null && urlBaseStr.Length == 3)
                {
                    string[] hostBaseArr = uri.Host.Split('.');

                    if (hostBaseArr != null && hostBaseArr.Length == 3)
                    {
                        ret = !urlBaseStr[2].Equals(hostBaseArr[2], StringComparison.OrdinalIgnoreCase);
                    }
                }
            }

            return ret;
        }

        public static string GetFileNameFromUri(this Uri uri)
        {
            // Get complete url
            string completeUrl = uri.ToString().ToUpperInvariant();

            return completeUrl.Substring(completeUrl.LastIndexOf("/") + 1, (completeUrl.Length - completeUrl.LastIndexOf("/") - 1));
        }

        public static string GetFileNameFromUriWithParameters(this Uri uri)
        {

            return System.IO.Path.GetFileName(uri.LocalPath);
        }
        public static string Isvalid(string UriStr, string DefaultUri)
        {
            if (Uri.IsWellFormedUriString(UriStr, UriKind.Absolute))
            {
                return new Uri(UriStr, UriKind.Absolute).ToString();
            }
            return new Uri(DefaultUri, UriKind.Absolute).ToString();
        }
        #endregion
    }
}
