﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.Utils.Naming
{

    /// <summary>
    /// Generates container and blob names
    /// https://blogs.msdn.microsoft.com/jmstall/2014/06/12/azure-storage-naming-rules/
    /// </summary>
    public static class MeeloNamingStrategy
    {
       
   
        public static string GetTenantContainerName(string tenantId)
        {
            return string.Format("mt-{0}", tenantId).ToLower();
        }

        public static string GetUserContainer(string tenantId, string userId )
        {
            return string.Format("mt{0}-u-{1}", tenantId, userId);
        }

        public static string GetUserSettingsBlobName(string tenantId, string userId)
        {
            return string.Format("mt{0}-u-{1}-s.json");
        }


        public static string GetWorkspaceContainerName(string tenantId, string workspaceId)
        {
            return string.Format("mt{0}-w-{1}", tenantId, workspaceId);
        }

        public static string GetWorkspaceBlobName(string tenantId, string workspaceId)
        {
            return string.Format("mt{0}-w-{1}.json", tenantId, workspaceId);
        }

        public static string GetEventContainerName(string tenantId, string eventId)
        {
            return string.Format("mt{0}-evt-{1}", tenantId, eventId);
        }

        public static string GetEventBlobName(string tenantId, string eventId)
        {
            return string.Format("mt{0}-evt-{1}.json", tenantId, eventId);
        }
        public static string GetNewLoginSessionId()
        {
            return System.Guid.NewGuid().ToString("N");
        }

      

        public static string GetSystemLogFileContainerName()
        {
            return "meelosystemlogs";
        }


    }
}
