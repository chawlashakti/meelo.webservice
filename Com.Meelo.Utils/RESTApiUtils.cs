﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using System.Web.Http;


namespace Com.Meelo.Utils
{
    public static class RESTApiUtils
    {

        public const string APP_ACCESS_KEY = "6c9c0908d66e493a98734b8871d75ba0";
        public const string APP_ACCESS_HASH = "ndmBMoD0gg4rmNg3oKM63v1nMyYiwcuMMaDWdIUqLTE=";
        public const string APP_ACCESS_SALT = "4pY5gg==";
        public const string X_MEELO_APIVERSION = "X-MEELO-APIVERSION";
        public const string X_MEELO_ACCESSHASH = "X-MEELO-ACCESSHASH";

        public static readonly SaltedHash SaltHash = new SaltedHash();

        public static string ValidateVersion(HttpRequestMessage request, string apiVersion)
        {
            try
            {
                string version = request.Headers.GetValues(apiVersion).FirstOrDefault();

                if (string.IsNullOrEmpty(version))
                {
                    throw new HttpRequestException("BadVersionHeader");

                }

                return version;

            }
            catch (Exception)
            {

                throw new HttpRequestException("BadVersionHeader");

            }



        }



        public static bool ValidateHashPassword(HttpRequestMessage request, string accessHashInput)
        {
            bool ret = false;
            string accessHash = request.Headers.GetValues(accessHashInput).FirstOrDefault();

            if (string.IsNullOrEmpty(accessHash))
            {
                ret = false;


            }
            else
            {

                if (!SaltHash.VerifyHashString(APP_ACCESS_KEY, accessHash, APP_ACCESS_SALT))
                {
                    ret = false;

                    throw new HttpRequestException("BadHash");

                }
                else
                {
                    ret = true;
                }

            }


            return ret;


        }


        public static bool ValidateHashPassword(HttpRequestMessage request)
        {
            bool ret = false;
            string accessHash = request.Headers.GetValues(X_MEELO_ACCESSHASH).FirstOrDefault();

            if (string.IsNullOrEmpty(accessHash))
            {
                ret = false;


            }
            else
            {

                if (!SaltHash.VerifyHashString(APP_ACCESS_KEY, accessHash, APP_ACCESS_SALT))
                {
                    ret = false;

                   // throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Forbidden) { ReasonPhrase = "BadHash" });

                }
                else
                {
                    ret = true;
                }

            }


            return ret;


        }




    }
}
