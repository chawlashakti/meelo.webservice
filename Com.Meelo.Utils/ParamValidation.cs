﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.Utils
{
    public static class ParamValidation
    {

        public static void ValidateParamsForNull(params object[] values)
        {

            foreach (object o in values)
            {
                if (o == null)
                    throw new ArgumentNullException("Argument is null:");

            }

        }

        public static void ValidateParamsArrayForNull(params Array[] values)
        {

            foreach (Array o in values)
            {

                if (o == null || o.Length == 0)
                    throw new ArgumentNullException("Argument is null:");

            }

        }
        public static void ValidateParamsForNull(params Param[] values)
        {

            foreach (Param o in values)
            {
                if (o.Value == null || o.Value == System.DBNull.Value)
                    throw new ArgumentNullException("Argument is null:" + o.Name);

            }

        }

        public static void ValidateStringParamsForNullOrEmpty(params Param[] values)
        {

            foreach (Param o in values)
            {
                if (string.IsNullOrEmpty(o.Value as string))
                    throw new ArgumentNullException("Argument is null or empty:" + o.Name);

            }

        }



        public static void ValidateIntParamsForZero(params Param[] values)
        {

            foreach (Param o in values)
            {
                if (o.Value == null || o.Value == System.DBNull.Value || Convert.ToInt32(o.Value) <= 0)
                    throw new ArgumentNullException("Argument is null or less than equal to 0:" + o.Name);

            }

        }

        public static void ValidateLongParamsForZero(params Param[] values)
        {

            foreach (Param o in values)
            {
                if (o.Value == null || o.Value == System.DBNull.Value || Convert.ToInt64(o.Value) <= 0)
                    throw new ArgumentNullException("Argument is null or less than equal to 0:" + o.Name);

            }

        }

        public static void ValidateEmailParams(params Param[] values)
        {

            foreach (Param o in values)
            {
                string s = o.Value as string;
                if (string.IsNullOrEmpty(s) || s.Length > 100)
                    throw new ArgumentNullException("Argument is null or exceeds limit of 100:" + o.Name);

            }

        }



        public static void ValidateBlobNameParams(params Param[] values)
        {


            foreach (Param o in values)
            {
                string s = o.Value as string;
                if (string.IsNullOrEmpty(s) || s.Length > 255)
                    throw new ArgumentNullException("Argument is null or exceeds limit of 255:" + o.Name);

            }

        }

        public static void ValidateDateTimeMaxMinValues(params Param[] values)
        {

            foreach (Param o in values)
            {

                if (o.DateTimeValue == DateTime.MaxValue || o.DateTimeValue == DateTime.MinValue)
                    throw new ArgumentNullException("Argument is null or exceeds limit:" + o.Name);

            }
        }


        public static void ValidateDateTimeRangeValues(DateTime startTime, DateTime endTime)
        {


            if (startTime == DateTime.MaxValue || startTime == DateTime.MinValue || endTime == DateTime.MaxValue || endTime == DateTime.MinValue || startTime > endTime)
                throw new ArgumentNullException("Start Time cannot be greater than End Time");


        }

        #region Defaul Database Objects
        public static string NULLString(object s)
        {
            if (s == null || s == System.DBNull.Value)
                return string.Empty;
            else
                return Convert.ToString(s);
        }

        public static long NULLLong(object s)
        {
            if (s == null || s == System.DBNull.Value)
                return 0;
            else
                return Convert.ToInt64(s);
        }


        public static int NULLInt(object s)
        {
            if (s == null || s == System.DBNull.Value)
                return 0;
            else
                return Convert.ToInt32(s);
        }

        public static short NULLShort(object s)
        {
            if (s == null || s == System.DBNull.Value)
                return 0;
            else
                return Convert.ToInt16(s);
        }
        public static DateTime? NULLDateTime(object s)
        {
            if (s == null || s == System.DBNull.Value)
                return null;
            else
                return Convert.ToDateTime(s);
        }

        public static object NULLObj(object s)
        {
            if (s == null || s == System.DBNull.Value)
                return null;
            else
                return s;
        }

        public static bool NULLBool(object s)
        {
            if (s == null || s == System.DBNull.Value)
                return false;
            else
                return Convert.ToBoolean(s);
        }


        public static byte[] NULLBytes(object s)
        {
            if (s == null || s == System.DBNull.Value)
                return null;
            else
                return s as byte[];
        }

        public static float NULLFloat(object s)
        {
            if (s == null || s == System.DBNull.Value)
                return 0.0F;
            else
                return Convert.ToSingle(s);
        }

        public static double NULLDouble(object s)
        {
            if (s == null || s == System.DBNull.Value)
                return 0.0D;
            else
                return Convert.ToDouble(s);
        }

        public static decimal NULLDecimal(object s)
        {
            if (s == null || s == System.DBNull.Value)
                return 0.0M;
            else
                return Convert.ToDecimal(s);
        }
        #endregion
    }
}
