﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.Utils
{
    public sealed class Param
    {
        public string Name { get; set; }
        public object Value { get; set; }
        public DateTime DateTimeValue { get; set; }

    }
}
