﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.RetryPolicies;
using System.Configuration;
using System.IO;
using System.Diagnostics;
using Microsoft.Win32;
using System.Security;
using System.IO.Compression;
using Com.Meelo.Utils.Config;

namespace Com.Meelo.Utils.Azure
{
    public class WABlobStorageHelper
    {
        #region private fields

        private CloudStorageAccount _cloudStorageAccount;
        //  private StorageCredentials _credentials;
        private CloudBlobClient _blobClient;
        private CloudBlobContainer _blobContainer;
        private string _containerName = "microsoftsilverlining";
        private const string STORAGE_ACCOUNT_CONNECTION_STRING = "StorageAccountConnectionString";
        private const string STORAGE_ACC_FORMAT = "DefaultEndpointsProtocol={0};AccountName={1};AccountKey={2}";

        public TimeSpan? BlobRequestTimeout
        {
            get { return BlobClient.DefaultRequestOptions.ServerTimeout; }
            set { BlobClient.DefaultRequestOptions.ServerTimeout = value; }
        }


        #endregion

        public static readonly IRetryPolicy RETRY_POLICY = new LinearRetry(TimeSpan.Zero, 3);
        public const string STORAGE_CONNECTION_STRING_TEMPLATE = "DefaultEndpointsProtocol=https;AccountName={0};AccountKey={1}";
        public const string DIAGNOSTIC_CONNECTION_STRING_NAME = "Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString";


        public const int CacheControlMaxExpiration = Int32.MaxValue; // about 68 years
        public const int CacheControlOneWeekExpiration = 7 * 24 * 60 * 60; // 1 week
        public const int CacheControlOneHourExpiration = 60 * 60; // 1 hr

        public CloudStorageAccount CloudStorageAccount { get { return _cloudStorageAccount; } set { this._cloudStorageAccount = value; } }
        #region constructors

        /// <summary>
        /// Default constructor. 
        /// </summary>
        /// Loads the cloud storage account from the app.config or we.config file, and will override 
        public WABlobStorageHelper()
        {

        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="storageAccount"></param>
        public WABlobStorageHelper(CloudStorageAccount storageAccount)
        {
            this._cloudStorageAccount = storageAccount;
        }

        public WABlobStorageHelper(string configStringSettingName)
        {
            InitFromRoleConfigSetting(configStringSettingName);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="accountName"></param>
        /// <param name="accountKey"></param>
        /// <param name="useHttps"></param>
        public WABlobStorageHelper(string accountName, string accountKey, bool useHttps)
        {
            Init(accountName, accountKey, useHttps);
        }

        /// <summary>
        /// Overloaded constructor. Allows for creation of StorageHelper object with custom endpoints not defined in config files.
        /// </summary>
        /// <param name="accountName">Account name in Windows Azure portal.</param>
        /// <param name="accountKey">Account key.</param>
        /// <param name="blobEndpointURI"></param>
        /// <param name="queueEndpointURI"></param>
        /// <param name="tableEndpointURI"></param>
        public WABlobStorageHelper(string accountName, string accountKey, Uri blobEndpointURI, Uri queueEndpointURI, Uri tableEndpointURI)
        {
            StorageCredentials credentials = new StorageCredentials(accountName, accountKey);
            this._cloudStorageAccount = new CloudStorageAccount(credentials, blobEndpointURI, queueEndpointURI, tableEndpointURI, null);
        }



        public static WABlobStorageHelper GetWAStorageHelper(string storageAccountConnectionString)
        {
            //if (accountName.IndexOf("devstoreaccount") > -1)
            //{
            //    return new WAStorageHelper("StorageAccountConnectionString");
            //}
            //else
            //{
            //    return new WAStorageHelper(accountName, accountKey, useHttps);
            //}

            return new WABlobStorageHelper(CloudStorageAccount.Parse(storageAccountConnectionString));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accountName"></param>
        /// <param name="accountKey"></param>
        /// <param name="useHttps"></param>
        /// <returns></returns>
        public static CloudStorageAccount GetCloudStorageAccount(string accountName, string accountKey, bool useHttps)
        {

            WABlobStorageHelper was = new WABlobStorageHelper(accountName, accountKey, useHttps);
            return was.CloudStorageAccount;


        }
        #endregion

        #region properties

        /// <summary>
        /// The Account information used to access storage.
        /// </summary>
        /// <remarks>Defaults to retrieving from DataConnectionString configuration settings if not set explicitly or in the constructor.</remarks>
        public CloudStorageAccount StorageAccountInfo
        {
            get { return this._cloudStorageAccount; }
            set { this._cloudStorageAccount = value; }
        }

        public CloudBlobContainer BlobContainer
        {
            get
            {
                if (this._blobContainer == null)
                {
                    this._blobContainer = GetBlobContainer(_containerName);
                }
                return this._blobContainer;
            }
            set { this._blobContainer = value; }
        }

        public CloudBlobClient BlobClient
        {
            get
            {
                // use a singleton pattern to create this so it's only loaded when necessary, and only created once.
                if (this._blobClient == null)
                {
                    this._blobClient = this._cloudStorageAccount.CreateCloudBlobClient();
                    this._blobClient.DefaultRequestOptions.ServerTimeout = TimeSpan.FromMinutes(5);
                }
                return this._blobClient;
            }
            set { this._blobClient = value; }
        }



        #endregion



        #region blob methods

        #region container methods

        /// <summary>
        /// Return a Blob container.
        /// </summary>
        /// <returns>a CloudBlobContainer object.</returns>
        public CloudBlobContainer GetBlobContainer(string address)
         {
            CloudBlobContainer container = BlobClient.GetContainerReference(address.ToLower());
            container.CreateIfNotExists();
            container.FetchAttributes();
            return container;
        }

        /// <summary>
        /// Return a list of BLOB containers.
        /// </summary>
        /// <returns></returns>
        /// 
        public IEnumerable<CloudBlobContainer> GetContainers(string prefix = null, int timeoutInMinutes = 5)
        {

            BlobClient.DefaultRequestOptions.ServerTimeout = TimeSpan.FromMinutes(timeoutInMinutes);
            if (!string.IsNullOrEmpty(prefix))
            {
                return this.BlobClient.ListContainers(prefix, ContainerListingDetails.All);
            }
            else
            {
                return this.BlobClient.ListContainers();
            }
        }

        public string[] GetContainerNames(IEnumerable<CloudBlobContainer> containers)
        {
            if (containers != null && containers.Count() > 0)
            {
                var cN = from CloudBlobContainer b in containers
                         select b.Name;
                return cN.ToArray<string>();
            }
            return new string[] { };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="containerName"></param>
        /// <returns></returns>
        public bool DoesContainerExist(string containerName)
        {
            CloudBlobContainer container = GetBlobContainer(containerName);
            return container.Exists();
        }

        public ContainerResultSegment GetContainerSegmented(string prefix, int maxResults, BlobContinuationToken continuationToken, int timeoutInMinutes = 5)
        {
            BlobClient.DefaultRequestOptions.ServerTimeout = TimeSpan.FromMinutes(timeoutInMinutes);
            return BlobClient.ListContainersSegmented(prefix, ContainerListingDetails.All, maxResults, continuationToken);
        }

        public IList<CloudBlobContainer> GetContainers(string prefix, TimeSpan timeoutInMinutes)
        {
            BlobClient.DefaultRequestOptions.ServerTimeout = timeoutInMinutes;
            return BlobClient.ListContainers(prefix, ContainerListingDetails.All).ToList<CloudBlobContainer>();
        }

        public bool CreateContainer(string containerName)
        {
            CloudBlobContainer container = GetBlobContainer(containerName);
            //GetBlobContainer already calls CreateifNotExists
            //return container.CreateIfNotExists();
            return true;
        }

        public bool CreateContainer(string containerName, BlobContainerPermissions permissions, IDictionary<string, string> metadata)
        {

            CloudBlobContainer container = GetBlobContainer(containerName);
            bool result = container.CreateIfNotExists();
            if (result)
            {
                container.SetPermissions(permissions);

                if (metadata != null && metadata.Count > 0)
                {
                    foreach (KeyValuePair<string, string> kp in metadata)
                    {
                        container.Metadata.Add(kp);
                    }
                }

                container.SetMetadata();
            }
            return result;
        }

        public BlobContainerProperties GetContainerProperties(string containerName)
        {
            CloudBlobContainer container = GetBlobContainer(containerName);
            return container.Properties;
        }

        public void SetContainerMetadata(string containerName, IDictionary<string, string> metadata)
        {
            CloudBlobContainer container = GetBlobContainer(containerName);
            container.CreateIfNotExists();
            container.Metadata.Clear();
            if (metadata != null && metadata.Count > 0)
            {
                foreach (KeyValuePair<string, string> kp in metadata)
                {
                    container.Metadata.Add(kp);
                }
            }

            container.SetMetadata();
        }

        public BlobContainerPermissions GetContainerPermissions(string containerName)
        {
            CloudBlobContainer container = GetBlobContainer(containerName);
            return container.GetPermissions();
        }

        public void SetContainerPermissions(string containerName, BlobContainerPermissions permissions)
        {
            CloudBlobContainer container = GetBlobContainer(containerName);
            container.SetPermissions(permissions);
        }

        public void DeleteContainer(string containerName)
        {
            CloudBlobContainer container = GetBlobContainer(containerName);
            container.Delete();
        }

        public IEnumerable<string> ListBlobNames(string containerName, string prefix = null)
        {
            IEnumerable<IListBlobItem> blobItems = ListBlobs(containerName, prefix);
            IEnumerable<string> names = ParseBlobNames(blobItems);

            return names;
        }


        public IEnumerable<string> ListBlobNames(string containerName, string prefix, BlobRequestOptions options)
        {
            IEnumerable<IListBlobItem> blobItems = ListBlobs(containerName, prefix, options);
            IEnumerable<string> names = ParseBlobNames(blobItems);

            return names;
        }

        public static IEnumerable<string> ParseBlobNames(IEnumerable<IListBlobItem> blobItems)
        {
            var blobNames = from item in blobItems
                            select Path.GetFileName(item.Uri.LocalPath);
            //select item.Container.Uri.MakeRelativeUri(item.Uri).ToString(); // item.Uri.AbsoluteUri.Replace(item.Container.Uri.AbsoluteUri, string.Empty);

            return blobNames;
        }

        private IEnumerable<string> ListBlobNames(string containerName, string prefix, int maxResults, BlobRequestOptions options = null, int timeoutInMinutes = 5)
        {
            if (maxResults == 0) maxResults = 100;
            BlobContinuationToken continuation = null;
            IEnumerable<string> names = null;

            do
            {
                BlobResultSegment resultSegment = ListBlobs(containerName, prefix, maxResults, continuation, options, timeoutInMinutes);
                continuation = resultSegment.ContinuationToken;
                if (names == null)
                    names = ParseBlobNames(resultSegment.Results);
                else
                    names.Concat(ParseBlobNames(resultSegment.Results));
            }
            while (continuation != null);

            return names;
        }

        private IEnumerable<IListBlobItem> ListBlobsSegmented(string containerName, string prefix, int maxResults, BlobRequestOptions options, int timeoutInMinutes = 5)
        {
            if (maxResults == 0) maxResults = 100;
            BlobContinuationToken continuation = null;
            IEnumerable<IListBlobItem> names = null;

            do
            {
                BlobResultSegment resultSegment = ListBlobs(containerName, prefix, maxResults, continuation, options, timeoutInMinutes);
                continuation = resultSegment.ContinuationToken;
                if (names == null)
                    names = resultSegment.Results;
                else
                    names.Concat(resultSegment.Results);
            }
            while (continuation != null);

            return names;
        }

        private IEnumerable<CloudBlobContainer> ListContainersSegmented(string containerName, string prefix, int maxResults, BlobRequestOptions options, int timeoutInMinutes = 5, ContainerListingDetails listingDetails = ContainerListingDetails.None)
        {
            if (maxResults == 0) maxResults = 100;
            BlobContinuationToken continuation = null;
            IEnumerable<CloudBlobContainer> names = null;

            do
            {
                ContainerResultSegment resultSegment = ListContainersSegmented(prefix, maxResults, continuation, options, timeoutInMinutes, listingDetails);
                continuation = resultSegment.ContinuationToken;
                if (names == null)
                    names = resultSegment.Results;
                else
                    names.Concat(resultSegment.Results);
            }
            while (continuation != null);

            return names;
        }

        public IEnumerable<IListBlobItem> ListBlobs(string containerName, string prefix, int maxResults, ref BlobContinuationToken continuationToken, BlobRequestOptions options, int timeoutInMinutes = 5)
        {
            BlobResultSegment blobResultSegment = ListBlobs(containerName, prefix, maxResults, continuationToken, options, timeoutInMinutes);
            if (blobResultSegment != null && blobResultSegment.Results.Count() > 0)
            {
                continuationToken = blobResultSegment.ContinuationToken; // set so that next query can get next set of results
                return FilterBlobCollection(blobResultSegment.Results.AsParallel(), item => item.Container.Name == containerName);
            }

            return null;
        }

        public BlobResultSegment ListBlobs(string containerName, string prefix, int maxResults, BlobContinuationToken continuationToken, BlobRequestOptions options)
        {
            return BlobClient.ListBlobsSegmented(string.Format("\\{0}\\{1}", containerName, prefix), true, BlobListingDetails.All, maxResults, continuationToken, options, null);
        }

        public BlobResultSegment ListBlobs(string containerName, string prefix, int maxResults, BlobContinuationToken continuationToken, bool useFlatListing, BlobListingDetails listingDetails, BlobRequestOptions options)
        {
            return BlobClient.ListBlobsSegmented(string.Format("\\{0}\\{1}", containerName, prefix), true, listingDetails, maxResults, continuationToken, options, null);
        }
        /// <summary>
        /// Returns all blobs in the container
        /// </summary>
        /// <param name="containerName">The name of the container.</param>
        /// <param name="prefix">A prefix for filtering blobs. Use null to get all blobs.</param>
        /// <returns>A list of blobs found in the container matching the prefix.</returns>
        public IEnumerable<IListBlobItem> ListBlobs(string containerName, string prefix, int timeoutInMinutes = 5)
        {
            return ListBlobs(containerName, prefix, new BlobRequestOptions() { ServerTimeout = TimeSpan.FromMinutes(timeoutInMinutes) });
        }

        /// <summary>
        /// Returns all blobs in the container.
        /// </summary>
        /// <param name="containerName">The name of the container.</param>
        /// <param name="prefix">A prefix for filtering blobs. Use null to get all blobs.</param>
        /// <param name="options">Options for the request.</param>
        /// <returns>A list of blobs found in the container matching the prefix.</returns>
        public IEnumerable<IListBlobItem> ListBlobs(string containerName, string prefix, BlobRequestOptions options)
        {

            string adjustedPrefix = string.Format("{0}/{1}", containerName, prefix);
            return BlobClient.ListBlobs(adjustedPrefix, true, BlobListingDetails.All, options);
        }

        public BlobResultSegment ListBlobs(string containerName, string prefix, int maxResults, BlobContinuationToken continuationToken, BlobRequestOptions options, int timeoutInMinutes = 5, bool useFlatBlobListing = true)
        {
            CloudBlobContainer container = GetBlobContainer(containerName);
            string adjustedPrefix = string.Format("{0}/{1}", containerName, prefix);
            BlobClient.DefaultRequestOptions.ServerTimeout = TimeSpan.FromMinutes(timeoutInMinutes);
            return BlobClient.ListBlobsSegmented(adjustedPrefix, useFlatBlobListing, BlobListingDetails.All, maxResults, continuationToken, options, null);
        }


        public ContainerResultSegment ListContainersSegmented(string prefix, int maxResults, BlobContinuationToken continuationToken, BlobRequestOptions options, int timeoutInMinutes = 5, ContainerListingDetails listingDetails = ContainerListingDetails.None)
        {
            BlobClient.DefaultRequestOptions.ServerTimeout = TimeSpan.FromMinutes(timeoutInMinutes);
            return BlobClient.ListContainersSegmented(prefix, listingDetails, maxResults, continuationToken, options, null);
        }

        public IEnumerable<IListBlobItem> FilterBlobCollection(IEnumerable<IListBlobItem> blobCollection, Func<IListBlobItem, bool> predicate)
        {
            return blobCollection.Where<IListBlobItem>(predicate);
        }

        public string GetSharedAccessSignatureForPackageContainer(string containerName, string policyName, int expirationHours)
        {

            //Get a reference to the container for which shared access signature will be created.
            CloudBlobContainer container = BlobClient.GetContainerReference(containerName);
            bool created = container.CreateIfNotExists();

            if (created)
            {

                return CreateBlobContainerPolicyAndReturnSAS(container, policyName, expirationHours, SharedAccessBlobPermissions.Write | SharedAccessBlobPermissions.Read);

            }
            else
            {

                BlobContainerPermissions blobPermissions = container.GetPermissions();

                if (blobPermissions != null && blobPermissions.SharedAccessPolicies.Count > 0)
                {
                    SharedAccessBlobPolicy policy = (from perm in blobPermissions.SharedAccessPolicies
                                                     where perm.Key == policyName
                                                     select perm.Value).FirstOrDefault();

                    if (policy != null)
                    {
                        if (policy.SharedAccessExpiryTime > DateTime.UtcNow.AddHours(expirationHours))
                        {
                            //valid policy
                            return container.GetSharedAccessSignature(new SharedAccessBlobPolicy(), policyName);
                        }
                        else
                        {

                            //policy expired
                            blobPermissions.SharedAccessPolicies.Remove(policyName);
                            container.SetPermissions(blobPermissions);

                            return CreateBlobContainerPolicyAndReturnSAS(container, policyName, expirationHours, SharedAccessBlobPermissions.Write | SharedAccessBlobPermissions.Read);
                        }


                    }
                    else
                    {

                        return CreateBlobContainerPolicyAndReturnSAS(container, policyName, expirationHours, SharedAccessBlobPermissions.Write | SharedAccessBlobPermissions.Read);

                    }
                }
                else
                {
                    return CreateBlobContainerPolicyAndReturnSAS(container, policyName, expirationHours, SharedAccessBlobPermissions.Write | SharedAccessBlobPermissions.Read);


                }


            }

            //Get the shared access signature to share with clients.
            //Note that this call passes in an empty access policy, so that the shared access signature will use the 
            //'mypolicy' access policy that's defined on the container.
            // return container.GetSharedAccessSignature(new SharedAccessBlobPolicy(), policyName);
        }

        private string CreateBlobContainerPolicyAndReturnSAS(CloudBlobContainer container, string policyName, int expirationHours, SharedAccessBlobPermissions perm)
        {

            //Create a permission policy, consisting of a container-level access policy and a public access setting, and store it on the container. 
            BlobContainerPermissions blobPermissions = new BlobContainerPermissions();
            //The container-level access policy provides read/write access to the container for 10 hours.
            blobPermissions.SharedAccessPolicies.Add(policyName, new SharedAccessBlobPolicy()
            {
                // SharedAccessStartTime = DateTime.UtcNow,
                SharedAccessStartTime = DateTime.UtcNow.AddMinutes(-5),

                SharedAccessExpiryTime = DateTime.UtcNow.AddHours(expirationHours),
                Permissions = perm
            });

            //The public access setting explicitly specifies that the container is private, so that it can't be accessed anonymously.
            blobPermissions.PublicAccess = BlobContainerPublicAccessType.Off;

            //Set the permission policy on the container.
            container.SetPermissions(blobPermissions);


            return container.GetSharedAccessSignature(new SharedAccessBlobPolicy(), policyName);
        }

        public string GetSharedAccessSignatureForPackageContainer(string containerName, string policyName, int expirationHours, SharedAccessBlobPermissions perm)
        {

            //Get a reference to the container for which shared access signature will be created.
            CloudBlobContainer container = BlobClient.GetContainerReference(containerName);
            bool created = container.CreateIfNotExists();

            if (created)
            {

                return CreateBlobContainerPolicyAndReturnSAS(container, policyName, expirationHours, perm);

            }
            else
            {

                BlobContainerPermissions blobPermissions = container.GetPermissions();

                if (blobPermissions != null && blobPermissions.SharedAccessPolicies.Count > 0)
                {
                    SharedAccessBlobPolicy policy = (from perms in blobPermissions.SharedAccessPolicies
                                                     where perms.Key == policyName
                                                     select perms.Value).FirstOrDefault();

                    if (policy != null)
                    {

                        return container.GetSharedAccessSignature(new SharedAccessBlobPolicy(), policyName);

                    }
                    else
                    {

                        return CreateBlobContainerPolicyAndReturnSAS(container, policyName, expirationHours, perm);

                    }
                }
                else
                {
                    return CreateBlobContainerPolicyAndReturnSAS(container, policyName, expirationHours, perm);


                }


            }
        }
        #endregion

        #region blob operations

        #region Byte[] put

        public string PutBlob(string containerName, string blobName, Byte[] contents, int timeoutInMinutes = 5)
        {
            ICloudBlob blob = this.GetBlob(containerName, blobName);
            return PutBlob(blob, contents, timeoutInMinutes);

        }

        public string PutBlob(string containerName, string blobName, Byte[] contents, bool overwrite, int timeoutInMinutes = 5)
        {
            ICloudBlob blob = this.GetBlob(containerName, blobName);
            AccessCondition acs;
            BlobRequestOptions options = CreateOverwriteOption(blob, overwrite, out acs, timeoutInMinutes);
            return PutBlob(blob, contents, overwrite, timeoutInMinutes);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="containerName"></param>
        /// <param name="blobName"></param>
        /// <param name="contents"></param>
        /// <param name="fileName">File name is used only for setting content type. The file is not uploaded from file system</param>
        /// <param name="overwrite"></param>
        /// <param name="metadata"></param>
        public string PutBlob(string containerName, string blobName, Byte[] contents, string fileName, bool overwrite, IDictionary<string, string> metadata, int timeoutInMinutes = 5)
        {
            ICloudBlob blob = this.GetBlob(containerName, blobName);
            if (metadata != null && metadata.Count > 0)
            {
                foreach (KeyValuePair<string, string> kp in metadata)
                {
                    blob.Metadata.Add(kp);
                }
            }
            AccessCondition acs;
            BlobRequestOptions options = CreateOverwriteOption(blob, overwrite, out acs, timeoutInMinutes);
            return PutBlob(blob, contents, fileName, overwrite, timeoutInMinutes);

        }




        /// <summary>
        /// Uploads string contents with metadata
        /// </summary>
        /// <param name="containerName"></param>
        /// <param name="blobName"></param>
        /// <param name="contents"></param>
        /// <param name="overwrite"></param>
        /// <param name="metadata"></param>
        public string PutBlob(string containerName, string blobName, string contents, bool overwrite, IDictionary<string, string> metadata, int timeoutInMinutes = 5)
        {
            AccessCondition acs;
            ICloudBlob blob = this.GetBlob(containerName, blobName);
            if (metadata != null && metadata.Count > 0)
            {
                foreach (KeyValuePair<string, string> kp in metadata)
                {
                    blob.Metadata.Add(kp);
                }
            }
            BlobRequestOptions options = CreateOverwriteOption(blob, overwrite, out acs, timeoutInMinutes);
            return PutBlob(blob, contents, overwrite);

        }

        public string PutBlob(ICloudBlob blob, Byte[] contents, int timeoutInMinutes = 5)
        {
            return PutBlob(blob, contents, true, timeoutInMinutes);
        }

        public string PutBlob(ICloudBlob blob, Byte[] contents, bool overwrite, int timeoutInMinutes = 5)
        {
            AccessCondition acs;
            BlobRequestOptions options = CreateOverwriteOption(blob, overwrite, out acs, timeoutInMinutes);
            using (MemoryStream mem = new MemoryStream(contents))
            {
                blob.UploadFromStream(mem, acs, options);


                return blob.Uri.ToString();
            }
        }

        public void SetCacheControl(ICloudBlob blob, string cacheControl = "public, max-age=31536000, must-revalidate")
        {
            blob.SetCacheControl(cacheControl);
        }

        public void PutBlobAsync(ICloudBlob blob, Byte[] contents, bool overwrite, int timeoutInMinutes = 5)
        {

            AccessCondition acs;
            BlobRequestOptions options = CreateOverwriteOption(blob, overwrite, out acs, timeoutInMinutes);
            IAsyncResult asynResult = null;
            using (System.Threading.ManualResetEvent evt = new System.Threading.ManualResetEvent(false))
            {
                MemoryStream mem = new MemoryStream(contents);
                asynResult = blob.BeginUploadFromStream(mem, acs, options, null, result =>
                {
                    MemoryStream c = (MemoryStream)result.AsyncState;

                    c.Close();
                    evt.Set();

                }, mem);

                evt.WaitOne();
            }


        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="blob"></param>
        /// <param name="contents"></param>
        /// <param name="fileName">File name is used only for setting content type. The file is not uploaded from file system</param>
        /// <param name="overwrite"></param>
        public string PutBlob(ICloudBlob blob, Byte[] contents, string fileName, bool overwrite, int timeoutInMinutes = 5)
        {

            blob.Properties.ContentType = WABlobStorageHelper.GetContentTypeFromExtension(Path.GetExtension(fileName));
            return PutBlob(blob, contents, overwrite, timeoutInMinutes);
        }
        #endregion

        #region File put

        public void PutBlobFromFile(string containerName, string blobName, string fileName, int timeoutInMinutes = 5)
        {
            ICloudBlob blob = this.GetBlob(containerName, blobName);
            PutBlobFromFile(blob, fileName);
        }

        public void PutBlobFromFile(string containerName, string blobName, string fileName, IDictionary<string, string> metadata)
        {
            ICloudBlob blob = this.GetBlob(containerName, blobName);
            if (metadata != null && metadata.Count > 0)
            {
                foreach (KeyValuePair<string, string> kp in metadata)
                {
                    blob.Metadata.Add(kp);
                }
            }
            PutBlobFromFile(blob, fileName);
        }

        public void PutBlobFromFile(ICloudBlob blob, string fileName, int timeoutInMinutes = 5)
        {
            PutBlobFromFile(blob, fileName, true, timeoutInMinutes);
        }

        public void PutBlobFromFile(string containerName, string blobName, string fileName, bool overwrite, int timeoutInMinutes = 5)
        {
            ICloudBlob blob = this.GetBlob(containerName, blobName);
            PutBlobFromFile(blob, fileName, overwrite, timeoutInMinutes);
        }

        public void PutBlobFromFile(string containerName, string blobName, string fileName, bool overwrite, bool zip, IDictionary<string, string> metadata, int timeoutInMinutes = 5)
        {
            ICloudBlob blob = this.GetBlob(containerName, blobName);
            if (metadata != null && metadata.Count > 0)
            {
                foreach (KeyValuePair<string, string> kp in metadata)
                {
                    blob.Metadata.Add(kp);
                }
            }
            if (zip)
                UploadGZipCompressedFileToBlob(blob, fileName, overwrite, timeoutInMinutes);
            else
                PutBlobFromFile(blob, fileName, overwrite, timeoutInMinutes);
        }

        public void PutBlobFromFile(ICloudBlob blob, string fileName, bool overwrite, int timeoutInMinutes = 5)
        {
            AccessCondition acs;
            BlobRequestOptions options = CreateOverwriteOption(blob, overwrite, out acs, timeoutInMinutes);

            blob.Properties.ContentType = WABlobStorageHelper.GetContentTypeFromExtension(Path.GetExtension(fileName));
            using (FileStream fs = File.OpenRead(fileName))
            {
                blob.UploadFromStream(fs, acs, options);
            }
        }

        #endregion

        #region Stream put

        public string PutBlob(string containerName, string blobName, Stream contents, int timeoutInMinutes = 5)
        {
            ICloudBlob blob = this.GetBlob(containerName, blobName);
            PutBlob(blob, contents);
            return blob.Uri.ToString();
        }

        public string PutBlob(string containerName, string blobName, Stream contents, bool overwrite)
        {
            ICloudBlob blob = this.GetBlob(containerName, blobName);

            PutBlob(blob, contents, overwrite);
            return blob.Uri.ToString();
        }

        public string PutBlob(string containerName, string blobName, Stream contents, bool overwrite, IDictionary<string, string> metadata)
        {
            ICloudBlob blob = this.GetBlob(containerName, blobName);
            if (metadata != null && metadata.Count > 0)
            {
                foreach (KeyValuePair<string, string> kp in metadata)
                {
                    blob.Metadata.Add(kp);
                }
            }

            PutBlob(blob, contents, overwrite);
            return blob.Uri.ToString();
        }
        public string PutBlob(ICloudBlob blob, Stream contents, int timeoutInMinutes = 5)
        {
            blob.UploadFromStream(contents, AccessCondition.GenerateEmptyCondition(), new BlobRequestOptions() { ServerTimeout = TimeSpan.FromMinutes(timeoutInMinutes) });
            return blob.Uri.ToString();
        }

        public string PutBlob(string blobUri, Stream contents, int timeoutInMinutes = 5)
        {
            ICloudBlob blob = GetBlob(blobUri);
            return PutBlob(blob, contents, timeoutInMinutes);
        }

        public string PutBlob(string blobUri, Stream contents, bool overwrite, int timeoutInMinutes = 5)
        {
            ICloudBlob blob = GetBlob(blobUri);
            return PutBlob(blob, contents, overwrite, timeoutInMinutes);
        }
        public string PutBlob(ICloudBlob blob, Stream contents, bool overwrite, int timeoutInMinutes = 5)
        {
            AccessCondition acs;
            BlobRequestOptions options = CreateOverwriteOption(blob, overwrite, out acs, timeoutInMinutes);
            blob.UploadFromStream(contents, acs, options);
            return blob.Uri.ToString();
        }

        #endregion

        #region string put

        public string PutBlob(string containerName, string blobName, string contents, int timeoutInMinutes = 5, BlobType blobType = BlobType.BlockBlob)
        {
            ICloudBlob blob = this.GetBlob(containerName, blobName, false, blobType);
            PutBlob(blob, contents, timeoutInMinutes);
            return blob.Uri.ToString();
        }

        public string PutBlob(string containerName, string blobName, string contents, bool overwrite, int timeoutInMinutes = 5, BlobType blobType = BlobType.BlockBlob)
        {
            ICloudBlob blob = this.GetBlob(containerName, blobName, false, blobType);

            PutBlob(blob, contents, overwrite, timeoutInMinutes);
            return blob.Uri.ToString();
        }

        public string PutBlob(ICloudBlob blob, string contents, int timeoutInMinutes = 5)
        {
            return PutBlob(blob, contents, true, timeoutInMinutes);
        }

        public string PutBlob(ICloudBlob blob, string contents, bool overwrite, int timeoutInMinutes = 5)
        {
            AccessCondition acs;
            BlobRequestOptions options = CreateOverwriteOption(blob, overwrite, out acs, timeoutInMinutes);
            Encoding encoding = GetDefaultEncoding();
            using (MemoryStream mem = new MemoryStream(encoding.GetBytes(contents)))
            {
                blob.UploadFromStream(mem, acs, options); // UTF 8 is consistent with single parameter overloaded method
                return blob.Uri.ToString();
            }
        }

        #endregion

        public ICloudBlob GetBlob(string containerName, string blobName)
        {
            CloudBlobContainer container = GetBlobContainer(containerName);
            ICloudBlob blob = container.GetBlockBlobReference(blobName);
            return blob;
        }

        public ICloudBlob CreateBlob(string containerName, string blobName, byte[] contents, int timeoutInMinutes = 5)
        {
            ICloudBlob blob = GetBlob(containerName, blobName);
            PutBlob(blob, contents, timeoutInMinutes);
            return blob;
        }

        public ICloudBlob CreateBlob(string containerName, string blobName, string contents, int timeoutInMinutes = 5)
        {
            ICloudBlob blob = GetBlob(containerName, blobName);
            PutBlob(blob, contents, timeoutInMinutes);
            return blob;
        }

        public ICloudBlob CreateBlob(string containerName, string blobName, Stream contents, int timeoutInMinutes = 5)
        {
            ICloudBlob blob = GetBlob(containerName, blobName);
            PutBlob(blob, contents, timeoutInMinutes);
            return blob;
        }

        public ICloudBlob CreateBlobFileContent(string containerName, string blobName, string fileName, int timeoutInMinutes = 5)
        {
            BlobRequestOptions largeBlobDownloadOptions = new BlobRequestOptions()
            {
                ServerTimeout = TimeSpan.FromMinutes(timeoutInMinutes) // Larger timeout for larger file

            };
            ICloudBlob blob = GetBlob(containerName, fileName);
            PutBlobFromFile(blob, fileName, false); // since we're creating, we shouldn't be overwriting anything
            return blob;
        }

        public void DeleteBlob(string containerName, string blobName, int timeoutInMinutes = 5)
        {
            CloudBlobContainer container = GetBlobContainer(containerName);
            ICloudBlob blob = container.GetBlobReferenceFromServer(blobName);
            blob.DeleteIfExists(DeleteSnapshotsOption.None, AccessCondition.GenerateEmptyCondition(), new BlobRequestOptions() { ServerTimeout = TimeSpan.FromMinutes(timeoutInMinutes) });
        }

        public void DeleteBlobs(IEnumerable<ICloudBlob> blobs, int timeoutInMinutes = 5)
        {
            BlobRequestOptions largeBlobDownloadOptions = new BlobRequestOptions()
            {
                ServerTimeout = TimeSpan.FromMinutes(timeoutInMinutes) // Larger timeout for larger file

            };
            Parallel.ForEach<ICloudBlob>(blobs, blob => { blob.DeleteIfExists(DeleteSnapshotsOption.None, AccessCondition.GenerateEmptyCondition(), largeBlobDownloadOptions); });
        }

        /// <summary>
        /// Deletes the blob if it has not been modified since the blob was retrieved.
        /// </summary>
        /// <param name="blob">The blob to be deleted.</param>
        /// <returns>True if the blob was deleted, false otherwise.</returns>
        public bool DeleteBlobIfNotModified(ICloudBlob blob, int timeoutInMinutes = 5)
        {
            // This tested slower - probably due to exception overhead
            try
            {
                BlobRequestOptions options = new BlobRequestOptions
                {
                    ServerTimeout = TimeSpan.FromMinutes(timeoutInMinutes)
                };
                blob.Delete(DeleteSnapshotsOption.None, AccessCondition.GenerateIfNotModifiedSinceCondition((DateTimeOffset)blob.Properties.LastModified), options);
                return true;
            }
            catch (StorageException)
            {
                // if (ex. == StorageErrorCode.ConditionFailed)
                return false; // blob has been modified since downloading, and was not deleted
                // throw ex; // something else went wrong
            }
        }

        public bool DoesBlobExist(string containerName, string blobName)
        {
            ICloudBlob blob = GetBlob(containerName, blobName);
            return blob.Exists();
        }

        #region Get Contents

        public byte[] GetBlobContentsAsByte(string containerName, string blobName, int timeoutInMinutes = 5)
        {
            BlobRequestOptions largeBlobDownloadOptions = new BlobRequestOptions()
            {
                ServerTimeout = TimeSpan.FromMinutes(timeoutInMinutes) // Larger timeout for larger file

            };
            ICloudBlob blob = GetBlob(containerName, blobName);
            return GetBlobContentsAsByte(blob, largeBlobDownloadOptions);
        }

        public byte[] GetBlobContentsAsByte(string containerName, string blobName, BlobRequestOptions options)
        {
            ICloudBlob blob = GetBlob(containerName, blobName);
            return GetBlobContentsAsByte(blob, options);
        }

        public byte[] GetBlobContentsAsByte(ICloudBlob blob, BlobRequestOptions options)
        {

            using (MemoryStream mem = new MemoryStream())
            {
                blob.DownloadToStream(mem, AccessCondition.GenerateEmptyCondition(), options);
                return mem.ToArray();
            }
        }

        public string GetBlobContentsAsText(string containerName, string blobName, int timeoutInMinutes = 5)
        {
            BlobRequestOptions largeBlobDownloadOptions = new BlobRequestOptions()
            {
                ServerTimeout = TimeSpan.FromMinutes(timeoutInMinutes) // Larger timeout for larger file

            };
            ICloudBlob blob = GetBlob(containerName, blobName);

            Encoding encoding = GetDefaultEncoding();


            byte[] array = GetBlobContentsAsByte(containerName, blobName, timeoutInMinutes);
            return encoding.GetString(array);

        }

        public string GetBlobContentsAsText(string blobAbsoluteUri, int timeoutInMinutes = 5)
        {
            BlobRequestOptions largeBlobDownloadOptions = new BlobRequestOptions()
            {
                ServerTimeout = TimeSpan.FromMinutes(timeoutInMinutes) // Larger timeout for larger file

            };
            ICloudBlob blob = GetBlob(blobAbsoluteUri);

            Encoding encoding = GetDefaultEncoding();

            byte[] array = GetBlobContentsAsByte(blob, largeBlobDownloadOptions);

            return encoding.GetString(array);
        }
        public string GetBlobContentsAsText(string containerName, string blobName, BlobRequestOptions options)
        {


            Encoding encoding = GetDefaultEncoding();

            byte[] array = GetBlobContentsAsByte(containerName, blobName, options);

            return encoding.GetString(array);

        }
        /// <summary>
        /// Gets the default encoding for the blob, which is UTF-8.
        /// </summary>
        /// <returns>The default <see cref="Encoding"/> object.</returns>
        private static Encoding GetDefaultEncoding()
        {
            Encoding encoding = Encoding.UTF8;

            return encoding;
        }
        public ICloudBlob GetBlobContentsAsFile(string containerName, string blobName, string fileName, int timeoutInMinutes = 5, bool fetchMetadata = false)
        {
            BlobClient.DefaultRequestOptions.ServerTimeout = TimeSpan.FromMinutes(5);


            BlobRequestOptions largeBlobDownloadOptions = new BlobRequestOptions()
            {
                ServerTimeout = TimeSpan.FromMinutes(timeoutInMinutes)
                // Larger timeout for larger file

            };

            return GetBlobContentsAsFile(containerName, blobName, fileName, largeBlobDownloadOptions, fetchMetadata);


        }
        public void GetBlobAsFileWithCleanup(string containerName, string blobName, string destinationFilePath, int timeoutInMinutes = 5)
        {
            if (File.Exists(destinationFilePath))
            {
                try
                {

                    File.Delete(destinationFilePath);
                }
                catch (Exception ex)
                {
                    Trace.WriteLine("Exception deleting old file " + ex.Message, "Error");

                }

            }
            if (!File.Exists(destinationFilePath))
            {
                GetBlobContentsAsFile(containerName, blobName, destinationFilePath, timeoutInMinutes);


            }
            //else
            //{
            //    LogInfo("Assembly already exists and is loaded. Develop an Unload Assembly function to unload the assembly when done so that it is not locked.");
            //    LogInfo("Loading and Unloading Assemblies: http://people.oregonstate.edu/~reeset/blog/archives/466");

            //}


        }
        public ICloudBlob GetBlobContentsAsFile(string containerName, string blobName, string fileName, BlobRequestOptions options, bool fetchAttributes = false, AccessCondition acs = null)
        {

            ICloudBlob blob = null;

            using (var fileStream = File.Create(fileName))
            {
                blob = GetBlobContentsAsStream(containerName, blobName, fileStream, options, acs);

            }

            if (fetchAttributes && blob != null)
            {

                blob.FetchAttributes();
            }

            return blob;
        }



        public void GetBlobContentsAsFileIfNotModified(string containerName, string blobName, string fileName, int timeoutInMinutes = 5)
        {
            ICloudBlob blob = GetBlob(containerName, blobName);
            AccessCondition acs;
            BlobRequestOptions options = CreateIfNotModifiedOption(blob, out acs, timeoutInMinutes);

            try
            {
                GetBlobContentsAsFile(containerName, blobName, fileName, options, false, acs);
            }
            catch (StorageException ex)
            {
                //if (ex.ErrorCode == StorageErrorCode.BadRequest)
                //    throw new InvalidOperationException(string.Format("{0} was not downloaded, since the blob has been modified.", blobName));
                //else
                throw ex;
            }
        }

        //public void GetBlobContentsAsFileIfModified(string containerName, string blobName, string fileName, int timeoutInMinutes=5)
        //{
        //    ICloudBlob blob = GetBlob(containerName, blobName);
        //    AccessCondition acs;
        //    BlobRequestOptions options = CreateIfModifiedOption(blob, out acs, timeoutInMinutes);
        //    blob.FetchAttributes();
        //    try
        //    {
        //        GetBlobContentsAsFile(containerName, blobName, fileName, options, false, acs);
        //    }
        //    catch (StorageClientException ex)
        //    {
        //        if (ex.ErrorCode == StorageErrorCode.BadRequest)
        //            throw new InvalidOperationException(string.Format("{0} was not downloaded, since the blob has not been modified.", blobName));
        //        else
        //            throw ex;
        //    }
        //}

        public ICloudBlob GetBlobContentsAsStream(string containerName, string blobName, Stream stream, int timeoutInMinutes = 5)
        {
            BlobRequestOptions largeBlobDownloadOptions = new BlobRequestOptions()
            {
                ServerTimeout = TimeSpan.FromMinutes(timeoutInMinutes) // Larger timeout for larger file

            };
            ICloudBlob blob = GetBlob(containerName, blobName);
            blob.DownloadToStream(stream, AccessCondition.GenerateEmptyCondition(), largeBlobDownloadOptions);
            return blob;
        }
        public ICloudBlob GetBlobContentsAsStream(string containerName, string blobName, Stream stream, BlobRequestOptions options, AccessCondition acs = null)
        {
            ICloudBlob blob = GetBlob(containerName, blobName);
            if (acs == null)
            {
                acs = AccessCondition.GenerateEmptyCondition();

            }
            blob.DownloadToStream(stream, acs, options);

            return blob;
        }

        public void GetBlobContentsAsStream(string blobAbsoluteUri, Stream stream, int timeoutInMinutes = 5)
        {
            ICloudBlob blob = GetBlob(blobAbsoluteUri);
            BlobRequestOptions largeBlobDownloadOptions = new BlobRequestOptions()
            {
                ServerTimeout = TimeSpan.FromMinutes(timeoutInMinutes) // Larger timeout for larger file

            };
            blob.DownloadToStream(stream, AccessCondition.GenerateEmptyCondition(), largeBlobDownloadOptions);
        }

        public void GetBlobContentsAsStream(string blobAbsoluteUri, Stream stream, BlobRequestOptions options)
        {
            ICloudBlob blob = GetBlob(blobAbsoluteUri);
            blob.DownloadToStream(stream, AccessCondition.GenerateEmptyCondition(), options);
        }

        public static string GetSharedAccessSignatureBlobUrl(ICloudBlob cb, SharedAccessBlobPermissions perm, int minutesValid)
        {


            if (cb.BlobType == BlobType.BlockBlob)
            {
                CloudBlockBlob b = new CloudBlockBlob(cb.Uri, cb.ServiceClient.Credentials);
                return GetSharedAccessSignatureBlockBlobUrl(b, perm, minutesValid);
            }
            else if (cb.BlobType == BlobType.PageBlob)
            {

                CloudPageBlob b = new CloudPageBlob(cb.Uri, cb.ServiceClient.Credentials);
                return GetSharedAccessSignaturePageBlobUrl(b, perm, minutesValid);
            }
            else
            {

                CloudBlockBlob b = new CloudBlockBlob(cb.Uri, cb.ServiceClient.Credentials);
                return GetSharedAccessSignatureBlockBlobUrl(b, perm, minutesValid);

            }

        }

        public static string GetSharedAccessSignatureBlockBlobUrl(CloudBlockBlob cb, SharedAccessBlobPermissions perm, int minutesValid)
        {
            var readPolicy = new SharedAccessBlobPolicy()
            {
                Permissions = perm,
                //  SharedAccessStartTime = DateTime.UtcNow,
                SharedAccessExpiryTime = DateTime.UtcNow.AddMinutes(minutesValid)

            };
            return cb.Uri.AbsoluteUri + cb.GetSharedAccessSignature(readPolicy);

        }

        public static string GetSharedAccessSignaturePageBlobUrl(CloudPageBlob cb, SharedAccessBlobPermissions perm, int minutesValid)
        {
            var readPolicy = new SharedAccessBlobPolicy()
            {
                Permissions = perm,
                //  SharedAccessStartTime = DateTime.UtcNow,
                SharedAccessExpiryTime = DateTime.UtcNow.AddMinutes(minutesValid)

            };
            return cb.Uri.AbsoluteUri + cb.GetSharedAccessSignature(readPolicy);

        }

        public string GetSharedAccessSignatureBlobUrl(string blobAbsoluteUri, SharedAccessBlobPermissions perm, int minutesValid)
        {
            ICloudBlob blob = GetBlob(blobAbsoluteUri);
            return GetSharedAccessSignatureBlobUrl(blob, perm, minutesValid);
        }

        public string GetSharedAccessSignatureBlobUrl(string containerName, string blobName, SharedAccessBlobPermissions perm, int minutesValid)
        {
            ICloudBlob blob = GetBlob(containerName, blobName);
            return GetSharedAccessSignatureBlobUrl(blob, perm, minutesValid);
        }
        #endregion

        /// <summary>
        /// Returns a reference to the blob in storage.
        /// </summary>
        /// <param name="containerName">The name of the container for the blob.</param>
        /// <param name="blobName">The name of the blob.</param>
        /// <returns>A CloudBlob object.</returns>
        public ICloudBlob GetBlob(string containerName, string blobName, bool fetchAttributes = false, BlobType blobType = BlobType.BlockBlob)
        {
            CloudBlobContainer container = GetBlobContainer(containerName);
            ICloudBlob blob = null;
            if (blobType == BlobType.BlockBlob)
            {
                blob = container.GetBlockBlobReference(blobName);
            }
            else
            {
                blob = container.GetPageBlobReference(blobName);
            }
            if (fetchAttributes)
            {
                blob.FetchAttributes();
            }
            return blob;
        }

        public ICloudBlob GetBlobReferenceFromServer(string containerName, string blobName, bool fetchAttributes = false)
        {
            CloudBlobContainer container = GetBlobContainer(containerName);
            ICloudBlob blob = null;

            blob = container.GetBlobReferenceFromServer(blobName);

            if (fetchAttributes)
            {
                blob.FetchAttributes();
            }
            return blob;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="absoluteBlobUri"></param>
        /// <returns></returns>
        public ICloudBlob GetBlob(string absoluteBlobUri, bool fetchAttributes = false)
        {
            ICloudBlob blob = BlobClient.GetBlobReferenceFromServer(new Uri(absoluteBlobUri));

            if (fetchAttributes)
            {
                blob.FetchAttributes();
            }

            return blob;

        }

        public ICloudBlob GetBlobIfModified(ICloudBlob blob)
        {
            ICloudBlob compareBlob = blob.Container.GetBlobReferenceFromServer(blob.Uri.ToString());
            blob.FetchAttributes();
            if (DateTimeOffset.Compare((DateTimeOffset)blob.Properties.LastModified, (DateTimeOffset)compareBlob.Properties.LastModified) < 0)
            {
                return compareBlob;
            }
            return blob;
        }






        public static string GetFileNameFromUrl(string url)
        {
            return url.Substring(url.LastIndexOf("/") + 1, (url.Length - url.LastIndexOf("/") - 1));


        }
        public static ICloudBlob CopyBlob(

           string sourceBlobUri,
           CloudBlobContainer destContainer,
          string destinationBlobName,
             out CopyStatus cs,
          bool waitForCopyToComplete = true,
            AccessCondition sourceAcs = null,
           AccessCondition destinationAcs = null,
           BlobRequestOptions options = null,
            BlobType srcBlobType = BlobType.BlockBlob,
            int maxWaitTimeInSecs = 300,
            bool overwrite = true)
        {
            cs = CopyStatus.Pending;

            // Create appropriate destination blob type to match the source blob
            ICloudBlob destBlob;
            if (srcBlobType == BlobType.BlockBlob)
            {
                destBlob = destContainer.GetBlockBlobReference(destinationBlobName);
            }
            else if (srcBlobType == BlobType.PageBlob)
            {
                destBlob = destContainer.GetPageBlobReference(destinationBlobName);
            }
            else
            {

                destBlob = destContainer.GetBlockBlobReference(destinationBlobName);
            }

            if (options == null)
            {

                options = new BlobRequestOptions()
                {
                    RetryPolicy = RETRY_POLICY

                };

            }

            bool write = true;

            if (destBlob.Exists() && (!overwrite))
            {
                write = false;

            }

            if (write && srcBlobType == BlobType.BlockBlob)
            {
                // copy using src blob as SAS
                CloudBlockBlob db = destBlob as CloudBlockBlob;
                if (db != null)
                {
                    db.StartCopy(new Uri(sourceBlobUri), sourceAcs, destinationAcs, options);

                    if (waitForCopyToComplete)
                    {

                        MonitorCopy(destBlob.Name, destContainer, out cs, waitTimeInSecs: maxWaitTimeInSecs);


                    }

                    return destBlob;
                }
            }

            return null;

        }

        public static ICloudBlob CopyBlob(

          Uri sourceBlobUri,
          CloudBlobContainer destContainer,
         string destinationBlobName,
            out CopyStatus cs,
         bool waitForCopyToComplete = true,
           AccessCondition sourceAcs = null,
          AccessCondition destinationAcs = null,
          BlobRequestOptions options = null,
           BlobType srcBlobType = BlobType.BlockBlob,
           int maxWaitTimeInSecs = 300,
            bool overwrite = true)
        {
            cs = CopyStatus.Pending;

            // Create appropriate destination blob type to match the source blob
            ICloudBlob destBlob;
            if (srcBlobType == BlobType.BlockBlob)
            {
                destBlob = destContainer.GetBlockBlobReference(destinationBlobName);
            }
            else if (srcBlobType == BlobType.PageBlob)
            {
                destBlob = destContainer.GetPageBlobReference(destinationBlobName);
            }
            else
            {

                destBlob = destContainer.GetBlockBlobReference(destinationBlobName);
            }

            bool write = true;

            if (destBlob.Exists() && (!overwrite))
            {
                write = false;

            }

            if (write && srcBlobType == BlobType.BlockBlob)
            {
                CloudBlockBlob db = destBlob as CloudBlockBlob;

                if (db != null)
                {
                    // copy using src blob as SAS
                    db.StartCopy(sourceBlobUri, sourceAcs, destinationAcs, options);

                    if (waitForCopyToComplete)
                    {

                        MonitorCopy(destBlob.Name, destContainer, out cs, waitTimeInSecs: maxWaitTimeInSecs);


                    }

                    return destBlob;
                }

            }
            return null;
        }

        public static ICloudBlob CopyBlob(
             CloudBlobContainer srcContainer,
             ICloudBlob srcBlob,
             CloudBlobContainer destContainer,
             bool waitForCopyToComplete = true,
            int maxWaitTimeInSecs = 300,
            bool overwrite = true)
        {
            CopyStatus cs = CopyStatus.Pending;
            // get the SAS token to use for all blobs
            string blobToken = srcContainer.GetSharedAccessSignature(
                               new SharedAccessBlobPolicy());



            // Create appropriate destination blob type to match the source blob
            ICloudBlob destBlob;
            if (srcBlob.Properties.BlobType == BlobType.BlockBlob)
            {
                destBlob = destContainer.GetBlockBlobReference(srcBlob.Name);
            }
            else
            {
                destBlob = destContainer.GetPageBlobReference(srcBlob.Name);
            }

            bool write = true;
            if (destBlob.Exists() && (!overwrite))
            {
                write = false;

            }

            if (write && srcBlob.Properties.BlobType == BlobType.BlockBlob)
            {
                CloudBlockBlob db = destBlob as CloudBlockBlob;

                if (db != null)
                {
                    // copy using src blob as SAS
                    db.StartCopy(new Uri(srcBlob.Uri.AbsoluteUri + blobToken));

                    if (waitForCopyToComplete)
                    {

                        MonitorCopy(destBlob.Name, destContainer, out cs, waitTimeInSecs: maxWaitTimeInSecs);


                    }

                    return destBlob;
                }

            }

            return null;

        }


        public static void CopyBlobs(
              CloudBlobContainer srcContainer,
              string policyId,
              CloudBlobContainer destContainer,
            bool overwrite = true)
        {
            // get the SAS token to use for all blobs
            string blobToken = srcContainer.GetSharedAccessSignature(
                               new SharedAccessBlobPolicy(), policyId);


            var srcBlobList = srcContainer.ListBlobs(useFlatBlobListing: true);
            foreach (var src in srcBlobList)
            {
                var srcBlob = src as ICloudBlob;

                // Create appropriate destination blob type to match the source blob
                ICloudBlob destBlob;
                if (srcBlob.Properties.BlobType == BlobType.BlockBlob)
                {
                    destBlob = destContainer.GetBlockBlobReference(srcBlob.Name);
                }
                else
                {
                    destBlob = destContainer.GetPageBlobReference(srcBlob.Name);
                }

                bool write = true;
                if (destBlob.Exists() && (!overwrite))
                {
                    write = false;

                }

                if (write && srcBlob.Properties.BlobType == BlobType.BlockBlob)
                {
                    CloudBlockBlob db = destBlob as CloudBlockBlob;

                    if (db != null)
                    {
                        // copy using src blob as SAS
                        db.StartCopy(new Uri(srcBlob.Uri.AbsoluteUri + blobToken));

                    }
                }
            }
        }

        public static void MonitorCopy(CloudBlobContainer destContainer, int waitTimeInSecs = 120)
        {
            bool pendingCopy = true;
            int threadWaitTimeInSecs = 60;
            DateTime waitTill = DateTime.UtcNow.AddSeconds(waitTimeInSecs);
            while (pendingCopy && (waitTill >= DateTime.UtcNow))
            {
                pendingCopy = false;
                var destBlobList = destContainer.ListBlobs(
                    useFlatBlobListing: true, blobListingDetails: BlobListingDetails.Copy);

                foreach (var dest in destBlobList)
                {
                    var destBlob = dest as ICloudBlob;
                    // destBlob = destContainer.GetBlobReferenceFromServer(destBlob.Name);

                    if (destBlob.CopyState.Status == CopyStatus.Aborted ||
                        destBlob.CopyState.Status == CopyStatus.Failed)
                    {
                        // Log the copy status description for diagnostics 
                        // and restart copy
                        // Log(destBlob.CopyState);
                        //pendingCopy = true;
                        //destBlob.StartCopyFromBlob(destBlob.CopyState.Source);

                        break;
                    }
                    else if (destBlob.CopyState.Status == CopyStatus.Pending)
                    {
                        // We need to continue waiting for this pending copy
                        // However, let us log copy state for diagnostics
                        //  Log(destBlob.CopyState);
                        //For now, don't retry
                        pendingCopy = true;
                    }
                    // else we completed this pending copy
                }

                System.Threading.Thread.Sleep(TimeSpan.FromSeconds(threadWaitTimeInSecs));
            };
        }

        public static void MonitorCopy(string destBlobName, CloudBlobContainer destContainer, out CopyStatus status, int waitTimeInSecs = 60)
        {
            status = CopyStatus.Pending;
            bool pendingCopy = true;
            int threadWaitTimeInSecs = 60;
            DateTime waitTill = DateTime.UtcNow.AddSeconds(waitTimeInSecs);
            while (pendingCopy && (waitTill >= DateTime.UtcNow))
            {
                pendingCopy = false;

                var blobsList = destContainer.ListBlobs(useFlatBlobListing: true, blobListingDetails: BlobListingDetails.Copy);

                foreach (var blob in blobsList)
                {
                    var destBlob = blob as ICloudBlob;
                    //   destBlob = destContainer.GetBlobReferenceFromServer(destBlob.Name);

                    if (destBlob.Name == destBlobName)
                    {
                        status = destBlob.CopyState.Status;

                        if (status == CopyStatus.Success)
                        {
                            break;

                        }
                        else
                            if (status == CopyStatus.Aborted ||
                                status == CopyStatus.Failed)
                        {
                            // Log the copy status description for diagnostics 
                            // and restart copy
                            // Log(destBlob.CopyState);
                            // pendingCopy = true;
                            // destBlob.StartCopyFromBlob(destBlob.CopyState.Source);

                            //For now, don't retry
                            // pendingCopy = false;
                            break;
                            // System.Threading.Thread.Sleep(TimeSpan.FromSeconds(waitTimeInSecs));
                        }
                        else if (status == CopyStatus.Pending)
                        {
                            // We need to continue waiting for this pending copy
                            // However, let us log copy state for diagnostics
                            //  Log(destBlob.CopyState);

                            pendingCopy = true;

                            System.Threading.Thread.Sleep(TimeSpan.FromSeconds(threadWaitTimeInSecs));
                        }
                    }
                }




            }

        }
        #endregion

        #endregion



        #region static methods

        /// <summary>
        /// Gets the Storage Helper object based on config data.
        /// It reads the following from the configuration file
        /// </summary>
        /// <returns></returns>
        public static WABlobStorageHelper GetStorageHelper()
        {
            return new WABlobStorageHelper();
        }

        public static WABlobStorageHelper GetStorageHelper(string accountName, string accountKey, bool useHttps)
        {
            return new WABlobStorageHelper(accountName, accountKey, useHttps);
        }

        public static WABlobStorageHelper GetStorageHelper(CloudStorageAccount account)
        {
            return new WABlobStorageHelper(account);
        }

        public static void GetBlobContentsAsFile(string containerName, string blobName, string fileName, WABlobStorageHelper helper)
        {
            helper.GetBlobContentsAsFile(containerName, blobName, fileName);
        }

        public static ICloudBlob GetBlob(string containerName, string blobName, WABlobStorageHelper storageHelper)
        {
            return storageHelper.GetBlob(containerName, blobName);
        }



        public static string GetContentTypeFromExtension(string extension) //add more content types (text, zip, video, etc.)
        {
            string cType = "application/unknown";
            try
            {

                RegistryKey key = Registry.ClassesRoot.OpenSubKey(extension, false);
                if ((key != null) && (key.GetValue("Content Type") != null))
                {
                    return key.GetValue("Content Type").ToString();
                }

                string silverlightSpecial;
                if (((silverlightSpecial = extension) != null) && (silverlightSpecial == ".xap"))
                {
                    cType = "application/x-silverlight-app";
                }
                return cType;
            }
            catch (SecurityException)
            {
            }

            if (extension == null)
            {
                return cType;
            }
            string imageFiles = extension;
            if (!(imageFiles == ".docx") && !(imageFiles == ".doc"))
            {
                if ((imageFiles != ".jpg") && (imageFiles != ".jpeg"))
                {
                    if (imageFiles != ".png")
                    {
                        return cType;
                    }
                    return "image/png";
                }
            }
            else
            {
                return "application/msword";
            }
            return "image/jpeg";
        }

        public static MemoryStream GZipCompressedBlobContents(string fileName)
        {
            var compressedData = new MemoryStream();

            using (var gzipStream = new GZipStream(compressedData,
                CompressionMode.Compress))
            {
                byte[] fileBytes = File.ReadAllBytes(fileName);
                gzipStream.Write(fileBytes, 0,
                    fileBytes.Length);
            }

            return compressedData;
        }

        public static void UploadGZipCompressedFileToBlob(ICloudBlob blob, string filename, bool overwrite, int timeoutInMinutes = 5)
        {
            AccessCondition acs;
            BlobRequestOptions options = CreateOverwriteOption(blob, overwrite, out acs, timeoutInMinutes);
            using (MemoryStream compressedData = GZipCompressedBlobContents(filename))
            {
                blob.UploadFromStream(compressedData, acs, options);
            }
        }



        public static string ConvertToStorageAccountConnectionString(string storageAccountName, string storageAccountKey, bool useHttps = true)
        {
            return string.Format(STORAGE_ACC_FORMAT, (useHttps) ? "https" : "http", storageAccountName, storageAccountKey);

        }

        #endregion

        #region private helper methods

        private void Init(string accountName, string accountKey, bool useHttps)
        {
            StorageCredentials credentials = new StorageCredentials(accountName, accountKey);
            this._cloudStorageAccount = new CloudStorageAccount(credentials, useHttps);
        }

        private void InitFromRoleConfigSetting(string configString)
        {

            try
            {

                this._cloudStorageAccount = CloudStorageAccount.Parse(
     ConfigUtils.GetStringConfigurationValue(configString));

                if (this._cloudStorageAccount == null)
                {

                    this._cloudStorageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings[configString]);

                }

            }
            catch (Exception)
            {

                this._cloudStorageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings[configString]);

            }
        }
        private void InitFromCustomEndpoints(string accountName, string accountKey)
        {

            Uri blobStorageEndPoint = new Uri(ConfigurationManager.AppSettings["BlobStorageCustomEndpoint"]);
            Uri queueStorageEndpoint = new Uri(ConfigurationManager.AppSettings["QueueStorageCustomEndpoint"]);
            Uri tableStorageEndpoint = new Uri(ConfigurationManager.AppSettings["TableStorageCustomEndpoint"]);
            StorageCredentials credentials = new StorageCredentials(accountName, accountKey);
            this._cloudStorageAccount = new CloudStorageAccount(credentials, blobStorageEndPoint, queueStorageEndpoint, tableStorageEndpoint, null);
        }

        /*   private void InitFromRoleConfigSetting(string configString)
           {

               try
               {
                   if (RoleEnvironment.IsAvailable)
                   {
                       this._cloudStorageAccount = CloudStorageAccount.Parse(
             Microsoft.WindowsAzure.CloudConfigurationManager.GetSetting(configString));


                   }
                   else
                   {

                       this._cloudStorageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings[configString]);
                   }


               }
               catch (Exception)
               {

                   this._cloudStorageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings[configString]);

               }
           }
           */
        #endregion

        #region private static helper methods

        private static BlobRequestOptions CreateOverwriteOption(ICloudBlob blob, bool overwrite, out AccessCondition acs, int timeoutInMinutes = 5)
        {
            acs = AccessCondition.GenerateEmptyCondition();

            BlobRequestOptions options = new BlobRequestOptions();
            options.ServerTimeout = TimeSpan.FromMinutes(timeoutInMinutes);
            if (!overwrite)
                acs = AccessCondition.GenerateIfNoneMatchCondition(blob.Properties.ETag);
            return options;
        }

        private static BlobRequestOptions CreateIfModifiedOption(ICloudBlob blob, out AccessCondition acs, int timeoutInMinutes = 5)
        {
            acs = AccessCondition.GenerateIfModifiedSinceCondition((DateTimeOffset)blob.Properties.LastModified);
            return new BlobRequestOptions
            {

                ServerTimeout = TimeSpan.FromMinutes(timeoutInMinutes),
                RetryPolicy = RETRY_POLICY

            };
        }

        private static BlobRequestOptions CreateIfNotModifiedOption(ICloudBlob blob, out AccessCondition acs, int timeoutInMinutes = 5)
        {
            acs = AccessCondition.GenerateIfNotModifiedSinceCondition((DateTimeOffset)blob.Properties.LastModified);
            return new BlobRequestOptions
            {
                ServerTimeout = TimeSpan.FromMinutes(timeoutInMinutes),
                RetryPolicy = RETRY_POLICY
            };
        }






        #endregion


        private static AccessCondition GetIfNotModifiedSinceTimeAccessCondition(ICloudBlob blob)
        {

            return new AccessCondition()
            {
                IfNotModifiedSinceTime = blob.Properties.LastModified

            };

        }

        private static AccessCondition GetIfModifiedSinceTimeAccessCondition(ICloudBlob blob)
        {

            return new AccessCondition()
            {
                IfModifiedSinceTime = blob.Properties.LastModified

            };

        }

        private static AccessCondition CreateOverwriteAccessCondition(ICloudBlob blob)
        {

            return new AccessCondition()
            {
                IfNoneMatchETag = blob.Properties.ETag

            };

        }

        public long GetFileSize(string fileUrl)
        {
            try
            {


                ICloudBlob cb = GetBlob(fileUrl, true);

                return cb.Properties.Length;


            }
            catch (Exception)
            {


            }

            return 1;

        }

        public void SetCacheControl(string containerName, string blobName, string cacheControl = "public, max-age=36000, must-revalidate", int timeoutInMinutes = 5)
        {
            ICloudBlob blob = this.GetBlob(containerName, blobName);

            blob.SetCacheControl(cacheControl);
        }

        public void SetContentType(string containerName, string blobName, string contentType = "application/json", int timeoutInMinutes = 5)
        {
            ICloudBlob blob = this.GetBlob(containerName, blobName);

            blob.SetContentType(contentType);
        }

        public void SetCacheControlContentType(string containerName, string blobName, string cacheControl = "public, max-age=36000, must-revalidate", string contentType = "application/json", int timeoutInMinutes = 5)
        {
            ICloudBlob blob = this.GetBlob(containerName, blobName);

            blob.SetCacheControlContentType(cacheControl, contentType);
        }

        /// <summary>
        ///   Iterates through each blob in the specified container and adds the
        ///   Cache-Control and ContentType headers
        /// </summary>
        public static void EnsureStaticFileHeaders(
            CloudBlobContainer container,
            int cacheControlMaxAgeSeconds)
        {
            string cacheControlHeader = "public, max-age=" + cacheControlMaxAgeSeconds.ToString();

            var blobInfos = container.ListBlobs(null, true, BlobListingDetails.All,
                new BlobRequestOptions() { RetryPolicy = RETRY_POLICY });
            Parallel.ForEach(blobInfos, async (blobInfo) =>
            {
                ICloudBlob bl = blobInfo as ICloudBlob;

                // get the blob properties and set headers if necessary
                ICloudBlob blob = await container.GetBlobReferenceFromServerAsync(bl.Name);
                blob.FetchAttributes();
                var properties = blob.Properties;

                bool wasModified = false;

                // see if a content type is defined for the extension
                string extension = Path.GetExtension(blobInfo.Uri.LocalPath);
                string contentType = GetContentType(extension);
                if (String.IsNullOrEmpty(contentType))
                {
                    Trace.TraceWarning("Content type not found for extension:" + extension);
                }
                else
                {
                    if (properties.ContentType != contentType)
                    {
                        properties.ContentType = contentType;
                        wasModified = true;
                    }
                }

                if (properties.CacheControl != cacheControlHeader)
                {
                    properties.CacheControl = cacheControlHeader;
                    wasModified = true;
                }

                if (wasModified)
                {
                    blob.SetProperties();
                }
            });
        }

        /// <summary>
        ///   Finds all js and css files in a container and creates a gzip compressed
        ///   copy of the file with ".gzip" appended to the existing blob name
        /// </summary>
        public static void EnsureGzipFiles(
            CloudBlobContainer container,
            int cacheControlMaxAgeSeconds)
        {
            string cacheControlHeader = "public, max-age=" + cacheControlMaxAgeSeconds.ToString();

            var blobInfos = container.ListBlobs(null, true, BlobListingDetails.All,
                  new BlobRequestOptions() { RetryPolicy = RETRY_POLICY });
            Parallel.ForEach(blobInfos, async (blobInfo) =>
            {
                ICloudBlob bl = blobInfo as ICloudBlob;

                string blobUrl = blobInfo.Uri.ToString();
                ICloudBlob blob = await container.GetBlobReferenceFromServerAsync(blobUrl);

                // only create gzip copies for css and js files
                string extension = Path.GetExtension(blobInfo.Uri.LocalPath);
                if (extension != ".css" && extension != ".js")
                    return;

                // see if the gzip version already exists
                string gzipUrl = blobUrl + ".gzip";


                ICloudBlob gzipBlob = container.GetBlockBlobReference(gzipUrl);
                if (gzipBlob.Exists())
                    return;

                // create a gzip version of the file
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    // push the original blob into the gzip stream
                    using (GZipStream gzipStream = new GZipStream(
                        memoryStream, CompressionMode.Compress))
                    using (Stream blobStream = blob.OpenRead())
                    {
                        blobStream.CopyTo(gzipStream);
                    }

                    // the gzipStream MUST be closed before its safe to read from the memory stream
                    //byte[] compressedBytes = memoryStream.ToArray();

                    // upload the compressed bytes to the new blob
                    await gzipBlob.UploadFromStreamAsync(memoryStream);

                    // set the blob headers
                    gzipBlob.Properties.CacheControl = cacheControlHeader;
                    gzipBlob.Properties.ContentType = GetContentType(extension);
                    gzipBlob.Properties.ContentEncoding = "gzip";
                    gzipBlob.SetProperties();
                }
            });
        }


        public static void EnsureGzipFilesAll(string prefix,
         CloudBlobContainer container,
         int cacheControlMaxAgeSeconds)
        {
            string cacheControlHeader = "public, max-age=" + cacheControlMaxAgeSeconds.ToString();

            var blobInfos = container.ListBlobs(prefix, true, BlobListingDetails.All,
                  new BlobRequestOptions() { RetryPolicy = RETRY_POLICY });
            Parallel.ForEach(blobInfos, async (blobInfo) =>
            {
                ICloudBlob bl = blobInfo as ICloudBlob;

                string blobUrl = blobInfo.Uri.ToString();
                ICloudBlob blob = await container.GetBlobReferenceFromServerAsync(blobUrl);


                string extension = Path.GetExtension(blobInfo.Uri.LocalPath);
                if (extension.Equals(".gzip", StringComparison.InvariantCultureIgnoreCase) || extension.Equals(".gz", StringComparison.InvariantCultureIgnoreCase))
                    return;

                // see if the gzip version already exists
                string gzipUrl = blobUrl + ".gzip";


                ICloudBlob gzipBlob = container.GetBlockBlobReference(gzipUrl);
                if (gzipBlob.Exists())
                    return;

                // create a gzip version of the file
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    // push the original blob into the gzip stream
                    using (GZipStream gzipStream = new GZipStream(
                        memoryStream, CompressionMode.Compress))
                    using (Stream blobStream = blob.OpenRead())
                    {
                        blobStream.CopyTo(gzipStream);
                    }

                    // the gzipStream MUST be closed before its safe to read from the memory stream
                    //byte[] compressedBytes = memoryStream.ToArray();

                    // upload the compressed bytes to the new blob
                    await gzipBlob.UploadFromStreamAsync(memoryStream);

                    // set the blob headers
                    gzipBlob.Properties.CacheControl = cacheControlHeader;
                    gzipBlob.Properties.ContentType = GetContentType(extension);
                    gzipBlob.Properties.ContentEncoding = "gzip";
                    gzipBlob.SetProperties();
                }
            });
        }

        /// <summary>
        /// Uploads the file named blobName on the local drive to the blob storage.
        /// </summary>
        /// <param name="containerName"></param>
        /// <param name="blobName"></param>
        /// <param name="cacheControl"></param>
        /// <param name="contentType"></param>
        /// <returns></returns>
        public async Task<string> GZipBlobAsync(string containerName, string blobName, string filePath, string cacheControl = "public, max-age=144000, must-revalidate", string contentType = "text/xml")
        {
            CloudBlobContainer container = GetBlobContainer(containerName);
            ICloudBlob gzipBlob = container.GetBlockBlobReference(blobName);
            string blobUri = null;
            using (MemoryStream memoryStream = new MemoryStream())
            {

                // push the original blob into the gzip stream
                using (GZipStream gzipStream = new GZipStream(
                    memoryStream, CompressionMode.Compress))
                using (Stream blobStream = File.OpenRead(filePath))
                {
                    blobStream.CopyTo(gzipStream);
                }

                // the gzipStream MUST be closed before its safe to read from the memory stream
                byte[] compressedBytes = memoryStream.ToArray();

                // upload the compressed bytes to the new blob
                await gzipBlob.UploadFromByteArrayAsync(compressedBytes, 0, compressedBytes.Length);

                // set the blob headers
                gzipBlob.Properties.CacheControl = cacheControl;
                string extension = Path.GetExtension(blobName);
                gzipBlob.Properties.ContentType = GetContentType(extension);
                gzipBlob.Properties.ContentEncoding = "gzip";
                gzipBlob.SetProperties();
                blobUri = gzipBlob.Uri.AbsoluteUri;
            }


            return blobUri;
        }


        public async Task<string> GZipBlobAsyncFromString(string containerName, string blobName, string content, string cacheControl = "public, max-age=144000, must-revalidate", string contentType = "text/xml")
        {
            CloudBlobContainer container = GetBlobContainer(containerName);
            ICloudBlob gzipBlob = container.GetBlockBlobReference(blobName);
            string blobUri = null;


            using (MemoryStream memoryStream = new MemoryStream())
            {

                // push the original blob into the gzip stream
                using (GZipStream gzipStream = new GZipStream(
                    memoryStream, CompressionMode.Compress))
                using (Stream blobStream = new MemoryStream())
                {
                    using (StreamWriter sw = new StreamWriter(blobStream))
                    {
                        sw.Write(content);
                        sw.Flush();
                        blobStream.Position = 0;
                        blobStream.CopyTo(gzipStream);

                    }
                }

                // the gzipStream MUST be closed before its safe to read from the memory stream
                byte[] compressedBytes = memoryStream.ToArray();

                // upload the compressed bytes to the new blob
                await gzipBlob.UploadFromByteArrayAsync(compressedBytes, 0, compressedBytes.Length);

                // set the blob headers
                gzipBlob.Properties.CacheControl = cacheControl;
                string extension = Path.GetExtension(blobName);
                gzipBlob.Properties.ContentType = GetContentType(extension);
                gzipBlob.Properties.ContentEncoding = "gzip";
                gzipBlob.SetProperties();
                blobUri = gzipBlob.Uri.AbsoluteUri;
            }


            return blobUri;
        }

        public async Task<string> GZipDecompressBlobAsync(string containerName, string blobName)
        {
            ICloudBlob blob = GetBlobReferenceFromServer(containerName, blobName);

            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (Stream gzip = new GZipStream(blob.OpenRead(), CompressionMode.Decompress))
                {


                    gzip.CopyToStream(memoryStream);
                }
                //Notes:Set the position to zero because the memory stream was returning empty string
                memoryStream.Position = 0;
                StreamReader reader = new StreamReader(memoryStream);
                return await reader.ReadToEndAsync();
            }
        }
        /// <summary>
        ///   Gets the content type for the specified extension
        /// </summary>
        public static string GetContentType(string extension)
        {
            switch (extension.ToLowerInvariant())
            {
                case ".jpg":
                case ".jpeg":
                    return "image/jpeg";
                case ".png":
                    return "image/png";
                case ".gif":
                    return "image/gif";
                case ".ico":
                    return "image/x-icon";
                case ".css":
                    return "text/css";
                case ".js":
                    return "text/javascript";
                case ".svg":
                    return "image/svg+xml";
                case ".mp3":
                    return "audio/mpeg";
                case ".mp4":
                    return "video/mp4";
                case ".eot":
                    return "application/vnd.ms-fontobject";
                case ".woff":
                    return "application/x-woff";
                case ".ttf":
                    return "font/ttf";
                case ".otf":
                    return "font/otf";
                case ".xml":
                    return "text/xml";
                case ".json":
                    return "application/json";
            }

            return null;
        }

        public void DeleteBlobsIfOld(string containerName, string prefix, TimeSpan limit)
        {
            try
            {
                var list = ListBlobsSegmented(containerName, prefix, 1000, new BlobRequestOptions());

                if (list != null && list.Count() > 0)
                {

                    Parallel.ForEach(list, async bl =>
                    {
                        try
                        {
                            {
                                ICloudBlob blob = bl as ICloudBlob;

                                await blob.FetchAttributesAsync();

                                if (DateTimeOffset.UtcNow - blob.Properties.LastModified.Value >= limit)
                                {
                                    //delete the blob
                                    await blob.DeleteIfExistsAsync();
                                }

                            }

                        }
                        catch (Exception)
                        {
                            //ignore for now. Not a critical function
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public static class BlobExtensions
    {

        //Set the Cache-Control header on the blob to specify your desired refresh interval (secs).
        //   blob.SetCacheControl("public, max-age=31536000, must-revalidate");

        //A convenience method to set the Cache-Control header.
        public static void SetCacheControl(this ICloudBlob blob, string value)
        {
            blob.Properties.CacheControl = value;
            blob.SetProperties();
        }


        public static void SetContentType(this ICloudBlob blob, string value)
        {
            blob.Properties.ContentType = value;
            blob.SetProperties();
        }

        public static void SetCacheControlContentType(this ICloudBlob blob, string cacheControl, string contentType)
        {
            blob.Properties.ContentType = contentType;
            blob.Properties.CacheControl = cacheControl;
            blob.SetProperties();
        }


    }
}
