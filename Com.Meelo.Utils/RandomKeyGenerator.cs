﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace Com.Meelo.Utils
{
    public enum RandomStringGenerationType
    {
        SIMPLE_STRING,
        STRING_INTEGER_COMBINATION,
        STRING_GUID_COMBINATION,
        STRING_TICKS_COMBINATION


    }
    public static class RandomKeyGenerator
    {
        private static readonly Random Random = new Random();
        private static readonly Random R = new Random();
        public const int MAX_TEMPLATES_PER_GROUP = 10000;
        private static readonly double[] FudgeFactors = new double[] { 0.5d, 0.7d, 0.85d, 0.9d, 0.95d, 1.0d, 1.1d, 1.15d, 1.2d, 1.25d, 1.3d, 1.4d, 1.5d };

        public static long GetRandomKey()
        {
            var rng = RandomNumberGenerator.Create();
            //bytes to hold number
            var bytes = new byte[8];
            //randomize
            rng.GetNonZeroBytes(bytes);
            //Convert
            return BitConverter.ToInt64(bytes, 0);

        }

    
        ///       <summary>
        /// Generates a random string with the given length
        /// </summary>
        /// <param name="size">Size of the string</param>
        /// <param name="lowerCase">If true, generate lowercase string</param>
        /// <returns>Random string</returns>
        public static string GetRandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            //  Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * R.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }

        public static int GetRandomNumber(int min = 49152, int max = 65535)
        {

            return Random.Next(min, max);

        }

        public static double GetRandomDouble()
        {

            return Random.NextDouble();

        }
        public static string GenerateRandomString(RandomStringGenerationType ranType, int maxLength = 15, bool lowerCase = true)
        {
            if (maxLength > 0)
            {
                switch (ranType)
                {
                    case RandomStringGenerationType.SIMPLE_STRING:

                        return GetRandomString(maxLength, true);
                    case RandomStringGenerationType.STRING_GUID_COMBINATION:

                        string rS = string.Format("{0}{1}{2}", GetRandomString(2, true), System.Guid.NewGuid().ToString("N"), GetRandomString(maxLength, true));
                        return rS.Substring(0, maxLength);
                    case RandomStringGenerationType.STRING_INTEGER_COMBINATION:

                        string pt = string.Format("{0}{1}{2}", GetRandomString(2, true), GetRandomNumber(), GetRandomString(maxLength, true));

                        return pt.Substring(0, maxLength);
                    case RandomStringGenerationType.STRING_TICKS_COMBINATION:

                        string st = string.Format("{0}{1}{2}", GetRandomString(2, true), DateTime.UtcNow.Ticks, GetRandomString(maxLength, true));

                        return st.Substring(0, maxLength);

                    default:
                        return GetRandomString(maxLength, true);
                };

            }

            return "";
        }

        public static long GetLongTicksForPK()
        {

            return DateTime.MaxValue.Ticks - DateTime.UtcNow.Ticks;

        }

    }


}
