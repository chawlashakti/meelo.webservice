﻿--db_accessadmin 
--db_backupoperator 
--db_datareader 
--db_datawriter 
--db_ddladmin 
--db_denydatareader 
--db_denydatawriter 
--db_owner 
--db_securityadmin 

use [master]
CREATE LOGIN readonlylogin WITH password='12Meel012$readonly';
use [meelodb]
CREATE USER readonlyuser FROM LOGIN readonlylogin;
EXEC sp_addrolemember 'db_datareader', 'readonlyuser';

use [master]
CREATE LOGIN writelogin WITH password='12Meel012$write';
use [meelodb]
CREATE USER writeuser FROM LOGIN writelogin;
EXEC sp_addrolemember 'db_datawriter', 'writeuser';