﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
//using Dajbych.TransientFaultHandling;
using System.Threading.Tasks;

namespace Com.Meelo.Utils.Sql
{
    public class SqlRetryConnectionManager
    {




        public SqlRetryConnectionManager()
        {



        }




        public async Task<SqlConnection> OpenConnectionAsync(string connectionString)
        {

            SqlConnection con = null;

            try
            {




                SqlConnection connection = new SqlConnection(connectionString);

                await connection.OpenWithRetryAsync();

                con = connection;



                return con;

            }

            catch (Exception ex)
            {

                throw new Exception("Error connecting to " + connectionString, ex);

            }

        }


        public SqlConnection OpenConnection(string connectionString)
        {

            SqlConnection con = null;

            try
            {




                SqlConnection connection = new SqlConnection(connectionString);

                connection.OpenWithRetry();

                con = connection;



                return con;

            }

            catch (Exception ex)
            {

                throw new Exception("Error connecting to " + connectionString, ex);

            }

        }
    }
}
