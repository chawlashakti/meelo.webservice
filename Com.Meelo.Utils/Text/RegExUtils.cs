﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Collections;

namespace Com.Meelo.Utils.Text
{
    public sealed class RegExUtils
    {
        private const RegexOptions DEFAULT_REGEX_OPTIONS = RegexOptions.IgnorePatternWhitespace |
            RegexOptions.Multiline;
        private RegExUtils()
        {

        }

        #region Macros Replacement RegEx Functions

        public static Match[] FindSubstrings(string source, string matchPattern,
            bool findAllUnique)
        {
            SortedList uniqueMatches = new SortedList();
            Match[] retArray = null;

            Regex RE = new Regex(matchPattern);//, RegexOptions.Multiline);
            MatchCollection theMatches = RE.Matches(source);

            if (findAllUnique)
            {
                for (int counter = 0; counter < theMatches.Count; counter++)
                {
                    if (!uniqueMatches.ContainsKey(theMatches[counter].Value))
                    {
                        uniqueMatches.Add(theMatches[counter].Value, theMatches[counter]);
                    }
                }

                retArray = new Match[uniqueMatches.Count];
                uniqueMatches.Values.CopyTo(retArray, 0);
            }
            else
            {
                retArray = new Match[theMatches.Count];
                theMatches.CopyTo(retArray, 0);
            }

            return (retArray);
        }

        public static string Replace(string source, char matchPattern, string replaceStr)
        {
            return (Replace(source, matchPattern.ToString(), replaceStr, -1, 0,
                DEFAULT_REGEX_OPTIONS));
        }

        public static string Replace(string source, char matchPattern, string replaceStr
            , RegexOptions options)
        {
            return (Replace(source, matchPattern.ToString(), replaceStr, -1, 0,
                DEFAULT_REGEX_OPTIONS));
        }


        public static string Replace(string source, string matchPattern, string replaceStr)
        {
            return (Replace(source, matchPattern, replaceStr, -1, 0, DEFAULT_REGEX_OPTIONS));
        }

        public static string Replace(string source, string matchPattern, string replaceStr,
            RegexOptions options)
        {
            return (Replace(source, matchPattern, replaceStr, -1, 0, options));
        }


        public static string Replace(string source, char matchPattern, string replaceStr,
            int count)
        {
            return (Replace(source.ToString(), matchPattern.ToString(), replaceStr,
                count, 0, DEFAULT_REGEX_OPTIONS));
        }

        public static string Replace(string source, char matchPattern, string replaceStr,
            int count, int startPos)
        {
            return (Replace(source.ToString(), matchPattern.ToString(), replaceStr,
                count, startPos, DEFAULT_REGEX_OPTIONS));
        }

        public static string Replace(string source, char matchPattern, string replaceStr,
            int count, int startPos, RegexOptions options)
        {
            return (Replace(source.ToString(), matchPattern.ToString(), replaceStr,
                count, startPos, options));
        }



        public string Replace(string source, string matchPattern, string replaceStr,
            int count)
        {
            return (Replace(source, matchPattern, replaceStr, count, 0,
                DEFAULT_REGEX_OPTIONS));
        }

        public string Replace(string source, string matchPattern, string replaceStr,
            int count, int startPos)
        {
            return (Replace(source, matchPattern, replaceStr, count, startPos,
                DEFAULT_REGEX_OPTIONS));
        }

        public static string Replace(string source, string matchPattern, string replaceStr,
            int count, int startPos, RegexOptions options)
        {
            if (replaceStr == null)
                return source;
            Regex RE = new Regex(matchPattern, options);
            string newString = RE.Replace(source, replaceStr, count, startPos);

            return (newString);
        }


        #endregion
    }
}
