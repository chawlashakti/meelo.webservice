﻿using Com.Meelo.CoreLib;
using Com.Meelo.CoreLib.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.Utils
{
    public class Mapper
    {
        public void Map(DataTable dt, List<CoreLib.Workspace> WorkspaceList)
        {
            foreach (DataRow row in dt.Rows)
            {
                CoreLib.Workspace Workspace = new CoreLib.Workspace();
                Workspace.Id = Convert.ToInt64(row["Id"]);
                Workspace.OrgId = Convert.ToInt64(row["OrgId"]);
                Workspace.Name = Convert.ToString(row["Name"]);
                Workspace.Description = Convert.ToString(row["Description"]);
                Workspace.FriendlyName = Convert.ToString(row["FriendlyName"]);
                Workspace.IdmResourceIdentity = Convert.ToString(row["IdmResourceIdentity"]);
                Workspace.Building = new CoreLib.Building
                {
                    Id = Convert.ToInt64(row["BuildingId"]),
                    BuildingName = Convert.ToString(row.GetValue("Building"))
                };
                if (row["Floor"] != System.DBNull.Value)
                    Workspace.Floor = new CoreLib.Floor { Name = Convert.ToString(row["Floor"]) };
                Workspace.Address = new CoreLib.Address
                {
                    Country = Convert.ToString(row["Country"]),
                    State = Convert.ToString(row["State"]),
                    City = Convert.ToString(row["City"]),
                    PostalCode = Convert.ToString(row["PostalCode"]),
                    GpsLocation = new CoreLib.GpsLocation { Location = Convert.ToString(row["LocationString"]), Lat = Convert.ToDecimal(row["Lat"]), Long = Convert.ToDecimal(row["Long"]) },
                    Floor = Convert.ToString(row["Floor"])
                };
                Workspace.WebLink = new Uri(Utils.UriExtensions.Isvalid(Convert.ToString(row["WebLink"]), ResourceHelper.GetResources("DefaultUrl")));
                Workspace.Photos = new List<CoreLib.Photo> {
                    new CoreLib.Photo{
                         WebLink= new Uri(Utils.UriExtensions.Isvalid(Convert.ToString(row["PhotoLinks"]),  ResourceHelper.GetResources("DefaultUrl")))
                    }
                };
                Workspace.PhotoLinks = Convert.ToString(row["PhotoLinks"]);
                Workspace.Ratings = Convert.ToInt32(row["Ratings"]);
                if (row["Created"] != System.DBNull.Value)
                    Workspace.Created = Convert.ToDateTime(row["Created"]);
                if (row["LastUpdated"] != System.DBNull.Value)
                    Workspace.LastUpdated = Convert.ToDateTime(row["LastUpdated"]);
                if (row["NextReservationStartTime"] != System.DBNull.Value)
                    Workspace.NextReservationStartTime = Convert.ToDateTime(row["NextReservationStartTime"]);
                if (row["NextreservationEndTime"] != System.DBNull.Value)
                    Workspace.NextReservationEndTime = Convert.ToDateTime(row["NextreservationEndTime"]);
                if (row["LimitedAccessRoles"] != System.DBNull.Value)
                    Workspace.LimitedAccessRoles = new List<CoreLib.RoleType> { (CoreLib.RoleType)Enum.Parse(typeof(CoreLib.RoleType), (Convert.ToString(row["LimitedAccessRoles"]))) };
                //Workspace.LimitedAccessUsers = new List<User>{ new User
                //   {
                //    Id = Convert.ToInt64(row["LimitedAccessUsers"])
                //   }
                //};
                Workspace.Tags = new Dictionary<string, string> { { Convert.ToString(row["Tags"]), Convert.ToString(row["Tags"]) } };
                Workspace.Maps = new List<Uri> { new Uri(Utils.UriExtensions.Isvalid(Convert.ToString(row["MapsUri"]), ResourceHelper.GetResources("DefaultUrl"))) };
                //Workspace.IconUri = new List<Capabilites> { new Capabilites {IconUri = Resource.DefaultUrl } };
                Workspace.WorkspaceType = (CoreLib.WorkspaceType)Enum.Parse(typeof(CoreLib.WorkspaceType), (Convert.ToString(row["WorkspaceType"])));
                Workspace.OccupancyStatus = (CoreLib.WorkspaceStatusType)Enum.Parse(typeof(CoreLib.WorkspaceStatusType), (Convert.ToString(row["OccupancyStatus"])));
                Workspace.Capacity = Convert.ToInt32(row["Capacity"]);
                if (row["LastRenovated"] != System.DBNull.Value)
                    Workspace.LastRenovated = Convert.ToDateTime(row["LastRenovated"]);
                Workspace.CommunicationInfo = new CoreLib.CommunicationInfo
                {
                    PhoneNumber = Convert.ToString(row["CommunicationInfo_PhoneNumber"]),
                    TelephonyDetails = Convert.ToString(row["CommunicationInfo_TelephonyDetails"]),
                    AccessDescription = Convert.ToString(row["CommunicationInfo_AccessDescription"]),
                    OtherInfo = new Dictionary<string, string> { { Convert.ToString(row["CommunicationInfo_Email"]),
                            Convert.ToString(row["CommunicationInfo_Email"]) } }
                };
                Workspace.IsAutoCheckinEnabled = Convert.ToBoolean(row["IsAutoCheckinEnabled"]);
                Workspace.IsAutoCheckoutEnabled = Convert.ToBoolean(row["IsAutoCheckoutEnabled"]);
                Workspace.AccessType = (CoreLib.WorkspaceAccessType)Enum.Parse(typeof(CoreLib.WorkspaceAccessType), (Convert.ToString(row["AccessType"])));
                Workspace.MaintenanceOwner = new CoreLib.User { Id = Convert.ToInt64(row["MaintenanceOwner"]) };
                Workspace.AreaInSqFt = Convert.ToDecimal(row["AreaInSqFt"]);
                Workspace.DistanceFromFloorOrigin = new Point();
                Workspace.HasControllableDoor = Convert.ToBoolean(row["HasControllableDoor"]);
                Workspace.BeaconUid = Convert.ToString(row["BeaconUid"]);
                Workspace.IsBlocked = Convert.ToBoolean(row["IsBlocked"]);
                Workspace.IsHidden = Convert.ToBoolean(row["IsHidden"]);
                Workspace.ObjectId = Convert.ToString(row["ObjectId"]);
                WorkspaceList.Add(Workspace);
            }
        }
        public void Map(DataTable dt, List<BuildingWorkspacesViewModel> BuildingList)
        {
            foreach (DataRow row in dt.Rows)
            {
                BuildingWorkspacesViewModel WorkspaceList = new BuildingWorkspacesViewModel();
                WorkspaceList.RowId = Convert.ToInt32(row["RowId"]);
                WorkspaceList.RatingCount = Convert.ToInt32(row["RatingCount"]);
                WorkspaceList.WorkspaceCount = Convert.ToInt32(row["WorkspaceCount"]);
                WorkspaceList.Distance = Convert.ToDecimal(row["Distance"]);
                WorkspaceList.BuildingId = Convert.ToString(row["BuildingId"]);
                WorkspaceList.OrgId = Convert.ToInt64(row["OrgId"]);
                WorkspaceList.Street = Convert.ToString(row["Street"]);
                WorkspaceList.Building = Convert.ToString(row["Building"]);
                WorkspaceList.City = Convert.ToString(row["City"]);
                WorkspaceList.Country = Convert.ToString(row["Country"]);
                WorkspaceList.State = Convert.ToString(row["State"]);
                WorkspaceList.PostalCode = Convert.ToString(row["PostalCode"]);
                WorkspaceList.LocationString = Convert.ToString(row["LocationString"]);
                if (row["Lat"] != System.DBNull.Value)
                    WorkspaceList.Lat = Convert.ToDouble(row["Lat"]);
                if (row["Long"] != System.DBNull.Value)
                    WorkspaceList.Long = Convert.ToDouble(row["Long"]);
                WorkspaceList.PrimaryContactId = (int?)Convert.ToInt32(row["PrimaryContactId"]);
                WorkspaceList.Description = Convert.ToString(row["Description"]);
                WorkspaceList.MapsUri = Convert.ToString(row["MapsUri"]);
                WorkspaceList.DoFloorsStartAtGround = Convert.ToBoolean(row["DoFloorsStartAtGround"]);
                WorkspaceList.WorkspaceId = Convert.ToInt64(row["WorkspaceId"]);
                WorkspaceList.WorkspaceName = Convert.ToString(row["WorkspaceName"]);
                WorkspaceList.WsDescription = Convert.ToString(row["WsDescription"]);
                WorkspaceList.FriendlyName = Convert.ToString(row["FriendlyName"]);
                WorkspaceList.IdmResourceIdentity = Convert.ToString(row["IdmResourceIdentity"]);
                WorkspaceList.Floor = Convert.ToInt32(row["Floor"]);
                WorkspaceList.WsCity = Convert.ToString(row["WsCity"]);
                WorkspaceList.WsCountry = Convert.ToString(row["WsCountry"]);
                WorkspaceList.WsState = Convert.ToString(row["WsState"]);
                WorkspaceList.WsPostalCode = Convert.ToString(row["WsPostalCode"]);
                WorkspaceList.WsLocationString = Convert.ToString(row["WsLocationString"]);
                if (row["WsLat"] != System.DBNull.Value)
                    WorkspaceList.WsLat = Convert.ToDouble(row["WsLat"]);
                if (row["WsLong"] != System.DBNull.Value)
                    WorkspaceList.WsLong = Convert.ToDouble(row["WsLong"]);
                WorkspaceList.WebLink = Convert.ToString(row["WebLink"]);
                WorkspaceList.PhotoLinks = Convert.ToString(row["PhotoLinks"]);
                WorkspaceList.Ratings = Convert.ToInt32(row["Ratings"]);
                WorkspaceList.Created = Convert.ToDateTime(row["Created"]);
                WorkspaceList.LastUpdated = Convert.ToDateTime(row["LastUpdated"]);
                WorkspaceList.NextReservationStartTime = (row["NextReservationStartTime"] == DBNull.Value) ? (DateTime?)null : ((DateTime)row["NextReservationStartTime"]);
                WorkspaceList.NextreservationEndTime = (row["NextreservationEndTime"] == DBNull.Value) ? (DateTime?)null : ((DateTime)row["NextreservationEndTime"]);
                WorkspaceList.LimitedAccessRoles = Convert.ToString(row["LimitedAccessRoles"]);
                WorkspaceList.LimitedAccessUsers = Convert.ToString(row["LimitedAccessUsers"]);
                WorkspaceList.Tags = Convert.ToString(row["Tags"]);
                WorkspaceList.WorkspaceMapsUri = Convert.ToString(row["WorkspaceMapsUri"]);
                WorkspaceList.WorkspaceType = Convert.ToInt32(row["WorkspaceType"]);
                WorkspaceList.OccupancyStatus = Convert.ToInt32(row["OccupancyStatus"]);
                WorkspaceList.Capacity = Convert.ToInt32(row["Capacity"]);
                WorkspaceList.LastRenovated = (row["LastRenovated"] == DBNull.Value) ? (DateTime?)null : ((DateTime)row["LastRenovated"]);
                WorkspaceList.CommunicationInfo_Email = Convert.ToString(row["CommunicationInfo_Email"]);
                WorkspaceList.CommunicationInfo_PhoneNumber = Convert.ToString(row["CommunicationInfo_PhoneNumber"]);
                WorkspaceList.CommunicationInfo_TelephonyDetails = Convert.ToString(row["CommunicationInfo_TelephonyDetails"]);
                WorkspaceList.CommunicationInfo_AccessDescription = Convert.ToString(row["CommunicationInfo_AccessDescription"]);
                WorkspaceList.CommunicationInfo_OtherInfo = Convert.ToString(row["CommunicationInfo_OtherInfo"]);
                WorkspaceList.IsAutoCheckinEnabled = Convert.ToBoolean(row["IsAutoCheckinEnabled"]);
                WorkspaceList.IsAutoCheckoutEnabled = Convert.ToBoolean(row["IsAutoCheckoutEnabled"]);
                WorkspaceList.AccessType = Convert.ToInt32(row["AccessType"]);
                WorkspaceList.MaintenanceOwner = (long)row["MaintenanceOwner"];
                WorkspaceList.AreaInSqFt = Convert.ToString(row["AreaInSqFt"]);
                WorkspaceList.DistanceFromFloorOrigin = Convert.ToString(row["DistanceFromFloorOrigin"]);
                WorkspaceList.HasControllableDoor = Convert.ToBoolean(row["HasControllableDoor"]);
                WorkspaceList.BeaconUid = Convert.ToString(row["BeaconUid"]);
                WorkspaceList.IsBlocked = Convert.ToBoolean(row["IsBlocked"]);
                WorkspaceList.IsHidden = Convert.ToBoolean(row["IsHidden"]);
                WorkspaceList.ObjectId = Convert.ToString(row["ObjectId"]);
                //WorkspaceList.CapabilitiesId =
                //WorkspaceList.CapabilityId =
                //WorkspaceList.EnablementProcedure =
                WorkspaceList.Name = Convert.ToString(row["Floor"]);
                string CapablitiesJson = Convert.ToString(row["Capabilities"]).Replace("\\", "");
                WorkspaceList.IconUri = JsonConvert.DeserializeObject<List<Capabilites>>(CapablitiesJson);
                //WorkspaceList.CapabilityDescription =
                WorkspaceList.TenantId = Convert.ToString(row["TenantId"]);
                // WorkspaceList.Location GeoLocation =

                BuildingList.Add(WorkspaceList);
            }
        }
        public void Map(SqlDataReader row, Workspace Workspace)
        {
            if (row.Read() && row != null && row.HasRows)
            {
                Workspace.Id = Convert.ToInt64(row["Id"]);
                Workspace.OrgId = Convert.ToInt64(row["OrgId"]);
                Workspace.Name = Convert.ToString(row["Name"]);
                Workspace.Description = Convert.ToString(row["Description"]);
                Workspace.FriendlyName = Convert.ToString(row["FriendlyName"]);
                Workspace.IdmResourceIdentity = Convert.ToString(row["IdmResourceIdentity"]);
                Workspace.Building = new CoreLib.Building
                {
                    Id = Convert.ToInt64(row["BuildingId"]),
                    BuildingName = Convert.ToString(row["Building"]),
                    Address = new Address
                    {
                        Country = Convert.ToString(row["Country"]),
                        State = Convert.ToString(row["State"]),
                        City = Convert.ToString(row["City"]),
                        PostalCode = Convert.ToString(row["PostalCode"]),
                        GpsLocation = new CoreLib.GpsLocation { Lat = Convert.ToDecimal(row["bLat"]), Long = Convert.ToDecimal(row["bLong"]) },
                        Street = Convert.ToString(row["Street"])

                    }
                };
                if (row["Floor"] != System.DBNull.Value)
                    Workspace.Floor = new CoreLib.Floor { Name = Convert.ToString(row["Floor"]) };
                Workspace.Address = new CoreLib.Address
                {
                    Country = Convert.ToString(row["Country"]),
                    State = Convert.ToString(row["State"]),
                    City = Convert.ToString(row["City"]),
                    PostalCode = Convert.ToString(row["PostalCode"]),
                    GpsLocation = new CoreLib.GpsLocation { Location = Convert.ToString(row["LocationString"]), Lat = Convert.ToDecimal(row["Lat"]), Long = Convert.ToDecimal(row["Long"]) },

                };
                Workspace.WebLink = new Uri(Utils.UriExtensions.Isvalid(Convert.ToString(row["WebLink"]), ResourceHelper.GetResources("DefaultUrl")));
                Workspace.Photos = new List<CoreLib.Photo> {
                    new CoreLib.Photo{
                         WebLink= new Uri(Utils.UriExtensions.Isvalid(Convert.ToString(row["PhotoLinks"]),  ResourceHelper.GetResources("DefaultUrl")))
                    }
                };
                Workspace.Ratings = Convert.ToInt32(row["Ratings"]);
                if (row["Created"] != System.DBNull.Value)
                    Workspace.Created = Convert.ToDateTime(row["Created"]);
                if (row["LastUpdated"] != System.DBNull.Value)
                    Workspace.LastUpdated = Convert.ToDateTime(row["LastUpdated"]);
                if (row["NextReservationStartTime"] != System.DBNull.Value)
                    Workspace.NextReservationStartTime = Convert.ToDateTime(row["NextReservationStartTime"]);
                if (row["NextreservationEndTime"] != System.DBNull.Value)
                    Workspace.NextReservationEndTime = Convert.ToDateTime(row["NextreservationEndTime"]);
                if (row["LimitedAccessRoles"] != System.DBNull.Value)
                    Workspace.LimitedAccessRoles = new List<CoreLib.RoleType> { (CoreLib.RoleType)Enum.Parse(typeof(CoreLib.RoleType), (Convert.ToString(row["LimitedAccessRoles"]))) };
                //Workspace.LimitedAccessUsers = new List<User>{ new User
                //   {
                //    Id = Convert.ToInt64(row["LimitedAccessUsers"])
                //   }
                //};
                Workspace.Tags = new Dictionary<string, string> { { Convert.ToString(row["Tags"]), Convert.ToString(row["Tags"]) } };
                Workspace.Maps = new List<Uri> { new Uri(Utils.UriExtensions.Isvalid(Convert.ToString(row["MapsUri"]), ResourceHelper.GetResources("DefaultUrl"))) };
                //Workspace.IconUri = new List<Capabilites> { new Capabilites {IconUri = Resource.DefaultUrl } };
                Workspace.WorkspaceType = (CoreLib.WorkspaceType)Enum.Parse(typeof(CoreLib.WorkspaceType), (Convert.ToString(row["WorkspaceType"])));
                Workspace.OccupancyStatus = (CoreLib.WorkspaceStatusType)Enum.Parse(typeof(CoreLib.WorkspaceStatusType), (Convert.ToString(row["OccupancyStatus"])));
                Workspace.Capacity = Convert.ToInt32(row["Capacity"]);
                if (row["LastRenovated"] != System.DBNull.Value)
                    Workspace.LastRenovated = Convert.ToDateTime(row["LastRenovated"]);
                Workspace.CommunicationInfo = new CoreLib.CommunicationInfo
                {
                    PhoneNumber = Convert.ToString(row["CommunicationInfo_PhoneNumber"]),
                    TelephonyDetails = Convert.ToString(row["CommunicationInfo_TelephonyDetails"]),
                    AccessDescription = Convert.ToString(row["CommunicationInfo_AccessDescription"]),
                    OtherInfo = new Dictionary<string, string> { { Convert.ToString(row["CommunicationInfo_Email"]),
                            Convert.ToString(row["CommunicationInfo_Email"]) } }
                };
                Workspace.IsAutoCheckinEnabled = Convert.ToBoolean(row["IsAutoCheckinEnabled"]);
                Workspace.IsAutoCheckoutEnabled = Convert.ToBoolean(row["IsAutoCheckoutEnabled"]);
                Workspace.AccessType = (CoreLib.WorkspaceAccessType)Enum.Parse(typeof(CoreLib.WorkspaceAccessType), (Convert.ToString(row["AccessType"])));
                Workspace.MaintenanceOwner = new CoreLib.User { Id = Convert.ToInt64(row["MaintenanceOwner"]) };
                Workspace.AreaInSqFt = Convert.ToDecimal(row["AreaInSqFt"]);
                Workspace.DistanceFromFloorOrigin = new Point();
                Workspace.HasControllableDoor = Convert.ToBoolean(row["HasControllableDoor"]);
                Workspace.BeaconUid = Convert.ToString(row["BeaconUid"]);
                Workspace.IsBlocked = Convert.ToBoolean(row["IsBlocked"]);
                Workspace.IsHidden = Convert.ToBoolean(row["IsHidden"]);
                Workspace.ObjectId = Convert.ToString(row["ObjectId"]);
            }
        }
        public void Map(SqlDataReader dr, CoreLib.Organization Org)
        {
            if (dr.Read())
            {
                Org.OrgId = Convert.ToInt64(dr["Id"]);
                Org.FriendlyName = Convert.ToString(dr["FriendlyName"]);
                Org.CorporateName = Convert.ToString(dr["CorporateName"]);
                Org.MeeloLicense = new CoreLib.MeeloLicense { LicenseId = Convert.ToInt64(dr["MeeloLicenseId"]) };
                Org.Address = new CoreLib.Address
                {
                    Street = Convert.ToString(dr["Street"]),
                    Building = Convert.ToString(dr["Building"]),
                    City = Convert.ToString(dr["City"]),
                    Country = Convert.ToString(dr["Country"]),
                    State = Convert.ToString(dr["State"]),
                    PostalCode = Convert.ToString(dr["PostalCode"]),
                    GpsLocation = new CoreLib.GpsLocation { Lat = Convert.ToDecimal(dr["Lat"]), Long = Convert.ToDecimal(dr["Long"]), Location = Convert.ToString(dr["LocationString"]) }
                };
                Org.OrgContact = new CoreLib.User { Mail = Convert.ToString(dr["PrimaryContactId"]) };
                Org.OrgId = Convert.ToInt64(dr["Id"]);
                Org.TenantId = Convert.ToString(dr["TenantId"]);
            }
        }
        public void Map(DataTable dt, CoreLib.Building Bld)
        {
            if (dt!=null && dt.Rows.Count>0)
            {
                DataRow dr = dt.Rows[0];
                Bld.Id = Convert.ToInt64(dr["Id"]);
                Bld.OrgId = Convert.ToInt64(dr["OrgId"]);
                Bld.BuildingName = Convert.ToString(dr["Building"]);
                Bld.Address = new CoreLib.Address
                {
                    Street = Convert.ToString(dr["Street"]),
                    Building = Convert.ToString(dr["Building"]),
                    City = Convert.ToString(dr["City"]),
                    Country = Convert.ToString(dr["Country"]),
                    State = Convert.ToString(dr["State"]),
                    PostalCode = Convert.ToString(dr["PostalCode"]),
                    GpsLocation = new CoreLib.GpsLocation { Lat = Convert.ToDecimal(dr["Lat"]), Long = Convert.ToDecimal(dr["Long"]), Location = Convert.ToString(dr["LocationString"]) }
                };
            }
        }
        
        public void Map(DataTable dt, List<BuildingViewModel> Building)
        {
            IEnumerable<BuildingViewModel> WorkspaceList = dt.AsEnumerable().Select(m => new BuildingViewModel()
            {
                Id = m.Field<long>("BuildingId"),
                OrgId = m.Field<long>("OrgId"),
                Building = m.Field<string>("Building"),
                PostalCode = m.Field<string>("PostalCode"),
                WorkspaceCount = m.Field<int>("WorkspaceCount"),
                gpsLocation = new GpsLocation { Lat = m.Field<decimal>("BuildingLat"), Long = m.Field<decimal>("BuildingLong"), Location = m.Field<string>("BuildingLocationString") }
            });
            Building = WorkspaceList.ToList();
        }
        public void Map(DataTable dt, List<Building> Building)
        {
            foreach (DataRow row in dt.Rows)
            {
                Building Bld = new CoreLib.Building();
                Bld.Id = Convert.ToInt64(row["Id"]);
                Bld.OrgId = Convert.ToInt64(row["OrgId"]);
                Bld.BuildingName = Convert.ToString(row["Building"]);
                Bld.Description = Convert.ToString(row["Description"]);
                Bld.DoFloorsStartAtGround = Convert.ToBoolean(row["DoFloorsStartAtGround"]);
                //if (row["Created"] != System.DBNull.Value)
                //    Bld.Created = Convert.ToDateTime(row["Created"]);
                //if (row["LastUpdated"] != System.DBNull.Value)
                //    Bld.LastUpdated = Convert.ToDateTime(row["LastUpdated"]);
                Bld.Address = new CoreLib.Address
                {
                    Street = Convert.ToString(row["Street"]),
                    Building = Convert.ToString(row["Building"]),
                    City = Convert.ToString(row["City"]),
                    Country = Convert.ToString(row["Country"]),
                    State = Convert.ToString(row["State"]),
                    PostalCode = Convert.ToString(row["PostalCode"]),
                    GpsLocation = new CoreLib.GpsLocation { Lat = Convert.ToDecimal(row["Lat"]), Long = Convert.ToDecimal(row["Long"]), Location = Convert.ToString(row["LocationString"]) }
                };
                Building.Add(Bld);
            }
        }

        public void Map(DataTable dt, List<User> users)
        {

            foreach (DataRow dr in dt.Rows)
            {
                User user = new User();
                user.Id = Convert.ToInt64(dr["Id"]);
                user.OrgId = Convert.ToInt64(dr["OrgId"]);
                user.DisplayName = Convert.ToString(dr["DisplayName"]);
                user.FirstName = Convert.ToString(dr["FirstName"]);
                user.PreferredName = Convert.ToString(dr["PreferredName"]);
                user.JobTitle = Convert.ToString(dr["JobTitle"]);
                user.Mail = Convert.ToString(dr["Mail"]);
                user.MobilePhone = Convert.ToString(dr["MobilePhone"]);
                user.BusinessPhones = new List<string> { Convert.ToString(dr["BusinessPhones"]) };
                user.OfficeLocation = new CoreLib.GpsLocation();
                user.OfficeLocation.Location = Convert.ToString(dr["OfficeLocationString"]);
                user.OfficeLocation.Lat = Convert.ToDecimal(dr["OfficeLocationLat"]);
                user.OfficeLocation.Long = Convert.ToDecimal(dr["OfficeLocationLong"]);

                user.PreferredLanguage = Convert.ToString(dr["Language"]);
                user.Birthday = Convert.ToDateTime(dr["Birthday"]);
                user.Surname = Convert.ToString(dr["Surname"]);
                user.UserPrincipalName = Convert.ToString(dr["UserPrincipalName"]);
                user.AccountEnabled = Convert.ToBoolean(dr["AccountEnabled"]);
                //user.MeeloLicense.LicenseId = (string)dr["[AccountEnabled]"];
                //user.MeeloPlan.PlanId = (string)dr["MeeloPlanId"];
                user.CompanyName = Convert.ToString(dr["CompanyName"]);
                user.DepartmentName = Convert.ToString(dr["DepartmentName"]);
                user.DepartmentId = Convert.ToString(dr["DepartmentId"]);
                user.HireDate = Convert.ToDateTime(dr["HireDate"]);
                user.CollabProviderUniqueId = Convert.ToString(dr["CollabProviderUniqueId"]);
                user.ImAddresses = new Dictionary<string, string> { { Convert.ToString(dr["ImAddresses"]), Convert.ToString(dr["ImAddresses"]) } };
                user.MailboxSettings = new CoreLib.MailboxSettings
                {
                    TimeZone = Convert.ToString(dr["MailboxSettings_Timezone"]),
                    LocaleInfo = new CoreLib.LocaleInfo
                    {
                        Locale = Convert.ToString(dr["MailboxSettings_Locale"]),
                        DisplayName = Convert.ToString(dr["MailboxSettings_Locale_DisplayName"])
                    },
                    AutomaticRepliesSetting = new CoreLib.AutomaticRepliesSetting
                    {
                        ExternalAudience = Convert.ToString(dr["AutomaticRepliesSetting_ExternalAudience"]),
                        ExternalReplyMessage = Convert.ToString(dr["AutomaticRepliesSetting_ExternalReplyMessage"]),
                        InternalReplyMessage = Convert.ToString(dr["AutomaticRepliesSetting_InternalReplyMessage"]),
                        ScheduledStartDateTime = new CoreLib.DateTimeTimeZone { DateTimeOffset = Convert.ToString(dr["AutomaticRepliesSetting_ScheduledStartTime"]) },
                        ScheduledEndDateTime = new CoreLib.DateTimeTimeZone { DateTimeOffset = Convert.ToString(dr["AutomaticRepliesSetting_ScheduledEndTime"]) },
                        Status = Convert.ToString(dr["AutomaticRepliesSetting_Status"])
                    }
                };
                user.Address = new CoreLib.Address
                {
                    Street = Convert.ToString(dr["Street"]),
                    Building = Convert.ToString(dr["Building"]),
                    Floor = Convert.ToString(dr["Floor"]),
                    City = Convert.ToString(dr["City"]),
                    Country = Convert.ToString(dr["Country"]),
                    State = Convert.ToString(dr["State"]),
                    PostalCode = Convert.ToString(dr["PostalCode"])
                };
                user.UsageLocation = new CoreLib.GpsLocation();
                user.UsageLocation.Location = Convert.ToString(dr["UsageLocation_LocationString"]);
                user.UsageLocation.Lat = Convert.ToDecimal(dr["UsageLocation_Lat"]);
                user.UsageLocation.Long = Convert.ToDecimal(dr["UsageLocation_Long"]);
                user.Role = Convert.ToString(dr["Role"]);
                user.PreferredLanguage = Convert.ToString(dr["Language"]);
                user.ObjectId = Convert.ToString(dr["ObjectId"]);
                users.Add(user);
            }
        }
        public void Map(SqlDataReader dr, User user)
        {
            if (dr.Read())
            {
                user.Id = Convert.ToInt64(dr["Id"]);
                user.OrgId = Convert.ToInt64(dr["OrgId"]);
                user.DisplayName = Convert.ToString(dr["DisplayName"]);
                user.FirstName = Convert.ToString(dr["FirstName"]);
                user.PreferredName = Convert.ToString(dr["PreferredName"]);
                user.JobTitle = Convert.ToString(dr["JobTitle"]);
                user.Mail = Convert.ToString(dr["Mail"]);
                user.MobilePhone = Convert.ToString(dr["MobilePhone"]);
                user.BusinessPhones = new List<string> { Convert.ToString(dr["BusinessPhones"]) };
                user.OfficeLocation = new CoreLib.GpsLocation();
                user.OfficeLocation.Location = Convert.ToString(dr["OfficeLocationString"]);
                user.OfficeLocation.Lat = Convert.ToDecimal(dr["OfficeLocationLat"]);
                user.OfficeLocation.Long = Convert.ToDecimal(dr["OfficeLocationLong"]);

                user.PreferredLanguage = Convert.ToString(dr["Language"]);
                user.Birthday = Convert.ToDateTime(dr["Birthday"]);
                user.Surname = Convert.ToString(dr["Surname"]);
                user.UserPrincipalName = Convert.ToString(dr["UserPrincipalName"]);
                user.AccountEnabled = Convert.ToBoolean(dr["AccountEnabled"]);
                //user.MeeloLicense.LicenseId = (string)dr["[AccountEnabled]"];
                //user.MeeloPlan.PlanId = (string)dr["MeeloPlanId"];
                user.CompanyName = Convert.ToString(dr["CompanyName"]);
                user.DepartmentName = Convert.ToString(dr["DepartmentName"]);
                user.DepartmentId = Convert.ToString(dr["DepartmentId"]);
                user.HireDate = Convert.ToDateTime(dr["HireDate"]);
                user.CollabProviderUniqueId = Convert.ToString(dr["CollabProviderUniqueId"]);
                user.ImAddresses = new Dictionary<string, string> { { Convert.ToString(dr["ImAddresses"]), Convert.ToString(dr["ImAddresses"]) } };
                user.MailboxSettings = new CoreLib.MailboxSettings
                {
                    TimeZone = Convert.ToString(dr["MailboxSettings_Timezone"]),
                    LocaleInfo = new CoreLib.LocaleInfo
                    {
                        Locale = Convert.ToString(dr["MailboxSettings_Locale"]),
                        DisplayName = Convert.ToString(dr["MailboxSettings_Locale_DisplayName"])
                    },
                    AutomaticRepliesSetting = new CoreLib.AutomaticRepliesSetting
                    {
                        ExternalAudience = Convert.ToString(dr["AutomaticRepliesSetting_ExternalAudience"]),
                        ExternalReplyMessage = Convert.ToString(dr["AutomaticRepliesSetting_ExternalReplyMessage"]),
                        InternalReplyMessage = Convert.ToString(dr["AutomaticRepliesSetting_InternalReplyMessage"]),
                        ScheduledStartDateTime = new CoreLib.DateTimeTimeZone { DateTimeOffset = Convert.ToString(dr["AutomaticRepliesSetting_ScheduledStartTime"]) },
                        ScheduledEndDateTime = new CoreLib.DateTimeTimeZone { DateTimeOffset = Convert.ToString(dr["AutomaticRepliesSetting_ScheduledEndTime"]) },
                        Status = Convert.ToString(dr["AutomaticRepliesSetting_Status"])
                    }
                };
                user.Address = new CoreLib.Address
                {
                    Street = Convert.ToString(dr["Street"]),
                    Building = Convert.ToString(dr["Building"]),
                    Floor = Convert.ToString(dr["Floor"]),
                    City = Convert.ToString(dr["City"]),
                    Country = Convert.ToString(dr["Country"]),
                    State = Convert.ToString(dr["State"]),
                    PostalCode = Convert.ToString(dr["PostalCode"])
                };
                user.UsageLocation = new CoreLib.GpsLocation();
                user.UsageLocation.Location = Convert.ToString(dr["UsageLocation_LocationString"]);
                user.UsageLocation.Lat = Convert.ToDecimal(dr["UsageLocation_Lat"]);
                user.UsageLocation.Long = Convert.ToDecimal(dr["UsageLocation_Long"]);
                user.Role = Convert.ToString(dr["Role"]);
                user.PreferredLanguage = Convert.ToString(dr["Language"]);
                user.ObjectId = Convert.ToString(dr["ObjectId"]);
            }
        }
        public void Map(DataTable dt, List<Event> EventList)
        {
            foreach (System.Data.DataRow row in dt.Rows)
            {
                Event EventObj = new Event();
                EventObj.Workspace = new Workspace
                {
                    Id = (long)row["WorkspaceId"],
                    Name = Convert.ToString(row["WorkspaceName"]),
                    PhotoLinks = Convert.ToString(row["PhotoLinks"]),
                    //IconUri = JsonConvert.DeserializeObject<List<Capabilites>>(Convert.ToString(row["Capabilities"]).Replace("\\", ""))
                };

                EventObj.Subject = Convert.ToString(row["Subject"]);
                EventObj.CreatedDateTime = new DateTime(Convert.ToDateTime(row["CreatedTime"]).Ticks, DateTimeKind.Utc);
                EventObj.StartTime = new DateTime(Convert.ToDateTime(row["StartTime"]).Ticks, DateTimeKind.Utc);
                EventObj.EndTime = new DateTime(Convert.ToDateTime(row["EndTime"]).Ticks, DateTimeKind.Utc);
                EventObj.Body = Convert.ToString(row["Body"]);
                EventObj.BodyPreview = Convert.ToString(row["BodyPreview"]);
                EventObj.Categories = new List<string> { Convert.ToString(row["Categories"]) };
                EventObj.iCalUId = Convert.ToString(row["iCalUid"]);
                EventObj.Id = (long)row["EventId"];
                EventObj.RefId = Convert.ToString(row["RefId"]);
                EventObj.Importance = (ImportanceType)Enum.Parse(typeof(ImportanceType), Convert.ToString(row["Importance"]));
                EventObj.PrimaryOwner = new User { Id = (long)row["UserId"], OrgId = (long)row["OrgId"] };
                //EventObj.IsOrganizer = true;
                EventObj.PatterenedRecurrence = new PatterenedRecurrrence { RecurrencePattern = new RecurrencePattern { }, RecurrenceRange = new RecurrenceRange { } };
                EventObj.Sensitivity = (SensitivityType)Enum.Parse(typeof(SensitivityType), Convert.ToString(row["Sensitivity"]));
                EventObj.Type = (EventType)Enum.Parse(typeof(EventType), Convert.ToString(row["EventType"]));
                EventObj.WebLink = new Uri(Convert.ToString(row["WebLink"]));
                EventObj.Attendees = new List<Attendee> { };
                EventObj.Services = new List<IService> { };
                EventObj.Tags = new Dictionary<string, string> { { Convert.ToString(row["Tags"]), Convert.ToString(row["Tags"]) } };
                if (Convert.ToString(row["CheckinTime"]).IsNullOrEmpty() == false)
                    EventObj.CheckinTime = new DateTime(Convert.ToDateTime(row["CheckinTime"]).Ticks, DateTimeKind.Utc);
                if (Convert.ToString(row["CheckoutTime"]).IsNullOrEmpty() == false)
                    EventObj.CheckoutTime = new DateTime(Convert.ToDateTime(row["CheckoutTime"]).Ticks, DateTimeKind.Utc);
                if (Convert.ToString(row["CheckinPerformedBy"]).IsNullOrEmpty() == false)
                    EventObj.CheckinPerformedBy = Convert.ToInt64(row["CheckinPerformedBy"]);
                if (Convert.ToString(row["CheckoutPerformedBy"]).IsNullOrEmpty() == false)
                    EventObj.CheckoutPerformedBy = Convert.ToInt64(row["CheckoutPerformedBy"]);
                EventObj.OnlineMeetingUrl = Convert.ToString(row["OnlineMeetingUrl"]);
                EventObj.EventCalendar = new Calendar { Id = (long)row["CalendarId"] };
                EventObj.CalenderEventId = Convert.ToString(row["CalenderEventId"]);
                EventList.Add(EventObj);
            }
        }
    }
}

