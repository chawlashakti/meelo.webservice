﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.IO;
using System.IO.Compression;

namespace Com.Meelo.Utils
{
    //TODO: http://dotnetslackers.com/SQL/re-689774_String_Matching_in_LINQ.aspx
    public static class StringExtensions
    {
        #region Class Methods

        public static string FormatWith(this string source, params object[] parameters)
        {
            return string.Format(CultureInfo.InvariantCulture, source, parameters);
        }

        public static bool IsNullOrEmpty(this string source)
        {
            return string.IsNullOrEmpty(source);
        }

        public static string Max(this string source, int maxLength)
        {


            return source.Length > maxLength ? source.Substring(0, maxLength) : source;
        }

        /// <summary>
        /// Compress a string into a byte array.
        /// </summary>
        /// <param name="str">String to compress.</param>
        /// <returns>Compressed byte array.</returns>
        public static byte[] Compress(this string str)
        {
            var bytes = Encoding.UTF8.GetBytes(str);

            using (var msi = new MemoryStream(bytes))
            using (var mso = new MemoryStream())
            {
                using (var gs = new GZipStream(mso, CompressionMode.Compress))
                {
                    msi.CopyTo(gs);
                }
                return mso.ToArray();
            }
        }

        /// <summary>
        /// Decompress a string from a byte array.
        /// </summary>
        /// <param name="bytes">Compressed string.</param>
        /// <returns>Uncompressed string.</returns>
        public static string Decompress(this byte[] bytes)
        {
            using (var msi = new MemoryStream(bytes))
            using (var mso = new MemoryStream())
            {
                using (var gs = new GZipStream(msi, CompressionMode.Decompress))
                {
                    gs.CopyTo(mso);
                }
                return Encoding.UTF8.GetString(mso.ToArray());
            }
        }
        #endregion
    }

}
