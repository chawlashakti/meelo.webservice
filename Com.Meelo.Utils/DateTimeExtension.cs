﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.Utils
{
    public static class DateTimeExtension
    {
        /// <summary>
        /// Generates the format "yyyy-MM-dd HH':'mm':'ss"
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static string ToIsoReadable(this DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-dd HH':'mm':'ss");
        }

        /// <summary>
        /// Generate the format "yyyy-MM-dd HH':'mm':'ss"
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static string ToIsoReadable(this DateTimeOffset dateTime)
        {
            return dateTime.ToString("yyyy-MM-dd HH':'mm':'ss");
        }
    }
}
