﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Meelo.Utils
{
    public static class EnumExtensions
    {
        public static T ToEnum<T>(this string enumString)
        {
            return (T)Enum.Parse(typeof(T), enumString);
        }

        public static T ToEnum<T>(this int enumInt)
        {
            return enumInt.ToString().ToEnum<T>();
        }

        public static T ToEnum<T>(this long enumLong)
        {
            return enumLong.ToString().ToEnum<T>();
        }

        public static T ToEnum<T>(this short enumShort)
        {
            return enumShort.ToString().ToEnum<T>();
        }

        public static T ToEnum<T>(this byte enumByte)
        {
            return (T)Enum.ToObject(typeof(T), enumByte);
        }

        public static T ToEnum<T>(this object enumObject)
        {
            return (T)Enum.ToObject(typeof(T), enumObject);
        }

        public static int ToInt(this Enum enumValue)
        {
            return Convert.ToInt32(enumValue);
        }
        public static long ToLong(this Enum enumValue)
        {
            return Convert.ToInt64(enumValue);
        }

        public static byte ToByte(this Enum enumValue)
        {
            return Convert.ToByte(enumValue);
        }

        public static short ToShort(this Enum enumValue)
        {
            return Convert.ToInt16(enumValue);
        }
    }
}
