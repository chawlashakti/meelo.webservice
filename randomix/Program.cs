﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tectil.NCommand.Contract;
using Tectil.NCommand;
using Com.Meelo.Normalizers;

namespace randomix
{
    /// <summary>
    /// https://github.com/tectil/NCommand
    /// Create random data
    /// </summary>
    class Program
    {
        [Command(description: "Create random workspaces.", Name ="createworkspaces")]
        public bool CreateWorkspaces(
            [Argument(description: "Name of the organization [default:meeloinc]", defaultValue:"meeloinc")] string orgname,
            [Argument(description: "Total number of objects to be created", defaultValue: 100)] long count
        )
        {
            // Create random objects
            return true;
        }

     
        static void Main(string[] args)
        {
            NCommands commands = new NCommands();
            commands.RunConsole(args);
        }
    }
}
